/* usbUhcdRhEmulate.c - USB UHCI HCD Roothub Emulation */

/*
 * Copyright (c) 2003-2004, 2007-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2004, 2007-2011 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01s,01aug11,ghs  Fix issue found during code review (WIND00255117)
01r,28jul10,ghs  Fix uhci exit issue(WIND00224419)
01q,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01p,09mar10,j_x  Changed for USB debug (WIND00184542)
01o,13jan10,ghs  vxWorks 6.9 LP64 adapting
01n,17sep09,y_l  Code Coverity CID(5196): Using if-else sentence instead
                 2-case-switch sentence (WIND00182326)
01m,10sep09,y_l  Code Coverity CID(19): USB setup packet parameter check error,
                 (WIND00176509)
01l,22jul09,ghs  Replace OS_DELAY_MS(1) during reset port using usbUhciDelay
                 instead(WIND00173947).
01k,18mar09,w_x  Correct reset signal timing by using OS_DELAY_MS (WIND00159391)
01j,27feb09,w_x  Add workaround for WIND00105458
01i,14aug08,w_x  Remove unnecessary port manage task and clean up root hub
                 emulation code (WIND00130272)
01h,07jul08,w_x  Code coverity changes (WIND00126885)
01g,04jun08,w_x  Changed URB callback to be called directly.(WIND00121282 fix)
01f,19may07,jrp  replaced taskSpawns with jobQueueAdd. WIND00090426
01e,15oct04,ami  Refgen Changes
01d,12Sep03,nrv  Status change
01c,28jul03,mat  Endian Related Changes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains routines which essentialy form a wrapper around
UHCI's root hub so as to make it appear as an ordinary hub.

INCLUDE FILES: usb2/usbOsal.h, usb2/usbUhcdCommon.h, usb2/usbHst.h,
usb2/usbUhcdScheduleQueue.h, usb2/usbUhcdSupport.h, usb2/BusAbstractionLayer.h,
usb2/usbUhcdScheduleQSupport.h, usb2/usbUhci.h

*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdRhEmulate.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains routines which essentialy
 *                     form a wrapper around UHCI's root hub so as to
 *                     make it appear as an ordinary hub.
 *
 *
 ******************************************************************************/


/* includes */

#include <usb/usbOsal.h>
#include "usbUhcdCommon.h"
#include <usb/usbHst.h>
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdSupport.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdScheduleQSupport.h"
#include "usbUhci.h"


/* defines */

#define INCLUDED_FROM_RH_EMULATE
#include "usbUhcdRhEmulate.h"
#undef  INCLUDED_FROM_RH_EMULATE


#define USB_UHCD_REQUEST_FAILURE        ERROR

/* To hold the number of ports supported by the Root hub */

#define USB_UHCD_ROOT_HUB_PORTS         0x02

/* define hub descriptor type value */

#define USB_UHCD_HUB_DESCRIPTOR         0x29

/* define Hub Characteristics */

#define USB_UHCD_GANGED_POWER_SWITCHING        0x0000
#define USB_UHCD_INDIVIDUAL_PORT_POWER         0x0001
#define USB_UHCD_HUB_NOT_COMPOUND_DEV          0x0000
#define USB_UHCD_HUB_COMPOUND_DEV              0x0004
#define USB_UHCD_GLOBAL_OVER_CURRENT_PROT      0x0000
#define USB_UHCD_INDIVIDUAL_PORT_OVER_CUR_PROT 0x0008
#define USB_UHCD_NO_OVER_CURRENT_PROT          0x0010

#define USB_UHCD_HUB_REMOVABLE                 0x00
#define USB_UHCD_HUB_NON_REMOVABLE             0x01
#define USB_UHCD_PORT_PWR_CTRL_MASK            0xFF

void usbUhcdCopy(UCHAR *, UCHAR *, UINT *, UINT);


/*******************************************************************************
*
* usbUhcdCopy - copy the required or maximum number of bytes
*
* This routine copies the required or maximum number of bytes from the source
* to the target.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdCopy
    (
    UCHAR *target,
    UCHAR *source,
    UINT *required,
    UINT max
    )
    {

    /*
     * Check if the required pointer is NULL or
     * the required number of bytes is greater than
     * the maximum number of bytes requested
     */

    if (required == NULL || (*required > max) ||  (*required == 0) )
        {
        USB_UHCD_VDBG("usbUhcdCopy - required pointer is NULL or > MAX \n",
                      1, 2, 3, 4, 5, 6);

        /* Copy maximum number of bytes */

        OS_MEMCPY (target, source, max);

        /* update *required to indicate that only max bytes were copied */

        if (required != NULL)
            {
            USB_UHCD_VDBG("usbUhcdCopy - required pointer is > MAX \n",
                          1, 2, 3, 4, 5, 6);

            *required = max;
            }
        }

    /* Copy the required number of bytes from the source to the target */

    else
        {
        USB_UHCD_VDBG("usbUhcdCopy - required pointer is copied \n",
                      1, 2, 3, 4, 5, 6);

        OS_MEMCPY (target, source, *required);
        }

    return;

    }/* End of usbUhcdCopy() */


/*******************************************************************************
*
* usbUhcdGetPortStatus - handle GET_PORT_STATUS request for Root hub
*
* This routine handles GET_PORT_STATUS hub specific request for the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving the port status
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetPortStatus
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {

    /* To hold the status of the hub */

    UINT32 status = 0;

    /* To hold the offset of the register */

    UINT32 offset = 0;

    /* To hold the port number */

    UINT8 port = 0;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;


    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and the DeviceRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetPortStatus - invalid parameters\n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Get the port index */

    port = (UINT8)wIndex;

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid port, invalid DevRequest parameters
     */

    if (port < USB_UHCD_PORT_1 ||
        port > USB_UHCD_PORT_2 ||
        wLength != USB_UHCD_BYTE_TX_DATA_STAGE ||
        wValue != 0)
        {
        USB_UHCD_ERR("usbUhcdGetPortStatus - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Get the correct offset based on port number */

    offset = (UINT32)((port == 1)? (USB_UHCD_PORT1) : (USB_UHCD_PORT2));

    /*
     * The hub's port status contains 4 bytes of information, out of which
     * the hub's port status is reported in the byte offsets 0 to 1
     * and the hub's port status change in the byte offsets 2 to 3
     */

    /* Update the port status change bytes - Start */

    /*
     * Following is the interpretation of the 16 bits of port status change,
     * which is being updated using the UHCI register
     * Bit 5-15 Reserved
     * Bit 4 - Reset change
     * Bit 3 - Overcurrent indicator Change
     * Bit 2 - Suspend Change
     * Bit 1 - Port Enable/Disable change
     * Bit 0 - Connect status change
     */

    /* Update the reset change bit */

    status |= pHCDData->usbUhcdResetChange[port-1];
    status <<= 1;

    /*
     * Update the overcurrent change bit
     * (ie) 0 since there is no overcurrent indication in UHCI
     */

    status |= 0;
    status <<= 1;

    /*
     * Update the suspend change bit
     * (ie) 0 since there is no suspend-change indication in UHCI
     */

    status |= 0;
    status <<= 1;

    /* Store enable/disable change bit */

    status |= usbUhcdGetBit(pHCDData, (UINT8)offset, 3);
    status <<= 1;

    /* Store connect status change */

    if (pHCDData->forceReconnect[port - 1] == TRUE)
        {
        USB_UHCD_VDBG("usbUhcdGetPortStatus - "
            "port %d got force reconnect, savedPortStatus %p \n",
            port, pHCDData->savedPortStatus[port - 1], 3, 4, 5, 6);

        status |= 1;

        pHCDData->forceReconnect[port - 1] = FALSE;
        }
    else
        {
        status |= usbUhcdGetBit(pHCDData, (UINT8)offset, 1);
        }

    status <<= 1;

    /* Update the port status change bytes - End */

    /* Update the port status bytes - Start */
    /*
     * Following is the interpretation of the 16 bits of port status
     * Bit 10-15 Reserved
     * Bit 9 - Low speed device is attached
     * Bit 8 - Port is powered on
     * Bit 5-7 Reserved
     * Bit 4 - Reset signalling asserted
     * Bit 3 - An overcurrent condition exists on the port
     * Bit 2 - Port is suspended
     * Bit 1 - Port is enabled
     * Bit 0 - Device is present on the port
     */

    /* Reserved .. fill next 6 bits with 0 */

    status <<= 6;

    /* Check whether a Low speed device is connected and update the status */

    status |= usbUhcdGetBit (pHCDData, (UINT8)offset, 8);
    status <<= 1;

    /* Store 1 (ie) ports are always powered on .. no power switching in UHCI */

    status |= 1;
    status <<= 1;

    /* Next 3 bits are 0 .. reserved */

    status <<= 3;

    /* Read the bit indicating whether a reset is asserted. Update the status */

    status |= usbUhcdGetBit (pHCDData, (UINT8)offset, 9);
    status <<= 1;

    /* Over current condition... store 0 ... no indication in UHCI */

    status |= 0;
    status <<= 1;

    /* If port is enabled, only then do a port suspend check */

    if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, 2))
        {
        USB_UHCD_VDBG("usbUhcdGetPortStatus - checking port %d suspend status\n",
                      port, 2, 3, 4, 5, 6);

        status |= usbUhcdGetBit ( pHCDData, (UINT8)offset, 12);
        status <<= 1;
        }

    /* if port is not enabled, port cannot be suspended */

    else
        {
        USB_UHCD_WARN("usbUhcdGetPortStatus - port %d is not enabled\n",
                      port, 2, 3, 4, 5, 6);

        /* port Suspend = 0 */

        status |= 0;
        status <<= 1;
        }

    /* Check if port is enabled and update the status */

    status |= usbUhcdGetBit (pHCDData, (UINT8)offset, 2);
    status <<= 1;

    /* Check if there is a device connected and update the status */

    status |= usbUhcdGetBit (pHCDData, (UINT8)offset, 0);

    /* status has to sent in LITTLE ENDIAN format */

    status = OS_UINT32_CPU_TO_LE(status);

    /* Update the port status bytes - End */

    /* Copy the status information generated into URB's buffer */

    usbUhcdCopy (pUrb->pTransferBuffer, (UCHAR *) &status, NULL, 4);

    /* Return the status */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetPortStatus() */

/*******************************************************************************
*
* usbUhcdSetPortFeature - handle SET_PORT_FEATURE request for Root hub
*
* This routine handles the SET_PORT_FEATURE hub specific request for Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in setting the port status
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdSetPortFeature
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {

    /* To hold the offset of the register */

    UINT32 offset = 0;

    /* To hold the port number */

    UINT8  port = 0;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    UINT16 portsc = 0;

    UINT32 count = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and the DevRequest pointers are valid */

    if (NULL == pUrb || NULL == pUrb->pTransferSpecificData)
        {
        USB_UHCD_ERR("usbUhcdSetPortFeature - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Extract the port number from URB's DevRequest */

    port = (UINT8)wIndex;

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid port, invalid DevRequest parameters
     */

    if (port < USB_UHCD_PORT_1 ||
        port > USB_UHCD_PORT_2 ||
        wLength != 0)
        {
        USB_UHCD_ERR("usbUhcdSetPortFeature - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* The parameters are valid */

    else
        {
        /* Get the correct offset based on port number */

        offset = (UINT32)((port == 1) ? (USB_UHCD_PORT1) : (USB_UHCD_PORT2));

        USB_UHCD_VDBG("usbUhcdSetPortFeature - set port feature \n",
                      1, 2, 3, 4, 5, 6);

        /* Switch on the feature to be selected */

        switch (wValue)
            {
            case USB_UHCD_PORT_RESET: /* If port reset */
                {
                USB_UHCD_VDBG("usbUhcdSetPortFeature - port reset \n",
                              1, 2, 3, 4, 5, 6);
                /*
                 * Clear the enable condition on the port.
                 * This is done as by giving a reset signal the port
                 * would be enabled
                 */

                usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PE);

                /*
                 * Give a minimum debounce time (100ms is minimum)
                 * Additional 10ms is given here for software delays.
                 */

                /* Assert a reset on the port */

                usbUhcdPortSetBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PR);

                /*
                 * The reset signal should be driven atleast for 50ms
                 * from the Root hub.
                 * Additional 10ms is given here for software delays.
                 */

                /*
                 * 7.1.7.5 Reset Signaling
                 *
                 * A hub signals reset to a downstream port by driving an
                 * extended SE0 at the port. After the reset is removed, the
                 * device will be in the Default state (refer to Section 9.1).
                 *
                 * The reset signaling can be generated on any Hub or Host
                 * Controller port by request from the USB System Software.
                 * The reset signaling must be driven for a minimum of 10ms
                 * (TDRST). After the reset, the hub port will transition to
                 * the Enabled state (refer to Section 11.5).
                 *
                 * As an additional requirement, Host Controllers and the
                 * USB System Software must ensure that resets issued to
                 * the root ports drive reset long enough to overwhelm any
                 * concurrent resume attempts by downstream devices. It
                 * is required that resets from root ports have a duration
                 * of at least 50 ms (TDRSTR). It is not required that this
                 * be 50 ms of continuous Reset signaling. However, if the
                 * reset is not continuous, the interval(s) between reset
                 * signaling must be less than 3 ms (TRHRSI), and the
                 * duration of each SE0 assertion must be at least 10 ms
                 * (TDRST).
                 */

                OS_DELAY_MS(60);

                /* Update the reset change field in the HCD structure */

                pHCDData->usbUhcdResetChange[port - 1] = 1;

                /* Stop reset signalling on the port */

                usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PR);

                /*
                 * As OS_DELAY_MS can not delay less than 1 tick,
                 * use UCHI frame to delay.
                 */

                usbUhciDelay(pHCDData, 1);

                /* Enable the port */

                usbUhcdPortSetBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PE);

                /* Give a settling time */

                /*
                 * Port Enabled/Disabled R/W. 1=Enable. 0=Disable.
                 *
                 * Ports can be enabled by host software only.
                 * Ports can be disabled by either a fault condition
                 * (disconnect event or other fault condition) or by host
                 * software. Note that the bit status does not change
                 * until the port state actually changes and that there
                 * may be a delay in disabling or enabling a port if
                 * there is a transaction currently in progress on
                 * the USB.
                 */

                do
                    {
                    portsc = (UINT16)usbUhcdReadReg(pHCDData, (UINT8)offset);

                    if (portsc & (1 << USB_UHCD_PORTSC_PE))
                        break;

                    usbUhciDelay(pHCDData, 1);

                    count++;

                    if (count > 10)
                        break;

                    } while (!(portsc & (1 << USB_UHCD_PORTSC_PE)));

                /* Clear the port staus change */

                usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_CSC);

                /* Clear the Port Enable change bit */

                usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PEC);

                break;
                }/* End of case USB_UHCD_PORT_RESET */

            case USB_UHCD_PORT_SUSPEND:/* If port suspend */
                {
                USB_UHCD_VDBG("usbUhcdSetPortFeature - port suspend \n",
                              1, 2, 3, 4, 5, 6);

                /*
                 * Suspend the port only if port is enabled
                 * and global suspend is inactive
                 */

                if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PE) &&
                    usbUhcdIsBitReset (pHCDData, USB_UHCD_USBCMD, USB_UHCD_PORTSC_PEC))
                    {
                    USB_UHCD_VDBG("usbUhcdSetPortFeature - "
                        "suspend for enable port\n",
                        1, 2, 3, 4, 5, 6);

                    /* Set the bit to suspend the port */

                    usbUhcdPortSetBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_SUSP);
                    }
                else
                    {
                    USB_UHCD_ERR("usbUhcdSetPortFeature - "
                        "suspend for un-enabled port \n",
                        1, 2, 3, 4, 5, 6);

                    return USB_UHCD_FAIL;
                    }
                break;
                }/* End of case USB_UHCD_PORT_SUSPEND */

                /* The following features are routineal no-operations */

            case USB_UHCD_PORT_POWER:          /* Port Power */
            case USB_UHCD_C_PORT_CONNECTION:   /* c_port_connection */
            case USB_UHCD_C_PORT_ENABLE:      /* c_port_enable */
            case USB_UHCD_C_PORT_SUSPEND :      /* c_port_suspend */
            case USB_UHCD_C_PORT_OVERCURRENT:  /* c_port_over_current */
            case USB_UHCD_C_PORT_RESET:        /* c_port_reset */
                {
                USB_UHCD_VDBG("usbUhcdSetPortFeature - c_port reset \n",
                              1, 2, 3, 4, 5, 6);
                break;
                }
            default:
                {
                USB_UHCD_ERR("usbUhcdSetPortFeature - invalid request \n",
                             1, 2, 3, 4, 5, 6);
                return USB_UHCD_FAIL;
                }/* End of default */
            }/* End of switch () */
        }/* End of else */

    /* Return success from the routine */

    return USB_UHCD_PASS;
    }/* End of usbUhcdSetPortFeature() */

/*******************************************************************************
*
* usbUhcdClearPortFeature - handle CLEAR_PORT_FEATURE request for Root hub
*
* This routine handles the CLEAR_PORT_FEATURE hub specific request for Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in clearing the port status
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdClearPortFeature
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {

    /* To hold the offset of the port register */

    UINT32 offset = 0;

    /* To hold the port number */

    UINT8 port = 0 ;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and the DevReqest pointers are valid */

    if (NULL == pUrb || NULL == pUrb->pTransferSpecificData)
        {
        USB_UHCD_VDBG("usbUhcdClearPortFeature - invalid parameters \n",
                      1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Extract the port number from URB */

    port = (UINT8)wIndex;

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid port, invalid DevRequest parameters
     */

    if (port < USB_UHCD_PORT_1 ||
        port > USB_UHCD_PORT_2 ||
        wLength != 0)
        {
        USB_UHCD_VDBG("usbUhcdClearPortFeature - invalid conditions \n",
                      1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* The parameters are ok */

    else
        {
        /* Get the correct offset based on port number */

        offset = (UINT32)((port == 1)? (USB_UHCD_PORT1) : (USB_UHCD_PORT2));

        USB_UHCD_VDBG("usbUhcdClearPortFeature - clear port feature \n",
                      1, 2, 3, 4, 5, 6);

        /* Switch on the feature selected */

        switch (wValue)
            {
            case USB_UHCD_PORT_ENABLE:/* Port enable */
                {
                USB_UHCD_VDBG("usbUhcdClearPortFeature - port enable\n",
                              1, 2, 3, 4, 5, 6);

                usbUhcdPortClearBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_CSC);

                break;
                }/* End of case USB_UHCD_PORT_ENABLE */
            case USB_UHCD_PORT_SUSPEND:/* Port Suspend */
                {
                USB_UHCD_VDBG("usbUhcdClearPortFeature - port suspend \n",
                              1, 2, 3, 4, 5, 6);

                /*
                 * Suspend can be cleared only if the port is enabled
                 * & suspended
                 */

                if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PE) &&
                    usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_SUSP))
                    {
                    /* If the HC is suspended, then clear that first */

                    if (usbUhcdIsBitSet (pHCDData, USB_UHCD_USBCMD, USB_UHCD_PORTSC_PEC))
                        {
                        /* Force global resume */

                        usbUhcdRegxSetBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_FGR);

                        /*
                         * A mininum of 20ms is required between setting and
                         * clearing the global resume
                         * Additional 10ms is given for the software delays.
                         */

                        OS_DELAY_MS(50);

                        /* Clear global resume */

                        usbUhcdRegxClearBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_FGR);
                        }

                    /* Drive port resume */

                    usbUhcdPortSetBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_RD);

                    /*
                     * A minimum of 20ms is required between setting and
                     * clearing the global resume
                     * Addition 10ms is given for software delays.
                     */

                    OS_DELAY_MS(30);

                    /* Stop port resume */

                    usbUhcdPortClearBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_RD);
                    }

                break;
                }/* End of case USB_UHCD_PORT_SUSPEND */

            case USB_UHCD_C_PORT_CONNECTION:/* if C_PORT_CONNECTOION */
                {
                USB_UHCD_VDBG("usbUhcdClearPortFeature - c_port connect \n",
                              1, 2, 3, 4, 5, 6);

                /* If there is a connect status change, clear it */

                if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_CSC) )
                    {
                    usbUhcdPortClearBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_CSC);
                    }

                break;
                }/* End of case USB_UHCD_C_PORT_CONNECTION */
            case USB_UHCD_C_PORT_ENABLE:/* C_PORT_ENABLED */
                {
                USB_UHCD_VDBG("usbUhcdClearPortFeature - c_port enable \n",
                              1, 2, 3, 4, 5, 6);

                /* If there is a Enable/Disable change, clear it */

                if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PEC))
                    {
                    usbUhcdPortClearBit (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PEC);
                    }

                break;
                }/* End of case USB_UHCD_C_PORT_ENABLED */
            case USB_UHCD_C_PORT_RESET:/* C_PORT_RESET */
                {
                USB_UHCD_VDBG("usbUhcdClearPortFeature - c_port reset \n",
                              1, 2, 3, 4, 5, 6);

                /* Clear the HCD maintained reset change flag */

                pHCDData->usbUhcdResetChange [port - 1] = 0;
                break;
                }/* End of case USB_UHCD_C_PORT_RESET */

                /* The feature listed below are a routineal no operation */

            case USB_UHCD_PORT_POWER:         /* Port Power */
            case USB_UHCD_C_PORT_OVERCURRENT: /* c_port_over_current */
            case USB_UHCD_C_PORT_SUSPEND :     /* c_port_suspend */
            case USB_UHCD_PORT_INDICATOR:     /* Port Indicator */
                {
                pUrb->nStatus = USBHST_SUCCESS;

                USB_UHCD_VDBG("usbUhcdClearPortFeature - c_port indicator \n",
                              1, 2, 3, 4, 5, 6);
                break;
                }
            default:
                {
                USB_UHCD_ERR("usbUhcdClearPortFeature - invalid request \n",
                             1, 2, 3, 4, 5, 6);
                return USB_UHCD_FAIL;
                }/* End of default */
            }/* End of switch */
        }/* End of else */

    return USB_UHCD_PASS;
    }/* End of usbUhcdClearPortFeature() */


/*******************************************************************************
*
* usbUhcdGetHubDescriptor - handle GET_HUB_DESCRIPTOR request for Root hub
*
* This routine handles the GET_HUB_DESCRIPTOR hub specific request for
* the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving the hub descriptor
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetHubDescriptor
    (
    USBHST_URB *    pUrb
    )
    {

    /* To hold the descriptor string */

    UCHAR desc[13];

    /* To hold the number of ports supported by the Root hub */

    UINT8 nport = USB_UHCD_ROOT_HUB_PORTS;

    /*
     * To hold the number of bytes of the hub descriptor
     * This is done as the number of bytes is dependent on the number
     * of downstream ports supported by the Root hub.
     */

    UINT8 nPortBytes = 0;

    /* To hold the index into the ports */

    UINT8 i = 0;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetHubDescriptor - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);
        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid DevRequest parameters
     */

    if (wValue != 0x2900 ||
        wIndex != 0)
        {
        USB_UHCD_ERR("usbUhcdGetHubDescriptor - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);
        return USB_UHCD_FAIL;
        }

    /*
     * Determine the number of bytes that the descriptor would occupy
     * ie. the num of bytes req to accomodate info about the ports
     */

    nPortBytes = nport / 8;

    if (nport % 8)
        nPortBytes++;

    /* Population of the values of hub descriptor - Start */

    /* Length of the descriptor */

    desc[0] = (UINT8)(7 + (nPortBytes * 2));

    /* Hub Descriptor type */

    desc[1] = USB_UHCD_HUB_DESCRIPTOR;

    /* Number of downstream ports */

    desc[2] = nport;

    /*
     * The following 2 bytes give the hub characteristics
     * The Root hub doesn't provide any overcurrent protection
     */

    desc[3] = USB_UHCD_INDIVIDUAL_PORT_POWER | \
              USB_UHCD_HUB_NOT_COMPOUND_DEV  | \
              USB_UHCD_GLOBAL_OVER_CURRENT_PROT;

    desc[4] = 0;

    /* The power On to Power Good Time for the Root hub is 1 * 2ms */

    desc[5] = 1;

    /* There are no specific maximim current requirements */

    desc[6] = 0;

    /* The last few bytes of the descriptor is based on the number of ports */

    for (i = 0; i < nPortBytes; i++)
        {

        /* Indicates whether the hub is removable */

        desc [7+i] = USB_UHCD_HUB_REMOVABLE;

        /* Port power control mask should be 1 for all the ports */

        desc [7+nPortBytes+i] = USB_UHCD_PORT_PWR_CTRL_MASK;
        }

    /* Population of the values of hub descriptor - End */

    /* Copy the descriptor to URB's buffer */

    usbUhcdCopy (pUrb->pTransferBuffer, desc, &(pUrb->uTransferLength), desc[0]);

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetHubDescriptor() */


/*******************************************************************************
*
* usbUhcdGetHubStatus - handle GET_HUB_STATUS request for Root hub
*
* This routine handles the GET_HUB_STATUS hub specific request for the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving the hub status
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetHubStatus
    (
    USBHST_URB *    pUrb
    )
    {

    /* To hold the hub status string */

    UCHAR buf[2] = {0, 0};

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    /* Check if the URB and DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetHubStatus - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid DevRequest parameters
     */

    if (wLength != 2 ||
        wValue != 0 ||
        wIndex != 0)
        {
        USB_UHCD_ERR("usbUhcdGetHubStatus - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Copy the hub status to URB's buffer */

    usbUhcdCopy (pUrb->pTransferBuffer, buf, NULL, 2);

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetHubStatus () */


/*******************************************************************************
*
* usbUhcdGetBusState - handle GET_BUS_STATE request for Root hub
*
* This routine handles the GET_BUS_STATE hub specific request for the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving the bus state
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetBusState
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {

    /* To hold the Bus State 1 byte information */

    UCHAR buf=0;

    /* To hold the offset of the register */

    UINT8 offset = 0;

    /* To hold the port number */

    UINT8 port = 0;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and the DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetBusState - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Extract the port number from URB */

    port = (UINT8)wIndex;

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid port number
     */

    if (port < USB_UHCD_PORT_1  ||  port > USB_UHCD_PORT_2)
        {
        USB_UHCD_ERR("usbUhcdGetBusState - invalid port number %d \n",
                     port, 2, 3, 4, 5, 6);
        return USB_UHCD_FAIL;
        }

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid DevRequest parameters
     */

    if (wLength != 1 || wValue != 0 )
        {
        USB_UHCD_ERR("usbUhcdGetBusState - invalid request parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Get the correct offset based on port number */

    offset = (UINT8)((port == 1) ? (USB_UHCD_PORT1) : (USB_UHCD_PORT2));

    /*
     * The request for GET_BUS_STATE returns an 1 byte information
     * with the following field interpretation
     * Bit 0 - Value of the D- signal
     * Bit 1 - Value of the D+ signal
     * Bits 2-7 - Reserved and reset to 0
     */

    /* Fill the buffer based on Line status field of PORTSC  - Start */

    /* Bit 4 - Gives the value of D+ signal logical level */

    buf |= usbUhcdIsBitSet (pHCDData, offset, USB_UHCD_PORTSC_LS_DP);

    buf  = (UINT8)(buf << 1);

    /* Bit 5 gives the value of D- signal logical level */

    buf |= usbUhcdIsBitSet (pHCDData, offset, USB_UHCD_PORTSC_LS_DM);

    /* Fill buffer based on Line status field of PORTSC  - End */

    /* Copy the buf value to URB's buffer */

    usbUhcdCopy (pUrb->pTransferBuffer, &buf, NULL, 1);

    /* Return a success status */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetBusState() */


/*******************************************************************************
*
* usbUhcdGetDescriptor - handle GET_DESCRIPTOR request for Root hub
*
* This routine handles the GET_DESCRIPTOR standard device request issued
* to the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving the descriptor
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetDescriptor
    (
    USBHST_URB *    pUrb
    )
    {

    /* To hold the descriptor index */

    UINT8 descIndex = 0;

    /* To hold the descriptor type */

    UINT8 descType = 0;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and the DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetDescriptor - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Get the descriptor Index */

    descIndex = (UINT8)(wValue &  (0xFF));

    /* Get the descriptor type */

    descType  = (UINT8)((wValue & (0xFF00)) >> 8);

    /*
     * Check whether there are any invalid conditions
     * ie invalid DevRequest parameters
     */

    if (wIndex != 0 ||
        descIndex != 0 ||
        (descType != USB_UHCD_DESCRIPTOR
        && descType != USB_UHCD_CONFIGURATION_DESCRIPTOR))
        {
        USB_UHCD_ERR("usbUhcdGetDescriptor - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Switch based on the descriptor needed */

    if (descType == USB_UHCD_DESCRIPTOR)
        {
        USB_UHCD_VDBG("usbUhcdGetDescriptor - device descriptor request \n",
                      1, 2, 3, 4, 5, 6);

        /* Copy the Device descriptor */

        usbUhcdCopy ( pUrb->pTransferBuffer,
                      usbUhcdGdDeviceDescriptor,
                      &(pUrb->uTransferLength),
                      0x12);
        }/* End of case USB_UHCD_DESCRIPTOR */
     else /* USB_UHCD_CONFIGURATION_DESCRIPTOR: Config descriptor request */
        {
        USB_UHCD_VDBG("usbUhcdGetDescriptor - config descriptor request \n",
                     1, 2, 3, 4, 5, 6);

        /* Copy the Configuration descriptor */

        usbUhcdCopy (pUrb->pTransferBuffer,
                     usbUhcdGdConfigDescriptor,
                     &(pUrb->uTransferLength),
                     0x19);
        }/* End of case USB_UHCD_CONFIGURATION_DESCRIPTOR */

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetDescriptor() */

/*******************************************************************************
*
* usbUhcdSetConfiguration - handle SET_CONFIGURATION request for Root hub
*
* This routine handles the SET_CONFIGURATION standard device request issued to
* the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in setting configuration
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdSetConfiguration
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {
    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and DevRequest pointers are valid */

    if (NULL == pUrb || NULL == pUrb->pTransferSpecificData)
        {
        USB_UHCD_ERR("usbUhcdSetConfiguration - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /*
     * Check whether there are any invalid conditions
     * ie invalid DevRequest parameters
     */

    if (wIndex != 0 ||
        wLength !=0 ||
        (wLength != USB_UHCD_CONFIG_VALUE &&
         wLength != 0))
        {
        USB_UHCD_ERR("usbUhcdSetConfiguration - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Record the configuration value passed */

    pHCDData->rootHub.uConfigValue = (UINT8)wLength;

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdSetConfiguration() */


/*******************************************************************************
*
* usbUhcdGetConfiguration - handle GET_CONFIGURATION request for Root hub
*
* This routine handles the GET_CONFIGURATION standard device request issued to
* the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving configuration
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetConfiguration
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {
    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetConfiguration - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /*
     * Check whether there are any invalid conditions
     * ie invalid DevRequest parameters
     */

    if (wIndex != 0 ||
        wLength != 1 ||
        wValue != 0)
        {
        USB_UHCD_ERR("usbUhcdGetConfiguration - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* Copy the configuration to URB's buffer */

    usbUhcdCopy (pUrb->pTransferBuffer,
        &(pHCDData->rootHub.uConfigValue), NULL, 1);

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetConfiguration() */


/*******************************************************************************
*
* usbUhcdGetStatus - handle GET_STATUS request for Root hub
*
* This routine handles the GET_STATUS standard device request issued
* to the Root hub.
*
* RETURNS: USB_UHCD_FAIL if there is an error in retrieving status
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL INT32 usbUhcdGetStatus
    (
    USBHST_URB *    pUrb
    )
    {
    /* To hold the 2 byte status information */

    /*
     * The status is made 1 cause the Root Hub is self powered
     * and does not support suppot
     */

    UINT16 status = 0x01;

    UINT16 wValue = 0;

    UINT16 wIndex = 0;

    UINT16 wLength = 0;

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* Check if the URB and DevRequest pointers are valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData) ||
        (NULL == pUrb->pTransferBuffer))
        {
        USB_UHCD_ERR("usbUhcdGetStatus - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /*
     * Check whether there are any invalid conditions
     * ie invalid DevRequest parameters
     */

    if (wLength != 0 &&
        wLength != 2)
        {
        USB_UHCD_ERR("usbUhcdGetStatus - invalid conditions \n",
                     1, 2, 3, 4, 5, 6);

        return USB_UHCD_FAIL;
        }

    /* There is no specific status information which need to be updated */

    /* Copy the status to URB's buffer */

    status = OS_UINT16_LE_TO_CPU(status);

    usbUhcdCopy (pUrb->pTransferBuffer, (UCHAR *) &status, NULL, 2);

    /* Return success */

    return USB_UHCD_PASS;
    }/* End of usbUhcdGetStatus() */


/*******************************************************************************
*
* usbUhcdProcessRhControlTransfer - handle control transfer for Root hub
*
* This routine handles all the control transfers issued to the Root hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbUhcdProcessRhControlTransfer
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {
    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* To hold the status of the routine call */

    INT32 retVal;

    UINT16 wValue = 0;

    /* Check if the URB and DevRequest pointers are valid */

    if (NULL == pUrb || NULL == pUrb->pTransferSpecificData)
        {
        USB_UHCD_ERR("usbUhcdProcessRhControlTransfer - invalid parameters\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    pSetupPacket = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);

    /* HUB requests - (start) */

    /* ClearPortFeature request */

    if (pSetupPacket->bmRequestType == 0x23 &&
        pSetupPacket->bRequest == 1)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "clear port feature request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the ClearPortFeature request */

        retVal = usbUhcdClearPortFeature (pHCDData, pUrb);
        }

    /* GetHubDescriptor request */

    else if (pSetupPacket->bmRequestType == 0xA0 &&
             pSetupPacket->bRequest == 6)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get hub descriptor request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetHubDescriptor request */

        retVal = usbUhcdGetHubDescriptor (pUrb);
        }

    /* GetHubStatus request */

    else if (pSetupPacket->bmRequestType == 0xA0 &&
             pSetupPacket->bRequest == 0)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get hub status request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetHubStatus request */

        retVal = usbUhcdGetHubStatus ( pUrb);
        }

    /* GetPortStatus request */

    else if (pSetupPacket->bmRequestType == 0xA3 &&
             pSetupPacket->bRequest == 0)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get port status request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetPortStatus request */

        retVal = usbUhcdGetPortStatus (pHCDData, pUrb);
        }

    /* SetPortFeature request */

    else if (pSetupPacket->bmRequestType == 0x23 &&
             pSetupPacket->bRequest == 3)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get port feature request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the SetPortFeature request */

        retVal = usbUhcdSetPortFeature (pHCDData, pUrb);
        }

    /* GetBusState request */

    else if (pSetupPacket->bmRequestType == 0xA3 &&
             pSetupPacket->bRequest == 2)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get bus state request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetBusState request */

        retVal = usbUhcdGetBusState (pHCDData, pUrb);
        }

    /* SetHubDescriptor request */

    else if (pSetupPacket->bmRequestType == 0x20 &&
             pSetupPacket->bRequest == 7)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get hub descriptor request \n", 1, 2, 3, 4, 5, 6);

        /* This should be a STALL - this should be changed */

        retVal = USB_UHCD_PASS;
        }

    /* ClearHubFeature request */

    else if (pSetupPacket->bmRequestType == 0x20 &&
             pSetupPacket->bRequest == 1)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "clear hub feature request \n", 1, 2, 3, 4, 5, 6);
        /*
         * There is no routineality involved
         * in clearing the hub feature
         * So return a success
         */

        retVal = USB_UHCD_PASS;
        }

    /* SetHubFeature request */

    else if (pSetupPacket->bmRequestType == 0x20 &&
             pSetupPacket->bRequest == 3)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "set hub feature request \n", 1, 2, 3, 4, 5, 6);

        /*
         * There is no routineality involved
         * in setting the hub feature
         * So return a success
         */

        retVal = USB_UHCD_PASS;
        }

    /* HUB requests - (End) */

    /* DEVICE requests (start) */

    /* GetDescriptor request */

    else if (pSetupPacket->bmRequestType == 0x80 &&
             pSetupPacket->bRequest == 6)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get descriptor request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetDescriptor request */

        retVal = usbUhcdGetDescriptor (pUrb);
        }

    /* SetConfiguration request */

    else if (pSetupPacket->bmRequestType == 0 &&
             pSetupPacket->bRequest == 9)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "set configuration request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the SetConfiguration request */

        retVal = usbUhcdSetConfiguration (pHCDData, pUrb);
        }

    /* SetAddress request */

    else if (pSetupPacket->bmRequestType == 0 &&
             pSetupPacket->bRequest == 5)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "set address request \n", 1, 2, 3, 4, 5, 6);

        pHCDData->rootHub.uDeviceAddress = (UINT8)wValue;

        retVal = USB_UHCD_PASS;
        }

    /* GetConfiguration request */

    else if (pSetupPacket->bmRequestType == 0x80 &&
             pSetupPacket->bRequest == 8)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get configuration request \n",  1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetConfiguration request */

        retVal = usbUhcdGetConfiguration (pHCDData, pUrb);
        }

    /* SetDescriptor request */

    else if (pSetupPacket->bmRequestType == 0 &&
             pSetupPacket->bRequest == 7)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "set descriptor request \n", 1, 2, 3, 4, 5, 6);

        /* This should be an error - should be changed */

        retVal = USB_UHCD_PASS;
        }

    /* GetStatus request */

    else if ((pSetupPacket->bmRequestType & 0xFC) == 0x80 &&
             pSetupPacket->bRequest == 0)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "get status request \n", 1, 2, 3, 4, 5, 6);

        /* Call the routine to handle the GetStatus request */

        retVal = usbUhcdGetStatus (pUrb);
        }

    /* SetFeature request */

    else if ((pSetupPacket->bmRequestType & 0xFC) == 0 &&
             pSetupPacket->bRequest == 3)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "set feature request \n", 1, 2, 3, 4, 5, 6);

        /*
         * There is no routineality involved in setting the feature
         * So a success is returned here
         */

        retVal = USB_UHCD_PASS;
        }

    /* ClearFeature request */

    else if ((pSetupPacket->bmRequestType & 0xFC) == 0 &&
             pSetupPacket->bRequest == 1)
        {
        USB_UHCD_VDBG("usbUhcdProcessRhControlTransfer - "
            "clear feature request \n", 1, 2, 3, 4, 5, 6);

        /*
         * There is no routineality involved in clearing the feature
         * So a success is returned here
         */

        retVal = USB_UHCD_PASS;
        }

    /* DEVICE requests (End) */

    /* If invalid request is issued, return a failure */

    else
        {
        USB_UHCD_ERR("usbUhcdProcessRhControlTransfer - "
            "invalid device request \n", 1, 2, 3, 4, 5, 6);

        retVal = USB_UHCD_FAIL;
        }

    /* If request completed sucessfully, fill URB's status as 0 */

    if (retVal)
        {
        pUrb->nStatus = 0;
        }

    /* If request completed un-sucessfully, fill URB's status as 4 - STALL */

    else
        {
        USB_UHCD_ERR("usbUhcdProcessRhControlTransfer - "
            "request completed un-sucessfully \n", 1, 2, 3, 4, 5, 6);

        pUrb->nStatus = USBHST_STALL_ERROR;

        }

    /* If the callback routine pointer is not NULL, call the routine */

    if (NULL != pUrb->pfCallback)
        {
        /* Call the callback directly */

        pUrb->pfCallback(pUrb);
        }

    return;

    }/* End of usbUhcdProcessRhControlTransfer() */


/*******************************************************************************
*
* usbUhcdProcessRhInterruptTransfer - handle interrupt transfer for Root hub
*
* This routine handles the interrupt transfers issued to the Root hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbUhcdProcessRhInterruptTransfer
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb
    )
    {
    /* To hold the index of the port */

    INT32 i = 0;

    /* To hold the status information */

    UINT8 status = 0;

    /* To hold the offset of the register */

    UINT32 offset = 0;

    /* To hold the Port status and control register value */

    UINT16 portStatus = 0;

    /* Check if the URB pointer is valid */

    if ((NULL == pUrb) ||
        (NULL == pUrb->pTransferBuffer) ||
        (NULL == pUrb->pfCallback) ||
        (NULL == pHCDData))
        {
        USB_UHCD_ERR("usbUhcdProcessRhInterruptTransfer - invalid parameters\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Check whether the semaphore has been released from
     * the manage port routine, or from the cancel urb for
     * the root hub.
     */

    if (pHCDData->flagRhIntDel == TRUE)
        {
        USB_UHCD_WARN("usbUhcdProcessRhInterruptTransfer - "
            "interrupt task for RH has been deleted \n", 1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * The status bitmap is as follows
     * Bit 0 - Change in the hub itself
     * Bit 1 - Change in hub port 1
     * Bit 2 - Change in hub port 2
     * etc
     */

    /* Form the status change bitmap for both the ports */

    for (i = 0; i < 2; ++i)
        {

        /* Base Address offset. First Port1 then port2. */

        offset = (UINT32)(USB_UHCD_PORT1 + i * 2);

        if (usbUhcdIsBitSet (pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PR))
            {
            USB_UHCD_DBG("usbUhcdProcessRhInterruptTransfer - "
                "Port in reset but reset has not issued \n", 1, 2, 3, 4, 5, 6);

           /*
            * This is some quirky conditon that we should handle;
            * The port is set to signal reset in the set port feature request,
            * and that request will wait for the time required by the spec, then
            * deassert the reset singal; so if we meet a reset here, we may be
            * working with a quirky controller; so we should deassert the reset
            * signal here;
            */

            /* To have time delay enough to meet the spec: 50 ms plus a 10 ms */

            OS_DELAY_MS(60);

            /* Deassert the reset signal */

            usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PR);
            }

        /* Read the PORTSC register to get the port status */

        portStatus = (UINT16)usbUhcdReadReg(pHCDData, (UINT8)offset);

        /* Check if there is a status change in the ports */

        /*
         * The UHCI has 3 bits in PORTSC register represents port status has
         * changed; these are :
         * bit 1 USB_UHCD_PORTSC_CSC - Connect Status Change
         * bit 3 USB_UHCD_PORTSC_PEC - Port Enable Change
         * bit 6 USB_UHCD_PORTSC_RD - Resume Detected
         *
         * Because we are actually polling the PORTSC for changes, and we have
         * a savedPortStatus to save the previous port status; we can have more
         * accurate detection of the changes; that is, we only report the same
         * change conditon once when these above bits change from a 0 to a 1;
         *
         * There is some quirky conditon where none of these change bits above
         * got changed when a device is attached; We try our best to see if the
         * CCS bit got changed;
         *
         * The UHCI has no hardware "Reset Change", but we have also a software
         * "Reset Change" which got set when the set port feature(Reset) is
         * issued; We singal this as a "port change" to let the hub driver to
         * clear this "Reset Change";
         */

        if (((portStatus & (1 << USB_UHCD_PORTSC_CSC)) && /* CSC 0 to 1 */
             !(pHCDData->savedPortStatus[i] & (1 << USB_UHCD_PORTSC_CSC)))||
            ((portStatus & (1 << USB_UHCD_PORTSC_PEC)) && /* PEC 0 to 1 */
             !(pHCDData->savedPortStatus[i] & (1 << USB_UHCD_PORTSC_PEC)))||
            ((portStatus & (1 << USB_UHCD_PORTSC_RD)) &&  /* RD 0 to 1 */
             !(pHCDData->savedPortStatus[i] & (1 << USB_UHCD_PORTSC_RD)))||
            ((portStatus & (1 << USB_UHCD_PORTSC_CCS)) != /* CCS toggles */
            (pHCDData->savedPortStatus[i] & (1 << USB_UHCD_PORTSC_CCS))) ||
            (pHCDData->usbUhcdResetChange[i] == TRUE)) /* Reset changed */
            {
            USB_UHCD_DBG("usbUhcdProcessRhInterruptTransfer - "
                "status changed on port%d: " \
                "savedPortStatus %p ==> currentPortStatus %p \n",
                i, pHCDData->savedPortStatus[i], portStatus, 4, 5, 6);
            /*
             * Workaround for WIND00105458 :
             *
             * When connecting a device to one of the external hub's port,
             * sometimes the external hub is disconnected from the root
             * port if the external hub is connected directly under the
             * root port of the UHCI.
             *
             * There are several situations that can happen for these quirky
             * hubs, such as the z-tek 4-port high speed hub (0x05E3:0x0608).
             *
             * a) savedPortStatus 0x95 ==> currentPortStatus 0x8a
             *
             * We got a true "disconnect" event, even the external hub is
             * still "connected" on the root port. When this happens,
             * the hub driver will first remove device tree from the root
             * hub; and the root hub will detect a "connect change" later,
             * so there will be a true "re-connect" event;
             *
             * b) savedPortStatus 0x95 ==> currentPortStatus 0x99
             *
             * In this case, the hub is not truely "disconnected", but
             * the port has been "disabled". To workaround this, we have
             * to do some trick:
             *
             * i) re-enable the root port
             *
             * ii) pretend a "re-connect", by using a flag 'forceReconnect',
             * which will cause the hub driver to detect a "connect change"
             * event next time it calls usbUhcdGetPortStatus.
             *
             * iii)so the hub driver will issue a reset later, and then
             * the external hub comes back to life
             *
             * c) savedPortStatus 0x95 ==> currentPortStatus 0x9b
             *
             * In this case, the root port is disabled (PE cleared and
             * PEC bit set); but the CCS and CSC bits are set, which
             * makes a connect change event. So, we have two options
             * to make the root hub back:
             *
             * i) option 1, clear the PEC bit right away, and let the
             * CCS and CSC bits to report a connct change, then the hub
             * driver will try to handle this in the connect change
             * handler. This is the previous workwround of WIND00105458.
             *
             * ii) option 2, do not clear PEC bit here, just leave it
             * unchanged; then the hub driver will finally get the
             * "port enable change" when it calls to usbUhcdGetPortStatus,
             * and the hub driver will handle this in the port enable
             * change handler, and the port enable change handler will
             * then start to "reset" the root port, and then it comes
             * back again.
             *
             * We are taking option 2 here.
             *
             * d) savedPortStatus 0x8a ==> currentPortStatus 0x91
             *
             * This happens when doing the pnp operation quickly, which
             * may be following the 0x95->0x8a event. In this case, we
             * can handle it the same way as for siutation b), that is
             * "re-enable" and force a "re-connect".
             */

            if (((portStatus & 0x000C) == 0x0008) || /* PE 0, PEC 1 */
                (((pHCDData->savedPortStatus[i] & 0x000A) == 0x000A) &&
                 ((portStatus & 0x0007) == 0x0001))) /* CSC 1 to 0, but CCS 1 */
                {
                USB_UHCD_DBG("usbUhcdProcessRhInterruptTransfer - "
                    "PORTSC[%d] = %p, PEC bit is set, maybe due to EOF2 \n",
                    i, portStatus, 3, 4, 5, 6);
                /*
                 * Force re-enable and re-connect for the root port
                 * when the port is disabled but still connected
                 */

                if ((portStatus & 0x0003) == 0x0001)
                    {
                    USB_UHCD_DBG("usbUhcdProcessRhInterruptTransfer - "
                        "port still connected, force reenable \n",
                        1, 2, 3, 4, 5, 6);

                    /* Clear the Port Enable Change bit */

                    usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PEC);

                    /* Enable the port */

                    usbUhcdPortSetBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PE);

                    OS_DELAY_MS(10);

                    /* Clear the port staus change */

                    usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_CSC);


                    /* Clear the Port Enable Change bit */

                    usbUhcdPortClearBit(pHCDData, (UINT8)offset, USB_UHCD_PORTSC_PEC);


                    /* Read the PORTSC register to get the port status */

                    portStatus = (UINT16)usbUhcdReadReg(pHCDData, (UINT8)offset);

                    USB_UHCD_DBG("usbUhcdProcessRhInterruptTransfer - "
                        "new portStatus %p, savedPortStatus %p \n",
                        portStatus, pHCDData->savedPortStatus[i], 3, 4, 5, 6);

                    pHCDData->forceReconnect[i] = TRUE;
                    }
                }

            /* Record it in the status bit map */

            status = (UINT8)(status | (1 << (i + 1)));
            }

        /* Save the port status value */

        pHCDData->savedPortStatus[i] = portStatus;
        }/* End of for() */

    /* Prepare to report the status if the URB is not canceled */

    if (pHCDData->flagRhIntDel == FALSE)
        {
        /* Fill URB's status */

        pUrb->nStatus = USBHST_SUCCESS;

        /* Update the buffer with the status bitmap */

        USB_UHCD_VDBG("usbUhcdProcessRhInterruptTransfer - "
            "updating the buffer with status bitmap \n", 1, 2, 3, 4, 5, 6);

        /* Copy the status to URB's buffer */

        usbUhcdCopy (pUrb->pTransferBuffer,
                     (UCHAR *) &status,
                     &(pUrb->uTransferLength),
                    1);

        /*
         * Call the callback routine because we have checked
         * the callback routine pointer is not NULL
         */

        /* Call the callback directly */

        pUrb->pfCallback(pUrb);
        }

    else
        {
        /* Make the flag as false, so to trace the next cancel request */

        pHCDData->flagRhIntDel = FALSE;
        }

    return;

    }/* End of usbUhcdProcessRhInterruptTransfer() */


/*******************************************************************************
*
* usbUhcdQueueRhRequest - handle request queued doe the Roob hub
*
* This routine handles the request queued to the Root hub.
*
* RETURNS: FALSE if the request for the Root hub is not queued.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdQueueRhRequest
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb ,
    ULONG           uPipeHandle
    )
    {
    USB_UHCD_PIPE * pHCDPipe ;

    pHCDPipe = (USB_UHCD_PIPE *)uPipeHandle;

    /* If a control transfer is requested */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER )
        {
        USB_UHCD_VDBG("usbUhcdQueueRhRequest - request is control transfer\n",
                      1, 2, 3, 4, 5, 6);

        /* Call the control transfer for a root hub*/

        usbUhcdProcessRhControlTransfer(pHCDData, pUrb);
        }

    /* If a interrupt transfer is requested */

    else if ( pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER )
        {
        USB_UHCD_VDBG("usbUhcdQueueRhRequest - request is interrupt transfer\n",
                      1, 2, 3, 4, 5, 6);

        pHCDData->flagRhIntDel = FALSE;

        usbUhcdProcessRhInterruptTransfer(pHCDData, pUrb);

        }

    /* No other types of transfer is supported by the Root Hub */

    else
        {
        USB_UHCD_ERR("usbUhcdQueueRhRequest - invalid request to root hub \n",
                     1, 2, 3, 4, 5, 6);
        return FALSE;
        }

    return TRUE;
    }/* End of usbUhcdQueueRhRequest() */


/*******************************************************************************
*
* usbUhcdRhCreatePipe - create a pipe specific to an endpoint for the Root hub
*
* This routine is used to create a pipe specific to an endpoint for the Root hub.
* The inforamtion about the endpoint is obtained from the endpoint descriptor
* <pEndpointDescriptor>.
*
* RETURNS: USBHST_SUCCESS if the pipe was created successfully
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdRhCreatePipe
    (
    pUSB_UHCD_DATA  pHCDData,
    UINT8           uDeviceAddress,
    UINT8           uDeviceSpeed,
    PUCHAR          pEndpointDescriptor,
    ULONG *         puPipeHandle
    )
    {

    /* To hold the status of the routine call */

    USBHST_STATUS status = USBHST_FAILURE;

    /* Pointer to the Endpoint Descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pEndpointDescriptor) ||(NULL == puPipeHandle))
        {
        USB_UHCD_ERR("usbUhcdRhCreatePipe - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    /* Switch based on the endpoint type */

    switch (pEndpointDesc->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK)
        {
        case USBHST_CONTROL_TRANSFER:/* Control endpoint */
            {
            /* If the endpoint number is not 0, it is an error */

            if ( 0 !=
                 (pEndpointDesc->bEndpointAddress & 0x0F))
                {
                USB_UHCD_ERR("usbUhcdRhCreatePipe - "
                    "control endpoint address %d is invalid \n",
                    (pEndpointDesc->bEndpointAddress & 0x0F), 2, 3, 4, 5, 6);

                return USBHST_INVALID_REQUEST;
                }
            else
                {

                /* Allocate memory for the control endpoint */

                pHCDData->rootHub.pControlPipe = usbUhcdFormEmptyPipe();

                /* Check if pipe is created successfully */

                if (NULL == pHCDData->rootHub.pControlPipe)
                    {
                    USB_UHCD_ERR("usbUhcdRhCreatePipe - "
                        "memory allocate failed for RH control endpoint\n",
                        1, 2, 3, 4, 5, 6);

                    return USBHST_INSUFFICIENT_MEMORY;
                    }

                /* Populate the fields of the control pipe - Start */

                pHCDData->rootHub.pControlPipe->uAddress = uDeviceAddress;

                pHCDData->rootHub.pControlPipe->uEndpointAddress = 0;
                pHCDData->rootHub.pControlPipe->uSpeed = uDeviceSpeed;

                pHCDData->rootHub.pControlPipe->uMaximumPacketSize =
                pEndpointDesc->wMaxPacketSize;

                /* Copy the pipe direction */

                pHCDData->rootHub.pControlPipe->uEndpointDir =
                pEndpointDesc->bEndpointAddress >>7;

                /* Copy the pipe type */

                pHCDData->rootHub.pControlPipe->uEndpointType =
                    USBHST_CONTROL_TRANSFER;

                pHCDData->rootHub.pControlPipe->PipeSynchEventID =
                    OS_CREATE_EVENT(OS_EVENT_SIGNALED);

                if (NULL == pHCDData->rootHub.pControlPipe->PipeSynchEventID)
                    {
                    USB_UHCD_ERR(
                        "usbUhcdRhCreatePipe - PipeSynchEventID not created\n",
                        0, 0, 0, 0, 0, 0);

                    return USBHST_INSUFFICIENT_RESOURCE;
                    }

                /* Populate the fields of the control pipe - End */

                /* Update the pipe handle information */

                *(puPipeHandle) = (ULONG)(pHCDData->rootHub.pControlPipe);

                status = USBHST_SUCCESS;
                }
            break;
            }
        case USBHST_INTERRUPT_TRANSFER:/* Interrupt endpoint */
            {
            /* Allocate memory for the interrupt endpoint */

            pHCDData->rootHub.pInterruptPipe = usbUhcdFormEmptyPipe();

            /* Check if pipe is created successfully */

            if (NULL == pHCDData->rootHub.pInterruptPipe)
                {
                USB_UHCD_ERR("usbUhcdRhCreatePipe - "
                    "memory allocate failed for RH interrupt endpoint \n",
                    1, 2, 3, 4, 5, 6);

                return USBHST_INSUFFICIENT_MEMORY;
                }

            /* Populate the fields of the control pipe - Start */

            pHCDData->rootHub.pInterruptPipe->uAddress = uDeviceAddress;

            pHCDData->rootHub.pInterruptPipe->uEndpointAddress = 0;
            pHCDData->rootHub.pInterruptPipe->uSpeed =uDeviceSpeed;
            pHCDData->rootHub.pInterruptPipe->uMaximumPacketSize =
                pEndpointDesc->wMaxPacketSize;

            /* Copy the pipe direction */

            pHCDData->rootHub.pInterruptPipe->uEndpointDir =
                pEndpointDesc->bEndpointAddress >>7;;

            /* Copy the pipe type */

            pHCDData->rootHub.pInterruptPipe->uEndpointType =
                USBHST_INTERRUPT_TRANSFER;

            pHCDData->rootHub.pInterruptPipe->PipeSynchEventID =
                OS_CREATE_EVENT(OS_EVENT_SIGNALED);

            if (NULL == pHCDData->rootHub.pInterruptPipe->PipeSynchEventID)
                {
                USB_UHCD_ERR(
                    "usbUhcdRhCreatePipe - PipeSynchEventID not created\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INSUFFICIENT_RESOURCE;
                }

            /* Populate the fields of the control pipe - End */

            /* Update the pipe handle information */

            *(puPipeHandle) = (ULONG)(pHCDData->rootHub.pInterruptPipe);

            status = USBHST_SUCCESS;
            break;
            }
        default:
            {
            USB_UHCD_ERR("usbUhcdRhCreatePipe - invalid request \n",
                         1, 2, 3, 4, 5, 6);

            status = USBHST_INVALID_REQUEST;
            }
        }/* End of switch */

    return status;

    }/* End of routine usbUhcdRhCreatePipe() */


/*******************************************************************************
*
* usbUhcdRhDeletePipe - delete a pipe specific to an endpoint for Root hub
*
* This routine is used to delete a pipe specific to an endpoint for the Root hub.
* The pipe to be deleted is specified by <uPipeHandle>.
*
* Delete pipe for specified endpoint
*
* RETURNS: USBHST_SUCCESS if the pipe was deleted successfully
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdRhDeletePipe
    (
    pUSB_UHCD_DATA  pHCDData,
    ULONG           uPipeHandle
    )
    {
    /* Check the validity of the parameters */

    if ((0 == uPipeHandle) || (NULL == pHCDData))
        {
        USB_UHCD_ERR("usbUhcdRhDeletePipe - invalid parameters \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if it is a control pipe delete request */

    if (uPipeHandle == (ULONG)(pHCDData->rootHub.pControlPipe))
        {
        USB_UHCD_VDBG("usbUhcdRhDeletePipe - delete a control pipe \n",
                      1, 2, 3, 4, 5, 6);

        if (pHCDData->rootHub.pControlPipe->PipeSynchEventID)
            {
            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDData->rootHub.pControlPipe->PipeSynchEventID);

            pHCDData->rootHub.pControlPipe->PipeSynchEventID = NULL;
            }


        OS_FREE(pHCDData->rootHub.pControlPipe);

        pHCDData->rootHub.pControlPipe = NULL;
        }

    /* Check if it is an interrupt pipe delete request */

    else if (uPipeHandle == (ULONG)(pHCDData->rootHub.pInterruptPipe))
        {
        USB_UHCD_VDBG("usbUhcdRhDeletePipe - delete a interrupt pipe \n",
                      1, 2, 3, 4, 5, 6);

        if (pHCDData->rootHub.pInterruptPipe->PipeSynchEventID)
            {
            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDData->rootHub.pInterruptPipe->PipeSynchEventID);

            pHCDData->rootHub.pInterruptPipe->PipeSynchEventID = NULL;
            }

        OS_FREE(pHCDData->rootHub.pInterruptPipe);

        pHCDData->rootHub.pInterruptPipe = NULL;
        }
    else
        {
        USB_UHCD_ERR("usbUhcdRhDeletePipe - invalid pipe handle \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    return USBHST_SUCCESS;
    }/* End of usbUhcdRhDeletePipe() */


/*******************************************************************************
*
* usbUhcdCancelRhRequest - cancel a request submitted for an endpoint.
*
* This routine is used to cancel a request submitted to a endpoint for transfer.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER if parameters are invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdCancelRhRequest
    (
    pUSB_UHCD_DATA  pHCDData,
    USBHST_URB *    pUrb ,
    ULONG           uPipeHandle
    )
    {

    /*
     * CancelUrb for the hub, is for only for the interrupt request.
     */

    /*
     * Update the flag
     */

    pHCDData->flagRhIntDel = TRUE;

    return USBHST_SUCCESS;
    }/* End of usbUhcdCancelRhRequest() */

/********************* End of File UHCD_RH_Emulate.c *************************/

