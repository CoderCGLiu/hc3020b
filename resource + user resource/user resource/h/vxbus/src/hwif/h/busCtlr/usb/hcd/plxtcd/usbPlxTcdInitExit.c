/* usbPlxTcdInitExit.c - USB PLX TCD Init and Exit Module */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,03may13,wyy  Remove compiler warning (WIND00356717)
01e,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01d,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01c,10oct12,s_z  Correct AB silicon detection issue (WIND00382688)
01b,04sep12,s_z  Use usbTgtTcdRegister/usbTgtTcdUnRegister API
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This file provides the initialization and exit routines for USB PLX
Target Controller Driver.

INCLUDE FILES: usbPlxTcd, usb/usbPlatform.h, hwif/vxbus/hwConf.h,
               drv/pci/pciConfigLib.h, ../src/hwif/h/vxbus/vxbAccess.h,
               spinLockLib.h, rebootLib.h, hookLib.h, usbPlxTcdIsr.h
*/

/* includes */

#include <usbPlxTcd.h>
#include <usb/usbPlatform.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <drv/pci/pciConfigLib.h>
#include "../src/hwif/h/vxbus/vxbAccess.h"
#include <spinLockLib.h>
#include <rebootLib.h>
#include <hookLib.h>
#include <usbPlxTcdIsr.h>
#include <usbPlxTcdUtil.h>

/* extern */

IMPORT UINT16 gUsbPlxTcdVid[];          /* VID */
IMPORT UINT16 gUsbPlxTcdPid[];          /* PID */
IMPORT UINT16 gUsbPlxTcdBcdDevice[];    /* bcdDevice */
IMPORT char * gUsbPlxTcdMfgString[];    /* Manufacture string */
IMPORT char * gUsbPlxTcdProdString[];   /* Producture string */
IMPORT char * gUsbPlxTcdSerialString[]; /* Serial num string */
IMPORT char * gUsbPlxTcdName[];         /* TCD name */
IMPORT UINT8  gUsbPlxTcdConfigNum[];    /* The configuration count */
IMPORT UINT8  gUsbPlxTcdIsrPriority[];  /* Interrupt task priority */
IMPORT UINT8  gUsbPlxTcdDmaType;        /* Dma Type */
IMPORT UINT8  gUsbPlxTcdAlwaysShortPkt[];

IMPORT USBTGT_TCD_FUNCS gUsbPlxTcdDriver;

/* Forward declarartions */

LOCAL STATUS usbPlxTcdInstanceEnable
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
LOCAL STATUS usbPlxTcdInstanceDisable
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
LOCAL STATUS usbPlxTcdInstanceLoad
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
LOCAL STATUS usbPlxTcdInstanceUnLoad
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
LOCAL VOID usbPlxTcdVxbPlbInit
    (
    VXB_DEVICE_ID pDev
    );
LOCAL void usbPlxTcdVxbPciInit
    (
    VXB_DEVICE_ID pDev                    /* struct vxbDev * */
    );
LOCAL VOID usbPlxTcdVxbNullFunction
    (
    VXB_DEVICE_ID pDev
    );
LOCAL STATUS usbPlxTcdVxbDeviceRemove
    (
    VXB_DEVICE_ID pDev
    );
LOCAL void usbPlxTcdVxbDeviceConnect
    (
    VXB_DEVICE_ID pDev
    );

/*
 * List of supported device IDs.
 */

LOCAL struct vxbPciID usbPlxTcdVxbPciDevIDLIst[] =
    {
        /* { devID, vendID } */
        {USB_PLX_DEVICE_ID_2282, USB_PLX_VENDOR_ID_17CC}, /* NET 2282 */
        {USB_PLX_DEVICE_ID_2380, USB_PLX_VENDOR_ID_10B5}, /* PLX 2380 */
        {USB_PLX_DEVICE_ID_3380, USB_PLX_VENDOR_ID_10B5}, /* PLX 3380 */
        {USB_PLX_DEVICE_ID_3382, USB_PLX_VENDOR_ID_10B5}, /* PLX 3382 */
    };

/* Structure to store the driver functions for vxBus */

LOCAL struct drvBusFuncs usbPlxTcdVxbPlbDriverFuncs =
    {
    usbPlxTcdVxbPlbInit,      /* init 1 */
    usbPlxTcdVxbNullFunction, /* init 2 */
    usbPlxTcdVxbDeviceConnect /* Dev Connect */
    };

/* Structure to store the driver functions for vxBus */

LOCAL struct drvBusFuncs usbPlxTcdVxbPciDriverFuncs =
    {
    usbPlxTcdVxbPciInit,      /* init 1 */
    usbPlxTcdVxbNullFunction, /* init 2 */
    usbPlxTcdVxbDeviceConnect /* Dev Connect */
    };

/* Instance methods */

LOCAL DRIVER_METHOD usbPlxTcdVxbDrvMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbPlxTcdVxbDeviceRemove),
    DEVMETHOD_END
    };

/* DRIVER_REGISTRATION for the PLX TCD device residing on PLB */

LOCAL DRIVER_REGISTRATION usbPlxTcdVxbPlbDevRegistration =
    {
    NULL,
    VXB_DEVID_BUSCTRL,                             /* bus controller */
    VXB_BUSID_PLB,                                 /* bus id - PLB Bus Type */
    VXB_VER_4_0_0,                                 /* vxBus version Id */
    "usbPlxTcdVxbPlb",                             /* drv name */
    &usbPlxTcdVxbPlbDriverFuncs,                   /* pDrvBusFuncs */
    &usbPlxTcdVxbDrvMethods[0],                    /* pMethods */
    NULL,                                          /* probe routine */
    NULL                                           /* vxbParams */
    };

/*
 * DRIVER_REGISTRATION for PLX TCD device residing on PCI Bus
 */

LOCAL PCI_DRIVER_REGISTRATION usbPlxTcdVxbPciDevRegistration =
    {
        {
        NULL,
        VXB_DEVID_BUSCTRL,                   /* bus controller */
        VXB_BUSID_PCI,                       /* bus id - PCI Bus Type */
        VXB_VER_4_0_0,                       /* vxBus version Id */
        "usbPlxTcdVxbPci",                   /* drv name */
        &usbPlxTcdVxbPciDriverFuncs,         /* pDrvBusFuncs */
        &usbPlxTcdVxbDrvMethods [0],         /* pMethods */
        NULL,                                /* probe routine */
        NULL                                 /* vxbParams */
        },
    NELEMENTS(usbPlxTcdVxbPciDevIDLIst), /* idListLen */
    &usbPlxTcdVxbPciDevIDLIst [0]        /* idList */
    };

LOCAL BOOL  gUsbPlxTcdRegistered = FALSE;

/* glolal */

usbPlxTcdInterfacePrototype gpUsbPlxTcdLoad = NULL;
usbPlxTcdInterfacePrototype gpUsbPlxTcdUnLoad = NULL;
usbPlxTcdInterfacePrototype gpUsbPlxTcdEnable = NULL;
usbPlxTcdInterfacePrototype gpUsbPlxTcdDisable = NULL;

/* Controller instance list */

LIST gUsbPlxTcdList;

/*******************************************************************************
*
* usbPlxTcdVxbRegister - register the PLX TCD with vxBus
*
* This routine registers the PLX TCD Controller Driver driver with vxBus.
* Note that this can be called early in the initialization sequence.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdVxbRegister
    (
    void
    )
    {
    /* Do not registered multiple times */

    if (gUsbPlxTcdRegistered != FALSE)
        {
        USB_PLX_ERR("The USB PLX TCD driver already registered\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /* Init the global list */

    lstInit(&gUsbPlxTcdList);

    /* Register the PLX driver for PLB bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
                         &usbPlxTcdVxbPlbDevRegistration) == ERROR)
        {
        USB_PLX_ERR("Registering USB PLX TCD Driver over PLB Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Register the PLX driver for PCI bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
                         &usbPlxTcdVxbPciDevRegistration) == ERROR)
        {
        USB_PLX_ERR("Registering USB PLX TCD Driver over PCI Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    gUsbPlxTcdRegistered  = TRUE;

    /* Set the interfaces for OCD to use as a hook */

    gpUsbPlxTcdLoad = usbPlxTcdInstanceLoad;
    gpUsbPlxTcdUnLoad = usbPlxTcdInstanceUnLoad;
    gpUsbPlxTcdEnable = usbPlxTcdInstanceEnable;
    gpUsbPlxTcdDisable = usbPlxTcdInstanceDisable;

    return;
    }


/*******************************************************************************
*
* usbPlxTcdVxbDeregister - deregister the PLX TCD from vxBus
*
* This routine deregisters the PLX TCD from vxBus.
*
* RETURNS: OK, or ERROR if unable to deregister with vxBus
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdVxbDeregister
    (
    void
    )
    {
    if (gUsbPlxTcdRegistered != TRUE)
        {
        USB_PLX_ERR("The USB PLX TCD driver already deregistered\n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Deregisterr the PLX TCD driver for PCI bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
                              &usbPlxTcdVxbPciDevRegistration) == ERROR)
        {
        USB_PLX_ERR("Deregistering USB PLX TCD Driver over PCI Bus\n",
                    1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    gUsbPlxTcdRegistered = FALSE;

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdVxbNullFunction - dummy routine to satisfy the vxbus interface
*
* This routine is a dummy function to satisfy the vxbus interface.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbPlxTcdVxbNullFunction
    (
    VXB_DEVICE_ID pDev
    )
    {
    /* This is a dummy routine which simply returns */

    return ;
    }

/*******************************************************************************
*
* usbPlxTcdVxbPlbInit - do the BSP specific Initializaion
*
* This routine does the BSP specific initializaiton incase it is required.
* For many PLB based controllers, different BSP level initialization is
* requried.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL VOID usbPlxTcdVxbPlbInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    HCF_DEVICE    * pHcf = NULL;
    void          (* pPlbInit1) (void) = NULL;

    /* Get the HCF device from the instance id */

    pHcf = hcfDeviceGet (pDev);

    if (NULL == pHcf || OK != devResourceGet (pHcf,
                              "usbPlxTcdInit",
                              HCF_RES_ADDR,
                              (void *)&(pPlbInit1)))
       {
       USB_PLX_WARN("devResourceGet usbPlxTcdInit fail\n", 1, 2, 3, 4, 5, 6);
       }

    if (pPlbInit1 != NULL)
        {
        (*pPlbInit1)();
        }

    return;
    }


/*******************************************************************************
*
* usbPlxTcdVxbPciInit - legacy PCI support implementation
*
* This routine implements the legacy support for usb Plx 228x Controller.
*
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdVxbPciInit
    (
    VXB_DEVICE_ID pDev                    /* struct vxbDev * */
    )
    {
    UINT32 * pHandler;
    UINT8    uPciCmd = 0;

    /* validate parameters */

    if (pDev == NULL)
        {
        USB_PLX_ERR("usbPlxTcdVxbPciInit pDev NULL\n",
                    1, 2, 3, 4, 5, 6);

        return;
        }

    /* Retrieve the contents of the HCCPARAMS register */

    USB_PLX_DBG("usbPlxTcdVxbPciInit for pDev %p and unit %d\n",
                pDev, pDev->unitNumber, 3, 4, 5 ,6);

    if (vxbRegMap (pDev, 0, (void **)&pHandler) == ERROR)
        {
        USB_PLX_ERR("usbPlxTcdVxbPciInit vxbRegMap error\n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    /* Get the PCI command register */

    VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_COMMAND, 1, uPciCmd);

    /* Enable memory access and PCI bus master */

    uPciCmd |= (PCI_CMD_MEM_ENABLE | PCI_CMD_MASTER_ENABLE);

    VXB_PCI_BUS_CFG_WRITE(pDev, PCI_CFG_COMMAND, 1, uPciCmd);

    return;
    }


/*******************************************************************************
*
* usbPlxTcdRebootHook - reboot hook to disable the host controller
*
* This routine is called on a warm reboot to disable the target controller by
* the BSP.
*
* RETURNS: 0, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbPlxTcdRebootHook
    (
    int startType
    )
    {
    NODE *            pNode;
    pUSB_PLX_TCD_DATA pTCDData;

    pNode = lstFirst(&gUsbPlxTcdList);

    while (NULL != pNode)
        {
        pTCDData = USB_PLX_TCD_DATA_CONV_FROM_NODE(pNode);

        if ((NULL != pTCDData) &&
            (NULL != pTCDData->pPlxCtrl))
            {
            usbPlxTcdInstanceDisable(pTCDData->pPlxCtrl);
            }

        pNode = lstNext(pNode);
        }
    return 0;
    }


/*******************************************************************************
*
* usbPlxTcdHardwareUnInit - perform the hardware specific un-initialization
*
* This routine performs the hardware specific un-initialization.
*
* RETURNS: OK, or ERROR if failed to initialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdHardwareUnInit
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    VXB_DEVICE_ID pDev;
    UINT8         uIndex;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pPlxCtrl->pDev))
        {
        USB_PLX_ERR("pPlxCtrl is invalid parameter\n",
                    1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdHardwareUnInit(): In\n",
                 1, 2, 3, 4, 5, 6);

    /* Get the VxBus Device instance */

    pDev = pPlxCtrl->pDev;

    /* Clean the interrupt mask */

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_PCIIRQENB1_REG,
                        0);

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_PCIIRQENB0_REG,
                        0);

    /* Clear all the endpoint status and DMA status */

    for (uIndex = USB_PLX_ENDPT_A; uIndex <= USB_PLX_ENDPT_D; uIndex ++)
        {
        /* Disable the DMA */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMACTL_OFFSET(uIndex),
                            0);
        /* Clean all the DMA status */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMASTAT_OFFSET(uIndex),
                            USB_PLX_REG_READ32(pPlxCtrl,
                            USB_PLX_DMASTAT_OFFSET(uIndex)));

        }

     /* Clean up the Enpoint status */

    for (uIndex = 0; uIndex <= pPlxCtrl->uMaxEpNum; uIndex ++)
        {

        /* Clean all the EP STAT register */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_EP_STAT_OFFSET(uIndex),
                            USB_PLX_REG_READ32(pPlxCtrl,
                            USB_PLX_EP_STAT_OFFSET(uIndex)));
        }

    USB_PLX_DBG ("usbPlxTcdHardwareUnInit(): Clean interrupt mask, EP DMA status\n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdHardwareInit - perform the hardware specific initialization
*
* This routine performs the hardware specific initialization.
*
* RETURNS: OK, or ERROR if failed to initialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdHardwareInit
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    VXB_DEVICE_ID pDev;
    UINT8         uIndex;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pPlxCtrl->pDev))
        {
        USB_PLX_ERR("pPlxCtrl is invalid parameter\n",
                  1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdHardwareInit(): In\n",
                 1, 2, 3, 4, 5, 6);

    /* Get the VxBus Device instance */

    pDev = pPlxCtrl->pDev;

    /* Clean all endpoints' status */

    for (uIndex = 0; uIndex <= pPlxCtrl->uMaxEpNum; uIndex ++)
        {
         USB_PLX_REG_WRITE32 (pPlxCtrl,
                              USB_PLX_EP_STAT_OFFSET(uIndex),
                              USB_PLX_REG_READ32 (pPlxCtrl,
                              USB_PLX_EP_STAT_OFFSET(uIndex)));
        }

    /* Clean all DMA channels' status */

    for (uIndex = USB_PLX_ENDPT_A; uIndex <= USB_PLX_ENDPT_D; uIndex ++)
        {
        /* Disable the DMA */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMACTL_OFFSET(uIndex),
                            0);

        /* Clean all the DMA status */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMASTAT_OFFSET(uIndex),
                            USB_PLX_REG_READ32(pPlxCtrl,
                            USB_PLX_DMASTAT_OFFSET(uIndex)));

        }

    /*
     * Set the interrupt mask
     *
     * NOTE: PLX3380 can get the status change interrupt
     * but NET2282 not. (USB_PLX_XIRQENB1_CS)
     *
     * Do not enable the status change interrupt to
     * support NET2282 contorller.
     *
     * Need to figure out the reason.
     */

    pPlxCtrl->uIrqMask1 =  USB_PLX_XIRQENB1_RESM |
              USB_PLX_XIRQENB1_SUSREQCHG |
              USB_PLX_XIRQENB1_RPRESET |
              USB_PLX_XIRQENB1_VBUS   |
              USB_PLX_XIRQENB1_DMA(1) |
              USB_PLX_XIRQENB1_DMA(2) |
              USB_PLX_XIRQENB1_DMA(3) |
              USB_PLX_XIRQENB1_DMA(4);

    pPlxCtrl->uIrqMask0 = USB_PLX_IRQENB0_SETUP |
                          USB_PLX_IRQENB0_EP(0);

    /* Enable the related interrupts */

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_PCIIRQENB1_REG,
                        pPlxCtrl->uIrqMask1);

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_PCIIRQENB0_REG,
                        pPlxCtrl->uIrqMask0);

    /* Configure the standard response register */

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_STDRSP_REG,
                        0);

    USB_PLX_DBG ("usbPlxTcdHardwareInit(): Set interrupt mask PCIIRQENB0 %x "
                 "PCIIRQENB1 %x\n",
                 pPlxCtrl->uIrqMask0, pPlxCtrl->uIrqMask1,
                 3, 4, 5, 6);

    /* Enable the U1/U2/LTM feature */

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_USBCTL2_REG,
                        USB_PLX_REG_READ32(pPlxCtrl,
                        USB_PLX_USBCTL2_REG) |
                        USB_PLX_USBCTL2_LTMEN |
                        USB_PLX_USBCTL2_U2EN |
                        USB_PLX_USBCTL2_U1EN);

    /* AA silicon workarounds */

    if (pPlxCtrl->uRevision == USB_PLX_VERSION_NUM_AA)
        {
        /* AA silicon auto exit U1/U2 workaround */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                                0x718,
                                0x19800000);

        /* Disable auto polarity inversion, errata */

        /*
         * Add the workaroud for the Super speed training fail issue
         * Internal testing shows that, set bit 6 and bit 7 will make the
         * AA,AB version reset and can not be used over hub.
         * Do not set any bit is OK.
         * Set Bit 6 only will be OK too.
         *
         * TODO:
         * with all the bit set, with the analyzer, it works fast.
         * With no bit set, with analyzer, it will reset timely.
         *
         * Customer has no analyzer issue on his hardware.
         */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            0x71C,
                            (USB_PLX_REG_READ32(pPlxCtrl, 0x71C) & ~(1<<7)) |
                            (1<<6));
        }

    /* Chapter 9, enumeration workaround */

     USB_PLX_REG_WRITE32(pPlxCtrl,
                         0x780,
                         (USB_PLX_REG_READ32(pPlxCtrl,
                                             0x780) &
                        ~(0xF)) |
                          0x3);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdCtrlDataUnInit - perform the hardware specific initialization
*
* This routine performs the hardware specific initialization.
*
* RETURNS: OK, or ERROR if failed to initialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdCtrlDataUnInit
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    VXB_DEVICE_ID pDev = NULL;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("pDev is invalid parameter\n",
                    1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    pDev = pPlxCtrl->pDev;

    if ((NULL != pDev) &&
        (NULL != pDev->pDrvCtrl))
        {
        /* Reset the pDev->pDrvCtrl pointer */

        pDev->pDrvCtrl = NULL;
        }

    /* Free the data resource */

    OSS_FREE(pPlxCtrl);

    USB_PLX_DBG ("usbPlxTcdCtrlDataUnInit(): free pPlxCtrl %p\n",
                 pPlxCtrl, 2, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdCtrlDataInit - perform the hardware specific initialization
*
* This routine performs the hardware specific initialization.
*
* RETURNS: OK, or ERROR if failed to initialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_PLX_CTRL usbPlxTcdCtrlDataInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    UINT8         i;

    /* Parameter verification */

    if (NULL == pDev)
        {
        USB_PLX_ERR("pDev is invalid parameter\n",
                    1, 2, 3, 4, 5, 6);

        return NULL;
        }

    if (NULL != pDev->pDrvCtrl)
        {
        /*
         * The controler data structure has been create by other driver,
         * maybe the host side
         */

        pPlxCtrl = pDev->pDrvCtrl;

        if ((pPlxCtrl->uMagicCode & USB_PLX_MAGIC_ID) == USB_PLX_MAGIC_ID)
            {
            /* The data structure is valid or we will re-create it */

            return pPlxCtrl;
            }
        }

    /* Allocate memory for the USB_PLX_CTRL structure */

    pPlxCtrl = (pUSB_PLX_CTRL)OSS_CALLOC(sizeof(USB_PLX_CTRL));

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Malloc USB_PLX_CTRL failed\n", 1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Initialize the controller data */

    for (i = 0; (i < VXB_MAXBARS) && (i < USB_PLX_MAX_REGBASE_COUNT); i++)
        {
        if (pDev->regBaseFlags[i] != VXB_REG_NONE)
            {
            if (OK == vxbRegMap (pDev, i, &pPlxCtrl->pRegAccessHandle))
                {
                /* Save the register base just to keep things a little simpler */

                pPlxCtrl->uRegBase[i] = (ULONG)pDev->pRegBase[i];

                USB_PLX_DBG("USB PLX regbase[%d] %p\n",
                            i, pPlxCtrl->uRegBase[i], 3, 4, 5, 6);
                break;
                }
            }
        }

    if (i >= VXB_MAXBARS)
        {
        USB_PLX_ERR ("Unable to locate usable BAR \n",
                     1, 2, 3, 4, 5 ,6);

        OSS_FREE (pPlxCtrl);

        return NULL;
        }

    /* Populate the required resources and parameters */

    switch (pDev->busID)
        {
        case VXB_BUSID_PLB:
            {
            /*
             * The underlying bus type is PLB. Query the hwconf file for
             * address conversion functions and populate the required
             * function pointers.
             */
            }
        break;

        case VXB_BUSID_PCI:
            {
            /*
             * Read device ID and revision ID from the config space
             */

            VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_DEVICE_ID, 2,
                                  pPlxCtrl->uDeviceId);

            VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_REVISION, 1,
                                  pPlxCtrl->uRevision);

            /*
             * populate the function pointer for byte converions. Byte swapping
             * is required only if bus type is PCI and architecture is BE.
             * NOTE: for PLB no byte swapping is required
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
            /*
             * PCI is little endian, and most USB PCI controller cards use
             * little endian format for these PLX TCD data structures, so on
             * big endian systems we need this swap! As of this writing, we
             * have not seen any USB PCI card that use big endian data
             * strcutures, so we right now only consider the normal case.
             * If someday we really see such a USB PCI card that use big endian
             * data structures, we may need to use the same mechanism as for
             * the PLB controllers(see above)!
             */

             pPlxCtrl->pDescSwap = vxbSwap32;
             pPlxCtrl->pUsbSwap = vxbSwap32;
#endif
            }
        break;
        default:

            USB_PLX_ERR("busID %p invalid\n ",
                        pDev->busID, 2, 3, 4, 5, 6);

            OSS_FREE (pPlxCtrl);

            return NULL;
        }

    /* Initialize the spinlock */

    SPIN_LOCK_ISR_INIT(&pPlxCtrl->spinLock, 0);

    /* Get a unit number for pDev */

    vxbNextUnitGet (pDev);

    pPlxCtrl->uMagicCode = USB_PLX_MAGIC_ID;

    if ((pPlxCtrl->uDeviceId == USB_PLX_DEVICE_ID_2280) ||
        (pPlxCtrl->uDeviceId == USB_PLX_DEVICE_ID_2282))
        {
        /*
         * Ep 0 with A,B,C,D,E,F
         * The max enpoint number is USB_PLX_ENDPT_F
         */

        pPlxCtrl->uMaxEpNum = USB_PLX_ENDPT_F;
        }
    else
        {
        /*
         * Ep 0 with A,B,C,D
         * The max enpoint number is USB_PLX_ENDPT_D
         */

        pPlxCtrl->uMaxEpNum = USB_PLX_ENDPT_D;
        }

    /* Set the VxBus Device instance */

    pPlxCtrl->pDev = pDev;

    /* Save the common hardware information as driver control */

    pDev->pDrvCtrl = pPlxCtrl;

    USB_PLX_DBG ("usbPlxTcdCtrlDataInit(): Create pPlxCtrl %p pDev %p "
                 "uDeviceId 0x%X\n",
                 pPlxCtrl, pDev,
                 pPlxCtrl->uDeviceId, 4, 5, 6);

    return pPlxCtrl;
    }


/*******************************************************************************
*
* usbPlxTcdDataUnInit - un-initialize usb_plx_tcd_data structure.
*
* This routine un-initializes the usb_plx_tcd_data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbPlxTcdDataUnInit
    (
    pUSB_PLX_TCD_DATA pTCDData
    )
    {
    pUSBTGT_TCD      pTcd = NULL;
    pUSB_PLX_IRQ_BOX pIrqBox = NULL;
    NODE *           pNode = NULL;

    /* Parameter verification */

    if (pTCDData == NULL)
        {
        USB_PLX_ERR("Invalid parameter, pTCDData is NULL\n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    /* Delete the reboot hook */

    (void)rebootHookDelete(usbPlxTcdRebootHook);

    /* Destroy the isr thread */

    if ((pTCDData->isrHandlerThread != 0) &&
        (pTCDData->isrHandlerThread != OS_THREAD_FAILURE))
        {
        (void)OS_DESTROY_THREAD(pTCDData->isrHandlerThread);
        pTCDData->isrHandlerThread = OS_THREAD_FAILURE;
        }

    /* Destroy the isr event */

    if (pTCDData->isrEventId != NULL)
        {
        OS_DESTROY_EVENT(pTCDData->isrEventId);
        pTCDData->isrEventId = NULL;
        }

    /* Remove the TCD from the TML */

    if ((pTcd = pTCDData->pTcd) != NULL)
        {
        /* Un-Register pTCD from TML */

        (void)usbTgtTcdUnRegister(pTcd);

        pTCDData->pTcd = NULL;
        }

    /* Destroy the mutex */

    if (pTCDData->tcdSyncMutex != NULL)
        {
        (void)OS_DESTROY_EVENT(pTCDData->tcdSyncMutex);
        pTCDData->tcdSyncMutex = NULL;
        }

    /* Destroy irqBox on the free list */

    pNode = lstFirst(&pTCDData->irqBoxFreeList);

    while (NULL != pNode)
        {
        pIrqBox = (pUSB_PLX_IRQ_BOX)pNode;
        lstDelete(&pTCDData->irqBoxFreeList, &pIrqBox->irqNode);
        OS_FREE (pIrqBox);
        pNode = lstNext(pNode);
        }

    /* Destroy irqBox on the pending list */

    pNode = lstFirst(&pTCDData->irqBoxPendList);

    while (NULL != pNode)
        {
        pIrqBox = (pUSB_PLX_IRQ_BOX)pNode;
        lstDelete(&pTCDData->irqBoxPendList, &pIrqBox->irqNode);
        OS_FREE (pIrqBox);
        pNode = lstNext(pNode);
        }

    USB_PLX_DBG ("usbPlxTcdDataUnInit(): pTCDData %p\n",
                 pTCDData, 2, 3, 4, 5, 6);

    return ;
    }


/*******************************************************************************
*
* usbPlxTcdDataInit - initialize usb_plx_tcd_data structure.
*
* This routine initializes the usb_plx_tcd_data structure.
*
* RETURNS: OK, or ERROR if usb_plx_tcd_data initialization is unsuccessful
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdDataInit
    (
    pUSB_PLX_TCD_DATA   pTCDData
    )
    {
    pUSBTGT_TCD           pTcd = NULL;
    USBTGT_DEVICE_INFO *  pDeviceInfo = NULL;
    pUSB_PLX_CTRL         pPlxCtrl = NULL;
    USBTGT_TCD_INFO       usbTgtTcdInfo;
    VXB_DEVICE_ID         pDev = NULL;
    char                  taskName[0x20];
    pUSB_PLX_IRQ_BOX      pIrqBox = NULL;
    int                   i;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == pTCDData->pPlxCtrl) ||
        (NULL == pTCDData->pPlxCtrl->pDev))
        {
        USB_PLX_ERR("Invalid parameter, %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pDev"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_DBG ("usbPlxTcdDataInit(): pTCDData %p in\n",
                 pTCDData, 2, 3, 4, 5, 6);

    pPlxCtrl = pTCDData->pPlxCtrl;
    pDev = pTCDData->pPlxCtrl->pDev;

    /* Add the reboot hook */

    if (ERROR == rebootHookAdd((FUNCPTR)usbPlxTcdRebootHook))
        {
        USB_PLX_ERR("Error in hooking the routine usbPlxTcdRebootHook\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Init pipe list */

    lstInit(&(pTCDData->tcdPipeList));
    lstInit(&(pTCDData->irqBoxFreeList));
    lstInit(&(pTCDData->irqBoxPendList));

    /* Init the isrEvent */

    pTCDData->isrEventId = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    if (NULL == pTCDData->isrEventId)
        {
        USB_PLX_ERR("Create isrEventId fail\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Init the pTcdSyncMutex */

    pTCDData->tcdSyncMutex = semMCreate(SEM_Q_PRIORITY |
                                         SEM_INVERSION_SAFE |
                                         SEM_DELETE_SAFE);
    if (NULL == pTCDData->tcdSyncMutex)
        {
        USB_PLX_ERR("Create pTcdSyncMutex fail\n", 1, 2, 3, 4, 5, 6);

        usbPlxTcdDataUnInit(pTCDData);

        return ERROR;
        }

    /* Assign resource for the irq box */

    for (i = 0; i < USB_PLX_MAX_IRQ_BOX; i ++)
        {
        pIrqBox = (pUSB_PLX_IRQ_BOX)OSS_CALLOC(sizeof (USB_PLX_IRQ_BOX));

        if (NULL == pIrqBox)
            {
            usbPlxTcdDataUnInit(pTCDData);

            return ERROR;
            }
        lstAdd(&(pTCDData->irqBoxFreeList), &pIrqBox->irqNode);
        }

    pTCDData->isAlaysShortPkt = gUsbPlxTcdAlwaysShortPkt[pDev->unitNumber];

    /* Update the TCD create information */

    usbTgtTcdInfo.pDev = pDev;
    usbTgtTcdInfo.pTcdFuncs = &gUsbPlxTcdDriver;
    usbTgtTcdInfo.pTcdSpecific = (pVOID)pTCDData;
    usbTgtTcdInfo.uVendorID = gUsbPlxTcdVid[pDev->unitNumber];
    usbTgtTcdInfo.uProductID = gUsbPlxTcdPid[pDev->unitNumber];
    usbTgtTcdInfo.uBcdDevice = gUsbPlxTcdBcdDevice[pDev->unitNumber];
    usbTgtTcdInfo.pMfgString = gUsbPlxTcdMfgString[pDev->unitNumber];
    usbTgtTcdInfo.pProdString = gUsbPlxTcdProdString[pDev->unitNumber];
    usbTgtTcdInfo.pSerialString = gUsbPlxTcdSerialString[pDev->unitNumber];
    usbTgtTcdInfo.pTcdName = gUsbPlxTcdName[pDev->unitNumber];
    usbTgtTcdInfo.uTcdUnit = (UINT8)pDev->unitNumber;
    usbTgtTcdInfo.uConfigCount = gUsbPlxTcdConfigNum[pDev->unitNumber];

    USB_PLX_DBG ("usbPlxTcdDataInit(): Init the usbTgtTcdInfo\n",
                 1, 2, 3, 4, 5, 6);

    /* Init the device information */

    pDeviceInfo = &(usbTgtTcdInfo.DeviceInfo);

    /* Support self power don't support the remote wakeup */

    pDeviceInfo->uDeviceFeature = USB_DEV_STS_LOCAL_POWER;

    /* The speed capability */

    pDeviceInfo->uDevSpeedCap = (0x1 << USB_SPEED_FULL) |
                                (0x1 << USB_SPEED_HIGH) |
                                (0x1 << USB_SPEED_SUPER);

    /* Support control transfer and enabled */

    pDeviceInfo->txEpCaps[0].bUsable = TRUE;
    pDeviceInfo->txEpCaps[0].uMaxPktSize = USB_ENDPOINT_MAXPSIZE(0x40);
    pDeviceInfo->txEpCaps[0].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL);

    pDeviceInfo->rxEpCaps[0].bUsable = FALSE;
    pDeviceInfo->rxEpCaps[0].uMaxPktSize = 0;
    pDeviceInfo->rxEpCaps[0].uXferTypeBitMap = 0;

    /* Support other transfer and enabled */

    for (i = 1; i <= pPlxCtrl->uMaxEpNum; i++)
        {
        /* The max packet size is 512 and support all transfer types */

        pDeviceInfo->txEpCaps[i].bUsable = TRUE;
        pDeviceInfo->txEpCaps[i].bRxTxShared = TRUE;
        pDeviceInfo->txEpCaps[i].uMaxPktSize = USB_ENDPOINT_MAXPSIZE(1024);
        pDeviceInfo->txEpCaps[i].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL) |
                                                   (0x1 << USB_ATTR_ISOCH) |
                                                   (0x1 << USB_ATTR_BULK) |
                                                   (0x1 << USB_ATTR_INTERRUPT);
        pDeviceInfo->txEpCaps[i].uSyncTypeBitMap = (0x1 << USB_ATTR_EP_SYNCH_TYPE_NOSYNCH) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_ASYNCH) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_ADAPTIVE) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_SYNCH);

        pDeviceInfo->txEpCaps[i].uUsageTypeBitMap = (0x1 << USB_ATTR_EP_USAGE_TYPE_DATA) |
                                                   (0x1 << USB_ATTR_EP_USAGE_TYPE_FEEDBACK) |
                                                   (0x1 << USB_ATTR_EP_USAGE_TYPE_IMFEEDBACK);

        pDeviceInfo->rxEpCaps[i].bUsable = TRUE;
        pDeviceInfo->rxEpCaps[i].bRxTxShared = TRUE;

        pDeviceInfo->rxEpCaps[i].uMaxPktSize = USB_ENDPOINT_MAXPSIZE(1024);
        pDeviceInfo->rxEpCaps[i].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL) |
                                                   (0x1 << USB_ATTR_ISOCH) |
                                                   (0x1 << USB_ATTR_BULK) |
                                                   (0x1 << USB_ATTR_INTERRUPT);
        pDeviceInfo->rxEpCaps[i].uSyncTypeBitMap = (0x1 << USB_ATTR_EP_SYNCH_TYPE_NOSYNCH) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_ASYNCH) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_ADAPTIVE) |
                                                   (0x1 << USB_ATTR_EP_SYNCH_TYPE_SYNCH);

        pDeviceInfo->rxEpCaps[i].uUsageTypeBitMap = (0x1 << USB_ATTR_EP_USAGE_TYPE_DATA) |
                                                   (0x1 << USB_ATTR_EP_USAGE_TYPE_FEEDBACK) |
                                                   (0x1 << USB_ATTR_EP_USAGE_TYPE_IMFEEDBACK);
        }

    /* Use the information register to TML */

    pTCDData->pTcd = usbTgtTcdRegister(&usbTgtTcdInfo);

    if (NULL == pTCDData->pTcd)
        {
        USB_PLX_ERR("Register the TCD to TML fail\n",
                     1, 2, 3, 4, 5 ,6);

        usbPlxTcdDataUnInit(pTCDData);

        return ERROR;
        }

    pTcd = pTCDData->pTcd;

    USB_PLX_DBG ("usbPlxTcdDataInit():Create the irq hander\n",
                 1, 2, 3, 4, 5, 6);

    /*
     * Assign an unique name for the interrupt handler thread
     * This buffer is temp buffer, with the max size 0x20
     */

    snprintf(taskName, 0x20, "tPlxIsr%d", pTcd->uTcdUnit);

    /* Create the interrupt handler thread for handling the interrupts */

    pTCDData->isrHandlerThread = OS_CREATE_THREAD(taskName,
                               gUsbPlxTcdIsrPriority[pTcd->uTcdUnit],
                               usbPlxTcdInterruptHandler,
                               (void *)pPlxCtrl->pTCDData);

    /* Check whether the thread creation is successful */

    if (OS_THREAD_FAILURE == pTCDData->isrHandlerThread)
        {
        USB_PLX_ERR("Interrupt handler thread is not created\n",
                     1, 2, 3, 4, 5 ,6);

        usbPlxTcdDataUnInit(pTCDData);

        return ERROR;
        }
    pTCDData->uDmaType = gUsbPlxTcdDmaType;
    pTCDData->ep0Stage = USB_TCD_EP0_STAGE_IDLE;

    USB_PLX_DBG ("usbPlxTcdDataInit():Done\n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdInstanceUnLoad - unload the target controller instance
*
* This routine is to unload the target controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdInstanceUnLoad
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pPlxCtrl->pTCDData))
        {
        USB_PLX_ERR("Invalid Parameters, %s is NULL\n",
                    ((NULL == pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDData"), 2, 3, 4, 5 ,6);

        return ERROR;
        }

    pTCDData = (pUSB_PLX_TCD_DATA)(pPlxCtrl->pTCDData);

    USB_PLX_DBG ("usbPlxTcdInstanceUnLoad():pPlxCtrl %p pTCDData %p\n",
                 pPlxCtrl, pTCDData, 3, 4, 5, 6);

    /* Release the resource of the pTCDData */

    usbPlxTcdDataUnInit(pTCDData);

    pPlxCtrl->pTCDData = NULL;

    if (ERROR != lstFind(&gUsbPlxTcdList, &pTCDData->tcdNode))
        {
        lstDelete(&gUsbPlxTcdList, &pTCDData->tcdNode);
        }

    OS_FREE(pTCDData);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdInstanceLoad - add a target controller instance
*
* This routine is to add a target controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdInstanceLoad
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Invalid parameter\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdInstanceLoad(): In\n",
                 1, 2, 3, 4, 5, 6);

    pTCDData = (pUSB_PLX_TCD_DATA)OSS_CALLOC(sizeof(USB_PLX_TCD_DATA));

    /* Check if memory allocation is successful */

    if (NULL == pTCDData)
        {
        USB_PLX_ERR("Memory not allocated for USB_PLX_DATA structure\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdInstanceLoad(): alloc pTCDData %p\n",
                 pTCDData, 2, 3, 4, 5, 6);

    /* Add the TCD to global list */

    lstAdd(&gUsbPlxTcdList, &pTCDData->tcdNode);

    /* Save the common hardware information structure */

    pTCDData->pPlxCtrl = pPlxCtrl;

    /* Save the TCD information structure */

    pPlxCtrl->pTCDData = pTCDData;

    /* Initialize the target Controller data structure */

    if (OK != usbPlxTcdDataInit(pTCDData))
        {
        USB_PLX_ERR("usbPlxTcdDataInit failed\n",
                      1, 2, 3, 4, 5 ,6);

        /* Uninitialize the hardware */

        pTCDData->pPlxCtrl = NULL;

        /* Remove the tcd from the global list */

        if (ERROR != lstFind(&gUsbPlxTcdList, &pTCDData->tcdNode))
            lstDelete(&gUsbPlxTcdList, &pTCDData->tcdNode);

        OS_FREE(pTCDData);
        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdInstanceLoad(): Done\n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdVxbDeviceRemove - remove the PLX TCD target controller
*
* This routine removes the PLX TCD target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdVxbDeviceRemove
    (
    VXB_DEVICE_ID       pDev
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;

    /* Parameter verification */

    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USB_PLX_ERR("Invalid Parameters, %s is NULL\n",
                    ((NULL == pDev) ? "pDev" :
                     "pDrvCtrl"), 2, 3, 4, 5 ,6);

        return ERROR;
        }

    USB_PLX_DBG("usbPlxTcdVxbDeviceRemove(): In\n",
                 1, 2, 3, 4, 5, 6);

    pPlxCtrl = (pUSB_PLX_CTRL)(pDev->pDrvCtrl);

    (void)(vxbIntDisable (pDev,
                           0,
                           (usbPlxTcdIsr),
                           (void *)(pPlxCtrl)));

    /* Disable the hardware interrupt */

    USB_PLX_PCI_IRQ_DISABLE(pPlxCtrl);

    /* Disable the target controller instance */

    (void) usbPlxTcdInstanceDisable(pPlxCtrl);

    /* Unload the target controller instance */

    (void) usbPlxTcdInstanceUnLoad(pPlxCtrl);

    /* Uninitialize the hardware */

    (void) usbPlxTcdHardwareUnInit(pPlxCtrl);

    (void)(vxbIntDisconnect(pDev,
                            0,
                            (usbPlxTcdIsr),
                            (void *)(pPlxCtrl)));

    (void) usbPlxTcdCtrlDataUnInit(pPlxCtrl);

    USB_PLX_DBG("usbPlxTcdVxbDeviceRemove(): Done\n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }


/*****************************************************************************
*
* usbPlxTcdVxbDeviceConnect - vxBus instConnect handler
*
* This routine implements the VxBus instConnect handler for the
* device instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void usbPlxTcdVxbDeviceConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;

    /* Parameter verification */

    if (NULL == pDev)
        {
        USB_PLX_ERR("Ivalid parameter, pDev is NULL\n",
                    1, 2, 3, 4, 5 ,6);
        return;
        }

    USB_PLX_DBG("usbPlxTcdVxbDeviceConnect(): in\n",
                 1, 2, 3, 4, 5, 6);

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): Init pPlxCtrl\n",
                 1, 2, 3, 4, 5, 6);

    /* Create the PLX controller data */

    pPlxCtrl = usbPlxTcdCtrlDataInit(pDev);

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("usbPlxTcdVxbConnect() init the pPlxCtrl fail\n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): Disable interrupts\n",
                 1, 2, 3, 4, 5, 6);

    /* Disable the hardware interrupt */

    USB_PLX_PCI_IRQ_DISABLE(pPlxCtrl);

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): hardware init\n",
                 1, 2, 3, 4, 5, 6);

    /* Initialize the common hardware data structure */

    if (usbPlxTcdHardwareInit(pPlxCtrl) != OK)
        {
        USB_PLX_ERR("usbPlxTcdHardwareInit failed\n",
                    1, 2, 3, 4, 5, 6);

        (void) usbPlxTcdCtrlDataUnInit(pPlxCtrl);

        return;
        }

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): connect the interrupt handler\n",
                 1, 2, 3, 4, 5, 6);

    /* Register the interrupt handler for the IRQ */

    if (ERROR == (vxbIntConnect (pDev,
                                 0,
                                 (usbPlxTcdIsr),
                                 (void *)(pPlxCtrl))))
        {
        USB_PLX_ERR("Error hooking the core ISR %p for pDev %p\n",
                    usbPlxTcdIsr, pDev, 3, 4, 5 ,6);

        (void) usbPlxTcdCtrlDataUnInit(pPlxCtrl);

        return;
        }

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): Load the TCD instance\n",
                 1, 2, 3, 4, 5, 6);

    /* Add a target controller instance */

    if (usbPlxTcdInstanceLoad(pPlxCtrl) != OK)
        {
        USB_PLX_ERR("usbPlxTcdInstanceLoad failed\n",
                    1, 2, 3, 4, 5, 6);
        (void) usbPlxTcdHardwareUnInit(pPlxCtrl);
        (void) usbPlxTcdCtrlDataUnInit(pPlxCtrl);
        return;
        }

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): Enable the TCD instance\n",
                 1, 2, 3, 4, 5, 6);

    if (ERROR == (vxbIntEnable (pDev,
                                 0,
                                 (usbPlxTcdIsr),
                                 (void *)(pPlxCtrl))))
        {
        USB_PLX_ERR("Enable ISR %p for pDev %p\n",
                    usbPlxTcdIsr, pDev, 3, 4, 5 ,6);

        (void) usbPlxTcdInstanceUnLoad(pPlxCtrl);
        (void) usbPlxTcdHardwareUnInit(pPlxCtrl);
        (void) usbPlxTcdCtrlDataUnInit(pPlxCtrl);

        return;
        }

    /* Just set the magic code, do not soft connect controller */

    pPlxCtrl->uMagicCode = USB_PLX_MAGIC_ALIVE;

    USB_PLX_VDBG("usbPlxTcdVxbDeviceConnect(): Enable interrupt\n",
                 1, 2, 3, 4, 5, 6);

    /* Enable the hardware interrupt */

    USB_PLX_PCI_IRQ_ENABLE(pPlxCtrl);

    USB_PLX_DBG("usbPlxTcdVxbDeviceConnect():Done\n",
                 1, 2, 3, 4, 5, 6);

    return;
    }


/*******************************************************************************
*
* usbPlxTcdInstanceEnable - enable the target controller instance
*
* This routine is to enable the target controller instance.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdInstanceEnable
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT32 uUsbCtl = 0;

    /* Set the magic code first */

    pPlxCtrl->uMagicCode = USB_PLX_MAGIC_ALIVE;

    uUsbCtl = USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_USBCTL_REG);

    /* Set all data enpoint as zero for U1/U2 workaround */

    usbPlxTcdEnableDataEpsAsZero(pPlxCtrl);

    /* Set the device detect enable bit */

    uUsbCtl |= USB_PLX_USBCTL_REG_USBDE |
               USB_PLX_USBCTL_REG_RPWE;

    USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_USBCTL_REG, uUsbCtl);

    USB_PLX_DBG("usbPlxTcdInstanceEnable(): Enable this instance \n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdInstanceDisable - disbale the PLX TCD instance
*
* This routine is to disable the PLX TCD instance.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdInstanceDisable
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT32 uUsbCtl = 0;

    pPlxCtrl->uMagicCode = USB_PLX_MAGIC_DEAD;

    /* Clear device detect enable bit */

    uUsbCtl = USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_USBCTL_REG);

    uUsbCtl &= ~ USB_PLX_USBCTL_REG_USBDE;

    USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_USBCTL_REG, uUsbCtl);

    USB_PLX_DBG("usbPlxTcdInstanceDisable(): Disable this instance \n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }


