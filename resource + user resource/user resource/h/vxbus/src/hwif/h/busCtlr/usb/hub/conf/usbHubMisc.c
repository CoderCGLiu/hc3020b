/* usbHubMisc.c - debug functionality functions */

/*
 * Copyright (c) 2003-2004, 2010, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2004, 2010, 2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01i,04jan13,s_z  Remove compiler warning (WIND00390357)
01h,08jul10,m_y  Modify for coding convention
01g,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
01f,24may10,m_y  Change OS_ASSERT to if judgement for usb robustness (WIND00183499)
01e,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01d,11mar10,j_x  Changed for USB debug (WIND00184542)
01c,13jan10,ghs  vxWorks 6.9 LP64 adapting
01b,15oct04,ami  Refgen Changes
01a,16Sep03,amn  Changing the code to WRS standards
*/

/*
DESCRIPTION

This module provides the debug functionality functions for the
USB Hub Class Driver

INCLUDE FILES: usb2/usbHubCommon.h, usb2/usbHubUtility.h, usb2/usbHubMisc.h

*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbHubMisc.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This module provides the debug functionality functions for
 *                    the USB Hub Class Driver.
 *
 *
 ******************************************************************************/


/************************** INCLUDE FILES *************************************/

#include "usbHubCommon.h"
#include "usbHubUtility.h"
#include "usb/usbHubMisc.h"



/****************** MODULE SPECIFIC  MACRO DEFINITIONS ************************/

/* Defines for the return types for detected devices */

#define USBHUB_NODETYPE_HUB      (0x02)
#define USBHUB_NODETYPE_DEVICE   (0x01)
#define USBHUB_NODETYPE_NONE     (0x00)


/************************ GLOBAL FUNCTIONS ************************************/

/***************************************************************************
*
* usbHubPortCntGet -  returns the number of ports connected to a  hub.
*
* This routine returns the number of ports connected to a  hub.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if error occured.
*
* ERRNO: None
*
* \NOMANUAL
*/

USBHST_STATUS usbHubPortCntGet
    (
    UINT32  uHubId,
    PUINT16 pPortCount
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* The pointer to the actual hub */
    pUSB_HUB_INFO pHub             = NULL;

    /* port number of the device */
    UINT8 uPortIndex           = 0;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* if NULL parameter return USBHST_INVALID_PARAMETER */
    if (NULL == pPortCount)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL == pPortCount ) */

    /*
     * Call usbHubFindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */
    pParentHub = usbHubFindParentHubInBuses(uHubId);

    /* if not found and not a root hub, return USBHST_INVALID_PARAMETER */
    if (NULL == pParentHub)
        {
        /* The bus List pointer to be used for browsing the list */
        pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

        /* While the pbusList is not null */
        while ( NULL != pBusList )
            {
            /* Check if we have a root hub info */
            if ( NULL != pBusList->pRootHubInfo)
                {
                /* Check if the device handles match */
                if (uHubId == pBusList->pRootHubInfo->uDeviceHandle)
                    {
                    /* we got the hub */
                    pHub = pBusList->pRootHubInfo;
                    break;
                    }
                } /* End of if ( NULL != pBusList->pRootHubInfo) */

            /* Move the pointer to the next bus */
            pBusList=pBusList->pNextBus;

            }/* End of while (pBus... */

        /* Check if we found the root hub if not, return error */
        if (NULL == pBusList)
            {
            /* Debug Message */
            USB_HUB_ERR("parent hub not found \n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;
            }/* End of if NULL... */

        }/* End of if (NULL == pParentHub ) */
    /* there is a parent hub */
    else
        {
        /*
         * HUB_FindPortNumber() to find the port number for this device handle.
         * If this is not found, then return USBHST_INVALID_PARAMETER.
         */
        uPortIndex = usbHubFindPortNumber(pParentHub,uHubId);

        /* Check  if the port is found */
        if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
            {
            /* Debug Message */
            USB_HUB_ERR("port number not found \n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;

            } /* End of if (PORT_NUM.. */

        /* Check if the port number is within limits */
        if (uPortIndex >= (pParentHub->HubDescriptor.bNbrPorts))
            {
            /* Debug Message */

            USB_HUB_ERR("port num %d > max %d \n",
                        uPortIndex,
                        pParentHub->HubDescriptor.bNbrPorts,
                        3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;

            } /* End of (uPortIndex > (pParentHub->.... */

        /*
         * Retrieve HUB_INFO structure from
         * parentHub::pPortList[uPortIndex]::pHub.
         */

        pPort = pParentHub->pPortList[uPortIndex];

        if (pPort == NULL)
            {
            USB_HUB_ERR("The pointer of the port is NULL \n",
                        1, 2, 3, 4, 5, 6);
            return USBHST_INVALID_PARAMETER;
            }

        /* retrieve the pHub */
        pHub = pPort->pHub;

        /*
         * Check if the port is actually a hub. if not, return failure.
         */
        if (NULL == pHub)
            {
            /* Debug Message */

            USB_HUB_ERR("hub handle 0x%x is not a hub \n",
                        uHubId, 2, 3, 4, 5, 6);

            return USBHST_FAILURE;

            } /* End of if (NULL == pPort->pHub) */

        } /* End of else of if (NULL ==pParentHub) */

    /* retrieve the port count information */

    *pPortCount = pHub->HubDescriptor.bNbrPorts;

    /* return USBHST_SUCCESS */
    return USBHST_SUCCESS;

} /* End of usbdHubPortCountget() */


/***************************************************************************
*
* usbHubNodeIDGet - Gets the node id of the node connected to hub port.
*
* This routine gets the node id of the node connected to hub port.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if error occured.
*
* ERRNO: None
*
* \NOMANUAL
*/

USBHST_STATUS usbHubNodeIDGet
    (
    UINT32  uHubId,
    UINT16  uPortIndex,
    PUINT16 pNodeType,
    PUINT32 pNodeId
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* The pointer to the actual hub */
    pUSB_HUB_INFO pHub             = NULL;

    /* port number of the device */
    UINT8 uPortCount           = 0;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* if NULL parameter return USBHST_INVALID_PARAMETER */
    if ((NULL == pNodeType)||(NULL == pNodeId))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if ((NULL == pNodeType)||(NULL == pNodeId)) */

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */
    pParentHub = usbHubFindParentHubInBuses(uHubId);

    /* if not found and not a root hub, return USBHST_INVALID_PARAMETER */
    if (NULL == pParentHub)
        {
        /* The bus List pointer to be used for browsing the list */
        pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

        /* While the pbusList is not null */
        while ( NULL != pBusList )
            {
            /* Check if we have a root hub info */
            if ( NULL != pBusList->pRootHubInfo)
                {
                /* Check if the device handles match */
                if (uHubId == pBusList->pRootHubInfo->uDeviceHandle)
                    {
                    /* we got the hub */
                    pHub = pBusList->pRootHubInfo;
                    break;
                    }
                } /* End of if ( NULL != pBusList->pRootHubInfo) */

            /* Move the pointer to the next bus */
            pBusList=pBusList->pNextBus;

            }/* End of while (pBus... */

        /* Check if we found the root hub */
        if (NULL == pBusList)
            {
            /* Debug Message */
            USB_HUB_ERR("parent hub not found \n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;
            }/* End of if NULL... */

        }/* End of if (NULL == pParentHub ) */
    else
        {
        /*
         * HUB_FindPortNumber() to find the port number for this device handle.
         * If this is not found, then return USBHST_INVALID_PARAMETER.
         */
        uPortCount = usbHubFindPortNumber(pParentHub,uHubId);

        /* Check  if the port is found */
        if (USB_PORT_NUMBER_NOT_FOUND == uPortCount)
            {
            /* Debug Message */
            USB_HUB_ERR("port number not found \n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;

            } /* End of if (PORT_NUM.. */

        /* Check if the port number is within limits */
        if (uPortCount >= (pParentHub->HubDescriptor.bNbrPorts))
            {
            /* Debug Message */
            USB_HUB_ERR("port num %d > max %d \n",
                        uPortCount,
                        pParentHub->HubDescriptor.bNbrPorts,
                        3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;

            } /* End of (uPortCount > (pParentHub->.... */

        /*
         * Retrieve HUB_INFO structure from
         * parentHub::pPortList[uPortCount]::pHub.
         */
        pPort = pParentHub->pPortList[uPortCount];

        if (pPort == NULL)
            {
            USB_HUB_ERR("The pointer of the port is NULL\n", 1, 2, 3 ,4 ,5, 6);
            return USBHST_INVALID_PARAMETER;
            }

        /* retrieve the pHub */
        pHub = pPort->pHub;

        /*
         * Check if the port is actually a hub. if not, return failure.
         */
        if (NULL == pHub)
            {
            /* Debug Message */

            USB_HUB_ERR("hub handle 0x%x is not a hub \n",
                        uHubId, 2, 3, 4, 5, 6);

            return USBHST_FAILURE;

            } /* End of if (NULL == pHub) */

        } /* End of else of if (NULL == pParentHub) */

    /* Check if the port Index parameter is within limits */
    if (uPortIndex >= (pHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */
        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        } /* End of (uPortCount > (pHub->.... */

    /*
     * Retrieve HUB_PORT_INFO structure from
     * pHub::pPortList[uPortIndex].
     */
    pPort = pHub->pPortList[uPortIndex];

    /* Check if the port is having a device connection */
    if (NULL == pPort)
        {
        /* Debug Message */
        USB_HUB_VDBG("usbHubNodeIDGet - no device connection \n",
                    1, 2, 3, 4, 5, 6);

        /* no device connected */
        * pNodeType = USBHUB_NODETYPE_NONE;
        * pNodeId = 0 ;

        /* return success */
        return USBHST_SUCCESS;

        } /* End of if (NULL == pPort..)*/

    *pNodeId = pPort->uDeviceHandle;

    /* Check if the port is having a valid device handle detected by usb stack*/
    if (0 == pPort->uDeviceHandle )
        {
        /* Debug Message */
        USB_HUB_VDBG("usbHubNodeIDGet - no device connection \n",
                    1, 2, 3, 4, 5, 6);

        * pNodeType = USBHUB_NODETYPE_NONE;

        /* return success */
        return USBHST_SUCCESS;

        } /* End of if (0 == pPort->uDeviceHandle ) */

    /* Check if the port is having a device */
    if (NULL == pPort->pHub )
        {
        /* Debug Message */
        USB_HUB_VDBG("usbHubNodeIDGet - normal device connection \n",
                    1, 2, 3, 4, 5, 6);

        * pNodeType = USBHUB_NODETYPE_DEVICE;

        /* return success */
        return USBHST_SUCCESS;

        } /* if (NULL == pPort->pHub ) */

    /* the port is having a Hub connected to it */
    * pNodeType = USBHUB_NODETYPE_HUB;

    /* Debug Message */
    USB_HUB_VDBG("usbHubNodeIDGet - hub connection \n",
                1, 2, 3, 4, 5, 6);

    /* return success */
    return USBHST_SUCCESS;

} /* End of usbdNodeIdGet */


/***************************************************************************
*
* usbHubGetRootHubHandle - Finding the root hub handle of a Bus.
*
* This routine is called by the USB Host Software Stack for finding the root
* hub handle of a bus.
*
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if error occured.
*
* ERRNO: None
*
* \NOMANUAL
*/

USBHST_STATUS usbHubGetRootHubHandle
    (
    UINT8   uBusHandle,
    PUINT32 pRootHubHandle
    )
    {
    /* Bus List pointer */
    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

    /* Root hub structure */
    pUSB_HUB_INFO pRootHub = NULL;

    /* Validate the parameters */
    if (NULL == pRootHubHandle)
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        /* return USBHST_INVALID_PARAMETER */
        return USBHST_INVALID_PARAMETER;

        } /* End of if (NULL == pRootHubHandle) */

    /*
     * Scan through the g_pGlobalBus to find the matching HUB_BUS_INFO structure
     * for the uBusHandle. If not found return.
     */
    while (NULL != pBusList)
        {
        /* Check for duplicate entries */
        if (pBusList->uBusHandle == uBusHandle)
            {
            /* Jump out of the loop */
            break;

            } /* End of if (pBusList->....*/

        /* Move the pointer to the next bus */
        pBusList=pBusList->pNextBus;

        } /* End of While (NULL != pBusList) */

    /* Check if we found the bus */
    if (NULL == pBusList)
        {
        /* Debug Message */
        USB_HUB_ERR("bus not found \n",
                    1, 2, 3, 4, 5, 6);

        /* nope.. we did not find the bus, so we return  failure*/
        return USBHST_FAILURE;

        } /* End of if (NULL==pBusList) */

    /* Retrieve the HUB_INFO structure from the HUB_BUS_INFO structure.*/
    pRootHub = pBusList->pRootHubInfo;

    /* Check if the root hub is present */
    if (NULL == pRootHub)
        {
        /* Debug Message */
        USB_HUB_ERR("root hub not found \n",
                    1, 2, 3, 4, 5, 6);

        /* nope.. we did not find the root hub, so we return  failure*/
        return USBHST_FAILURE;
        } /* End of if if (NULL == pRootHub) */

    /* Retrieve the device handle */
    *pRootHubHandle = pRootHub->uDeviceHandle;

    /* Check if the root hub handle is valid */
    if (0 == *pRootHubHandle)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid root hub handle \n",
                    1, 2, 3, 4, 5, 6);

        /* nope.. we did not find the root hub, so we return  failure*/
        return USBHST_FAILURE;

        } /* End of if if (0 == pRootHub->uDeviceHandle) */

   /* return success */
   return USBHST_SUCCESS;

}/* End of usbHubGetRootHubHandle() */


/**************************** End of File usbHubMisc.c *************************/
