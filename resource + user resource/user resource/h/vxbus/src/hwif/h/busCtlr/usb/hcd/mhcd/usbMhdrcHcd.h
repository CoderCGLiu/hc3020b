/* usbMhdrcHcd.h - MHCI host controller driver interface definition */

/*
 * Copyright (c) 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01j,21apr13,wyy  Make the preprocessor look for usbMhdrc.h and 
                 usbMhdrcPlatform.h in the current directory first
01i,17feb12,s_z  Fix 15+ devices pending issue by redefining channel
                 swith interval (WIND0033840)
01h,28mar11,w_x  Use spinlock for USB_MHDRC_HCD_REPORT_EVENT (WIND00262862)
01g,23mar11,w_x  Move USB_MHDRC_HCD_VBUSERROR_RECOVERY_MAX to usbMhdrc.h
01f,16sep10,m_y  Update the prototype for MHCI's exit routine (WIND00232860)
01e,16aug10,w_x  VxWorks 64 bit audit and warning removal
01d,13mar10,s_z  Add Inventra DMA support for OMAP3EVM
01c,02feb10,s_z  Code cleaning
01b,03nov09,s_z  Delete some defination for the rewriting
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file contains the constants, data structures and functions exposed by the
MHCI USB host controller driver.

*/

#ifndef __INCusbMhdrcHcdh
#define __INCusbMhdrcHcdh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <vxWorks.h>
#include <lstLib.h>
#include <cacheLib.h>
#include <hwif/vxbus/vxBus.h>
#include <usb/usb.h>
#include <usb/usbHst.h>
#include <usb/usbHcdInstr.h>
#include "usbMhdrc.h" 
#include "usbMhdrcPlatform.h" 

/* defines */

#define DMA_FLUSH(pBfr, bytes)               CACHE_DMA_FLUSH (pBfr, bytes)
#define DMA_INVALIDATE(pBfr, bytes)          CACHE_DMA_INVALIDATE (pBfr, bytes)
#define USER_FLUSH(pBfr, bytes)              CACHE_USER_FLUSH (pBfr, bytes)
#define USER_INVALIDATE(pBfr,bytes)          CACHE_USER_INVALIDATE (pBfr, bytes)

#define USB_MHDRC_INTERRUPT_MASK              0x80000042

/* The max number messagers */

#define USB_MHDRC_HCD_MAX_MSG        50

#define USB_MHDRC_HCD_MAX_DEVICE_ADDRESS                (127)

#define USB_MHDRC_HCD_FRAME_NUM_MAX                     (0x800)

#define USB_MHDRC_HCD_EP0_MAX_PACKET_SIZE               (0x40)

#define USB_MHDRC_HCD_TRANSFER_INTERVAL_TO_SWITCH       (8)

#define USB_MHDRC_ENDPOINT_CHANNEL_1                     (1)
#define USB_MHDRC_ENDPOINT_CHANNEL_2                     (2)
#define USB_MHDRC_ENDPOINT_CHANNEL_3                     (3)
#define USB_MHDRC_ENDPOINT_CHANNEL_4                     (4)
#define USB_MHDRC_ENDPOINT_CHANNEL_MORE                  (0xFFE0)


/* Use En 1 and Ep 2 for interrupt transaction */
#define USB_MHDRC_HCD_INTERRUPT_CHANNEL_MASK ((0x1 << USB_MHDRC_ENDPOINT_CHANNEL_1) | \
                                         (0x1 << USB_MHDRC_ENDPOINT_CHANNEL_2) | \
                                         (USB_MHDRC_ENDPOINT_CHANNEL_MORE))


/* Use Ep 3 and Ep 4 for Bulk transaction */

#define USB_MHDRC_HCD_BULK_CHANNEL_MASK      ((0x1 << USB_MHDRC_ENDPOINT_CHANNEL_3) | \
                                         (0x1 << USB_MHDRC_ENDPOINT_CHANNEL_4) | \
                                         (USB_MHDRC_ENDPOINT_CHANNEL_MORE))


#define USB_MHDRC_HCD_MUTEX_CREATION_OPTS   (SEM_Q_PRIORITY |       \
                                            SEM_INVERSION_SAFE |    \
                                            SEM_DELETE_SAFE)

/* Define the schedule frequence union as milliseconds */

#define USB_MHDRC_HCD_SCHEDULE_FREQ_MS    (500)

/* ISR polling Ms */

#define USB_MHDRC_HCD_ISR_POLLING_MS      (20)

/*
 * Macro definition to convet a CPU address to a BUS address.
 */

#define USB_MHCD_CONVERT_TO_BUS_MEM(INDEX, ADDRESS)                            \
    (((ADDRESS == 0)|| ((gpMHCDData[INDEX]->pMHDRC->pCpuToBus) == NULL)) ?     \
     (pVOID)ADDRESS: (*(gpMHCDData[INDEX]->pMHDRC->pCpuToBus))((pVOID)ADDRESS))

/*
 * Macro definition to convet a BUS address to a CPU address.
 */

#define USB_MHCD_CONVERT_FROM_BUS_MEM(INDEX, ADDRESS)                          \
    (((ADDRESS == 0) || ((gpMHCDData[INDEX]->pMHDRC->pBusToCpu) == NULL)) ?    \
     (pVOID)ADDRESS : (*(gpMHCDData[INDEX]->pMHDRC->pBusToCpu))((pVOID)ADDRESS))

/*
 * Macro definition to swap the 32 bit values of HC data structures
 */

#define USB_MHCD_SWAP_DESC_DATA(INDEX,VALUE)                                   \
    ((((gpMHCDData[INDEX])->pMHDRC->pDescSwap) == NULL) ? VALUE :              \
                (*(gpMHCDData[INDEX]->pMHDRC->pDescSwap))(VALUE))

/*
 * Macro definition to swap the 32 bit values of USB formated data
 */

#define USB_MHCD_SWAP_USB_DATA(INDEX,VALUE)                                    \
    ((((gpMHCDData[INDEX]->pMHDRC)->pUsbSwap) == NULL) ? VALUE :               \
                (*(gpMHCDData[INDEX]->pMHDRC->pUsbSwap))(VALUE))

/* typedefs */

/* Flags for PIPE */

typedef enum
    {
    USB_MHCD_PIPE_FLAG_OPEN                = (0x1 << 0),
    USB_MHCD_PIPE_FLAG_SCHEDULED           = (0x1 << 1),
    USB_MHCD_PIPE_FLAG_NEED_PING           = (0x1 << 2),
    USB_MHCD_PIPE_FLAG_DELETE              = (0x1 << 3),
    USB_MHCD_PIPE_FLAG_MODIFY_DEFAULT_PIPE = (0x1 << 4),
    USB_MHCD_PIPE_FLAG_NEED_ZLP            = (0x1 << 5),
    USB_MHCD_PIPE_FLAG_NEED_PROCESS_ZLP    = (0x1 << 6)
    } USB_MHCD_PIPE_FLAG;

/* Flags for requestinfo */

typedef enum
    {
     USB_MHCD_REQUEST_TO_BE_CANCELED    = (0x1 << 0),
     USB_MHCD_REQUEST_STILL_TRANSFERING = (0x1 << 1),
     USB_MHCD_REQUEST_TRACTION_FINISH   = (0x1 << 2),
     USB_MHCD_REQUEST_TO_REMOVE         = (0x1 << 3)
    }USB_MHCD_REQUEST_FLAG;

/* Flags' stage for the transfering */
/*
 * Mark the request will be canceled , to stop submitting another urb
 * Mark the request is transfering, to cancel it,you need do special
 * Mark the request is in a special status in the transfer,may be it just
 * change into a new status, but still not start transfer??
 */

/* TODO: to see if it can use one flag */

typedef enum
    {
    USB_MHCD_REQUEST_STAGE_NON_CONTROL = 0,
    USB_MHCD_REQUEST_STAGE_SETUP       = 1,
    USB_MHCD_REQUEST_STAGE_DATA        = 2,
    USB_MHCD_REQUEST_STAGE_STATUS      = 3,
    USB_MHCD_REQUEST_STAGE_DONE        = 4,
    USB_MHCD_REQUEST_STAGE_DMA_START   = 5
    }USB_MHCD_CONTROL_REQUEST_STAGE_FLAG;

/* Command for the transfer task */

typedef enum
    {
    USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS   = (0x1 << 0),
    USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS         = (0x1 << 1),
    USB_MHCD_TRANSFER_CMD_CONTROL_CHANNEL_PROCESS = (0x1 << 2),
    USB_MHCD_TRANSFER_CMD_COMPLITE_PROCESS        = (0x1 << 3)
    } USB_MHCD_TRANSFER_CMD;

struct usb_mhcd_requeset_info;

typedef struct usb_mhcd_pipe
    {
    NODE pipeListNode;       /* Pipe List node */
    SEM_ID pPipeSynchMutex;
    LIST   RequestInfoList;
    UINT32 uHandle;
    UINT16 uIsoPipeUrbCount;          /* Record the urb count of the ISO pipe */
    UINT16 uIsoPipeCurrentUrbIndex;   /* Record the next urb need to deal with */

    UINT16 uMaximumPacketSize; /* To hold the maximum packet size */
    UINT16 uMultiTransPerMicroFrame;
    UINT16 uHubInfo;           /*
                                * This holds any additional details needed for an
                                * endpoint like the details of the parent hub address
                                * and the port number to which the device is
                                * connected.
                                */
                               /* Nearest high speed hub and port number
                                * info; used for split transfer to full/low
                                * speed devices. high byte holds high speed
                                * address, low byte is port number
                                */

    UINT8  uEndpointAddress;  /* Address of the endpoint */
    UINT8  uDeviceAddress;    /* Address of the device holding the endpoint */
    UINT8  uSpeed;            /* Speed of the device, defined according to HOST_TYPE reg */
    UINT8  uEndpointType;     /* Type of endpoint */
    UINT8  uEndpointDir;      /* Direction of the endpoint IN Ture, OUT False */
    UINT8  uDataToggle;
    UINT8  uDmaChannel;       /*Hardware DMA channel for this PIPE */
    UINT8  uPipeFlag;         /*
                               * Flag indicating whether the endpoint
                               * is being deleted.
                               */
    UINT8  bIsHalted;         /*
                               * Flag indicating that the endpoint is halted. This is
                               * set only when the low/ full speed control/ bulk
                               * endpoint results in a transaction error.
                               */
    UINT8  bInterval;/* For periodic pipes, the interval between packets in cycles */
    UINT8  uEndPointEngineAsigned;
    UINT16 uListIndex;        /* Index into the array of endpoint structures. */
    ULONG  uBandwidth;        /* Bandwidth reserved for this endpoint. */
    UINT8  uUFrameMaskValue;  /*
                               * To hold the mask value indicating
                               * the microframes in which the transfer should
                               * happen
                               */
    UINT8 uBusIndex;
    }USB_MHCD_PIPE,*pUSB_MHCD_PIPE;

struct usb_mhcd_data;


/* Data structure maintained by HCD to hold the request information */

typedef struct usb_mhcd_requeset_info
    {
    NODE                    requestNode; /* Must be the first */

    UINT32                  uIsoUrbCurrentPacketIndex; /* The current packet index */
    UINT32                  uIsoUrbTotalPacketCount;   /* Total packet count of Iso URB*/
    UINT32                  uActLength;                /* Actual length of the transfer */
    UINT32                  uXferSize;            /* Bytes size we expect to transfer */
    UINT8 *                 pCurrentBuffer;      /* Data buffer address */
    UINT32                  uTransactionLength;   /* Length of data in one transaction */
    pUSBHST_URB             pUrb;         /* Pointer to the URB */
    pUSB_MHCD_PIPE          pHCDPipe;     /* Pointer to the HCD maintained data structure */
    struct usb_mhcd_data *  pHCDData;     /* pointer to the HCDData */
    UINT16                  uFrameNumberLast; /* Record the last frame number when be scheduled */
    UINT16                  uFrameNumberNext; /* Record the next frame number will be scheduled */
    UINT16                  uIsoPipeUrbIndex;
    UINT8                   uTransferFlag;
    UINT8                   uStage;
    UINT8                   uRetried;
    UINT8                   submitState;
    }USB_MHCD_REQUEST_INFO, * pUSB_MHCD_REQUEST_INFO;

typedef struct _USB_MHCD_RH_PORT_STATUS
    {
    UINT16 uRHPortStatus;
    UINT16 uRHPortChangeStatus;
    }USB_MHCD_RH_PORT_STATUS,*pUSB_MHCD_RH_PORT_STATUS;

typedef struct _USB_MHCD_RH_HUB_STATUS
    {
    UINT16 uRHHubStatus;
    UINT16 uRHHubChangeStatus;
    }USB_MHCD_RH_HUB_STATUS,*pUSB_MHCD_RH_HUB_STATUS;

typedef struct _USB_MHCD_RH_DATA
    {
    UCHAR *      pPortStatus;               /*
                                             * Pointer to an array of port status
                                             * information
                                             */

    USB_MHCD_RH_PORT_STATUS pRootHubPortStatus[USB_MHDRC_RH_DOWNSTREAM_PORT];

    UCHAR        HubStatus[USB_HUB_STATUS_SIZE];/* Buffer holding the hub status */
    UINT8        uNumDownstreamPorts;      /*
                                            * Number of downstream ports supported
                                            * by the Root hub.
                                            */
    UCHAR *      pHubInterruptData;        /*
                                            * Buffer holding the data to be
                                            * returned on an interrupt request.
                                            */
    UINT32       uSizeInterruptData;       /*
                                            * To hold the size of
                                            * the interrupt data
                                            */
    pUSB_MHCD_PIPE  pControlPipe;          /* Control pipe information */
    pUSB_MHCD_PIPE  pInterruptPipe;        /*
                                            * Pointer to the USB_EHCD_PIPE data
                                            * structure for the interrupt endpoint.
                                            */

    pUSB_MHCD_REQUEST_INFO pPendingInterruptRequest;

    UINT8        bRemoteWakeupEnabled;     /*
                                            * Flag indicating whether Remote Wakeup
                                            * is enabled or not
                                            */
    UINT8        bInterruptEndpointHalted; /*
                                            * Flag indicating whether the
                                            * interrupt endpoint is halted or not.
                                            */
    UINT8        uDeviceAddress;           /* Address of the Root hub. */
    UINT8        uConfigValue;             /* Value of the configuration which is set. */
    }USB_MHCD_RH_DATA, *pUSB_MHCD_RH_DATA;

typedef struct usb_mhcd_transfer_task_info
    {
    struct usb_mhcd_data * pHCDData;     /* Device point */
    pUSB_MHCD_REQUEST_INFO pRequestInfo;
    UINT32                 uCmdCode;     /* Command code */
    USBHST_STATUS          uCompleteStatus;
    } USB_MHCD_TRANSFER_TASK_INFO;

struct usb_musbmhdrc;


/* This is the data structure maintained for every Host Controller. */

typedef struct usb_mhcd_data
    {
    /* Global parts */

    UINT8  uBusIndex;

    struct usb_musbmhdrc *  pMHDRC;

    /* Set Bitn to 1 means the HostChannneln is idle, 0 is used */

    UINT16 uFreeEpChannelBitMap;

    /* Map channels to pipes */

    pUSB_MHCD_REQUEST_INFO pRequestInChannel[USB_MHDRC_ENDPOINT_MAX];

    /* Queue definition for data transfer schedule */

    SEM_ID            pHcdSynchMutex;

    /* Default pipe information */

    pUSB_MHCD_PIPE    pDefaultPipe;


    LIST  PeriodicPipeList;
    LIST  NonPeriodicPipeList;

    UINT16  uHcdTxIrqStatus;
    UINT16  uHcdRxIrqStatus;
    UINT8   uHcdUsbIrqStatus;

    /* Event indicate that an interrupt has occurred. */

    OS_EVENT_ID     isrEvent;

    /* Thread ID of interrupt handler thread */

    OS_THREAD_ID    IntHandlerThreadID;

    /* Time to poll the interrupt status and drive SESSION bit */
    int             isrPollingRateMs;
    
    /* MsgQ for transfer management */

    MSG_Q_ID        TransferThreadMsgID;

    /* Task for transfer management */

    OS_THREAD_ID    TransferThreadID;

    /* Data structure holding the detail of the root hub */

    USB_MHCD_RH_DATA RHData;
    }USB_MHCD_DATA, *pUSB_MHCD_DATA;

#define USB_MHDRC_HCD_REPORT_EVENT(pMHDRC, pHCDData)                        \
    {                                                                       \
    SPIN_LOCK_ISR_TAKE(&(pMHDRC)->spinLock);                                \
    (pHCDData)->uHcdTxIrqStatus |= (pMHDRC)->uTxIrqStatus;                  \
    (pHCDData)->uHcdRxIrqStatus |= (pMHDRC)->uRxIrqStatus;                  \
    (pHCDData)->uHcdUsbIrqStatus |= (pMHDRC)->uUsbIrqStatus;                \
    (pMHDRC)->uTxIrqStatus = 0;                                             \
    (pMHDRC)->uRxIrqStatus = 0;                                             \
    (pMHDRC)->uUsbIrqStatus = 0;                                            \
    SPIN_LOCK_ISR_GIVE (&(pMHDRC)->spinLock);                               \
    OS_RELEASE_EVENT((pHCDData)->isrEvent);                                 \
    }

STATUS usbMhdrcHcdDataInit
    (
    pUSB_MHCD_DATA   pHCDData
    );

void usbMhdrcHcdDataUnInit
    (
    pUSB_MHCD_DATA pHCDData
    );

STATUS usbMhdrcHcdInstanceAdd
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

STATUS usbMhdrcHcdInstanceRemove
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

STATUS usbMhdrcHcdInstanceEnable
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

STATUS usbMhdrcHcdInstanceDisable
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

/* externals */

IMPORT pUSB_MHCD_DATA * gpMHCDData;
IMPORT UINT32  gMHCDControllerCount;
IMPORT void usbVxbMhdrcHcdInstantiate (void);
IMPORT STATUS usbMhdrcHcdInit (void);
IMPORT STATUS usbMhdrcHcdExit (void);
IMPORT void usbMhdrcHcdReboot(void);

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcHcdh */

