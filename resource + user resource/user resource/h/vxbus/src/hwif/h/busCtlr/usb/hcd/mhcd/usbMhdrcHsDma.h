/* usbMhdrcHsDma.h - Internal DMA (HSDMA) procession of Mentor Graphics */

/*
 * Copyright (c) 2009 - 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,15sep11,m_y  Using one structure to describe HSDMA and CPPIDMA 
01f,12apr11,m_y  Modify dma interrupt thread priority (WIND00265911)
01e,09mar11,w_x  Code clean up for make man
01d,16aug10,w_x  VxWorks 64 bit audit and warning removal
01c,03jun10,s_z  Coding convention
01b,26nov09,s_z  Correct the type define of interrupt status for vxAtomicOr
                 operation issue
01a,27oct09,s_z  written
*/

/*
DESCRIPTION

This file defines the Internal DMA special register related macros for the 
Mentor Graphics Controller.

INCLUDE FILES: vxWorks.h semLib.h lstLib.h hwif/vxbus/vxBus.h
               usb/usbOsal.h usb/usbHst.h
*/

#ifndef __INCusbMhdrcHsDmah
#define __INCusbMhdrcHsDmah

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <vxWorks.h>
#include <semLib.h>
#include <lstLib.h>
#include <hwif/vxbus/vxBus.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>


/* defines */

#define USB_MHDRC_HS_DMA_TX_MAX_LEN       (0x10000)
#define USB_MHDRC_DMA_INT_THREAD_PRIORITY (49)

/***************************DMA related registers (START)**********************/
#define USB_MHDRC_HS_DMA_BASE             (0x200)

#define USB_MHDRC_HS_DMA_INTR             (0x200) /* 8 bits register */
#define USB_MHDRC_HS_DMA_CONTROL_OFFSET   (0x04)
#define USB_MHDRC_HS_DMA_ADDRESS_OFFSET   (0x08)
#define USB_MHDRC_HS_DMA_COUNT_OFFSET     (0x0C)

/* Internal DMA channel n control register ,16 bits */

#define USB_MHDRC_HS_DMA_CTNL_CH(n)                                           \
                  (USB_MHDRC_HS_DMA_BASE |                                    \
                   USB_MHDRC_HS_DMA_CONTROL_OFFSET |                          \
                   ((n) << 4))

/* Internal DMA channel n address register ,32 bits */

#define USB_MHDRC_HS_DMA_ADDR_CH(n)                                           \
                  (USB_MHDRC_HS_DMA_BASE |                                    \
                   USB_MHDRC_HS_DMA_ADDRESS_OFFSET |                          \
                   ((n) << 4))

/* Internal DMA channel n count register ,32 bits */

#define USB_MHDRC_HS_DMA_COUNT_CH(n)                                          \
                  (USB_MHDRC_HS_DMA_BASE |                                    \
                   USB_MHDRC_HS_DMA_COUNT_OFFSET |                            \
                   ((n) << 4))

/* Bit's map of the internal DMA control reigster */

#define USB_MHDRC_HS_DMA_CTNL_DMA_ENAB        (0x0001) /* Enable */
#define USB_MHDRC_HS_DMA_CTNL_DIR_MASK        (0x0002) /* Transfer dir */
#define USB_MHDRC_HS_DMA_CTNL_DIR_TX          (0x0002)
#define USB_MHDRC_HS_DMA_CTNL_DIR_RX          (0x0000)
#define USB_MHDRC_HS_DMA_CTNL_MODE_MASK       (0x0004) /* Transfer mode */
#define USB_MHDRC_HS_DMA_CTNL_MODE_0          (0x0000)
#define USB_MHDRC_HS_DMA_CTNL_MODE_1          (0x0004)
#define USB_MHDRC_HS_DMA_CTNL_IE              (0x0008) /* Interrupt ebable */
#define USB_MHDRC_HS_DMA_CTNL_EP_MASK         (0x00F0) /* EP mask */
#define USB_MHDRC_HS_DMA_CTNL_ERR             (0x0100) /* DMA ERROR */
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_MASK      (0x0600)  /* Burst mode mask */
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_UNSPEC    (0x0000)  /* unspecified length */
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_INCR4     (0x0200)  /* INCR4 */
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_INCR8     (0x0400)  /* INCR8 */
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_INCR16    (0x0600)  /* INCR16 */

#define USB_MHDRC_HS_DMA_BURST_INCR_SIZE_64    (0x40)
#define USB_MHDRC_HS_DMA_BURST_INCR_SIZE_32    (0x20)
#define USB_MHDRC_HS_DMA_BURST_INCR_SIZE_16    (0x10)

#define USB_MHDRC_HS_DMA_CTNL_DMA_ENAB_OFFSET (0)
#define USB_MHDRC_HS_DMA_CTNL_DMA_DIR_OFFSET  (1)
#define USB_MHDRC_HS_DMA_CTNL_DMA_MODE_OFFSET (2)
#define USB_MHDRC_HS_DMA_CTNL_IE_MODE_OFFSET  (3)
#define USB_MHDRC_HS_DMA_CTNL_EP_OFFSET       (4)
#define USB_MHDRC_HS_DMA_CTNL_ERR_OFFSET      (8)
#define USB_MHDRC_HS_DMA_CTNL_BRSTM_OFFSET    (9)

/*****************************DMA related registers (END)**********************/

STATUS usbMhdrcPlatformHsDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );
STATUS usbMhdrcPlatformHsDmaUnSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );
void usbMhdrcHsDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
void usbMhdrcHsDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcHsDmah */
