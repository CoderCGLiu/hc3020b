/* usbTgtMscDataStructure.h - Defines for USB Target Mass Storage Class driver */

/*
 * Copyright (c) 2010-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01l,17oct12,s_z  Fix short packet receive issue (WIND00374594) 
01k,10may12,s_z  Remove compiler warning
01j,10apr12,s_z  add mediaChanged into structure usb_tgt_msc_lun to record the 
                 media change event (WIND00328309)
01i,12aug11,m_y  add uTotalLen into structure usb_tgt_msc_dev to record the 
                 total transfer length
01h,11apr11,m_y  add uActLen into structure usb_tgt_msc_dev to record the 
                 current transfer length
01g,25mar11,m_y  remove useless definitions
01f,24mar11,s_z  Changed based on the code review
01e,17mar11,m_y  remove usb_tgt_msc_lunf structure and some useless macros
01d,17mar11,m_y  modify code based on the code review
01c,03mar11,m_y  code clean up 
01b,28dec10,x_f  clean up
01a,26sep10,m_y  written.
*/

#ifndef __INCusbTgtMscDataStructureh
#define __INCusbTgtMscDataStructureh

#ifdef    __cplusplus
extern "C" {
#endif

/* includes */

#include <vxWorks.h>
#include <stdio.h>
#include <errnoLib.h>
#include <logLib.h>
#include <string.h>
#include <blkIo.h>
#include <usb/usb.h>
#include <usb/usbdLib.h>
#include <drv/usb/usbBulkDevLib.h>
#include <usb/usbOsal.h>
#include <drv/manager/device.h>
#include <drv/xbd/bio.h>
#include <lstLib.h>
#include <usb/usbTgt.h>

/* defines */

#define MSC_DATA_BUF_LEN                         (0x10000)
#define USB_TGT_MSC_THREAD_PRIORITY              (100)
#define MAX_MSC_CMD_LEN                          (16)
#define DEVICE_ID_LGTH      8          /* device id length */

#define USE_SCSI_SUBCLASS  /* SCSI Protocol */

/* type defines */

typedef enum usb_tgt_msc_dev_state
    {
    USB_MSC_DEV_INIT = (0x1 << 0),
    USB_MSC_DEV_DISCONNECT = (0x1 << 2),
    USB_MSC_DEV_CONNECT = (0x1 << 1),
    USB_MSC_DEV_CONFIG =  (0x1 << 3),
    USB_MSC_DEV_ADDRESS = (0x1 << 4)
    }USB_TGT_MSC_DEV_STATE;

typedef enum usb_tgt_msc_cmd_state
    {
    USBTGT_MSC_CBW_NORMAL  = (0x1 << 0),
    USBTGT_MSC_CBW_INVALID = (0x1 << 2)
    }USB_TGT_MSC_CMD_STATE;

typedef enum usb_tgt_msc_lun_state
    {
    USB_MSC_LUN_INIT   = 1,
    USB_MSC_LUN_DETACH = 2, /* the device is detached to the lun */    
    USB_MSC_LUN_ATTACH = 3, /* the device is attached to the lun */
    USB_MSC_LUN_DELETE = 4
    }USB_TGT_MSC_LUN_STATE;

typedef enum usb_tgt_msc_buf_state
    {
    USB_TGT_MSC_BUF_EMPTY = 0,
    USB_TGT_MSC_BUF_FULL = 1,
    USB_TGT_MSC_BUF_BUSY = 2
    }USB_TGT_MSC_BUF_STATE;

typedef enum usb_tgt_msc_pipe_state
    {
    USB_TGT_MSC_PIPE_UNSTALL = 0,
    USB_TGT_MSC_PIPE_STALL = 1,
    USB_TGT_MSC_PIPE_IDLE = 2,
    USB_TGT_MSC_PIPE_BUSY = 3
    }USB_TGT_MSC_PIPE_STATE;

typedef enum usb_tgt_msc_erp_state
    {
    USB_TGT_MSC_ERP_IDLE,
    USB_TGT_MSC_ERP_BUSY
    }USB_TGT_MSC_ERP_STATE;

/* Command Block Wrapper */

typedef struct usbMscCbw
    {
    UINT32  dCBWSignature;          /* CBW Signature                  */
    UINT32  dCBWTag;                /* Tag field                      */
    UINT32  dCBWDataTransferLength; /* Size of data (bytes)           */
    UINT8   bmCBWFlags;             /* direction bit                  */
    UINT8   bCBWLUN;                /* Logical Unit Number            */
    UINT8   bCBWCBLength;           /* Length of command block        */
    UINT8   CBWCB [16];             /* buffer for command block       */
    } WRS_PACK_ALIGN(1) USB_MSC_CBW, *pUSB_MSC_CBW; 

/* Command Status Wrapper */

typedef struct usbMscCsw
    {
    UINT32  dCSWSignature;          /* CBW Signature                  */
    UINT32  dCSWTag;                /* Tag field                      */
    UINT32  dCSWDataResidue;        /* Size of residual data (bytes)  */
    UINT8   bCSWStatus;             /* buffer for command block       */
    } WRS_PACK_ALIGN(1) USB_MSC_CSW, *pUSB_MSC_CSW;

typedef struct stdInquiryData           /* STD_INQUIRY_DATA */
    {
    UINT8   devQualType;            /* peripheral device type */
    UINT8   RemovableMedia;         /* removeable media bit */
    UINT8   version;                /* 0x0 The device does not claim */
                                    /* conformance to any standard. */
    UINT8   byte3;                  /* response data format in lower 4 bits */
                                    /* upper 4 bits reserved */
    UINT8   additionalLgth;         /* length of parameters in bytes */
    UINT8   sccs;
    UINT8   byte6;                  /* reserved */
    UINT8   byte7;                  /* reserved */
    UINT8   vendorId[8];            /* vendor indentification */
    UINT8   productId[16];          /* producat indentification */
    UINT8   productRevLevel[4];     /* product revision data */
                                    /* 36 bytes is the minimum required */
    UINT8   vendorSpecific[20];     /* vendor specific info */
    UINT8   byte56;                 /* Reserved */
    UINT8   reserved1;              /* reserved - 1 */
    UINT16  versionDescriptor[8];   /* version descripttion */
    UINT8   reserved2[22];          /* reservered - 2 */
    } WRS_PACK_ALIGN(1) STD_INQUIRY_DATA, *pSTD_INQUIRY_DATA;

typedef struct vpdSupportedPage     /* VPD_SUPPORTED_PAGE */
    {
    UINT8   devQualType;            /* peripheral device type */	
    UINT8   pageCode;               /* page code */
    UINT8   reserved;               /* reserved */
    UINT8   pageLgth;               /* page length */
    UINT8   page_0;
    UINT8   page_80;
    UINT8   page_83;
    }WRS_PACK_ALIGN(1) VPD_SUPPORTED_PAGE, *pVPD_SUPPORTED_PAGE;

typedef struct commandData          /* COMMAND_DATA */
    {
    UINT8   devQualType;            /* peripheral device type */
    UINT8   support;                /* future support */	
    UINT8   reserved[2];            /* reserved */
    UINT8   cdbSize;                /* command block size */
    UINT8   cdbOpCode;              /* operation opcode */
    UINT8   cdbUsageMap;            /* command usage map */
    } WRS_PACK_ALIGN(1) COMMAND_DATA, *pCOMMAND_DATA;

typedef struct vpdDeviceIdDescriptor    /* VPD_DEVICE_ID_DESCRIPTOR */
    {
    UINT8   codeSet;
    UINT8   assocIdType;
    UINT8   reserved;               /* reserved */
    UINT8   idLgth;                 /* device id descriptor length */
    UINT8   devId[DEVICE_ID_LGTH];  /* device id */
    } WRS_PACK_ALIGN(1) VPD_DEVICE_ID_DESCRIPTOR, *pVPD_DEVICE_ID_DESCRIPTOR;

typedef struct vpdDeviceIdPge       /* VPD_DEVICE_ID_PAGE */
    {
    UINT8   devQualType;            /* peripheral device type */
    UINT8   pageCode;               /* page code */
    UINT8   reserved;               /* reserved */
    UINT8   pageLgth;               /* page lenght */
    VPD_DEVICE_ID_DESCRIPTOR devIdDescr;/* device descriptor id */
    } WRS_PACK_ALIGN(1) VPD_DEVICE_ID_PAGE, *pVPD_DEVICE_ID_PAGE;

typedef struct vpdUnitSerialNumPage /* VPD_UNIT_SERIAL_NUM_PAGE */
    {
    UINT8   devQualType;            /* peripheral device type */
    UINT8   pageCode;               /* page code */
    UINT8   reserved;               /* reserved */
    UINT8   pageLgth;               /* page length */
    UINT8   serialNum[24];          /* serial number */
    } WRS_PACK_ALIGN(1) VPD_UNIT_SERIAL_NUM_PAGE, *pVPD_UNIT_SERIAL_NUM_PAGE;

typedef struct modeParameterHeader  /* MODE_PARAMETER_HEADER */
    {
    UINT8   dataLen;                /* mode data length */ 
    UINT8   mediumType;             /* type of media */
    UINT8   deviceParameter;        /* device paramter */
    UINT8   blockDescriptorLgth;    /* block desc. length */
    } WRS_PACK_ALIGN(1) MODE_PARAMETER_HEADER, *pMODE_PARAMETER_HEAD;

typedef struct modeParamterPage		/* MODE_PARAMETER_PAGE */
    {
    UINT8   pageCode;               /* page code */
    UINT8   pageLth;                /* page length */
    UINT8   writeCacheDisable;      /* cache disabled for writes */
    UINT8   logicalBlockSize[2];    /* logical block size */
    UINT8   numberOfLogicalBlocks[5];   /* number of logical blocks */
    }WRS_PACK_ALIGN(1) MODE_PARAMETER_PAGE, *pMODE_PARAMETER_PAGE;

typedef struct modeParametrList     /* MODE_PARAMETER_LIST */
    {
    MODE_PARAMETER_HEADER   header; /* MODE_PARAMETER_HEADER */
    MODE_PARAMETER_PAGE     params; /* MODE_PARAMETER_PAGE */
    } WRS_PACK_ALIGN(1) MODE_PARAMETER_LIST, *pMODE_PARAMETER_LIST;

typedef struct senseData            /* SENSE_DATA */
    {
    UINT8   responseCode;           /* request code */ 
    UINT8   obsolete;               /* zero always */
    UINT8   senseKey;               /* sense key - depends on command */
    UINT8   info[4];                /* info field - command specific */
    UINT8   additionalSenseLgth;    /* additional data to follow */
                                    /* UFI device set this to 10 */
    UINT8   cmdSpecificInfo[4];     /* command specific info */
    UINT8   asc;                    /* additional sense code */
    UINT8   ascq;                   /* additional sense code qualifier */
    UINT8   fieldReplaceableUnitCode;   /* optional field set to zero */
    UINT8   senseKeySpecific[3];    /* reserved for additiona data */
    } WRS_PACK_ALIGN(1) SENSE_DATA, *pSENSE_DATA;

typedef struct capacityData         /* CAPACITY_DATA */
    {
    UINT8   lba_3;                  /* logical block address - byte 3 */
    UINT8   lba_2;                  /* logical block address - byte 2 */
    UINT8   lba_1;                  /* logical block address - byte 1 */ 
    UINT8   lba_0;                  /* logical block address - byte 0 */
    UINT8   blockLen_3;             /* block length - byte 3 */
    UINT8   blockLen_2;             /* block length - byte 2 */
    UINT8   blockLen_1;             /* block length - byte 1 */
    UINT8   blockLen_0;             /* block length - byte 0 */
    } WRS_PACK_ALIGN(1) CAPACITY_DATA, *pCAPACITY_DATA;
    
typedef struct modeParameterHeader10    /* MODE_PARAMETER_HEADER10 for mode
                                           sense10 command */
    {
    UINT8   dataLenMSB;             /* mode data length MSB */
    UINT8   dataLenLSB;             /* mode data length MSB */
    UINT8   mediumType;             /* type of media */
    UINT8   deviceParameter;        /* device paramter */
    UINT16  reservedBytes;          /* reserved */
    UINT16  blockDescriptorLgth;    /* block desc. length */
    } WRS_PACK_ALIGN(1) MODE_PARAMETER_HEADER10, *pMODE_PARAMETER_HEAD10;

typedef struct modeParametrList10   /* MODE_PARAMETER_LIST10 for mode
                                       sense 10 command */
    {
    MODE_PARAMETER_HEADER10 header; /* MODE_PARAMETER_HEADER10 */
    MODE_PARAMETER_PAGE     params; /* MODE_PARAMETER_PAGE */
    } WRS_PACK_ALIGN(1) MODE_PARAMETER_LIST10, *pMODE_PARAMETER_LIST10;

typedef struct usb_tgt_msc_lun USB_TGT_MSC_LUN, *pUSB_TGT_MSC_LUN;

typedef struct usb_tgt_msc_dev
    {
    USB_TARG_CHANNEL      targChannel;  /* Target channel */    
    NODE                  devNode;      /* Node to attach to the global list */
    char                  tcdName[USBTGT_MAX_NAME_SZ]; /* TCD's name of this device */
    int                   tcdUnit;      /* TCD's unit number */
    char                  protocolType[USBTGT_COMMON_STR_LEN];
    USB_TGT_MSC_DEV_STATE devState;     /* Current state of this device */
    UINT32                uCurConfig;   /* Current config index */
    UINT32                uAltSetting;  /* Current altsetting value */
    UINT32                nLuns;        /* LUN count of this device */
    SEM_ID                lunEventId;   /* EventId for lun list */
    LIST                  lunList;      /* LUN list */
    pUSB_TGT_MSC_LUN      pCurrentLun;  /* Current LUN */
    USB_TARG_PIPE         bulkOutPipe;
    USB_TARG_PIPE         bulkInPipe;
    USB_ERP               bulkOutErp;
    USB_ERP               bulkInErp;
    pUSB_MSC_CBW          pCBW;
    pUSB_MSC_CSW          pCSW;
    UINT8 *               pBuf;
    UINT8                 uBufState;
    UINT8                 bulkOutPipeState;
    UINT8                 bulkOutErpState;
    UINT8                 bulkInPipeState;
    UINT8                 bulkInErpState;
    UINT8                 cmd[MAX_MSC_CMD_LEN]; /* Command */
    UINT8                 uCmdStatus;           /* Parse command status */
    UINT32                cmdLen;               /* Command length */
    UINT32                uActLen;              /* Actual read/write length */
    UINT32                uTotalLen;            /* Total transfer length */
    } USB_TGT_MSC_DEV, *pUSB_TGT_MSC_DEV;

struct usb_tgt_msc_lun
    {
    NODE                  lunNode;      /* Node to attach to device list */
    device_t              devXbdHandle; /* XBD partion handler */
    char                  attachDevName[USBTGT_COMMON_STR_LEN];
    BOOL                  bReadOnly;
    BOOL                  bRemovable;
    pSENSE_DATA           pSenseData;    /* Current sense data of this LUN */
    pCAPACITY_DATA        pCapacityData; /* Current capacity data of this LUN */
    USB_TGT_MSC_LUN_STATE lunState;      /* Current state of this LUN */
    UINT32                lunIndex;      /* Index of this LUN */
    unsigned              perBlockSize;  /* Per Block Size */
    sector_t              nBlocks;       /* Number of Blocks */
    pUSB_TGT_MSC_DEV      pUsbTgtMscDev; /* Pointer to the device */
    BOOL                  mediaRemoved;
    BOOL                  mediaReady;
    BOOL                  mediaChanged;
    UINT8                 mediaPrevent;
    UINT8                 pwrConditions;
    MODE_PARAMETER_LIST   deviceParamList;
    MODE_PARAMETER_LIST   deviceParamListMask;
    MODE_PARAMETER_LIST10 deviceParamList10;
    MODE_PARAMETER_LIST10 deviceParamListMask10;
    struct bio            bio; 
    SEMAPHORE             bioDoneSem;
    };

IMPORT LIST g_usbTgtMscDevList;
IMPORT OS_EVENT_ID g_usbTgtMscListEvent;

#define DEV_NODE_TO_USB_TGT_MSC_DEV(pNode)                             \
     ((pNode == NULL)? (NULL):                                         \
     ((USB_TGT_MSC_DEV *) ((char *) (pNode) -                          \
                            OFFSET(USB_TGT_MSC_DEV, devNode))))

#define LUN_NODE_TO_USB_TGT_MSC_LUN(pNode)                             \
         ((pNode == NULL)? (NULL):                                     \
         ((USB_TGT_MSC_LUN *) ((char *) (pNode) -                      \
                               OFFSET(USB_TGT_MSC_LUN, lunNode))))
                               
#ifdef    __cplusplus
}
#endif

#endif /* __INCusbTgtMscDataStructureh */

