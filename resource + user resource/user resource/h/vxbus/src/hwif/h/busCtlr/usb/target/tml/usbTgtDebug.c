/*  usbTgtDebug.c  - USB Target Stack Debug Module */

/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,02Sep14,bbz  clear the warnings of no check of the return value (VXW6-83095)
01b,06may13,s_z  Remove compiler warning (WIND00356717)
01a,18feb12,s_z  Move here from usbTgt.c
*/

/*
DESCRIPTION

This module provides the debug routines for USB target stack. 

INCLUDE FILES: usbMhdrcHsDma.h
*/


#ifdef USBTGT_SHOW_ENABLE

/* The TGT TCD list */

IMPORT LIST gUsbTgtTcdList;

/* The TGT function driver list */

IMPORT LIST gUsbTgtFuncDriverList;

/* Event used for synchronising the access of the TGT list */

IMPORT OS_EVENT_ID gUsbTgtTcdListLock;
IMPORT OS_EVENT_ID gUsbTgtFuncListLock;


/*******************************************************************************
*
* usbTgtStrDescShow - show the information of struct pUSBTGT_STRING
*
* This routine is used to show the information of struct pUSBTGT_STRING, 
* which is useful for debugging.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtStrDescShow 
    (
    pUSBTGT_STRING pString
    )
    {
    if (NULL == pString)
        {
        USB_TGT_ERR ("pString is NULL\n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    printf("strNode               = %p\n", &pString->strNode);
    printf("pFuncDriver           = %p\n", pString->pFuncDriver);    
    printf("uLangId               = %p\n", &pString->uLangId);
    
    printf("pFuncDriver FuncName  = %s\n", ((pString->pFuncDriver != NULL) ? 
                                                    pString->pFuncDriver->pFuncName:
                                                    "NULL" ));

    printf("uOldStrIndex          = 0x%02X\n", pString->uOldStrIndex);
    printf("uNewStrIndex          = 0x%02X\n", pString->uNewStrIndex);
    }


/*******************************************************************************
*
* usbTgtConfigShow - show the information of struct usbtgt_config
*
* This routine is used to show the information of struct usbtgt_config, 
* which is useful for debugging.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtConfigShow 
    (
    pUSBTGT_CONFIG pUsbTgtConfig
    )
    {
    if (NULL == pUsbTgtConfig)
        {
        USB_TGT_ERR ("pUsbTgtCOnfig is NULL\n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    printf("configNode            = %p\n", &pUsbTgtConfig->configNode);
    printf("pTcd                  = %p\n", pUsbTgtConfig->pTcd);    
    printf("ConfigDesc            = %p\n", &pUsbTgtConfig->ConfigDesc);

    printf("  bLength             = 0x%02X\n", pUsbTgtConfig->ConfigDesc.length);
    printf("  bDescriptorType     = 0x%02X\n", pUsbTgtConfig->ConfigDesc.descriptorType);
    printf("  wTotalLength        = 0x%04X\n", TO_LITTLEW(pUsbTgtConfig->ConfigDesc.totalLength));
    printf("  bNumInterfaces      = 0x%02X\n", pUsbTgtConfig->ConfigDesc.numInterfaces);
    printf("  bConfigurationValue = 0x%02X\n", pUsbTgtConfig->ConfigDesc.configurationValue);
    printf("  iConfiguration      = 0x%02X\n", pUsbTgtConfig->ConfigDesc.configurationIndex);
    printf("  bmAttributes        = 0x%02X\n", pUsbTgtConfig->ConfigDesc.attributes);
    printf("  MaxPower            = 0x%02X\n", pUsbTgtConfig->ConfigDesc.maxPower);
    
    printf("DeviceInfo            = %p\n", &pUsbTgtConfig->DeviceInfo);
    printf("ConfigMutex           = %p\n",   pUsbTgtConfig->ConfigMutex);    
    printf("uConfigIndex          = 0x%X\n", pUsbTgtConfig->uConfigIndex);
    printf("uIfNumUsed            = 0x%X\n", pUsbTgtConfig->uIfNumUsed);    
    printf("uEpNumUsed            = 0x%X\n", pUsbTgtConfig->uEpNumUsed);
    }


/*******************************************************************************
*
* usbTgtFuncShow - show the information in sturcture <'USBTGT_FUNC_DRIVER'>
*
* This routine is used to show the information included in sturcture 
* <'USBTGT_FUNC_DRIVER'>, which is useful for debugging.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtFuncShow 
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    UINT16         uIndex = 0;
    pUSB_DESCR_HDR pDescHeader = NULL;
    UINT8 *        pBufTemp = NULL;
    UINT16         uBufLen  = 0;
    
    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR ("pFuncDriver is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    printf("funcDriverNode      = %p\n", &pFuncDriver->funcDriverNode);
    printf("funcMutex           = %p\n", pFuncDriver->funcMutex);
    printf("uFuncMagicID        = 0x%X\n",pFuncDriver->uFuncMagicID);

    
    printf("pFuncSpecific       = %p\n", pFuncDriver->pFuncSpecific);
    printf("ppFsFuncDescTable   = %p\n", pFuncDriver->ppFsFuncDescTable);
    printf("ppHsFuncDescTable   = %p\n", pFuncDriver->ppHsFuncDescTable);
    printf("ppSsFuncDescTable   = %p\n", pFuncDriver->ppSsFuncDescTable);
    
    printf("pFuncCallbackTable  = %p\n", pFuncDriver->pFuncCallbackTable);
    printf("pCallbackParam      = %p\n", pFuncDriver->pCallbackParam);
    
    printf("targChannel         = 0x%X\n", pFuncDriver->targChannel);
    printf("pipeList            = %p\n", &pFuncDriver->pipeList);
    
    printf("pTcd                = %p\n", pFuncDriver->pTcd);
    printf("pFuncName           = %s\n", pFuncDriver->pFuncName);
    printf("pTcdName            = %s\n", pFuncDriver->pTcdName);
    printf("uTcdUnit            = 0x%X\n", pFuncDriver->uTcdUnit);

    
    printf("uConfigToBind       = 0x%X\n", pFuncDriver->uConfigToBind);
    printf("uCurrentConfig      = 0x%X\n", pFuncDriver->uCurrentConfig);
    printf("pConfig             = %p\n", pFuncDriver->pConfig);
    printf("pSsDescBuf            = %p\n", pFuncDriver->pSsDescBuf);
    printf("uSsDescBufLen         = 0x%X\n", pFuncDriver->uSsDescBufLen);
    printf("pHsDescBuf          = %p\n", pFuncDriver->pHsDescBuf);
    printf("uHsDescBufLen       = 0x%X\n", pFuncDriver->uHsDescBufLen);
    printf("pFsDescBuf          = %p\n", pFuncDriver->pFsDescBuf);
    printf("uFsDescBufLen       = 0x%X\n", pFuncDriver->uFsDescBufLen);
    printf("uIADDesc[0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X]\n",
           pFuncDriver->uIADDesc[0],
           pFuncDriver->uIADDesc[1],
           pFuncDriver->uIADDesc[2],
           pFuncDriver->uIADDesc[3],
           pFuncDriver->uIADDesc[4],
           pFuncDriver->uIADDesc[5],
           pFuncDriver->uIADDesc[6],
           pFuncDriver->uIADDesc[7]);

    printf("pSsDescBuf Data:             \n");

    pDescHeader = (pUSB_DESCR_HDR)pFuncDriver->pSsDescBuf;
    uBufLen = 0;
    
    while ((pDescHeader) &&
           (uBufLen < pFuncDriver->uSsDescBufLen) &&
           (pDescHeader->length > 2))
        {
        pBufTemp = (UINT8 *)pDescHeader;
        
        for(uIndex = 0; uIndex < pDescHeader->length; uIndex ++)
            {
            printf(" 0x%02X", pBufTemp[uIndex]);
            }
        printf ("\n");

        uBufLen = (UINT16) (uBufLen + pDescHeader->length);
        pDescHeader = (pUSB_DESCR_HDR)(pBufTemp + uIndex);
        }

    printf("pHsDescBuf Data:           \n");

    pDescHeader = (pUSB_DESCR_HDR)pFuncDriver->pHsDescBuf;
    uBufLen = 0;
    
    while ((pDescHeader) &&
           (uBufLen < pFuncDriver->uHsDescBufLen) &&
           (pDescHeader->length > 0))
        {
        pBufTemp = (UINT8 *)pDescHeader;
        
        for(uIndex = 0; uIndex < pDescHeader->length; uIndex ++)
            {
            printf(" 0x%02X", pBufTemp[uIndex]);
            }
        printf ("\n");

        uBufLen = (UINT16) (uBufLen + pDescHeader->length);
        pDescHeader = (pUSB_DESCR_HDR)(pBufTemp + uIndex);
        }
    
    printf("pFsDescBuf Data:           \n");
    
    pDescHeader = (pUSB_DESCR_HDR)pFuncDriver->pFsDescBuf;
    uBufLen = 0;
    
    while ((pDescHeader) &&
           (uBufLen < pFuncDriver->uFsDescBufLen) &&
           (pDescHeader->length > 0))
        {
        pBufTemp = (UINT8 *)pDescHeader;
        
        for(uIndex = 0; uIndex < pDescHeader->length; uIndex ++)
            {
            printf(" 0x%02X", pBufTemp[uIndex]);
            }
        printf ("\n");

        uBufLen = (UINT16) (uBufLen + pDescHeader->length);
        pDescHeader = (pUSB_DESCR_HDR)(pBufTemp + uIndex);
        }
    printf("uIfNumUsed          = 0x%X\n", pFuncDriver->uIfNumUsed);
    printf("uIfNumMin           = 0x%X\n", pFuncDriver->uIfNumMin);
    printf("uIfNumMax           = 0x%X\n", pFuncDriver->uIfNumMax);

    }

/*******************************************************************************
*
* usbTgtTcdShow - show the information of sturcture <'USBTGT_TCD'>
*
* This routine is used to show the information of the structure <'USBTGT_TCD'>,
* which is useful for debugging.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtTcdShow 
    (
    pUSBTGT_TCD pTcd
    )
    {
    NODE * pNode = NULL;
    int count;
    int index;
    
    if (NULL == pTcd)
        {
        USB_TGT_ERR ("pTcd is NULL\n",
                     1, 2, 3, 4, 5, 6);

        return;
        }
    
    printf("usbTgtTcdNode     = %p\n", &pTcd->usbTgtTcdNode);
    printf("tcdMutex          = %p\n", pTcd->tcdMutex);

    
    printf("funcList          = %p\n", &pTcd->funcList);
    printf("configList        = %p\n", &pTcd->configList);

    count = lstCount(&pTcd->configList);

    printf ("Show the TCD configuration  members \n");

    printf ("pTcd->configList has 0x%d member\n", count);

    for (index = 1; index <= count; index ++)
        {
        printf ("\nShow the TCD configuration member #%d \n", index);
        
        pNode = lstNth (&pTcd->configList, index);

        usbTgtConfigShow ((pUSBTGT_CONFIG)pNode);
        }

    count = lstCount(&pTcd->funcList);

    printf ("Show the TCD function dirver  members \n");

    printf ("pTcd->funcList has 0x%d member\n", count);

    printf("strDescList       = %p\n", &pTcd->strDescList);

    count = lstCount(&pTcd->strDescList);
    for (index = 1; index <= count; index ++)
        {
        printf ("\nShow the TCD String member #%d \n", index);
        
        pNode = lstNth (&pTcd->strDescList, index);

        usbTgtStrDescShow ((pUSBTGT_STRING)pNode); 
        }
    
    printf("controlPipe       = %p\n", pTcd->controlPipe);
    printf("controlErp        = %p\n", &pTcd->controlErp);
    
    printf("SetupPkt          = 0x%02X 0x%02X 0x%04X 0x%04X 0x%04X \n",
                               pTcd->SetupPkt.requestType,
                               pTcd->SetupPkt.request,
                               pTcd->SetupPkt.value,
                               pTcd->SetupPkt.index,
                               pTcd->SetupPkt.length);
    printf("dataBfr           = 0x%08X 0x%08X ... \n", 
                               *(UINT32 *)&pTcd->dataBfr[0],
                               *(UINT32 *)&pTcd->dataBfr[4]);
    
    printf("uTCDMagicID       = 0x%X\n", pTcd->uTCDMagicID);
    printf("uVendorID         = 0x%X\n", pTcd->uVendorID);
    printf("uProductID        = 0x%X\n", pTcd->uProductID);

    
    printf("pMfgString        = %s\n", pTcd->pMfgString);
    printf("pProdString       = %s\n", pTcd->pProdString);
    printf("pSerialString     = %s\n", pTcd->pSerialString);
    printf("pTcdName          = %s\n", pTcd->pTcdName);

    
    printf("uTcdUnit          = 0x%X\n", pTcd->uTcdUnit);
    printf("pTcdFuncs         = %p\n", pTcd->pTcdFuncs);
    printf("pTcdSpecific      = %p\n", pTcd->pTcdSpecific);

    
    printf("DeviceInfo        = %p\n", &pTcd->DeviceInfo);

   
    printf("  bLength         = 0x%02X\n", pTcd->DeviceDesc.length);
    printf("  bDescriptorType = 0x%02X\n", pTcd->DeviceDesc.descriptorType);
    printf("  bcdUSB          = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.bcdUsb));
    printf("  bDeviceClass    = 0x%02X\n", pTcd->DeviceDesc.deviceClass);
    printf("  bDeviceSubClass = 0x%02X\n", pTcd->DeviceDesc.deviceSubClass);
    printf("  bDeviceProtocol = 0x%02X\n", pTcd->DeviceDesc.deviceProtocol);
    printf("  bMaxPacketSize0 = 0x%02X\n", pTcd->DeviceDesc.maxPacketSize0);
    printf("  idVendor        = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.vendor));
    printf("  idProduct       = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.product));
    printf("  bcdDevice       = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.bcdDevice));
    printf("  iManufacturer   = 0x%02X\n", pTcd->DeviceDesc.manufacturerIndex);
    printf("  iProduct        = 0x%02X\n", pTcd->DeviceDesc.productIndex);
    printf("  iSerialNumber   = 0x%02X\n", pTcd->DeviceDesc.serialNumberIndex);
    printf("  bNumConfigs     = 0x%02X\n", pTcd->DeviceDesc.numConfigurations);


    printf("devQualifierDesc  = %p\n", &pTcd->devQualifierDesc);
    printf ("pUsbTgtLangDescr = 0x%X 0x%X 0x%X\n", pTcd->pUsbTgtLangDescr->length,
                pTcd->pUsbTgtLangDescr->descriptorType,
                pTcd->pUsbTgtLangDescr->langId[0]);
   
    printf("uSpeed            = 0x%X\n", pTcd->uSpeed);
    printf("uDeviceAddress    = 0x%X\n", pTcd->uDeviceAddress);  
    printf("uAddrToSet        = 0x%X\n", pTcd->uAddrToSet);
    printf("uCurrentConfig    = 0x%X\n", pTcd->uCurrentConfig);
    printf("controlErpUsed    = 0x%lX\n", pTcd->controlErpUsed);
    }

/*******************************************************************************
*
* usbTgtShow - show the information of the target stack
*
* This routine is used to show the information of the target stack. It will
* list all the TCDs and Function drivers added to the global list 
* <'gUsbTgtTcdList'> and <'gUsbTgtFuncDriverList'>.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtShow 
    (
    void
    )
    {
    NODE *              pNode = NULL;
    int                 count;
    int                 index;
    
    OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

    /* Init the global TCD list */

    count = lstCount(&gUsbTgtTcdList);

    printf ("Show the TCD list members \n");

    printf ("gUsbTgtTcdList has 0x%d member\n", count);

    for (index = 1; index <= count; index ++)
        {
        printf ("\nShow the TCD member #%d \n",index);
        
        pNode = lstNth (&gUsbTgtTcdList, index);

        usbTgtTcdShow ((pUSBTGT_TCD)pNode);
        }
    
    (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);
    
    printf ("\nShow the Function Driver list members \n");

    printf ("gUsbTgtFuncDriverList has 0x%d member\n", count);

    OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);

    /* Init the global function driver list */

    count =  lstCount(&gUsbTgtFuncDriverList);

    for (index = 1; index <= count; index ++)
        {
        printf ("\nShow the Function Driver member #%d \n", index);

        pNode = lstNth (&gUsbTgtFuncDriverList, index);

        usbTgtFuncShow ((pUSBTGT_FUNC_DRIVER)pNode);
        }

    (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);
    }

/*******************************************************************************
*
* usbTgtTcdDevDescShow - show the TCD's device descriptor
*
* This routine is used to show the TCD's device descriptor.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtTcdDevDescShow 
    (
    pUSBTGT_TCD pTcd
    )
    {
    if (NULL == pTcd)
        {
        printf ("pTCD is NULL\n");
        return;
        }
    
    /* Show the device descriptor */
    
    printf("Device Descriptor:\n");
    printf(" bLength         = 0x%02X\n", pTcd->DeviceDesc.length);
    printf(" bDescriptorType = 0x%02X\n", pTcd->DeviceDesc.descriptorType);
    printf(" bcdUSB          = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.bcdUsb));
    printf(" bDeviceClass    = 0x%02X\n", pTcd->DeviceDesc.deviceClass);
    printf(" bDeviceSubClass = 0x%02X\n", pTcd->DeviceDesc.deviceSubClass);
    printf(" bDeviceProtocol = 0x%02X\n", pTcd->DeviceDesc.deviceProtocol);
    printf(" bMaxPacketSize0 = 0x%02X\n", pTcd->DeviceDesc.maxPacketSize0);
    printf(" idVendor        = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.vendor));
    printf(" idProduct       = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.product));
    printf(" bcdDevice       = 0x%04X\n", FROM_LITTLEW(pTcd->DeviceDesc.bcdDevice));
    printf(" iManufacturer   = 0x%02X\n", pTcd->DeviceDesc.manufacturerIndex);
    printf(" iProduct        = 0x%02X\n", pTcd->DeviceDesc.productIndex);
    printf(" iSerialNumber   = 0x%02X\n", pTcd->DeviceDesc.serialNumberIndex);
    printf(" bNumConfigs     = 0x%02X\n", pTcd->DeviceDesc.numConfigurations);

    return;
    }

/*******************************************************************************
*
* usbTgtTcdConfigDescShow - show the configuration descriptor data
*
* This routine is used to show configuration descriptor data stored in 
* the struct usbtgt_config.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtTcdConfigDescShow 
    (
    pUSBTGT_CONFIG pConfig
    )
    {
    if (NULL == pConfig)
        {
        printf ("pConfig is NULL\n");
        return;
        }
    
    /* Show the device descriptor */
    
    printf("Configure Descriptor:\n");
    printf("  length             = 0x%02X\n", pConfig->ConfigDesc.length);
    printf("  descriptorType     = 0x%02X\n", pConfig->ConfigDesc.descriptorType);
    printf("  totalLength        = 0x%04X\n", FROM_LITTLEW(pConfig->ConfigDesc.totalLength));
    printf("  numInterfaces      = 0x%02X\n", pConfig->ConfigDesc.numInterfaces);
    printf("  configurationValue = 0x%02X\n", pConfig->ConfigDesc.configurationValue);
    printf("  configurationIndex = 0x%02X\n", pConfig->ConfigDesc.configurationIndex);
    printf("  maxPower           = 0x%02X\n", pConfig->ConfigDesc.maxPower);

    return;
    }

/*******************************************************************************
*
* usbTgtFuncDescShow - show all the descriptors supplied by function driver
*
* This routine is used to show all the descriptors supplied by function driver.
* This routine will search and show all the data stored in the <'pDescBuf'> or 
* <'pHsDescBuf'> according to the <'uSpeed'>.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtFuncDescShow 
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver,
    UINT8               uSpeed
    )
    {
    pUSB_DESCR_HDR pDescHeader = NULL;
    pUSB_INTERFACE_DESCR pIfDesc = NULL;
    pUSB_ENDPOINT_DESCR  pEpDesc = NULL;
    UINT8 *        pBufTemp = NULL;
    UINT16         uDescLen  = 0;
    UINT16         uBufLen  = 0;

    if (NULL == pFuncDriver)
        {
        printf ("pFuncDriver is NULL\n");
        return;
        }

    switch (uSpeed)
        {
        case USB_SPEED_HIGH:
            pBufTemp = pFuncDriver->pHsDescBuf;
            uDescLen = pFuncDriver->uHsDescBufLen;
            break;
        case USB_SPEED_FULL:
            /* Fall through */
        case USB_SPEED_LOW:
            pBufTemp = pFuncDriver->pFsDescBuf;
            uDescLen = pFuncDriver->uFsDescBufLen;
            break;
        case USB_SPEED_SUPER:
            /* Fall through */
        default:
            pBufTemp = pFuncDriver->pSsDescBuf;
            uDescLen = pFuncDriver->uSsDescBufLen;
            break;
        }
    
    if (NULL == pBufTemp)
        {
        return;
        }

    pDescHeader = (pUSB_DESCR_HDR)pBufTemp;
    uBufLen = 0;
    
    while ((pDescHeader) &&
           (uBufLen < uDescLen) &&
           (pDescHeader->length > 2))
        {
        pBufTemp = (UINT8 *)pDescHeader;

        switch (pDescHeader->descriptorType)
            {
            case USB_DESCR_DEVICE:
                
                /* Ignore it */
                
                break;
            case USB_DESCR_CONFIGURATION:
                
                /* Ignore it */
                                
                break;
            case USB_DESCR_INTERFACE:

                pIfDesc = (pUSB_INTERFACE_DESCR)pDescHeader;
                
                printf("Interface Descriptor:\n");
                printf("   length            = 0x%02X\n", pIfDesc->length);
                printf("   descriptorType    = 0x%02X\n", pIfDesc->descriptorType);
                printf("   interfaceNumber   = 0x%02X\n", pIfDesc->interfaceNumber);
                printf("   alternateSetting  = 0x%02X\n", pIfDesc->alternateSetting);
                printf("   numEndpoints      = 0x%02X\n", pIfDesc->numEndpoints);
                printf("   interfaceClass    = 0x%02X\n", pIfDesc->interfaceClass);
                printf("   interfaceSubClass = 0x%02X\n", pIfDesc->interfaceSubClass);
                printf("   interfaceProtocol = 0x%02X\n", pIfDesc->interfaceProtocol);
                printf("   interfaceIndex    = 0x%02X\n", pIfDesc->interfaceIndex);

                break;
            case USB_DESCR_ENDPOINT:

                pEpDesc = (pUSB_ENDPOINT_DESCR)pDescHeader;
                
                printf("EndPoint Descriptor:\n");

                printf("    length          = 0x%02X\n", pEpDesc->length);
                printf("    descriptorType  = 0x%02X\n", pEpDesc->descriptorType);
                printf("    endpointAddress = 0x%02X\n", pEpDesc->endpointAddress);
                printf("    attributes      = 0x%02X\n", pEpDesc->attributes);
                printf("    maxPacketSize   = 0x%04X\n", FROM_LITTLEW(pEpDesc->maxPacketSize));
                printf("    interval        = 0x%02X\n", pEpDesc->interval);
                
                break;
            case USB_DESCR_HID:
                break;
            case USB_DESCR_REPORT:
                break;
            case USB_DESCR_PHYSICAL:
                break;
            default:
                break;
                
            }

        uBufLen = (UINT16)(uBufLen + pDescHeader->length);
        pDescHeader = (pUSB_DESCR_HDR)(pBufTemp + pDescHeader->length);
        }    

    return;
    }


/*******************************************************************************
*
* usbTgtDevShow - initialize the usb target stack
*
* This routine initializes the usb target stack. It will initialize one TCD list
* and one function driver list which used to store the added TCD and function
* driver which installed in the system.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtDevShow 
    (
    void
    )
    {
    NODE *      pNode = NULL;
    pUSBTGT_TCD pTcd  = NULL;
    pUSBTGT_CONFIG pConfig = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    int         tcdCount;
    int         tcdIndex;
    int         configIndex;
    int         configCount;
    int         funcCount;
    int         funcIndex;

    OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

    /* Init the global TCD list */

    tcdCount = lstCount(&gUsbTgtTcdList);

    printf ("Show the TCD list members \n");

    printf ("gUsbTgtTcdList has 0x%d member\n", tcdCount);

    for (tcdIndex = 1; tcdIndex <= tcdCount; tcdIndex ++)
        {
        printf ("\nShow the TCD member #%d \n", tcdIndex);
        
        pNode = lstNth (&gUsbTgtTcdList, tcdIndex);

        pTcd = (pUSBTGT_TCD)pNode;

        if (NULL != pTcd)
            {
            usbTgtTcdDevDescShow(pTcd);
            
            configCount = lstCount(&pTcd->configList);

            for (configIndex = 1; configIndex <= configCount; configIndex ++)
                {
                pNode = lstNth (&pTcd->configList, configIndex);
                pConfig = (pUSBTGT_CONFIG)pNode;
                if (NULL != pConfig)
                    {
                    usbTgtTcdConfigDescShow(pConfig);
                    
                    funcCount = lstCount(&pTcd->funcList);
                    for (funcIndex = 1; funcIndex <= funcCount; funcIndex ++)
                        {
                        pNode = lstNth (&pTcd->funcList, funcIndex);
                        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
                        if (NULL != pFuncDriver)
                            {
                            usbTgtFuncDescShow(pFuncDriver, pTcd->uSpeed);
                            }
                        }
                    }
                }
            }
        
        }
    
    (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);
   
    }

#endif /* USBTGT_SHOW_ENABLE */

