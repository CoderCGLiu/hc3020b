/*  usbMhdrcTcdDebug.c  - USB Mentor Graphics TCD Debug routines */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,15sep11,m_y  using one structure to describe HSDMA and CPPIDMA 
01f,13dec11,m_y  Modify according to code check result (WIND00319317)
01e,12apr11,m_y  add more information show
01d,30mar11,s_z  Changed based on the redefinition of struct usbtgt_device_info
01c,28mar11,w_x  Correct uNumEps usage (WIND00262862)
01b,23mar11,m_y  code clean up based on the review result
01a,20oct10,m_y  write
*/

/*
DESCRIPTION

This file provides the debug routines for USB Mentor Graphics Target Controller 
Driver.

INCLUDE FILES: usbMhdrcHsDma.h
*/

#include "usbMhdrcHsDma.h"

/*******************************************************************************
*
* usbMhdrcTcdRegDump - dump the register value 
*
* This routine dumps the register value. 
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdRegDump
    (
    pUSB_MHDRC_TCD_DATA pTCDData
    )
    {
    pUSB_MUSBMHDRC pMHDRC = NULL;
    int            i;

    pMHDRC = pTCDData->pMHDRC;

    printf("FADDR     = 0x%X\n", USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_FADD));
    printf("POWER     = 0x%X\n", USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER));
    printf("INTRTX    = 0x%X\n", USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_INTRTX));
    printf("INTRRX    = 0x%X\n", USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_INTRRX));
    printf("INTRTXE   = 0x%X\n", USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_INTRTXE));
    printf("INTRRXE   = 0x%X\n", USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_INTRRXE));
    printf("INTRUSB   = 0x%X\n", USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_INTRUSB));
    printf("INTRUSBE  = 0x%X\n", USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_INTRUSBE));
    printf("FRAME     = 0x%X\n", USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_FRAME));
    printf("TESTMODE  = 0x%X\n", USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_TESTMODE));
    
    printf("TXCSR[%d]  = 0x%X\n", 1, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(1)));

    for (i = 0; i < pMHDRC->uNumEps; i++)
        {
        USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_INDEX, i);
        printf("TXFIFOSZ[%d]    = 0x%X\n", 
               i, USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_TXFIFOSZ));
        printf("RXFIFOSZ[%d]    = 0x%X\n", 
               i, USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_RXFIFOSZ));
        printf("TXFIFOADDR[%d]    = 0x%X\n", 
               i, USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_TXFIFOADDR));
        printf("RXFIFOADDR[%d]    = 0x%X\n", 
               i, USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_RXFIFOADDR));
        }
    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_INDEX, 0);

    for (i = 0; i < pMHDRC->uNumEps; i++)
        {
        printf("TXMAXP[%d]   = 0x%X \n", i, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXMAXP_EP(i)));
        printf("TXCSR[%d]    = 0x%X \n", i, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(i)));
        printf("RXMAXP[%d]   = 0x%X \n", i, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXMAXP_EP(i)));
        printf("RXCSR[%d]    = 0x%X \n", i, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(i)));
        }
    
    for (i = 0; i <= pMHDRC->uNumDmaChannels; i++)
        {
        printf("ADDR[%d]     = 0x%X \n", i, USB_MHDRC_REG_READ32(pMHDRC, USB_MHDRC_HS_DMA_ADDR_CH(i)));
        printf("COUNT[%d]    = 0x%X \n", i, USB_MHDRC_REG_READ32(pMHDRC, USB_MHDRC_HS_DMA_COUNT_CH(i)));
        printf("CNTL[%d]     = 0x%X \n", i, USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_HS_DMA_CTNL_CH(i)));
        }
    
    }

/*******************************************************************************
*
* usbMhdrcTcdPipeShow - show the usb_mhdrc_tcd_pipe structure's information
*
* This routine shows the usb_mhdrc_tcd_pipe structure's information.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdPipeShow
    (
    pUSB_MHDRC_TCD_PIPE pTCDPipe
    )
    {
    if (pTCDPipe != NULL)
        {
        printf("Information of pTCDPipe %p as below:\n", pTCDPipe);
        
        printf("ustatus 0x%X, uDevAddress %d, uDmaChannel %d, "
               "requestcount %d, freeRequestCount %d\n", 
               pTCDPipe->uStatus, 
               pTCDPipe->uDevAddress,
               pTCDPipe->uDmaChannel, 
               lstCount(&pTCDPipe->requestList),
               lstCount(&pTCDPipe->freeRequestList));
        
        printf("uEpType %d, uEpAddress %d, uEpDir %d, uMaxPacketSize %d\n",
               pTCDPipe->uEpType,
               pTCDPipe->uEpAddress,
               pTCDPipe->uEpDir,
               pTCDPipe->uMaxPacketSize);
        }
    
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdRequestShow - show the usb_mhdrc_tcd_request structure's information
*
* This routine shows the usb_mhdrc_tcd_request structure's information.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdRequestShow
    (
    pUSB_MHDRC_TCD_REQUEST pRequest
    )
    {
    if (pRequest != NULL)
        {
        printf("Information of pRequest %p as below:\n", pRequest);

        printf("uActLength %d, uXferSize %d, pCurrentBuffer %p\n",
               pRequest->uActLength,
               pRequest->uXferSize,
               pRequest->pCurrentBuffer);
        
        printf("pErp %p pTCDPipe %p\n",
               pRequest->pErp, 
               pRequest->pTCDPipe);
        }
    }

/*******************************************************************************
*
* usbMhdrcTcdDataShow - show the usb_mhdrc_tcd_data structure's information
*
* This routine shows the usb_mhdrc_tcd_data structure's information
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdDataShow
    (
    pUSB_MHDRC_TCD_DATA pTCDData
    )
    {
    int                 i;
    NODE *              pNode = NULL;
    
    pUSBTGT_TCD         pTCD = NULL;
    pUSB_MUSBMHDRC      pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE pTCDPipe = NULL;
    pUSBTGT_DEVICE_INFO pDeviceInfo = NULL;
    pUSB_MHDRC_DMA_DATA pDMAData = NULL;
    

    if (pTCDData == NULL)
        {
        printf("usbMhdrcTcdDataShow: Invalid pTCDData\n");
        return;
        }

    pMHDRC = pTCDData->pMHDRC;
    pTCD  = pTCDData->pTCD;
    pDMAData = &(pMHDRC->dmaData);

    printf("ep0Stag          = %d\n",   pTCDData->ep0Stage);
    printf("uIntRxMask       = 0x%X\n", pTCDData->uIntRxMask);
    printf("uIntTxMask       = 0x%X\n", pTCDData->uIntTxMask);
    printf("uIntUsbMask      = 0x%X\n", pTCDData->uIntUsbMask);
    printf("uTestMode        = 0x%X\n", pTCDData->uTestMode);
    printf("isDeviceSuspend  = 0x%X\n", pTCDData->isDeviceSuspend);
    printf("isRemoteWakeup   = 0x%X\n", pTCDData->isRemoteWakeup);
    printf("isSelfPower      = 0x%X\n", pTCDData->isSelfPower);
    printf("isSoftConnected  = 0x%X\n", pTCDData->isSoftConnected);
    printf("uFreeDmaChannelBitMap  = 0x%X\n", pDMAData->uFreeDmaChannelBitMap);

    printf("\n\npTCD         = %p\n", pTCD);
    if (pTCD != NULL)
        {
        printf("pTCD->bWakeupEnable = 0x%X\n", pTCD->bWakeupEnable);
        printf("pTCD->pTcdName      = %s\n", pTCD->pTcdName);
        printf("pTCD->uTcdUnit      = 0x%X\n", pTCD->uTcdUnit);
        printf("pTCD->uTCDMagicID   = 0x%X\n", pTCD->uTCDMagicID);
        printf("pTCD->uDeviceAddress= 0x%X\n", pTCD->uDeviceAddress);
        }

    printf("\n\npMHDRC       = %p\n", pMHDRC);
    if (pMHDRC != NULL)
        {
        printf("pMHDRC->uNumEps        = 0x%X\n", pMHDRC->uNumEps);
        printf("pMHDRC->uRxIrqStatus   = 0x%X\n", pMHDRC->uRxIrqStatus);
        printf("pMHDRC->uTxIrqStatus   = 0x%X\n", pMHDRC->uTxIrqStatus);
        printf("pMHDRC->uUsbIrqStatus  = 0x%X\n", pMHDRC->uUsbIrqStatus);

        printf("pMHDRC->uDmaType       = 0x%X\n", pMHDRC->uDmaType);
        printf("pMHDRC->uNumDmaChannels= 0x%X\n", pMHDRC->uNumDmaChannels);
        
        for(i = 0; i < pMHDRC->uNumEps; i++)
            {
            printf("pMHDRC->uRxFifoSize[%d]    = 0x%X\n", i, pMHDRC->uRxFifoSize[i]);
            printf("pMHDRC->uTxFifoSize[%d]    = 0x%X\n", i, pMHDRC->uTxFifoSize[i]);
            }
        }

    if ((pMHDRC != NULL) && (pTCD != NULL))
        {
        pDeviceInfo = &(pTCD->DeviceInfo);
        for(i = 0; i < pMHDRC->uNumEps; i++)
            {
            printf("txEpCaps[%d]\n"
                   "             bUsable         = 0x%X\n"
                   "             uMaxPktSize     = 0x%X\n"
                   "             uXferTypeBitMap = 0x%X\n",
                   i, 
                   pDeviceInfo->txEpCaps[i].bUsable, 
                   pDeviceInfo->txEpCaps[i].uMaxPktSize, 
                   pDeviceInfo->txEpCaps[i].uXferTypeBitMap);
            
            printf("rxEpCaps[%d]\n"
                   "             bUsable         = 0x%X\n"
                   "             uMaxPktSize     = 0x%X\n"
                   "             uXferTypeBitMap = 0x%X\n",
                   i, 
                   pDeviceInfo->rxEpCaps[i].bUsable, 
                   pDeviceInfo->rxEpCaps[i].uMaxPktSize, 
                   pDeviceInfo->rxEpCaps[i].uXferTypeBitMap);
            
            }
        }
    
    pNode = lstFirst(&(pTCDData->pipeList));
    while(pNode != NULL)
        {
        pTCDPipe = PIPE_NODE_TO_USB_MHDRC_TCD_PIPE(pNode);
        usbMhdrcTcdPipeShow(pTCDPipe);
        pNode = lstNext(pNode);
        }
    }


