/* usbTgtMscLib.h - Defines for USB Target Mass Storage Class library module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01b,17mar11,m_y  modify code based on the code review
01a,28dec10,x_f  written
*/

#ifndef __INCusbTgtMscLibh
#define __INCusbTgtMscLibh

#ifdef    __cplusplus
extern "C" {
#endif

#include <usbTgtMscDataStructure.h>

/* defines */

/* RBC commands */

#define RBC_CMD_FORMAT              0x04    /* format command */
#define RBC_CMD_READ10              0x28    /* read (10) command */
#define RBC_CMD_READ12              0xA8    /* read (12) command */
#define RBC_CMD_READCAPACITY        0x25    /* read media capacity command*/
#define RBC_CMD_STARTSTOPUNIT       0x1B    /* start/stop file */
#define RBC_CMD_SYNCCACHE           0x35    /* sync. cache command */
#define RBC_CMD_VERIFY10            0x2F    /* verify (10) command */
#define RBC_CMD_WRITE10             0x2A    /* write (10) command */
#define RBC_CMD_WRITE12             0xAA    /* write (12) command */

/* SPC-2 commands */

#define RBC_CMD_INQUIRY             0x12    /* inquiry command */
#define RBC_CMD_MODESELECT6         0x15    /* mode select command */
#define RBC_CMD_MODESENSE6          0x1A    /* mode sense command */
#define RBC_CMD_PERSISTANTRESERVIN  0x5E    /* reserve IN */
#define RBC_CMD_PERSISTANTRESERVOUT 0x5F    /* reserve OUT */
#define RBC_CMD_PRVENTALLOWMEDIUMREMOVAL 0x1E   /* prevent/ allow medium */
                                                /* removal */
#define RBC_CMD_RELEASE6            0x17    /* release command */
#define RBC_CMD_REQUESTSENSE        0x03    /* request sense command */
#define RBC_CMD_RESERVE6            0x16    /* reserve command */
#define RBC_CMD_TESTUNITREADY       0x00    /* test unit ready command */
#define RBC_CMD_WRITEBUFFER         0x3B    /* write buffer */
#define RBC_CMD_MODESENSE10         0x5A    /* mode sense 10 command */
#define RBC_CMD_MODESELECT10        0x55    /* mode select 10 command */

/* vendor specific commands */

#define RBC_CMD_VENDORSPECIFIC_23   0x23    /* vendor specific command */
#define RBC_CMD_SIZE6               0x06    /* size 6 command */
#define RBC_CMD_SIZE10              0x0A    /* size 10 command */
#define RBC_CMD_SIZE12              0x0C    /* size 12 command */

#ifdef USE_SCSI_SUBCLASS
#   undef  USE_RBC_SUBCLASS
#else
#   define USE_RBC_SUBCLASS     /* RBC Protocol */    
#endif

/* USB Mass Storage Class */

#define USB_MSC_CLASS_MASS_STORAGE 0x08 
#define USB_MSC_SUBCLASS_RBC       0x01  /* Reduced Block Commands (RBC) */
#define USB_MSC_SUBCLASS_SCSI      0x06  /* SCSI transparent command set */
                                         /* (Use the SCSI INQUIRY        */
                                         /* command to determine the     */
                                         /* peripheral device type for   */
                                         /* most devices.)               */
#define USB2_MSC_PROTOCOL_BOT      0x50  /* Bulk Only (Recommended       */
                                         /* for most devices)            */

/* Class specific Mass storage reset request */

#define USB_MSC_RESET                           0xFF 

/* Class specific Get Max LUN request */

#define USB_MSC_GET_MAX_LUN                     0xFE 

/* Command Block Wrapper Signature 'USBC' */

#define USB_MSC_CBW_SIGNATURE                   0x43425355
#define USB_MSC_CBW_MAX_CDB_LEN                 16 

/* Command Status Wrapper Signature 'USBS' */

#define USB_MSC_CSW_SIGNATURE                   0x53425355

/* CBW length */

#define USB_MSC_CBW_LENGTH                      31

/* function declaration */

void usbTgtMscBulkOutErpCallbackCBW  (pVOID erp);
void usbTgtMscBulkInErpCallbackCSW   (pVOID erp);
void usbTgtMscBulkInErpCallbackData  (pVOID erp);
void usbTgtMscBulkOutErpCallbackData (pVOID erp);

#ifdef    __cplusplus
}
#endif

#endif /* __INCusbTgtMscLibh */
