/* usbOhciIsr.c - USB OHCI Driver Interrupt Handler */

/*
 * Copyright (c) 2004-2009, 2005-2011, 2013, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2004-2009, 2005-2011, 2013, 2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01s,16jul14,wyy  Unify APIs of USB message to support error report (VXW6-16596)
01r,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of 
                 vxBus Device, and HC count (such as g_EHCDControllerCount or 
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01q,07jan11,ghs  Clean up compile warnings (WIND00247082)
01p,02jul10,m_y  Modify for coding convention
01o,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01n,21mar10,w_x  Avoid taking up 100% CPU time by USB task (WIND00201956)
01m,08mar10,j_x  Changed for USB debug (WIND00184542)
01l,13jan10,ghs  vxWorks 6.9 LP64 adapting
01k,17feb09,s_z  Invalidate the cache of pHCCA->uHccaDoneHead correctly
                 (WIND00155180)
01j,21jul08,w_x  Fix typo in usbOhciProcessInterrupts
01i,21mar07,jrp  vxBus runaway interrupt fix
01h,08oct06,ami  Changes for USB-vxBus changes
01g,12apr05,pdg  Fix for mouseTest application not returning
01f,12apr05,ami  MS7751SE macro changed to SH7750
01e,05apr05,pdg  Fix for devices not detected on ms7751se
01d,28mar05,pdg  non-PCI changes
01c,02mar05,ami  SPR #106373 (OHCI Max Host Controller Issue)
01b,27jun03,nld  Changing the code to WRS standards
01a,17mar02,ssh  Initial Version
*/

/*
DESCRIPTION
This file defines the interrupt handler for the USB OHCI Driver.

INCLUDE FILES: usbOhci.h, usbOhciRegisterInfo.h
*/

/*
 INTERNAL
 ******************************************************************************
 * Filename         : OHCI_ISR.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This file defines the interrupt handler for the OHCI
 *                    driver.
 *
 *
 ******************************************************************************/

/******************************** INCLUDE FILES *******************************/

#include <usb/usbOsal.h>
#include <usbOhci.h>
#include <usbOhciRegisterInfo.h>
#include <usbOhciUtil.h>
#include <usb/usbHst.h>

/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/

#ifndef USB_OHCI_POLLING_MODE
LOCAL VOID usbOhciIsr(pUSB_OHCD_DATA   pHCDData);
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */

LOCAL VOID usbOhciPollingIsr(UINT8 uHostControllerIndex);

/* Function to process the OHCI interrupts */

LOCAL VOID usbOhciProcessInterrupts(UINT8 uHostControllerIndex);

/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/

#ifndef USB_OHCI_POLLING_MODE
/***************************************************************************
*
* usbOhciIsr - interrupt handler for USB handling OHCI Controller interrupts
*
* This function is registered as the interrupt handler for handling OHCI
* Controller interrupts.
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL VOID usbOhciIsr
    (
    pUSB_OHCD_DATA   pHCDData
    )
    {
    /* To hold the value read from the registers */

    UINT32                  uRegisterValue = 0;

    /* Check whether the host controller index is valid */
  //   printk("@@@ %s %d\n",__FUNCTION__,__LINE__);
    if (NULL == pHCDData)
        {
        USB_OHCD_ERR("invalid parameter of pHCDData \n",
                 1,  2, 3, 4, 5, 6);
        return;
        }

    if (USB_OHCD_MAGIC_ALIVE != pHCDData->isrMagic)
        {
        USB_OHCD_ERR("The magic code != 0xbeefbeef "
                     "pHCDData->isrMagic = 0x%8x \n",
                     pHCDData->isrMagic,
                     2, 3, 4, 5, 6);
        return;
        }

    /*
     * NOTE: In a system with shared interrupts, the ISR will be called
     *       under conditions when another device sharing the interrupt
     *       line generates an interrupt.
     *
     *       Hence, make sure that the OHCI interrupts are enabled. If
     *       not, it indicates that the interrupt was for another device
     *       sharing the interrupt line. Hence, return without proceeding.
     *
     *       THIS CHECK SHOULD NOT BE MADE AGAINST THE INTERRUPT STATUS
     *       REGISTER. THIS IS BECAUSE, THE INTERRUPTS ARE CLEARED ONLY
     *       AFTER THE INTERRUPT CONDITION IS HANDLED. HENCE, A CHECK
     *       AGAINST THE INTERRUPT STATUS REGISTER WILL GIVE WRONG
     *       INFORMATION.
     */

    /* Read the contents of the interrupt enable register */

    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                          USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET);

    /* If the OHCI interrupts are not enabled, return without proceeding */

    if (0 == uRegisterValue)
        {
        USB_OHCD_VDBG("OHCI interrupts are not enabled \n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /* Disable the interrupts */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                        uRegisterValue);

    /* Signal the ISR event
     * Note that the semaphore is taken by ohciPollingIsr()
     */

    OS_RELEASE_EVENT(pHCDData->isrEvent);

    /*
     * Note:  The interrupts are re-enabled at task level by the
     * task released by pHCDData->isrEvent
     */
    return;

    } /* End of function usbOhciIsr () */
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */


/***************************************************************************
*
* usbOhciPollingIsr - thread for handling USB OHCI Controller interrupts
*
* This function is spawn as thread for handling USB OHCI Controller
* interrupts.
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL VOID usbOhciPollingIsr
    (
    UINT8 uHostControllerIndex
    )
    {
#ifndef USB_OHCI_POLLING_MODE
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* Check whether the host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Debug print */

        USB_OHCD_ERR("invalid host controller index %d \n",
                     uHostControllerIndex, 2, 3, 4, 5, 6); 

        /* Invalid host controller index */
        return;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */

    /* Call the OHCI interrupt infinitely */

    while (1)
        {
#ifdef USB_OHCI_POLLING_MODE
        /*
         * Reschedule the task so that other OHCI polling mode ISR can
         * execute.
         */

        OS_RESCHEDULE_THREAD();
#else
        /* Wait for the ISR event */

        OS_WAIT_FOR_EVENT(pHCDData->isrEvent, WAIT_FOREVER);
#endif
         /* Call the interrupt handler */

        usbOhciProcessInterrupts (uHostControllerIndex);
        }

    } /* End of function usbOhciPollingIsr () */


/***************************************************************************
*
* usbOhciProcessInterrupts - Function for handling OHCI Controller interrupts
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL VOID usbOhciProcessInterrupts
    (
    UINT8 uHostControllerIndex
    )
    {
    /* To hold the value read from the interrupt status registers */

    UINT32  uInterruptStatus = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    UINT32 uRegReadValue = 0;

    /* To hold the head pointer to the list of transfers completed */

    PVOID pTransferCompletion = NULL;

    /* To hold the pointer to the HCCA */

    PUSB_OHCI_HCCA  pHCCA = NULL;

    UINT32          uBufferAddr = 0x0;


    /* Check whether the host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT )
        {
        /* Debug print */

        USB_OHCD_ERR("invalid host controller index %d \n",
                     uHostControllerIndex, 2, 3, 4, 5, 6);

        /* Invalid host controller index */
        return;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Validity check for pHCDData->pDev */

    if (pHCDData->pDev == NULL)
        return;

#ifndef USB_OHCI_POLLING_MODE
    /* Disable the interrupts */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */

    /* Read the contents of the HC Interrupt Status register */

    uInterruptStatus =
        USB_OHCI_REG_READ (pHCDData->pDev,
                           USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET);

    /* We do not expect UE (unrecoverable error) to be handled here */

    if (uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_UE)
        {
        USB_OHCD_ERR("USB_OHCI_INTERRUPT_STATUS_UE..." \
                      "fatal hardware error\n", 1, 2, 3, 4, 5, 6);

        OS_RESCHEDULE_THREAD ();

        /* Post a message to report error */

        usbMsgPost(USBMSG_HCD_OHCD_ERROR,
               pHCDData->pDev,
               (void *)((unsigned long)uHostControllerIndex));

        return;
        }

    /* Read the contents of the control register */

    uRegReadValue = USB_OHCI_REG_READ(pHCDData->pDev,
                                      USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Check if there are elements in the endpoint disabled list */

    if (lstCount(&(pHCDData->disableEDList)) != 0)
       {
       uInterruptStatus &= (USB_OHCI_INTERRUPT_MASK |
                            USB_OHCI_INTERRUPT_STATUS_SOF);
       }
    else
       {
       uInterruptStatus &= USB_OHCI_INTERRUPT_MASK;
       USB_OHCI_REG_WRITE (pHCDData->pDev,
                           USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                           (uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_SOF));
       }

    /* Check whether an interrupt is pending */

    if (uInterruptStatus != 0)
        {

        /*
         * Check whether the Write Back Done Head interrupt is pending.
         *
         * NOTE: Check for the Write Back Done Head interrupt before checking
         *       the SOF interrupt. This is because the SOF interrupt is used
         *       for cancelling the transfers.
         *
         *       However, the transfer which is marked for cancellation could
         *       have been completed. Hence, Write Back Done Head interrupt
         *       should be checked before attempting to cancel a transfer
         *       (i.e. servicing the SOF interrupt).
         */
        if ((uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_DONE_HEAD) ==
            USB_OHCI_INTERRUPT_STATUS_DONE_HEAD)
            {


            /* Retrieve the HCCA information */

            pHCCA = pHCDData->pHcca;

            /* Retrieve the HCCA Done head */

            uBufferAddr = (UINT32)pHCCA->uHccaDoneHead;

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            /* Mask the general transfer descriptor */

            uBufferAddr &= ~(USB_OHCI_TD_ALIGN_MASK);

            /* Retrieve the virtual address */

            pTransferCompletion = usbOhciPhyToVirt(pHCDData,
                                                   uBufferAddr,
                                                   USB_OHCI_MEM_MAP_TD);
            /* Debug print */

            USB_OHCD_VDBG("HC%d: received write back done head interrupt, TD[%p]\n",
                          uHostControllerIndex, (ULONG) pTransferCompletion,
                          3, 4, 5, 6);

            /* Clear the write back done head interrupt */

            USB_OHCI_REG_WRITE (pHCDData->pDev,
                                USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                                USB_OHCI_INTERRUPT_STATUS_DONE_HEAD);

            /* Call the function to process the done head interrupt */

            usbOhciProcessTransferCompletion (uHostControllerIndex,
                                              pTransferCompletion);

            }

        /*
         * NOTE: The least significant bit of the HccaDoneHead is set to 1
         *       to indicate whether an unmasked HcInterruptStatus was set
         *       when HccaDoneHead was written.
         *
         *       HANDLING OF THE ABOVE INTERRUPT CONDITION IS PENDING.
         */


        /* Check whether the SOF interrupt is pending */

        if ((uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_SOF) ==
            USB_OHCI_INTERRUPT_STATUS_SOF)
            {
            /* Clear the SOF interrupt */

            USB_OHCI_REG_WRITE (pHCDData->pDev,
                                USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                                USB_OHCI_INTERRUPT_STATUS_SOF);

            usbOhciCleanDisabledPipe(pHCDData);
            }	/* End of if ((uInterruptStatus ...)) */

        /* Check whether the root hub status change interrupt is pending */

        if ((uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_RHSC) ==
            USB_OHCI_INTERRUPT_STATUS_RHSC)
            {
#if (CPU == SH7750)
            /*
             * As long as any of the status change bits are set in the Root hub
             * port registers, the Root hub status change interrupt does not
             * get cleared only in the case of
             * the onboard host controller. Due to this reason, the RHSC
             * interrupt is generated frequently. Because of this,
             * the "usbOhciIsr" thread does not allow any tasks of
             * lower priority to be executed if the scheduling mode is a
             * priority based scheduling.
             * This is done to explicitly reschedule
             * other threads in the system.
             */

            OS_RESCHEDULE_THREAD();
#else
            /*
             * Very much similar to the above, but manifesting itself on
             * different architectures running vxBus.  This case was a race
             * condition between the task reporting a root hub and the
             * controller signaling the RHSC interrupt.  The race condition
             * manifested itself as a large number of RHSC interrupts.
             *
             * The fix is to allow the rescheduling of threads if we have
             * a RHSC interrupt that does not correpond to pending request.
             * Note that a the root hub will not be able to clear the RHSC bit
             * in the Interrupt Status register if the root hub port event has
             * not been cleared in the corresponding port status register.
             */

            if (pHCDData->pRootHubInterruptRequest == NULL)
                {
                USB_OHCD_DBG("usbOhciProcessInterrupts - "
                    "no root hub interrupt request pending, delaying...\n",
                    1, 2, 3, 4, 5, 6);

                OS_RESCHEDULE_THREAD ();
                }
#endif
            /*
             * Check whether the status change request is pending for
             * the root hub.
             */

            if (pHCDData->pRootHubInterruptRequest != NULL)
                {
                /* Call the function to process the root hub status change */

                usbOhciProcessRootHubStatusChange (uHostControllerIndex);
                }

            /* Clear the root hub status change interrupt */

            USB_OHCI_REG_WRITE (pHCDData->pDev,
                                USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                                USB_OHCI_INTERRUPT_STATUS_RHSC);

            }

        /* Check whether the resume detect interrupt is pending */

        if ((uInterruptStatus & USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT) ==
            USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT)
            {
            /* Clear the resume detect interrupt */

            USB_OHCI_REG_WRITE (pHCDData->pDev,
                                USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                                USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT);

            /*
             * Check whether the remote wakeup feature is enabled for
             * the root hub.
             */

            if (pHCDData->bRemoteWakeupEnabled)
                {
                /* Service the remote wakeup interrupt */
                /* PENDING */
                }

            }

        }	/* End of if (uInterruptStatus != 0) */

#ifndef USB_OHCI_POLLING_MODE
    /*
     * Enable the interrupts - This enable is done at task level.
     */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);

#endif /* End of #ifndef USB_OHCI_POLLING_MODE */

    return;
    } /* End of function usbOhciProcessInterrupts () */

/* End of File usbOhciIsr.c */

