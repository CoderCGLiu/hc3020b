/* usbPlxTcdUtil.h - USB PLX TCD utility module */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01c,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01b,10oct12,s_z  Remove un-used routine (WIND00382688)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This module contains the prototypes of the utility functions
which are used by the USB PLX Target Controller Driver. 
*/

#ifndef __INCusbPlxTcdUtilh
#define __INCusbPlxTcdUtilh

#include <usbPlxTcd.h>

#ifdef  __cplusplus
extern "C" {
#endif

/* declartion */

IMPORT void usbPlxTcdFIFOWrite
    (
    pUSB_PLX_CTRL pPlxCtrl,  
    UINT8 *       pSrcBuffer,
    UINT32        uCount,    
    UINT8         uEpIndex  
    );
IMPORT UINT32 usbPlxTcdFIFORead
    (
    pUSB_PLX_CTRL pPlxCtrl,   
    UINT8 *       pDisBuffer, 
    UINT32        uCount,     
    UINT8         uEpIndex    
    );
IMPORT void usbPlxTcdEpRegRead
    (
    pUSB_PLX_CTRL pPlxCtrl,
    UINT8         uEpIndex
    );
IMPORT UINT8 usbPlxTcdBusSpeedGet
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
IMPORT STATUS usbPlxTcdPipePhyEpRelease
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    );
IMPORT STATUS usbPlxTcdPipePhyEpAssign
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    );
IMPORT STATUS usbPlxTcdRequestComplete
    (
    pUSB_PLX_TCD_REQUEST pRequest,
    int                  status
    );
IMPORT STATUS usbPlxTcdPipeProcessStart
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe
    );
IMPORT STATUS usbPlxTcdSchedule
    (
    pUSB_PLX_TCD_DATA pTCDData
    );
IMPORT void usbPlxTcdPipeFlush
    (
    pUSB_PLX_TCD_PIPE pTCDPipe,
    int               status
    );
IMPORT pUSB_PLX_TCD_PIPE usbPlxTcdPipeFind
    (
    pUSB_PLX_TCD_DATA pTCDData,
    UINT8             uPhyEpIndex
    );
IMPORT STATUS usbPlxTcdRequestRelease
    (
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    );
IMPORT pUSB_PLX_TCD_REQUEST usbPlxTcdRequestReserve
    (
    pUSB_PLX_TCD_PIPE pTCDPipe
    );
IMPORT pUSB_PLX_TCD_REQUEST usbPlxTcdFirstPendingReqGet
    (
    pUSB_PLX_TCD_PIPE pTCDPipe
    );
IMPORT void usbPlxTcdLgoUxWorkaround
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
IMPORT void usbPlxTcdEnableDataEpsAsZero
    (
    pUSB_PLX_CTRL pPlxCtrl
    );
IMPORT void usbPlxTcdPipeSequNumReset
    (
    pUSB_PLX_CTRL     pPlxCtrl,
    pUSB_PLX_TCD_PIPE pTCDPipe
    );

#endif /* __INCusbPlxTcdUtilh */

