/* usbHubInitialization.c - Initialization and cleanup of HUB class driver */

/*
 * Copyright (c) 2003-2004, 2006-2007, 2010-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2004, 2006-2007, 2010-2012 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01k,24aug12,w_x  Fix task schedule issue in single BusM mode (WIND00370558)
01j,19jul12,w_x  Add support for USB 3.0 host (WIND00188662)
01i,06jul11,ghs  Change hub protocol code for match more hubs (WIND00183500)
01h,08jul10,m_y  Modify for coding convention
01g,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01f,11mar10,j_x  Changed for USB debug (WIND00184542)
01e,13jan10,ghs  vxWorks 6.9 LP64 adapting
01d,08apr07,sup  changing the reference to usbHubInitialization.h as the file
                 has been moved
01c,08oct06,ami  Changes for USB-vxBus changes
01b,15oct04,ami  Refgen Changes
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This module provides the initialization and the clean up functions
for the USB Hub Class Driver.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHubCommon.h,
usb2/usbHubGlobalVariables.h, usb2/usbHubInitialization.h,
usb2/usbHubClassInterface.h, usb2/usbHst.h, usb2/usbHcdInstr.h

*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : HUB_Initialization.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This module provides the initialization and the clean up
 *                    functions for the USB Hub Class Driver.
 *
 *
 ******************************************************************************/


/************************** INCLUDE FILES *************************************/

#include "usb/usbOsal.h"
#include "./conf/usbHubCommon.h"
#include "./conf/usbHubGlobalVariables.h"
#include "usb/usbHubInitialization.h"
#include "./conf/usbHubClassInterface.h"
#include "usb/usbHst.h"
#include "usb/usbHcdInstr.h"
#include "usb/usbd.h"
#include "hwif/vxbus/vxBus.h"

/**************************** SUB MODULE INCLUDES *****************************/

#include "./conf/usbHubGlobalVariables.c"
#include "./conf/usbHubUtility.c"
#include "./conf/usbHubPortEventHandler.c"
#include "./conf/usbHubEventHandler.c"
#include "./conf/usbHubBusManager.c"
#include "./conf/usbHubClassInterface.c"

/* usbHubMisc.c has interfaces needed for usrUsbTool */

#include "./conf/usbHubMisc.c"

/* defines */

#define HUB_DRIVER_NAME "vxbUsbHubClass"    /* our USBD client name */


/******************* MODULE SPECIFIC VARIABLES DEFINITION *********************/

/* This stores the information about the Hub Driver. */
LOCAL USBHST_DEVICE_DRIVER       HubDriverInfo;

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
OS_THREAD_ID BusManagerThreadID;
#endif

/************************ GLOBAL FUNCTIONS DEFINITION *************************/

/***************************************************************************
*
* usbHubInit - registers USB Hub Class Driver function pointers.
*
* This function initializes the global variables and registers USB
* Hub Class Driver function pointers with the USB Host Software Stack. This also
* retrieves the USB Host Software Stack functions for future access.
*
* RETURNS: 0 , -1 on fail.
*
* ERRNO: None
*/
INT8 usbHubInit(void)
    {
    /* Variable to store the result */
    USBHST_STATUS         Result = USBHST_FAILURE;

    /* Void pointer to handle the data interchange with the USB Host software */
    void *                pContext = NULL;

  //  printk("%s:%d\n",__FUNCTION__,__LINE__);
    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_INIT_EXIT,
        "Entering usbHubInit() Function",
        USB_HUB_WV_FILTER);

    /* Initialize the global bus list by setting  gpGlobalBus  to NULL.*/
    gpGlobalBus = NULL;
   // printk("%s:%d\n",__FUNCTION__,__LINE__);
    /*
     * Initialize HubDriverInfo with the function pointers to the HUB_AddHub,
     * HUB_RemoveHub, HUB_SuspendHub, HUB_ResumeHub and the hub class code.
     */

   /* Clear the HubDriverInfo structure */
    OS_MEMSET(&HubDriverInfo,0,sizeof(USBHST_DEVICE_DRIVER));

    /* State that we are using class specific matching */
    HubDriverInfo.bFlagVendorSpecific  = FALSE;

    /* The Class code for Hub class devices is 0x09H */
    HubDriverInfo.uVendorIDorClass     = USBHST_HUB_CLASS;

    /* The Sub Class code for Hub class devices is 0x00H */
    HubDriverInfo.uProductIDorSubClass = HUB_SUB_CLASS_CODE;

    /* The device protocol for Hub class devices is 0x00H */
    HubDriverInfo.uBCDUSBorProtocol    = USBD_NOTIFY_ALL;

    /* Call HUB_AddHub to add a normal Hub */
    HubDriverInfo.addDevice            = usbHubAdd;

    /* Call HUB_RemoveHub to remove a normal Hub */
    HubDriverInfo.removeDevice         = usbHubRemove;

    /* Call HUB_SuspendHub to suspend a Hub */
    HubDriverInfo.suspendDevice        = usbHubSuspend;

    /* Call HUB_ResumeHub to resume a Hub */
    HubDriverInfo.resumeDevice         = usbHubResume;

    /*
     * Initialize HUB_FUNCTION_LIST structure with the function pointers to the
     * HUB_AddRootHub, HUB_RemoveRootHub, HUB_SelectiveSuspendDevice,
     * HUB_SelectiveResumeDevice, HUB_CheckPower and HUB_ResetDevice.
     */
    /* Clear the USBHST_FUNCTION_LIST structure */
	
    OS_MEMSET(&g_usbHstFunctionList,0,sizeof(USBHST_FUNCTION_LIST));

    /* Call HUB_SelectiveSuspendDevice to selectively suspend a device */
    g_usbHstFunctionList.HubFunctionList.selectiveSuspendDevice = usbHubSelectiveSuspendDevice;

    /* Call HUB_SelectiveResumeDevice to selectively resume a device */
    g_usbHstFunctionList.HubFunctionList.selectiveResumeDevice  = usbHubSelectiveResumeDevice;

    /* Call HUB_AddRootHub to add a root hub device */
    g_usbHstFunctionList.HubFunctionList.addRootHub             = usbHubAddRoot;

    /* Call HUB_RemoveRootHub to remove a root hub device */
    g_usbHstFunctionList.HubFunctionList.removeRootHub          = usbHubRemoveRoot;

    /* Call HUB_CheckPower to check the power compatibility of a device */
    g_usbHstFunctionList.HubFunctionList.checkForPower          = usbHubCheckPower;

    /* Call HUB_ResetDevice to selectively reset a device */
    g_usbHstFunctionList.HubFunctionList.resetDevice            = usbHubResetDevice;

    /* Call HUB_ClearTT to submit ClearTT Request */
    g_usbHstFunctionList.HubFunctionList.clearTT                = usbHubClearTT;

    /* Call Hub_ResetTT to submit a ResetTT Request*/
    g_usbHstFunctionList.HubFunctionList.resetTT                = usbHubResetTT;

    /* Cast the hub function list pointer to (PVOID) */
    pContext = (PVOID)(&g_usbHstFunctionList);
    /*
     * Register with the USB Host software
     * we pass the pointer to the Hub Function list to the USB Host Software
     * stack.
     * In return we get the pointer to the USBHST Function list.
     */
     
    Result = usbHstDriverRegister(&HubDriverInfo, &pContext, HUB_DRIVER_NAME);
    //printk("%s:%d\n",__FUNCTION__,__LINE__);
    /* If USBHST_RegisterDriver does not return USBHST_SUCCESS then return -1.*/
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("unable to register hub driver to USBD, " \
                    "result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        return -1;
        }

     /* Verify the pContext  */
    OS_ASSERT(NULL != pContext);

    /* Verify the new device interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.newDevice);

    /* Verify the configure device interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.configureDevice);

    /* Verify the remove device interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice);

    /* Verify the suspend interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.suspendDevice);

    /* Verify the Resume interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.resumeDevice);

    /* Verify the ResetTTComplete interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.resetTTComplete);

    /* Verify the ClearTTComplete interface */
    OS_ASSERT(NULL != g_usbHstFunctionList.UsbdToHubFunctionList.clearTTComplete);
    //printk("%s:%d\n",__FUNCTION__,__LINE__);
    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT ( USB_HUB_WV_INIT_EXIT,
                        "Exiting usbHubInit() Function",
                        USB_HUB_WV_FILTER);

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS

    BusManagerThreadID = OS_CREATE_THREAD((char *) "tUsbBusM",
                                                USB_HUB_THREAD_PRIORITY,
                                                usbHubThread,
                                                (long)NULL);
    if (BusManagerThreadID == OS_THREAD_FAILURE)
        return -1;

#endif 

    /* Return 0. */
    return 0;

    } /* End of HUB_Init() function */


/***************************************************************************
*
* usbHubExit - de-registers and cleans up the USB Hub Class Driver.
*
* de-registers and cleans up the USB Hub Class Driver from the USB Host Software
* Stack.
*
* RETURNS: None
*
* ERRNO: None
*/
INT8 usbHubExit (void)
    {
    /* Store the value of the de-registration */
    USBHST_STATUS Result;

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
    
    if ((BusManagerThreadID != NULL) &&
        (BusManagerThreadID != OS_THREAD_FAILURE))
        {
        OS_DESTROY_THREAD(BusManagerThreadID);
        
        BusManagerThreadID = OS_THREAD_FAILURE;
        }
    
#endif 

    /* If  gpGlobalBus  is not NULL, then return -1. */
    if (NULL != gpGlobalBus)
        {
        /* Debug Message */

        USB_HUB_ERR("g_GlobalBus is not null, " \
                    "BusHandle=0x%X \n",
                    gpGlobalBus->uBusHandle, 2, 3, 4, 5, 6);
        return -1;
        }

    /* Call the USBHST_DeregisterDriver with HubDriverInfo.*/
    Result = usbHstDriverDeregister(&HubDriverInfo);

    /* If the USBHST_DeregisterDriver call fails then return -1. */
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to de-register hub driver from USBD, " \
                    "result=0x%X \n",
                    Result, 2, 3, 4, 5, 6);

        return -1;
        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_INIT_EXIT,
        "Exiting usbHubInit() Function",
        USB_HUB_WV_FILTER);

    /* Return 0.*/
    return 0;

    } /* End of HUB_Exit() function */


/**************************** End of File HUB_Initialization.c ****************/
