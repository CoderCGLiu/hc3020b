/* usbPlxTcdDma.h - USB PLX TCD DMA module */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This file contains the prototypes of the utility functions
which are used by the USB PLX Target Controller Driver. 

*/

#ifndef __INCusbPlxTcdDmah
#define __INCusbPlxTcdDmah

#include <usbPlxTcd.h>

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */

/* typedefs */


#define USB_PLX_DMA_MAX_CH  4


struct usb_plx_ctrl;

typedef struct usb_plx_dma_data    
    {    
    OS_EVENT_ID  pSynchMutex; 
    OS_EVENT_ID  isrEvent;
    OS_THREAD_ID irqThread;   
    UINT32      uDmaOffset; 
    UINT32      isrMagic;
    UINT16      uFreeDmaChannelBitMap; 
    UINT8       uIrqStatus;
    UINT8       uMaxDmaChannel;   
    BOOL        bEnable; 
    USB_PLX_DMA_QUEUE dmaQueue[USB_PLX_DMA_MAX_CH];
    UINT8 (* pDmaChAssign)(struct usb_plx_ctrl * pPlxCtrl,
                           UINT8 uEpIndex);    
    STATUS (* pDmaChRelease)(struct usb_plx_ctrl * pPlxCtrl,  
                             UINT8 uChannel);    
    STATUS (* pDmaStart)(struct usb_plx_ctrl * pPlxCtrl,  
                         UINT8 uChannel);    
    STATUS (* pDmaAbort)(struct usb_plx_ctrl * pPlxCtrl,  
                         UINT8 uChannel);    
    STATUS (* pDmaDisable)(struct usb_plx_ctrl * pPlxCtrl,  
                         UINT8 uChannel);    
    STATUS (* pDmaEnable)(struct usb_plx_ctrl * pPlxCtrl, 
                           UINT8 uChannel);        
    STATUS (* pDmaQueue)(struct usb_plx_ctrl * pPlxCtrl,
                         void * pRequest);    
    STATUS (* pDmaDeQueure)(struct usb_plx_ctrl * pPlxCtrl,  
                            void * pRequest);   
    void (*pDmaIrqHandler)(struct usb_plx_ctrl * pPlxCtrl);
    void (*pDmaIsr)(struct usb_plx_ctrl * pPlxCtrl);  
    void (*pDmaCallback)(struct usb_plx_ctrl * pPlxCtrl); 
    int  (*pDmaIoCtrl)(struct usb_plx_ctrl * pPlxCtrl, int cmd, void * pContext);
    } USB_PLX_DMA_DATA, *pUSB_PLX_DMA_DATA;

/* declartion */

IMPORT STATUS usbPlxTcdDmaStart
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    );
IMPORT STATUS usbPlxTcdDmaStop
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe
    );


#endif /* __INCusbPlxTcdDmah */


