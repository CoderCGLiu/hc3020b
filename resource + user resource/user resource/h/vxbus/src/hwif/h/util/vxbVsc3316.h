/* vxbVsc3316.h - VxBus VSC3316/3318 driver interface header file */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,16jan12,wap  written
*/

/*
DESCRIPTION

This header file describes the APIs and structures used with the VSC3316/3308
crossbar switch driver. APIs are provided to load a new input/output link
configuration to initialize the bridge to match the hardware.
*/

#ifndef __INCvxbVsc3316h
#define __INCvxbVsc3316h 

#ifdef __cplusplus
extern "C" {
#endif

#define VSC_CONNECTIONS_MAX	16

/* typedef */

typedef struct vscConnection
    {
    int		vscInput;
    int		vscOutput;
    } VSC_CONNECTION;

typedef struct vscTable
    {
    int			vscConnCnt;
    VSC_CONNECTION	vscConnections[VSC_CONNECTIONS_MAX];
    } VSC_TABLE;

IMPORT STATUS vsc3316Config (int, VSC_TABLE *);
IMPORT STATUS vsc3308Config (int, VSC_TABLE *);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbVsc3316h */

