/* sioChanUtil.h - SIO Channel Utilities module */

/*
 * Copyright (c) 2005-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* 
modification history
--------------------
01c,03may10,h_k  added vxbSerialChanGet() prototype.
01b,25mar09,h_k  added prototype for sioNextChannelNumberAssign,
                 sysSerialChanConnect and sysSerialConnectAll.
01a,08apr05,tor  created
*/

#ifndef __INCsioChanUtil_H
#define __INCsioChanUtil_H

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

/* defines */

/* typedefs */

typedef struct sioChanMethod
    {
    int sioChanNo;  /* channel number desired */
    void * pChan;   /* pChan for specified channel */
    } SIO_CHANNEL_INFO;

/* globals */

IMPORT char sioChanGet_desc[];

/* locals */

/* forward declarations */

IMPORT int sioNextChannelNumberAssign (void);
IMPORT STATUS sysSerialChanConnect (int sioChan);
IMPORT void sysSerialConnectAll (void);
IMPORT SIO_CHAN * vxbSerialChanGet (int sioChan);

#ifdef __cplusplus
}
#endif
#endif /* __INCsioChanUtil_H */
