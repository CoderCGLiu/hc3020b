/* vxbRtcLib.h - VxBus RTC interfaces header file */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,14may12,y_y  changed alarmSet routine, added alarm unit, callback 
                 and callback paramete.
01a,24feb12,l_z  written
*/

/*
DESCRIPTION

This library provides the RTC specific interfaces

*/

#ifndef __INCvxbRtcLibh
#define __INCvxbRtcLibh 

#include <hwif/vxbus/vxBus.h>
#include <vxBusLib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define  MAX_ALARMS                2  /* The maximum number of the hardware alarm */

#define  RTC_ALARM_SEC_SUPPORT     0x01

/* alarm callback */

typedef STATUS (*RTC_ALARM_FUNC) (VOID *pArg); 

/* structure to be used and updated RTC time and Alarm */

struct vxbRtcFunctionality
    {
    char   timerName[MAX_DRV_NAME_LEN + 1];
    UINT8  alarmNum;                  /* Alarm numbers */
    UINT8  alarmCap[MAX_ALARMS];      /* Alarm attribute */
    STATUS (*rtcSet)(VXB_DEVICE_ID pInst, struct tm * rtc_time);
    STATUS (*rtcGet)(VXB_DEVICE_ID pInst, struct tm * rtc_time); 
    STATUS (*alarmSet)(VXB_DEVICE_ID pInst, UINT8, struct tm * rtc_time, 
                       RTC_ALARM_FUNC rtcAlarmFunc, void *pArg);
    STATUS (*alarmGet)(VXB_DEVICE_ID pInst, UINT8, struct tm * rtc_time); 
    } VXB_I2C_RTC_FUNC;

struct vxbRtcHandle
    {
    struct vxbRtcFunctionality  *pRtcFunc;
    VXB_DEVICE_ID                pInst;
    };

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbRtcLibh */

