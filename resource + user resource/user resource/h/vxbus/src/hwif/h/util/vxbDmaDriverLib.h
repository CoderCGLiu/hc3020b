/* vxbDmaDriverLib.h - vxBus DMA Driver interfaces header file */

/*
 * Copyright (c) 2006 - 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/* 
modification history
--------------------
01c,07feb07,rcs  typedef VXB_DMA_REQUEST
01b,05oct06,pdg  updated review comments
01a,22Jun06,pdg written
*/

/*
DESCRIPTION

This file defines the macros, structures and interfaces which can be used
by the DMA device drivers.
*/
#ifndef __INCvxbDmaDriverLibh
#define __INCvxbDmaDriverLibh 

#ifdef __cplusplus
extern "C" {
#endif

#include <hwif/util/vxbDmaLib.h>

typedef struct vxbDmaRequest
    {
    VXB_DEVICE_ID	instance;	/* DMA requestor device id */
    UINT32		minQueueDepth;	/* minimum queue depth requested */
    UINT32		flags;		/* flags used during DMA allocation */	
    VXB_DMA_RESOURCE_ID	pChan;		/* DMA channel id */
    void *		pDedicatedChanInfo; /* dedicated channel information */
    }VXB_DMA_REQUEST;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbDmaDriverLibh */ 
