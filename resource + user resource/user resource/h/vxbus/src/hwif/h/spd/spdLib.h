/* spdLib.h - serial presence detect common library header */

/*
 * Copyright (c) 2013, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
15may14,wyt  fix the type of some members of stuct ddr3SpdData.
01d,26mar13,syt  added descriptions for some data structure definitions.
01c,26feb13,syt  clean up.
01b,18feb13,syt  rename lawRegBase to regBase1 in struct ddrHwconf. 
01a,26jan13,syt  writen.
*/


#ifndef __INCspdLibh
#define __INCspdLibh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

/* defines */

#ifndef UINT64
#   define UINT64 unsigned long long
#endif /* UINT64 */

/* SPD data length in EEPROM */

#define MAX_SPD_BYTE                        256

/* The max DIMM slot number on each DDR controller */

#ifndef MAX_SLOT_NUM_PRE_CTLR
#   define MAX_SLOT_NUM_PRE_CTLR            4
#endif /* MAX_SLOT_NUM_PRE_CTLR */

#define DATA_BUS_WIDTH_8                    0
#define DATA_BUS_WIDTH_16                   1
#define DATA_BUS_WIDTH_32                   2
#define DATA_BUS_WIDTH_64                   3

/* Byte 3 Key Byte / Module Type for DDR3 SPD */

#define DDR3_MODULE_TYPE_MASK               0x0f
#define DDR3_MODULE_TYPE_RDIMM              0x01
#define DDR3_MODULE_TYPE_UDIMM              0x02
#define DDR3_MODULE_TYPE_SO_DIMM            0x03
#define DDR3_MODULE_TYPE_MICRO_DIMM         0x04
#define DDR3_MODULE_TYPE_MINI_RDIMM         0x05
#define DDR3_MODULE_TYPE_MINI_UDIMM         0x06
#define DDR3_MODULE_TYPE_MINI_CDIMM         0x07
#define DDR3_MODULE_TYPE_72B_SO_UDIMM       0x08
#define DDR3_MODULE_TYPE_72B_SO_RDIMM       0x09
#define DDR3_MODULE_TYPE_72B_SO_CDIMM       0x0a
#define DDR3_MODULE_TYPE_LRDIMM             0x0b
#define DDR3_MODULE_TYPE_16B_SO_DIMM        0x0c
#define DDR3_MODULE_TYPE_32B_SO_DIMM        0x0d

#define DDR_SDRAM                           7
#define DDR2_SDRAM                          8
#define DDR3_SDRAM                          11

#define SINGLE_RANK                         0x01
#define DUAL_RANK                           0x02
#define QUAD_RANK                           0x04

#define DRAM_DEVICE_BYTE                    2
#define DRAM_DDR_TYPE                       0x07
#define DRAM_DDR2_TYPE                      0x08
#define DRAM_DDR3_TYPE                      0x0b

#define SPD_BANK_BITS_MASK                  0x70
#define SPD_BANK_BITS_SHIFT                 4
#define SPD_CAPACITY_MASK                   0x0f
#define SIZE_256Mb                          0x10000000

#define ROW_ADDR_BITS_MASK                  0x38
#define ROW_ADDR_BITS_SHIFT                 0x03
#define COL_ADDR_BITS_MASK                  0x07

#define VDD_1V5                             0x00
#define VDD_1V35_1V5                        0x02
#define VDD_1V2X_1V35_1V5                   0x06

#define RANK_NUM_MASK                       0x38
#define RANK_NUM_SHIFT                      0x03
#define DEV_WIDTH_MASK                      0x07

#define MOD_PRI_BUS_WIDTH_MASK              0x07
#define MOD_EXT_BUS_WIDTH_MASK              0x18
#define MOD_EXT_BUS_WIDTH_SHIFT             0x03

#define FTB_DIVISOR(x)                      (x & 0x0f)
#define FTB_DIVIDEND(x)                     ((x & 0xf0) >> 4)

#define CL_4                                0x0001
#define CL_5                                0x0002
#define CL_6                                0x0004
#define CL_7                                0x0008
#define CL_8                                0x0010
#define CL_9                                0x0020
#define CL_10                               0x0040
#define CL_11                               0x0080
#define CL_12                               0x0100
#define CL_13                               0x0200
#define CL_14                               0x0400
#define CL_15                               0x0800
#define CL_16                               0x1000
#define CL_17                               0x2000
#define CL_18                               0x4000
#define CL(x)                               (x+4)

#define TRAS_HIGH_BITS_MASK                 0x0f
#define TRC_HIGH_BITS_MASK                  0xf0

/* type defines */

/*
 * This structure holds the information of the installed DIMMs' lowest rate
 * timing values, DIMM's DRAM type and Register Control Words. 
 */

typedef struct dimmComnParams {
    UINT32  fineTB;             /* Fine Timebase (ps) */
    UINT32  mediumTB;           /* Medium Timebase (ps) */
    UINT32  tCKmin;             /* SDRAM Minimum Cycle Time (ps) */
    UINT32  CASlatency;         /* SDRAM /CAS latencies supported */
    UINT32  tAAmin;             /* Minimum CAS Latency Time (ps) */
    UINT32  tWRmin;             /* Minimum Write Recovery Time (ps) */
    UINT32  tRCDmin;            /* Minimum RAS# to CAS# Delay Time (ps) */
    UINT32  tRRDmin;            /* Minimum Row Active to Row Active Delay Time (ps) */
    UINT32  tRPmin;             /* Minimum Row Precharge Delay Time (ps) */
    UINT32  tRASmin;            /* Minimum active to precharge time (ps) */
    UINT32  tRCmin;             /* Minimum Active to Active/Refresh Delay Time (ps) */
    UINT32  tRFCmin;            /* Minimum Refresh Recovery Delay Time (ps) */
    UINT32  tWTRmin;            /* Minimum Internal Write to Read Command Delay Time (ps) */
    UINT32  tRTPmin;            /* Minimum Internal Read to Precharge Command Delay Time (ps) */
    UINT32  tFAWmin;            /* Minimum Four Activate Window Delay Time (ps) */
    UINT32  tRefreshRate;       /* Minimum refreshRate (ps) */
    UINT32  registeredDimm;     /* registered Dimm */
    UINT32  unbufferedDimm;     /* unbuffered Dimm */
    UINT32  mirroredDimm;       /* Mirrored Dimm */
    UINT32  vaildDimmNum;       /* Vaild DIMM number */
    UINT32  eccSupport;         /* Support ECC function */
    UINT32  addLat;             /* Additive latency */
    UINT32  dramType;           /* DRAM type, such as DDR, DDR2 or DDR2 */

    /* DDR3 RDIMM */

    UINT8   rcw[16];            /* Register Control Word 0-15 */
} DIMM_COMN_PARAMS;

/* 
 * This structure holds the DIMM's parameters converted from SPD data, which is
 * required by DDR controller drivers.
 */

typedef struct dimmParams {

    UINT32  crcCoverage;        /* CRC coverage */
    UINT32  spdByteTotal;       /* SPD Bytes Total */
    UINT32  spdByteUsed;        /* SPD Bytes Used */
    UINT32  spdRev;             /* SPD Revision */

    UINT32  dramModType;        /* DRAM Module Type */
    UINT32  dramType;           /* DRAM Device Type */
    UINT32  dramBankAddrBits;   /* DRAM Bank Address Bits */
    UINT64  dramCapacity;       /* Total SDRAM capacity */
    UINT64  rankCapacity;       /* rank capacity */
    UINT64  modCapacity;        /* DRAM module capacity */
    UINT32  rowAddrWidth;       /* Row Address Bits */
    UINT32  colAddrWidth;       /* Column Address Bits */
    UINT32  voltage;            /* Module operating voltage */
    UINT32  rankNum;            /* Number of Ranks */
    UINT32  dramDevWidth;       /* SDRAM Device Width */
    UINT32  modPriBusWidth;     /* Module Memory Primary bus width */
    UINT32  modExtBusWidth;     /* Module Memory extension bus width */

    UINT32  registeredDimm;     /* Registered DIMM */
    UINT32  mirroredDimm;       /* Mirrored DIMM */

    UINT32  fineTB;             /* Fine Timebase (ps) */
    UINT32  mediumTB;           /* Medium Timebase (ps) */
    UINT32  tCKmin;             /* SDRAM Minimum Cycle Time (ps) */
    UINT32  CASlatency;         /* SDRAM /CAS latencies supported */
    UINT32  tAAmin;             /* Minimum CAS Latency Time (ps) */
    UINT32  tWRmin;             /* Minimum Write Recovery Time (ps) */
    UINT32  tRCDmin;            /* Minimum RAS# to CAS# Delay Time (ps) */
    UINT32  tRRDmin;            /* Minimum Row Active to Row Active Delay Time (ps) */
    UINT32  tRPmin;             /* Minimum Row Precharge Delay Time (ps) */
    UINT32  tRASmin;            /* Minimum active to precharge time (ps) */
    UINT32  tRCmin;             /* Minimum Active to Active/Refresh Delay Time (ps) */
    UINT32  tRFCmin;            /* Minimum Refresh Recovery Delay Time (ps) */
    UINT32  tWTRmin;            /* Minimum Internal Write to Read Command Delay Time (ps) */
    UINT32  tRTPmin;            /* Minimum Internal Read to Precharge Command Delay Time (ps) */
    UINT32  tFAWmin;            /* Minimum Four Activate Window Delay Time (ps) */
    UINT32  tRefreshRate;       /* Minimum refreshRate (ps) */

    /* DDR3 RDIMM */

    UINT8   rcw[16];            /* Register Control Word 0-15 */

    UINT64  memBase;            /* System memory base of the DIMM */
    UINT64  memSize;            /* System memory size of the DIMM */
    UINT32  slotRankCfg;        /* DIMM slot configuration, such as CS lines */
    UINT32  slotRankNum;        /* The vaild rank number on the DIMM */
} DIMM_PARAMS;

/* DDR1 DIMM SPD format */

typedef struct ddrSpdData {
    UINT8 info_size;            /*  0 # bytes written into serial memory */
    UINT8 chip_size;            /*  1 Total # bytes of SPD memory device */
    UINT8 mem_type;             /*  2 Fundamental memory type */
    UINT8 nrow_addr;            /*  3 # of Row Addresses on this assembly */
    UINT8 ncol_addr;            /*  4 # of Column Addrs on this assembly */
    UINT8 nrows;                /*  5 Number of DIMM Banks */
    UINT8 dataw_lsb;            /*  6 Data Width of this assembly */
    UINT8 dataw_msb;            /*  7 ... Data Width continuation */
    UINT8 voltage;              /*  8 Voltage intf std of this assembly */
    UINT8 clk_cycle;            /*  9 SDRAM Cycle time @ CL=X */
    UINT8 clk_access;           /* 10 SDRAM Access from Clk @ CL=X (tAC) */
    UINT8 config;               /* 11 DIMM Configuration type */
    UINT8 refresh;              /* 12 Refresh Rate/Type */
    UINT8 primw;                /* 13 Primary SDRAM Width */
    UINT8 ecw;                  /* 14 Error Checking SDRAM width */
    UINT8 min_delay;            /* 15 for Back to Back Random Address */
    UINT8 burstl;               /* 16 Burst Lengths Supported */
    UINT8 nbanks;               /* 17 # of Banks on SDRAM Device */
    UINT8 cas_lat;              /* 18 CAS# Latencies Supported */
    UINT8 cs_lat;               /* 19 CS# Latency */
    UINT8 write_lat;            /* 20 Write Latency (aka Write Recovery) */
    UINT8 mod_attr;             /* 21 SDRAM Module Attributes */
    UINT8 dev_attr;             /* 22 SDRAM Device Attributes */
    UINT8 clk_cycle2;           /* 23 Min SDRAM Cycle time @ CL=X-0.5 */
    UINT8 clk_access2;          /* 24 SDRAM Access from
                                      Clk @ CL=X-0.5 (tAC) */
    UINT8 clk_cycle3;           /* 25 Min SDRAM Cycle time @ CL=X-1 */
    UINT8 clk_access3;          /* 26 Max Access from Clk @ CL=X-1 (tAC) */
    UINT8 trp;                  /* 27 Min Row Precharge Time (tRP)*/
    UINT8 trrd;                 /* 28 Min Row Active to Row Active (tRRD) */
    UINT8 trcd;                 /* 29 Min RAS to CAS Delay (tRCD) */
    UINT8 tras;                 /* 30 Minimum RAS Pulse Width (tRAS) */
    UINT8 bank_dens;            /* 31 Density of each bank on module */
    UINT8 ca_setup;             /* 32 Addr + Cmd Setup Time Before Clk */
    UINT8 ca_hold;              /* 33 Addr + Cmd Hold Time After Clk */
    UINT8 data_setup;           /* 34 Data Input Setup Time Before Strobe */
    UINT8 data_hold;            /* 35 Data Input Hold Time After Strobe */
    UINT8 res_36_40[5];         /* 36-40 reserved for VCSDRAM */
    UINT8 trc;                  /* 41 Min Active to Auto refresh time tRC */
    UINT8 trfc;                 /* 42 Min Auto to Active period tRFC */
    UINT8 tckmax;               /* 43 Max device cycle time tCKmax */
    UINT8 tdqsq;                /* 44 Max DQS to DQ skew (tDQSQ max) */
    UINT8 tqhs;                 /* 45 Max Read DataHold skew (tQHS) */
    UINT8 res_46;               /* 46 Reserved */
    UINT8 dimm_height;          /* 47 DDR SDRAM DIMM Height */
    UINT8 res_48_61[14];        /* 48-61 Reserved */
    UINT8 spd_rev;              /* 62 SPD Data Revision Code */
    UINT8 cksum;                /* 63 Checksum for bytes 0-62 */
    UINT8 mid[8];               /* 64-71 Mfr's JEDEC ID code per JEP-106 */
    UINT8 mloc;                 /* 72 Manufacturing Location */
    UINT8 mpart[18];            /* 73 Manufacturer's Part Number */
    UINT8 rev[2];               /* 91 Revision Code */
    UINT8 mdate[2];             /* 93 Manufacturing Date */
    UINT8 sernum[4];            /* 95 Assembly Serial Number */
    UINT8 mspec[27];            /* 99-127 Manufacturer Specific Data */
} DDR_SPD_DATA;

/* DDR2 DIMM SPD format */

typedef struct ddr2SpdData {
    UINT8 dataSize;             /*  0 # bytes written into serial memory */
    UINT8 chipSize;             /*  1 Total # bytes of SPD memory device */
    UINT8 memType;              /*  2 Fundamental memory type */
    UINT8 rowAddrNum;           /*  3 # of Row Addresses on this assembly */
    UINT8 colAddrNum;           /*  4 # of Column Addrs on this assembly */
    UINT8 rankNum;              /*  5 Number of DIMM Ranks */
    UINT8 dataWidth;            /*  6 Module Data Width */
    UINT8 res7;                 /*  7 Reserved */
    UINT8 volLevel;             /*  8 Voltage intf std of this assembly */
    UINT8 clkCycle;             /*  9 SDRAM Cycle time @ CL=X */
    UINT8 clkAccess;            /* 10 SDRAM Access from Clk @ CL=X (tAC) */
    UINT8 config;               /* 11 DIMM Configuration type */
    UINT8 refresh;              /* 12 Refresh Rate/Type */
    UINT8 primw;                /* 13 Primary SDRAM Width */
    UINT8 ecw;                  /* 14 Error Checking SDRAM width */
    UINT8 res_15;               /* 15 Reserved */
    UINT8 burstl;               /* 16 Burst Lengths Supported */
    UINT8 bankNum;              /* 17 # of Banks on Each SDRAM Device */
    UINT8 cas_lat;              /* 18 CAS# Latencies Supported */
    UINT8 mech_char;            /* 19 DIMM Mechanical Characteristics */
    UINT8 dimm_type;            /* 20 DIMM type information */
    UINT8 mod_attr;             /* 21 SDRAM Module Attributes */
    UINT8 dev_attr;             /* 22 SDRAM Device Attributes */
    UINT8 clk_cycle2;           /* 23 Min SDRAM Cycle time @ CL=X-1 */
    UINT8 clk_access2;          /* 24 SDRAM Access from Clk @ CL=X-1 (tAC) */
    UINT8 clk_cycle3;           /* 25 Min SDRAM Cycle time @ CL=X-2 */
    UINT8 clk_access3;          /* 26 Max Access from Clk @ CL=X-2 (tAC) */
    UINT8 trp;                  /* 27 Min Row Precharge Time (tRP)*/
    UINT8 trrd;                 /* 28 Min Row Active to Row Active (tRRD) */
    UINT8 trcd;                 /* 29 Min RAS to CAS Delay (tRCD) */
    UINT8 tras;                 /* 30 Minimum RAS Pulse Width (tRAS) */
    UINT8 rank_dens;            /* 31 Density of each rank on module */
    UINT8 ca_setup;             /* 32 Addr+Cmd Setup Time Before Clk (tIS) */
    UINT8 ca_hold;              /* 33 Addr+Cmd Hold Time After Clk (tIH) */
    UINT8 data_setup;           /* 34 Data Input Setup Time Before Strobe (tDS) */
    UINT8 data_hold;            /* 35 Data Input Hold Time After Strobe (tDH) */
    UINT8 twr;                  /* 36 Write Recovery time tWR */
    UINT8 twtr;                 /* 37 Int write to read delay tWTR */
    UINT8 trtp;                 /* 38 Int read to precharge delay tRTP */
    UINT8 mem_probe;            /* 39 Mem analysis probe characteristics */
    UINT8 trctrfc_ext;          /* 40 Extensions to trc and trfc */
    UINT8 trc;                  /* 41 Min Active to Auto refresh time tRC */
    UINT8 trfc;                 /* 42 Min Auto to Active period tRFC */
    UINT8 tckmax;               /* 43 Max device cycle time tCKmax */
    UINT8 tdqsq;                /* 44 Max DQS to DQ skew (tDQSQ max) */
    UINT8 tqhs;                 /* 45 Max Read DataHold skew (tQHS) */
    UINT8 pll_relock;           /* 46 PLL Relock time */
    UINT8 Tcasemax;             /* 47 Tcasemax */
    UINT8 psiTAdram;            /* 48 Thermal Resistance of DRAM Package from
                                      Top (Case) to Ambient (Psi T-A DRAM) */
    UINT8 dt0_mode;             /* 49 DRAM Case Temperature Rise from Ambient
                                      due to Activate-Precharge/Mode Bits
                                      (DT0/Mode Bits) */
    UINT8 dt2n_dt2q;            /* 50 DRAM Case Temperature Rise from Ambient
                                      due to Precharge/Quiet Standby
                                      (DT2N/DT2Q) */
    UINT8 dt2p;                 /* 51 DRAM Case Temperature Rise from Ambient
                                      due to Precharge Power-Down (DT2P) */
    UINT8 dt3n;                 /* 52 DRAM Case Temperature Rise from Ambient
                                      due to Active Standby (DT3N) */
    UINT8 dt3pfast;             /* 53 DRAM Case Temperature Rise from Ambient
                                      due to Active Power-Down with
                                      Fast PDN Exit (DT3Pfast) */
    UINT8 dt3pslow;             /* 54 DRAM Case Temperature Rise from Ambient
                                      due to Active Power-Down with Slow
                                      PDN Exit (DT3Pslow) */
    UINT8 dt4r_dt4r4w;          /* 55 DRAM Case Temperature Rise from Ambient
                                      due to Page Open Burst Read/DT4R4W
                                      Mode Bit (DT4R/DT4R4W Mode Bit) */
    UINT8 dt5b;                 /* 56 DRAM Case Temperature Rise from Ambient
                                      due to Burst Refresh (DT5B) */
    UINT8 dt7;                  /* 57 DRAM Case Temperature Rise from Ambient
                                      due to Bank Interleave Reads with
                                      Auto-Precharge (DT7) */
    UINT8 psiTApll;             /* 58 Thermal Resistance of PLL Package form
                                      Top (Case) to Ambient (Psi T-A PLL) */
    UINT8 psiTAreg;             /* 59 Thermal Reisitance of Register Package
                                      from Top (Case) to Ambient
                                      (Psi T-A Register) */
    UINT8 dtpllactive;          /* 60 PLL Case Temperature Rise from Ambient
                                      due to PLL Active (DT PLL Active) */
    UINT8 dtregact;             /* 61 Register Case Temperature Rise from
                                      Ambient due to Register Active/Mode Bit
                                      (DT Register Active/Mode Bit) */
    UINT8 spd_rev;              /* 62 SPD Data Revision Code */
    UINT8 cksum;                /* 63 Checksum for bytes 0-62 */
    UINT8 mid[8];               /* 64 Mfr's JEDEC ID code per JEP-106 */
    UINT8 mloc;                 /* 72 Manufacturing Location */
    UINT8 mpart[18];            /* 73 Manufacturer's Part Number */
    UINT8 rev[2];               /* 91 Revision Code */
    UINT8 mdate[2];             /* 93 Manufacturing Date */
    UINT8 sernum[4];            /* 95 Assembly Serial Number */
    UINT8 mspec[27];            /* 99-127 Manufacturer Specific Data */

} DDR2_SPD_DATA;

/* DDR3 DIMM SPD format */

typedef struct ddr3SpdData {

    /* General Section: Bytes 0-59 */

    UINT8 infoSizeCrc;          /*  0 # bytes written into serial memory,
                                      CRC coverage */
    UINT8 spdRev;               /*  1 Total # bytes of SPD mem device */
    UINT8 dramType;             /*  2 Key Byte / Fundamental mem type */
    UINT8 moduleType;           /*  3 Key Byte / Module Type */
    UINT8 densityBanks;         /*  4 SDRAM Density and Banks */
    UINT8 addressing;           /*  5 SDRAM Addressing */
    UINT8 moduleVol;            /*  6 Module nominal voltage, VDD */
    UINT8 moduleOrg;            /*  7 Module Organization */
    UINT8 busWidth;             /*  8 Module Memory Bus Width */
    UINT8 ftbDiv;               /*  9 Fine Timebase (FTB)
                                      Dividend / Divisor */
    UINT8 mtbDividend;          /* 10 Medium Timebase (MTB) Dividend */
    UINT8 mtbDivisor;           /* 11 Medium Timebase (MTB) Divisor */
    UINT8 tCKmin;               /* 12 SDRAM Minimum Cycle Time */
    UINT8 res13;                /* 13 Reserved */
    UINT8 casLatLsb;            /* 14 CAS Latencies Supported,
                                      Least Significant Byte */
    UINT8 casLatMsb;            /* 15 CAS Latencies Supported,
                                      Most Significant Byte */
    UINT8 tAAmin;               /* 16 Min CAS Latency Time */
    UINT8 tWRmin;               /* 17 Min Write REcovery Time */
    UINT8 tRCDmin;              /* 18 Min RAS# to CAS# Delay Time */
    UINT8 tRRDmin;              /* 19 Min Row Active to
                                      Row Active Delay Time */
    UINT8 tRPmin;               /* 20 Min Row Precharge Delay Time */
    UINT8 tRAStRCext;           /* 21 Upper Nibbles for tRAS and tRC */
    UINT8 tRASminLsb;           /* 22 Min Active to Precharge
                                      Delay Time */
    UINT8 tRCminLsb;            /* 23 Min Active to Active/Refresh
                                      Delay Time, LSB */
    UINT8 tRFCminLsb;           /* 24 Min Refresh Recovery Delay Time */
    UINT8 tRFCminMsb;           /* 25 Min Refresh Recovery Delay Time */
    UINT8 tWTRmin;              /* 26 Min Internal Write to
                                      Read Command Delay Time */
    UINT8 tRTPmin;              /* 27 Min Internal Read to Precharge
                                      Command Delay Time */
    UINT8 tFAWMsb;              /* 28 Upper Nibble for tFAW */
    UINT8 tFAWmin;              /* 29 Min Four Activate Window
                                      Delay Time*/
    UINT8 optFeatures;          /* 30 SDRAM Optional Features */
    UINT8 thermRefOpt;          /* 31 SDRAM Thermal and Refresh Opts */
    UINT8 thermalSensor;        /* 32 Module Thermal Sensor */
    UINT8 deviceType;           /* 33 SDRAM device type */
    INT8 fine_tCK_min;         /* 34 Fine offset for tCKmin */
    INT8 fine_tAA_min;         /* 35 Fine offset for tAAmin */
    INT8 fine_tRCD_min;        /* 36 Fine offset for tRCDmin */
    INT8 fine_tRP_min;         /* 37 Fine offset for tRPmin */
    INT8 fine_tRC_min;         /* 38 Fine offset for tRCmin */
    UINT8 res_39_59[21];        /* 39-59 Reserved, General Section */

    /* Module-Specific Section: Bytes 60-116 */

    union {
        struct {
            UINT8 moduleHeight;     /* 60 (Unbuffered) Module Nominal Height */
            UINT8 moduleThickness;  /* 61 (Unbuffered) Module Maximum Thickness */
            UINT8 refRawCard;       /* 62 (Unbuffered) Reference Raw Card Used */
            UINT8 addrMapping;      /* 63 (Unbuffered) Address Mapping from
                                          Edge Connector to DRAM */
            UINT8 res_64_116[53];   /* 64-116 (Unbuffered) Reserved */
        } unbuffered;
        struct {
            UINT8 moduleHeight;     /* 60 (Registered) Module Nominal Height */
            UINT8 moduleThickness;  /* 61 (Registered) Module Maximum Thickness */
            UINT8 refRawCard;       /* 62 (Registered) Reference Raw Card Used */
            UINT8 moduleAttributes; /* 63 DIMM Module Attributes */
            UINT8 thermal;          /* 64 RDIMM Thermal Heat Spreader Solution */
            UINT8 regIdLo;          /* 65 Register Manufacturer ID Code, Least Significant Byte */
            UINT8 regIdHi;          /* 66 Register Manufacturer ID Code, Most Significant Byte */
            UINT8 regRev;           /* 67 Register Revision Number */
            UINT8 regType;          /* 68 Register Type */
            UINT8 rcw[8];           /* 69-76 RC1,3,5...15 (MS Nibble) / RC0,2,4...14 (LS Nibble) */
        } registered;
        UINT8 uc[57];               /* 60-116 Module-Specific Section */
    } modSection;

    /* Unique Module ID: Bytes 117-125 */

    UINT8 midLsb;                   /* 117 Module MfgID Code LSB - JEP-106 */
    UINT8 midMsb;                   /* 118 Module MfgID Code MSB - JEP-106 */
    UINT8 mLocation;                /* 119 Mfg Location */
    UINT8 mDate[2];                 /* 120-121 Mfg Date */
    UINT8 serialNum[4];             /* 122-125 Module Serial Number */

    /* CRC: Bytes 126-127 */

    UINT8 crc[2];                   /* 126-127 SPD CRC */

    /* Other Manufacturer Fields and User Space: Bytes 128-255 */

    UINT8 mPartNum[18];             /* 128-145 Mfg's Module Part Number */
    UINT8 mRev[2];                  /* 146-147 Module Revision Code */
    UINT8 dMidLsb;                  /* 148 DRAM MfgID Code LSB - JEP-106 */
    UINT8 dMidMsb;                  /* 149 DRAM MfgID Code MSB - JEP-106 */
    UINT8 mSpecificData[26];        /* 150-175 Mfg's Specific Data */
    UINT8 customerUse[80];          /* 176-255 Open for Customer Use */
} DDR3_SPD_DATA;

#ifdef __cplusplus
    }
#endif /* __cplusplus */

#endif /* __INCspdLibh */
