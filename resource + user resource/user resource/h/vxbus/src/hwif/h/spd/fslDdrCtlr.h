/* fslDdrCtlr.h - Freescale DDR controller header file */

/*
 * Copyright (c) 2013, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
16jul14,xms  add definition for T1040 model.
15may14,wyt  add definition for T2080 model.
01f,14mar13,h_k  changed DDR_DATA_INIT_VALUE to 0 for a cosmetic purpose.
01e,11mar13,syt  updated the struct ddrHwconf and add two macros.
01d,04mar13,syt  add DDR controller Errata A-004934 and A-004390.
01c,26feb13,syt  code clean up. 
01b,18feb13,syt  optimize the struct of fslDdrCtlrDrv and clean
                 the codes.
01a,27jan13,syt  written.
*/

#ifndef __INCfslDdrCtlrh
#define __INCfslDdrCtlrh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

/* Rev 1 silicon errata for T4240 and T4160, to be fixed in Rev 2.0 silicon */

/*
 * Create the workaround for errata DDR A004390:
 * DDR violates tXPR for higher density DRAMs on the Rev 1.x.
 *
 * Workaround:
 * The following should be written before setting DDR_SDRAM_CFG[MEM_EN] and
 * after writing all other DDR configuration registers.
 *  1. Write DDR_SDRAM_MD_CNTL[CKE_CNTL] to a value of 2'b10.
 *  2. Wait at least tXPR for the memory used. Set DDR_SDRAM_CFG[MEM_EN].
 *  3. DDR_SDRAM_MD_CNTL[CKE_CNTL] may be cleared to allow hardware to
 *     automatically control the MCKE pins.
 *
 * BSP needs to define FSL_ERRATA_DDR_A004390 macro to apply the workaround.
 */

#define T4_ERRATUM_DDR4390              0x00010000

/*
 * Create the workaround for errata DDR A004934:
 * DDR controller may calibrate to non-optimal operating point on the Rev 1.x.
 *
 * Workaround:
 * Before enabling the DDR controller, set the DDR memory mapped register space
 * at offset 0xF70 to a value of 0x30003000.
 *
 * BSP needs to define FSL_ERRATA_DDR_A004934 macro to apply the workaround.
 */

#define T4_ERRATUM_DDR4934              0x00020000

/* DDR Registers Map */

#define CS0_BNDS                        0x000
#define CS1_BNDS                        0x008
#define CS2_BNDS                        0x010
#define CS3_BNDS                        0x018
#define CS0_CONFIG                      0x080
#define CS1_CONFIG                      0x084
#define CS2_CONFIG                      0x088
#define CS3_CONFIG                      0x08C
#define CS0_CONFIG_2                    0x0C0
#define CS1_CONFIG_2                    0x0C4
#define CS2_CONFIG_2                    0x0C8
#define CS3_CONFIG_2                    0x0CC
#define TIMING_CFG_3                    0x100
#define TIMING_CFG_0                    0x104
#define TIMING_CFG_1                    0x108
#define TIMING_CFG_2                    0x10C
#define DDR_SDRAM_CFG                   0x110
#define DDR_SDRAM_CFG_2                 0x114
#define DDR_SDRAM_MODE_CFG              0x118
#define DDR_SDRAM_MODE_CFG_2            0x11c
#define DDR_SDRAM_MD_CNTL               0x120
#define DDR_SDRAM_INTERVAL              0x124
#define DDR_DATA_INIT                   0x128
#define DDR_SDRAM_CLK_CTRL              0x130
#define DDR_INIT_ADDR                   0x148
#define DDR_INIT_EXT_ADDR               0x14c
#define TIMING_CFG_4                    0x160
#define TIMING_CFG_5                    0x164
#define DDR_ZQ_CNTL                     0x170
#define DDR_WRLVL_CNTL                  0x174
#define DDR_SR_CNTR                     0x17C
#define DDR_SDRAM_RCW_1                 0x180
#define DDR_SDRAM_RCW_2                 0x184
#define DDR_WRLVL_CNTL_2                0x190
#define DDR_WRLVL_CNTL_3                0x194
#define DDRx_DDR_SDRAM_MODE_3           0x200
#define DDRx_DDR_SDRAM_MODE_4           0x204
#define DDRx_DDR_SDRAM_MODE_5           0x208
#define DDRx_DDR_SDRAM_MODE_6           0x20C
#define DDRx_DDR_SDRAM_MODE_7           0x210
#define DDRx_DDR_SDRAM_MODE_8           0x214

#define DDRCDR_1                        0xB28
#define DDRCDR_2                        0xB2C
#define DDR_IP_REV1                     0xBF8
#define DDR_IP_REV2                     0xBFC
#define DDR_DATA_ERR_INJECT_HI          0xe00
#define DDR_DATA_ERR_INJECT_LO          0xe04
#define DDR_ECC_ERR_INJECT              0xe08
#define DDR_CAPTURE_DATA_HI             0xe20
#define DDR_CAPTURE_DATA_LO             0xe24
#define DDR_CAPTURE_ECC                 0xe28
#define DDR_ERR_DETECT                  0xe40
#define DDR_ERR_DISABLE                 0xe44
#define DDR_ERR_INT_EN                  0xe48
#define DDR_CAPTURE_ATTRIBUTES          0xe4c
#define DDR_CAPTURE_ADDRESS             0xe50
#define DDR_ERR_SBE                     0xe58

#define DDR_ODT_NEVER                   0x0
#define DDR_ODT_CS                      0x1
#define DDR_ODT_ALL_OTHER_CS            0x2
#define DDR_ODT_OTHER_DIMM              0x3
#define DDR_ODT_ALL                     0x4
#define DDR_ODT_SAME_DIMM               0x5
#define DDR_ODT_CS_AND_OTHER_DIMM       0x6
#define DDR_ODT_OTHER_CS_ONSAMEDIMM     0x7

/* CS bank(chip select) interleaving mode */

#define DDR_CS0_CS1                     0x40
#define DDR_CS2_CS3                     0x20
#define DDR_CS0_CS1_AND_CS2_CS3         (DDR_CS0_CS1 | DDR_CS2_CS3)
#define DDR_CS0_CS1_CS2_CS3             (DDR_CS0_CS1_AND_CS2_CS3 | 0x04)

/* DDR SDRAM interval configuration */

#define DDR_CACHE_LINE_INTERLEAVING     0x0
#define DDR_PAGE_INTERLEAVING           0x1
#define DDR_BANK_INTERLEAVING           0x2
#define DDR_SUPERBANK_INTERLEAVING      0x3
#define DDR_3WAY_1KB_INTERLEAVING       0xA
#define DDR_3WAY_4KB_INTERLEAVING       0xC
#define DDR_3WAY_8KB_INTERLEAVING       0xD
#define DDR_4WAY_1KB_INTERLEAVING       0x1A
#define DDR_4WAY_4KB_INTERLEAVING       0x1C
#define DDR_4WAY_8KB_INTERLEAVING       0x1D

/* DDR SDRAM control configuration register bit defines */

#define SDRAM_CFG_MEM_EN                0x80000000
#define SDRAM_CFG_SREN                  0x40000000
#define SDRAM_CFG_ECC_EN                0x20000000
#define SDRAM_CFG_RD_EN                 0x10000000
#define SDRAM_CFG_SDRAM_TYPE_DDR1       0x02000000
#define SDRAM_CFG_SDRAM_TYPE_DDR2       0x03000000
#define SDRAM_CFG_SDRAM_TYPE_MASK       0x07000000
#define SDRAM_CFG_SDRAM_TYPE_SHIFT      24
#define SDRAM_CFG_DYN_PWR               0x00200000
#define SDRAM_CFG_DBW_MASK              0x00180000
#define SDRAM_CFG_DBW_SHIFT             19
#define SDRAM_CFG_32_BE                 0x00080000
#define SDRAM_CFG_16_BE                 0x00100000
#define SDRAM_CFG_8_BE                  0x00040000
#define SDRAM_CFG_NCAP                  0x00020000
#define SDRAM_CFG_2T_EN                 0x00008000
#define SDRAM_CFG_BI                    0x00000001
#define FSL_DDR3_SDRAM                  7

/* DDR SDRAM control configuration 2 register bit defines */

#define SDRAM_CFG2_D_INIT               0x00000010
#define SDRAM_CFG2_ODT_CFG_MASK         0x00600000
#define SDRAM_CFG2_ODT_NEVER            0
#define SDRAM_CFG2_ODT_ONLY_WRITE       1
#define SDRAM_CFG2_ODT_ONLY_READ        2
#define SDRAM_CFG2_ODT_ALWAYS           3

#define TIMING_CFG_2_CPO_MASK           0x0F800000

/* DDR SDRAM mode configuration registers bit defines */

#define MD_CNTL_MD_EN                   0x80000000
#define MD_CNTL_CS_SEL_CS0              0x00000000
#define MD_CNTL_CS_SEL_CS1              0x10000000
#define MD_CNTL_CS_SEL_CS2              0x20000000
#define MD_CNTL_CS_SEL_CS3              0x30000000
#define MD_CNTL_CS_SEL_CS0_CS1          0x40000000
#define MD_CNTL_CS_SEL_CS2_CS3          0x50000000
#define MD_CNTL_MD_SEL_MR               0x00000000
#define MD_CNTL_MD_SEL_EMR              0x01000000
#define MD_CNTL_MD_SEL_EMR2             0x02000000
#define MD_CNTL_MD_SEL_EMR3             0x03000000
#define MD_CNTL_SET_REF                 0x00800000
#define MD_CNTL_SET_PRE                 0x00400000
#define MD_CNTL_CKE_CNTL_LOW            0x00100000
#define MD_CNTL_CKE_CNTL_HIGH           0x00200000
#define MD_CNTL_WRCW                    0x00080000
#define MD_CNTL_MD_VALUE(x)             (x & 0x0000FFFF)

/* DDR Control Driver Register 1 bit defines */

#define DDR_CDR1_DHC_EN                 0x80000000
#define DDR_CDR1_ODT_SHIFT              17
#define DDR_CDR1_ODT_MASK               0x6
#define DDR_CDR2_ODT_MASK               0x1
#define DDR_CDR1_ODT(x)                 ((x & DDR_CDR1_ODT_MASK) << DDR_CDR1_ODT_SHIFT)
#define DDR_CDR2_ODT(x)                 (x & DDR_CDR2_ODT_MASK)

#define DDR_CDR_ODT_OFF                 0x0
#define DDR_CDR_ODT_120ohm              0x1
#define DDR_CDR_ODT_180ohm              0x2
#define DDR_CDR_ODT_75ohm               0x3
#define DDR_CDR_ODT_110ohm              0x4
#define DDR_CDR_ODT_60hm                0x5
#define DDR_CDR_ODT_70ohm               0x6
#define DDR_CDR_ODT_47ohm               0x7

#define DDR3_RTT_OFF                    0
#define DDR3_RTT_60_OHM                 1 /* RTT_Nom = RZQ/4 */
#define DDR3_RTT_120_OHM                2 /* RTT_Nom = RZQ/2 */
#define DDR3_RTT_40_OHM                 3 /* RTT_Nom = RZQ/6 */
#define DDR3_RTT_20_OHM                 4 /* RTT_Nom = RZQ/12 */
#define DDR3_RTT_30_OHM                 5 /* RTT_Nom = RZQ/8 */

#define DDR2_RTT_OFF                    0
#define DDR2_RTT_75_OHM                 1
#define DDR2_RTT_150_OHM                2
#define DDR2_RTT_50_OHM                 3

#define DDR_BL4                         4
#define DDR_BC4                         DDR_BL4
#define DDR_OTF                         6
#define DDR_BL8                         8

#define DDR_DATA_INIT_VALUE             0x00000000

/* define bank(chip select) interleaving mode */

#define BANK0_BANK1_INTLV               0x40
#define BANK2_BANK3_INTLV               0x20
#define BANK0_BANK1_AND_BANK2_BANK3     (BANK0_BANK1_INTLV | BANK2_BANK3_INTLV)
#define BANK0_BANK1_BANK2_BANK3         (BANK0_BANK1_AND_BANK2_BANK3 | 0x04)
#define BANK_INT_LVL_MASK               BANK0_BANK1_BANK2_BANK3

#define SOC_MODEL_CHECK                 (*((volatile UINT32 *)T4_SVR) & 0xfff00000)
#define FSL_T4_MODEL                    0x82400000
#define FSL_T2080_MODEL                 0x85300000
#define FSL_T1040_MODEL                 0x85200000

#define SOC_VERSION_CHECK               (*((volatile UINT32 *)T4_SVR) & 0x000000ff)
#define SOC_VER_20                      0x20

/* DDR slot rank signal lines configuration defines */

#define CS0                             0x01
#define CS1                             0x02
#define CS2                             0x04
#define CS3                             0x08
#define CS0_CS1                         0x03
#define CS2_CS3                         0x0c
#define CS0_CS1_CS2_CS3                 0x0f


/* Default DDR controler number on SOC */

#define MAX_DDR_CTLR_NUM                3
#define DDR_CTLR_DRV_SIZE               0x1000

#define DDR_IP_VER_47                   0x47
#define DDR_IP_VER(x)                   (((x & 0x0f00) >> 4) | (x & 0x000f))

/* DDR Controller ID */

#define TGTIF_DDRSDRAM                  0x10
#define TGTIF_DDRSDRAM2                 0x11
#define TGTIF_DDRSDRAM3                 0x12
#define T4_LAW_NUM                      32

#define CS_NUM_PRE_CTLR                 4

#define DDR_ECC_ENABLE                  0x01
#define DDR_ECC_DISABLE                 0x00
#define DDR_FIX_PARAMS_ENABLE           0x02
#define DDR_FIX_PARAMS_DISABLE          0x00

/* type defines */

typedef UINT32 (*BUS_FREQ_GET) (void);

typedef struct odtConfigParams {
    UINT32  odtRdCfg;
    UINT32  odtWrCfg;
    UINT32  odtRttNorm;
    UINT32  odtRttWr;
} ODT_CONFIG_PARAMS;

/* DDR controller CS optional register struct */

typedef struct ddrCsOptParams {
    UINT32  autoPrecharge;
    UINT32  odtRdCfg;
    UINT32  odtWrCfg;
    UINT32  odtRttNorn;
    UINT32  odtRttWr;
} DDR_CS_OPT_PARAMS;

/* DRAM DDR controller hardware confiuration struct */

typedef struct fslDdrCtlrParams {

    UINT32  slotNumPreDdrCtlr;  /* Slot number pre DDR controller */

    /*
     * Memory organization parameters
     *
     * if DIMM is present in the system
     * where DIMMs are with respect to chip select
     * where chip selects are with respect to memory boundaries
     */

    UINT32  registeredDimm;     /* Use registered DIMM support */
    UINT32  ipRev;              /* DDr controller IP version */
    UINT32  unqMrsEn;           /* Unique MRS Enable */

    /* Options local to a Chip Select */

    DDR_CS_OPT_PARAMS   csOptParams[CS_NUM_PRE_CTLR];

    /* Special configurations for chip select */

    UINT32  intLvEn;            /* Memory controller interleave enable */
    UINT32  intLvMode;          /* Interleaving control */
    UINT32  baIntlvCtl;         /* Bank (chip select) interleaving control */
    UINT32  addrHash;           /* Address Hash enable */

    /* Operational mode parameters */

    UINT32  eccMode;            /* Use ECC mode */
    UINT32  dramDataInit;       /* Use hardware DDR data initialization */
    UINT32  dqsConfig;          /* Use DQS maybe only with DDR2 */
    UINT32  selfRefreshEnable;  /* SREN - self-refresh during sleep */
    UINT32  dynamicPower;       /* DYN_PWR */

    UINT32  dataBusWidth;       /* Memory data width (16-bit, 32-bit, 64-bit) */
    UINT32  burstLength;        /* BL4, OTF and BL8 */
    UINT32  otfBurstChop;       /* On-The-Fly Burst Chop enable */

    UINT32  mirroredDimm;       /* Mirrior DIMMs for DDR3 */
    UINT32  quadRankPresent;    /* Quad rank DIMM */
    UINT32  apEnable;           /* Address parity enable for RDIMM */

    /* Global Timing Parameters */

    UINT32  ddrFreq;            /* MHz unit */
    UINT32  ddrClockCycle;      /* Picosecond unit */
    UINT32  casLatencyOverride; /* CAS_latency valuue override enable */
    UINT32  casLatencyOverrideValue;    /* CAS_latency override valuue */
    UINT32  casLatency;         /* CAS_latency value */
    UINT32  additiveLatencyOverride;
    UINT32  additiveLatencyOverrideValue;
    UINT32  additiveLatency;    /*  */
    UINT32  clkAdjust;          /* Clock adjust */
    UINT32  cpo;
    UINT32  wrDataDelay;        /* DQS adjust */
    UINT32  wrlvlOverride;
    UINT32  wrlvlSample;        /* Write leveling */
    UINT32  wrlvlStart;         /* Write leveling start time for DQS[0] */
    UINT32  wrlvlCntl2;         /* DDR write leveling control 2 */
    UINT32  wrlvlCntl3;         /* DDR write leveling control 3 */
    UINT32  halfStrengthEnable; /* Global half-strength override */
    UINT32  enable2T;           /* Enable 2T timing */
    UINT32  enable3T;           /* Enable 3T timing */
    UINT32  bstopre;            /* Precharge interval */
    UINT32  tCKEps;             /* tCKE */
    UINT32  tFAWps;             /* tFAW */

    UINT32  rttOverride;        /* rtt override enable */
    UINT32  rttOverrideValue;   /* that is Rtt_Nom for DDR3 */
    UINT32  rttWrOverrideValue; /* this is Rtt_WR for DDR3 */

    UINT32  autoSelfRefreshEn;  /* Automatic self refresh */
    UINT32  srIt;               /* Self Refresh Idle Threshold */
    UINT32  zqEnable;           /* ZQ calibration enable */
    UINT32  wrlvlEnable;        /* Write leveling enable */

    UINT32  rcwOverride;        /* RCW override for RDIMM */
    UINT32  rcw1;               /* RCW1 */
    UINT32  rcw2;               /* RCW2 */

    /* control register */

    UINT32  ddrCdr1;            /* DDR Control Driver Register 1 value */
    UINT32  ddrCdr2;            /* DDR Control Driver Register 2 value */
    UINT32  trwtOverride;       /* read-to-write turnaround override */
    UINT32  trwt;               /* read-to-write turnaround */
} FSL_DDR_CTLR_PARAMS;

/* DRAM DIMM slot hardware confiuration struct */

typedef struct dimmSlotHwconf {
    UINT32  i2cRegBase;         /* I2C controller bass address */
    UINT32  slotDevAddr;        /* DIMM slot I2C device address */
    UINT32  slotRankCfg;        /* Slot rank configuration */
} DIMM_SLOT_CONF;

/* The importent DDR controller registers struct */

typedef struct  ddrReg
    {
    struct {
        UINT32  csBnds;         /* Chip select n memory bounds */
        UINT32  csConfig;       /* Chip select n configuration */
        UINT32  csConfig2;      /* Chip select n configuration 2 */
    } cs[CS_NUM_PRE_CTLR];
    UINT32  timingCfg3;         /* DDR SDRAM timing configuration 3 */
    UINT32  timingCfg0;         /* DDR SDRAM timing configuration 0 */
    UINT32  timingCfg1;         /* DDR SDRAM timing configuration 1 */
    UINT32  timingCfg2;         /* DDR SDRAM timing configuration 2 */
    UINT32  ddrSdramCfg;        /* DDR SDRAM control configuration */
    UINT32  ddrSdramCfg2;       /* DDR SDRAM control configuration 2 */
    UINT32  ddrSdramMode;       /* DDR SDRAM mode configuration */
    UINT32  ddrSdramMode2;      /* DDR SDRAM mode configuration 2 */
    UINT32  ddrSdramMode3;      /* DDR SDRAM mode configuration 3 */
    UINT32  ddrSdramMode4;      /* DDR SDRAM mode configuration 4 */
    UINT32  ddrSdramMode5;      /* DDR SDRAM mode configuration 5 */
    UINT32  ddrSdramMode6;      /* DDR SDRAM mode configuration 6 */
    UINT32  ddrSdramMode7;      /* DDR SDRAM mode configuration 7 */
    UINT32  ddrSdramMode8;      /* DDR SDRAM mode configuration 8 */
    UINT32  ddrSdramMdCntl;     /* DDR SDRAM mode control */
    UINT32  ddrSdramInterval;   /* DDR SDRAM interval configuration */
    UINT32  ddrDataInit;        /* DDR SDRAM data initialization */
    UINT32  ddrSdramClkCntl;    /* DDR SDRAM clock control */
    UINT32  ddrInitAddr;        /* DDR training initialization address */
    UINT32  ddrInitExtAddr;     /* DDR training initialization extended address */
    UINT32  timingCfg4;         /* DDR SDRAM timing configuration 4 */
    UINT32  timingCfg5;         /* DDR SDRAM timing configuration 5 */
    UINT32  ddrZqCntl;          /* DDR ZQ calibration control */
    UINT32  ddrWrlvlCntl;       /* DDR write leveling control */
    UINT32  ddrWrlvlCntl2;      /* DDR write leveling control 2 */
    UINT32  ddrWrlvlCntl3;      /* DDR write leveling control 3 */
    UINT32  ddrSrCntr;          /* DDR Self Refresh Counter register */
    UINT32  ddrSdramRcw1;       /* DDR Register Control Words 1 */
    UINT32  ddrSdramRcw2;       /* DDR Register Control Words 2 */
    UINT32  ddrEor;             /* DDR Enhanced Optimization Register */
    UINT32  ddrCdr1;            /* DDR Control Driver Register 1 */
    UINT32  ddrCdr2;            /* DDR Control Driver Register 2 */
    UINT32  errDisable;         /* Memory error disable */
    UINT32  errIntEn;           /* Memory error interrupt enable */
    } FSL_DDR_REG;

/* DDR controller configurations struct */

typedef struct ddrHwconf {
    UINT64  regBase;            /* DDR controller base */
    UINT64  regBase1;           /* Other related controller (LAW) base */
    UINT64  memBase;            /* DDR DIMM start address in system address map */
    UINT64  memSize;            /* DDR DIMM memory size in system address map */
    DIMM_SLOT_CONF *    pDimmSlotHwconf;/* DIMM slot hardware configuration info */
    UINT32              slotNumPreCtlr; /* DIMM slot number on pre DDR controller */
    BUS_FREQ_GET        ddrBusFreqGet;  /* DDR bus frequncy get function pointer */
    BUS_FREQ_GET        i2cBusFreqGet;  /* I2C bus frequncy get function pointer */
    FSL_DDR_REG *       fixRegParams;   /* Entry of DDR controller register value table */
    UINT32              ddrUsrCfg;      /* user configuration paramters for DDR CTLR */
} DDR_HWCONF;

/* SOC DDR configuration table */

typedef struct sysDdrConf {
    UINT32          ddrCtlrNum;     /* DDR controller number number */
    DDR_HWCONF *    pDdrHwconf;     /* DDR controller configurations */
} SYS_MEM_HWCONF;

/* DDR controller device relatred infomations struct */

typedef struct  ddrDev {
    DDR_HWCONF *        pDdrHwconf;
    UINT8               dimmSpdData[MAX_SLOT_NUM_PRE_CTLR][MAX_SPD_BYTE];
    DIMM_PARAMS         dimmParams[MAX_SLOT_NUM_PRE_CTLR];
    DIMM_COMN_PARAMS    dimmComnParams;
    UINT32              ddrDevIndex;
    UINT32              dimmSlotNum;
    UINT32              i2cBusClkFreq;
    UINT32              ddrBusClkFreq;
    UINT64              memBase;
    UINT64              memSize;
} DDR_DEV;

/* Chip select n memory bounds format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  sa:16;          /* Starting address for chip select (bank) */
        UINT32  ea:16;          /* Ending address for chip select (bank) */
        } field;
    } DDRx_CS_BNDS;

/* Chip select n configuration format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  csEn:1;         /* Chip select n enable */
        UINT32  :1;             /* Reserved field */
        UINT32  intlvEn:1;      /* Memory controller interleave enable */
        UINT32  :1;             /* Reserved field */
        UINT32  intlvCtl:4;     /* Interleaving control */
        UINT32  apEn:1;         /* Chip select n auto-precharge enable */
        UINT32  odtRdCfg:3;     /* ODT for reads configuration */
        UINT32  :1;             /* Reserved field */
        UINT32  odtWrCfg:3;     /* ODT for writes configuration */
        UINT32  baBitsCs:2;     /* Number of bank bits for SDRAM on CS */
        UINT32  :3;             /* Reserved field */
        UINT32  rowBitsCs:3;    /* Number of row bits for SDRAM on CS */
        UINT32  :5;             /* Reserved field */
        UINT32  colBitsCs:3;    /* Number of column bits for SDRAM on CS */
        } field;
    } DDR_CS_CONFIG;

/* DDR SDRAM timing configuration 0 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  rwt:2;          /* Read-to-write turnaround (tRTW) */
        UINT32  wrt:2;          /* Write-to-read turnaround */
        UINT32  rrt:2;          /* Read-to-read turnaround */
        UINT32  wwt:2;          /* Write-to-write turnaround */
        UINT32  actPdExit:4;    /* Active powerdown exit timing (tXARD and tXARDS) */
        UINT32  prePdExit:4;    /* Precharge powerdown exit timing (tXP) */
        UINT32  extPrePdExit:2; /* Extended precharge powerdown exit timing (tXP) */
        UINT32  :5;             /* Reserved field */
        UINT32  taxPd:1;
        UINT32  :3;             /* Reserved field */
        UINT32  mrsCyc:5;       /* Mode register set cycle time (tMRD, tMOD) */
        } field;
    } DDR_TIMING_CFG0;

/* DDR SDRAM timing configuration 1 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  preToAct:4;     /* Precharge-to-activate interval (tRP) */
        UINT32  actToPre:4;     /* Activate to precharge interval (tRAS) */
        UINT32  actToRw:4;      /* Activate to read/write interval for SDRAM (tRCD) */
        UINT32  casLst:3;       /* MCAS latency from READ command */
        UINT32  :1;             /* Reserved field */
        UINT32  refRec:4;       /* Refresh recovery time (tRFC) */
        UINT32  wrRec:4;        /* Last data to precharge minimum interval (tWR). */
        UINT32  actToAct:4;     /* Activate-to-activate interval (tRRD) */
        UINT32  wrToRd:4;       /* Last write data pair to read command issue interval (tWTR) */
        } field;
    } DDR_TIMING_CFG1;

/* DDR SDRAM timing configuration 2 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  addLat:4;       /* Additive latency */
        UINT32  :4;             /* Reserved field */
        UINT32  cpo:1;          /* cpo bit */
        UINT32  wrLat:4;        /* Write latency. */
        UINT32  :2;             /* Reserved field */
        UINT32  rdToPre:4;      /* Read to precharge (tRTP) */
        UINT32  wrDataDelay:4;  /* Write command to write data strobe timing adjustment */
        UINT32  ckePls:3;       /* Minimum CKE pulse width (tCKE) */
        UINT32  fourAct:6;      /* Window for four activates (tFAW) */
        } field;
    } DDR_TIMING_CFG2;

/* DDR SDRAM timing configuration 3 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  :3;             /* Reserved field */
        UINT32  extPreToAct:1;  /* Extended precharge-to-activate interval (tRP) */
        UINT32  :2;             /* Reserved field */
        UINT32  extActToPre:2;  /* Extended Activate to precharge interval (tRAS ) */
        UINT32  :1;             /* Reserved field */
        UINT32  extActToRw:1;   /* Extended activate to read/write interval for SDRAM (tRCD) */
        UINT32  :1;             /* Reserved field */
        UINT32  extRefRec:5;    /* Extended refresh recovery time ( tRFC) */
        UINT32  :2;             /* Reserved field */
        UINT32  extCasLat:2;    /* Extended MCAS latency from READ command */
        UINT32  :1;             /* Reserved field */
        UINT32  ExtAddLat:1;    /* Extended Additive Latency */
        UINT32  :1;             /* Reserved field */
        UINT32  extWrRec:1;     /* Extended last data to precharge minimum interval (tWR) */
        UINT32  :5;             /* Reserved field */
        UINT32  cntlAdj:3;      /* Control Adjust */
        } field;
    } DDR_TIMING_CFG3;

/* DDR SDRAM timing configuration 4 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  rwt:4;          /* Read-to-write turnaround for same chip select */
        UINT32  wrt:4;          /* Write-to-read turnaround for same chip select */
        UINT32  rrt:4;          /* Read-to-read turnaround for same chip select */
        UINT32  wwt:4;          /* Write-to-write turnaround for same chip select */
        UINT32  extRwt:2;       /* Extended read-to-write turnaround (tRTW) */
        UINT32  :1;             /* Reserved field */
        UINT32  extWrt:1;       /* Extended write-to-read turnaround */
        UINT32  :1;             /* Reserved field */
        UINT32  extRrt:1;       /* Extended read-to-read turnaround */
        UINT32  :1;             /* Reserved field */
        UINT32  extWwt:1;       /* Extended write-to-write turnaround */
        UINT32  :3;             /* Reserved field */
        UINT32  extRefInt:1;    /* Refresh interval */
        UINT32  :2;             /* Reserved field */
        UINT32  dllLock:2;      /* DDR SDRAM DLL Lock Time */
        } field;
    } DDR_TIMING_CFG4;

/* DDR SDRAM timing configuration 5 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  :3;             /* Reserved field */
        UINT32  rOdtOn:5;       /* Read to ODT on */
        UINT32  :1;             /* Reserved field */
        UINT32  rOdtOff:3;      /* Read to ODT off */
        UINT32  :3;             /* Reserved field */
        UINT32  wOdtOn:5;       /* Write to ODT On Specifies the number of cycles */
        UINT32  :1;             /* Reserved field */
        UINT32  wOdtOff:3;      /* Write to ODT Off */
        UINT32  :8;             /* Reserved field */
        } field;
    } DDR_TIMING_CFG5;

/* DDR SDRAM control configuration format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  memEn:1;        /* DDR SDRAM interface logic enable */
        UINT32  seEn:1;         /* Self refresh enable (during sleep) */
        UINT32  eccEn:1;        /* ECC enable */
        UINT32  rdEn:1;         /* Registered DIMM enable */
        UINT32  :1;             /* Reserved field */
        UINT32  sdramType:3;    /* Type of SDRAM device to be used */
        UINT32  :2;             /* Reserved field */
        UINT32  dynPwr:1;       /* Dynamic power management mode */
        UINT32  dbw:2;          /* DRAM data bus width */
        UINT32  en8B:1;         /* 8-beat burst enable */
        UINT32  :1;             /* Reserved field */
        UINT32  en3T:1;         /* Enable 3T timing */
        UINT32  en2T:1;         /* Enable 2T timing */
        UINT32  baIntlvCtl:7;   /* Bank (chip select) interleaving control */
        UINT32  :4;             /* Reserved field */
        UINT32  hse:1;          /* Global half-strength override */
        UINT32  :1;             /* Reserved field */
        UINT32  memHalt:1;      /* DDR memory controller halt */
        UINT32  bi:1;           /* Bypass initialization */
        } field;
    } DDRX_DDR_SDRAM_CFG;

/* DDR SDRAM control configuration 2 format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  frcSr:1;        /* Force self refresh */
        UINT32  srIe:1;         /* Self-refresh interrupt enable */
        UINT32  dllRstDis:1;
        UINT32  :2;             /* Reserved field */
        UINT32  dqsCfg:1;
        UINT32  :3;             /* Reserved field */
        UINT32  odtCfg:2;       /* ODT configuration */
        UINT32  :5;             /* Reserved field */
        UINT32  numPr:4;        /* Number of posted refreshes */
        UINT32  ddrSlow:1;      /* DDR Slow Frequency */
        UINT32  x4En:1;         /* x4 DRAM enable */
        UINT32  qdEn:1;         /* Quad-rank enable */
        UINT32  unqMrsEn:1;     /* Unique MRS Enable */
        UINT32  :1;             /* Reserved field */
        UINT32  obcCfg:1;       /* On-The-Fly Burst Chop Configuration */
        UINT32  apEn:1;         /* Address Parity Enable */
        UINT32  dInit:1;        /* DRAM data initialization */
        UINT32  :1;             /* Reserved field */
        UINT32  rcwEn:1;        /* Register Control Word Enable */
        UINT32  cdDis:1;        /* Corrupted Data Disable */
        UINT32  mdEn:1;      /* Mirrored DIMM Enable */
        } field;
    } DDRX_DDR_SDRAM_CFG2;

/* DDR ZQ calibration control register format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  zqEn:1;         /* ZQ Calibration Enable */
        UINT32  :3;             /* Reserved field */
        UINT32  zqInit:4;       /* POR ZQ Calibration Time (tZQinit) */
        UINT32  :4;             /* Reserved field */
        UINT32  zqOper:4;       /* Normal Operation Full Calibration Time (tZQoper) */
        UINT32  :4;             /* Reserved field */
        UINT32  zqcs:4;         /* Normal Operation Short Calibration Time (tZQCS) */
        UINT32  :8;             /* Reserved field */
        } field;
    } DDRx_DDR_ZQ_CNTL;

/* DDR write leveling control register format */

typedef union
    {
    UINT32 regVal;
    struct                      /* Bit field description */
        {
        UINT32  wrlvlEn:1;      /* Write Leveling Enable */
        UINT32  :4;             /* Reserved field */
        UINT32  wrlvlMrd:3;     /* tWL_MRD */
        UINT32  :1;             /* Reserved field */
        UINT32  wrlvlOdtEn:3;   /* tWL_ODTEN */
        UINT32  :1;             /* Reserved field */
        UINT32  wrlvlDqsEn:3;   /* tWL_DQSEN */
        UINT32  wrlvlSmpl:4;    /* Write leveling sample time */
        UINT32  :1;             /* Reserved field */
        UINT32  wrlvlWlr:3;     /* Write leveling repetition time */
        UINT32  :3;             /* Reserved field */
        UINT32  wrlvlStart:5;   /* Write leveling start time for DQS[0] */
        } field;
    } DDRx_DDR_WRLVL_CNTL;

/* DDR Register Control Words 1 register format */

typedef union
    {
    UINT32  regVal;
    struct
        {
        UINT32  rcw0:4;         /* Register Control Word 0 */
        UINT32  rcw1:4;         /* Register Control Word 1 */
        UINT32  rcw2:4;         /* Register Control Word 2 */
        UINT32  rcw3:4;         /* Register Control Word 3 */
        UINT32  rcw4:4;         /* Register Control Word 4 */
        UINT32  rcw5:4;         /* Register Control Word 5 */
        UINT32  rcw6:4;         /* Register Control Word 6 */
        UINT32  rcw7:4;         /* Register Control Word 7 */
        } field;
    } DDRx_DDR_SDRAM_RCW_1;

/* DDR Register Control Words 2 register format */

typedef union
    {
    UINT32  regVal;
    struct
        {
        UINT32  rcw8:4;         /* Register Control Word 8 */
        UINT32  rcw9:4;         /* Register Control Word 9 */
        UINT32  rcw10:4;        /* Register Control Word 10 */
        UINT32  rcw11:4;        /* Register Control Word 11 */
        UINT32  rcw12:4;        /* Register Control Word 12 */
        UINT32  rcw13:4;        /* Register Control Word 13 */
        UINT32  rcw14:4;        /* Register Control Word 14 */
        UINT32  rcw15:4;        /* Register Control Word 15 */
        } field;
    } DDRx_DDR_SDRAM_RCW_2;

/* LAW controller struct */

typedef struct  lawBarCtlr
    {
    UINT32  lawBarH;
    UINT32  lawBarL;
    UINT32  lawAr;
    UINT32  reserved;
    } LAW_BAR_CTLR;

/* LAWn base address register high format */

typedef union
        {
        UINT32  regVal;
        struct
            {
            UINT32  :24;        /* Reserved field */
            UINT32  baseAddrHigh:8; /* 0-7 of the 40-bit base address */
            } field;
        } LAW_BAR_H;

/* LAWn base address register low format */

typedef union
        {
        UINT32  regVal;
        struct
            {
            UINT32  baseAddrLow:20; /* 8-27 of the 40-bit base address */
            UINT32  :12;        /* Reserved field */
            } field;
        } LAW_BAR_L;

/* LAWn attribute register format */

typedef union
        {
        UINT32  regVal;
        struct
            {
            UINT32  en:1;       /* Enable the local access window */
            UINT32  :3;         /* Reserved field */
            UINT32  trgtId:8;   /* Target identifier */
            UINT32  csdId:8;    /* Coherency subdomain identifier */
            UINT32  :6;         /* Reserved field */
            UINT32  size:6;     /* Size of the window */
            } field;
        } LAW_AR;

typedef struct fslDdrCtlrDrv {
    FSL_DDR_CTLR_PARAMS ddrCtlrParams;
    DDR_DEV             ddrDev;
    FSL_DDR_REG         ddrReg;
    UINT64              regBase;
    UINT64              lawRegBase;
} FSL_DDR_CTLR_DRV;

typedef struct forceSpecificParams {
    UINT32 rankNum;
    UINT32 ddrClkFreqHigh;      /* MHz */
    UINT32 rankCapacity;        /* GB */
    UINT32 clkAdjust;           /* force CLOCK_ADJUST value for DDR_SDRAM_CLK_CNTL */
    UINT32 wrlvlStart;          /* force WRLVL_WLR value for DDR_WRLVL_CNTL */
    UINT32 wrlvlCntl2;          /* force value for DDR_WRLVL_CNTL_2 */
    UINT32 wrlvlCntl3;          /* force value for DDR_WRLVL_CNTL_3 */
    UINT32 cpo;                 /* force CPO value for TIMING_CFG_2 */
    UINT32 wrDataDelay;         /* force WR_DATA_DELAY value for TIMING_CFG_2 */
    UINT32 forceEnable2T;       /* force enable 2T timing */
} FORCE_SPECIFIC_PARAMS;

#ifdef __cplusplus
    }
#endif /* __cplusplus */

#endif /* __INCfslDdrCtlrh */
