/* vxbSI31xxStorage.h - Silicon Image 31xx SATA driver header */

/*
 * Copyright (c) 2006, 2007, 2009-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01j,12jun12,e_d  add variable in SIL31XX_PORT for compatibility.
01i,25jun12,sye  fixed compile warnings. (WIND00356579)
01h,30may12,e_d  check attach status before remove logical device. (WIND00351659)
01g,06apr12,sye  fixed compile issue when included by a CPP file. (WIND00342562)
01f,25nov11,e_d  Modified the code with sata driver redesign. (WIND00307297)
01e,16mar10,b_m  Added defines for enable feature and transfer mode.
01d,21sep09,z_l  Added hob_nsect. (WIND00180173)
01c,26sep07,h_k  converted to new reg access methods.
01b,14may07,dee  vxbus version
01a,28feb06,add  Initial version.
*/

#ifndef __INCvxbSI31xxStorageh
#define __INCvxbSI31xxStorageh

/* includes */

#include <tool/gnu/toolMacros.h>
#include <blkIo.h>
#include <cacheLib.h>
#include <drv/xbd/xbd.h>
#include <hwif/vxbus/vxBus.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */

/* macros for reading and writing using vxbus methods */

#define VXB_SI31XX_HANDLE(p)    (p->regHandle)
#define VXB_SI31XX_BAR(p, q)    (ULONG)(p->regBase[q])

#define VXBWRITE32(w,x,y,z)                               \
    vxbWrite32 (VXB_SI31XX_HANDLE(w),                     \
                (UINT32 *)(VXB_SI31XX_BAR(w, x) + y), z)

#define VXBREAD32(w,x,y,z)                                \
    z = vxbRead32 (VXB_SI31XX_HANDLE(w),                  \
       (UINT32 *)(VXB_SI31XX_BAR(w, x) + y))

#define PORTREG_OFFSET  0x2000
#define SATA_CMD_TRYNUM 5

/* some specific defines for the Silicon Image 3124/3132 SATA controller */

enum
    {
    SIL31XX_MAX_CTRL        = 4,
    SIL31XX_MAX_PORTS       = 4,
    SIL31XX_GBLREGS         = 0,    /* bar index for global registers */
    SIL31XX_PORTREGS        = 2,    /* bar index for port registers */
    SIL31XX_VENDOR_ID       = 0x1095,

    SIL3124_DEVICE_ID       = 0x3124,
    SIL3124_NUM_PORTS       = 4,
    SIL3124_INT_PORTS_MASK  = 0xf,

    SIL3132_DEVICE_ID       = 0x3132,
    SIL3132_NUM_PORTS       = 2,
    SIL3132_INT_PORTS_MASK  = 0x3
    };

/* port register offsets from base of bar + 0x2000 * portnumber */

enum
    {
    SIL31XX_GBL_CTRL_SET_REG   = 0x0040,
    SIL31XX_GBL_INT_STAT_REG   = 0x0044,
    SIL31XX_BIST_CTRL_REG      = 0x0050,
    SIL31XX_BIST_PATTERN_REG   = 0x0054,
    SIL31XX_BIST_STAT_REG      = 0x0058,
    SIL31XX_PORT_CTRL_SET_REG  = 0x1000,
    SIL31XX_PORT_STATUS_REG    = 0x1000,
    SIL31XX_PORT_CTRL_CLR_REG  = 0x1004,
    SIL31XX_PORT_INT_STAT_REG  = 0x1008,
    SIL31XX_PORT_INT_ENBL_REG  = 0x1010,
    SIL31XX_PORT_INT_CLR_REG   = 0x1014,
    SIL31XX_PORT_ERR_REG       = 0x1024,
    SIL31XX_PORT_SLOT_STAT_REG = 0x1800,
    SIL31XX_PORT_CMD_ACT_REG   = 0x1C00,
    SIL31XX_PORT_SCTRL_REG     = 0x1f00,
    SIL31XX_PORT_SSTAT_REG     = 0x1f04,
    SIL31XX_PORT_SERR_REG      = 0x1f08
    };

/* SIL31XX register definitions */

enum
    {
    GLOBAL_RESET        = 0x80000000,
    PORT_RESET          = 0x00000001,
    PORT_INTNCR         = 0x00000008,
    PORT_CMDLEN16       = 0x00000020,
    PORT_32BIT_ACT      = 0x00000400,

/* interrupt enable bit fields INT_ENBL_REG/INT_CLR_REG */

    PORT_INT_CMD_COMPL  = 0x00000001,  /* command completion */
    PORT_INT_CMD_ERROR  = 0x00000002,  /* command error */
    PORT_INT_PM         = 0x00000010,  /* power management change */

/* Serial ATA Status register bit definitions SIL31XX_PORT_SSTAT_REG */

    SSTAT_DEVICE_PRESENT_PHY    = 0x00000003,  /* device and phy present */
    SSTAT_DETECT_MASK           = 0x0000000f,  /* mask for DET field */

/* Port control set register SIL31XX_PORT_CTRL_SET_REG */

    PCTRL_SET_PORTRESET         = 0x00000001,  /* port reset */
    PCTRL_SET_DEVRESET          = 0x00000002,
    PCTRL_SET_PORTINIT          = 0x00000004,  /* init port */
    PCTRL_SET_INTNCOR           = 0x00000008,
    PCTRL_SET_LEDDISABLE        = 0x00000010,
    PCTRL_SET_PACKLEN           = 0x00000020,
    PCTRL_SET_RESUME            = 0x00000040,
    PCTRL_SET_TRANBIST          = 0x00000080,
    PCTRL_SET_32BITACT          = 0x00000400,

/* Port status register */

    PSTATUS_PORTRDY             = 0x80000000,  /* Port ready */
    PSTATUS_CMDERR              = 0x00000002,  /* port command error */
    PSTATUS_PHYRDYCHG           = 0x00000010   /* phy ready change */
    };

/* SGE flag bit define */

enum
    {
    SGE_FLAG_XCF                = 0x10000000,
    SGE_FLAG_DRD                = 0x20000000,
    SGE_FLAG_LNK                = 0x40000000,
    SGE_FLAG_TRM                = 0x80000000
    };

/* return code from xdbIoctl(XBD_BIST_GET_RESULT) */

enum
    {
    SIL31XX_PRB_CONTROL_OVERRIDE    = 1 << 0,
    SIL31XX_PRB_CONTROL_RETRANSMIT  = 1 << 1,
    SIL31XX_PRB_CONTROL_EXTCOMMAND  = 1 << 2,
    SIL31XX_PRB_CONTROL_RECEIVE     = 1 << 3,
    SIL31XX_PRB_CONTROL_ATAPI_READ  = 1 << 4,
    SIL31XX_PRB_CONTROL_ATAPI_WRITE = 1 << 5,
    SIL31XX_PRB_CONTROL_INTMASK     = 1 << 6,
    SIL31XX_PRB_CONTROL_SOFTRESET   = 1 << 7
    };

/* control area define */

typedef enum BISTStatus
      {
      SIL31XX_BIST_NOT_RUN    = 0,
      SIL31XX_BIST_SUCCESSFUL = 1,
      SIL31XX_BIST_FAILED     = 2
      } SIL31XX_BIST_RESULT;

typedef struct
    {
    uint16_t  control;
    uint16_t  protocol;
    uint32_t  receiveCount;
    uint8_t   fisType;
    uint8_t   fisCtrl;
    uint8_t   cmdStatus;
    uint8_t   featuresErr;
    uint8_t   secNum;
    uint8_t   cylLow;
    uint8_t   cylHigh;
    uint8_t   devHead;
    uint8_t   secNumExp;
    uint8_t   cylLowExp;
    uint8_t   cylHighExp;
    uint8_t   featuresExp;
    uint8_t   secCount;
    uint8_t   secCountExp;
    uint8_t   rsvd1;
    uint8_t   devCtrl;
    uint32_t  rsvd2;
    uint32_t  rsvd3;
    uint32_t  sge0DataAdrsLow;
    uint32_t  sge0DataAdrsHigh;
    uint32_t  sge0DataCount;
    uint32_t  sge0Flags;
    uint32_t  sge1DataAdrsLow;
    uint32_t  sge1DataAdrsHigh;
    uint32_t  sge1DataCount;
    uint32_t  sge1Flags;
    } SIL31XX_PRB;

typedef struct sil31xxPort
    {
    BLK_DEV  blkDev; //add by yaoqiuguo for reworks I/O
    SATA_DEVICE            sataPortDev;
    uint32_t               regAdrsOff;
    SEM_ID                 cmdCmplSem;
    SEM_ID                 tagReady;
    STATUS                 cmdStatus;
    uint32_t               errorReg;
    uint32_t               sStatReg;
    uint32_t               sErrorReg;
    SEM_ID                 deviceChange;
    TASK_ID                monTaskId;
    SIL31XX_BIST_RESULT    bistResult;
    uint32_t               bistErrors;
    volatile SIL31XX_PRB * pPrb;
    VOIDFUNCPTR            phyCallback;
    VOID                 * callbackParam;
    } SIL31XX_PORT;

STATUS   sil31xxIsr(long);
STATUS   sil31xxBIST(SIL31XX_PORT *);
void     sil31xxBISTShow(BUS_DEVICE_ID);
STATUS   sil31xxRegisterPortCallback(int, int, VOIDFUNCPTR, VOID *);
void     sil31xxDrvVxbInit (BUS_DEVICE_ID pDev, int ctrlNum);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbSI31xxStorageh */
