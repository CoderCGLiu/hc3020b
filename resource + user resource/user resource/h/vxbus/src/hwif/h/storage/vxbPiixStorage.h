/* vxbPiixStorage.h - ICH ATA/ATAPI library header file */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,20may13,e_d  modified ATA_MAX_RW_SECTORS to 0xFFFF. (WIND00403183)
01d,11mar13,j_z  add read/write lock semaphore, AMD vendor.
01c,07sep12,sye  updated ataCtrl position for show routines. (WIND00354585) 
01b,06apr12,sye  fixed compile issue when included by a CPP file. (WIND00342562)
01a,25nov11,e_d  derived from vxbIntelIchStorage.h version 01j.
*/

#ifndef __INCvxbPiixStorageh
#define __INCvxbPiixStorageh

#ifndef _ASMLANGUAGE

/* includes */

#include <vxWorks.h>
#include <cdromFsLib.h>
#include <blkIo.h>
#include <wdLib.h>
#include <sys/fcntlcom.h>
#include <private/semLibP.h>
#include <spinLockLib.h>
#include <vxBusLib.h>
#include <hwif/util/vxbDmaBufLib.h>

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef _ASMLANGUAGE

/* defines */

/*
 * make it compatible for little/big endian machines.
 * i.e. swap for Big Endian machines
 */

#if _BYTE_ORDER == _BIG_ENDIAN
#   define ATA_SWAP(x) LONGSWAP(x)
#else
#   define ATA_SWAP(x) (x)
#endif /* _BYTE_ORDER */

#define MAX_28LBA   0x0fffffff
#define MAX_48LBA   (0xffffffff | ((sector_t)0xffff<<32))
#define MASK_48BIT  0x0000000000ff

#define PIIX_CLASS_ID             0x01010000

#if !defined(ATA_ZERO)
#   define ATA_ZERO (0)
#endif /* ATA_ZERO */

    enum
        {
        INTEL_VID = 0x8086,
        AMD_VID = 0x1022,
        INTEL_ICH_DEVICE_ID = 0x2411,
        INTEL_ICH0_DEVICE_ID = 0x2421,
        INTEL_ICH2_DEVICE_ID = 0x244B,
        INTEL_ICH2M_DEVICE_ID = 0x24AA,
        INTEL_ICH3M_DEVICE_ID = 0x248A,
        INTEL_6300ESB_I_DEVICE_ID = 0x25A2,
        INTEL_6300ESB_S_DEVICE_ID = 0x25A3,
        INTEL_6321ESB_DEVICE_ID = 0x2680,
        INTEL_ICH4_DEVICE_ID = 0x2C8A,
        INTEL_ICH5_PATA_DEVICE_ID = 0x24DB,
        INTEL_ICH5_SATA_DEVICE_ID = 0x24D1,
        INTEL_ICH5R_SATA_DEVICE_ID = 0x24DF,
        INTEL_ICH6_DEVICE_ID = 0x266F,
        INTEL_ICH6_SATA_DEVICE_ID = 0x2651,
        INTEL_ICH6R_SATA_DEVICE_ID = 0x2652,
        INTEL_ICH6M_SATA_DEVICE_ID = 0x2653,
        INTEL_ICH7_PATA_DEVICE_ID = 0x269E,
        INTEL_ICH7A_SATA_DEVICE_ID = 0x27C4,
        INTEL_ICH7B_SATA_DEVICE_ID = 0x27C0,
        INTEL_ICH8M_SATA_DEVICE_ID  = 0x2828,
        INTEL_ICH8M_PATA_DEVICE_ID  = 0x2850,
        INTEL_ICH10F2_SATA_DEVICE_ID = 0x3a20,
        INTEL_ICH10F5_SATA_DEVICE_ID = 0x3a26,
        INTEL_3100_SATA_DEVICE_ID1 = 0x2680,
        INTEL_TOLAPAI_SATA_DEVICE_ID = 0x5028,
        INTEL_CROWNBEACH_PATA_DEVICE_ID = 0x811A,
        INTEL_ICH9R_PATA_DEVICE_ID = 0x2920,
        INTEL_PCH_4PORT_SATA_DEVICE_ID_0 = 0x3B20,
        INTEL_PCH_4PORT_SATA_DEVICE_ID_1 = 0x3B2E
        };

/* defines */

#define ATA_MAX_CTRLS          2

#define PRI_NATIVE_EN          0x1
#define PRI_NATIVE_CAP         0x2
#define SEC_NATIVE_EN          0x4
#define SEC_NATIVE_CAP         0x8

/* register offset */

#define ATA_DATA               0   /* (RW) data register (16 bits) */
#define ATA_ERROR              1   /* (R)  error register          */
#define ATA_FEATURE            1   /* (W)  feature/precompensation */
#define ATA_SECCNT             2   /* (RW) sector count for ATA.
                                    * R-Interrupt reason W-unused  */
#define ATA_SECTOR             3   /* (RW) first sector number.
                                    * ATAPI- Reserved for SAMTAG   */
#define ATA_CYL_LO             4   /* (RW) cylinder low byte
                                    * ATAPI - Byte count Low       */
#define ATA_CYL_HI             5   /* (RW) cylinder high byte
                                    * ATAPI - Byte count High      */
#define ATA_SDH                6   /* (RW) sector size/drive/head
                                    * ATAPI - drive select         */
#define ATA_COMMAND            7   /* (W)  command register        */
#define ATA_STATUS             7   /* (R)  immediate status        */
#define ATA_A_STATUS           0   /* (R)  alternate status        */
#define ATA_D_CONTROL          0   /* (W)  disk controller control */
#define ATA_D_ADDRESS          1   /* (R)  disk controller address */

#define ATAPI_DATA             0   /* (RW/RW)data reg. (16 bits)   */
#define ATAPI_ERROR            1   /* (R /R )error reg.            */
#define ATAPI_FEATURE          1   /* (W /W )feature reg.          */
#define ATAPI_SECCNT_INTREASON 2   /* (RW/R )seccount/intr reason  */
#define ATAPI_SECTOR           3   /* (RW/  )Sector Number reg     */
#define ATAPI_CYLLOW_BCOUNT_LO 4   /* (RW/RW)cylLow/byte count low */
#define ATAPI_CYLHI_BCOUNT_HI  5   /* (RW/RW)cylHi/byte count high */
#define ATAPI_CYLLOW           4   /* (RW/RW)cylLow/byte count low */
#define ATAPI_CYLHI            5   /* (RW/RW)cylHi/byte count high */
#define ATAPI_SDH_D_SELECT     6   /* (RW/RW)sdh/drive select reg. */
#define ATAPI_STATUS           7   /* (R /R )status reg.           */
#define ATAPI_COMMAND          7   /* (W /W )command reg.          */
#define ATAPI_D_CONTROL        0   /* (W /W )device control        */
#define ATAPI_ASTATUS          1   /* (R /R )alternate status      */

/* removable media notification feature */

#define IOCTL_ENA_REMOVE_NOTIFY            0x14
#define IOCTL_DISABLE_REMOVE_NOTIFY        0x15

/* Advanced Power Management feature */

#define IOCTL_ENB_SET_ADV_POW_MNGMNT       0X20
#define IOCTL_DISABLE_ADV_POW_MNGMNT       0X21

/* Power-up in standby feature */

#define IOCTL_ENB_POW_UP_STDBY             0X30

/* Host protected area feature */

#define IOCTL_READ_NATIVE_MAX_ADDRESS      0XC0
#define IOCTL_SET_MAX_ADDRESS              0XC1
#define IOCTL_SET_MAX_SET_PASS             0XC2
#define IOCTL_SET_MAX_LOCK                 0XC3
#define IOCTL_SET_MAX_UNLOCK               0XC4
#define IOCTL_SET_MAX_FREEZE_LOCK          0XC5

/* SMART feature */

#define IOCTL_SMART_READ_DATA              0XD0
#define IOCTL_SMART_ENABLE_ATTRIB_AUTO     0XD1
#define IOCTL_SMART_DISABLE_ATTRIB_AUTO    0XD2
#define IOCTL_SMART_SAVE_ATTRIB            0XD3
#define IOCTL_SMART_OFFLINE_IMMED          0XD4
#define EXEC_OFF_IMMED_OFF_MODE            0
#define EXEC_SHORT_SELF_IMMED_OFF_MODE     1
#define EXEC_EXT_SELF_IMMED_OFF_MODE       2
#define ABORT_OFF_MODE_SELF_IMMED          127
#define EXEC_SHORT_SELF_IMMED_CAP_MODE     129
#define EXEC_EXT_SELF_IMMED_CAP_MODE       130
#define IOCTL_SMART_READ_LOG_SECTOR        0XD5
#define IOCTL_SMART_WRITE_LOG_SECTOR       0XD6
#define LOG_DIR                            0
#define ERROR_LOG                          1
#define SELFTEST_LOG                       6
#define IOCTL_SMART_ENABLE_OPER            0XD8
#define IOCTL_SMART_DISABLE_OPER           0XD9
#define IOCTL_SMART_RETURN_STATUS          0XDA

/* Power management feature */

#define IOCTL_CHECK_POWER_MODE             0XE5   /* ATA_CMD_CHECK_POWER_MODE */
#define IOCTL_IDLE_IMMEDIATE               0XE1   /* ATA_CMD_IDLE_IMMEDIATE   */
#define IOCTL_SLEEP                        0XE6   /* ATA_CMD_SLEEP            */
#define IOCTL_STANDBY_IMMEDIATE            0XE0   /* ATA_CMD_STANDBY_IMMEDIATE*/

#define ATA_MAX_DRIVES                     2      /* Always 2 drives per controller */

/* word 2 of device parametes specific configuration values */

#define ATA_SPEC_CONFIG_VALUE_0            0x37c8  /* SET FEATURE subcomm req */
#define ATA_SPEC_CONFIG_VALUE_1            0x738c  /* SET FEATURE subcomm req */
#define ATA_SPEC_CONFIG_VALUE_2            0x8c73  /* SET FEATURE subcomm NOT req */
#define ATA_SPEC_CONFIG_VALUE_3            0xc837  /* SET FEATURE subcomm NOT req */


/*
 * Drive Types.
 * This is 12 to 8 bits of 1st word of "ATAPI Identify device" command
 * Table , Page 113, Ref-1.
 */

#define CONFIG_DEV_TYPE_MASK                0x1f00
#define CONFIG_DEV_TYPE_DIRECT              0x00
#define CONFIG_DEV_TYPE_SEQUENTIAL          0x01
#define CONFIG_DEV_TYPE_PRINTER             0x02
#define CONFIG_DEV_TYPE_PROCESSOR           0x03
#define CONFIG_DEV_TYPE_WRITE_ONCE          0x04
#define CONFIG_DEV_TYPE_CD_ROM              0x05
#define CONFIG_DEV_TYPE_SCANNER             0x06
#define CONFIG_DEV_TYPE_OPTICAL             0x07
#define CONFIG_DEV_TYPE_MEDIUM_CHANGER      0x08
#define CONFIG_DEV_TYPE_COMMUNICATION       0x09
#define CONFIG_DEV_TYPE_ARRAY_CONTROLLER    0x0C
#define CONFIG_DEV_TYPE_ENCLOSER_SERVICE    0x0D
#define CONFIG_DEV_TYPE_RED_BLK_CMD_DEV     0x0E
#define CONFIG_DEV_TYPE_OPT_CARD_RW         0x00
#define CONFIG_DEV_TYPE_UNKNOWN             0x1F

#define ATA_SIGNATURE                       0x01010000
#define ATAPI_SIGNATURE                     0x010114EB

/* maximum length in bytes of an ATAPI command */

#define ATAPI_MAX_CMD_LENGTH                12

/* default timeout for ATA sync sem  */

#ifndef ATA_SEM_TIMEOUT_DEF
#   define ATA_SEM_TIMEOUT_DEF              5
#endif /* ATA_SEM_TIMEOUT_DEF */

/* default timeout for ATA watch dog */

#ifndef ATA_WDG_TIMEOUT_DEF
#   define ATA_WDG_TIMEOUT_DEF              5
#endif  /* These two are also defined in pc.h */

/* Device types */

/* device must be identified */

#define ATA_TYPE_INIT                       0xFF

/* Device  states */

#define ATA_DEV_OK                0   /* device is OK                      */
#define ATA_DEV_NONE              1   /* device absent or does not respond */
#define ATA_DEV_DIAG_F            2   /* device diagnostic failed          */
#define ATA_DEV_PREAD_F           3   /* read device parameters failed     */
#define ATA_DEV_MED_CH            4   /* medium have been changed          */
#define ATA_DEV_INIT              255 /* uninitialized device              */

/* Errors */

/* Register mode and other definitions */

/* size/drive/head register +6 : addressing mode CHS or LBA */
/* These are only in ATA1, ATA2. not defined in ATAPI 5*/

#define ATA_SDH_IBM               0xa0    /* chs, 512 bytes sector, ecc */
#define ATA_SDH_LBA               0xe0    /* lba, 512 bytes sector, ecc */

/** Register Bit Definitions **/

/* Device Control register +6 WR Control Block */

#define ATA_CTL_4BIT              0x8     /* use 4 head bits (wd1003) */
#define ATA_CTL_SRST              0x4     /* reset controller  */
#define ATA_CTL_NIEN              0x2     /* disable interrupts */

/* Feature Register */

#define FEAT_OVERLAP              0x02    /* command may be overlapped */
#define FEAT_DMA                  0x01    /* data will be transferred via DMA */

/* Error register +1 RD */

#define ERR_ABRT                  0x04    /* command aborted ATA_ERR_ABRT */

/* other bits of error register are command dependent */

/* Error Register */

#define ERR_SENSE_KEY             0xf0 /* Sense Key mask            */

#define SENSE_NO_SENSE            0x00 /* no sense sense key        */
#define SENSE_RECOVERED_ERROR     0x10 /* recovered error sense key */
#define SENSE_NOT_READY           0x20 /* not ready sense key       */
#define SENSE_MEDIUM_ERROR        0x30 /* medium error sense key    */
#define SENSE_HARDWARE_ERROR      0x40 /* hardware error sense key  */
#define SENSE_ILLEGAL_REQUEST     0x50 /* illegal request sense key */
#define SENSE_UNIT_ATTENTION      0x60 /* unit attention sense key  */
#define SENSE_DATA_PROTECT        0x70 /* data protect sense key    */
#define SENSE_BLANK_CHECK         0x80 /* blank check */
#define SENSE_VENDOR_SPECIFIC     0x90 /* vendor specific skey */
#define SENSE_COPY_ABORTED        0xa0 /* copy aborted */
#define SENSE_ABORTED_COMMAND     0xb0 /* aborted command sense key */
#define SENSE_EQUAL               0xc0 /* equal */
#define SENSE_VOLUME_OVERFLOW     0xd0 /* volume overflow */
#define SENSE_MISCOMPARE          0xe0 /* miscompare sense key      */
#define SENSE_RESERVED            0xf0

#define ERR_MCR                   0x08 /* Media Change Requested    */
#define ERR_ABRT                  0x04 /* Aborted command           */
#define ERR_EOM                   0x02 /* End Of Media              */
#define ERR_ILI                   0x01 /* Illegal Length Indication */

/* Interrupt Reason Register */

#define INTR_RELEASE              0x04 /*Bus released before command completion*/
#define INTR_IO                   0x02 /*1 - In to the Host; 0 - Out to device */
#define INTR_COD                  0x01 /* 1 - Command; 0 - user Data           */

#define USE_LBA                   0x40 /* used to 'OR' into Dev/Head register */

/* The drive number bit */

#define ATA_DRIVE_BIT             4    /* usage :-      1<<ATA_DRIVE_BIT */

/* status register +7 RD */

#define ATA_STAT_BUSY             0x80 /* controller busy */
#define ATA_STAT_READY            0x40 /*selected drive ready-ATA_STAT_DRDY */
#define ATA_STAT_DMAR             0x20 /*DMA Ready */
#define ATA_STAT_SERV             0x10 /*Service */
#define ATA_STAT_DRQ              0x08 /* Data Request    */
#define ATA_STAT_ERR              0x01 /* Error Detect    */
#define ATA_STAT_CHK              0x01 /* check    */

/* following are not in ATAPI5 */

#define ATA_STAT_WRTFLT           0x20 /* write fault. ATA_STAT_BIT5
                                        * sff8020i says that it is for
                                        * DMA Ready also.
                                        */
#define ATA_STAT_SEEKCMPLT        0x10 /* seek complete. ATA_STAT_BIT4
                                        * sff8020i- this is for service and
                                        * DSC.
                                        */
#define ATA_STAT_ECCCOR           0x04 /* sff8020-i correctable error occured
                                        * ATA_STAT_CORR
                                        */

/* sub command of ATA_CMD_SET_MAX */ /* Table 30, 8.38, Ref-1 */

#define ATA_SUB_SET_MAX_ADDRESS     0x00
#define ATA_SUB_SET_MAX_SET_PASS    0x01
#define ATA_SUB_SET_MAX_LOCK        0x02
#define ATA_SUB_SET_MAX_UNLOCK      0x03
#define ATA_SUB_SET_MAX_FREEZE_LOCK 0x04

/* ATA_SUB_SET_MAX_ADDRESS sector count options */

#define SET_MAX_VOLATILE            0x00
#define SET_MAX_NON_VOLATILE        0x01

/* sub command of ATA_CMD_SMART */ /* Table 32, Page 184, Ref-1 */

#define ATA_SMART_READ_DATA         0XD0
#define ATA_SMART_ATTRIB_AUTO       0XD2
#define ATA_SMART_SAVE_ATTRIB       0XD3
#define ATA_SMART_OFFLINE_IMMED     0XD4
#define ATA_SMART_READ_LOG_SECTOR   0XD5
#define ATA_SMART_WRITE_LOG_SECTOR  0XD6
#define ATA_SMART_ENABLE_OPER       0XD8
#define ATA_SMART_DISABLE_OPER      0XD9
#define ATA_SMART_RETURN_STATUS     0XDA

/* arg1 values of Sub command ATA_SMART_ATTRIB_AUTO */

#define ATA_SMART_SUB_ENABLE_ATTRIB_AUTO    0xf1
#define ATA_SMART_SUB_DISABLE_ATTRIB_AUTO   0x00

/* arg1 values of Sub command ATA_SMART_OFFLINE_IMMED */

#define ATA_SMART_SUB_EXEC_OFF_IMMED_OFF_MODE           0
#define ATA_SMART_SUB_EXEC_SHORT_SELF_IMMED_OFF_MODE    1
#define ATA_SMART_SUB_EXEC_EXT_SELF_IMMED_OFF_MODE      2
#define ATA_SMART_SUB_ABORT_OFF_MODE_SELF_IMMED         127
#define ATA_SMART_SUB_EXEC_SHORT_SELF_IMMED_CAP_MODE    129
#define ATA_SMART_SUB_EXEC_EXT_SELF_IMMED_CAP_MODE      130

/*
 * arg1 values of Sub command ATA_SMART_READ_LOG_SECTOR /
 *                            ATA_SMART_WRITE_LOG_SECTOR
 */
#define ATA_SMART_SUB_LOG_DIRECTORY 0x00
#define ATA_SMART_SUB_ERROR_LOG     0x01
#define ATA_SMART_SUB_SELF_TEST_LOG 0x06

/* transfer modes of ATA_SUB_SET_RWM ODE */ /* Table 28, Page 168, Ref-1 */

#define ATA_PIO_DEF_W       0x00    /* PIO default trans. mode */
#define ATA_PIO_DEF_WO      0x01    /* PIO default trans. mode, no IORDY */
#define ATA_PIO_W_0         0x08    /* PIO flow control trans. mode 0 */
#define ATA_PIO_W_1         0x09    /* PIO flow control trans. mode 1 */
#define ATA_PIO_W_2         0x0a    /* PIO flow control trans. mode 2 */
#define ATA_PIO_W_3         0x0b    /* PIO flow control trans. mode 3 */
#define ATA_PIO_W_4         0x0c    /* PIO flow control trans. mode 4 */

#define ATA_DMA_SINGLE_0    0x10    /* singleword DMA mode 0 */
#define ATA_DMA_SINGLE_1    0x11    /* singleword DMA mode 1 */
#define ATA_DMA_SINGLE_2    0x12    /* singleword DMA mode 2 */

#define ATA_DMA_MULTI_0     0x20    /* multiword DMA mode 0 */
#define ATA_DMA_MULTI_1     0x21    /* multiword DMA mode 1 */
#define ATA_DMA_MULTI_2     0x22    /* multiword DMA mode 2 */

#define ATA_DMA_ULTRA_0     0x40    /* Ultra DMA mode 0     */
#define ATA_DMA_ULTRA_1     0x41    /* Ultra DMA mode 1     */
#define ATA_DMA_ULTRA_2     0x42    /* Ultra DMA mode 2     */
#define ATA_DMA_ULTRA_3     0x43    /* Ultra DMA mode 3     */
#define ATA_DMA_ULTRA_4     0x44    /* Ultra DMA mode 4     */
#define ATA_DMA_ULTRA_5     0x45    /* Ultra DMA mode 5     */
#define ATA_DMA_ULTRA_6     0x46    /* Ultra DMA mode 6 (not supported) */


/* configuration flags: transfer mode, bits, unit, geometry
 *
 *    15  14  13  12  | 11  10   9   8  |  7   6   5   4  |  3   2   1   0
 *  ------------------|-----------------|-----------------|----------------
 *     0   0   0   0  |  0   0   0   0  |  0   0   0   0  |  0   0   0   0
 *  ---------|--------|--------|--------|----|---|--------|----|-----------|
 *  -BIT MASK|-PIOMASK|--------|GEO MASK|----|---|DMA MASK|PIO-|----Mode---|
 *    32  16   P   P                           U   M   S     P
 *             I   I                           L   U   I     I
 *     B   B   O   O                           T   L   N     O
 *     I   I                                   R   T   G
 *     T   T   M   S                           A   I   L     F
 *     S   S   U   I                                   E     L
 *             L   N                                         O
 *             T   G                           D   D   D     W
 *             I   L                           M   M   M
 *                 E                           A   A   A     C
 *                                                           O
 *                                                           N
 *                                                           T
 *                                                           R
 *                                                           O
 *                                                           L
 *
 */
#define ATA_MODE_MASK    0x00FF               /* transfer mode mask         */
#define ATA_GEO_MASK     0x0300               /* geometry mask              */
#define ATA_PIO_MASK     0x3000               /* RW PIO mask                */
#define ATA_BITS_MASK    0xc000               /* RW bits size mask          */

#define ATA_PIO_DEF_0    ATA_PIO_DEF_W        /* PIO default mode           */
#define ATA_PIO_DEF_1    ATA_PIO_DEF_WO       /* PIO default mode, no IORDY */
#define ATA_PIO_0        ATA_PIO_W_0          /* PIO mode 0                 */
#define ATA_PIO_1        ATA_PIO_W_1          /* PIO mode 1                 */
#define ATA_PIO_2        ATA_PIO_W_2          /* PIO mode 2                 */
#define ATA_PIO_3        ATA_PIO_W_3          /* PIO mode 3                 */
#define ATA_PIO_4        ATA_PIO_W_4          /* PIO mode 4                 */
#define ATA_PIO_AUTO     0x000D               /* PIO max supported mode     */

#define ATA_DMA_AUTO     0x0046               /* DMA max supported mode     */

#define ATA_GEO_FORCE    0x0100               /* set geometry in the table  */
#define ATA_GEO_PHYSICAL 0x0200               /* set physical geometry      */
#define ATA_GEO_CURRENT  0x0300               /* set current geometry       */

#define ATA_PIO_SINGLE   0x1000               /* RW PIO single sector       */
#define ATA_PIO_MULTI    0x2000               /* RW PIO multi sector        */

/* PIO Mode codes, these are also offset of set values */

#define ATA_SET_PIO_MODE_0      0x0
#define ATA_SET_PIO_MODE_1      0x1
#define ATA_SET_PIO_MODE_2      0x2
#define ATA_SET_PIO_MODE_3      0x3
#define ATA_SET_PIO_MODE_4      0x4

/* Single, multi and Udma codes, these are also offset of set values */

#define ATA_SET_SDMA_MODE_0     0x0
#define ATA_SET_SDMA_MODE_1     0x1
#define ATA_SET_SDMA_MODE_2     0x2

#define ATA_SET_MDMA_MODE_0     0x0
#define ATA_SET_MDMA_MODE_1     0x1
#define ATA_SET_MDMA_MODE_2     0x2

#define ATA_SET_UDMA_MODE_0     0x0
#define ATA_SET_UDMA_MODE_1     0x1
#define ATA_SET_UDMA_MODE_2     0x2
#define ATA_SET_UDMA_MODE_3     0x3
#define ATA_SET_UDMA_MODE_4     0x4
#define ATA_SET_UDMA_MODE_5     0x5

/* Bit masks */

#define ATA_BIT_MASK0           0x0001
#define ATA_BIT_MASK1           0x0002
#define ATA_BIT_MASK2           0x0004
#define ATA_BIT_MASK3           0x0008
#define ATA_BIT_MASK4           0x0010
#define ATA_BIT_MASK5           0x0020
#define ATA_BIT_MASK6           0x0040
#define ATA_BIT_MASK7           0x0080
#define ATA_BIT_MASK8           0x0100
#define ATA_BIT_MASK9           0x0200
#define ATA_BIT_MASK10          0x0400
#define ATA_BIT_MASK11          0x0800
#define ATA_BIT_MASK12          0x1000
#define ATA_BIT_MASK13          0x2000
#define ATA_BIT_MASK14          0x4000
#define ATA_BIT_MASK15          0x8000


#define ATA_BITS_16             0x4000        /* RW bits size, 16 bits      */
#define ATA_BITS_32             0x8000        /* RW bits size, 32 bits      */

#define ATA_BYTES_PER_BLOC      512

#define ATAPI_CDROM_BYTE_PER_BLK   2048      /* user data in CDROM Model   */
#define ATAPI_BLOCKS                100      /* number of blocks */

#define ATA_MULTISEC_MASK       0x00ff

/* Capabilities fields and masks */

#define ATA_INTER_DMA_MASK      0x8000
#define ATA_CMD_QUE_MASK        0x4000
#define ATA_OVERLAP_MASK        0x2000
#define ATA_IORDY_MASK          0x0800
#define ATA_IOLBA_MASK          0x0200
#define ATA_DMA_CAP_MASK        0x0100

/* hardware reset results - bit masks*/

#define ATA_HWRR_CBLID          0x2000

/* PIO Mode bits and masks */

#define ATA_PIO_MASK_012        0x03  /* PIO Mode 0,1,2 */
#define ATA_PIO_MASK_34         0x02  /* PIO Mode 3,4 */

/* LBA Mask and bits */

#define ATA_LBA_HEAD_MASK       0x0f000000
#define ATA_LBA_CYL_MASK        0x00ffff00
#define ATA_LBA_SECTOR_MASK     0x000000ff

/* capabilities */

#define CAPABIL_DMA             0x0100   /* DMA Supported               */
#define CAPABIL_LBA             0x0200   /* LBA Supported               */
#define CAPABIL_IORDY_CTRL      0x0400   /* IORDY can be disabled       */
#define CAPABIL_IORDY           0x0800   /* IORDY Supported             */
#define CAPABIL_OVERLAP         0x2000   /* Overlap Operation Supported */

/* sub command of ATA_CMD_SET_MAX */ /* Table 30, 8.38, Ref-1 */

#define ATA_SUB_SET_MAX_ADDRESS     0x00
#define ATA_SUB_SET_MAX_SET_PASS    0x01
#define ATA_SUB_SET_MAX_LOCK        0x02
#define ATA_SUB_SET_MAX_UNLOCK      0x03
#define ATA_SUB_SET_MAX_FREEZE_LOCK 0x04

/* ATA_SUB_SET_MAX_ADDRESS sector count options */

#define SET_MAX_VOLATILE            0x00
#define SET_MAX_NON_VOLATILE        0x01

/*
 * arg1 values of Sub command ATA_SMART_READ_LOG_SECTOR /
 *                            ATA_SMART_WRITE_LOG_SECTOR
 */

#define ATA_SMART_SUB_LOG_DIRECTORY 0x00
#define ATA_SMART_SUB_ERROR_LOG     0x01
#define ATA_SMART_SUB_SELF_TEST_LOG 0x06

/* transfer modes of ATA_SUB_SET_RWM ODE */ /* Table 28, Page 168, Ref-1 */

#define ATA_PIO_DEF_W               0x00  /* PIO default trans. mode */
#define ATA_PIO_DEF_WO              0x01  /* PIO default trans. mode, no IORDY */
#define ATA_PIO_W_0                 0x08  /* PIO flow control trans. mode 0 */
#define ATA_PIO_W_1                 0x09  /* PIO flow control trans. mode 1 */
#define ATA_PIO_W_2                 0x0a  /* PIO flow control trans. mode 2 */
#define ATA_PIO_W_3                 0x0b  /* PIO flow control trans. mode 3 */
#define ATA_PIO_W_4                 0x0c  /* PIO flow control trans. mode 4 */

#define ATA_DMA_SINGLE_0            0x10  /* singleword DMA mode 0 */
#define ATA_DMA_SINGLE_1            0x11  /* singleword DMA mode 1 */
#define ATA_DMA_SINGLE_2            0x12  /* singleword DMA mode 2 */

#define ATA_DMA_MULTI_0             0x20  /* multiword DMA mode 0 */
#define ATA_DMA_MULTI_1             0x21  /* multiword DMA mode 1 */
#define ATA_DMA_MULTI_2             0x22  /* multiword DMA mode 2 */

#define ATA_DMA_ULTRA_0             0x40  /* Ultra DMA mode 0     */
#define ATA_DMA_ULTRA_1             0x41  /* Ultra DMA mode 1     */
#define ATA_DMA_ULTRA_2             0x42  /* Ultra DMA mode 2     */
#define ATA_DMA_ULTRA_3             0x43  /* Ultra DMA mode 3     */
#define ATA_DMA_ULTRA_4             0x44  /* Ultra DMA mode 4     */
#define ATA_DMA_ULTRA_5             0x45  /* Ultra DMA mode 5     */
#define ATA_DMA_ULTRA_6             0x46  /* Ultra DMA mode 6 (not supported) */

/* configuration flags: transfer mode, bits, unit, geometry
 *
 *    15  14  13  12  | 11  10   9   8  |  7   6   5   4  |  3   2   1   0
 *  ------------------|-----------------|-----------------|----------------
 *     0   0   0   0  |  0   0   0   0  |  0   0   0   0  |  0   0   0   0
 *  ---------|--------|--------|--------|----|---|--------|----|-----------|
 *  -BIT MASK|-PIOMASK|--------|GEO MASK|----|---|DMA MASK|PIO-|----Mode---|
 *    32  16   P   P                           U   M   S     P
 *             I   I                           L   U   I     I
 *     B   B   O   O                           T   L   N     O
 *     I   I                                   R   T   G
 *     T   T   M   S                           A   I   L     F
 *     S   S   U   I                                   E     L
 *             L   N                                         O
 *             T   G                           D   D   D     W
 *             I   L                           M   M   M
 *                 E                           A   A   A     C
 *                                                           O
 *                                                           N
 *                                                           T
 *                                                           R
 *                                                           O
 *                                                           L
 *
 */

#define ATA_MODE_MASK           0x00FF          /* transfer mode mask         */
#define ATA_GEO_MASK            0x0300          /* geometry mask              */
#define ATA_PIO_MASK            0x3000          /* RW PIO mask                */
#define ATA_BITS_MASK           0xc000          /* RW bits size mask          */

#define ATA_PIO_DEF_0           ATA_PIO_DEF_W   /* PIO default mode           */
#define ATA_PIO_DEF_1           ATA_PIO_DEF_WO  /* PIO default mode, no IORDY */
#define ATA_PIO_0               ATA_PIO_W_0     /* PIO mode 0                 */
#define ATA_PIO_1               ATA_PIO_W_1     /* PIO mode 1                 */
#define ATA_PIO_2               ATA_PIO_W_2     /* PIO mode 2                 */
#define ATA_PIO_3               ATA_PIO_W_3     /* PIO mode 3                 */
#define ATA_PIO_4               ATA_PIO_W_4     /* PIO mode 4                 */
#define ATA_PIO_AUTO            0x000D          /* PIO max supported mode     */

#define ATA_DMA_AUTO            0x0046          /* DMA max supported mode     */

#define ATA_GEO_FORCE           0x0100          /* set geometry in the table  */
#define ATA_GEO_PHYSICAL        0x0200          /* set physical geometry      */
#define ATA_GEO_CURRENT         0x0300          /* set current geometry       */

#define ATA_PIO_SINGLE          0x1000          /* RW PIO single sector       */
#define ATA_PIO_MULTI           0x2000          /* RW PIO multi sector        */

/* PIO Mode codes, these are also offset of set values */

#define ATA_SET_PIO_MODE_0      0x0
#define ATA_SET_PIO_MODE_1      0x1
#define ATA_SET_PIO_MODE_2      0x2
#define ATA_SET_PIO_MODE_3      0x3
#define ATA_SET_PIO_MODE_4      0x4

/* Single, multi and Udma codes, these are also offset of set values */

#define ATA_SET_SDMA_MODE_0     0x0
#define ATA_SET_SDMA_MODE_1     0x1
#define ATA_SET_SDMA_MODE_2     0x2

#define ATA_SET_MDMA_MODE_0     0x0
#define ATA_SET_MDMA_MODE_1     0x1
#define ATA_SET_MDMA_MODE_2     0x2

#define ATA_SET_UDMA_MODE_0     0x0
#define ATA_SET_UDMA_MODE_1     0x1
#define ATA_SET_UDMA_MODE_2     0x2
#define ATA_SET_UDMA_MODE_3     0x3
#define ATA_SET_UDMA_MODE_4     0x4
#define ATA_SET_UDMA_MODE_5     0x5

/* Bit masks */

#define ATA_BIT_MASK0           0x0001
#define ATA_BIT_MASK1           0x0002
#define ATA_BIT_MASK2           0x0004
#define ATA_BIT_MASK3           0x0008
#define ATA_BIT_MASK4           0x0010
#define ATA_BIT_MASK5           0x0020
#define ATA_BIT_MASK6           0x0040
#define ATA_BIT_MASK7           0x0080
#define ATA_BIT_MASK8           0x0100
#define ATA_BIT_MASK9           0x0200
#define ATA_BIT_MASK10          0x0400
#define ATA_BIT_MASK11          0x0800
#define ATA_BIT_MASK12          0x1000
#define ATA_BIT_MASK13          0x2000
#define ATA_BIT_MASK14          0x4000
#define ATA_BIT_MASK15          0x8000


#define ATA_BITS_16             0x4000     /* RW bits size, 16 bits      */
#define ATA_BITS_32             0x8000     /* RW bits size, 32 bits      */

#define ATA_BYTES_PER_BLOC      512

#define ATA_MAX_RW_SECTORS         MAX_SECTORS_FISLBA48  /* max sectors per transfer   */
#define ATAPI_CDROM_BYTE_PER_BLK   2048                  /* user data in CDROM Model   */
#define ATAPI_BLOCKS               100                   /* number of blocks */

#define ATA_MULTISEC_MASK   0x00ff

/* Capabilities fields and masks */

#define ATA_INTER_DMA_MASK  0x8000
#define ATA_CMD_QUE_MASK    0x4000
#define ATA_OVERLAP_MASK    0x2000
#define ATA_IORDY_MASK      0x0800
#define ATA_IOLBA_MASK      0x0200
#define ATA_DMA_CAP_MASK    0x0100

/* hardware reset results - bit masks*/
#define ATA_HWRR_CBLID      0x2000

/* PIO Mode bits and masks */

#define ATA_PIO_MASK_012    0x03  /* PIO Mode 0,1,2 */
#define ATA_PIO_MASK_34     0x02  /* PIO Mode 3,4 */

/* LBA Mask and bits */

#define ATA_LBA_HEAD_MASK   0x0f000000
#define ATA_LBA_CYL_MASK    0x00ffff00
#define ATA_LBA_SECTOR_MASK 0x000000ff

/* capabilities */

#define CAPABIL_DMA         0x0100   /* DMA Supported               */
#define CAPABIL_LBA         0x0200   /* LBA Supported               */
#define CAPABIL_IORDY_CTRL  0x0400   /* IORDY can be disabled       */
#define CAPABIL_IORDY       0x0800   /* IORDY Supported             */
#define CAPABIL_OVERLAP     0x2000   /* Overlap Operation Supported */

/* Command Related Definitions */

/* ATAPI_CMD_START_STOP_UNIT. Page-197,10.8.25,Ref-2 */

#define STOP_DISK           0X00
#define START_DISK          0X01
#define EJECT_DISK          0X02
#define LOAD_DISK           0X03

/* ATAPI_CMD_PREVENT_ALLOW_MEDIUM_REMOVAL */

#define MEDIA_UNLOCK        0x00
#define MEDIA_LOCK          0x01

#define I82371AB_MAC_64_K   0x10000  /* pow(2,16) */
#define I82371AB_MAC_4_K    0x01000  /* pow(2,12) */
#define I82371AB_MAC_512    0x00200  /* pow(2,9) */


/* max number of controllers */

#define I82371AB_MAX_CTRLS  0x2      /* 0 and 1 only */

/* Offset in the PCI configuaration space */

#define PCI_CFG_BMIBA       PCI_CFG_BASE_ADDRESS_4

/* 0x20-0x23 default = 0x0000 R/W */

#define PCI_CFG_IDETIM(ctrl)  (0x40 + ctrl * 2)
#define PCI_CFG_IDETIM_PRI    0x40   /* 0x40-0x41 default = 0x0000 R/W */
#define PCI_CFG_IDETIM_SEC    0x42   /* 0x42-0x43 default = 0x0000 R/W */
#define PCI_CFG_SIDETIME      0x44   /* 0x44      default = 0x00   R/W */
#define PCI_CFG_UDMACTL       0x48   /* 0x48      default = 0x00   R/W */
#define PCI_CFG_UDMATIM       0x4A   /* 0x4A-0x4B default = 0x0000 R/W */


    /* Page-78, 2.7, Ref-1 */

#define I82371AB_BMICOMadd(Piix4DMACtl,ctrl)  (Piix4DMACtl->ioBaseAddress  \
                                               + 0x0 + ctrl * 8)
#define I82371AB_BMISTAadd(Piix4DMACtl,ctrl)  (Piix4DMACtl->ioBaseAddress  \
                                              + 0x2 + ctrl * 8)
#define I82371AB_BMIDTPadd(Piix4DMACtl,ctrl)  (Piix4DMACtl->ioBaseAddress  \
                                              + 0x4 + ctrl * 8)

#define I82371AB_BMI_ALL          0xff
#define I82371AB_BMICOM           0x00
#define I82371AB_BMISTA           0x01
#define I82371AB_BMIDTP           0x02

    /* BMICOM */

#define I82371AB_RWCON            0x08
#define I82371AB_SSBM             0x01


    /* BMISTA */

#define I82371AB_DMA1CAP          0x40
#define I82371AB_DMA0CAP          0x20
#define I82371AB_INT_STATUS       0x04
#define I82371AB_IDEDMA_ERROR     0x02
#define I82371AB_BMIDE_ACTIVE     0x01


    /* bit masks */

#define I82371AB_BMBIA_ADD_MASK   0xfff0   /* valid bits 15 to 4 */

    /* PCI status */

#define I82371AB_PCISTS_BME       0x0004   /* bus master enable bit*/
#define I82371AB_PCISTS_IOSE      0x0001   /* I/O space enable bit */

/* IDE Timing Register */

#define I82371AB_IDETIM_ENE       0x8000   /* IDE enable */
#define I82371AB_IDETIM_PPE0      0x0004   /* PPC0 enable */
#define I82371AB_IDETIM_IE0       0x0002   /* IORDY Sample Point
                                              Enable Drive 0 */

/*
 * A small delay for register hardware settling time
 * read unused ISA register, about 720ns per read
 */

#define I82371AB_DELAY()         sysInByte(0x84); \
                                 sysInByte(0x84); \
                                 sysInByte(0x84); \
                                 sysInByte(0x84)

/* typedefs */

typedef struct pci_ide_controller
    {
    void *            pPiix4Handler;

    /* PCI configuaration Registers */

    short             pciHeaderCommand;
    UINT16            ideTim[2];    /* Primary   channel - 40,41 */
                                    /* Secondary channel - 42,43 */
    UINT8             slaveIdeTim;  /* 44    */
    UINT8             uDmaCtl;      /* 48    */
    UINT16            uDmaTime;     /* 4A-4B */

    /* IO Space Registers */

    UINT32            ioBaseAddress;
    UINT8             bmiCom[2];   /* 0x00 */
    UINT8             bmiSta[2];   /* 0x02 */
    UINT32            bmiDtp[2];   /* 0x04 */
    UINT32          * pPRDTable[I82371AB_MAX_CTRLS];
    }PCI_IDE_DMA_CTL;

#define END_OF_TABLE  0x8000;  /* end of table bit indication */

typedef struct i82371DescTable
    {
    void * buffer;
    UINT16 count;
    UINT16 eot;
    } i82371_DESC_TBL;

/* host controller access macros */

#define ATA_IO_BYTE_READ(...) \
    vxbRead8 (__VA_ARGS__)

#define ATA_IO_BYTE_WRITE(...) \
    vxbWrite8 (__VA_ARGS__)

#define ATA_IO_WORD_READ(...) \
    vxbRead16 (__VA_ARGS__)

#define ATA_IO_WORD_WRITE(...) \
    vxbWrite16 (__VA_ARGS__)

#define ATA_IO_DOUBLE_READ(...) \
    vxbRead32 (__VA_ARGS__)

#define ATA_IO_DOUBLE_WRITE(...) \
    vxbWrite32 (__VA_ARGS__)

#define ATA_IO_NWORD_READ(ioAdrs, pData, nWords) \
    do { \
        int w; \
        UINT16 * pDataPtr = (UINT16 *) pData; \
        for (w = 0; w < nWords; w++, pDataPtr++) \
            *pDataPtr = ATA_IO_WORD_READ (ioAdrs); \
    } while ((0))

#define ATA_IO_NWORD_WRITE(ioAdrs, pData, nWords) \
    do { \
        int w; \
        UINT16 * pDataPtr = (UINT16 *) pData; \
        for (w = 0; w < nWords; w++, pDataPtr++) \
            ATA_IO_WORD_WRITE (ioAdrs, *pDataPtr); \
    } while ((0))

#define ATA_IO_NLONG_READ(ioAdrs, pData, nLongs) \
    do { \
        int l; \
        UINT32 * pDataPtr = (UINT32 *) pData; \
        for (l = 0; l < nLongs; l++, pDataPtr++) \
            *pDataPtr = ATA_IO_DOUBLE_READ (ioAdrs); \
    } while ((0))

#define ATA_IO_NLONG_WRITE(ioAdrs, pData, nLongs) \
    do { \
        int l; \
        UINT32 * pDataPtr = (UINT32 *) pData; \
        for (l = 0; l < nLongs; l++, pDataPtr++) \
            ATA_IO_DOUBLE_WRITE (ioAdrs, *pDataPtr); \
    } while ((0))

#define ATA_LIKE_WAIT_FOREVER   (sysClkRateGet() * 5)

/* Delay to ensure Status Register content is valid */

#define ATA_WAIT_STATUS         sysDelay ()  /* >= 400 ns */


/*
 * This macro provides a small delay, which is expected to be more than 400nSec
 * that is used in several places in the ATA command protocols:
 * 1) It is recommended that the host delay 400ns after
 *    writing the command register.
 * 2) ATA-4 has added a new requirement that the host delay
 *    400ns if the DEV bit in the Device/Head register is
 *    changed.  This was not recommended or required in ATA-1,
 *    ATA-2 or ATA-3.
 * 3) ATA-4 has added another new requirement that the host delay
 *    after the last word of a data transfer before checking the
 *    status register.  This was not recommended or required in
 *    ATA-1, ATA-2 or ATA-3.
 *
 */

#define ATA_DELAY_400NSEC                                               \
    do {                                                                \
        ATA_IO_BYTE_READ(PIIX4_ATA_A_STATUS);                           \
        ATA_IO_BYTE_READ(PIIX4_ATA_A_STATUS);                           \
        ATA_IO_BYTE_READ(PIIX4_ATA_A_STATUS);                           \
        ATA_IO_BYTE_READ(PIIX4_ATA_A_STATUS);                           \
        ATA_IO_BYTE_READ(PIIX4_ATA_A_STATUS);                           \
    } while ((0))

/* ATA/ATAPI registers */                                      /* (ATA/ATAPI) */

#define PIIX4_ATA_DATA              pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_DATA)
#define PIIX4_ATA_ERROR             pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_ERROR)
#define PIIX4_ATA_FEATURE           pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_FEATURE)
#define PIIX4_ATA_SECCNT_INTREASON  pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_SECCNT)
#define PIIX4_ATA_SECTOR            pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_SECTOR)
#define PIIX4_ATA_CYLLOW_BCOUNT_LO  pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_CYL_LO)
#define PIIX4_ATA_CYLHI_BCOUNT_HI   pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_CYL_HI)
#define PIIX4_ATA_CYLLOW            pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_CYL_LO)
#define PIIX4_ATA_CYLHI             pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_CYL_HI)
#define PIIX4_ATA_SDH_D_SELECT      pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_SDH)
#define PIIX4_ATA_STATUS            pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_STATUS)
#define PIIX4_ATA_COMMAND           pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[0] \
                                    + ATA_COMMAND)

#define PIIX4_ATA_A_STATUS          pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[1] \
                                    + ATA_A_STATUS)
#define PIIX4_ATA_D_CONTROL         pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[1] \
                                    + ATA_D_CONTROL)
#define PIIX4_ATA_D_ADDRESS         pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[1] \
                                    + ATA_D_ADDRESS)

/* Bus Master macros */

#define BM_IDE_COMMAND                  0
#define BM_IDE_STATUS                   2
#define BM_IDE_DESCRIPTOR_TABLE_POINTER 4
#define PIIX4_BM_COMMAND            pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[2] \
                                    + BM_IDE_COMMAND)
#define PIIX4_BM_STATUS             pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[2] \
                                    + BM_IDE_STATUS)
#define PIIX4_BM_DTP                pCtrl->regHandle, \
                                    (void*)((UINT8 *)pCtrl->regBase[2] \
                                    + BM_IDE_DESCRIPTOR_TABLE_POINTER)

/* enum */

typedef enum  /* with respect to host/memory */
    {
    IN_DATA  = O_RDONLY, /* from drive to memory */
    OUT_DATA = O_WRONLY, /* to drive from memory */
    NON_DATA             /* non data command     */
    } ATA_DATA_DIR;

typedef struct piix4_ata_drive
    {
    SATA_DEVICE  sataPortDev;
    UINT32       signature;

   UINT8        state;
    STATUS       (*Reset)(void*,int Ctrl,int dev);
    } PIIX4_ATA_DRIVE;

typedef struct ioAdrs
    {
    UINT8 *base;
    void  *handle;
    } IO_ADRS_RESOURCE;

typedef struct piix4_ata_reg
    {
    IO_ADRS_RESOURCE command_block;
    IO_ADRS_RESOURCE control_block;
    IO_ADRS_RESOURCE bus_master_block;
    } PIIX4_ATA_REG;

typedef struct ataCtrl
    {
    PIIX4_ATA_DRIVE  drive[ATA_MAX_DRIVES]; /* drives per controller   */
    PIIX4_ATA_REG    ataReg;                /* ATA registers */
    VOIDFUNCPTR      ataIntr;               /* interrupt handler */
    SEMAPHORE        ataBioReadySem;        /* bio queue counting semaphore */
    SEMAPHORE        syncSem;               /* binary sem for syncronization */
    SEMAPHORE        muteSem;               /* mutex sem for mutual-exclusion */
    struct bio      *bioQueueh;             /* bio queue head for master device */
    struct bio      *bioQueuet;             /* bio queue tail for master device */
    TASK_ID         svcTaskId;              /* id of service task for this ctrl */
    WDOG_ID         wdgId;                  /* watch dog                        */
    int             ctrl;                   /* controller number */
    int             pwrdown;                /* power down mode                */
    int             ctrlType;               /* type of controller             */
    int             intCount;               /* interrupt count                */
    int             intStatus;              /* interrupt status               */
    short           configType;             /* user recommended configuaration.
                                             * This is the value passed to ataPiDrv
                                             * during driver intialization.
                                             */
    int             semTimeout;             /* timeout seconds for sync semaphore */
    int             wdgTimeout;             /* timeout seconds for watch dog      */
    BOOL            wdgOkay;                /* watch dog status                   */
    BOOL            installed;              /* TRUE if a driver is installed      */
    BOOL            changed;                /* TRUE if a card is installed        */
    BOOL            uDmaCableOkay;          /* Set to 1 if the devices are connected
                                             * to the controller with a 80 conductor
                                             * cable using 40 pin connector
                                             * (UDMA cable).
                                             */
    spinlockIsr_t   spinlock;                /* SMP */
    struct piix4DrvCtrl * pDrvCtrl;
    } PIIX4_ATA_CTRL;

typedef struct smart_data /* Table 34, section 8.41.5.8, Ref-1 */
    {
    short    vendorSpecific0[362];         /* 0 - 361     */
    short    offLineCollectionStatus;      /* 362
                                            * Table 35, 8.41.5.8.1 Ref-1
                                            */
    short    selfTestExecutionStatus;      /* 363
                                            * Table 36, 8.41.5.8.2 Ref-1
                                            */
    short    timeOffLine1;                 /* 364 in secs */
    short    timeOffLine2;                 /* 365 in secs */
    short    vendorSpecific366;            /* 366         */
    short    offlineCollectionCapability;  /* 367         */
    short    smartCapability1;             /* 368         */
    short    smartCapability2;             /* 369         */
    short    errorLogCapability;           /* 370
                                            * bit-0:  1-supported
                                            *         0-notsupported
                                            */
    short    vendorSpecific371;            /* 371         */
    short    shortSelfTestPollTime;        /* 372 in mins */
    short    extendedSelfTestPollTime;     /* 373 in mins */
    short    reserved374[12];              /* 374 - 385   */
    short    vendorSpecific386[125];       /* 386-510     */
    short    checksum;                     /* 511         */
    } SMART_DATA;

typedef struct smart_log_dir_entry
    {
    short          noOfSectors;
    short          reserved;
    } SMART_LOG_DIR_ENTRY;

typedef struct smart_log_directory    /* Table 38, section 8.41.6.8.1, Ref-1 */
    {
    short           smartLogVersion1;         /* 0                 */
    short           smartLogVersion2;         /* 1                 */
    SMART_LOG_DIR_ENTRY smartLogEntry[255];   /* 2 - 511 (255 * 2) */
    } SMART_LOG_DIRECTORY;

typedef struct readTocStruct
    {
    UINT32   transferLength;
    UINT8 *  pResultBuf;
    } READ_TOC_STRUCT;

typedef struct command_data_struct /* Table 41 , 8.41.6.8.2.3.1, Ref-1 */
    {
    short dControl;   /* Device Control / Alternate status */
    short feature;    /* Error / Feature */
    short seccnt;
    short sector;
    short cylLo;
    short cylHi;
    short sdh;
    short command;
    short timeStampLSB;
    short timeStampNextLSB;
    short timeStampNextMSB;
    short timeStampMSB;
    } COMMAND_DATA_STRUCT;

typedef struct error_data_struct /* Table 42 , 8.41.6.8.2.3.2, Ref-1 */
    {
    short reserved0;
    short error;                 /* Error / Feature */
    short seccnt;
    short sector;
    short cylLo;
    short cylHi;
    short sdh;
    short status;                /* Status / command */
    short extendedErrorInfo[19];
    short state;                 /* 27 */
    short LifeTimeStampLSB;      /* 28 */
    short LifeTimeStampMSB;      /* 29 */
    } ERROR_DATA_STRUCT;

typedef struct error_log_data_struct /* Table 40, 8.41.6.8.2.3, Ref-1 */
    {
    COMMAND_DATA_STRUCT commandDataStruct[5]; /* fifth is where error reported*/
    ERROR_DATA_STRUCT   errorDataStruct;
    } ERROR_LOG_DATA_STRUCT;


typedef struct smart_error_log_sector /* Table 39, 8.41.6.8.2 , Ref-1 */
    {
    short                    errorLogVersion;        /* 0       */
    short                    errorLogIndex;          /* 1       */
    ERROR_LOG_DATA_STRUCT    ErrLogDataStruct[5];    /* 2 - 91
                                                      * 92-181
                                                      * 182-271
                                                      * 272-361
                                                      * 362-451
                                                      */
    short                    DeviceErrCount1;        /* 452     */
    short                    DeviceErrCount2;        /* 453     */
    short                    reserved454[57];        /* 454-510 */
    short                    checksum;               /* 511     */
    } SMART_ERROR_LOG_SECTOR;

typedef struct self_test_descriptor  /* Table 45, 8.41.6.8.3.2, Ref-1 */
    {
    short sector;               /* 0 */
    short selfTestExecStatus;   /* 1 */
    short lifeTimeStampLSB;     /* 2 */
    short lifeTimeStampMSB;     /* 3 */
    short failChechPoint;       /* 4 */
    short failingLBALSB;        /* 5 */
    short failingLBAnextLSB;    /* 6 */
    short failingLBAnextMSB;    /* 7 */
    short failingLBAMSB;        /* 8 */
    short vendorSpecific[15];   /* 9 - 23 */
    } SELF_TEST_DESCRIPTOR;

typedef UINT8 CMD_PKT [ATAPI_MAX_CMD_LENGTH];

typedef struct piix4AtaResource
    {
    int       devices;
    short     configType;
    int       semTimeout;
    int       wdgTimeout;
    } PIIX4_ATA_RESOURCE;

typedef struct piix4DrvCtrl
    {
    SATA_HOST            ataCtrl [ATA_MAX_CTRLS]; /* be here for show routine */
    VXB_DEVICE_ID        pDev;
    PIIX4_ATA_RESOURCE * pAtaResources;
    int                  useBlockWrapper;
    PCI_IDE_DMA_CTL      Piix4DMACtl;
    SEMAPHORE            syncSem[ATA_MAX_CTRLS];
    SEMAPHORE            muteSem[ATA_MAX_CTRLS];
    SEMAPHORE            rwSem[ATA_MAX_CTRLS];
    WDOG_ID              wdgId[ATA_MAX_CTRLS];      /* watch dog */
    spinlockIsr_t        spinlock[ATA_MAX_CTRLS];   /* SMP       */
    int                  semTimeout[ATA_MAX_CTRLS];
    int                  wdgTimeout[ATA_MAX_CTRLS];
    int                  intCount;                  /* interrupt count    */
    int                  intStatus;                 /* interrupt status   */
    BOOL                 wdgOkay[ATA_MAX_CTRLS];    /* watch dog status   */
    VOIDFUNCPTR          ataIntr[ATA_MAX_CTRLS];    /* interrupt handler  */
    FUNCPTR              ataDmaInit;                /* initialize the DMA hardware */
    FUNCPTR              ataDmaSet;                 /* setup the DMA hardware for xfer */
    FUNCPTR              ataDmaStart;               /* start the DMA operation */
    FUNCPTR              ataDmaStop;                /* stop DMA functions */
    FUNCPTR              ataDmaCheck;               /* test status of DMA */
    FUNCPTR              ataDmaModeSet;             /* set mode of DMA operations */
    FUNCPTR              ataDmaModeNegotiate;       /* determine DMA xfer mode */
    FUNCPTR              ataDmaReset;               /* reset the DMA controller */
    BOOL                 ataHostDmaSupportOkay;     /* DMA supported */
    } PIIX4_DRV_CTRL;

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbPiixStorage */
