/* vxbFslSataStorage.h - Freescale SATA driver header */

/*
 * Copyright (c) 2008, 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01p,17oct13,m_y  modify to fix compile warning
01o,18sep13,m_y  add code to support XBD sched policy and NCQ
01n,05feb13,wyt  add t4240 definition.
01m,03sep12,sye  removed unused indirect PRDs. (WIND00373114)
01l,10jul12,sye  fixed compile warnings. (WIND00355046)
01k,06apr12,sye  fixed compile issue when included by a CPP file. (WIND00342562)
01j,08mar12,e_d  moved CMD_RETRY_NUM macro to vxbSataLib.h. (WIND00333858)
01i,08mar12,fao  add p1010 SVR define. (WIND00337944) 
01h,21jan12,d_l  add p2041 definition.
01g,22oct11,e_d  updated vxbfslSataStorage.h with sata redesigned. (WIND00307297)
01f,08aug11,y_y  add workaround for p5020/p3041 SATA port.
01e,07mar11,b_m  Added ATA major revision defines in ATA_ID result.
01d,23aug10,e_d  Added driver support for SATA controller new version.
01c,18sep08,z_l  Added DMA definitions and cleaned up code.
01b,07jul08,x_s  converted to new reg access methods.
01a,21feb08,x_s  Initial version based on vxbSI31xxStorage.h.
*/

#ifndef __INCvxbFslSataStorageh
#define __INCvxbFslSataStorageh

/* includes */

#include <cacheLib.h>
#include <hwif/vxbus/vxBus.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */

/* max secs to wait for ATA_CMD_ID to return */

#define ID_WAIT_IN_SECS                  60
#define TRYNUM                           5000

#define FSL_SATA_SUPSETREG_OFFSET        0x100
#define FSL_SATA_CONSTSREG_OFFSET        0x140
#define FSL_SATA_SYSREG_OFFSET           0x40C
#define FSL_SATA_MAX_PRD                 63
#define FSL_SATA_MAX_PRD_USABLE          (FSL_SATA_MAX_PRD - 1)
#define FSL_SATA_MAX_PRD_DIRECT          16
#define FSL_SATA_MAX_CMDS                16
#define FSL_SATA_CMD_SLOT_SIZE           (FSL_SATA_MAX_CMDS * 16)
#define FSL_SATA_MAX_PRD_SIZE            (FSL_SATA_MAX_PRD * 16)
#define FSL_MAX_PRD_SIZE                 (0xFFFFFFFF)

#define FSL_SATA_CMD_DESC_CFIS_SZ        32
#define FSL_SATA_CMD_DESC_SFIS_SZ        32
#define FSL_SATA_CMD_DESC_ACMD_SZ        16
#define FSL_SATA_CMD_DESC_RSRVD          16
#define FSL_SATA_PRDE_RSRVD              8
#define FSL_SATA_PRDE_NUM                4

#define FSL_SATA_CMD_LEN                (FSL_SATA_CMD_DESC_CFIS_SZ +  \
                                         FSL_SATA_CMD_DESC_SFIS_SZ +  \
                                         FSL_SATA_CMD_DESC_ACMD_SZ +  \
                                         FSL_SATA_CMD_DESC_RSRVD)

#define FSL_SATA_CMD_DESC_SIZE          (FSL_SATA_CMD_DESC_CFIS_SZ +  \
                                         FSL_SATA_CMD_DESC_SFIS_SZ +  \
                                         FSL_SATA_CMD_DESC_ACMD_SZ +  \
                                         FSL_SATA_CMD_DESC_RSRVD +    \
                                         FSL_SATA_MAX_PRD * 16)
                                               
#define	FSL_SATA_CMD_DESC_OFFSET_TO_PRDT (FSL_SATA_CMD_DESC_CFIS_SZ + \
                                          FSL_SATA_CMD_DESC_SFIS_SZ + \
                                          FSL_SATA_CMD_DESC_ACMD_SZ + \
                                          FSL_SATA_CMD_DESC_RSRVD)   


#define FSL_SATA_MAX_CMDS_SIZE          FSL_SATA_CMD_DESC_SIZE * FSL_SATA_MAX_CMDS

#define FSL_SATA_PORT_PRIV_DMA_SZ       (FSL_SATA_CMD_SLOT_SIZE +     \
                                         FSL_SATA_CMD_DESC_SIZE *  \
                                         FSL_SATA_MAX_CMDS)

/* SVR define */

#define SVR_PPC_8315E                    0x80B40000
#define SVR_PPC_8315                     0x80B50000
#define SVR_PPC_8379E                    0x80C20000
#define SVR_PPC_8379                     0x80C30000
#define SVR_PPC_8377E                    0x80C60000
#define SVR_PPC_8377                     0x80C70000
#define SVR_PPC_8536E                    0x803F0000
#define SVR_PPC_8536                     0x80370000
#define SVR_PPC_1022                     0x80E00000
#define SVR_PPC_5020E                    0x82280000
#define SVR_PPC_5020                     0x82200000
#define SVR_PPC_5010E                    0x82290000
#define SVR_PPC_5010                     0x82210000
#define SVR_PPC_3041E                    0x82190000
#define SVR_PPC_3041                     0x82110000
#define SVR_PPC_1010                     0x80F10000 
#define SVR_PPC_1010E                    0x80F90000 
#define SVR_PPC_2041                     0x82100000
#define SVR_PPC_2041E                    0x82180000
#define SVR_PPC_1010                     0x80F10000 
#define SVR_PPC_1010E                    0x80F90000
#define SVR_PPC_4240                     0x82400000
#define SVR_PPC_4240E                    0x82480000
#define SVR_PPC_MASK                     0xFFFF0000

/* Sata command registers */

#define FSL_COMMAND_CQR                  0
#define FSL_COMMAND_CAR                  8
#define FSL_COMMDAND_CCR                 0x10
#define FSL_COMMDAND_CER                 0x18
#define FSL_COMMAND_DER                  0x20
#define FSL_COMMAND_CHBA                 0x24
#define FSL_COMMAND_HSTATUS              0x28
#define FSL_COMMAND_HCONTROL             0x2C
#define FSL_COMMAND_CQPMP                0x30
#define FSL_COMMAND_SIGNATURE            0x34
#define FSL_COMMAND_ICC                  0x38

/* Host Status Register (HStatus) definement */

#define HSTATUS_MASK                     0x3F
#define HSTATUS_ONLINE                   (1 << 31)
#define HSTATUS_GOING_OFFLINE            (1 << 30)
#define HSTATUS_BIST_ERR                 (1 << 29)

#define HSTATUS_FATAL_ERR_HC_MASTER_ERR  (1 << 18)
#define HSTATUS_FATAL_ERR_PARITY_ERR_TX  (1 << 17)
#define HSTATUS_FATAL_ERR_PARITY_ERR_RX  (1 << 16)
#define HSTATUS_FATAL_ERR_DATA_UNDERRUN  (1 << 13)
#define HSTATUS_FATAL_ERR_DATA_OVERRUN   (1 << 12)
#define HSTATUS_FATAL_ERR_CRC_ERR_TX     (1 << 11)
#define HSTATUS_FATAL_ERR_CRC_ERR_RX     (1 << 10)
#define HSTATUS_FATAL_ERR_FIFO_OVRFL_TX  (1 << 9)
#define HSTATUS_FATAL_ERR_FIFO_OVRFL_RX  (1 << 8)

#define HSTATUS_FATAL_ERROR_DECODE       (HSTATUS_FATAL_ERR_HC_MASTER_ERR | \
                                         HSTATUS_FATAL_ERR_PARITY_ERR_TX | \
                                         HSTATUS_FATAL_ERR_PARITY_ERR_RX | \
                                         HSTATUS_FATAL_ERR_DATA_UNDERRUN | \
                                         HSTATUS_FATAL_ERR_DATA_OVERRUN  | \
                                         HSTATUS_FATAL_ERR_CRC_ERR_TX    | \
                                         HSTATUS_FATAL_ERR_CRC_ERR_RX    | \
                                         HSTATUS_FATAL_ERR_FIFO_OVRFL_TX | \
                                         HSTATUS_FATAL_ERR_FIFO_OVRFL_RX)

#define HSTATUS_INT_ON_FATAL_ERR         (1 << 5)
#define HSTATUS_INT_ON_PHYRDY_CHG        (1 << 4)
#define HSTATUS_INT_ON_SIGNATURE_UPDATE  (1 << 3)
#define HSTATUS_INT_ON_SNOTIFY_UPDATE    (1 << 2)
#define HSTATUS_INT_ON_SINGL_DEVICE_ERR  (1 << 1)
#define HSTATUS_INT_ON_CMD_COMPLETE      (1 << 0)

#define HSTATUS_INT_ON_ERROR             (HSTATUS_INT_ON_FATAL_ERR | \
                                         HSTATUS_INT_ON_SINGL_DEVICE_ERR)

/* Host Control Register (HControl) definement */

#define HCONTROL_ONLINE_PHY_RST          (1 << 31)
#define HCONTROL_FORCE_OFFLINE           (1 << 30)
#define HCONTROL_CLEAR_ERROR             (1 << 27)
#define HCONTROL_PARITY_PROT_MOD         (1 << 14)
#define HCONTROL_DPATH_PARITY            (1 << 12)
#define HCONTROL_SNOOP_ENABLE            (1 << 10)
#define HCONTROL_PMP_ATTACHED            (1 << 9)
#define HCONTROL_COPYOUT_STATFIS         (1 << 8)
#define HCONTROL_IE_ON_FATAL_ERR         (1 << 5)
#define HCONTROL_IE_ON_PHYRDY_CHG        (1 << 4)
#define HCONTROL_IE_ON_SIGNATURE_UPDATE  (1 << 3)
#define HCONTROL_IE_ON_SNOTIFY_UPDATE    (1 << 2)
#define HCONTROL_IE_ON_SINGL_DEVICE_ERR  (1 << 1)
#define HCONTROL_IE_ON_CMD_COMPLETE      1

/* SATA interface status Register */

#define SSTATUS_DET_MASK                 0x0F
#define SSTATUS_DET_NODEVICE             0
#define SSTATUS_DET_NOPHY                1
#define SSTATUS_DET_DEVICEANDPHY         3
#define SSTATUS_DET_PHYDISABLE           4

/* SATA Interface Error Register (SError) */

#define SERROR_CRC_ERROR                 (1 << 21)

#define HCONTROL_DEFAULT_PORT_IRQ_ENABLE_MASK \
                     ( HCONTROL_IE_ON_FATAL_ERR        | \
                       HCONTROL_IE_ON_PHYRDY_CHG       | \
                       HCONTROL_IE_ON_SIGNATURE_UPDATE | \
                       HCONTROL_IE_ON_SNOTIFY_UPDATE   | \
                       HCONTROL_IE_ON_SINGL_DEVICE_ERR | \
                       HCONTROL_IE_ON_CMD_COMPLETE )

#define HCONTROL_DEFAULT_PORT_IRQ_EXCEPT_PHYRDY  \
                     ( HCONTROL_IE_ON_FATAL_ERR        | \
                       HCONTROL_IE_ON_SIGNATURE_UPDATE | \
                       HCONTROL_IE_ON_SNOTIFY_UPDATE   | \
                       HCONTROL_IE_ON_SINGL_DEVICE_ERR | \
                       HCONTROL_IE_ON_CMD_COMPLETE )

#define HCONTROL_EXT_INDIRECT_SEG_PRD_FLAG  (UINT32)(1 << 31)
#define HCONTROL_DATA_SNOOP_ENABLE_V1       (1 << 22)
#define HCONTROL_DATA_SNOOP_ENABLE_V2       (1 << 28)

/* SATA Superset Registers */

#define SUPERSET_SSTATUS                    0
#define SUPERSET_SERROR                     4
#define SUPERSET_SCONTROL                   8
#define SUPERSET_SNOTIFY                    0xC

/* SATA Control Status Registers */

#define CONTROL_STATUS_TRANSCFG             0
#define CONTROL_STATUS_TRANSSTATUS          4
#define CONTROL_STATUS_LINKCFG              8
#define CONTROL_STATUS_LINKCFG1             0xC
#define CONTROL_STATUS_LINKCFG2             0x10
#define CONTROL_STATUS_LINKSTATUS           0x14
#define CONTROL_STATUS_LINKSTATUS1          0x18
#define CONTROL_STATUS_PHYCTRLCFG           0x1C
#define CONTROL_STATUS_RESERVED             0x20

#define CONTROL_STATUS_PHY_BIST_ENABLE      0x01

/* Command Header Table entry */

typedef struct
    {
    uint32_t cda;
    uint32_t prdeFisLen;
    uint32_t ttl;
    uint32_t descInfo;
    } FSLSATA_CMDHDR_TBL;

/* Description information definement */
#define CMD_DESC_RES                        (1 << 11)
#define VENDOR_SPECIFIC_BIST                (1 << 10)
#define CMD_DESC_SNOOP_ENABLE               (1 << 9)
#define FPDMA_QUEUED_CMD                    (1 << 8)
#define SRST_CMD                            (1 << 7)
#define BIST                                (1 << 6)
#define ATAPI_CMD                           (1 << 5)

/* Command Descriptor */

typedef struct
    {
    uint8_t  cfis[FSL_SATA_CMD_DESC_CFIS_SZ];
    uint8_t  sfis[FSL_SATA_CMD_DESC_SFIS_SZ];
    uint8_t  acmd[FSL_SATA_CMD_DESC_ACMD_SZ];
    uint8_t  reserved[FSL_SATA_CMD_DESC_RSRVD];
    uint32_t prdt[FSL_SATA_MAX_PRD_DIRECT * FSL_SATA_PRDE_NUM];
    uint32_t prdtIndirect[(FSL_SATA_MAX_PRD - FSL_SATA_MAX_PRD_DIRECT) * FSL_SATA_PRDE_NUM];
    } FSLSATA_COMMAND_DESC;

/* Physical region table descriptor */

typedef struct
    {
    uint32_t dba;
    uint8_t  reserved[FSL_SATA_PRDE_RSRVD];
    uint32_t extAndDwc;
    } FSLSATA_PRDE;

/* register low level access routines */

#define FSL_SATA_BAR(p)         ((SATA_HOST *)(p)->pDrvCtrl)->regBase[0]
#define FSL_SATA_HANDLE(p)      ((SATA_HOST *)(p)->pDrvCtrl)->regHandle

#define CSR_READ_4(pDev, addr)              \
        vxbRead32(FSL_SATA_HANDLE(pDev),    \
                  (UINT32 *)((char *)FSL_SATA_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)       \
        vxbWrite32(FSL_SATA_HANDLE(pDev),   \
                   (UINT32 *)((char *)FSL_SATA_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)     \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)     \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

typedef struct fslSataPort
    {
    SATA_DEVICE            sataPortDev;
    FSLSATA_CMDHDR_TBL   * cmdslot;
    uint32_t               cmdslot_paddr;
    FSLSATA_COMMAND_DESC * cmdentry;
    uint32_t               cmdentry_paddr;
    FSLSATA_PRDE         * pPrb;

    uint32_t               ssrAdrsOff;
    uint32_t               csrAdrsOff;
    uint32_t               scrAdrsOff;
    SEM_ID                 cmdCmplSem[FSL_SATA_MAX_CMDS];
    SEM_ID                 tagReady;
    STATUS                 cmdStatus;
    BOOL                   atapiWait;
    uint32_t               errorReg;
    uint32_t               sStatReg;
    uint32_t               sErrorReg;
    uint32_t               sataCapabilities;

    VOIDFUNCPTR            phyCallback;
    VOID                 * callbackParam;
    SEM_ID                 deviceChange;
    TASK_ID                monTaskId;
    UINT32                 nextTag;
    } FSLSATA_PORT;

STATUS   fslSataIsr(SATA_HOST *);
STATUS   fslSataRegisterPortCallback(int, int, VOIDFUNCPTR, VOID *);
void     fslSataDrvVxbInit (BUS_DEVICE_ID pDev);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbFslSataStorageh */
