/* vxbAt91Mci.h - Atmel MCI driver header */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,04jan12,e_d  written.
*/

#ifndef __INCvxbAt91Mcih
#define __INCvxbAt91Mcih

#ifdef __cplusplus
extern "C" {
#endif

/* MCI driver name */

#define MCI_NAME                              "at91Mci"

/* MCI card monitor task name */

#define MCI_CARD_MON_TASK_NAME                "at91CardMon"

/* MCI card monitor task priority */

#define MCI_CARD_MON_TASK_PRI                 (100)

/* Command delay */

#define MCI_CMD_DELAY                         (100)

/* MCI card monitor task stack size */

#define MCI_CARD_MON_TASK_STACK               (8192)

/* MCI DMA frame element size */

#define MMC_FRAME_ELEMENT_SIZE                (4)

/* seconds to wait for SD/MMC command or data done */

#define SDMMC_CMD_WAIT_IN_SECS                (5)

/* MCI default clock frequence */

#define MCI_DEF_CLK_FREQ                      (132096000)

/* MCI power saving divider value */

#define MCI_POW_SAVDVR_VALUE                  (0x7)
#define MCI_DTOCYC_VALUE                      (0xF)

/* MCI check card insert time */

#define MCI_CHECKCARD_DEFAULT                 (10)

/* Register Offsets */

#define MCI_CR              0x000   /* MCI Control Register              */
#define MCI_MR              0x004   /* MCI Mode Register                 */
#define MCI_DTOR            0x008   /* MCI Data Timeout Register         */
#define MCI_SDCR            0x00C   /* MCI SD/SDIO Card Register         */
#define MCI_AR              0x010   /* MCI Argument Register             */
#define MCI_CMDR            0x014   /* MCI Command Register              */
#define MCI_BLKR            0x018   /* MCI Block Register                */
#define MCI_RSR0            0x020   /* MCI Response0 Register            */
#define MCI_RSR1            0x024   /* MCI Response1 Register            */
#define MCI_RSR2            0x028   /* MCI Response2 Register            */
#define MCI_RSR3            0x02C   /* MCI Response3 Register            */
#define MCI_RDR             0x030   /* MCI Receive  Data Register        */
#define MCI_TDR             0x034   /* MCI Transmit Data register        */
#define MCI_SR              0x040   /* MCI Status Register               */
#define MCI_IER             0x044   /* MCI Interrupt Enable Register     */
#define MCI_IDR             0x048   /* MCI Interrupt Disable Register    */
#define MCI_IMR             0x04C   /* MCI Interrupt Mask Register       */
#define MCI_PDC_RPR         0x100   /* Receive Pointer Register          */
#define MCI_PDC_RCR         0x104   /* Receive Counter Register          */
#define MCI_PDC_TPR         0x108   /* Transmit Pointer Register         */
#define MCI_PDC_TCR         0x10C   /* Transmit Counter Register         */
#define MCI_PDC_RNPR        0x110   /* Receive Next Pointer Register     */
#define MCI_PDC_RNCR        0x114   /* Receive Next Counter Register     */
#define MCI_PDC_TNPR        0x118   /* Transmit Next Pointer Register    */
#define MCI_PDC_TNCR        0x11C   /* Transmit Next Counter Register    */
#define MCI_PDC_PTCR        0x120   /* PDC Transfer Control Register     */
#define MCI_PDC_PTSR        0x124   /* PDC Transfer Status Register      */

/* MCI Control register */

#define MCI_CR_MCIEN        (1 << 0)
#define MCI_CR_MCIDIS       (1 << 1)
#define MCI_CR_PWSEN        (1 << 2)
#define MCI_CR_PWSDIS       (1 << 3)
#define MCI_CR_SWRST        (1 << 7)

/* MCI Mode register */

#define MCI_MR_CLKDIVMSK    0xFF
#define MCI_MR_PWSDIV(x)    ((x) << 8)
#define MCI_MR_RDPROOF      (1 << 11)
#define MCI_MR_WRPROOF      (1 << 12)
#define MCI_MR_PDCFBYTE     (1 << 13)
#define MCI_MR_PDCPADV      (1 << 14)
#define MCI_MR_PDCMODE      (1 << 15)
#define MCI_MR_BLKLEN(x)    ((x) << 16)

/* MCI Data Timeout register */

#define MCI_DTOR_DTOMUL(x)  ((x) << 4)

/* MCI SDIO register */

#define MCI_SDCR_SDCBUS     (1 << 7)

/* MCI Command register */

#define MCI_CMDR_RSPTYPNO        0
#define MCI_CMDR_RSPTYP48        1
#define MCI_CMDR_RSPTYP136       2
#define MCI_CMDR_RSPTYP(x)       ((x) << 6)
#define MCI_CMDR_SPCMD_INITCMD   1
#define MCI_CMDR_SPCMD_SYNCMD    2
#define MCI_CMDR_SPCMD_INTCMD    4
#define MCI_CMDR_SPCMD_INTRESP   5
#define MCI_CMDR_SPCMD(x)        ((x) << 8)
#define MCI_CMDR_OPDCMD          (1 << 11)
#define MCI_CMDR_MAXLAT          (1 << 12)
#define MCI_CMDR_TRCMD_NODAT     0
#define MCI_CMDR_TRCMD_STAT      1
#define MCI_CMDR_TRCMD_STOP      2
#define MCI_CMDR_TRCMD(x)        ((x) << 16)
#define MCI_CMDR_TRDIR           (1 << 18)
#define MCI_CMDR_TPTYP_SBLK      0
#define MCI_CMDR_TPTYP_MBLK      1
#define MCI_CMDR_TPTYP_MMCSTM    2
#define MCI_CMDR_TPTYP_SDIOBYTE  4
#define MCI_CMDR_TPTYP_SDIOBLK   5
#define MCI_CMDR_TRTYP(x)        ((x) << 19)
#define MCI_CMDR_IOSPCMD_SUPCOM  1
#define MCI_CMDR_IOSPCMD_RESCOM  2
#define MCI_CMDR_IOSPCMD(x)      ((x) << 24)

/* MCI Status register */

#define MCI_SR_CMDRDY            (1 << 0)
#define MCI_SR_RXRDY             (1 << 1)
#define MCI_SR_TXRDY             (1 << 2)
#define MCI_SR_BLKE              (1 << 3)
#define MCI_SR_DTIP              (1 << 4)
#define MCI_SR_NOTBUSY           (1 << 5)
#define MCI_SR_ENDRX             (1 << 6)
#define MCI_SR_ENDTX             (1 << 7)
#define MCI_SR_SDIOIRQA          (1 << 8)
#define MCI_SR_SDIOIRQB          (1 << 9)
#define MCI_SR_RXBUFF            (1 << 14)
#define MCI_SR_TXBUFF            (1 << 15)
#define MCI_SR_RINDE             (1 << 16)
#define MCI_SR_RDIRE             (1 << 17)
#define MCI_SR_RCRCE             (1 << 18)
#define MCI_SR_RENDE             (1 << 19)
#define MCI_SR_RTOE              (1 << 20)
#define MCI_SR_DCRCE             (1 << 21)
#define MCI_SR_DTOE              (1 << 22)
#define MCI_SR_OVRE              (1 << 30)
#define MCI_SR_UNRE              (1 << 31)

#define MCI_SR_DATACOMPL         (MCI_SR_ENDRX | MCI_SR_ENDTX |      \
                                 MCI_SR_RXBUFF | MCI_SR_TXBUFF)

#define MCI_SR_ERROR             (MCI_IER_DCRCE | MCI_IER_DTOE |     \
                                 MCI_IER_RCRCE | MCI_IER_RDIRE |     \
                                 MCI_IER_RENDE | \
                                 MCI_IER_RINDE | MCI_IER_RTOE)

/* MCI interrput enable register */

#define MCI_IER_CMDRDY           (1 << 0)
#define MCI_IER_RXRDY            (1 << 1)
#define MCI_IER_TXRDY            (1 << 2)
#define MCI_IER_BLKE             (1 << 3)
#define MCI_IER_DTIP             (1 << 4)
#define MCI_IER_NOTBUSY          (1 << 5)
#define MCI_IER_ENDRX            (1 << 6)
#define MCI_IER_ENDTX            (1 << 7)
#define MCI_IER_SDIOIRQA         (1 << 8)
#define MCI_IER_SDIOIRQB         (1 << 9)
#define MCI_IER_RXBUFF           (1 << 14)
#define MCI_IER_TXBUFF           (1 << 15)
#define MCI_IER_RINDE            (1 << 16)
#define MCI_IER_RDIRE            (1 << 17)
#define MCI_IER_RCRCE            (1 << 18)
#define MCI_IER_RENDE            (1 << 19)
#define MCI_IER_RTOE             (1 << 20)
#define MCI_IER_DCRCE            (1 << 21)
#define MCI_IER_DTOE             (1 << 22)
#define MCI_IER_OVRE             (1 << 30)
#define MCI_IER_UNRE             (1 << 31)

/* MCI block register */

#define MCI_BLKR_BLKLEN(x)       ((x) << 16)

/* PDC_PTCR : (PDC Offset: 0x20) PDC Transfer Control Register */

#define MCI_PDC_RXTEN            ((unsigned int) 0x1 <<  0)
#define MCI_PDC_RXTDIS           ((unsigned int) 0x1 <<  1)
#define MCI_PDC_TXTEN            ((unsigned int) 0x1 <<  8)
#define MCI_PDC_TXTDIS           ((unsigned int) 0x1 <<  9)

/* Block Attributes Register */

#define BLK_CNT_SHIFT            (16)

#define CLK_FREQ_400K            (400000)
#define MMC_CLKD_MASK            (0x3ff)

/* Transfter Type Register */

#define CMD_CMDINX(x)            ((x) & 0x3F)
#define CMD_RSPTYP_NORSP         0x00000000
#define CMD_RSPTYP_LEN136        0x00000040
#define CMD_RSPTYP_LEN48         0x00000080
#define CMD_RSPTYP_RESV          0x00000000
#define CMD_CMDTYP_NORMAL        0x00000000
#define CMD_CMDTYP_INIT          0x00000100
#define CMD_CMDTYP_SYNC          0x00000200
#define CMD_CMDTYP_RESV          0x00000300
#define CMD_CMDTYP_INTCMD        0x00000400
#define CMD_CMDTYP_INTRES        0x00000500
#define CMD_OPDCMD               0x00000000
#define CMD_DATA_PRESENT         0x00200000
#define CMD_CICEN                0x00100000
#define CMD_CCCEN                0x00080000
#define CMD_MULTI_BLK            0x00000020
#define CMD_DIR_READ             0x00000010
#define CMD_AC12EN               0x00000004
#define CMD_BCEN                 0x00000002
#define CMD_DMAEN                0x00000001

typedef BOOL (*AT91MCI_FUNCPTR) (void);

/* MCI driver control */

typedef struct mciDrvCtrl
    {
    VXB_DEVICE_ID           pDev;
    void *                  regBase;
    void *                  regHandle;
    UINT8                   slot;
    SEM_ID                  devChange;
    SEM_ID                  cmdDone;
    SEM_ID                  dataDone;
    BOOL                    cardIns;
    UINT32                  intSts;
    volatile UINT32         intMask;
    UINT32                  hcVer;
    UINT32                  clkSpeed;
    SDMMC_HOST              host;
    SDMMC_CARD              card;
    AT91MCI_FUNCPTR         cardDetect;
    AT91MCI_FUNCPTR         cardWpCheck;
    }MCI_DRV_CTRL;

/* register low level access routines */

#define MCI_BAR(p)         ((MCI_DRV_CTRL *)(p)->pDrvCtrl)->regBase
#define MCI_HANDLE(p)      ((MCI_DRV_CTRL *)(p)->pDrvCtrl)->regHandle

#define CSR_READ_4(pDev, addr)              \
        vxbRead32(MCI_HANDLE(pDev),       \
                  (UINT32 *)((char *)MCI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)       \
        vxbWrite32(MCI_HANDLE(pDev),      \
                   (UINT32 *)((char *)MCI_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)     \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)     \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbAt91Mcih */

