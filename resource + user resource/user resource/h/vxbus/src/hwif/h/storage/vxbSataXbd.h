/* vxbSataXbd.h - Generic SATA block driver header */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,18jun13,m_y  add code to support XBD sched policy
01b,06apr12,sye  fixed compile issue when included by a CPP file. (WIND00342562)
01a,18aug11,e_d  written.
*/

#ifndef __INCvxbSataXbdh
#define __INCvxbSataXbdh

/* includes */

#include <drv/xbd/xbd.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */

/* SATA XBD service task name */

#define SATA_XBD_SVC_TASK_NAME     "sataXbdSvc"

/* SATA XBD service task priority */

#define SATA_XBD_SVC_TASK_PRI      254

/* SATA XBD service task stack size */

#define SATA_XBD_SVC_TASK_STACK    8192

/* SATA XBD REQ service task name */

#define SATA_XBD_REQ_SVC_TASK_NAME "sataXbdReqSvc"


/* SATA XBD block device */

typedef struct satacXbdDev
    {
    XBD             xbd;                /* xbd device */
    devname_t       devName;            /* name of xbd device */
    TASK_ID         svcTaskId;          /* service task id */
    SEM_ID          bioReady;           /* wake up bio service task */
    SEM_ID          bioMutex;           /* lock bio queue */
    struct bio *    bioHead;            /* head of bio queue */
    struct bio *    bioTail;            /* tail of bio queue */
    SEM_ID          xbdSemId;           /* used for xbdBlkDevCreateSync() */
    BOOL            xbdInserted;        /* inserted or removed */
    BOOL            xbdInstantiated;    /* TRUE if stack init is complete */
    void *          pPort;              /* pointer to SATA device */
    FUNCPTR         blkRd;              /* block read routine */
    FUNCPTR         blkWt;              /* block write routine */
    SEM_ID          reqMutex;           /* lock req list */
    SEM_ID          reqReadySem;        /* request queue counting semaphore */ 
    LIST            reqList;            /* request use xbdMuteSem to protect */
    TASK_ID         reqSevcTask;        /* request service task */
    FUNCPTR         xferReq;            /* xfer request routine */
    } SATA_XBD_DEV, *pSATA_XBD_DEV;

/* forward declarations */

device_t sataXbdDevCreate (void * , char *);
STATUS sataXbdDevDelete (void *);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbSataXbdh */

