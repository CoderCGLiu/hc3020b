/* vxbSdStorage.h - MMC driver header file for vxBus */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,01dec12,e_d  created
*/

#ifndef __INCvxbSdStorageh
#define __INCvxbSdStorageh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

/* includes */

#include <../src/hwif/h/storage/blkXbd.h>


/* SD/MMC XBD service task priority */

#define SDMMC_XBD_SVC_TASK_PRI      50

/* forward declarations */

#define SDCARD_NAME                 "sdStorage"
#define SDHC_MAX_RW_SECTORS         2048

/* SSR define */

typedef struct
    {
    UINT8 ssrData[64];
    } SD_SSR;

/* SD card */

typedef struct sdInfo
    {
    UINT32          sdSpec;             /* SD spec version */
    UINT32          sdSec;              /* SD security verison */
    UINT32          auSize;             /* AU size */
    UINT32          eraseSize;          /* Erase size */
    UINT32          eraseTimeout;       /* Erase timeout */
    UINT32          eraseOffset;        /* Erase offset */       
    UINT8           speedClass;         /* Class speed */
    BOOL            dat4Bit;            /* DAT bus width */
    SD_CID          cid;                /* CID register */
    SDMMC_CSD       csd;                /* CSD register */
    SDMMC_SCR       scr;                /* SCR register */
    SD_SSR          ssr;                /* SSR register */  
    UINT16          dsr;                /* DSR register */
    } SD_INFO;

typedef struct sdCardCtrl
    {
    BLK_XBD_DEV     xbdDev;             /* XBD block device */
    VXB_DEVICE_ID   pInst;
    void *          pInfo;               /* detailed information */
    BOOL            attached;           /* attached to host */
    BOOL            isWp;               /* write protected */
    UINT32          idx;                /* card index */
    BOOL            highCapacity;       /* high capacity > 2GB */
    UINT32          tranSpeed;          /* transfer speed */
    BOOL            highSpeed;          /* 50 MHz */
    UINT32          readBlkLen;         /* read block length */
    UINT32          writeBlkLen;        /* write block length */
    BOOL            hasDsr;             /* DSR implemented */
    UINT64          blkNum;             /* number of blocks */
    UINT64          capacity;           /* user data capacity */
    } SD_CARD_CTRL;

#ifdef __cplusplus
        }
#endif /* __cplusplus */
    
#endif /* __INCvxbSdStorageh */

