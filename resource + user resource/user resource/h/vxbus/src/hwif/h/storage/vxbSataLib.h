/* vxbSataLib.h - Generic SATA command Library header */

/*
 * Copyright (c) 2011-2013, 2015-2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01n,18jan16,hma  add rename sata disk or patition function(VXW6-10898)
01m,21dec15,hma  fix sata tests error somteimes (VXW6-83231)
01l,17sep14,txu  add S.M.A.R.T support (VXW6-82629)
01k,21oct13,m_y  add structure sata_data_vec to avoid compile warning
01j,18jun13,m_y  add code to support XBD sched policy and TRIM feature
01i,25apr13,e_d  add cache flush command support macro. (WIND00414281)
01h,08mar13,j_z  add some ATA command defines, decrease retry counts. (WIND00403643)
01g,05sep12,sye  updated parameter type of show routines. (WIND00354585)
01f,03sep12,sye  adjusted ATAPI sense buffer to 18 bytes. (WIND00373114)
01e,10jul12,sye  fixed sataDmaTblTag type definition. (WIND00355046)
01d,25may12,e_d  added ATAPI_SENSEKEY_MASK macro. (WIND00346474)
01c,06apr12,sye  fixed compile issue when included by a CPP file. (WIND00342562)
01b,08mar12,e_d  add macro and variable with device spin-up status. (WIND00333858)
01a,18sep11,e_d  written.
*/

#ifndef __INCvxbSataLibh
#define __INCvxbSataLibh

/* includes */

#include <vxWorks.h>
#include <intLib.h>
#include <stdio.h>
#include <string.h>
#include <ioLib.h>
#include <memPartLib.h>
#include <vxBusLib.h>
#include <cacheLib.h>
#include <cacheLib.h>
#include <errnoLib.h>
#include <spinLockLib.h>
#include <hwif/util/vxbDmaBufLib.h>
#include <../src/hwif/h/storage/vxbSataXbd.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */

#define SATA_MAX_DEV                   32
#define SATA_MAX_REGBASE               6
#define IDENTIFYSIZE                   512
#define SATA_NCQDEFAULT_DEPTH          16
#define CMD_RETRY_NUM                  3

/* supported SATA driver name */

#define AHCI_DRIVER_NAME               "ahciSata"
#define FSLSATA_DRIVER_NAME            "fslSata"
#define PIIX_DRIVER_NAME               "ichAta"
#define SI31XX_DRIVER_NAME             "SI31xx"

/* max bytes transfered per PRD */

#define MAX_BYTES_PER_PRD              0x400000
#define MAX_SECTORS_FISLBA48           0xFFFF
#define MAX_SECTORS_FISNOMAL           0xFF

#define ATA_TYPE_NONE                  0x00    /* NONE device */
#define ATA_TYPE_ATA                   0x01    /* ATA device */
#define ATA_TYPE_ATAPI                 0x02    /* ATAPI device */

/* ATA command soft flag bit */

#define ATA_FLAG_SRST_ON               1 << 0
#define ATA_FLAG_SRST_OFF              1 << 1
#define ATA_FLAG_NON_SEC_DATA          1 << 2
#define ATA_FLAG_ATAPI                 1 << 3
#define ATA_FLAG_NCQ                   1 << 4
#define ATA_FLAG_WAIT_SPINUP           1 << 5
#define ATA_FLAG_NON_DATA              1 << 6
#define ATA_FLAG_OUT_DATA              1 << 7
#define ATA_FLAG_IN_DATA               1 << 8

/* ATA command define */

#define ATA_CMD_READ                   0x20
#define ATA_CMD_READ_EXT               0x24
#define ATA_CMD_READ_DMA_EXT           0x25
#define ATA_CMD_WRITE                  0x30
#define ATA_CMD_WRITE_EXT              0x34
#define ATA_CMD_WRITE_DMA_EXT          0x35
#define ATA_CMD_VERIFY                 0x40
#define ATA_CMD_VERIFY_EXT             0x42
#define ATA_CMD_FPDMA_READ             0x60
#define ATA_CMD_FPDMA_WRITE            0x61
#define ATA_CMD_EDD                    0x90
#define ATA_CMD_PACKET                 0xA0
#define ATA_CMD_ID_ATAPI               0xA1
#define ATA_CMD_SET_MULTI              0xC6
#define ATA_CMD_READ_DMA               0xC8
#define ATA_CMD_WRITE_DMA              0xCA
#define ATA_CMD_FLUSH_CACHE            0xE7
#define ATA_CMD_FLUSH_CACHE_EXT        0xEA
#define ATA_CMD_ID_ATA                 0xEC
#define ATA_CMD_SET_FEATURES           0xEF
#define ATA_CMD_READ_MULTI             0xC4
#define ATA_CMD_READ_MULTI_EXT         0x29
#define ATA_CMD_WRITE_MULTI            0xC5
#define ATA_CMD_WRITE_MULTI_EXT        0x39
#define ATA_CMD_SEEK                   0x70
#define ATA_CMD_RECALIB                0x10
#define ATA_CMD_DATA_SET_MANAGEMENT    0x06
#define ATA_DSM_TRIM                   0x01

#define ATA_CMD_READ_FPDMA_QUEUED      0x60    /* read sector NCQ */
#define ATA_CMD_WRITE_FPDMA_QUEUED     0x61    /* write sector NCQ */
#define ATA_CMD_SMART                  0xB0

/* SMART sub Commands */

#define ATA_SMART_CMD_READ_DATA         0xD0
#define ATA_SMART_CMD_READ_THRESHOLDS   0xD1
#define ATA_SMART_CMD_EN_DIS_AUTOSAVE   0xD2
#define ATA_SMART_CMD_SAVE_ATTRIBUTES   0xD3
#define ATA_SMART_CMD_OFFLINE_DIAGS     0xD4
#define ATA_SMART_CMD_ENABLE_SMART      0xD8
#define ATA_SMART_CMD_DISABLE_SMART     0xD9
#define ATA_SMART_CMD_RETURN_STATUS     0xDA

/* SMART Defines */

#define ATA_SMART_OK                    0
#define ATA_SMART_EXCEEDED              1
#define ATA_SMART_CNT_DIS_AUTOSAVE      0
#define ATA_SMART_CNT_EN_AUTOSAVE      0xF1

/* HPA feature */

#define ATA_CMD_READ_NATIVE_MAX        0xF8
#define ATA_CMD_READ_NATIVE_MAX_EXT    0x27
#define ATA_CMD_SET_MAX_ADDR           0xF9
#define ATA_CMD_SET_MAX_ADDR_EXT       0x37

/* Stream feature */

#define ATA_CMD_CONFIG_STREAM	       0x51
#define ATA_CMD_READ_STREAM_DMA_EXT    0x2A
#define ATA_CMD_READ_STREAM_EXT        0x2B
#define ATA_CMD_WRITE_STREAM_DMA_EXT   0x3A
#define ATA_CMD_WRITE_STREAM_EXT       0x3B

/* Security feature */

#define ATA_CMD_SECURITY_SET_PASSWORD     0XF1   /* PIO data-out, M-SECURITY */
#define ATA_CMD_SECURITY_UNLOCK           0XF2   /* PIO data-out, M-SECURITY */
#define ATA_CMD_SECURITY_ERASE_PREPARE    0XF3   /* Non-data, M-SECURITY     */
#define ATA_CMD_SECURITY_ERASE_UNIT       0XF4   /* PIO data-out, M-SECURITY */
#define ATA_CMD_SECURITY_FREEZE_LOCK      0XF5   /* Non-data, M-SECURITY     */
#define ATA_CMD_SECURITY_DISABLE_PASSWORD 0XF6   /* PIO data-out, M-SECURITY */

/* Trusted feature */

#define ATA_CMD_TRUSTED_RECV              0x5C
#define ATA_CMD_TRUSTED_RECV_DMA          0x5D
#define ATA_CMD_TRUSTED_SEND              0x5E
#define ATA_CMD_TRUSTED_SEND_DMA          0x5F

/* ATA device register */

#define ATA_DEVICE_REG_DEV             4
#define ATA_DEVICE_REG_OBS0            5
#define ATA_DEVICE_REG_LBA             6
#define ATA_DEVICE_REG_OBS1            7

/* ATA device control register */

#define ATA_CONTROL_REG_NLEN           1
#define ATA_CONTROL_REG_SRST           2
#define ATA_CONTROL_REG_HOB            7

/* FIS type define */

#define FIS_TYPE_HOSTTODEV_REG         0x27
#define FIS_TYPE_DEVTOHOST_REG         0x34
#define FIS_TYPE_DEVTOHOST_DMAACT      0x39
#define FIS_TYPE_DEVTOHOST_DMASET      0x41
#define FIS_TYPE_DEVTOHOST_DATA        0x46
#define FIS_TYPE_DEVTOHOST_BIST        0x58
#define FIS_TYPE_DEVTOHOST_PIOSET      0x5F
#define FIS_TYPE_DEVTOHOST_SETBIT      0xA1

#define FIS_TYPE_HOSTTODEV_REG_CBIT    1 << 7
#define FIS_TYPE_DEVTOHOST_REG_IBIT    1 << 6
#define FIS_TYPE_DEVTOHOST_DMASET_DBIT 1 << 5

#define FIS_TYPE_HOSTTODEV_REG_SIZE    5

#define ATAPI_SECTOR_SIZE              2048
#define ATA_SECTOR_SIZE                512

/* ATA identify info */

#define ATA_INFO_SPECCONFIG0           0x37C8
#define ATA_INFO_SPECCONFIG1           0x738C
#define ATA_INFO_MULSECTOR_MASK        0x00FF
#define ATA_LBA48_SECTOR_MASK          0x0000000000FF

#define ATA_INFO_CAPABILITY_IORDY      (1 << 11)
#define ATA_INFO_CAPABILITY_LBA        (1 << 9)
#define ATA_INFO_CAPABILITY_DMA        (1 << 8)

#define ATA_INFO_FEATURESET0_LHEAD     (1 << 6)
#define ATA_INFO_FEATURESET0_WCACHE    (1 << 5)
#define ATA_INFO_FEATURESET0_PM        (1 << 3)      
#define ATA_INFO_FEATURESET0_SECURITY  (1 << 1)      
#define ATA_INFO_FEATURESET0_SMART     (1 << 0)

#define ATA_INFO_FEATURESET1_CFLUSH     (1 << 12)
#define ATA_INFO_FEATURESET1_CFLUSH_EXT (1 << 13)
#define ATA_INFO_FEATURESET1_LBA48     (1 << 10)
#define ATA_INFO_FEATURESET1_SPINUP    (1 << 5)
#define ATA_INFO_FEATURESET1_APM       (1 << 3)
#define ATA_INFO_NCQ_SUPPORT           (1 << 8)

#define ATA_INFO_FEATURESUP2_STREAM    (1 << 4)
#define ATA_INFO_RESERVED48_TRUSTED    (1 << 0)

#define ATA_INFO_PIOMODE_MASK          0x03
#define ATA_INFO_VALID_MASK            0x07
#define ATA_WORD64_70_VALID            0x02
#define ATA_WORD88_VALID               0x04

#define ATA_SUB_ENABLE_8BIT            0x01  /* enable 8bit data transfer */
#define ATA_SUB_ENABLE_WCACHE          0x02  /* enable write cache */
#define ATA_SUB_SET_RWMODE             0x03  /* set transfer mode */
#define ATA_SUB_ENABLE_APM             0x05  /* enable advanced power management */
#define ATA_SUB_ENABLE_SPINUP          0x06  /* enable spinup */
#define ATA_SUB_ACK_SPINUP             0x07  /* set ata to spin up */
#define ATA_SUB_DISABLE_RETRY          0x33  /* disable retry */
#define ATA_SUB_SET_LENGTH             0x44  /* length of vendor specific bytes */
#define ATA_SUB_SET_CACHE              0x54  /* set cache segments */
#define ATA_SUB_DISABLE_LOOK           0x55  /* disable read look-ahead feature */
#define ATA_SUB_DISABLE_REVE           0x66  /* disable reverting to power on def */
#define ATA_SUB_DISABLE_ECC            0x77  /* disable ECC */
#define ATA_SUB_DISABLE_8BIT           0x81  /* disable 8bit data transfer */
#define ATA_SUB_DISABLE_WCACHE         0x82  /* disable write cache */
#define ATA_SUB_DISABLE_APM            0x85  /* disable advanced power management */
#define ATA_SUB_ENABLE_ECC             0x88  /* enable ECC */
#define ATA_SUB_ENABLE_RETRY           0x99  /* enable retries */
#define ATA_SUB_ENABLE_LOOK            0xaa  /* enable read look-ahead feature */
#define ATA_SUB_SET_PREFETCH           0xab  /* set maximum prefetch */
#define ATA_SUB_SET_4BYTES             0xbb  /* 4 bytes of vendor specific bytes */
#define ATA_SUB_ENABLE_REVE            0xcc  /* enable reverting to power on def */

#define ATA_SUB_ENABLE_APM_MAX         0xFE
#define ATA_RWMODE_PIO                 0x0
#define ATA_RWMODE_ADVANCEPIO          0x08
#define ATA_RWMODE_SINGEDMA            0x10
#define ATA_RWMODE_MULTDMA             0x20
#define ATA_RWMODE_ULTRADMA            0x40

/* ATAPI identify info */

#define ATAPI_CONFIG_TYPE_MASK         (0x3 << 14)
#define ATAPI_CONFIG_TYPE_ATAPI        (0x2 << 14)

/* transfer modes of sata device */

#define SATA_PIO_DEF_W                 0x00  /* PIO default trans. mode */
#define SATA_PIO_DEF_WO                0x01  /* PIO default trans. mode, no IORDY */
#define SATA_PIO_W_0                   0x08  /* PIO flow control trans. mode 0 */
#define SATA_PIO_W_1                   0x09  /* PIO flow control trans. mode 1 */
#define SATA_PIO_W_2                   0x0a  /* PIO flow control trans. mode 2 */
#define SATA_PIO_W_3                   0x0b  /* PIO flow control trans. mode 3 */
#define SATA_PIO_W_4                   0x0c  /* PIO flow control trans. mode 4 */
#define SATA_DMA_SINGLE_0              0x10  /* singleword DMA mode 0 */
#define SATA_DMA_SINGLE_1              0x11  /* singleword DMA mode 1 */
#define SATA_DMA_SINGLE_2              0x12  /* singleword DMA mode 2 */
#define SATA_DMA_MULTI_0               0x20  /* multiword DMA mode 0 */
#define SATA_DMA_MULTI_1               0x21  /* multiword DMA mode 1 */
#define SATA_DMA_MULTI_2               0x22  /* multiword DMA mode 2 */
#define SATA_DMA_ULTRA_0               0x40  /* ultra DMA mode 0 */
#define SATA_DMA_ULTRA_1               0x41  /* ultra DMA mode 1 */
#define SATA_DMA_ULTRA_2               0x42  /* ultra DMA mode 2 (UDMA/33) */
#define SATA_DMA_ULTRA_3               0x43  /* ultra DMA mode 3 */
#define SATA_DMA_ULTRA_4               0x44  /* ultra DMA mode 4 (UDMA/66) */
#define SATA_DMA_ULTRA_5               0x45  /* ultra DMA mode 5 (UDMA/100) */
#define SATA_DMA_ULTRA_6               0x46  /* ultra DMA mode 5 (UDMA/133) */

/* ATAPI commands */

#define ATAPI_CMD_DEVICE_RESET         0x08    /* ATAPI Soft Reset */
#define ATAPI_CMD_PKTCMD               0xA0    /* ATAPI Pakcet Command */
#define ATAPI_CMD_IDENTD               0xA1    /* ATAPI Identify Device */
#define ATAPI_CMD_SERVICE              0xA2    /* Service */

/* CDROM commands */

#define ATAPI_CDROM_CMD_TEST_READY     0x00     /* CDROM Test Unit Ready      */
#define ATAPI_CDROM_CMD_REQUEST_SENSE  0x03     /* CDROM Request Sense        */
#define ATAPI_CDROM_CMD_INQUIRY        0x12     /* CDROM Inquiry              */
#define ATAPI_CDROM_CMD_STARTUNIT      0x1B     /* CDROM Start unit */
#define ATAPI_CDROM_CMD_READ_CDROM_CAP 0x25     /* CDROM Read CD-ROM Capacity */
#define ATAPI_CDROM_CMD_READ_10        0x28     /* CDROM Read (10) */
#define ATAPI_CDROM_CMD_LOAD           0xA6     /* CDROM LOAD/UNLOAD */
#define ATAPI_CDROM_CMD_READ_12        0xA8     /* CDROM Read (12) */
#define ATAPI_CDROM_CMD_MECHANISMSTS   0xBD     /* CDROM MECHANISM Status */

#define ATAPI_CDROM_CMD_STARTUNIT_STOP 0x0      /* start unit command to stop */
#define ATAPI_CDROM_CMD_STARTUNIT_STAR 0x1      /* start unit command to start */
#define ATAPI_CDROM_CMD_STARTUNIT_LOAD 0x3      /* start unit command to load */

#define ATAPI_SENSEKEY_MASK            0xF      /* sense key mask */
#define ATAPI_SENSEKEY_NOSENSE         0x0      /* sense key no sense */

#define ATAPI_SENSE_LEN                18       /* sense code buffer len */
#define ATAPI_MECHANISM_LEN            8        /* mechanism buffer len */
#define ATAPI_READCAPACITY_LEN         8        /* capacity buffer len */

#define FIS_ATA_CMDLEN                 64
#define FIS_ATAPI_CMDLEN               16

#define USE_LBA                        0x40   /* used to 'OR' into Dev/Head register */

#define ATA_PIO_MULTI                  0x2000 /* RW PIO multi sector        */

#define ATA_SET_PIO_MODE_0             0x0
#define ATA_SET_PIO_MODE_1             0x1
#define ATA_SET_PIO_MODE_2             0x2
#define ATA_SET_PIO_MODE_3             0x3
#define ATA_SET_PIO_MODE_4             0x4

/* FIS struct define */

typedef struct
    {
    UINT8      fisAtaCmd[FIS_ATA_CMDLEN];
    UINT8      fisApatiCmd[FIS_ATAPI_CMDLEN];
    UINT16     fisCmdLen;
    UINT16     fisCmdFlag;
    }FIS_CMD;

/* SATA data transfered */

typedef struct
    {
    void *      buffer;
    UINT32      blkNum;
    UINT32      blkSize;
    UINT32      tagSet;
    } SATA_DATA;

typedef struct sata_data_vec {
    bus_addr_t iov_base; /* Base address. */
    size_t     iov_len;  /* Length. */
}SATA_DATA_VEC, *pSATA_DATA_VEC;

#define ATA_MAX_SG_CNT              256
#define ATA_MAX_LBA_ENTRY           256


typedef struct
    {
    UINT32      tagSet;
    UINT32      flag;
    UINT32      startBlk;    
    UINT32      bufCnt;
    SATA_DATA_VEC  bufVec[ATA_MAX_SG_CNT];
    } SATA_DATA_EXT;

struct lbavec
    {
    UINT32  lbaL;
    UINT16  lbaH;
    UINT16  blkCnt; 
    };

typedef struct 
    {
    UINT32 cnt;
    struct lbavec lbaEntry[ATA_MAX_LBA_ENTRY];    
    }ATA_TRIM_PARAMETER, *pATA_TRIM_PARAMETER;

typedef struct ata_cmd_desc
    {
	UINT32		    flags;
    UINT32          tag;
	UINT8			protocol;	
	UINT8			control;		    
	UINT8			featureExp;	
	UINT8			nsectExp;	    
	UINT8			lbaLowExp;
	UINT8		    lbaMidExp;
	UINT8			lbaHighExp;
	UINT8			feature;
	UINT8			nsect;
	UINT8			lbaLow;
	UINT8			lbaMid;
	UINT8			lbaHigh;
	UINT8			device;
	UINT8			command;	/* IO operation */
    }ATA_CMD_DESC, *pATA_CMD_DESC;

typedef struct sg_list
    {
    int          bufCnt;
    SATA_DATA_VEC bufVec[ATA_MAX_SG_CNT];
    }SG_LIST, *pSG_LIST;

/* ATA register define */

typedef struct
    {
    UINT8      resvData0[2];
    UINT8      command;
    UINT8      feature;
    UINT8      lbaLow;
    UINT8      lbaMid;
    UINT8      lbaHigh;
    UINT8      device;
    UINT8      lbaLowExp;
    UINT8      lbaMidExp;
    UINT8      lbaHighExp;
    UINT8      featureExp;
    UINT8      sector;
    UINT8      sectorExp;
    UINT8      resvData1;
    UINT8      control;
    } ATA_REG;

typedef union
    {
    ATA_REG    ataReg;
    FIS_CMD    fisCmd;
    }FIS_ATA_REG;

typedef struct
    {
    UINT16 config;                 /* general configuration */
    UINT16 cylinders;              /* number of cylinders */
    UINT16 specWord;               /* spec word */
    UINT16 heads;                  /* number of heads */
    UINT16 bytesTrack;             /* number of unformatted bytes/track */
    UINT16 bytesSec;               /* number of unformatted bytes/sector */
    UINT16 sectors;                /* number of sectors/track */
    UINT16 bytesGap;               /* minimum bytes in intersector gap */
    UINT16 bytesSync;              /* minimum bytes in sync field */
    UINT16 vendstat;               /* number of words of vendor status */
    char   serial[20];             /* controller serial number */
    UINT16 type;                   /* controller type */
    UINT16 size;                   /* sector buffer size, in sectors */
    UINT16 bytesEcc;               /* ecc bytes appended */
    char   rev[8];                 /* firmware revision */
    char   model[40];              /* model name */
    UINT16 multiSecs;              /* RW multiple support. bits 7-0 ia max secs */
    UINT16 reserved48;             /* reserved */
    UINT16 capabilities;           /* capabilities */
    UINT16 reserved50;             /* reserved */
    UINT16 pioMode;                /* PIO data transfer cycle timing mode */
    UINT16 dmaMode;                /* single word DMA data transfer cycle timing */
    UINT16 valid;                  /* field validity */
    UINT16 currentCylinders;       /* number of current logical cylinders */
    UINT16 currentHeads;           /* number of current logical heads */
    UINT16 currentSectors;         /* number of current logical sectors / track */
    UINT16 capacity0;              /* current capacity in sectors */
    UINT16 capacity1;              /* current capacity in sectors */
    UINT16 multiSet;               /* multiple sector setting */
    UINT16 sectors0;               /* total number of user addressable sectors */
    UINT16 sectors1;               /* total number of user addressable sectors */
    UINT16 singleDma;              /* single word DMA transfer */
    UINT16 multiDma;               /* multi word DMA transfer */
    UINT16 advancedPio;            /* flow control PIO transfer modes supported */
    UINT16 cycletimeDma;           /* minimum multiword DMA transfer cycle time */
    UINT16 cycletimeMulti;         /* recommended multiword DMA cycle time */
    UINT16 cycletimePioNoIordy;    /* min PIO transfer cycle time wo flow ctl */
    UINT16 cycletimePioIordy;      /* min PIO transfer cycle time w IORDY */
    UINT16 reserved69;             /* reserved */
    UINT16 reserved70;             /* reserved */
    UINT16 pktCmdRelTime;          /* [71]Typical Time for Release after Packet */
    UINT16 servCmdRelTime;         /* [72]Typical Time for Release after SERVICE */
    UINT16 reservedPacketID[2];    /* reserved for packet id*/
    UINT16 queueDepth;             /* maximum queue depth - 1 */
    UINT16 sataCapabilities;       /* [76] SATA capabilities */
    UINT16 reserved77;             /* reserved */
    UINT16 sataFeaturesSupported;  /* [78] SATA features Supported */
    UINT16 sataFeaturesEnabled;    /* [79] SATA features Enabled */
    UINT16 majorRevNum;            /* Major ata version number */
    UINT16 minorVersNum;           /* minor version number */
    UINT16 featuresSupported0;     /* [82] Features Supported word 0 */
    UINT16 featuresSupported1;     /* [83] Features Supported word 1 */
    UINT16 featuresSupported2;     /* [84] Features Supported word 2 */
    UINT16 featuresEnabled0;       /* [85] Features Enabled word 0 */
    UINT16 featuresEnabled1;       /* [86] Features Enabled word 1 */
    UINT16 featuresEnabled2;       /* [87] Features Enabled word 2 */
    UINT16 ultraDma;               /* ultra DMA transfer */
    UINT16 timeErase;              /* time to perform security erase */
    UINT16 timeEnhancedErase;      /* time to perform enhanced security erase */
    UINT16 currentAPM;             /* current power management value*/
    UINT16 passwordRevisionCode;   /* master password revision code */
    UINT16 resetResult;            /* result of last reset */
    UINT16 acousticManagement;     /* current and recommended acoustic management values */
    UINT16 reserved95[5];          /* reserved */
    UINT16 lba48Size[4];           /* 48-bit LBA size (stored little endian?) */
    UINT16 reserved104[23];        /* reserved */
    UINT16 removableFeatureSupport;/* removable media status notification feature set support */
    UINT16 securityStatus;         /* security status */
    UINT16 vendor[31];             /* vendor specific */
    UINT16 cfaPowerMode1;          /* cfa power model*/
    UINT16 reserved161[15];        /* reserved for assignment by Compact Flash Association */
    UINT16 currentMediaSN[30];     /* Current Media Serial Number */
    UINT16 reserved206[49];        /* reserved */
    UINT16 checksum;               /* integrity word */
    } ATA_IDENTIFY_INFO;

typedef struct
    {
    FUNCPTR         cmdIssue;       /* issue command */
    FUNCPTR         xferReq;
    CACHE_FUNCS     cacheFuncs;
    } SATA_HOST_OPS;

/* sata host controller define */

typedef struct
    {
    VXB_DEVICE_ID   pDev;            /* vxBus device ID */
    void *          regBase[SATA_MAX_REGBASE]; /* register adrs */
    void *          regHandle;       /* register handle function */
    void *          pioHandle;       /* pio handle function */
    void *          sataDev[SATA_MAX_DEV]; /* pointer to sata device struct */
    void *          pCtrlExt;        /* point to ctrl ext sturct if we need */
    SATA_HOST_OPS   ops;             /* sata ops function */
    UINT32          irq;             /* irq number */
    UINT32          numPorts;        /* port number */
    int             numImpPorts;     /* impport number */
    int             PortsBaseIndex;
    int             numCommandSlots; /* command slot number */
    int             numCtrl;         /* ctrl number */
    BOOL            installed;       /* installed */
    BOOL            okNcq;           /* reserved for NCQ */
    int  *          configType;      /* or'd configuration flags     */
    int             svcTaskCount;    /* number of service tasks per drive */
    VXB_DMA_TAG_ID  sataHostDmaTag;  /* sata host controller dma map tag */
    VXB_DMA_TAG_ID  sataHostDmaParentTag; /* sata host dma map parent tag */
    VXB_DMA_TAG_ID  sataDmaTblTag;   /* sata pRD memory map tag */
    VXB_DMA_MAP_ID  sataDmaTblMap;   /* sata pRD memory map */
    VXB_DMA_TAG_ID  sataCmdListTag;  /* sata command list tag */
    VXB_DMA_MAP_ID  sataCmdListMap;  /* sata command list map */
    VXB_DMA_TAG_ID  userBufTagId;    /* User buf tag ID */
    void *          pCmdList;
    LIST            freeReqPrivList;
    SEM_ID          sataCmdSem;
    } SATA_HOST,*pSATA_HOST;

#define XBD_REQ_MAP    (0x1)     /* Means the req already mapped */

typedef struct 
    {
    NODE               listNode;     /* list node */
    pSATA_HOST         pSataHost;    /* pointer to sata host */
    XBD_REQUEST *      pXbdReq;      /* Cur request */    
    void *             pDriveSpec;   /* pointer to the driver specifical */
    VXB_DMA_MAP_ID     usrBufMapId;  /* user buffer map id */
    ATA_CMD_DESC       ataCmdDesc;   /* ata cmd description */
    SG_LIST            sgList;       /* SG list */
    int                cmdTag;       /* Command tag slot */
    int                flag;         /* flag */
    }XBD_REQ_PRIV, *pXBD_REQ_PRIV;

#define LIST_NOEE_TO_XBD_REQ_PRIV(pNode)       \
         ((XBD_REQ_PRIV *) ((char *) (pNode) - OFFSET(XBD_REQ_PRIV, listNode)))


/* sata device define */

typedef struct
    {
    SATA_HOST *         host;           /* pointer to host */
    SATA_XBD_DEV        xbdDev;         /* XBD block device */
    ATA_IDENTIFY_INFO   info;           /* detailed information */
    FIS_ATA_REG         fisAtaReg;      /* ata register map */
    BOOL                okMulti;        /* MULTI: TRUE if supported */
    BOOL                okIordy;        /* IORDY: TRUE if supported */
    BOOL                okDma;          /* DMA:   TRUE if supported */
    BOOL                okLba;          /* LBA:   TRUE if supported */
    BOOL                okLba48;        /* 48-bit LBA: TRUE if supported */
    BOOL                attached;       /* device attached */
    BOOL                okNcq;          /* device NCQ support or not */
    BOOL                okSpin;         /* device spin up or not */
    UINT8               type;           /* device type */
    UINT8               diagnosticCode; /* device diagnostic code */
    UINT8               mechanismSts[ATAPI_MECHANISM_LEN]; /* mechanism status buffer */
    UINT8               senseCode[ATAPI_SENSE_LEN];        /* sense code */
    UINT16              capability;     /* device capability */
    UINT16              commandSet0;    /* device info for commandset 0 word */
    UINT16              commandSet1;    /* device info for commandset 1 word */
    UINT16              multiSecs;      /* supported max sectors for multiple RW */
    UINT16              pioMode;        /* supported max PIO mode */
    UINT16              singleDmaMode;  /* supported max single word DMA mode */
    UINT16              multiDmaMode;   /* supported max multi word DMA mode */
    UINT16              ultraDmaMode;   /* supported max ultra DMA mode */
    UINT16              rwMode;         /* RW mode: PIO[0,1,2,3,4] or DMA[0,1,2] */
    UINT16              rwFlag;         /* rw mode: PIO or singe DMA or ultra DMA */
    UINT16              ncqDepth;       /* reserved for NCQ */
    UINT32              idx;            /* device idx */
    UINT32              cylinders;      /* number of cylinders */
    UINT32              heads;          /* number of heads */
    UINT32              sectors;        /* number of sectors per track */
    UINT32              bytes;          /* number of bytes per sector */
    UINT32              num;            /* port num */
    UINT32              prdMaxSize;     /* Max PRD size */
    UINT32              fisMaxSector;   /* Max sectors for FIS */
    UINT32              tagBit;         /* reserved for mul tag */
    UINT64              totalSecNum48;  /* total sector size */
    UINT32              maxActiveReqs;  /* max active request */
    UINT32              maxBiosPerReq;  /* max bios per request */
    UINT32              maxXferBlks;    /* max transfer blocks */
    VXB_DMA_MAP_ID      sataDmaMap;     /* dma map id */
    UINT32              logicPortNum;    /* physical port number */
    } SATA_DEVICE;

#if _BYTE_ORDER == _BIG_ENDIAN

#   define SWAP32(x)            ((((x) & 0x000000ff) << 24) |  \
                                (((x) & 0x0000ff00) <<  8)   |  \
                                (((x) & 0x00ff0000) >>  8)   |  \
                                (((unsigned int)(x)) >> 24))

#   define SWAP16(x)            ((((x) & 0x00ff) << 8) |  \
                                (((x) & 0xff00) >> 8))
#   define SWAP32_BE(x)         (x)
#   define SWAP16_BE(x)         (x)
#else /* _BYTE_ORDER == _BIG_ENDIAN */
#   define SWAP32(x) (x)
#   define SWAP16(x) (x)
#   define SWAP32_BE(x)         ((((x) & 0x000000ff) << 24) |  \
                                (((x) & 0x0000ff00) <<  8)  |  \
                                (((x) & 0x00ff0000) >>  8)  |  \
                                (((unsigned int)(x)) >> 24))

#   define SWAP16_BE(x)         ((((x) & 0x00ff) << 8) |  \
                                (((x) & 0xff00) >> 8))
#endif /* _BYTE_ORDER == _BIG_ENDIAN */

/* function declarations */

STATUS sataIdentify (SATA_DEVICE * device);
STATUS sataFeatureSet (SATA_DEVICE * device, UINT8 feature);
void   sataSoftResetSet (SATA_DEVICE * device, BOOL isRst);
STATUS sataDevDiagnostic (SATA_DEVICE * device);
STATUS sataFlushCache (SATA_DEVICE * device);
STATUS sataAtapiTestReady (SATA_DEVICE * device);
STATUS sataAtapiReadCapacity (SATA_DEVICE * device);
STATUS sataAtapiReadStatus (SATA_DEVICE * device);
STATUS sataAtapiLoadDisc (SATA_DEVICE * device);
STATUS sataAtapiReadSense (SATA_DEVICE * device);
STATUS sataBlkWrite (SATA_DEVICE * device, sector_t blkNo,
                        UINT32 numBlks, UINT8 * pBuf);
STATUS sataBlkRead (SATA_DEVICE * device, sector_t blkNo,
                        UINT32 numBlks, UINT8 * pBuf);
STATUS sataSetMulSector (SATA_DEVICE * device);
STATUS sataDevIdxAlloc (SATA_DEVICE * device);
STATUS sataAtapiNormalInit (SATA_DEVICE * device);
STATUS sataAtaNormalInit (SATA_DEVICE * device);
void sataDevIdxFree (SATA_DEVICE * device);

void sataHostShow (VXB_DEVICE_ID pDev);
void sataDeviceShow (VXB_DEVICE_ID pDev, UINT32 portNum);
UINT32 ataMajorVerGet
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataTrimSupport
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataZeroReadAfterTrim
    (
    ATA_IDENTIFY_INFO *pInfo
    );
BOOL ataCheckSSD
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataTrustedSupport
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataSecuSupport
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataLLSCheck
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
UINT16 ataPhySectors
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
UINT16 ataLLSAlign
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataStreamSupport
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
BOOL ataHpaEnabled
    (
    ATA_IDENTIFY_INFO *   pInfo
    );
STATUS sataReadNativeMaxAddr
    (
    SATA_DEVICE *  device,
    UINT64      *  pMaxSector
    );
STATUS sataSetMaxAddr
    (
    SATA_DEVICE * device,
    UINT64        maxSector
    );
STATUS sataDsmTrim
    (
    SATA_DEVICE *       device,
    SECTOR_RANGE *      pRange,
    int                 cnt
    );
void sataXbdReqDone
    ( 
    XBD_REQUEST *  pXbdReq,
    unsigned       perStatus    
    );
UINT8 sataRwCmdGet
    (
    SATA_DEVICE *    pSataDevice,
    BOOL             bWrite
    );
void sataCmdDescToFis
    (
    pATA_CMD_DESC    pCmdDesc,
    UINT8 *          fis
    );
void sataRwCmdDescBuild
    (
    SATA_DEVICE *  pSataDevice,
    pATA_CMD_DESC  pCmdDesc,
    UINT64         lba,
    UINT16         ataCount,     
    UINT16         ataFeature,
    UINT8          ataCmd,
    UINT8          tag
    );
void sataDevXferReq
    (
    SATA_XBD_DEV * pSataXbd,
    XBD_REQUEST *  pXbdReq
    );
bus_addr_t ataBusAddrGet
    (
    XBD_REQUEST *  pXbdReq,
    int            bioIdx
    );
UINT32 ataBufLenGet
    (
    XBD_REQUEST *  pXbdReq,
    int            bioIdx
    );
pXBD_REQ_PRIV ataReqPrivGet
    (    
    pSATA_HOST      pSataHost,
    int             flag
    );
void ataReqPrivRelease
    (
    pSATA_HOST      pSataHost,     
    pXBD_REQ_PRIV   pReqPriv
    );
void ataMapUnLoad
    (
    pSATA_HOST    pSataHost,     
    XBD_REQUEST * pXbdReq
    );
STATUS ataMapLoad
    (
    pSATA_HOST     pSataHost,     
    XBD_REQUEST *  pXbdReq
    );

STATUS sataSmartEnable
    (
    SATA_DEVICE * device
    );
STATUS sataSmartDisable
    (
    SATA_DEVICE * device
    );
STATUS sataSmartSaveAttributes
    (
    SATA_DEVICE * device
    );
STATUS sataSmartAutosaveEnable
    (
    SATA_DEVICE * device
    );
STATUS sataSmartAutosaveDisable
    (
    SATA_DEVICE * device
    );
STATUS sataSmartOfflineDiag
    (
    SATA_DEVICE * device,
    UINT8 subcmd
    );
STATUS sataSmartReturnStatus
    (
    SATA_DEVICE * device,
    void        * pStat
    );
BOOL sataSmartIsEnabled
    (
    SATA_DEVICE * device,
    void        * arg
    );
STATUS sataSmartReadData
    (
    SATA_DEVICE * device,
    void *buffer
    );
STATUS sataSmartReadThresholds
    (
    SATA_DEVICE * device,
    void *buffer
    );
STATUS sataParmsGet
    (
    SATA_DEVICE * device,
    ATA_IDENTIFY_INFO  *parms
    );
void sataSmartShow
    (
    VXB_DEVICE_ID pDev,
    UINT32        portNum
    );
    
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbSataLibh */

