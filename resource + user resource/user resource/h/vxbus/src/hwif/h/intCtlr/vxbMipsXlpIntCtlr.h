/* vxbMipsXlpIntCtlr.h - interrupt controller head file for NetLogic XLP family */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/* 
modification history
--------------------
01b,12jul11,l_z  add SMP/AMP support
01a,25feb11,x_f  created
*/

/*
DESCRIPTION

This file supplies common structures and defines for XLP IO management 
and XLP interrupt controller.
*/

#ifndef __INCvxbMipsXlpIntCtlrh
#define __INCvxbMipsXlpIntCtlrh

#include <vxIpiLib.h>

/* typedefs */

typedef void   (*INT_HANDLER_FUNC)	(void * pArg);

typedef struct intInputTable
    {
    char * pName;
    int    busNum;
    int    devNum;
    int    funcNum;
    int    unitNum;
    int    index;
    int    irtIndex;
    int    intVector;
    int    cpu;
    } INT_INPUT_TABLE;

typedef struct intCtlrParameter
    {
    VXB_DEVICE_ID    pInst;
    int              pinInput;
    int              pinOutput;
    int              cpu;
    int              phyCpu;
    INT_HANDLER_FUNC routine;        /* routine to be called */
    void *           parameter;      /* parameter to be passed to routine */
    int              intNum;
    } INT_CTLR_PARAM;

typedef STATUS (*XLP_CONNECT_FUNC)(VXB_DEVICE_ID, int, int, int); 
typedef STATUS (*XLP_DISCONN_FUNC)(VXB_DEVICE_ID, int, int);
typedef STATUS (*XLP_ENABLE_FUNC )(VXB_DEVICE_ID, int, int);
typedef STATUS (*XLP_DISABLE_FUNC)(VXB_DEVICE_ID, int, int);
typedef VOID   (*XLP_ACK_FUNC)(INT_CTLR_PARAM *);
typedef VOID   (*XLP_IPI_EMIT_FUNC)(VXB_DEVICE_ID, int, int);

typedef struct vxbXlpIntCtlrFunc
    {
    XLP_CONNECT_FUNC    xlpIntCtlrHwConnect;
    XLP_DISCONN_FUNC    xlpIntCtlrHwDisconnect;
    XLP_ENABLE_FUNC     xlpIntCtlrHwEnable;
    XLP_DISABLE_FUNC    xlpIntCtlrHwDisable;
    XLP_ACK_FUNC        xlpIntCtlrHwAck;
    XLP_IPI_EMIT_FUNC   xlpIntCtlrHwIpiEmit;
    } VXB_XLP_INT_CTLR_FUNC;

typedef VXB_XLP_INT_CTLR_FUNC * (* xlpIntCtlrControlGetPrototype) (void);

#define V_PIN_OUTPUTS_PER_CPU    64  /* Number of interrupt lines per cpu */
#define V_NUM_PIN_INPUTS         160 /* Number of hardware interrupt sources */

#define V_NUM_IPI                32  /* Number of possible IPI types */

#define XLP_INT_NUM_CPU_OFFSET(cpu)     ((cpu) * V_PIN_OUTPUTS_PER_CPU)

#define XLP_NUM_CPUS             (pDrvCtrl->xlpCpuNum)

#endif /* __INCvxbMipsXlpIntCtlrh */

