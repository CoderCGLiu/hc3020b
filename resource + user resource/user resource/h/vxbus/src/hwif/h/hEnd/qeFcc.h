/* qeFcc.h - Fast Communications Controller header file for Quicc Engine */

/* Copyright (c) 2006 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,12mar06,dtr  created 
*/

/*
 * This file contains constants for the Fast Communications Controllers
 * (FCCs) in the Quicc Engine
 */

#ifndef __INCqeFcch
#define __INCqeFcch

#ifdef __cplusplus
extern "C" {
#endif

#ifndef M8260ABBREVIATIONS
#define M8260ABBREVIATIONS

#ifdef  _ASMLANGUAGE
#define CAST(x)
#else /* _ASMLANGUAGE */
typedef volatile UCHAR VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32 VINT32; /* volatile unsigned word */
typedef volatile INT16 VINT16; /* volatile unsigned halfword */
typedef volatile INT8 VINT8;   /* volatile unsigned byte */
typedef volatile UINT32 VUINT32; /* volatile unsigned word */
typedef volatile UINT16 VUINT16; /* volatile unsigned halfword */
typedef volatile UINT8 VUINT8;   /* volatile unsigned byte */
#define CAST(x) (x)
#endif  /* _ASMLANGUAGE */

#endif /* M8260ABBREVIATIONS */


/* FCC register set - use QE_FCC_<read/write>*/
 
#define QE_FCC_GMR  0x00 /* Gen Mode */
#define QE_FCC_PSMR 0x04 /* Prot Spec */
#define QE_FCC_TODR 0x08 /* transmit  on demand */
#define QE_FCC_DSR  0x0C   /* data sync */
#define QE_FCC_CCER 0x10  /* event */
#define QE_FCC_CCMR 0x14  /* mask */
#define QE_FCC_CCSR 0x18  /* status */

/* New feature registers */
#define QE_FCC_URFB                     0x20                 
#define QE_FCC_URFS                     0x24 
#define QE_FCC_URFET                    0x28
#define QE_FCC_URFSET                   0x2A
#define QE_FCC_UTFB                     0x2C
#define QE_FCC_UTFS                     0x30
#define QE_FCC_UTFET                    0x34
#define QE_FCC_UTFTT                    0x38
#define QE_FCC_UTPT                     0x3C
#define QE_FCC_URTRY                    0x40
#define QE_FCC_GUEMR                    0x90

/* MII/PHY/MAC  registers */
#define QE_FCC_MACCFG1	                0x100
#define QE_FCC_MACCFG2			0x104
#define QE_FCC_IPGIFG		        0x108
#define QE_FCC_HAFDUP			0x10c
#define QE_FCC_EMTR                     0x11c
#define QE_FCC_MIIMCFG			0x120
#define QE_FCC_MIIMCOM			0x124
#define QE_FCC_MIIMADD			0x128
#define QE_FCC_MIIMCON			0x12c
#define QE_FCC_MIIMSTAT			0x130
#define QE_FCC_MIIMIND			0x134
#define QE_FCC_IFCTL			0x138
#define QE_FCC_IFSTAT			0x13c
#define QE_FCC_MACSTNADDR1	       	0x140
#define QE_FCC_MACSTNADDR2	       	0x144
#define QE_FCC_TBIPA                    0x154


/* Stats counters */
#define QE_FCC_TX64                     0x180
#define QE_FCC_TX127			0x184
#define QE_FCC_TX255			0x188
#define QE_FCC_RX64			0x18C
#define QE_FCC_RX127			0x190
#define QE_FCC_RX255			0x194
#define QE_FCC_TXOK			0x198
#define QE_FCC_TXCF			0x19C
#define QE_FCC_TMCA			0x1A0
#define QE_FCC_TBCA			0x1A4
#define QE_FCC_RXFOK			0x1A8
#define QE_FCC_RBYT			0x1B0
#define QE_FCC_RXBOK			0x1AC
#define QE_FCC_RMCA			0x1B4
#define QE_FCC_RBCA			0x1B8
#define QE_FCC_SCAR			0x1BC
#define QE_FCC_SCAM			0x1C0

						/* in Internal RAM */
/* FCC Dual-Ported RAM definitions use QE_FCC_PRAM_<read/write>*/
 
#define QE_FCC_PRAM_RBASE	0x0		/* rx FIFO pointer offset */
#define QE_FCC_PRAM_TBASE	0x2		/* tx FIFO pointer offset */
#define QE_FCC_PRAM_RBMR	0x4		/* bus mode register */
#define QE_FCC_PRAM_TBMR     0x5		/* bus mode register */
#define QE_FCC_PRAM_MRBLR	0x6		/* max rx buffer length */
#define QE_FCC_PRAM_RSTATE	0x8
#define QE_FCC_PRAM_TSTATE	0x18		/* tx internal state */
#define QE_FCC_PRAM_NEXT_RBASE 0xC		/* RBD base address */
#define QE_FCC_PRAM_RBPTR	0x10		/* RBD pointer */
#define QE_FCC_PRAM_TBPTR	0x20		/* TBD pointer */
#define QE_FCC_PRAM_RCRC	0x28		/* temp rx CRC */
#define QE_FCC_PRAM_TCRC	0x2C		/* temp tx CRC */

#define QE_FCC_PRAM_STATBUF	0x3C		/* internal buffer */
#define QE_FCC_PRAM_CAM_PTR	0x40		/* CAM address */
#define QE_FCC_PRAM_C_MASK	0x44		/* MASK for CRC */
#define QE_FCC_PRAM_C_PRES	0x48		/* preset CRC */
#define QE_FCC_PRAM_CRCEC	0x4C		/* CRC error counter */
#define QE_FCC_PRAM_ALEC	0x50		/* alignment error counter */
#define QE_FCC_PRAM_DISFC	0x54		/* discard frame counter */
#define QE_FCC_PRAM_RET_LIM	0x58		/* retry limit */
#define QE_FCC_PRAM_RET_CNT	0x5A		/* retry limit counter */
#define QE_FCC_PRAM_P_PER	0x5C		/* persistence */
#define QE_FCC_PRAM_BOFF_CNT	0x5E		/* backoff counter */
#define QE_FCC_PRAM_GADDR_H	0x60		/* group address filter high */
#define QE_FCC_PRAM_GADDR_L	0x64		/* group address filter low */
#define QE_FCC_PRAM_TFCSTAT	0x68		/* out-of-sequence TBD stat */
#define QE_FCC_PRAM_TFCLEN	0x6A		/* out-of-sequence TBD length */
#define QE_FCC_PRAM_TFCPTR	0x6C		/* out-of-sequence TBD pointer*/
#define QE_FCC_PRAM_MFLR	0x70		/* max receive frame length */
#define QE_FCC_PRAM_PADDR_H	0x72		/* individual address high */
#define QE_FCC_PRAM_PADDR_M	0x74		/* individual address medium */
#define QE_FCC_PRAM_PADDR_L	0x76		/* individual address low */
#define QE_FCC_PRAM_IBD_CNT	0x78		/* internal BD counter */
#define QE_FCC_PRAM_IBD_START	0x7A		/* internal BD start pointer */
#define QE_FCC_PRAM_IBD_END	0x7C		/* internal BD end pointer */
#define QE_FCC_PRAM_TX_LEN	0x7E		/* tx frame length counter */
#define QE_FCC_PRAM_IBD_BASE	0x80		/* internal BD base */
#define QE_FCC_PRAM_IADDR_H	0xA0		/* individual addr filter high*/
#define QE_FCC_PRAM_IADDR_L	0xA4		/* individual addr filter low */
#define QE_FCC_PRAM_MINFLR	0xA8		/* min frame lenght */
#define QE_FCC_PRAM_TADDR_H	0xAA		/* set hash table addr high */
#define QE_FCC_PRAM_TADDR_M	0xAC		/* set hash table addr medium */
#define QE_FCC_PRAM_TADDR_L	0xAE		/* set hash table addr low */
#define QE_FCC_PRAM_PAD_PTR	0xB0		/* internal PAD pointer */
#define QE_FCC_PRAM_RES2	0xB2		/* reserved */
#define QE_FCC_PRAM_CF_RANGE	0xB4		/* control frame range */
#define QE_FCC_PRAM_MAX_B	0xB6		/* max BD byte counter */
#define QE_FCC_PRAM_MAXD1	0xB8		/* max DMA1 lenght */
#define QE_FCC_PRAM_MAXD2	0xBA		/* max DMA2 lenght */
#define QE_FCC_PRAM_MAXD	0xBC		/* rx max DMA lenght */
#define QE_FCC_PRAM_DMA_CNT	0xBE		/* rx DMA counter */
#define QE_FCC_PRAM_OCTC	0xC0		/* data octets number */
#define QE_FCC_PRAM_COLC	0xC4		/* collision estimate */
#define QE_FCC_PRAM_BROC	0xC8		/* received broadast packets */
#define QE_FCC_PRAM_MULC	0xCC		/* received multicast packets */
#define QE_FCC_PRAM_USPC	0xD0		/* good packets shorter than */
						/* 64 bytes */
#define QE_FCC_PRAM_FRGC	0xD4		/* bad packets shorter than */
						/* 64 bytes */
#define QE_FCC_PRAM_OSPC	0xD8		/* good packets longer than */
						/* 1518 bytes */
#define QE_FCC_PRAM_JBRC	0xDC		/* bad packets longer than */
						/* 1518 bytes */
#define QE_FCC_PRAM_P64C	0xE0		/* packets 64-byte long */
#define QE_FCC_PRAM_P65C	0xE4		/* packets < 128 bytes and */
						/* > 64 bytes */
#define QE_FCC_PRAM_P128C	0xE8		/* packets < 256 bytes and */ 
						/* > 127 bytes */
#define QE_FCC_PRAM_P256C	0xEC		/* packets < 512 bytes and */ 
						/* > 255 bytes */
#define QE_FCC_PRAM_P512C	0xF0		/* packets < 1024 bytes and */ 
						/* > 511 bytes */
#define QE_FCC_PRAM_P1024C	0xF4		/* packets < 1519 bytes and */ 
						/* > 1023 bytes */
#define QE_FCC_PRAM_CAM_BUF	0xF8		/* internal buffer */
#define QE_FCC_PRAM_RES3	0xFC		/* reserved */

/* General FCC Mode Register definitions */
 
#define QE_FCC_GMR_HDLC		0x00000000      /* HDLC mode */
#define QE_FCC_GMR_RES1		0x00000001      /* reserved mode */
#define QE_FCC_GMR_RES2		0x00000002      /* reserved mode */
#define QE_FCC_GMR_RES3		0x00000003      /* reserved mode */
#define QE_FCC_GMR_RES4		0x00000004      /* reserved mode */
#define QE_FCC_GMR_RES5		0x00000005      /* reserved mode */
#define QE_FCC_GMR_RES6		0x00000006      /* reserved mode */
#define QE_FCC_GMR_RES7		0x00000007      /* reserved mode */
#define QE_FCC_GMR_RES8		0x00000008      /* reserved mode */
#define QE_FCC_GMR_RES9		0x00000009      /* reserved mode */
#define QE_FCC_GMR_ATM		0x0000000a      /* ATM mode */
#define QE_FCC_GMR_RES10	0x0000000b      /* reserved mode */
#define QE_FCC_GMR_ETHERNET	0x0000000c      /* ethernet mode */
#define QE_FCC_GMR_RES11	0x0000000d      /* reserved mode */
#define QE_FCC_GMR_RES12	0x0000000e      /* reserved mode */
#define QE_FCC_GMR_RES13	0x0000000f      /* reserved mode */
#define QE_FCC_GMR_NORM		0x00000000      /* normal mode */
#define QE_FCC_GMR_LOOP		0x40000000      /* local loopback */
#define QE_FCC_GMR_ECHO		0x80000000      /* automatic echo */
#define QE_FCC_GMR_LOOP_ECHO	0xc0000000      /* loop & echo */
#define QE_FCC_GMR_TCI       	0x20000000      /* tx clock invert */
#define QE_FCC_GMR_TRX        	0x10000000      /* transparent receiver */
#define QE_FCC_GMR_TTX       	0x08000000      /* transparent transmitter */
#define QE_FCC_GMR_CDP        	0x04000000      /* CD* pulse */
#define QE_FCC_GMR_CTSP       	0x02000000      /* CTS* pulse */
#define QE_FCC_GMR_CDS        	0x01000000      /* CD* sampling */
#define QE_FCC_GMR_CTSS       	0x00800000      /* CTS* sampling */
#define QE_FCC_GMR_SYN_EXT    	0x00000000      /* external sync */
#define QE_FCC_GMR_SYN_AUTO   	0x00004000      /* automatic sync */
#define QE_FCC_GMR_SYN_8	0x00008000      /* 8-bit sync pattern */
#define QE_FCC_GMR_SYN_16	0x0000c000      /* 16-bit sync pattern */
#define QE_FCC_GMR_RTSM		0x00002000      /* RTS* mode */
#define QE_FCC_GMR_RENC_RES   	0x00001000      /* receiver encoding reserved */
#define QE_FCC_GMR_RENC_NRZI  	0x00000800      /* receiver encoding NRZI */
#define QE_FCC_GMR_RENC_NRZ   	0x00000000      /* receiver encoding NRZ */
#define QE_FCC_GMR_REVD       	0x00000400      /* reverse data */
#define QE_FCC_GMR_TENC_RES   	0x00000200      /* transmitter encoding res */
#define QE_FCC_GMR_TENC_NRZI  	0x00000100      /* transmitter encoding NRZI */
#define QE_FCC_GMR_TENC_NRZ   	0x00000000      /* transmitter encoding NRZ */
#define QE_FCC_GMR_TCRC_RES   	0x00000040      /* transparent CRC reserved */
#define QE_FCC_GMR_TCRC_16    	0x00000000      /* 16-bit transparent CRC */
#define QE_FCC_GMR_TCRC_32    	0x00000080      /* 32-bit transparent CRC */
#define QE_FCC_GMR_ENT		0x00000010      /* enable transmitter */
#define QE_FCC_GMR_ENR		0x00000020      /* enable receiver */

/* FCC Data Synchronization Register definitions */
 
#define QE_FDSR_SYN1_MASK	0x00ff      	/* sync pattern mask 1 */
#define QE_FDSR_SYN2_MASK	0xff00      	/* sync pattern mask 2 */
#define QE_FDSR_ETH_SYN1	0x55      	/* Ethernet sync pattern 1 */
#define QE_FDSR_ETH_SYN2	0xd5      	/* Ethernet sync pattern 2 */

/* FCC Transmit on Demand Register definitions */
 
#define QE_FTODR_TOD		0x8000      	/* transmit on demand */

/* FCC Bus Mode Register definitions */
 
#define QE_BMR_GBL		0x20      	/* global mem operation *
                                                 * enable snooping */
#define QE_BMR_BO_BE		0x10      	/* big-endian ordering */
#define QE_BMR_BO_LE		0x08      	/* little-endian ordering */
#define QE_BMR_TC2		0x04      	/* transfer code for TC[2] */
#define QE_BMR_DTB		0x02      	/* data is in the local bus */
#define QE_BMR_BDB		0x01      	/* BDs are in the local bus */
#define QE_BMR_SHIFT		24      	/* get to fcr bits in xstate */

/* FCC Ethernet Protocol Specific Mode Register definitions */
 
#define QE_FPSMR_ETH_ECM	0x04000000          /* full duplex enable */
#define QE_FPSMR_ETH_HSE	0x02000000          /* enable RMON mode */
#define QE_FPSMR_ETH_PRO	0x00400000          /* enable promiscous mode */
#define QE_FPSMR_ETH_CAP	0x00200000          /* flow control enable */
#define QE_FPSMR_ETH_RSH	0x00100000          /* receive short frame */
#define QE_FPSMR_ETH_RPM        0x00080000
#define QE_FPSMR_ETH_R10M       0x00040000
#define QE_FPSMR_ETH_RLPB       0x00020000
#define QE_FPSMR_ETH_TBIM       0x00010000
#define QE_FPSMR_ETH_AUFC       0x0000C000
#define QE_FPSMR_ETH_RMM        0x00001000
#define QE_FPSMR_ETH_MDCP       0x00000800
#define QE_FPSMR_ETH_CAM	0x00000400     /* CAM address matching */
#define QE_FPSMR_ETH_BRO	0x00000200     /* broadcast enable */

/* FCC Ethernet Event and Mask Register definitions */

#define QE_FCC_ETH_EVENT    0xffffffff          /* event mask */
#define QE_FCC_ETH_GRA      0x20000000          /* graceful stop event */
#define QE_FCC_ETH_RXC      0x04000000          /* rx control frame event */
#define QE_FCC_ETH_TXC      0x02000000          /* tx control frame event */
#define QE_FCC_ETH_TXE      0x01000000   

       /* transmission error event*/
#define QE_FCC_ETH_RXF0      0x00000001          /* frame received event */
#define QE_FCC_ETH_RXFn(n)   (1<<(n-1))
#define QE_FCC_ETH_BSY      0x08000000          /* busy condition */
#define QE_FCC_ETH_TXB0      0x00010000          /* buffer transmitted event*/
#define QE_FCC_ETH_TXBn(n)   (0x10000<<(n-1))
#define QE_FCC_ETH_RXB0      0x00000100          /* buffer received event */
#define QE_FCC_ETH_RXBn(n)   (0x100<<(n-1))

/* FCC Ethernet Receive Buffer Descriptor definitions */
 
#define QE_FCC_RBD_E       0x8000          /* buffer is empty */
#define QE_FCC_RBD_W       0x2000          /* last BD in ring */
#define QE_FCC_RBD_I       0x1000          /* interrupt on receive */
#define QE_FCC_RBD_L       0x0800          /* buffer is last in frame */
#define QE_FCC_RBD_F       0x0400          /* buffer is first in frame */
#define QE_FCC_RBD_M       0x0100          /* miss bit for prom mode */
#define QE_FCC_RBD_BC      0x0080          /* broadcast address frame */
#define QE_FCC_RBD_MC      0x0040          /* multicast address frame */
#define QE_FCC_RBD_LG      0x0020          /* frame length violation */
#define QE_FCC_RBD_NO      0x0010          /* nonoctet aligned frame */
#define QE_FCC_RBD_SH      0x0008          /* short frame received */
#define QE_FCC_RBD_CR      0x0004          /* Rx CRC error */
#define QE_FCC_RBD_OV      0x0002          /* overrun condition */
#define QE_FCC_RBD_CL      0x0001          /* collision condition */
 
/* FCC Ethernet Transmit Buffer Descriptor definitions */
 
#define QE_FCC_TBD_R       0x8000          /* buffer is ready */
#define QE_FCC_TBD_PAD     0x4000          /* auto pad short frames */
#define QE_FCC_TBD_W       0x2000          /* last BD in ring */
#define QE_FCC_TBD_I       0x1000          /* interrupt on transmit */
#define QE_FCC_TBD_L       0x0800          /* buffer is last in frame */
#define QE_FCC_TBD_TC      0x0400          /* auto transmit CRC */
#define QE_FCC_TBD_DEF     0x0200          /* defer indication */
#define QE_FCC_TBD_HB      0x0100          /* heartbeat */
#define QE_FCC_TBD_LC      0x0080          /* late collision */
#define QE_FCC_TBD_RL      0x0040          /* retransmission limit */
#define QE_FCC_TBD_RC      0x003c          /* retry count */
#define QE_FCC_TBD_UN      0x0002          /* underrun */
#define QE_FCC_TBD_CSL     0x0001          /* carrier sense lost */


#define QE_FCC_MACCFG1_RX_FLOW        0x00000020
#define QE_FCC_MACCFG1_TX_FLOW        0x00000010
#define QE_FCC_MACCFG1_SYNCD_RX_EN    0x00000008
#define QE_FCC_MACCFG1_RX_EN          0x00000004
#define QE_FCC_MACCFG1_SYNCD_TX_EN    0x00000002
#define QE_FCC_MACCFG1_TX_EN          0x00000001

#define QE_FCC_MACCFG2_PRE_LEN(l)         ((l<<12) & 0xf000)
#define QE_FCC_MACCFG2_PRE_LEN_GET(r)     ((r&0xf000)>>12)
#define QE_FCC_MACCFG2_IF_MODE(m)         ((m<<8) & 0x0300)
#define QE_FCC_MACCFG2_IF_MODE_GET(r)     ((r&0x0300)>>8)

#define QE_FCC_MACCFG2_IF_MODE_MASK       0x00000003
#define QE_FCC_MACCFG2_IF_MODE_MII        0x00000001
#define QE_FCC_MACCFG2_IF_MODE_GMII_TBI   0x00000002
#define QE_FCC_MACCFG2_SRP                0x00000080
#define QE_FCC_MACCFG2_STP                0x00000040
#define QE_FCC_MACCFG2_LENGTH_CHECK       0x00000010

#define QE_FCC_MACCFG2_MPE                0x00000008
#define QE_FCC_MACCFG2_PADCRC             0x00000004
#define QE_FCC_MACCFG2_CRC_EN             0x00000002
#define QE_FCC_MACCFG2_FULL_DUPLEX        0x00000001
 
#define MOT_TSEC_IPGIFG_NBBIPG1(l)      ((l&0x0000007f)<<24)
#define MOT_TSEC_IPGIFG_NBBIPG2(l)      ( (l&0x0000007f)<<16)
#define MOT_TSEC_IPGIFG_MIFGE(l)        ((l&0x000000ff)<<8)
#define MOT_TSEC_IPGIFG_BBIPG(l)        (l&0x0000007f)

#define QE_FCC_HALDUP_ALTBEB_TRUNC(l) ((l&0x0000000f)<<20)
#define QE_FCC_HALFDUP_BEB            0x00080000
#define QE_FCC_HALFDUP_BPNBO          0x00040000
#define QE_FCC_HALFDUP_NBO            0x00020000
#define QE_FCC_HALFDUP_EXCESS_DEF     0x00010000
#define QE_FCC_HALDUP_RETRY(v)        ((v&0x0000000F)<<12)
#define QE_FCC_HALDUP_COL_WINDOW(w)   (w&0x003f)

#ifdef __cplusplus
}
#endif

#endif /* __INCqeFcch */
