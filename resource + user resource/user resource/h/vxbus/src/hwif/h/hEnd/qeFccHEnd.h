/* qeFccHEnd.h - Freescale QE_FCC Ethernet network interface.*/

/* 
 * Copyright (c) 2006-2007 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
01f,24jul07,b_m  modify QE_FCC_MAX_DEVS to support 8 UCCs.
01e,10oct06,dtr  Added some entries to pDrvCtrl and defines to support 
                 Quicc Engine Lite version for 8323. 
01d,12sep06,dlk  Remove struct ifqueue rxQueue member of QE_FCC_DRV_CTRL.
01c,17jul06,rcs  Added Modular hEnd includes
01b,26jun06,dtr  USe more receive BDs.
01a,04jan06,dtr  Initial checkin of file, based on motTsec driver
*/


#ifndef __INCqeFccEndh
#define __INCqeFccEndh

/* includes */

#ifdef __cplusplus
extern "C" {
#endif

#include <net/ethernet.h>
#include <etherLib.h>
#include <miiLib.h>
#include <logLib.h>
#include "hEnd.h"

IMPORT void qeFccHEndRegister (void);

#ifndef M8260ABBREVIATIONS
#define M8260ABBREVIATIONS

#ifdef  _ASMLANGUAGE
#define CAST(x)
#else /* _ASMLANGUAGE */
typedef volatile UCHAR VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32 VINT32; /* volatile unsigned word */
typedef volatile INT16 VINT16; /* volatile unsigned halfword */
typedef volatile INT8 VINT8;   /* volatile unsigned byte */
typedef volatile UINT32 VUINT32; /* volatile unsigned word */
typedef volatile UINT16 VUINT16; /* volatile unsigned halfword */
typedef volatile UINT8 VUINT8;   /* volatile unsigned byte */
#define CAST(x) (x)
#endif  /* _ASMLANGUAGE */

#endif /* M8260ABBREVIATIONS */

#ifndef TEST_TOKEN_MERGE
    #define TEST_TOKEN_MERGE
    
    /* __(i) used for function names and variables */
    #define __(i)   qeFcc ## i
    
    /* _c_(i)used for macros  and typedefs */
    #define _c_(i)  QE_FCC_ ## i
    
    /* _m_ used for structure name - needed for offsetof */
    #define _m_(i)  qe_fcc_ ## i
    
    /* _o_(i) used for device name string */
    #define _o_(i)  qefcc ## i
    
    /* _p_(i) used for calls to configuration stub */
    #define _p_(i)  sysQeFcc ## i
#else
    #undef __(i)
        #define __(i)    qeFcc ## i
    #undef _c_(i)
        #define _c_(i)   QE_FCC_ ## i
    #undef _m_(i)
        #define _m_(i)   qe_fcc_ ## i
    #undef _o_(i)
        #define _o_(i)   qefcc ## i  
    #undef _p_(i)
        #define _p_(i)   sysQeFcc ## i  
#endif

#define HEND_RX_DMA
#define HEND_TX_DMA
#define HEND_INIT_TUPLES

#define QE_FCC_DEVICE_ON_NEXUS	TRUE
#define QE_FCC_HAS_MII_BUS TRUE
#define QE_FCC_NUM_EVENTS (QE_FCC_NUM_RX_QUEUES + QE_FCC_NUM_TX_QUEUES + 1)
#define QE_FCC_NUM_ISRS 1
#define QE_FCC_NUM_RX_QUEUES 1
#define QE_FCC_NUM_TX_QUEUES 1
#define QE_FCC_RX_EVENT_ID 0
#define QE_FCC_TX_EVENT_ID 1
#define QE_FCC_RX_POLL 0
#define QE_FCC_TX_PKT_COMP_EVENT_ID  (QE_FCC_NUM_RX_QUEUES + QE_FCC_RX_EVENT_ID + 1)
#define QE_FCC_HEND_TX_PAD 0
#define QE_FCC_FRAG_NUM_LIMIT	16 
#define QE_FCC_TX_INT_HANDLE 0
#define QE_FCC_RX_INT_HANDLE 1
#define QE_FCC_ERR_INT_HANDLE 2
#define QE_FCC_TX_DESC_CAPACITY 1
#define QE_FCC_TX_DESC_SPAN 1
#define QE_FCC_ALIGNMENT_OFFSET 0


#ifndef MAC_ADDR_LEN
#define MAC_ADDR_LEN 6
#endif

#define QE_MURAM_OFFSET(x) ((UINT32)x & 0xffff)

#define QE_FCC_CACHE_INVAL(address, len)                              \
    CACHE_DRV_INVALIDATE (&pDrvCtrl->bufCacheFuncs, (address), (len));

#define QE_FCC_CACHE_FLUSH(address, len)                               \
    CACHE_DRV_FLUSH (&pDrvCtrl->bufCacheFuncs, (address), (len));     


/* Debug Macros */
#define QE_FCC_DRV_DEBUG     0x0000
#define QE_FCC_DRV_DEBUG_RX         0x0001
#define QE_FCC_DRV_DEBUG_TX         0x0002
#define QE_FCC_DRV_DEBUG_POLL       0x0004
#define QE_FCC_DRV_DEBUG_MII        0x0008
#define QE_FCC_DRV_DEBUG_LOAD       0x0010
#define QE_FCC_DRV_DEBUG_IOCTL      0x80000
#define QE_FCC_DRV_DEBUG_INT        0x0040
#define QE_FCC_DRV_DEBUG_START      0x0080
#define QE_FCC_DRV_DEBUG_VXB_RW     0x20000
#define QE_FCC_DRV_DEBUG_INT_RX_ERR 0x0100
#define QE_FCC_DRV_DEBUG_INT_TX_ERR 0x0200
#define QE_FCC_DRV_DEBUG_RX_ERR     0x0400
#define QE_FCC_DRV_DEBUG_TX_ERR     0x0800
#define QE_FCC_DRV_DEBUG_TRACE      0x1000
#define QE_FCC_DRV_DEBUG_TRACE_RX   0x2000
#define QE_FCC_DRV_DEBUG_TRACE_TX   0x4000
#define QE_FCC_DRV_DEBUG_MONITOR    0x8000
#define QE_FCC_DRV_DEBUG_ANY        0xffff
#define QE_FCC_DRV_DEBUG_REG      0x10000
#define QE_FCC_DRV_DEBUG_INIT_MEM   QE_FCC_DRV_DEBUG_LOAD


#ifdef QE_FCC_DEBUG 

UINT32 qeFccEndDbg = (QE_FCC_DRV_DEBUG_ANY);

#define QE_FCC_DRV_LOGMSG(FLG, X0, X1, X2, X3, X4, X5, X6)    \
    {                                                           \
    if (qeFccEndDbg & FLG)                                       \
        if (_func_logMsg != NULL)                                       \
        logMsg (X0, X1, X2, X3, X4, X5, X6);                    \
    }

#else /* QE_FCC_DEBUG */
#define QE_FCC_DRV_LOGMSG(FLG, X0, X1, X2, X3, X4, X5, X6)
#endif 

#define QE_FCC_DEV_NAME        "qefcc"
#define QE_FCC_DEV_NAME_LEN    6
#define QE_FCC_MAX_DEVS        8

#define QE_FCC_1000M 3
#define QE_FCC_100M 2
#define QE_FCC_10M 1
#define QE_FCC_AUTO 0

/* Register offsets */
#include <../src/hwif/h/resource/qeCp.h>
#include <../src/hwif/h/hEnd/qeFcc.h>
#include <../src/hwif/h/resource/qeDpramLib.h>

#define QE_FCC_CCMR_RX_DEFAULT (QE_FCC_ETH_RXF0 | \
				QE_FCC_ETH_BSY )

#define QE_FCC_CCMR_TX_NORMAL_DEFAULT (QE_FCC_ETH_TXB0)

#define QE_FCC_CCMR_TX_ERR_DEFAULT (QE_FCC_ETH_TXE)

#define QE_FCC_CCMR_DEFAULT   ( QE_FCC_CCMR_RX_DEFAULT | \
								QE_FCC_CCMR_TX_NORMAL_DEFAULT | \
								QE_FCC_CCMR_TX_ERR_DEFAULT )


#define QE_FCC_RBMR_DEFAULT   ( QE_BMR_BO_BE ) 
#define QE_FCC_TBMR_DEFAULT   ( QE_BMR_BO_BE )
 
#define QE_FCC_PSMR_DEFAULT   (QE_FPSMR_ETH_HSE | 0x2000/* Must set*/) 


#define QE_FCC_VXB_ACCESS

#define QE_MURAM_ALLOC     qeMuRamAllocFunc

typedef struct
    {
    FUNCPTR enetAddrGet;    /* Driver call back to get the Ethernet address */
    FUNCPTR enetAddrSet;    /* Driver call back to set the Ethernet address */
    FUNCPTR enetEnable;     /* Driver call back to enable the ENET interface */
    FUNCPTR enetDisable;    /* Driver call back to disable the ENET interface */
    } QE_FCC_FUNC_TABLE;


typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } QE_FCC_RX_BD;
    
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } QE_FCC_TX_BD;
 
 /* RCS Remove later */   
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } QE_FCC_BD;

typedef struct
    {
    UINT8   *memBufPtr;    /* Buffer pointer for allocated buffer space */
    UINT32   memBufSize;   /* Buffer pool size */
    QE_FCC_BD *bdBasePtr;    /* Descriptor Base Address */
    UINT32   bdSize;       /* Descriptor Size */
    UINT32   rbdNum;       /* Number of Receive Buffer Descriptors  */
    UINT32   tbdNum;       /* Number of Transmit Buffer Descriptors */
    UINT32   unit0TxVec;
    UINT32   unit0RxVec;
    UINT32   unit0ErrVec;
    UINT32   unit1TxVec;
    UINT32   unit1RxVec;
    UINT32   unit1ErrVec;
    } QE_FCC_PARAMS;

typedef struct
    {
    UINT32   inum_tsecTx;    /* Transmit Interrupt */
    UINT32   inum_tsecRx;    /* Receive Interrupt */
    UINT32   inum_tsecErr;    /* Error Interrupt */
    FUNCPTR  inumToIvec;     /* function to convert INUM to IVEC */
    FUNCPTR  ivecToInum;     /* function to convert IVEC to INUM */
    } QE_FCC_INT_CTRL;


/* enum declarations for argument passing */
enum qeFccParams {QE_FCC_UNIT_TYPE                  = 0x10,
                    QE_FCC_CCSBAR_TYPE                = 0x11,
                    QE_FCC_QE_FCC_NUM_TYPE              = 0x12,
                    QE_FCC_MAC_ADDR_TYPE              = 0x13,
                    QE_FCC_USR_FLAGS_TYPE             = 0x14,
                    QE_FCC_PHY_PARAM_TYPE             = 0x15,
                    QE_FCC_FUNC_TABLE_PARAM_TYPE      = 0x16,
                    QE_FCC_CONFIG_PARAMS_TYPE         = 0x17};

typedef union mot_qeFcc_dl_event_rec
        {
        HEND_RX_CONTROL rx;
        HEND_TX_CONTROL tx;
        }QE_FCC_DL_EVENT_REC;
        
typedef struct mot_qeFcc_hend_event_rec
    {
    HEND_EVENT_REC hEnd;
    QE_FCC_DL_EVENT_REC dl;
    }QE_FCC_HEND_EVENT_REC;
    


/*
 * Initial Quicc Engine Map is as follows
 */

#define QE_MURAM_ADDR(x)  ((UINT32)x & 0xffff)




#define QE_FCC_MIIMCFG_RESET          0x80000000
#define QE_FCC_MIIMCFG_NO_PRE         0x00000010
#define QE_FCC_MIIMCFG_MCS(l)         (l&0x00000007)
#define QE_FCC_MIIMCFG_MCS_2          0x00000000
#define QE_FCC_MIIMCFG_MCS_4          0x00000001
#define QE_FCC_MIIMCFG_MCS_6          0x00000002
#define QE_FCC_MIIMCFG_MCS_8          0x00000003
#define QE_FCC_MIIMCFG_MCS_10         0x00000004
#define QE_FCC_MIIMCFG_MCS_14         0x00000005
#define QE_FCC_MIIMCFG_MCS_20         0x00000006
#define QE_FCC_MIIMCFG_MCS_28         0x00000007

#define QE_FCC_MIIMCOM_SCAN_CYCLE     0x00000002
#define QE_FCC_MIIMCOM_READ_CYCLE     0x00000001

#define QE_FCC_MIICFG_DEFAULT         (QE_FCC_MIIMCFG_MCS_14)

#define QE_FCC_MIIMADD_PHYADRS(a)     ((a&0x0000001f)<<8)
#define QE_FCC_MIIMADD_REGADRS(a)     (a&0x0000001f)

#define QE_FCC_MIIMCON_PHY_CTRL(a)    (a&0x0000ffff)

#define QE_FCC_MIIMSTAT_PHY(a)        (a&0x0000ffff)

#define QE_FCC_MIIMIND_NOT_VALID      0x00000004
#define QE_FCC_MIIMIND_SCAN           0x00000002
#define QE_FCC_MIIMIND_BUSY           0x00000001

#define QE_FCC_IFSTAT_EXCESS_DEF      0x00000200

#define QE_FCC_MACSTNADDR1_SA_1       0xff000000
#define QE_FCC_MACSTNADDR1_SA_2       0x00ff0000
#define QE_FCC_MACSTNADDR1_SA_3       0x0000ff00
#define QE_FCC_MACSTNADDR1_SA_4       0x000000ff
#define QE_FCC_MACSTNADDR2_SA_5       0xff000000
#define QE_FCC_MACSTNADDR2_SA_6       0x00ff0000

/* Transmit Buffer Descriptor bit definitions */

/* QE_FCC init string parameters */
/* "MMBASE:QE_FCC_PORT:MAC_ADRS:PHY_DEF_MODES:USER_FLAGS:FUNC_TABLE:EXT_PARMS"  */
/*        MMBASE - 85xx local base address. Used as base for driver memory space.
 *      MAC_ADRS - Mac address in 12 digit format eg. 00-A0-1E-11-22-33
 *  PHY_DEF_MODE - Default Attributes passed to the MII driver.
 * USER_DEF_MODE - Mandatory initialization user parameters.
 *    FUNC_TABLE - Table of BSP and Driver callbacks
 *         PARMS - Address of a structure that contains required initialization
 *                 parameters and driver specific tuning parameters.
 *     EXT_PARMS - Address of a structure that contains optional initialization
 *                 parameters and driver specific tuning parameters.
 */

/* MII/PHY PHY_DEF_MODE flags to init the phy driver */
#define QE_FCC_USR_MODE_DEFAULT         0
#define QE_FCC_USR_PHY_NO_AN   0x00000001  /* do not auto-negotiate */
#define QE_FCC_USR_PHY_TBL     0x00000002  /* use negotiation table */
#define QE_FCC_USR_PHY_NO_FD   0x00000004  /* do not use full duplex */
#define QE_FCC_USR_PHY_NO_100  0x00000008  /* do not use 100Mbit speed */
#define QE_FCC_USR_PHY_NO_HD   0x00000010  /* do not use half duplex */
#define QE_FCC_USR_PHY_NO_10   0x00000020  /* do not use 10Mbit speed */
#define QE_FCC_USR_PHY_MON     0x00000040  /* use PHY Monitor */
#define QE_FCC_USR_PHY_ISO     0x00000080  /* isolate a PHY */


/* ECNTRL Ethernet Control */
#define QE_FCC_USR_STAT_CLEAR     0x00000100 /* init + runtime clear mstats*/
#define QE_FCC_USR_STAT_AUTOZ     0x00000200 /* init */
#define QE_FCC_USR_STAT_ENABLE    0x00000400 /* init */

/* PHY bus configuration selections */
#define QE_FCC_USR_MODE_MASK      0x0003f800
#define QE_FCC_USR_MODE_TBI       0x00000800
#define QE_FCC_USR_MODE_RTBI      0x00001000
#define QE_FCC_USR_MODE_MII       0x00002000
#define QE_FCC_USR_MODE_GMII      0x00004000
#define QE_FCC_USR_MODE_RGMII     0x00008000
#define QE_FCC_USR_MODE_RGMII_10  0x00010000
#define QE_FCC_USR_MODE_RGMII_100 0x00020000



/* Driver Runtime Attributes */
#define QE_FCC_POLLING_MODE         0x01000000    /* polling mode */
#define QE_FCC_MULTICAST_MODE       0x02000000    /* multicast addressing */

#define FLAG_POLLING_MODE               0x0001
#define FLAG_PROMISC_MODE               0x0002
#define FLAG_ALLMULTI_MODE              0x0004
#define FLAG_MULTICAST_MODE             0x0008
#define FLAG_BROADCAST_MODE             0x0010



#define QE_FCC_MACCFG1_DEFAULT (0x0)


/* IF_MODE = 1 ; MII mode 10/100 only */
#define QE_FCC_MACCFG2_DEFAULT  \
                (QE_FCC_MACCFG2_PRE_LEN(7)  | \
                 0x20 | \
                 QE_FCC_MACCFG2_FULL_DUPLEX | \
                 QE_FCC_MACCFG2_PADCRC      | \
                 QE_FCC_MACCFG2_IF_MODE(QE_FCC_MACCFG2_IF_MODE_GMII_TBI))


/* Driver State Variables */

#define QE_FCC_STATE_INIT          0x00
#define QE_FCC_STATE_NOT_LOADED    0x00
#define QE_FCC_STATE_LOADED        0x01
#define QE_FCC_STATE_NOT_RUNNING   0x00
#define QE_FCC_STATE_RUNNING       0x02

/* Internal driver flags */

#define QE_FCC_OWN_BUF_MEM    0x01    /* internally provided memory for data*/
#define QE_FCC_INV_TBD_NUM    0x02    /* invalid tbdNum provided */
#define QE_FCC_INV_RBD_NUM    0x04    /* invalid rbdNum provided */
#define QE_FCC_POLLING        0x08    /* polling mode */
#define QE_FCC_PROM           0x20    /* promiscuous mode */
#define QE_FCC_MCAST          0x40    /* multicast addressing mode */
#define QE_FCC_FD             0x80    /* full duplex mode */
#define QE_FCC_OWN_BD_MEM     0x10    /* internally provided memory for BDs */
#define QE_FCC_MIN_TX_PKT_SZ   100     /* the smallest packet we send */

#define QE_FCC_CL_NUM_DEFAULT   128   /* number of tx clusters */
#define QE_FCC_CL_MULTIPLE       11   /* ratio of clusters to RBDs */
#define QE_FCC_TBD_NUM_DEFAULT  128    /* default number of TBDs */
#define QE_FCC_RBD_NUM_DEFAULT  64     /* default number of RBDs */
#define QE_FCC_TX_POLL_NUM      1     /* one TBD for poll operation */

#define QE_FCC_CL_OVERHEAD      4     /* prepended cluster overhead */
#define QE_FCC_CL_ALIGNMENT     64    /* cluster required alignment */
#define QE_FCC_CL_SIZE          1536  /* cluster size */
#define QE_FCC_MBLK_ALIGNMENT   64    /* mBlks required alignment */
#define QE_FCC_RX_BD_SIZE       0x8   /* size of QE_FCC BD */
#define QE_FCC_TX_BD_SIZE       0x8   /* size of QE_FCC BD */
#define QE_FCC_BD_ALIGN         64   /* required alignment for BDs */
#define QE_FCC_BUF_ALIGN        64   /* required alignment for data buffer */


/*
 * the total is 0x630 and it accounts for the required alignment
 * of receive data buffers, and the cluster overhead.
 */
#define XXX_QE_FCC_MAX_CL_LEN ((MII_ETH_MAX_PCK_SZ             \
                            + (QE_FCC_BUF_ALIGN - 1)       \
                            + QE_FCC_BUF_ALIGN             \
                            + (QE_FCC_CL_OVERHEAD - 1))    \
                            & (~ (QE_FCC_CL_OVERHEAD - 1)))

#define QE_FCC_MAX_CL_LEN     ROUND_UP(XXX_QE_FCC_MAX_CL_LEN,QE_FCC_BUF_ALIGN)

#define QE_FCC_RX_CL_SZ       (QE_FCC_MAX_CL_LEN)
#define QE_FCC_TX_CL_SZ       (QE_FCC_MAX_CL_LEN)



#define QE_FCC_CCER_CLEAR(pDev)					\
    QE_FCC_WRITE((pDev), QE_FCC_CCER, QE_FCC_READ((pDev), QE_FCC_CCER))

#define QE_FCC_RX_EVENT_CLEAR(pDev)					\
    QE_FCC_WRITE((pDev),QE_FCC_CCER,   \
                 QE_FCC_CCMR_RX_DEFAULT)

#define QE_FCC_RX_INT_ENABLE(pDev) \
    { int key;\
    key = intLock(); \
    pDrvCtrl->intMask |= QE_FCC_CCMR_RX_DEFAULT; \
    QE_FCC_WRITE((pDev),QE_FCC_CCMR,pDrvCtrl->intMask); \
    intUnlock(key);}

#define QE_FCC_TX_STALL_ENTER(pDev)

#define QE_FCC_TX_STALL_EXIT(pDev)

#define QE_FCC_TX_INT_DISABLE(pDev)			\
    QE_FCC_CLRBIT((pDev), QE_FCC_CCMR, QE_FCC_CCMR_TX_NORMAL_DEFAULT);

#define QE_FCC_TX_INT_ENABLE(pDev)		\
    { int key; \
      key= intLock(); \
      pDrvCtrl->intMask |= QE_FCC_CCMR_TX_NORMAL_DEFAULT;		\
      QE_FCC_WRITE((pDev), QE_FCC_CCMR, pDrvCtrl->intMask);	\
      intUnlock(key); }

#define QE_FCC_GET_EVENT_REC(x) ((QE_FCC_HEND_EVENT_REC *)(x))

#define QE_FCC_GET_DRV_CTRL(x) \
((int)(x) - (offsetof (struct qeFcc_drv_ctrl,eventRec[(((HEND_EVENT_REC *)(x))->index)])))

#define QE_FCC_TX_CAPACITY(x) (x)
    
/* MIB macros */
#define QE_FCC_HEND_M2_INIT(p)  \
    {                        \
    endM2Init(&(p)->hEnd.endObj, M2_ifType_ethernet_csmacd, \
              (u_char *) &(p)->hEnd.enetAddr, 6, ETHERMTU, \
              1000000000, \
              IFF_NOTRAILERS | IFF_MULTICAST | IFF_BROADCAST); \
    }
#define QE_FCC_HEND_M2_PKT(p,m,z)


#define QE_FCC_RX_TX_ALIGNMENT 0x100
#define QE_FCC_RXGLOBAL_PARAM_ALIGNMENT      0x100
#define QE_FCC_TXGLOBAL_PARAM_ALIGNMENT      0x80
#define QE_FCC_THREAD_RX_PARAM_ALIGNMENT   0x100
#define QE_FCC_THREAD_TX_PARAM_ALIGNMENT   0x80
#define QE_FCC_RQDESC_ALIGNMENT      0x100
#define QE_FCC_INTCOELESC_TABLE_ALIGNMENT      0x100
#define QE_FCC_RBD_QDESC_ALIGNMENT      0x100
#define QE_FCC_SQDESC_ALIGNMENT      0x100
#define QE_FCC_SCHED_ALIGNMENT      0x20
#define QE_FCC_TXQDESC_ALIGNMENT   0x100


#define QE_FCC_URFB_ALIGNMENT      0x1000
#define QE_FCC_UTFB_ALIGNMENT      0x1000
#define QE_FCC_URFB_SIZE           0x1000
#define QE_FCC_UTFB_SIZE           0x1000
#define QE_FCC_URFET_DEFAULT       0x1000
#define QE_FCC_URFSET_DEFAULT      0x1000
#define QE_FCC_UTFET_DEFAULT       0x400
#define QE_FCC_UTFTT_DEFAULT       0x200

#define QE_FCC_URFB_ALIGNMENT_100M      0x200
#define QE_FCC_UTFB_ALIGNMENT_100M      0x200
#define QE_FCC_URFB_SIZE_100M           0x200
#define QE_FCC_UTFB_SIZE_100M           0x200
#define QE_FCC_URFET_DEFAULT_100M       0x200
#define QE_FCC_URFSET_DEFAULT_100M      0x200
#define QE_FCC_UTFET_DEFAULT_100M      0x200
#define QE_FCC_UTFTT_DEFAULT_100M      0x100

#define QE_FCC_DEFAULT_TBIPA       (30)

#define QE_FCC_REMODER_EXF         0x80000000
#define QE_FCC_REMODER_VTAGOP_MSK  0x03c00000
#define QE_FCC_REMODER_VNONTAGOP   0x00200000
#define QE_FCC_REMODER_QOS_MSK     0x00030000
#define QE_FCC_REMODER_LOSSLESS_FC 0x00004000
#define QE_FCC_REMODER_RFSE        0x00001000
#define QE_FCC_REMODER_EXP         0x00000800
#define QE_FCC_REMODER_NUM_Q_MSK   0x00000700
#define QE_FCC_REMODER_DXE         0x00000008
#define QE_FCC_REMODER_DNE         0x00000004
#define QE_FCC_REMODER_IPCHK       0x00000002
#define QE_FCC_REMODER_IPAE        0x00000001


#define QE_FCC_TEMODER_SOF         0x8000
#define QE_FCC_TEMODER_SOBD        0x4000
#define QE_FCC_TEMODER_SCHED_EN    0x2000
#define QE_FCC_TEMODER_IPCHK       0x0400
#define QE_FCC_TEMODER_OPTIM1      0x0200
#define QE_FCC_TEMODER_RMONEN      0x0100
#define QE_FCC_TEMODER_NUM_Q_MSK   0x0007    


typedef struct 
    {
    UINT32 byteCnt;
    UINT32 TX_64;
    UINT32 TX_L64;
    UINT32 TX_65T127;
    UINT32 TX_128T255;
    UINT32 TX_256T511;
    UINT32 TX_512T1023;
    UINT32 TX_1024T1522;
    UINT32 Jabber;
    UINT32 broadcast;
    UINT32 unicast;
    UINT32 multicast;
    UINT32 flowControl;
    UINT32 pad[3];
    } QE_TXRMON_DESC;

typedef struct 
    {
    UINT32 placeholder;
    } QE_TXQDESC;

typedef struct
    {
    QE_FCC_BD   *pBdRingBase;
    UINT8       resvd[8];
    UINT32      lastBdCompletedAdrs; /*??? */
    UINT8       resvd2[0x30];
    } QE_SQDESC;

typedef struct
    {
    UINT16 cpuCount0;
    UINT16 cpuCount1;
    UINT16 ceCount0;
    UINT16 ceCount1;
    UINT16 cpuCount2;
    UINT16 cpuCount3;
    UINT16 ceCount2;
    UINT16 ceCount3;
    UINT16 cpuCount4;
    UINT16 cpuCount5;
    UINT16 ceCount4;
    UINT16 ceCount5;
    UINT16 cpuCount6;
    UINT16 cpuCount7;
    UINT16 ceCount6;
    UINT16 ceCount7;
    UINT32 weightStatus[8];
    UINT32 rtsrShadow;
    UINT32 time;
    UINT32 ttl;
    UINT32 mblInterval;
    UINT16 norTSRByyteTime;
    UINT8  FracSiz;
    UINT8  resvd1;
    UINT8  strictQPriority;
    UINT8  txASAP;
    UINT8  extraBW;
    UINT8  oldWFQMask;
    UINT8  weightFactor[8];
    UINT8  minW;
    UINT32 resvd2[2];     
    }QE_SCHED;

typedef struct 
    {
    UINT32 resvd[0x10];
    } QE_TXTHREAD_PARAM;


typedef struct
    {
    UINT16 tmoder;
    UINT8  rsvd[0x36];
    QE_SQDESC *pSqptr;
    QE_SCHED *pSchedulerBase;
    QE_TXRMON_DESC *pTxRmonBase;
    UINT32 tstate;
    UINT8 iphOffset[8];
    UINT8 vtagTable[0x20];
    QE_TXQDESC *pTqptr;
    UINT8 resv1[12];
    } QE_TXGLOBAL_PARAM;


typedef struct 
    {
    UINT32 resvd[0x20];
    } QE_RXTHREAD_PARAM;

typedef struct 
    {
    UINT32 byteCnt;
    UINT32 RX_64;
    UINT32 RX_L64;
    UINT32 RX_L64CRCE;
    UINT32 RX_65T127;
    UINT32 RX_128T255;
    UINT32 RX_256T511;
    UINT32 RX_512T1023;
    UINT32 RX_1024T1522;
    UINT32 Jabber;
    UINT32 JabberErr;
    UINT32 broadcast;
    UINT32 unicast;
    UINT32 multicast;
    UINT32 flowControl;
    UINT32 pad;
    } QE_RXRMON_DESC;

typedef struct 
    {
    UINT32 maxValue;
    UINT32 counter;
    } QE_INTCOELESC_TABLE;

typedef struct 
    {
    UINT8 resvd[40];
    } QE_RQDESC;

typedef struct 
    {
    UINT32 pBdBase;
    UINT32 pBdPtr;
    UINT32 pExternalBdBase;
    UINT32 pExternalBd;
    } QE_RBD_QDESC;

typedef struct 
    {
    UINT32 placeholder;
    } QE_EXPGLOBAL_PARAM;

typedef struct 
    {
    UINT32 placeholder;
    } QE_LOSSLESSFC_DESC;


typedef struct
    {
    UINT32 remoder;
    QE_RQDESC* pRqptr;
    UINT8      resvd[0x18];
    UINT16     typeOrLen;
    UINT8      resvd0;
    UINT8      rxGstPack;
    QE_RXRMON_DESC *     pRxRmon;
    UINT32     resvd1[2];
    QE_INTCOELESC_TABLE     *pIntCoelescingPtr;
    UINT8      resvd2[2];
    UINT8      rstate;
    UINT8      resvd3[15];
    UINT16     mrblr;
    QE_RBD_QDESC *pRbdptr;
    UINT16     mflr;
    UINT16     minflr;
    UINT16     maxd1;
    UINT16     maxd2;
    UINT32     ecamPtr;
    UINT32     l2qt;
    UINT32     l3qt[8];
    UINT16     vlanType;
    UINT16     tci;
    UINT8      addressFiltering[0x40];
    QE_EXPGLOBAL_PARAM     *pExpGlobalParam;
    QE_LOSSLESSFC_DESC     *pLossLessFC;
    UINT8      resv[8];
    } QE_RXGLOBAL_PARAM;


/* The definition of the driver control structure */
    
typedef struct qeFcc_drv_ctrl
    {
    HEND_DRV_CTRL hEnd;  
    QE_FCC_HEND_EVENT_REC  eventRec[QE_FCC_NUM_EVENTS];
    HEND_ISR_REC isrRec[QE_FCC_NUM_ISRS]; 
    int          numRxQueues;
    SL_LIST *    pRxQueueList;
    SL_LIST *    pTxQueueList;
    
    int          firstRxIndex;
    int          firstTxIndex;
    BOOL         firstStart;
    UINT32       inum_qeFccTx;  /* QE_FCC Tx interrupt num */
    UINT32       inum_qeFccRx;  /* QE_FCC Rx interrupt num */
    UINT32       inum_qeFccErr;  /*QE_FCC Err interrupt num */
    FUNCPTR      inumToIvec;  /* Conversion utility */
    FUNCPTR      ivecToInum;  /* Conversion utility */
    UINT32       initFlags;   /* user init flags */
    UINT32       userFlags;
    UINT32       retries;
    UINT32       maxRetries;

    VXB_DEVICE_ID  quiccEngineId;
    FUNCPTR       qeThreadMethod;
    FUNCPTR       qeMuRamMethod;
    FUNCPTR       qeCpcrMethod;
    CECDR_RXTX_INIT *cecdr_init;

    /* Bsp specific functions and call backs */
    QE_FCC_FUNC_TABLE * initFuncs;

    UINT32      qeFccNum;       /* physical QE_FCC 0 or 1 */
    UINT32      qeUccNum;       /* UCC num 1 to 8 */
    UINT32      maxLink;
    UINT32      fifoTxBase;     /* address of Tx FIFO in internal RAM */
    UINT32      fifoRxBase;     /* address of Rx FIFO in internal RAM */
    UINT16      txPramBase;
    UINT16      rxPramBase;
    QE_TXGLOBAL_PARAM* txGlobalParamBase;
    QE_RXGLOBAL_PARAM* rxGlobalParamBase;
    char      * pBufAlloc;      /* Allocated QE_FCC memory pool base */
    char      * pBufBase;       /* Rounded QE_FCC memory pool base */
    UINT32      bufSize;        /* QE_FCC memory pool size */

    QE_FCC_BD   * pBdAlloc;       /* QE_FCC BDs Alloc pointer */
    QE_FCC_BD   * pBdBase;        /* QE_FCC BDs base */
    UINT32      bdSize;         /* QE_FCC BDs size */
   
    QE_FCC_BD   * pTbdBase;   

    volatile BOOL rxStall;       /* rx handler stalled - no Tbd */
    PHY_INFO   * phyInfo;        /* info on a MII-compliant PHY */
    /* transmit buffer descriptor management */

    volatile QE_FCC_BD   * pTbdNext;        /* TBD index */

    UINT16      tbiAdr;         /* tbi interface address */
    UINT32      phyFlags;       /* Phy flags */
    UINT32      flags;          /* driver flags */
    UINT32      state;          /* driver state including load flag */
    UINT32      intMask;        /* interrupt mask register */
    UINT32      intErrorMask;   /* interrupt error mask register */
                                 /* transmit command */
    char       * pClBlkArea;     /* cluster block pointer */
    UINT32       clBlkSize;      /* clusters block memory size */
    char       * pMBlkArea;      /* mBlock area pointer */
    UINT32       mBlkSize;       /* mBlocks area memory size */
    CACHE_FUNCS  bufCacheFuncs;  /* cache descriptor */
    CL_POOL_ID   pClPoolId;      /* cluster pool identifier */

    BOOL         lscHandling;

    /* function pointers to support unit testing */

    FUNCPTR     netJobAdd;
    FUNCPTR     muxTxRestart;
    FUNCPTR     muxError;

    /* Bsp specific functions and call backs */

    FUNCPTR     phyInitFunc;     /* BSP Phy Init */
    FUNCPTR     phyStatusFunc;   /* Status Get function */
    FUNCPTR     miiPhyRead;      /* mii Read */
    FUNCPTR     miiPhyWrite;     /* mii Write */
    FUNCPTR     enetEnable;      /* enable ethernet */
    FUNCPTR     enetDisable;     /* disable ethernet */
    FUNCPTR     enetAddrGet;     /* get ethernet address */
    FUNCPTR     enetAddrSet;     /* set ethernet Address */
    FUNCPTR     extWriteL2AllocFunc; /* Use ext write alloc L2 for Tx BD */ 

    END_CAPABILITIES hwCaps;     /* interface hardware offload capabilities */

#ifdef QE_FCC_DBG
    QE_FCC_DRIVER_STATS *stats;
#endif
    UINT32 missedCnt;
    UINT32 passCnt;
    UINT32 packetCnt;
    UINT32 starveCnt;
    UINT32 busyMissed;
    BOOL   busyState;
    END_MEDIALIST	*mediaList;
    END_ERR lastError;
    UINT32 curMedia;
    UINT32 curStatus;
    VXB_DEVICE_ID pMiiBus;
#ifdef QE_FCC_HALF_DUPLEX_WAR
    WDOG_ID txWdog;
#endif
    } QE_FCC_DRV_CTRL;



typedef struct
    {
    UINT32 speed; /* Pass to method */
    BOOL duplex;
    UINT32 unit;
    UINT32 connectionType; /* Returned */
    }QE_FCC_MII_PHY_INFO;

#define QE_FCC_MII 0
#define QE_FCC_GMII 1
#define QE_FCC_TBI 2
#define QE_FCC_RTBI 3
#define QE_FCC_RGMII 4
#define QE_FCC_RMII 5


IMPORT STATUS cacheInvalidate (CACHE_TYPE, void *, size_t);
IMPORT STATUS cacheFlush (CACHE_TYPE, void *, size_t);
IMPORT int    intEnable (int);
IMPORT int    intDisable (int);

#ifdef __cplusplus
}
#endif

#endif /* __INCqeFccEndh */

