/* etsecHEnd.h - Freescale ETSEC Ethernet network interface.*/

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01k,16apr08,h_k  converted to new access method.
01j,12sep06,dlk  Remove MOT_ETSEC_RX_QUEUE support.
01i,17jul06,rcs  Added Modular hEnd includes
01h,31may06,dtr  Default less RBDs for additional rx queues.
01g,28apr06,dtr  Changing driver name to motetsec - SPR# 120448.
01f,07mar06,dlk  Disable interrupt coalescing by default (SPR #118631)
01e,21feb06,wap  Fix TX encapsulation code (SPR #117720)
01d,03feb06,wap  Merge in hEnd API changes
01c,16jan06,dtr  Add additional defines for FILER rules.
01b,09jan06,dtr  Add in FCB tx area to drvCtrl.
01a,04jan06,dtr  Initial checkin of file, based on motTsec driver
*/


#ifndef __INCetsecEndh
#define __INCetsecEndh

/* includes */

#ifdef __cplusplus
extern "C" {
#endif

#include <net/ethernet.h>
#include <etherLib.h>
#include <miiLib.h>
#include "hEnd.h"

IMPORT void motEtsecHEndRegister (void);

#ifndef M8260ABBREVIATIONS
#define M8260ABBREVIATIONS

#ifdef  _ASMLANGUAGE
#define CAST(x)
#else /* _ASMLANGUAGE */
typedef volatile UCHAR VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32 VINT32; /* volatile unsigned word */
typedef volatile INT16 VINT16; /* volatile unsigned halfword */
typedef volatile INT8 VINT8;   /* volatile unsigned byte */
typedef volatile UINT32 VUINT32; /* volatile unsigned word */
typedef volatile UINT16 VUINT16; /* volatile unsigned halfword */
typedef volatile UINT8 VUINT8;   /* volatile unsigned byte */
#define CAST(x) (x)
#endif  /* _ASMLANGUAGE */

#endif /* M8260ABBREVIATIONS */

#ifndef TEST_TOKEN_MERGE
    #define TEST_TOKEN_MERGE
    
    /* __(i) used for function names and variables */
    #define __(i)   motEtsec ## i
    
    /* _c_(i)used for macros  and typedefs */
    #define _c_(i)  MOT_ETSEC_ ## i
    
    /* _m_ used for structure name - needed for offsetof */
    #define _m_(i)  mot_etsec_ ## i
    
    /* _o_(i) used for device name string */
    #define _o_(i)  motetsec ## i
    
    /* _p_(i) used for calls to configuration stub */
    #define _p_(i)  sysMotEtsec ## i
#else
    #undef __(i)
        #define __(i)    motEtsec ## i
    #undef _c_(i)
        #define _c_(i)   MOT_ETSEC_ ## i
    #undef _m_(i)
        #define _m_(i)   mot_etsec_ ## i
    #undef _o_(i)
        #define _o_(i)   motetsec ## i  
    #undef _p_(i)
        #define _p_(i)   sysMotEtsec ## i  
#endif

#define HEND_RX_DMA
#define HEND_TX_DMA
#define HEND_INIT_TUPLES

#define MOT_ETSEC_DEVICE_ON_NEXUS	TRUE
#define MOT_ETSEC_HAS_MII_BUS TRUE
#define MOT_ETSEC_NUM_EVENTS (MOT_ETSEC_NUM_RX_QUEUES + MOT_ETSEC_NUM_TX_QUEUES + 1)
#define MOT_ETSEC_NUM_ISRS 3
#define MOT_ETSEC_NUM_RX_QUEUES 8
#define MOT_ETSEC_NUM_TX_QUEUES 1
#define MOT_ETSEC_RX_EVENT_ID 0
#define MOT_ETSEC_TX_EVENT_ID (MOT_ETSEC_NUM_RX_QUEUES + MOT_ETSEC_RX_EVENT_ID + 1)
#define MOT_ETSEC_RX_POLL 0
#define MOT_ETSEC_TX_PKT_COMP_EVENT_ID  (MOT_ETSEC_NUM_RX_QUEUES + MOT_ETSEC_RX_EVENT_ID + 1)
#define MOT_ETSEC_HEND_TX_PAD 0
#define MOT_ETSEC_FRAG_NUM_LIMIT	16 
#define MOT_ETSEC_TX_INT_HANDLE 0
#define MOT_ETSEC_RX_INT_HANDLE 1
#define MOT_ETSEC_ERR_INT_HANDLE 2
#define MOT_ETSEC_TX_DESC_CAPACITY 1
#define MOT_ETSEC_TX_DESC_SPAN 1
#define MOT_ETSEC_ALIGNMENT_OFFSET 0


#ifndef MAC_ADDR_LEN
#define MAC_ADDR_LEN 6
#endif


/* enum declarations for argument passing */
enum motEtsecParams {MOT_ETSEC_UNIT_TYPE                  = 0x10,
                    MOT_ETSEC_CCSBAR_TYPE                = 0x11,
                    MOT_ETSEC_ETSEC_NUM_TYPE              = 0x12,
                    MOT_ETSEC_MAC_ADDR_TYPE              = 0x13,
                    MOT_ETSEC_USR_FLAGS_TYPE             = 0x14,
                    MOT_ETSEC_PHY_PARAM_TYPE             = 0x15,
                    MOT_ETSEC_FUNC_TABLE_PARAM_TYPE      = 0x16,
                    MOT_ETSEC_CONFIG_PARAMS_TYPE         = 0x17};


#define MOT_ETSEC_CACHE_INVAL(address, len)                              \
    CACHE_DRV_INVALIDATE (&pDrvCtrl->bufCacheFuncs, (address), (len));

#define MOT_ETSEC_CACHE_FLUSH(address, len)                               \
    CACHE_DRV_FLUSH (&pDrvCtrl->bufCacheFuncs, (address), (len));     


/* Debug Macros */
#define MOT_ETSEC_DRV_DEBUG     0x0000
#define MOT_ETSEC_DRV_DEBUG_RX         0x0001
#define MOT_ETSEC_DRV_DEBUG_TX         0x0002
#define MOT_ETSEC_DRV_DEBUG_POLL       0x0004
#define MOT_ETSEC_DRV_DEBUG_MII        0x0008
#define MOT_ETSEC_DRV_DEBUG_LOAD       0x0010
#define MOT_ETSEC_DRV_DEBUG_IOCTL      0x0020
#define MOT_ETSEC_DRV_DEBUG_INT        0x0040
#define MOT_ETSEC_DRV_DEBUG_START      0x0080
#define MOT_ETSEC_DRV_DEBUG_INT_RX_ERR 0x0100
#define MOT_ETSEC_DRV_DEBUG_INT_TX_ERR 0x0200
#define MOT_ETSEC_DRV_DEBUG_RX_ERR     0x0400
#define MOT_ETSEC_DRV_DEBUG_TX_ERR     0x0800
#define MOT_ETSEC_DRV_DEBUG_TRACE      0x1000
#define MOT_ETSEC_DRV_DEBUG_TRACE_RX   0x2000
#define MOT_ETSEC_DRV_DEBUG_TRACE_TX   0x4000
#define MOT_ETSEC_DRV_DEBUG_MONITOR    0x8000
#define MOT_ETSEC_DRV_DEBUG_ANY        0xffff
#define MOT_ETSEC_DRV_DEBUG_REG      0x10000
#define MOT_ETSEC_DRV_DEBUG_INIT_MEM   MOT_ETSEC_DRV_DEBUG_LOAD


#define MOT_ETSEC_DEV_NAME        "motetsec"
#define MOT_ETSEC_DEV_NAME_LEN    9
#define MOT_ETSEC_MAX_DEVS        4
#define MOT_ETSEC_DEV_1           0
#define MOT_ETSEC_DEV_2           1
#define MOT_ETSEC_DEV_3           2
#define MOT_ETSEC_DEV_4           3
#define MOT_ETSEC_ADRS_1  0x00024000
#define MOT_ETSEC_ADRS_2  0x00025000
#define MOT_ETSEC_ADRS_3  0x00026000
#define MOT_ETSEC_ADRS_4  0x00027000
#define MOT_ETSEC_REG_CNT 25 

/* Register offsets */
    /* control status registers */
#define MOT_ETSEC_ETSECID                       0x000
#define MOT_ETSEC_ETSEC_ID2                     0x004 
#define MOT_ETSEC_IEVENT		        0x010
#define MOT_ETSEC_IMASK				0x014
#define MOT_ETSEC_EDIS				0x018
#define MOT_ETSEC_ECNTRL			0x020
#define MOT_ETSEC_MINFLR			0x024
#define MOT_ETSEC_PTV				0x028
#define MOT_ETSEC_DMACTRL			0x02c
#define MOT_ETSEC_TBIPA				0x030

    /* Fifo Registers */
    /* new */
#define MOT_ETSEC_FIFO_RX_PAUSE_CTRL		0x050
#define MOT_ETSEC_FIFO_RX_PAUSE_SHUTOFF         0x054
#define MOT_ETSEC_FIFO_RX_ALARM                 0x058
#define MOT_ETSEC_FIFO_RX_ALARM_SHUTOFF         0x05C

#define MOT_ETSEC_FIFO_TX_THR			0x08c
#define MOT_ETSEC_FIFO_STRV			0x098
#define MOT_ETSEC_FIFO_STRV_SHUT		0x09c

    /* Transmit Control and Status Registers */
#define MOT_ETSEC_TCTRL				0x100
#define MOT_ETSEC_TSTAT				0x104
#define MOT_ETSEC_DFVLAN                        0x108
    /* New */
#define MOT_ETSEC_TBDLEN				0x10c

#define MOT_ETSEC_TXIC				0x110
    /* New */
#define MOT_ETSEC_TQUEUE                        0x114
    /* Removed*/
#define MOT_ETSEC_CTBPTR				0x124
    /* New */
#define MOT_ETSEC_TR03WT                        0x140
#define MOT_ETSEC_TR47WT                        0x144
#define MOT_ETSEC_TBDHPTR                       0x180

    /* Changed */
#define MOT_ETSEC_TBPTR				0x184
    /* New */
#define MOT_ETSEC_TBPTRn(n)                     (0x184 + (n*0x8))

    /* New */
#define MOT_ETSEC_TBASEH                        0x200
    /* Changed name */
#define MOT_ETSEC_TBASE				0x204
#define MOT_ETSEC_TBASEn(n)			(0x204 + (n*0x8))

    /* Removed */
#define MOT_ETSEC_OSTBD				0x2b0
#define MOT_ETSEC_OSTBDP		        0x2b4

    /* Receive Control and Status registers */
#define MOT_ETSEC_RCTRL				0x300
#define MOT_ETSEC_RSTAT				0x304
#define MOT_ETSEC_RBDLEN				0x30c
#define MOT_ETSEC_RXIC				0x310

    /* New */
#define MOT_ETSEC_RQUEUE                        0x314
#define MOT_ETSEC_RBIFX                         0x330
#define MOT_ETSEC_RQFAR                         0x334
#define MOT_ETSEC_RQFCR                         0x338
#define MOT_ETSEC_RQFPR                         0x33c

    /* Removed */
#define MOT_ETSEC_CRBPTR		       	0x324

#define MOT_ETSEC_MRBLR				0x340
#define MOT_ETSEC_RBPTRH                        0x380
    /* Changed */
#define MOT_ETSEC_RBPTR				0x384
    /* New*/
#define MOT_ETSEC_RBPTRn(n)			(0x384 + (n*0x8))

#define MOT_ETSEC_RBASEH       			0x400
#define MOT_ETSEC_RBASE				0x404
    /* New*/
#define MOT_ETSEC_RBASEn(n)			(0x404 + (n*0x8))

    /* MAC registers */
#define MOT_ETSEC_MACCFG1			0x500
#define MOT_ETSEC_MACCFG2			0x504
#define MOT_ETSEC_IPGIFG		       	0x508
#define MOT_ETSEC_HAFDUP		       	0x50c
#define MOT_ETSEC_MAXFRM		       	0x510
#define MOT_ETSEC_MIIMCFG			0x520
#define MOT_ETSEC_MIIMCOM			0x524
#define MOT_ETSEC_MIIMADD			0x528
#define MOT_ETSEC_MIIMCON			0x52c
#define MOT_ETSEC_MIIMSTAT			0x530
#define MOT_ETSEC_MIIMIND			0x534
#define MOT_ETSEC_IFSTAT		       	0x53c

#define MOT_ETSEC_MACSTNADDR1			0x540
#define MOT_ETSEC_MACSTNADDR2			0x544

    /* new */
    /* where n = 1-15 */
#define MOT_ETSEC_MACADDR1(n)                   (0x540 + (n * 0x8))
#define MOT_ETSEC_MACADDR2(n)                   (0x544 + (n * 0x8))

#define MOT_ETSEC_TR64				0x680
#define MOT_ETSEC_TR127				0x684
#define MOT_ETSEC_TR255				0x688
#define MOT_ETSEC_TR511				0x68c
#define MOT_ETSEC_TR1K				0x690
#define MOT_ETSEC_TRMAX				0x694
#define MOT_ETSEC_TRMGV				0x698
#define MOT_ETSEC_RBYT				0x69c
#define MOT_ETSEC_RPKT				0x6a0
#define MOT_ETSEC_RFCS				0x6a4
#define MOT_ETSEC_RMCA				0x6a8
#define MOT_ETSEC_RBCA				0x6ac
#define MOT_ETSEC_RXCF				0x6b0
#define MOT_ETSEC_RXPF				0x6b4
#define MOT_ETSEC_RXUO				0x6b8
#define MOT_ETSEC_RALN				0x6bc
#define MOT_ETSEC_RFLR				0x6c0
#define MOT_ETSEC_RCDE				0x6c4
#define MOT_ETSEC_RCSE				0x6c8
#define MOT_ETSEC_RUND				0x6cc
#define MOT_ETSEC_ROVR				0x6d0
#define MOT_ETSEC_RFRG				0x6d4
#define MOT_ETSEC_RJBR				0x6d8
#define MOT_ETSEC_RDRP				0x6dc
#define MOT_ETSEC_TBYT				0x6e0
#define MOT_ETSEC_TPKT				0x6e4
#define MOT_ETSEC_TMCA				0x6e8
#define MOT_ETSEC_TBCA				0x6ec
#define MOT_ETSEC_TXPF				0x6f0
#define MOT_ETSEC_TDFR				0x6f4
#define MOT_ETSEC_TEDF				0x6f8
#define MOT_ETSEC_TSCL				0x6fc
#define MOT_ETSEC_TMCL				0x700
#define MOT_ETSEC_TLCL				0x704
#define MOT_ETSEC_TXCL				0x708
#define MOT_ETSEC_TNCL				0x70c
#define MOT_ETSEC_TDRP				0x714
#define MOT_ETSEC_TJBR				0x718
#define MOT_ETSEC_TFCS				0x71c
#define MOT_ETSEC_TXCF				0x720
#define MOT_ETSEC_TOVR				0x724
#define MOT_ETSEC_TUND				0x728
#define MOT_ETSEC_TFRG				0x72c
#define MOT_ETSEC_CAR1				0x730
#define MOT_ETSEC_CAR2				0x734
#define MOT_ETSEC_CAM1				0x738
#define MOT_ETSEC_CAM2				0x73c
#define MOT_ETSEC_RREJ                          0x740

#define MOT_ETSEC_IADDR0				0x800
#define MOT_ETSEC_IADDR1				0x804
#define MOT_ETSEC_IADDR2				0x808
#define MOT_ETSEC_IADDR3				0x80c
#define MOT_ETSEC_IADDR4				0x810
#define MOT_ETSEC_IADDR5				0x814
#define MOT_ETSEC_IADDR6				0x818
#define MOT_ETSEC_IADDR7				0x81c
#define MOT_ETSEC_GADDR0				0x880
#define MOT_ETSEC_GADDR1				0x884
#define MOT_ETSEC_GADDR2				0x888
#define MOT_ETSEC_GADDR3				0x88c
#define MOT_ETSEC_GADDR4				0x890
#define MOT_ETSEC_GADDR5				0x894
#define MOT_ETSEC_GADDR6				0x898
#define MOT_ETSEC_GADDR7				0x89c

    /* New */
#define MOT_ETSEC_FIFOCFG                       0xa00


#define MOT_ETSEC_ATTR				0xbf8
#define MOT_ETSEC_ATTRELI			0xbfc

/*
 * vxBus access macros
 * Note that ideally, we should use the vxbAccess macros hung off
 * the VXB_DEVICE, but that means incurring an extra function pointer
 * call for each register access. We take a shortcut, based on the
 * fact that we know the ETSEC is a local bus device and that it
 * doesn't require any byte swapping.
 *
 * Note the use of the volatile keyword here. This is required or else
 * the compiler might optimize some of the register accesses away. The
 * Diab compiler doesn't seem to do this, but the GNU compiler will
 * produce non-functional code if the volatile keyword is removed.
 * Caveat utilitor.
 */

#ifdef MOT_ETSEC_VXB_ACCESS
#include <../src/hwif/h/vxbus/vxbAccess.h>

#define ETSEC_HANDLE(p)		((ETSEC_DRV_CTRL *)(p)->pDrvCtrl)->hEnd.handle

#define ETSEC_READ(pDev, addr)					\
    vxbRead32 (ETSEC_HANDLE(pDev),				\
		(UINT32 *)((char *)pDev->pRegBase[0] + addr))

#define ETSEC_WRITE(pDev, addr, data)				\
    vxbWrite32 (ETSEC_HANDLE(pDev),				\
		(UINT32 *)((char *)pDev->pRegBase[0] + addr,	\
		data))
#else

#define ETSEC_READ(pDev, offset)		\
	*(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + offset)

#define ETSEC_WRITE(pDev, offset, val)		\
	do { \
	    volatile UINT32 *pReg = (UINT32 *)((UINT32)pDev->pRegBase[0] + offset); \
	    *(pReg) = (UINT32)(val);		\
	} while ((0))
#endif


#define ETSEC_SETBIT(pDev, offset, val)		\
	ETSEC_WRITE(pDev, offset, ETSEC_READ(pDev, offset) | (val))

#define ETSEC_CLRBIT(pDev, offset, val)		\
	ETSEC_WRITE(pDev, offset, ETSEC_READ(pDev, offset) & ~(val))


/* IEVENT and IMASK Register definitions */
#define MOT_ETSEC_IEVENT_BABR    0x80000000
#define MOT_ETSEC_IEVENT_RXC     0x40000000
#define MOT_ETSEC_IEVENT_BSY     0x20000000
#define MOT_ETSEC_IEVENT_EBERR   0x10000000
#define MOT_ETSEC_IEVENT_MSRO    0x04000000
#define MOT_ETSEC_IEVENT_GTSC    0x02000000
#define MOT_ETSEC_IEVENT_BABT    0x01000000
#define MOT_ETSEC_IEVENT_TXC     0x00800000
#define MOT_ETSEC_IEVENT_TXE     0x00400000
#define MOT_ETSEC_IEVENT_TXB     0x00200000
#define MOT_ETSEC_IEVENT_TXF     0x00100000
#define MOT_ETSEC_IEVENT_LC      0x00040000
#define MOT_ETSEC_IEVENT_CRL     0x00020000
#define MOT_ETSEC_IEVENT_XFUN    0x00010000
#define MOT_ETSEC_IEVENT_RXB0    0x00008000
#define MOT_ETSEC_IEVENT_MMRD	0x00000400
#define MOT_ETSEC_IEVENT_MMWR	0x00000200
#define MOT_ETSEC_IEVENT_GRSC    0x00000100
#define MOT_ETSEC_IEVENT_RXF0    0x00000080

#define MOT_ETSEC_IEVENTS_ALL   (MOT_ETSEC_IEVENT_BABR | MOT_ETSEC_IEVENT_RXC   | \
							MOT_ETSEC_IEVENT_BSY  | MOT_ETSEC_IEVENT_EBERR | \
							MOT_ETSEC_IEVENT_MSRO | MOT_ETSEC_IEVENT_GTSC  | \
							MOT_ETSEC_IEVENT_BABT | MOT_ETSEC_IEVENT_TXC   | \
							MOT_ETSEC_IEVENT_TXE  | MOT_ETSEC_IEVENT_TXB   | \
							MOT_ETSEC_IEVENT_TXF  | MOT_ETSEC_IEVENT_LC    | \
							MOT_ETSEC_IEVENT_CRL  | MOT_ETSEC_IEVENT_XFUN  | \
							MOT_ETSEC_IEVENT_RXB0 | MOT_ETSEC_IEVENT_GRSC  | \
							MOT_ETSEC_IEVENT_RXF0)   

/* Error Disable Registers */
#define MOT_ETSEC_EDIS_BSYDIS    0x20000000
#define MOT_ETSEC_EDIS_EBERRDIS  0x10000000
#define MOT_ETSEC_EDIS_TXEDIS    0x00400000
#define MOT_ETSEC_EDIS_LCDIS     0x00040000
#define MOT_ETSEC_EDIS_CRLDIS    0x00020000
#define MOT_ETSEC_EDIS_XFUNDIS   0x00010000

/* Ethernet Control Register */
#define MOT_ETSEC_ECNTRL_CLRCNT  0x00004000
#define MOT_ETSEC_ECNTRL_AUTOZ   0x00002000
#define MOT_ETSEC_ECNTRL_STEN    0x00001000
#define MOT_ETSEC_ECNTRL_TBIM    0x00000020
#define MOT_ETSEC_ECNTRL_RPM     0x00000010
#define MOT_ETSEC_ECNTRL_R100M   0x00000008

/* Minimum Frame Register Length */
#define MOT_ETSEC_MINFLR_MASK(l) (l&0x0000007f)

/* PTV Register Definition */
#define MOT_ETSEC_PTV_PTE(t)     (t<<16)
#define MOT_ETSEC_PTV_PT(t)      (t)

/* DMA Control Register */
#define MOT_ETSEC_DMACTRL_TDSEN  0x00000080
#define MOT_ETSEC_DMACTRL_TBDSEN 0x00000040
#define MOT_ETSEC_DMACTRL_GRS    0x00000010
#define MOT_ETSEC_DMACTRL_GTS    0x00000008
#define MOT_ETSEC_DMACTRL_TOD    0x00000004
#define MOT_ETSEC_DMACTRL_WWR    0x00000002
#define MOT_ETSEC_DMACTRL_WOP    0x00000001

/* TBI Physical Address Registers */
#define MOT_ETSEC_TBIPA_MASK(a)  (a&0x0000001f)

/* FIFO Transmit Threshold Registers */
#define MOT_ETSEC_FIFO_TX_THR_MASK(a)     (a&0x000001ff)

/* FIFO Transmit Starve Registers */
#define MOT_ETSEC_FIFO_TX_STARVE(a)  (a&0x000001ff)

/* FIFO Transmit Starve Shutoff Registers */
#define MOT_ETSEC_FIFO_TX_STARVE_SHUTOFF(a) (a&0x000001ff)

/* Transmit Control Register */
#define MOT_ETSEC_TCTRL_IPCSEN        0x00004000
#define MOT_ETSEC_TCTRL_TUCSEN        0x00002000
#define MOT_ETSEC_TCTRL_VLINS         0x00001000
#define MOT_ETSEC_TCTRL_THDF         0x00000800
#define MOT_ETSEC_TCTRL_RFC_PAUSE    0x00000010
#define MOT_ETSEC_TCTRL_TFC_PAUSE    0x00000008
#define MOT_ETSEC_TCTRL_TXSCHED_MSK   0x00000006
#define MOT_ETSEC_TCTRL_TXSCHED_SINGLE 0x00000000
#define MOT_ETSEC_TCTRL_TXSCHED_PRIO  0x00000002
#define MOT_ETSEC_TCTRL_TXSCHED_RR    0x00000004


#define MOT_ETSEC_TSTAT_THLT     0x80000000

/* Transmit Interrupt Coalescing */ 
#define MOT_ETSEC_IC_ICEN	0x80000000
#define MOT_ETSEC_IC_ICFCT(a)  ((a&0x000000ff)<<21)
#define MOT_ETSEC_IC_ICTT(a)   (a&0x0000ffff)

/* Recieve Queue - new */
#define MOT_ETSEC_RQUEUE_EX(n)   (0x00800000 >> n)
#define MOT_ETSEC_RQUEUE_EN(n)   (0x00000080 >> n)

/* Receive Bit Field Extract control register - new*/
#define MOT_ETSEC_RBIFX_BCTL(n)  (0xC0000000 >> (n * 8))


#define MOT_ETSEC_OSTBD_R        0x80000000
#define MOT_ETSEC_OSTBD_PAD      0x40000000
#define MOT_ETSEC_OSTBD_W        0x20000000
#define MOT_ETSEC_OSTBD_I        0x10000000
#define MOT_ETSEC_OSTBD_L        0x08000000
#define MOT_ETSEC_OSTBD_TC       0x04000000
#define MOT_ETSEC_OSTBD_DEF      0x02000000
#define MOT_ETSEC_OSTBD_LC       0x00800000
#define MOT_ETSEC_OSTBD_RL       0x00400000
#define MOT_ETSEC_OSTBD_RC(r)    ((r&0x0000003c)>>18)
#define MOT_ETSEC_OSTBD_UN       0x00020000
#define MOT_ETSEC_OSTBD_LEN(l)   (l&0x0000ffff)

/* Alot of Etsec Specific bits */
#define MOT_ETSEC_RCTRL_PAL      0x000f0000
#define MOT_ETSEC_RCTRL_VLEX     0x00002000
#define MOT_ETSEC_RCTRL_FILREN   0x00001000
#define MOT_ETSEC_RCTRL_FSQEN    0x00000800
#define MOT_ETSEC_RCTRL_GHTX     0x00000400
#define MOT_ETSEC_RCTRL_IPCSEN   0x00000200
#define MOT_ETSEC_RCTRL_TUCSEN   0x00000100
#define MOT_ETSEC_RCTRL_PRSDEP   0x000000C0
#define MOT_ETSEC_RCTRL_BC_REJ   0x00000010
#define MOT_ETSEC_RCTRL_PROM     0x00000008
#define MOT_ETSEC_RCTRL_RSF      0x00000004 /* Not defined in ETSEC */
#define MOT_ETSEC_RCTRL_EMEN     0x00000002

#define MOT_ETSEC_RSTAT_QHLT     0x00800000
#define MOT_ETSEC_RSTAT_QHLTn(n)  (0x00800000 >> n)
#define MOT_ETSEC_RSTAT_RXF      0x00000080
#define MOT_ETSEC_RSTAT_RXFn(n)   (0x00000080 >> n)

#define MOT_ETSEC_TDLEN_MASK(l)       (l&0x0000ffff)
#define MOT_ETSEC_RBDLEN_MASK(l)      (l&0x0000ffff)
#define MOT_ETSEC_CRBPTR_MASK(p)      (p&0xfffffff8)
#define MOT_ETSEC_MRBLR_MASK(l)       (l&0x0000ffc0)
#define MOT_ETSEC_RBASE_MASK(p)       (p&0xfffffff8)

#define MOT_ETSEC_MACCFG1_SOFT_RESET     0x80000000
#define MOT_ETSEC_MACCFG1_RESET_RX_MC    0x00080000
#define MOT_ETSEC_MACCFG1_RESET_TX_MC    0x00040000
#define MOT_ETSEC_MACCFG1_RESET_RX_FUN   0x00020000
#define MOT_ETSEC_MACCFG1_RESET_TX_FUN   0x00010000
#define MOT_ETSEC_MACCFG1_LOOPBACK       0x00000100
#define MOT_ETSEC_MACCFG1_RX_FLOW        0x00000020
#define MOT_ETSEC_MACCFG1_TX_FLOW        0x00000010
#define MOT_ETSEC_MACCFG1_SYNCD_RX_EN    0x00000008
#define MOT_ETSEC_MACCFG1_RX_EN          0x00000004
#define MOT_ETSEC_MACCFG1_SYNCD_TX_EN    0x00000002
#define MOT_ETSEC_MACCFG1_TX_EN          0x00000001

#define MOT_ETSEC_MACCFG2_PRE_LEN(l)         ((l<<12) & 0xf000)
#define MOT_ETSEC_MACCFG2_PRE_LEN_GET(r)     ((r&0xf000)>>12)
#define MOT_ETSEC_MACCFG2_IF_MODE(m)         ((m<<8) & 0x0300)
#define MOT_ETSEC_MACCFG2_IF_MODE_GET(r)     ((r&0x0300)>>8)

#define MOT_ETSEC_MACCFG2_IF_MODE_MASK       0x00000003
#define MOT_ETSEC_MACCFG2_IF_MODE_MII        0x00000001
#define MOT_ETSEC_MACCFG2_IF_MODE_GMII_TBI   0x00000002
#define MOT_ETSEC_MACCFG2_HUGE_FRAME         0x00000020
#define MOT_ETSEC_MACCFG2_LENGTH_CHECK       0x00000010
#define MOT_ETSEC_MACCFG2_PADCRC             0x00000004
#define MOT_ETSEC_MACCFG2_CRC_EN             0x00000002
#define MOT_ETSEC_MACCFG2_FULL_DUPLEX        0x00000001

#define MOT_ETSEC_IPGIFG_NBBIPG1(l)      ((l&0x0000007f)<<24)
#define MOT_ETSEC_IPGIFG_NBBIPG2(l)      ( (l&0x0000007f)<<16)
#define MOT_ETSEC_IPGIFG_MIFGE(l)        ((l&0x000000ff)<<8)
#define MOT_ETSEC_IPGIFG_BBIPG(l)        (l&0x0000007f)

#define MOT_ETSEC_HALDUP_ALTBEB_TRUNC(l) ((l&0x0000000f)<<20)
#define MOT_ETSEC_HALFDUP_BEB            0x00080000
#define MOT_ETSEC_HALFDUP_BPNBO          0x00040000
#define MOT_ETSEC_HALFDUP_NBO            0x00020000
#define MOT_ETSEC_HALFDUP_EXCESS_DEF     0x00010000
#define MOT_ETSEC_HALDUP_RETRY(v)        ((v&0x0000000F)<<12)
#define MOT_ETSEC_HALDUP_COL_WINDOW(w)   (w&0x003f)

#define MOT_ETSEC_MAXFRM_MASK(l)         (l&0x0000ffff)
#define MOT_ETSEC_MIIMCFG_RESET          0x80000000
#define MOT_ETSEC_MIIMCFG_NO_PRE         0x00000010
#define MOT_ETSEC_MIIMCFG_MCS(l)         (l&0x00000007)
#define MOT_ETSEC_MIIMCFG_MCS_2          0x00000000
#define MOT_ETSEC_MIIMCFG_MCS_4          0x00000001
#define MOT_ETSEC_MIIMCFG_MCS_6          0x00000002
#define MOT_ETSEC_MIIMCFG_MCS_8          0x00000003
#define MOT_ETSEC_MIIMCFG_MCS_10         0x00000004
#define MOT_ETSEC_MIIMCFG_MCS_14         0x00000005
#define MOT_ETSEC_MIIMCFG_MCS_20         0x00000006
#define MOT_ETSEC_MIIMCFG_MCS_28         0x00000007

#define MOT_ETSEC_MIIMCOM_SCAN_CYCLE     0x00000002
#define MOT_ETSEC_MIIMCOM_READ_CYCLE     0x00000001

#define MOT_ETSEC_MIIMADD_PHYADRS(a)     ((a&0x0000001f)<<8)
#define MOT_ETSEC_MIIMADD_REGADRS(a)     (a&0x0000001f)

#define MOT_ETSEC_MIIMCON_PHY_CTRL(a)    (a&0x0000ffff)

#define MOT_ETSEC_MIIMSTAT_PHY(a)        (a&0x0000ffff)

#define MOT_ETSEC_MIIMIND_NOT_VALID      0x00000004
#define MOT_ETSEC_MIIMIND_SCAN           0x00000002
#define MOT_ETSEC_MIIMIND_BUSY           0x00000001

#define MOT_ETSEC_IFSTAT_EXCESS_DEF      0x00000200

#define MOT_ETSEC_MACSTNADDR1_SA_1       0xff000000
#define MOT_ETSEC_MACSTNADDR1_SA_2       0x00ff0000
#define MOT_ETSEC_MACSTNADDR1_SA_3       0x0000ff00
#define MOT_ETSEC_MACSTNADDR1_SA_4       0x000000ff
#define MOT_ETSEC_MACSTNADDR2_SA_5       0xff000000
#define MOT_ETSEC_MACSTNADDR2_SA_6       0x00ff0000

/* Transmit Buffer Descriptor bit definitions */
#define MOT_ETSEC_TBD_R          0x8000
#define MOT_ETSEC_TBD_PADCRC     0x4000
#define MOT_ETSEC_TBD_W          0x2000
#define MOT_ETSEC_TBD_I          0x1000
#define MOT_ETSEC_TBD_L          0x0800
#define MOT_ETSEC_TBD_TC         0x0400
#define MOT_ETSEC_TBD_DEF        0x0200
#define MOT_ETSEC_TBD_HFE        0x0080
#define MOT_ETSEC_TBD_LC         0x0080		/* late collision */
#define MOT_ETSEC_TBD_CF         0x0040
#define MOT_ETSEC_TBD_RL         0x0040		/* retry limit reached */
#define MOT_ETSEC_TBD_RC         0x003c		/* retry count */
#define MOT_ETSEC_TBD_UN         0x0002		/* underrun */
#define MOT_ETSEC_TBD_TOE        0x0002
#define MOT_ETSEC_TBD_TR         0x0001		/* truncation */

#define MOT_ETSEC_TBD_ERR	\
	(MOT_ETSEC_TBD_LC|MOT_ETSEC_TBD_RL|MOT_ETSEC_TBD_UN|MOT_ETSEC_TBD_TR)

#define MOT_ETSEC_TX_RETRIES(x) \
	(((x) & MOT_ETSEC_TBD_RC) >> 2)

/* Receive Buffer Descriptors bit definitions */
#define MOT_ETSEC_RBD_E          0x8000
#define MOT_ETSEC_RBD_RO1        0x4000
#define MOT_ETSEC_RBD_W          0x2000
#define MOT_ETSEC_RBD_I          0x1000
#define MOT_ETSEC_RBD_L          0x0800
#define MOT_ETSEC_RBD_F          0x0400
#define MOT_ETSEC_RBD_M          0x0200
#define MOT_ETSEC_RBD_BC         0x0080
#define MOT_ETSEC_RBD_MC         0x0040
#define MOT_ETSEC_RBD_LG         0x0020
#define MOT_ETSEC_RBD_NO         0x0010
#define MOT_ETSEC_RBD_SH         0x0008
#define MOT_ETSEC_RBD_CR         0x0004
#define MOT_ETSEC_RBD_OV         0x0002
#define MOT_ETSEC_RBD_TR         0x0001

#define MOT_ETSEC_RBD_ERR  (MOT_ETSEC_RBD_TR | MOT_ETSEC_RBD_OV |  \
                           MOT_ETSEC_RBD_CR | MOT_ETSEC_RBD_SH |  \
                           MOT_ETSEC_RBD_NO | MOT_ETSEC_RBD_LG)

#define MOT_ETSEC_ATTR_ELCWT_NA       0x0
#define MOT_ETSEC_ATTR_ELCWT_RSVD     0x1
#define MOT_ETSEC_ATTR_ELCWT_L2       0x2
#define MOT_ETSEC_ATTR_ELCWT_L2_LOCK  0x3
#define MOT_ETSEC_ATTR_ELCWT(v)       ((v&0x00000003)<<13)

#define MOT_ETSEC_ATTR_BDLWT_NA       0x0
#define MOT_ETSEC_ATTR_BDLWT_RSVD     0x1
#define MOT_ETSEC_ATTR_BDLWT_L2       0x2
#define MOT_ETSEC_ATTR_BDLWT_L2_LOCK  0x3
#define MOT_ETSEC_ATTR_BDLWT(v)       ((v&0x00000003)<<10)

#define MOT_ETSEC_ATTR_RDSEN          0x00000080
#define MOT_ETSEC_ATTR_RBDSEN         0x00000040

#define MOT_ETSEC_ATTRELI_EL(v)       ((v&0x00003fff)<<16)
#define MOT_ETSEC_ATTRELI_EI(v)       (v&0x00003fff)

/* MOT_ETSEC init string parameters */
/* "MMBASE:MOT_ETSEC_PORT:MAC_ADRS:PHY_DEF_MODES:USER_FLAGS:FUNC_TABLE:EXT_PARMS"  */
/*        MMBASE - 85xx local base address. Used as base for driver memory space.
 *      MAC_ADRS - Mac address in 12 digit format eg. 00-A0-1E-11-22-33
 *  PHY_DEF_MODE - Default Attributes passed to the MII driver.
 * USER_DEF_MODE - Mandatory initialization user parameters.
 *    FUNC_TABLE - Table of BSP and Driver callbacks
 *         PARMS - Address of a structure that contains required initialization
 *                 parameters and driver specific tuning parameters.
 *     EXT_PARMS - Address of a structure that contains optional initialization
 *                 parameters and driver specific tuning parameters.
 */

/* MII/PHY PHY_DEF_MODE flags to init the phy driver */
#define MOT_ETSEC_USR_MODE_DEFAULT         0
#define MOT_ETSEC_USR_PHY_NO_AN   0x00000001  /* do not auto-negotiate */
#define MOT_ETSEC_USR_PHY_TBL     0x00000002  /* use negotiation table */
#define MOT_ETSEC_USR_PHY_NO_FD   0x00000004  /* do not use full duplex */
#define MOT_ETSEC_USR_PHY_NO_100  0x00000008  /* do not use 100Mbit speed */
#define MOT_ETSEC_USR_PHY_NO_HD   0x00000010  /* do not use half duplex */
#define MOT_ETSEC_USR_PHY_NO_10   0x00000020  /* do not use 10Mbit speed */
#define MOT_ETSEC_USR_PHY_MON     0x00000040  /* use PHY Monitor */
#define MOT_ETSEC_USR_PHY_ISO     0x00000080  /* isolate a PHY */


/* ECNTRL Ethernet Control */
#define MOT_ETSEC_USR_STAT_CLEAR     0x00000100 /* init + runtime clear mstats*/
#define MOT_ETSEC_USR_STAT_AUTOZ     0x00000200 /* init */
#define MOT_ETSEC_USR_STAT_ENABLE    0x00000400 /* init */

/* PHY bus configuration selections */
#define MOT_ETSEC_USR_MODE_MASK      0x0003f800
#define MOT_ETSEC_USR_MODE_TBI       0x00000800
#define MOT_ETSEC_USR_MODE_RTBI      0x00001000
#define MOT_ETSEC_USR_MODE_MII       0x00002000
#define MOT_ETSEC_USR_MODE_GMII      0x00004000
#define MOT_ETSEC_USR_MODE_RGMII     0x00008000
#define MOT_ETSEC_USR_MODE_RGMII_10  0x00010000
#define MOT_ETSEC_USR_MODE_RGMII_100 0x00020000

/* MOT_ETSEC extended initialization parameters */

/* Bit flags */
/* DMACTRL - Configure the DMA block */
#define MOT_ETSEC_TX_SNOOP_EN          0x00000001 /* snoop Tx Clusters */
#define MOT_ETSEC_TX_BD_SNOOP_EN       0x00000002 /* snoop txbds */
#define MOT_ETSEC_TX_WWR               0x00000004 /* init */
#define MOT_ETSEC_TXBD_WOP             0x00000008 /* init */

/* RCTRL - Receive Control flags */
#define MOT_ETSEC_BROADCAST_REJECT     0x00000010 /* Broadcast Reject */
#define MOT_ETSEC_PROMISCUOUS_MODE     0x00000008 /* Promiscuous Mode*/
#define MOT_ETSEC_RX_SHORT_FRAME       0x00000004 /* Rx Shorter Frames */
                                                 /* if (frame<MINFLR)*/
/* MACCFG1 - Mac Configuration */
#define MOT_ETSEC_MAC_LOOPBACK         0x00000080  /* init + runtime */
#define MOT_ETSEC_MAC_RX_FLOW          0x00000100  /* enable Rx Flow */
#define MOT_ETSEC_MAC_TX_FLOW          0x00000200  /* enable Tx Flow */
/* MACCFG2 Mac Configuration */
#define MOT_ETSEC_MAC_HUGE_FRAME       0x00000400  /* enable huge frame support*/
#define MOT_ETSEC_MAC_PADCRC           0x00000800  /* MAC pads short frames */
                                                  /* and appends CRC */
#define MOT_ETSEC_MAC_CRC_ENABLE       0x00001000  /* MAC appends CRC */
#define MOT_ETSEC_MAC_DUPLEX           0x00002000  /* MAC duplex mode */
/* ATTR */
#define MOT_ETSEC_RX_SNOOP_ENABLE      0x00004000  /* snoop Rx cluster */
#define MOT_ETSEC_RXBD_SNOOP_ENABLE    0x00008000  /* snoop rxbds */
/* MII flags */
#define MOT_ETSEC_MII_NOPRE            0x00010000  /* suppress preamble */
#define MOT_ETSEC_MII_RESET            0x00020000  /* runtime reset MII MGMT */
#define MOT_ETSEC_MII_SCAN_CYCLE       0x00040000  /* Continuous read */
#define MOT_ETSEC_MII_READ_CYCLE       0x00080000  /* Preform single read */
/* HALDUP */
#define MOT_ETSEC_HALDUP_ALTBEB        0x00100000  /* use alternate backoff */ 
                                                  /* algorithm */
#define MOT_ETSEC_HALDUP_BACK_PRESSURE 0x00200000  /* immediate retrans back */
                                                  /* pressure */
#define MOT_ETSEC_HALDUP_EX_DEFERENCE  0x00400000
#define MOT_ETSEC_HALDUP_NOBACKOFF     0x00800000

/* Driver Runtime Attributes */
#define MOT_ETSEC_POLLING_MODE         0x01000000    /* polling mode */
#define MOT_ETSEC_MULTICAST_MODE       0x02000000    /* multicast addressing */

#define FLAG_POLLING_MODE               0x0001
#define FLAG_PROMISC_MODE               0x0002
#define FLAG_ALLMULTI_MODE              0x0004
#define FLAG_MULTICAST_MODE             0x0008
#define FLAG_BROADCAST_MODE             0x0010


/* MOT_ETSEC Attribute Default Values */
#define MOT_ETSEC_IMASK_DEFAULT (MOT_ETSEC_IEVENT_RXC  | MOT_ETSEC_IEVENT_TXC   | \
                            MOT_ETSEC_IEVENT_MSRO | MOT_ETSEC_IEVENT_GTSC  | \
                            MOT_ETSEC_IEVENT_TXB  | MOT_ETSEC_IEVENT_RXB0  | \
                            MOT_ETSEC_IEVENT_TXF  | MOT_ETSEC_IEVENT_GRSC  | \
                            MOT_ETSEC_IEVENT_RXF0 )

#if 0
#define MOT_ETSEC_IEVENT_ERROR  (MOT_ETSEC_IEVENT_BSY  | MOT_ETSEC_IEVENT_EBERR | \
                            MOT_ETSEC_IEVENT_TXE  | MOT_ETSEC_IEVENT_LC    | \
                            MOT_ETSEC_IEVENT_CRL  | MOT_ETSEC_IEVENT_XFUN  | \
                            MOT_ETSEC_IEVENT_BABT | MOT_ETSEC_IEVENT_BABR )
#endif
#define MOT_ETSEC_MACCFG1_DEFAULT (0x0)


/* IF_MODE = 1 ; MII mode 10/100 only */
#define MOT_ETSEC_MACCFG2_DEFAULT  \
                (MOT_ETSEC_MACCFG2_PRE_LEN(7)  | \
                 MOT_ETSEC_MACCFG2_FULL_DUPLEX | \
                 MOT_ETSEC_MACCFG2_PADCRC      | \
                 MOT_ETSEC_MACCFG2_CRC_EN      | \
                 MOT_ETSEC_MACCFG2_IF_MODE(MOT_ETSEC_MACCFG2_IF_MODE_GMII_TBI))

/* CLRCNT = YES,  = NO,  STEN = YES,  TBIM = NO, RPM = NO, R100M = 10 */

#define MOT_ETSEC_ECNTRL_DEFAULT  (MOT_ETSEC_ECNTRL_STEN | MOT_ETSEC_ECNTRL_AUTOZ)

#define MOT_ETSEC_TBIPA_DEFAULT  (30)

#define MOT_ETSEC_EDIS_DEFAULT (MOT_ETSEC_EDIS_EBERRDIS 	| 	\
                               MOT_ETSEC_EDIS_TXEDIS 	| 	\
			       MOT_ETSEC_EDIS_LCDIS    	| 	\
                               MOT_ETSEC_EDIS_CRLDIS 	| 	\
			       MOT_ETSEC_EDIS_XFUNDIS )

/* Minimum Frame Receive Length */
#define MOT_ETSEC_MINFLR_DEFAULT (64)
/* Tx Flow Control PAUSE Time Default  */
#define MOT_ETSEC_PVT_DEFAULT  (MOT_ETSEC_PTV_PTE(0)+ MOT_ETSEC_PTV_PTE(0))

/* DMA Control Register Default */
#define MOT_ETSEC_DMACTRL_DEFAULT  (MOT_ETSEC_DMACTRL_TDSEN  | \
                                   MOT_ETSEC_DMACTRL_TBDSEN | \
                                   MOT_ETSEC_DMACTRL_WWR | \
                                   MOT_ETSEC_DMACTRL_WOP)

#define MOT_ETSEC_FIFO_TX_THR_DEFAULT           (32) 
#define MOT_ETSEC_FIFO_TX_STARVE_DEFAULT        (16) 
#define MOT_ETSEC_FIFO_TX_STARVE_OFF_DEFAULT    (32) 

/* Interrupt Coalescing */

#undef MOT_ETSEC_ENABLE_TXIC
#undef MOT_ETSEC_ENABLE_RXIC

#ifdef MOT_ETSEC_ENABLE_TXIC
/* These values are suspect, 16ct/300clks might be more reasonable */
#define MOT_ETSEC_TXIC_DEFAULT	(MOT_ETSEC_IC_ICEN |		\
				 MOT_ETSEC_IC_ICFCT(128) |	\
				 MOT_ETSEC_IC_ICTT(64))
#else
/* 0 would work */
#define MOT_ETSEC_TXIC_DEFAULT	(MOT_ETSEC_IC_ICFCT(128) |	\
				 MOT_ETSEC_IC_ICTT(64))
#endif /* MOT_ETSEC_ENABLE_TXIC */

#ifdef MOT_ETSEC_ENABLE_RXIC
#define MOT_ETSEC_RXIC_DEFAULT	(MOT_ETSEC_IC_ICEN |		\
				 MOT_ETSEC_IC_ICFCT(32) |	\
				 MOT_ETSEC_IC_ICTT(400))
#else
/* 0 would work */
#define MOT_ETSEC_RXIC_DEFAULT	(MOT_ETSEC_IC_ICFCT(32) |	\
				 MOT_ETSEC_IC_ICTT(400))
#endif /* MOT_ETSEC_ENABLE_RXIC */

/* BACK PRESSURE = NO, RFC_PAUSE = NO, TFC_PAUSE = NO */

#define MOT_ETSEC_TCTRL_DEFAULT  (0)

#define MOT_ETSEC_TSTAT_DEFAULT      (MOT_ETSEC_TSTAT_THLT)

/* PROMISCUOUS = NO, BROAD_REJECT = NO, RX SHORT FRAMES = NO */

#define MOT_ETSEC_RCTRL_DEFAULT (0)


#define MOT_ETSEC_RSTAT_DEFAULT      (0)
#define MOT_ETSEC_MRBLR_DEFAULT      MOT_ETSEC_MRBLR_MASK(0x600)

#define MOT_ETSEC_IPGIFG_DEFAULT     (MOT_ETSEC_IPGIFG_NBBIPG1(0x40) | \
                                     MOT_ETSEC_IPGIFG_NBBIPG2(0x60) | \
                                     MOT_ETSEC_IPGIFG_MIFGE(0x50)   | \
                                     MOT_ETSEC_IPGIFG_BBIPG(0x60))

#define MOT_ETSEC_HAFDUP_DEFAULT     (MOT_ETSEC_HALDUP_ALTBEB_TRUNC(0x0a) | \
                                     MOT_ETSEC_HALDUP_RETRY(0x0f)        | \
                                     MOT_ETSEC_HALDUP_COL_WINDOW(0x37)     )

#define MOT_ETSEC_MAXFRM_DEFAULT      MOT_ETSEC_MAXFRM_MASK(0x0600)
#define MOT_ETSEC_MIICFG_DEFAULT     (MOT_ETSEC_MIIMCFG_MCS(MOT_ETSEC_MIIMCFG_MCS_14))
#define MOT_ETSEC_MIICOM_DEFAULT      0
#define MOT_ETSEC_IFSTAT_DEFAULT      0

#define MOT_ETSEC_ATTR_DEFAULT_NO_L2  (MOT_ETSEC_ATTR_RDSEN	| \
                                      MOT_ETSEC_ATTR_RBDSEN	| \
                                MOT_ETSEC_ATTR_BDLWT(MOT_ETSEC_ATTR_BDLWT_NA)) 

#define MOT_ETSEC_ATTR_DEFAULT_L2				\
	(MOT_ETSEC_ATTR_RDSEN | MOT_ETSEC_ATTR_RBDSEN  |		\
	MOT_ETSEC_ATTR_BDLWT(MOT_ETSEC_ATTR_BDLWT_L2)  |		\
        MOT_ETSEC_ATTR_ELCWT(MOT_ETSEC_ATTR_ELCWT_L2))

/*
 * Set extraction length to 64 bytes. This should be enough
 * to cover the ethernet frame header, plus the IP header, which
 * theoretically should give us a performance boost when doing
 * IP forwarding.
 */
#define MOT_ETSEC_ATTRELI_EL_DEFAULT (MOT_ETSEC_ATTRELI_EL(128) | \
                                     MOT_ETSEC_ATTRELI_EI(0))


/* MOT_ETSEC Register flags */
#define MOT_ETSEC_FLAG_MINFLR      0x00000001
#define MOT_ETSEC_FLAG_MAXFRM      0x00000002
#define MOT_ETSEC_FLAG_PVT         0x00000004
#define MOT_ETSEC_FLAG_TBIPA       0x00000008
#define MOT_ETSEC_FLAG_FIFO_TX     0x00000010
#define MOT_ETSEC_FLAG_IADDR       0x00000020
#define MOT_ETSEC_FLAG_GADDR       0x00000040
#define MOT_ETSEC_FLAG_MACCFG2     0x00000080
#define MOT_ETSEC_FLAG_IPGIFGI     0x00000100
#define MOT_ETSEC_FLAG_HAFDUP      0x00000200
#define MOT_ETSEC_FLAG_MIICFG      0x00000400
#define MOT_ETSEC_FLAG_ATTR        0x00000800

typedef struct
    {
    /* function pointers for BSP and Driver call backs */
    FUNCPTR miiPhyInit;     /* BSP Phy driver init callback */
    FUNCPTR miiPhyInt;      /* BSP call back to process PHY status change interrupt */
    FUNCPTR miiPhyStatusGet;   /* Driver call back to get duplex status  */
    FUNCPTR miiPhyRead;     /* BSP call back to read MII/PHY registers */
    FUNCPTR miiPhyWrite;    /* BSP call back to write MII/PHY registers */
    FUNCPTR enetAddrGet;    /* Driver call back to get the Ethernet address */
    FUNCPTR enetAddrSet;    /* Driver call back to set the Ethernet address */
    FUNCPTR enetEnable;     /* Driver call back to enable the ENET interface */
    FUNCPTR enetDisable;    /* Driver call back to disable the ENET interface */
    FUNCPTR extWriteL2AllocFunc;    /* Driver call back to put tx bd in L2 */
    } MOT_ETSEC_FUNC_TABLE;

typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_ETSEC_RX_BD;
    
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_ETSEC_TX_BD;
 
 /* RCS Remove later */   
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_ETSEC_BD;
/* RCS Remove later */ 
typedef struct
    {
    UINT8   *memBufPtr;    /* Buffer pointer for allocated buffer space */
    UINT32   memBufSize;   /* Buffer pool size */
    MOT_ETSEC_BD *bdBasePtr;    /* Descriptor Base Address */
    UINT32   bdSize;       /* Descriptor Size */
    UINT32   rbdNum;       /* Number of Receive Buffer Descriptors  */
    UINT32   tbdNum;       /* Number of Transmit Buffer Descriptors */
    UINT32   unit0TxVec;
    UINT32   unit0RxVec;
    UINT32   unit0ErrVec;
    UINT32   unit1TxVec;
    UINT32   unit1RxVec;
    UINT32   unit1ErrVec;
    } MOT_ETSEC_PARAMS;

typedef struct
    {
    UINT32   inum_etsecTx;    /* Transmit Interrupt */
    UINT32   inum_etsecRx;    /* Receive Interrupt */
    UINT32   inum_etsecErr;    /* Error Interrupt */
    FUNCPTR  inumToIvec;     /* function to convert INUM to IVEC */
    FUNCPTR  ivecToInum;     /* function to convert IVEC to INUM */
    } MOT_ETSEC_INT_CTRL;


typedef struct
    {
    UINT32  numInts;
    UINT32  numZcopySends;
    UINT32  numNonZcopySends;
    UINT32  numTXBInts;
    UINT32  numBSYInts;
    UINT32  numRXFInts;
    UINT32  numRXBInts;
    UINT32  numGRAInts;
    UINT32  numRXCInts;
    UINT32  numTXCInts;
    UINT32  numTXEInts;
    UINT32  numOTHERInts;

    UINT32  numRXFHandlerEntries;
    UINT32  numRXFHandlerErrQuits;
    UINT32  numRXFHandlerFramesProcessed;
    UINT32  numRXFHandlerFramesRejected;
    UINT32  numRXFHandlerNetBufAllocErrors;
    UINT32  numRXFHandlerNetCblkAllocErrors;
    UINT32  numRXFHandlerNetMblkAllocErrors;
    UINT32  numRXFHandlerFramesCollisions;
    UINT32  numRXFHandlerFramesCrcErrors;
    UINT32  numRXFHandlerFramesLong;
    UINT32  numRXFExceedBurstLimit;

    UINT32  numNetJobAddErrors;

    UINT32  numRxStallsEntered;
    UINT32  numRxStallsCleared;

    UINT32  numTxStallsEntered;
    UINT32  numTxStallsCleared;
    UINT32  numTxStallErrors;

    UINT32  numLSCHandlerEntries;
    UINT32  numLSCHandlerExits;

    UINT32  txErr;
    UINT32  HbFailErr;
    UINT32  txLcErr;
    UINT32  txUrErr;
    UINT32  txCslErr;
    UINT32  txRlErr;
    UINT32  txDefErr;

    UINT32  rxBsyErr;
    UINT32  rxLgErr;
    UINT32  rxNoErr;
    UINT32  rxCrcErr;
    UINT32  rxOvErr;
    UINT32  rxShErr;
    UINT32  rxLcErr;
    UINT32  rxMemErr;
    } MOT_ETSEC_DRIVER_STATS;


typedef union etsec_filer_rbifx
    {
    struct 
	{
	UINT32 b0ctl:2;
	UINT32 b0offset:6;
	UINT32 b1ctl:2;
	UINT32 b1offset:6;
	UINT32 b2ctl:2;
	UINT32 b2offset:6;
	UINT32 b3ctl:2;
	UINT32 b3offset:6;
	}details;

    } FILER_RBIFX;


typedef union  etsec_filer_rqfar
    {
    struct
	{
	UINT32 reserved:24;
	UINT32 tableIndex:8;
	}details;
	
    } FILER_RQFAR;

typedef union  etsec_filer_rqfcr
    {
    struct
	{
	UINT32 rsvd1:16;
	UINT32 qnum:6;
	UINT32 clusterEnable:1;
	UINT32 reject:1;
	UINT32 and:1;
	UINT32 cmp:1;
	UINT32 rsvd2:1;
	UINT32 pid:4;
	}details;
    }FILER_RQFCR;


typedef union etsec_filer_rqfpr
    {
    struct
	{
	UINT32 rsvd1:16;
	UINT32 ebc:1;
	UINT32 vln:1;
	UINT32 cfi:1;
	UINT32 jum:1;
	UINT32 ipf:1;
	UINT32 fif:1;
	UINT32 ip4:1;
	UINT32 ip6:1;
	UINT32 icc:1;
	UINT32 icv:1;
	UINT32 tcp:1;
	UINT32 udp:1;
	UINT32 tuc:1;
	UINT32 tuv:1;
	UINT32 per:1;
	UINT32 eer:1;
	} propertyType;
    struct
	{
	UINT32 byte0:8; 
	UINT32 byte1:8; 
	UINT32 byte2:8; 
	UINT32 byte3:8; 
	} arb;
    struct
	{
	UINT32 resv:8;
	UINT32 destMAChigh:24;
	} dah;
    struct
	{
	UINT32 resv:8;
	UINT32 destMAClow:24;
	} dal;
    struct
	{
	UINT32 resv:8;
	UINT32 srcMAChigh:24;
	} sah;
    struct
	{
	UINT32 resv:8;
	UINT32 srcMAClow:24;
	} sal;
    struct
	{
	UINT32 rsvd:16;
	UINT32 etherType;
	}ety;
    struct
	{
	UINT32 rsvd:20;
	UINT32 vlanId:12;
	}vln;
    struct
	{
	UINT32 rsvd:29;
	UINT32 vlanPrio:3;
	}pri;
    struct
	{
	UINT32 rsvd:24;
	UINT32 typeOfService:8;
	}tos;
    struct
	{
	UINT32 rsvd:8;
	UINT32 l4ProtoId:24;
	}l4p;
    struct
	{
	UINT32 destIp;
	}dia;
    struct
	{
	UINT32 srcIp;
	}sia;

    struct
	{
	UINT32 rsvd:8;
	UINT32 destPort:24;
	}dpt;
    struct
	{
	UINT32 rsvd:8;
	UINT32 srcPort:24;
	}spt;
    }FILER_RQFPR;


typedef struct etsec_filer_entry
    {
    UINT32 filerEnable;
    UINT32 rbifx;
    UINT32 rqfar;
    UINT32 rqfcr;
    UINT32 rqfpr;
    } FILER_ENTRY;

#define FILER_TABLE_SIZE 256

#define MOT_ETSEC_RQFCR_PID_MASK 0
#define MOT_ETSEC_RQFCR_PID_PROP 1
#define MOT_ETSEC_RQFCR_PID_ARB  2
#define MOT_ETSEC_RQFCR_PID_DAH  3
#define MOT_ETSEC_RQFCR_PID_DAL  4
#define MOT_ETSEC_RQFCR_PID_SAH  5
#define MOT_ETSEC_RQFCR_PID_SAL  6
#define MOT_ETSEC_RQFCR_PID_ETY  7
#define MOT_ETSEC_RQFCR_PID_VID  8
#define MOT_ETSEC_RQFCR_PID_PRI  9
#define MOT_ETSEC_RQFCR_PID_TOS  10
#define MOT_ETSEC_RQFCR_PID_L4P  11
#define MOT_ETSEC_RQFCR_PID_DIA  12
#define MOT_ETSEC_RQFCR_PID_SIA  13
#define MOT_ETSEC_RQFCR_PID_DPT  14
#define MOT_ETSEC_RQFCR_PID_SPT  15

#define MOT_ETSEC_RQFCR_CMP_EQ   0
#define MOT_ETSEC_RQFCR_CMP_GTE  1
#define MOT_ETSEC_RQFCR_CMP_NE   2
#define MOT_ETSEC_RQFCR_CMP_LT   3

#define FILER_DISABLE 0
#define FILER_ENABLE 1

#define MOT_ETSEC_RQFCR_PID(x) (x & 0xf)
#define MOT_ETSEC_RQFCR_CMP(x) ((x & 3)<< 5)
#define MOT_ETSEC_RQFCR_AND(x) ((x & 1)<< 7)
#define MOT_ETSEC_RQFCR_REJ(x) ((x & 1)<< 8)
#define MOT_ETSEC_RQFCR_CLE(x) ((x & 1)<< 9)
#define MOT_ETSEC_RQFCR_Q(x) ((x & 0x3f)<< 10)

#define MOT_ETSEC_RQFAR_INDEX(x) (x & 0xff)
#define MOT_ETSEC_RBIFX_BYTE_ORDER(a0,b0,a1,b1,a2,b2,a3,b3) (((a0 & 3)<< 30) | \
						  ((a1 & 3)<< 22) | \
						  ((a2 & 3)<< 14) | \
						  ((a3 & 3)<< 6) | \
						  ((b0 & 0x3f)<< 24) | \
						  ((b1 & 0x3f)<< 16) | \
						  ((b2 & 0x3f)<< 8 ) | \
						  ((b3 & 0x3f)     ))

#define MOT_ETSEC_RQFPR_PROP(x)  (x & 0xffff)
#define MOT_ETSEC_RQFPR_ARB(x)  (x)
#define MOT_ETSEC_RQFPR_DAH(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_DAL(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_SAH(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_SAL(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_ETY(x)  (x & 0xffff)
#define MOT_ETSEC_RQFPR_VLN(x)  (x & 0xfff)
#define MOT_ETSEC_RQFPR_PRI(x)  (x & 0x7)
#define MOT_ETSEC_RQFPR_TOS(x)  (x & 0xff)
#define MOT_ETSEC_RQFPR_L4P(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_DIA(x)  (x)
#define MOT_ETSEC_RQFPR_SIA(x)  (x)
#define MOT_ETSEC_RQFPR_DPT(x)  (x & 0xffffff)
#define MOT_ETSEC_RQFPR_SPT(x)  (x & 0xffffff)

#define MOT_ETSEC_RQFPR_PROP_EBC 0x8000
#define MOT_ETSEC_RQFPR_PROP_VLN 0x4000
#define MOT_ETSEC_RQFPR_PROP_CFI 0x2000
#define MOT_ETSEC_RQFPR_PROP_JUM 0x1000
#define MOT_ETSEC_RQFPR_PROP_IPF 0x0800
#define MOT_ETSEC_RQFPR_PROP_FIF 0x0400
#define MOT_ETSEC_RQFPR_PROP_IP4 0x0200
#define MOT_ETSEC_RQFPR_PROP_IP6 0x0100
#define MOT_ETSEC_RQFPR_PROP_ICC 0x0080
#define MOT_ETSEC_RQFPR_PROP_ICV 0x0040
#define MOT_ETSEC_RQFPR_PROP_TCP 0x0020
#define MOT_ETSEC_RQFPR_PROP_UDP 0x0010
#define MOT_ETSEC_RQFPR_PROP_PER 0x0002
#define MOT_ETSEC_RQFPR_PROP_EER 0x0001


/* Driver State Variables */

#define MOT_ETSEC_STATE_INIT          0x00
#define MOT_ETSEC_STATE_NOT_LOADED    0x00
#define MOT_ETSEC_STATE_LOADED        0x01
#define MOT_ETSEC_STATE_NOT_RUNNING   0x00
#define MOT_ETSEC_STATE_RUNNING       0x02

/* Internal driver flags */

#define MOT_ETSEC_OWN_BUF_MEM    0x01    /* internally provided memory for data*/
#define MOT_ETSEC_INV_TBD_NUM    0x02    /* invalid tbdNum provided */
#define MOT_ETSEC_INV_RBD_NUM    0x04    /* invalid rbdNum provided */
#define MOT_ETSEC_POLLING        0x08    /* polling mode */
#define MOT_ETSEC_PROM           0x20    /* promiscuous mode */
#define MOT_ETSEC_MCAST          0x40    /* multicast addressing mode */
#define MOT_ETSEC_FD             0x80    /* full duplex mode */
#define MOT_ETSEC_OWN_BD_MEM     0x10    /* internally provided memory for BDs */
#define MOT_ETSEC_MIN_TX_PKT_SZ   100     /* the smallest packet we send */

#define MOT_ETSEC_CL_NUM_DEFAULT   128   /* number of tx clusters */
#define MOT_ETSEC_CL_MULTIPLE       11    /* ratio of clusters to RBDs */
#define MOT_ETSEC_TBD_NUM_DEFAULT  128    /* default number of TBDs */
#define MOT_ETSEC_RBD_NUM_DEFAULT  128    /* default number of RBDs */
#define MOT_ETSEC_SECONDARY_RBD_NUM_DEFAULT 32
#define MOT_ETSEC_TX_POLL_NUM      1     /* one TBD for poll operation */

#define MOT_ETSEC_CL_OVERHEAD      4     /* prepended cluster overhead */
#define MOT_ETSEC_CL_ALIGNMENT     64    /* cluster required alignment */
#define MOT_ETSEC_CL_SIZE          1536  /* cluster size */
#define MOT_ETSEC_MBLK_ALIGNMENT   64    /* mBlks required alignment */
#define MOT_ETSEC_RX_BD_SIZE          0x8   /* size of MOT_ETSEC BD */
#define MOT_ETSEC_TX_BD_SIZE          0x8   /* size of MOT_ETSEC BD */
#define MOT_ETSEC_BD_ALIGN         64   /* required alignment for BDs */
#define MOT_ETSEC_BUF_ALIGN        64   /* required alignment for data buffer */


/* Describe RX FCB bits */
/* 0 - 3 bytes */
#define MOT_ETSEC_RX_FCB_VLAN      0x80000000
#define MOT_ETSEC_RX_FCB_IP        0x40000000
#define MOT_ETSEC_RX_FCB_IP6       0x20000000
#define MOT_ETSEC_RX_FCB_TUP       0x10000000
#define MOT_ETSEC_RX_FCB_CIP       0x08000000
#define MOT_ETSEC_RX_FCB_CTU       0x04000000
#define MOT_ETSEC_RX_FCB_EIP       0x02000000
#define MOT_ETSEC_RX_FCB_ETU       0x01000000
#define MOT_ETSEC_RX_FCB_PER_MSK   0x000C0000
#define MOT_ETSEC_RX_FCB_RQ_MSK    0x00003F00
#define MOT_ETSEC_RX_FCB_PRO_MSK   0x000000ff
/* 4 - 7 bytes */
#define MOT_ETSEC_RX_FCB_VLCTL_MSK 0x0000ffff

#define MOT_ETSEC_RX_FCB_CHK_VLAN(x) (*x & MOT_ETSEC_RX_FCB_VLAN) 
#define MOT_ETSEC_RX_FCB_CHK_IP(x)   (*x & MOT_ETSEC_RX_FCB_IP) 
#define MOT_ETSEC_RX_FCB_CHK_IP6(x)  (*x & MOT_ETSEC_RX_FCB_IP6) 
#define MOT_ETSEC_RX_FCB_CHK_TUP(x)  (*x & MOT_ETSEC_RX_FCB_TUP) 
#define MOT_ETSEC_RX_FCB_CHK_CIP(x)  (*x & MOT_ETSEC_RX_FCB_CIP) 
#define MOT_ETSEC_RX_FCB_CHK_CTU(x)  (*x & MOT_ETSEC_RX_FCB_CTU) 
#define MOT_ETSEC_RX_FCB_CHK_EIP(x)  (*x & MOT_ETSEC_RX_FCB_EIP) 
#define MOT_ETSEC_RX_FCB_CHK_ETU(x)  (*x & MOT_ETSEC_RX_FCB_ETU) 
#define MOT_ETSEC_RX_FCB_CHK_PER(x)  ((*x & MOT_ETSEC_RX_FCB_PER_MSK)>>18) 

typedef struct txfcb {
	volatile UINT8		txfcb_flags;
	volatile UINT8		txfcb_rsvd;
	volatile UINT8		txfcb_l4os;
	volatile UINT8		txfcb_l3os;
	volatile UINT16		txfcb_phcs;
	volatile UINT16		txfcb_vlan;
} MOT_TX_FCB;

/* Describe TX FCB bits */
/* 0 - 3 bytes */

#define MOT_ETSEC_TX_FCB_VLAN      0x80
#define MOT_ETSEC_TX_FCB_IP        0x40
#define MOT_ETSEC_TX_FCB_IP6       0x20
#define MOT_ETSEC_TX_FCB_TUP       0x10
#define MOT_ETSEC_TX_FCB_UDP       0x08
#define MOT_ETSEC_TX_FCB_CIP       0x04
#define MOT_ETSEC_TX_FCB_CTU       0x02
#define MOT_ETSEC_TX_FCB_NPH       0x01

#define MOT_ETSEC_TX_FCB_L4OS_MSK  0x0000ff00
#define MOT_ETSEC_TX_FCB_L3OS_MSK  0x000000ff
#define MOT_ETSEC_TX_FCB_L3L4OS_MSK  0x0000ffff

/* 4 - 7 bytes */
#define MOT_ETSEC_TX_FCB_PHCS_MSK   0xffff0000
#define MOT_ETSEC_TX_FCB_VLCTL_MSK  0x0000ffff

#define MOT_ETSEC_TX_FCB_SET_VLAN(x) (*x | MOT_ETSEC_TX_FCB_VLAN) 
#define MOT_ETSEC_TX_FCB_SET_IP(x)   (*x | MOT_ETSEC_TX_FCB_IP) 
#define MOT_ETSEC_TX_FCB_SET_IP6(x)  (*x | MOT_ETSEC_TX_FCB_IP6) 
#define MOT_ETSEC_TX_FCB_SET_TUP(x)  (*x | MOT_ETSEC_TX_FCB_TUP) 
#define MOT_ETSEC_TX_FCB_SET_UDP(x)  (*x | MOT_ETSEC_TX_FCB_UDP) 
#define MOT_ETSEC_TX_FCB_SET_CIP(x)  (*x | MOT_ETSEC_TX_FCB_CIP) 
#define MOT_ETSEC_TX_FCB_SET_CTU(x)  (*x | MOT_ETSEC_TX_FCB_CTU) 
#define MOT_ETSEC_TX_FCB_SET_NPH(x)  (*x | MOT_ETSEC_TX_FCB_NPH)
 
#define MOT_ETSEC_TX_FCB_SET_L3L4OS(x,y,z)  (((*x & ~(MOT_ETSEC_TX_FCB_L3L4OS_MSK)) | \
                                            (MOT_ETSEC_TX_FCB_L3OS_MSK & y) | \
					    (MOT_ETSEC_TX_FCB_L4OS_MSK & (z<<8))))

#define MOT_ETSEC_TX_FCB_SET_PHCS(x,y)  ((*((UINT32*)x + 1) | (MOT_ETSEC_TX_FCB_PHCS_MSK & (y<<16))))
#define MOT_ETSEC_TX_FCB_SET_VLCTL(x,y)  ((*((UINT32*)x + 1) | (MOT_ETSEC_TX_FCB_VLCTL_MSK & y)))

#define MOT_ETSEC_FCB_LEN 8
/*
 * the total is 0x638 and it accounts for the required alignment
 * of receive data buffers, and the cluster overhead.
 */
#define XXX_MOT_ETSEC_MAX_CL_LEN ((MII_ETH_MAX_PCK_SZ             \
                            + (MOT_ETSEC_BUF_ALIGN - 1)       \
                            + MOT_ETSEC_BUF_ALIGN             \
                            + MOT_ETSEC_FCB_LEN               \
                            + (MOT_ETSEC_CL_OVERHEAD - 1))    \
                            & (~ (MOT_ETSEC_CL_OVERHEAD - 1)))

#define MOT_ETSEC_MAX_CL_LEN     ROUND_UP(XXX_MOT_ETSEC_MAX_CL_LEN,MOT_ETSEC_BUF_ALIGN)

#define MOT_ETSEC_RX_CL_SZ       (MOT_ETSEC_MAX_CL_LEN)
#define MOT_ETSEC_TX_CL_SZ       (MOT_ETSEC_MAX_CL_LEN)

/* BIT mask defines for hardware specific PHY events. */
#define MOT_ETSEC_PHY_EVENT_AUTONEG_ERROR    0x0001
#define MOT_ETSEC_PHY_EVENT_SPEED            0x0002
#define MOT_ETSEC_PHY_EVENT_DUPLEX           0x0004
#define MOT_ETSEC_PHY_EVENT_AUTONEG_COMPLETE 0x0008
#define MOT_ETSEC_PHY_EVENT_LINK             0x0010
#define MOT_ETSEC_PHY_EVENT_SYMBOL_ERROR     0x0020
#define MOT_ETSEC_PHY_EVENT_FALSE_CARRIER    0x0040
#define MOT_ETSEC_PHY_EVENT_FIFO_ERROR       0x0080
#define MOT_ETSEC_PHY_EVENT_XOVER            0x0100
#define MOT_ETSEC_PHY_EVENT_DOWNSHIFT        0x0200
#define MOT_ETSEC_PHY_EVENT_POLARITY         0x0400
#define MOT_ETSEC_PHY_EVENT_JABBER           0x0800

/* PHY Access definitions */
#define MOT_ETSEC_PHY_GIG_STATUS_REG	0xa
#define MOT_ETSEC_PHY_1000_M_LINK_FD     0x0800
#define MOT_ETSEC_PHY_1000_M_LINK_OK     0x1000

#define MOT_ETSEC_PHY_LINK_STATUS        0x5
#define MOT_ETSEC_PHY_10_M_LINK_FD       0x0040
#define MOT_ETSEC_PHY_100_M_LINK_FD      0x0100

typedef struct
    {
    UINT8  autonegError;            /* 0-N/A, 0 - none,   1- error */
    UINT8  duplex;                  /* 1 - half,   2- full */
#define MOT_ETSEC_PHY_DUPLEX_HALF    (1)
#define MOT_ETSEC_PHY_DUPLEX_FULL    (2)
    UINT8  speed;                   /* 0-N/A, 1 - 10, 2- 100, 3 -1G */
#define MOT_ETSEC_PHY_SPEED_10       (1)
#define MOT_ETSEC_PHY_SPEED_100      (2)
#define MOT_ETSEC_PHY_SPEED_1000     (3)
    UINT8  link;                    /* 0-N/A, 1 - down, 2- up */
    UINT8  symbolError;             /* 0-N/A, 1 - none, 2- error */
    UINT8  autoNegComplete;         /* 0-N/A, 1 - no, 2- completed */
    UINT8  energyDetect;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  falseCarrier;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  downShift;               /* 0-N/A, 1 - no, 2- detected */
    UINT8  fifoError;               /* 0-N/A, 1 - none, 2- error */
    UINT8  xover;                   /* 0-N/A, 1 - MDI, 2- MDIX */
    UINT8  polarity;                /* 0-N/A, 1 - normal, 2- reversed */
    UINT8  jabber;                  /* 0-N/A, 1 - no, 2- detected */
    UINT8  pageReceived;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  cableLength;             /* 0-N/A */
    UINT8  txPause;                 /* 0-N/A, 1 - no, 2- detected */
    UINT8  rxPause;                 /* 0-N/A, 1 - no, 2- detected */
    UINT8  farEndFault;             /* 0-N/A, 1 - no, 2- detected */
    UINT32 rxErrorCntr;             /* 0-N/A, number of rx errors */
    UINT32 reserved[4];             /* future use */
    } MOT_ETSEC_PHY_STATUS;

typedef struct mot_etsec_dl_callbacks
    {
    FUNCPTR extWriteL2AllocFunc;  /* Driver call back to put tx bd in L2 */   
    } MOT_ETSEC_DL_CALLBACKS;

typedef union mot_etsec_dl_event_rec
        {
        HEND_RX_CONTROL rx;
        HEND_TX_CONTROL tx;
        }MOT_ETSEC_DL_EVENT_REC;
        
typedef struct mot_etsec_hend_event_rec
    {
    HEND_EVENT_REC hEnd;
    MOT_ETSEC_DL_EVENT_REC dl;
    }MOT_ETSEC_HEND_EVENT_REC;
    
#if 0    
typedef struct mot_etsec_hend_qrec
    {
    MOT_ETSEC_HEND_EVENT_REC eRec;
    HEND_QREC  qRec;
    } MOT_ETSEC_HEND_QREC;
#endif

/* The definition of the driver control structure */
    
typedef struct mot_etsec_drv_ctrl
    {
    HEND_DRV_CTRL hEnd;  
    MOT_ETSEC_HEND_EVENT_REC  eventRec[MOT_ETSEC_NUM_EVENTS];
    HEND_ISR_REC isrRec[MOT_ETSEC_NUM_ISRS]; 
    int          numRxQueues;
    SL_LIST *    pRxQueueList;
    SL_LIST *    pTxQueueList;
    
    int          firstRxIndex;
    int          firstTxIndex;

    UINT32       inum_etsecTx;  /* MOT_ETSEC Tx interrupt num */
    UINT32       inum_etsecRx;  /* MOT_ETSEC Rx interrupt num */
    UINT32       inum_etsecErr;  /*MOT_ETSEC Err interrupt num */
    FUNCPTR      inumToIvec;  /* Conversion utility */
    FUNCPTR      ivecToInum;  /* Conversion utility */
    UINT32       initFlags;   /* user init flags */
    UINT32       userFlags;
    UINT32       retries;
    UINT32       maxRetries;

    /* Bsp specific functions and call backs */
    MOT_ETSEC_FUNC_TABLE * initFuncs;

    /* Driver specific init parameters */
    MOT_ETSEC_PARAMS     * initParms;

    /* Interrupt Controller Specific info */
    MOT_ETSEC_INT_CTRL *intCtrl;

    UINT32      etsecNum;        /* physical MOT_ETSEC 0 or 1 */
    UINT32      ecntrl;
    UINT32      fifoTxBase;     /* address of Tx FIFO in internal RAM */
    UINT32      fifoRxBase;     /* address of Rx FIFO in internal RAM */

    char      * pBufAlloc;      /* Allocated MOT_ETSEC memory pool base */
    char      * pBufBase;       /* Rounded MOT_ETSEC memory pool base */
    UINT32      bufSize;        /* MOT_ETSEC memory pool size */

    MOT_ETSEC_BD   * pBdAlloc;       /* MOT_ETSEC BDs Alloc pointer */
    MOT_ETSEC_BD   * pBdBase;        /* MOT_ETSEC BDs base */
    UINT32      bdSize;         /* MOT_ETSEC BDs size */
   
    MOT_ETSEC_BD   * pTbdBase;   

    volatile BOOL rxStall;       /* rx handler stalled - no Tbd */
    PHY_INFO   * phyInfo;        /* info on a MII-compliant PHY */
    /* transmit buffer descriptor management */

    volatile MOT_ETSEC_BD   * pTbdNext;        /* TBD index */

    UINT16      tbiAdr;         /* tbi interface address */
    UINT32      phyFlags;       /* Phy flags */
    UINT32      flags;          /* driver flags */
    UINT32      state;          /* driver state including load flag */
    UINT32      intMask;        /* interrupt mask register */
    UINT32      intErrorMask;   /* interrupt error mask register */
                                 /* transmit command */
    char       * pClBlkArea;     /* cluster block pointer */
    UINT32       clBlkSize;      /* clusters block memory size */
    char       * pMBlkArea;      /* mBlock area pointer */
    UINT32       mBlkSize;       /* mBlocks area memory size */
    CACHE_FUNCS  bufCacheFuncs;  /* cache descriptor */
    CL_POOL_ID   pClPoolId;      /* cluster pool identifier */

    BOOL         lscHandling;

    /* function pointers to support unit testing */

    FUNCPTR     netJobAdd;
    FUNCPTR     muxTxRestart;
    FUNCPTR     muxError;

    /* Bsp specific functions and call backs */

    FUNCPTR     phyInitFunc;     /* BSP Phy Init */
    FUNCPTR     phyStatusFunc;   /* Status Get function */
    FUNCPTR     miiPhyRead;      /* mii Read */
    FUNCPTR     miiPhyWrite;     /* mii Write */
    FUNCPTR     enetEnable;      /* enable ethernet */
    FUNCPTR     enetDisable;     /* disable ethernet */
    FUNCPTR     enetAddrGet;     /* get ethernet address */
    FUNCPTR     enetAddrSet;     /* set ethernet Address */
    FUNCPTR     extWriteL2AllocFunc; /* Use ext write alloc L2 for Tx BD */ 

    END_CAPABILITIES hwCaps;     /* interface hardware offload capabilities */

#ifdef MOT_ETSEC_DBG
    MOT_ETSEC_DRIVER_STATS *stats;
#endif
    UINT32 missedCnt;
    UINT32 passCnt;
    UINT32 packetCnt;
    UINT32 starveCnt;
    UINT32 busyMissed;
    BOOL   busyState;
    END_MEDIALIST	*mediaList;
    END_ERR lastError;
    UINT32 curMedia;
    UINT32 curStatus;
    VXB_DEVICE_ID pMiiBus;
    MOT_TX_FCB       * txFCBBuf;
#ifdef ETSEC_HALF_DUPLEX_WAR
    WDOG_ID txWdog;
#endif
    } MOT_ETSEC_DRV_CTRL;

#define MOT_ETSEC_EVENT_CLEAR(pDev)					\
    ETSEC_WRITE((pDev), MOT_ETSEC_IEVENT, ETSEC_READ((pDev), MOT_ETSEC_IEVENT))

#define MOT_ETSEC_RX_EVENT_CLEAR(pDev)					\
    ETSEC_WRITE((pDev),MOT_ETSEC_IEVENT,   \
        MOT_ETSEC_IEVENT_RXF0|MOT_ETSEC_IEVENT_BSY)

#define MOT_ETSEC_RX_INT_ENABLE(pDev) \
    { int key;\
    key = intLock(); \
    pDrvCtrl->intMask |= MOT_ETSEC_IEVENT_RXF0; \
    ETSEC_WRITE((pDev),MOT_ETSEC_IMASK,pDrvCtrl->intMask); \
    intUnlock(key);}

#define MOT_ETSEC_TX_STALL_ENTER(pDev)

#define MOT_ETSEC_TX_STALL_EXIT(pDev)					\
    ETSEC_SETBIT((pDev), MOT_ETSEC_TSTAT, MOT_ETSEC_TSTAT_THLT);


#define MOT_ETSEC_TX_INT_DISABLE(pDev)					\
    ETSEC_CLRBIT((pDev), MOT_ETSEC_IMASK, MOT_ETSEC_IEVENT_TXF);

#define MOT_ETSEC_TX_INT_ENABLE(pDev)					\
    { int key; \
      key= intLock(); \
      pDrvCtrl->intMask |= MOT_ETSEC_IEVENT_TXF;			\
      ETSEC_WRITE((pDev), MOT_ETSEC_IMASK, pDrvCtrl->intMask);	\
      intUnlock(key); }

#define MOT_ETSEC_GET_EVENT_REC(x) ((MOT_ETSEC_HEND_EVENT_REC *)(x))

#define MOT_ETSEC_GET_DRV_CTRL(x) \
((int)(x) - (offsetof (struct mot_etsec_drv_ctrl,eventRec[(((HEND_EVENT_REC *)(x))->index)])))

#define MOT_ETSEC_TX_CAPACITY(x) (x)
    
/* MIB macros */
#define MOT_ETSEC_HEND_M2_INIT(p)  \
    {                        \
    endM2Init(&(p)->hEnd.endObj, M2_ifType_ethernet_csmacd, \
              (u_char *) &(p)->hEnd.enetAddr, 6, ETHERMTU, \
              1000000000, \
              IFF_NOTRAILERS | IFF_MULTICAST | IFF_BROADCAST); \
    }
#define MOT_ETSEC_HEND_M2_PKT(p,m,z)

#ifndef EIOCSIFRULES
#define EIOCSIFRULES 0x10000000
#define EIOCGIFRULES 0x20000000
#endif

IMPORT STATUS cacheInvalidate (CACHE_TYPE, void *, size_t);
IMPORT STATUS cacheFlush (CACHE_TYPE, void *, size_t);
IMPORT int    intEnable (int);
IMPORT int    intDisable (int);

#ifdef __cplusplus
}
#endif

#endif /* __INCetsecEndh */

