/* hEndParamsSys.h - */

/* Copyright (c) 2005 Wind River Systems, Inc. */
                                                                                
/*
modification history
--------------------
01b,26sep05,dlk  Added sysHEndParamAttach() and sysHEndSearch() prototypes.
01a, 28may05, rcs written
*/

#ifndef __INChEndParamsSysh
#define __INChEndParamsSysh
 
#ifdef __cplusplus
extern "C" {
#endif

extern void sysHEndParamAttach (void * pParam, UINT32 paramType,
				UINT32 storageType, char * pName, UINT32 unit);
extern HEND_INST_SEARCH * sysHEndSearch (void);

#ifdef __cplusplus
}
#endif
#endif /* __INChEndParamsSysh */   
