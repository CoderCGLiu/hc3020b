/* hEndVxBus.h - */
 
/*
 * Copyright (c) 2005-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

                                                                                
/*
modification history
--------------------
01e,04aug08,h_k  updated VxBus version. (CQ:128598)
01d,14apr08,h_k  VXB_VERSION_3
01c,31jan06,pdg  updated vxbus version
01b,02sep05,mdo  Add vxb prefix
01a,28may05,rcs  written
*/
  
#ifndef __INChEndVxBush
#define __INChEndVxBush
 
#ifdef __cplusplus
extern "C" {
#endif

#ifndef
#define __HEND_STRING(x)    __STRING(x) /* expand x, then stringify */
#endif

struct drvBusFuncs __(Funcs) =
    {
    __(InstInit),        /* devInstanceInit */
    __(InstInit2),       /* devInstanceInit2 */
    __(InstConnect),     /* devConnect */
    NULL,                /* powerModeMgr */
    NULL,                /* create */
    NULL,                /* start */
    NULL,                /* detach */
    NULL,                /* quiesce */
    NULL,                /* suspend */
    NULL                 /* resume */
    };

struct vxbPlbRegister __(DevRegistration) =
    {
    /* b */
        {
        NULL,                    /* pNext */
        VXB_DEVID_DEVICE,        /* devID */
        VXB_BUSID_PLB,           /* busID = PCI */
        VXB_VER_4_0_0,           /* vxbVersion */
        __HEND_STRING(_o_(hend)), /* drvName */
        &__(Funcs),              /* pDrvBusFuncs */
        NULL                     /* devProbe */
        },
    };
#ifdef __cplusplus
}
#endif

#endif /* __INChEndVxBush */

