/* geiHEnd.h - Intel GEI Ethernet network interface.*/

/*
 * Copyright (c) 2005-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01j,16apr08,h_k  converted to new access method.
01i,13jun07,h_k  removed #warning.
01h,13jun07,tor  remove VIRT_ADDR
01g,14feb07,dtr  Move geiPciDevVendList init.
01f,13apr06,pmr  add 8257x support.
01f,17jul06,rcs  Added Modular hEnd includes
01e,23feb06,pmr  fix warning
01d,21feb06,pmr  SPR 117805.
01c,06feb06,pmr  cleanup
01b,27sep05,pmr  fixed warnings
01a,16aug05,pmr  written based on gei82543End.h
*/

#ifndef __INCgeiEndh
#define __INCgeiEndh

/* includes */

#ifdef __cplusplus
extern "C" {
#endif

#include <net/ethernet.h>
#include <etherLib.h>
#include <miiLib.h>
#include "hEnd.h"
#include <hwif/vxbus/vxbPciLib.h>

#ifndef TEST_TOKEN_MERGE
    #define TEST_TOKEN_MERGE
    
    /* __(i) used for function names and variables */
    #define __(i)   gei ## i
    
    /* _c_(i)used for macros  and typedefs */
    #define _c_(i)  GEI_ ## i
    
    /* _m_ used for structure name - needed for offsetof */
    #define _m_(i)  gei_ ## i
    
    /* _o_(i) used for device name string */
    #define _o_(i)  gei ## i
#else
    #undef __(i)
        #define __(i)    gei ## i
    #undef _c_(i)
        #define _c_(i)   GEI_ ## i
    #undef _m_(i)
        #define _m_(i)   gei_ ## i
    #undef _o_(i)
        #define _o_(i)   gei ## i  
#endif

IMPORT void geiHEndRegister (void);

#define HEND_RX_DMA
#define HEND_TX_DMA
#define HEND_INIT_TUPLES

#define GEI_DEVICE_ON_PCI   TRUE
#define GEI_HAS_MII_BUS FALSE
#define GEI_NUM_EVENTS 2
#define GEI_NUM_RX_QUEUES 1
#define GEI_NUM_TX_QUEUES 1
#define GEI_RX_POLL 0

#define GEI_MEMSIZE_CSR            (0x20000)     /* 128Kb CSR memory size */

#define GEI_NUM_ISRS 1

#define GEI_DEV_NAME         "gei"
#define GEI_DEV_NAME_LEN     4
#define GEI_MAX_DEVS         99	   /* in theory there should be no max */

#define GEI_CL_NUM_DEFAULT   128   /* number of tx clusters */
#define GEI_CL_MULTIPLE      11    /* ratio of clusters to RBDs */
#define GEI_TBD_NUM_DEFAULT  128   /* default number of TBDs */
#define GEI_RBD_NUM_DEFAULT  128   /* default number of RBDs */
#define GEI_TX_POLL_NUM      1     /* one TBD for poll operation */
#define GEI_FRAG_NUM_LIMIT   4 

#define GEI_CL_OVERHEAD      4    /* prepended cluster overhead */
#define GEI_CL_ALIGNMENT     64   /* cluster required alignment */
#define GEI_CL_SIZE          1536 /* cluster size */
#define GEI_MBLK_ALIGNMENT   64   /* mBlks required alignment */
#define GEI_RX_BD_SIZE       16   /* size of GEI RX BD */
#define GEI_TX_BD_SIZE       16   /* size of GEI TX BD */
#define GEI_BD_ALIGN         64   /* required alignment for BDs */
#define GEI_BUF_ALIGN        64   /* required alignment for data buffer */
#define GEI_RX_EVENT_ID      0
#define GEI_TX_EVENT_ID      1
#define GEI_ALIGNMENT_OFFSET 0
#ifndef GEI_OFFSET_VALUE          /* parameter in component CDF */
#define GEI_OFFSET_VALUE GEI_ALIGNMENT_OFFSET
#endif  /* GEI_OFFSET_VALUE */
#ifndef GEI_JUMBO_MTU_VALUE       /* parameter in component CDF */
#define GEI_JUMBO_MTU_VALUE 0
#endif  /* GEI_JUMBO_MTU_VALUE */

/*
 * the total is 0x630 and it accounts for the required alignment
 * of receive data buffers, and the cluster overhead.
 */

#define XXX_GEI_MAX_CL_LEN ((MII_ETH_MAX_PCK_SZ              \
                            + (GEI_BUF_ALIGN - 1)       \
                            + GEI_BUF_ALIGN             \
                            + (GEI_CL_OVERHEAD - 1))    \
                            & (~ (GEI_CL_OVERHEAD - 1)))

#define GEI_MAX_CL_LEN     ROUND_UP(XXX_GEI_MAX_CL_LEN,GEI_BUF_ALIGN)

#define GEI_RX_CL_SZ       (GEI_MAX_CL_LEN)
#define GEI_TX_CL_SZ       (GEI_MAX_CL_LEN)

typedef struct
    {
    /* function pointers for BSP and Driver call backs */
    FUNCPTR miiPhyInit;     /* BSP Phy driver init callback */
    FUNCPTR miiPhyInt;      /* BSP call back to process PHY status change interrupt */
    FUNCPTR miiPhyStatusGet;   /* Driver call back to get duplex status  */
    FUNCPTR miiPhyRead;     /* BSP call back to read MII/PHY registers */
    FUNCPTR miiPhyWrite;    /* BSP call back to write MII/PHY registers */
    FUNCPTR enetAddrGet;    /* Driver call back to get the Ethernet address */
    FUNCPTR enetAddrSet;    /* Driver call back to set the Ethernet address */
    FUNCPTR enetEnable;     /* Driver call back to enable the ENET interface */
    FUNCPTR enetDisable;    /* Driver call back to disable the ENET interface */
    FUNCPTR extWriteL2AllocFunc;    /* Driver call back to put tx bd in L2 */
    } GEI_FUNC_TABLE;

typedef struct
    {
    UINT32   phyAddr;        /* phy physical address */
    UINT32   phyDefMode;     /* default mode */
    UINT32   phyMaxDelay;    /* max delay */
    UINT32   phyDelayParm;   /* poll interval if in poll mode */
    MII_AN_ORDER_TBL * phyAnOrderTbl;  /* autonegotiation table */
    } GEI_PHY_PARAMS;

typedef struct gei_dl_callbacks
    {
    FUNCPTR extWriteL2AllocFunc;  /* Driver call back to put tx bd in L2 */   
    } GEI_DL_CALLBACKS;

typedef union gei_dl_event_rec
        {
        HEND_RX_CONTROL rx;
        HEND_TX_CONTROL tx;
        }GEI_DL_EVENT_REC;
        
typedef struct gei_hend_event_rec
    {
    HEND_EVENT_REC hEnd;
    GEI_DL_EVENT_REC dl;
    }GEI_HEND_EVENT_REC;
    
/* The definition of the driver control structure */

/* define */

#define UNKNOWN (-1)
                 
#ifndef PHYS_TO_BUS_ADDR
#define PHYS_TO_BUS_ADDR(unit,physAddr)                                   \
    ((int) pDrvCtrl->adaptor.sysLocalToBus ?                                  \
    (*pDrvCtrl->adaptor.sysLocalToBus) (unit, physAddr) : physAddr)
#endif /* PHYS_TO_BUS_ADDR */

#ifndef BUS_TO_PHYS_ADDR
#define BUS_TO_PHYS_ADDR(unit,busAddr)                                     \
    ((int) pDrvCtrl->adaptor.sysBusToLocal ?                                  \
    (*pDrvCtrl->adaptor.sysBusToLocal)(unit, busAddr) : busAddr)
#endif /* BUS_TO_PHYS_ADDR */

#define TX_DESC_TYPE_CLEAN              0   /* TX descriptor type */
#define TX_DESC_TYPE_COPY               1      /* -- copy/chain/EOP */
#define TX_DESC_TYPE_CHAIN              2
#define TX_DESC_TYPE_EOP                3

/* TX descriptors structure */

#define TXDESC_BUFADRLOW_OFFSET         0   /* buf mem low offset */
#define TXDESC_BUFADRHIGH_OFFSET        4   /* buf mem high offset */
#define TXDESC_LENGTH_OFFSET            8   /* buf length offset */
#define TXDESC_CSO_OFFSET               10  /* checksum offset */
#define TXDESC_CMD_OFFSET               11  /* command offset */
#define TXDESC_STATUS_OFFSET            12  /* status offset */
#define TXDESC_CSS_OFFSET               13  /* cksum start */
#define TXDESC_SPECIAL_OFFSET           14  /* special field */
#define TXDESC_SIZE                     16  /* descriptor size */
typedef char GEI_TX_BD[TXDESC_SIZE];

#define TXDESC_CTX_IPCSS_OFFSET         0   /* CXT descriptor IPCSS */
#define TXDESC_CTX_TUCSS_OFFSET         4   /* CXT descriptor TUCSS */

/*
 * Extended TCP/IP Context TX descriptor structure.
 * Identified by DEXT bit = 1 in TUCMD field, and DTYP = 0000b
 */

#define TXDESC_CTX_IPCSS_OFFSET         0   /* IP checksum start offset */
#define TXDESC_CTX_IPCSO_OFFSET         1   /* IP checksum offset offset */
#define TXDESC_CTX_IPCSE_OFFSET         2   /* IP checksum end offset */
#define TXDESC_CTX_TUCSS_OFFSET         4   /* TCP/UDP checksum start offset */
#define TXDESC_CTX_TUCSO_OFFSET         5   /* TCP/UDP cksum. offset offset */
#define TXDESC_CTX_TUCSE_OFFSET         6   /* TCP/UDP checksum end offset */
/*
 * The PKTLEN field is 20 bits, and DTYP is 4 bits. DTYP appears in the
 * High-order 4 bits of the byte at offset 10.
 */
#define TXDESC_CTX_PKTLEN_OFFSET        8   /* Packet len for segmentation */
#define TXDESC_CTX_DTYP_OFFSET          10  /* Descriptor type == 0000b */
#define TXDESC_CTX_TUCMD_OFFSET         11  /* context command byte offset */
#define TXDESC_CTX_STATUS_OFFSET        12  /* Status nibble byte offset */
#define TXDESC_CTX_HDRLEN_OFFSET        13  /* Header length offset (seg) */
#define TXDESC_CTX_MSS_OFFSET           14  /* MSS offset (segmentation) */

/* RX descriptor structure */

#define RXDESC_BUFADRLOW_OFFSET         0   /* buf mem low offset */
#define RXDESC_BUFADRHIGH_OFFSET        4   /* buf mem high offset */
#define RXDESC_LENGTH_OFFSET            8   /* length offset */
#define RXDESC_CHKSUM_OFFSET            10  /* cksum offset */
#define RXDESC_STATUS_OFFSET            12  /* status offset */
#define RXDESC_ERROR_OFFSET             13  /* error offset */
#define RXDESC_SPECIAL_OFFSET           14  /* special offset */
#define RXDESC_SIZE                     16  /* descriptor size */
typedef char GEI_RX_BD[RXDESC_SIZE];

#define TX_ETHER_PHY_SIZE               (EH_SIZE + ETHERMTU + 2)

#define PRO1000_PCI_VENDOR_ID           0x8086 /* Intel vendor ID */
#define PRO1000_PCI_DEVICE_ID           0x1001 /* device ID */
#define PRO1000F_PCI_SUBSYSTEM_ID       0x1003 /* device subsystem ID */
#define PRO1000T_PCI_SUBSYSTEM_ID       0x1004
#define PRO1000T_PCI_DEVICE_ID          0x1004 /* bizard case */
#define PRO1000F_BOARD                  0x1003 /* backward compatible */
#define PRO1000T_BOARD                  0x1003 /* backward compatible */
#define INTEL_PCI_VENDOR_ID             0x8086

#define PRO1000_573_BOARD               0x108b /* 82573 MAC */
#define PRO1000_546_BOARD               0x100e /* 82540/82545/82546 MAC */
#define PRO1000_544_BOARD               0x1008 /* 82544 MAC */
#define PRO1000_543_BOARD               0x1003 /* 82543 MAC */
#define GEI_BOARD_TYPE_UNKNOWN          0xffffffff

#define PRO1000_543_PCI_DEVICE_ID_T     (0x1001) /* Copper */
#define PRO1000_543_PCI_DEVICE_ID_FT    (0x1004) /* Fiber / Copper */
#define PRO1000_544_PCI_DEVICE_ID_XT    (0x1008) /* Copper */
#define PRO1000_544_PCI_DEVICE_ID_XF    (0x1009) /* Fiber */
#define PRO1000_544_PCI_DEVICE_ID_GC    (0x100c) /* Copper */
#define PRO1000_540_PCI_DEVICE_ID_XT    (0x100e) /* Copper only */
#define PRO1000_545_PCI_DEVICE_ID_XT    (0x100f) /* Copper */
#define PRO1000_546_PCI_DEVICE_ID_XT    (0x1010) /* Copper - 82546 EB */
#define PRO1000_545_PCI_DEVICE_ID_MF    (0x1011) /* Fiber */
#define PRO1000_546_PCI_DEVICE_ID_MF    (0x1012) /* Fiber */
#define PRO1000_541_PCI_DEVICE_ID_XT    (0x1078) /* Copper */
#define PRO1000_546_PCI_DEVICE_ID_XT2   (0x1079) /* Copper - 82546 GB */
#define PRO1000_573_PCI_DEVICE_ID_V    (0x108b) /* Copper - 82573 V */
#define PRO1000_571_PCI_DEVICE_ID_EB   (0x105e) /* Copper - 82571 EB */
#define PRO1000_573_PCI_DEVICE_ID_IAMT  (0x108c) /* Copper - 82573E IAMT */

#define GEI_COPPER_MEDIA                1
#define GEI_FIBER_MEDIA                 2

#define GEI_PHY_GMII_TYPE               1

#define DEFAULT_RXRES_PROCESS_FACTOR    2
#define DEFAULT_TIMER_INTERVAL          2       /* 2 seconds */
#define DEFAULT_DRV_FLAGS               0
#define DEFAULT_LOAN_RXBUF_FACTOR       4
#define DEFAULT_MBUF_COPY_SIZE          256
#define DEFAULT_NUM_TXDES               24
#define DEFAULT_NUM_RXDES               24
#define DEFAULT_RXINT_DELAY             0
#define DEFAULT_MULTI_FILTER_TYPE       MULTI_FILTER_TYPE_47_36
#define DEFAULT_FLOW_CONTRL             FLOW_CONTRL_HW
#define DEFAULT_TX_REPORT               TX_REPORT_RS
#define DEFAULT_TIPG_IPGT_F             6
#define DEFAULT_TIPG_IPGT_T             8
#define DEFAULT_TIPG_IPGR1              8
#define DEFAULT_TIPG_IPGR2              6

/* flags available to config system */ 

#define GEI_END_SET_TIMER               0x0001 /* use a watchdog timer */   
#define GEI_END_SET_RX_PRIORITY         0x0002 /* RX has higher priority (543 only) */
#define GEI_END_FREE_RESOURCE_DELAY     0x0004 /* allow delay to free loaned TX cluster */ 
#define GEI_END_JUMBO_FRAME_SUPPORT     0x0008 /* Jumbo Frame allowed */
#define GEI_END_RX_IP_XSUM              0x0010 /* RX IP XSUM allowed, not supported */
#define GEI_END_RX_TCPUDP_XSUM          0x0020 /* RX TCP XSUM allowed, not supported */
#define GEI_END_TX_IP_XSUM              0x0040 /* TX IP XSUM allowed, not supported */
#define GEI_END_TX_TCPUDP_XSUM          0x0080 /* TX TCP XSUM allowed, not supported */
#define GEI_END_TX_TCP_SEGMENTATION     0x0100 /* TX TCP segmentation, not supported */
#define GEI_END_TBI_COMPATIBILITY       0x0200 /* TBI compatibility workaround (543 only) */
#define GEI_END_USER_MEM_FOR_DESC_ONLY  0x0400 /* cacheable user mem for RX descriptors only */ 
#define GEI_END_FORCE_FLUSH_CACHE       0x1000 /* force flush data cache */
#define GEI_END_FORCE_INVALIDATE_CACHE  0x2000 /* force flush data cache */

#define GEI_END_CSUM       (GEI_END_RX_IP_XSUM | GEI_END_TX_IP_XSUM |\
                            GEI_END_RX_TCPUDP_XSUM | GEI_END_TX_TCPUDP_XSUM) 

/*
 * The offset at which the packet checksum starts.
 * Specified this way to handle and IPv6 header immediately
 * following an RFC 894 Ethernet header.
 */
#define GEI_RXCSUM_PKT_CSUM_OFF	(14 + 40)


#define GEI_DEFAULT_POLL_LOOPS		1
#define GEI_DEFAULT_RXDES_NUM           GEI_RBD_NUM_DEFAULT
#define GEI_DEFAULT_TXDES_NUM           GEI_TBD_NUM_DEFAULT
#define GEI_DEFAULT_USR_FLAG            (GEI_END_SET_TIMER | \
                                         GEI_END_SET_RX_PRIORITY | \
                                         GEI_END_FREE_RESOURCE_DELAY)

#define GEI_DEFAULT_ETHERHEADER         (SIZEOF_ETHERHEADER)
#define GEI_MAX_FRAME_SIZE              16288 /* based on default RX/TX 
                                               buffer configuration */

#define GEI_MAX_JUMBO_MTU_SIZE          (GEI_MAX_FRAME_SIZE - \
                                         GEI_DEFAULT_ETHERHEADER - \
                                         ETHER_CRC_LENGTH)   

#define GEI_DEFAULT_JUMBO_MTU_SIZE      9000      /* 9000 bytes */

#define AVAIL_TX_INT                    (INT_TXDW_BIT)
#define AVAIL_RX_INT                    (INT_RXO_BIT | INT_RXTO_BIT)
#define AVAIL_RX_TX_INT                 (AVAIL_TX_INT | AVAIL_RX_INT)
#define AVAIL_LINK_INT                  (INT_LSC_BIT)
#define INT_LINK_CHECK                  (AVAIL_LINK_INT | INT_RXCFG_BIT)
#define INTEL_82543GC_VALID_INT         (AVAIL_RX_TX_INT | INT_LINK_CHECK | INT_TXDLOW_BIT)
 
#define MAX_TXINT_DELAY                 65536
#define MAX_RXINT_DELAY                 65536
#define MIN_TXINT_DELAY                 1
#define TXINT_DELAY_LESS                5
#define TXINT_DELAY_MORE                512 

#define ETHER_CRC_LENGTH                4
#define RX_CRC_LEN                      ETHER_CRC_LENGTH
#define MAX_ETHER_PACKET_SIZE           1514 
#define MIN_ETHER_PACKET_SIZE           60
#define ETHER_ADDRESS_SIZE              6

#define CARRIER_EXTENSION_BYTE          0x0f

#define INTEL_82543GC_MTA_NUM           128

#define INTEL_82543GC_MULTIPLE_DES      8

#define TX_COLLISION_THRESHOLD          16
#define FDX_COLLISION_DISTANCE          64
#define HDX_COLLISION_DISTANCE          64
#define BIG_HDX_COLLISION_DISTANCE      512

#define NUM_RAR                         16
#define NUM_MTA                         128
#define NUM_VLAN                        128
#define MAX_NUM_MULTI                   (NUM_RAR + NUM_MTA - 1)

#define MULTI_FILTER_TYPE_47_36         0
#define MULTI_FILTER_TYPE_46_35         1
#define MULTI_FILTER_TYPE_45_34         2
#define MULTI_FILTER_TYPE_43_32         3

#define FLOW_CONTRL_NONE                0
#define FLOW_CONTRL_TRANSMIT            1
#define FLOW_CONTRL_RECEIVE             2
#define FLOW_CONTRL_ALL                 3
#define FLOW_CONTRL_HW                  0xf

#define TX_REPORT_RS                    1
#define TX_REPORT_RPS                   2

#define DMA_RX_PRIORITY                 1
#define DMA_FAIR_RX_TX                  2

#define FULL_DUPLEX_MODE                1
#define HALF_DUPLEX_MODE                2
#define DUPLEX_HW                       3
#define DEFAULT_DUPLEX_MODE             FULL_DUPLEX_MODE

#define END_SPEED_10M                   10000000    /* 10Mbs */
#define END_SPEED_100M                  100000000   /* 100Mbs */
#define END_SPEED_1000M                 1000000000  /* 1000Mbs */
#define END_SPEED                       END_SPEED_1000M

#define TYPE_PRO1000F_PCI               1
#define TYPE_PRO1000T_PCI               2
#define GEI82543_HW_AUTO                0x1
#define GEI82543_FORCE_LINK             0x2

/* general flags */

#define FLAG_POLLING_MODE               0x0001
#define FLAG_PROMISC_MODE               0x0002
#define FLAG_ALLMULTI_MODE              0x0004
#define FLAG_MULTICAST_MODE             0x0008
#define FLAG_BROADCAST_MODE             0x0010

/* misc */

#define TX_RESTART_NONE                 0x4000 /* muxTxRestart not scheduled */
#define TX_RESTART_TRUE                 0x8000 /* muxTxRestart is scheduled */
#define LINK_STATUS_OK                  0
#define LINK_STATUS_ERROR               1
#define LINK_STATUS_UNKNOWN             2
#define FREE_ALL_AUTO                   1
#define FREE_ALL_FORCE                  2
#define TX_LOAN_TRANSMIT                1
#define TX_COPY_TRANSMIT                2
#define LINK_TIMEOUT_IN_QUAR_SEC        12 /* 3s */

#define GEI_MII_PHY_CAP_FLAGS           (MII_PHY_10 | MII_PHY_100 | \
                                         MII_PHY_FD | MII_PHY_HD | \
                                         MII_PHY_1000T_FD)

/* Offsets of checksum fields within protocol headers */

#define IP_HDR_CKSUM_OFFSET             10  /* Offset within IP header */
#define TCP_HDR_CKSUM_OFFSET            16  /* Offset within TCP header */
#define UDP_HDR_CKSUM_OFFSET            6   /* Offset within UDP header */

/* register area */

/* TX registers */

#define INTEL_82543GC_TDBAL             0x3800  /* Tx Descriptor Base Low */
#define INTEL_82543GC_TDBAH             0x3804  /* Tx Descriptor Base High */
#define INTEL_82543GC_TDLEN             0x3808  /* Tx Descriptor Length */
#define INTEL_82543GC_TDH               0x3810  /* Tx Descriptor Head */
#define INTEL_82543GC_TDT               0x3818  /* Tx Descriptor Tail */

/* RX registers */

#define INTEL_82543GC_RDBAL             0x2800  /* Rx Descriptor Base Low */
#define INTEL_82543GC_RDBAH             0x2804  /* Rx Descriptor Base High */
#define INTEL_82543GC_RDLEN             0x2808  /* Rx Descriptor Length */
#define INTEL_82543GC_RDH               0x2810  /* Rx Descriptor Head */
#define INTEL_82543GC_RDT               0x2818  /* Rx Descriptor Tail */

/* Interrupt registers */

#define INTEL_82543GC_ICR               0xc0    /* Interrupt Cause Read */
#define INTEL_82543GC_ICS               0xc8    /* Interrupt Cause Set */
#define INTEL_82543GC_IMS               0xd0    /* Interrupt Mask Set/Read */
#define INTEL_82543GC_IMC               0xD8    /* Interrupt Mask Clear */

#define INTEL_82543GC_CTRL              0x0     /* Device Control */
#define INTEL_82543GC_STATUS            0x8     /* Device Status */
#define INTEL_82543GC_EECD              0x10    /* EEPROM/Flash Data */
#define INTEL_82543GC_EERD              0x14    /* shortcut EEPROM read access*/
#define INTEL_82543GC_CTRL_EXT          0x18    /* Extended Device Control */
#define INTEL_82543GC_MDI               0x20    /* MDI Control */
#define INTEL_82543GC_FCAL              0x28    /* Flow Control Adr Low */
#define INTEL_82543GC_FCAH              0x2c    /* Flow Control Adr High */
#define INTEL_82543GC_FCT               0x30    /* Flow Control Type */
#define INTEL_82543GC_VET               0x38    /* VLAN EtherType */
#define INTEL_82546EB_ITR               0xc4    /* register for 82540/5/6 MAC only, 
                                                   interrupt throttle register */
#define INTEL_82543GC_RCTL              0x100   /* Receive Control */
#define INTEL_82543GC_FCTTV             0x170   /* Flow Ctrl TX Timer Value */
#define INTEL_82543GC_TXCW              0x178   /* TX Configuration Word */
#define INTEL_82543GC_RXCW              0x180   /* RX Configuration Word */
#define INTEL_82543GC_TCTL              0x400   /* TX control */
#define INTEL_82543GC_TIPG              0x410   /* Transmit IPG */
#define INTEL_82543GC_EXTCNF_CTRL	0xF00	/* Extended config control */
#define INTEL_82543GC_EXTCNF_SIZE	0xF08	/* Extended config control */
#define INTEL_82543GC_PBA               0x1000  /* Packet Buffer Allocation */
#define INTEL_82543GC_FCRTL             0x2160  /* Flow ctrl RX Threshold Lo*/
#define INTEL_82543GC_FCRTH             0x2168  /* Flow ctrl RX Threshold hi*/
#define INTEL_82543GC_RDTR              0x2820  /* Rx Delay Timer Ring */
#define INTEL_82546EB_RADV              0x282C  /* register for 82540/5/6 MAC only,
                                                   absolute Rx Delay Timer register */
#define INTEL_82543GC_TIDV              0x3820  /* Tx Interrupt Delay Value */
#define INTEL_82543GC_TXDCTL            0x3828  /* Transmit descriptor control */
#define INTEL_82546EB_TADV              0x382C  /* register for 82540/5/6 MAC only,
                                                   absolute Tx Interrupt Delay register */
#define INTEL_82543GC_RXCSUM            0x5000  /* Receive Checksum control */
#define INTEL_82543GC_MTA               0x5200  /* Multicast Table Array */
#define INTEL_82543GC_RAL               0x5400  /* Rx Adr Low */
#define INTEL_82543GC_RAH               0x5404  /* Rx Adr High */
#define INTEL_82543GC_VLAN              0x5600  /* VLAN Filter Table Array */

/* Statistic Registers */

#define INTEL_82543GC_CRCERRS           0x4000
#define INTEL_82543GC_ALGNERRC          0x4004
#define INTEL_82543GC_SYMERRS           0x4008
#define INTEL_82543GC_RXERRC            0x400c
#define INTEL_82543GC_MPC               0x4010
#define INTEL_82543GC_SCC               0x4014
#define INTEL_82543GC_ECOL              0x4018
#define INTEL_82543GC_MCC               0x401c
#define INTEL_82543GC_LATECOL           0x4020
#define INTEL_82543GC_COLC              0x4028
#define INTEL_82543GC_TUC               0x402c
#define INTEL_82543GC_DC                0x4030
#define INTEL_82543GC_TNCRS             0x4034
#define INTEL_82543GC_SEC               0x4038
#define INTEL_82543GC_CEXTEER           0x403c
#define INTEL_82543GC_RLEC              0x4040
#define INTEL_82543GC_XONRXC            0x4048
#define INTEL_82543GC_XONTXC            0x404c
#define INTEL_82543GC_XOFFRXC           0x4050
#define INTEL_82543GC_XOFFTXC           0x4054
#define INTEL_82543GC_FCRUC             0x4058
#define INTEL_82543GC_PRC64             0x405c
#define INTEL_82543GC_PRC127            0x4060
#define INTEL_82543GC_PRC255            0x4064
#define INTEL_82543GC_PRC511            0x4068
#define INTEL_82543GC_PRC1023           0x406c
#define INTEL_82543GC_PRC1522           0x4070
#define INTEL_82543GC_GPRC              0x4074
#define INTEL_82543GC_BPRC              0x4078
#define INTEL_82543GC_MPRC              0x407c
#define INTEL_82543GC_GPTC              0x4080
#define INTEL_82543GC_GORL              0x4088
#define INTEL_82543GC_GORH              0x408c
#define INTEL_82543GC_GOTL              0x4090
#define INTEL_82543GC_GOTH              0x4094
#define INTEL_82543GC_RNBC              0x40a0
#define INTEL_82543GC_RUC               0x40a4
#define INTEL_82543GC_RFC               0x40a8
#define INTEL_82543GC_ROC               0x40ac
#define INTEL_82543GC_RJC               0x40b0
#define INTEL_82543GC_TORL              0x40c0
#define INTEL_82543GC_TORH              0x40c4
#define INTEL_82543GC_TOTL              0x40c8
#define INTEL_82543GC_TOTH              0x40cc
#define INTEL_82543GC_TPR               0x40d0
#define INTEL_82543GC_TPT               0x40d4
#define INTEL_82543GC_PTC64             0x40d8
#define INTEL_82543GC_PTC127            0x40dc
#define INTEL_82543GC_PTC255            0x40e0
#define INTEL_82543GC_PTC511            0x40e4
#define INTEL_82543GC_PTC1023           0x40e8
#define INTEL_82543GC_PTC1522           0x40ec
#define INTEL_82543GC_MPTC              0x40f0
#define INTEL_82543GC_BPTC              0x40f4
#define INTEL_82543GC_TSCTC             0x40f8
#define INTEL_82543GC_TSCTFC            0x40fc
#define INTEL_82543GC_RDFH              0x2410
#define INTEL_82543GC_RDFT              0x2418
#define INTEL_82543GC_RDFHS             0x2420
#define INTEL_82543GC_RDFTS             0x2428
#define INTEL_82543GC_RDFPC             0x2430
#define INTEL_82543GC_TDFH              0x3410
#define INTEL_82543GC_TDFT              0x3418
#define INTEL_82543GC_TDFHS             0x3420
#define INTEL_82543GC_TDFTS             0x3428
#define INTEL_82543GC_TDFPC             0x3430

/* Rx Configuration Word Field */

#define RXCW_C_BIT                      0x20000000

/* EEPROM Structure */

#define EEPROM_WORD_SIZE                0x40      /* 0-0x3f */
#define EEPROM_SUM                      0xBABA
#define EEPROM_INDEX_SIZE               0x40
#define EEPROM_INDEX_BITS               6
#define EEPROM_CMD_BITS                 3
#define EEPROM_DATA_BITS                16
#define EEPROM_READ_OPCODE              0x6
#define EEPROM_WRITE_OPCODE             0x5
#define EEPROM_ERASE_OPCODE             0x7

#define EEPROM_IA_ADDRESS               0x0
#define EEPROM_ICW1                     0xa
#define EEPROM_ICW1_SWDPIO_BITS         0x1e0
#define EEPROM_ICW1_SWDPIO_SHIFT        5
#define EEPROM_ICW1_ILOS_BIT            0x10
#define EEPROM_ICW1_FRCSPD_BIT          0x800
#define EEPROM_ICW1_ILOS_SHIFT          4

#define EEPROM_ICW2                     0xf
#define EEPROM_ICW2_PAUSE_BITS          0x3000
#define EEPROM_ICW2_ASM_DIR             0x2000
#define EEPROM_ICW2_SWDPIO_EXE_BITS     0xf0
#define EEPROM_ICW2_SWDPIO_EXE_SHIFT    4

#define BAR0_64_BIT                     0x04

/* TX Descriptor Command Fields */

#define TXD_CMD_EOP                     0x01
#define TXD_CMD_IFCS                    0x02
#define TXD_CMD_IC                      0x04
#define TXD_CMD_RS                      0x08
#define TXD_CMD_RPS                     0x10
#define TXD_CMD_DEXT                    0x20
#define TXD_CMD_VLE                     0x40
#define TXD_CMD_IDE                     0x80

/* for TX context descriptor only */

#define TXD_CMD_CTX_TCP                 0x01 /* 1 == TCP, 0 == UDP */
#define TXD_CMD_CTX_IP          0x02
#define TXD_CMD_CTX_TSE         0x04
#define TXD_CMD_CTX_RS          0x08
#define TXD_CMD_CTX_DEXT        0x20
#define TXD_CMD_CTX_IDE         0x80

/* TX Descriptor Status Fields */

#define TXD_STAT_DD                     0x01
#define TXD_STAT_EC                     0x02
#define TXD_STAT_LC                     0x04
#define TXD_STAT_TU                     0x08

/* TX Data Descriptor POPTS fields */

#define TXD_DATA_POPTS_IXSM_BIT         0x01
#define TXD_DATA_POPTS_TXSM_BIT         0x02

/* TX Data Descriptor DTYP Field */ 
#define TXD_DTYP_BIT                    0x01

/* RX Descriptor Status Field */

#define RXD_STAT_DD                     0x01
#define RXD_STAT_EOP                    0x02
#define RXD_STAT_IXSM                   0x04
#define RXD_STAT_VP                     0x08
#define RXD_STAT_RSV                    0x10
#define RXD_STAT_TCPCS                  0x20
#define RXD_STAT_IPCS                   0x40
#define RXD_STAT_PIF                    0x80

/* RX Descriptor Error Field */

#define RXD_ERROR_CE                    0x01        /* CRC or Align error */
#define RXD_ERROR_SE                    0x02        /* Symbol error */
#define RXD_ERROR_SEQ                   0x04        /* Sequence error */
#define RXD_ERROR_RSV                   0x08        /* reserved */
#define RXD_ERROR_CXE                   0x10        /* Carrier ext error */
#define RXD_ERROR_TCPE                  0x20        /* TCP/UDP CKSUM error */
#define RXD_ERROR_IPE                   0x40        /* IP CKSUM error */
#define RXD_ERROR_RXE                   0x80        /* RX data error */

/* Interrupt Register Fields */

#define INT_TXDW_BIT                    0x01        /* TX descriptor write-back */        
#define INT_TXQE_BIT                    0x02        /* TX ring empty */
#define INT_LSC_BIT                     0x04        /* link change interrupt */
#define INT_RXSEQ_BIT                   0x08        /* RX sequence error */
#define INT_RXDMT0_BIT                  0x10        /* RX descriptor Mini Threshold */
#define INT_RXO_BIT                     0x40        /* RX FIFO overrun */
#define INT_RXTO_BIT                    0x80        /* RX timer interrupt */
#define INT_MDAC_BIT                    0x200       /* MDIO complete interrupt */
#define INT_RXCFG_BIT                   0x400       /* Receiving /C/ ordered set */
#define INT_GPI0_BIT                    0x800       /* GPIO 0 interrupt (543 MAC) */
#define INT_GPI1_BIT                    0x1000      /* PHY (544 MAC) or GPIO1 (543) interrupt */ 
#define INT_GPI2_BIT                    0x2000      /* GPIO2 interrupt */
#define INT_GPI3_BIT                    0x4000      /* GPIO3 interrupt */
#define INT_TXDLOW_BIT                  0x8000      /* TX descriptor low threshold hit */

/* IMS register */ 

#define IMS_TXDW_BIT                    0x01
#define IMS_TXQE_BIT                    0x02
#define IMS_LSC_BIT                     0x04
#define IMS_RXSEQ_BIT                   0x08
#define IMS_RXDMT0_BIT                  0x10
#define IMS_RXO_BIT                     0x40
#define IMS_RXTO_BIT                    0x80
#define IMS_MDAC_BIT                    0x200
#define IMS_RXCFG_BIT                   0x400
#define IMS_TXDLOW_BIT                  0x8000

/* IMC register */

#define IMC_ALL_BITS                    0xffffffff
#define IMC_TXDW_BIT                    0x01
#define IMC_TXQE_BIT                    0x02
#define IMC_LSC_BIT                     0x04
#define IMC_RXSEQ_BIT                   0x08
#define IMC_RXDMT0_BIT                  0x10
#define IMC_RXO_BIT                     0x40
#define IMC_RXTO_BIT                    0x80
#define IMC_MDAC_BIT                    0x200
#define IMC_RXCFG_BIT                   0x400
#define IMC_TXDLOW_BIT                  0x8000

/* ICR register */

#define ICR_TXDW_BIT                    0x01
#define ICR_TXQE_BIT                    0x02
#define ICR_LSC_BIT                     0x04
#define ICR_RXSEQ_BIT                   0x08
#define ICR_RXDMT0_BIT                  0x10
#define ICR_RXO_BIT                     0x40
#define ICR_RXTO_BIT                    0x80
#define ICR_MDAC_BIT                    0x200
#define ICR_RXCFG_BIT                   0x400
#define ICR_TXDLOW_BIT                  0x8000

/* EEPROM Register Fields */

#define EECD_SK_BIT                     0x1
#define EECD_CS_BIT                     0x2
#define EECD_DI_BIT                     0x4
#define EECD_DO_BIT                     0x8
#define EECD_FWE_BIT			0x30
#define EECD_REQ_BIT                    0x40
#define EECD_GNT_BIT                    0x80
#define EECD_PRES_BIT                   0x100
#define EECD_SIZE_BIT                   0x200
#define EECD_AUTORD_DONE_BIT		0x200	/* EEPROM reload done */
#define EECD_EESIZE_BIT			0x7800
#define EECD_NVADDRS_BIT		0x18000
#define EECD_AUPDEN_BIT			0x100000
#define EECD_NVMTYPE_BIT		0x1800000

/* EEPROM SIZE */
#define EECD_EESIZE_128			0x0000
#define EECD_EESIZE_256			0x0800
#define EECD_EESIZE_512			0x1000
#define EECD_EESIZE_1K			0x1800
#define EECD_EESIZE_2K			0x2000
#define EECD_EESIZE_4K			0x2800
#define EECD_EESIZE_8K			0x3000
#define EECD_EESIZE_16K			0x3800
#define EECD_EESIZE_32K			0x4000
#define EECD_EESIZE_64K			0x4800

/* NVM address size */
#define EECD_NVADDRS_1			0x8000	/* eeprom, 1 address byte */
#define EECD_NVADDRS_2			0x10000 /* eeprom, 2 address bytes */
#define EECD_NVADDRS_3			0x18000 /* flash, 3 address bytes */

/* NVM type */
#define EECD_NVMTYPE_EEPROM		0x0000000
#define EECD_NVMTYPE_STANDALONE_FLASH	0x0800000
#define EECD_NVMTYPE_SHARED_FLASH	0x1000000
#define EECD_NVMTYPE_SIO		0x1800000

/* EERD register fields */

#define EERD_START_BIT			0x00000001
#define EERD_DONE_BIT			0x00000010
#define EERD_573_DONE_BIT		0x00000002
#define EERD_ADDR_BIT			0x0000FF00
#define EERD_573_ADDR_BIT		0x0000FFFC
#define EERD_DATA_BIT			0xFFFF0000

/* RAT field */

#define RAH_AV_BIT                      0x80000000

/* Control Register Field */

#define CTRL_FD_BIT                     0x1
#define CTRL_PRIOR_BIT                  0x4
#define CTRL_ASDE_BIT                   0x20
#define CTRL_SLU_BIT                    0x40
#define CTRL_ILOS_BIT                   0x80
#define CTRL_SPD_100_BIT                0x100
#define CTRL_SPD_1000_BIT               0x200
#define CTRL_FRCSPD_BIT                 0x800
#define CTRL_FRCDPX_BIT                 0x1000
#define CTRL_SWDPIN0_BIT                0x40000
#define CTRL_SWDPIN1_BIT                0x80000
#define CTRL_SWDPIN2_BIT                0x100000
#define CTRL_SWDPIN3_BIT                0x200000
#define CTRL_SWDPIO0_BIT                0x400000
#define CTRL_SWDPIO1_BIT                0x800000
#define CTRL_SWDPIO2_BIT                0x1000000
#define CTRL_SWDPIO3_BIT                0x2000000
#define CTRL_RST_BIT                    0x4000000
#define CTRL_RFCE_BIT                   0x8000000
#define CTRL_TFCE_BIT                   0x10000000
#define CTRL_PHY_RST_BIT                0x80000000
#define CTRL_MDC_BIT                    CTRL_SWDPIN3_BIT
#define CTRL_MDIO_BIT                   CTRL_SWDPIN2_BIT
#define CTRL_MDC_DIR_BIT                CTRL_SWDPIO3_BIT
#define CTRL_MDIO_DIR_BIT               CTRL_SWDPIO2_BIT
#define CTRL_SPEED_MASK                 0x300
#define CTRL_SWDPIOLO_SHIFT             22
#define CTRL_ILOS_SHIFT                 7

/* Receive Control Register Fields */

#define RCTL_MO_SHIFT                   12
#define RCTL_BSIZE_2048                 0
#define RCTL_BSIZE_4096                 0x00030000
#define RCTL_BSIZE_8192                 0x00020000
#define RCTL_BSIZE_16384                0x00010000
#define RCTL_EN_BIT                     0x00000002
#define RCTL_SBP_BIT                    0x00000004
#define RCTL_UPE_BIT                    0x00000008
#define RCTL_MPE_BIT                    0x00000010
#define RCTL_LPE_BIT                    0x00000020
#define RCTL_BAM_BIT                    0x00008000
#define RCTL_BSEX_BIT                   0x02000000

/* MDI register fields */

#define MDI_WRITE_BIT                   0x4000000
#define MDI_READ_BIT                    0x8000000
#define MDI_READY_BIT                   0x10000000
#define MDI_ERR_BIT                     0x40000000
#define MDI_REG_SHIFT                   16
#define MDI_PHY_SHIFT                   21

/* Flow Control Field and Initial value */

#define FCRTL_XONE_BIT                  0x80000000
#define FLOW_CONTROL_LOW_ADR            0x00c28001
#define FLOW_CONTROL_HIGH_ADR           0x00000100
#define FLOW_CONTROL_TYPE               0x8808
#define FLOW_CONTROL_TIMER_VALUE        0x100
#define FLOW_CONTROL_LOW_THRESH         0x4000
#define FLOW_CONTROL_HIGH_THRESH        0x8000

/* Extended Control Register Fields */

#define CTRL_EXT_SWDPIN4_BIT            0x10
#define CTRL_EXT_SWDPIN5_BIT            0x20
#define CTRL_EXT_SWDPIN6_BIT            0x40
#define CTRL_EXT_SWDPIN7_BIT            0x80
#define CTRL_EXT_SWDPIO4_BIT            0x100
#define CTRL_EXT_SWDPIO5_BIT            0x200
#define CTRL_EXT_SWDPIO6_BIT            0x400
#define CTRL_EXT_SWDPIO7_BIT            0x800
#define CTRL_EXT_ADSCHK_BIT		0x1000 /* initiate ADS sequence */
#define CTRL_EXT_EE_RST_BIT		0x2000 /* force EEPROM reload */
#define CTRL_EXT_IPS_BIT		0x4000 /* invert power state */
#define CTRL_EXT_SPD_BYPS_BIT		0x8000 /* speed select bypass */
#define CTRL_EXT_SWDPIOHI_SHIFT         8

#define CTRL_PHY_RESET_DIR4_BIT         CTRL_EXT_SWDPIO4_BIT
#define CTRL_PHY_RESET4_BIT             CTRL_EXT_SWDPIN4_BIT

/* Status Register Fields */

#define STATUS_FD_BIT                   0x1
#define STATUS_LU_BIT                   0x2
#define STATUS_TBIMODE_BIT              0x20
#define STATUS_SPEED_100_BIT            0x40
#define STATUS_SPEED_1000_BIT           0x80

/* TX control Register Fields */

#define TCTL_RST_BIT			0x1	/* software reset */
#define TCTL_EN_BIT                     0x2	/* TX enable */
#define TCTL_BCE_BIT			0x4	/* busy check enable */
#define TCTL_PSP_BIT                    0x8	/* pad short packets */
#define TCTL_SWXOFF_BIT			0x400000 /* SW Xoff transmission */
#define TCTL_PBE_BIT                    0x800000 /* packet burst enable */
#define TCTL_RTLC_BIT			0x1000000 /* retry on late collision */
#define TCTL_NRTU_BIT			0x2000000 /* no retransmit on uflow */
#define TCTL_MULR_BIT			0x10000000 /* multiuple request */
#define TCTL_COLD_BIT                   0x3ff000
#define TCTL_COLD_SHIFT                 12
#define TCLT_CT_SHIFT                   4

/* Extended configuration control bits */

#define EXTCNF_CTRL_PHY_WRITE_BIT	0x2 /* allow access to extended PHY conf */
#define EXTCNF_CTRL_DUD_EN_BIT		0x4 /* allow access to dock/undoc conf */
#define EXTCNF_CTRL_DOCK_OWNER_BIT	0x10 /* determine who loads dock config */
#define EXTCNF_CTRL_MDIO_SW_BIT		0x20 /* software owns MDIO */
#define EXTCNF_CTRL_MDIO_HW_BIT		0x40 /* hardware owns MDIO */

/* TIPG Register Fields */

#define TIPG_IPGR1_SHIFT                10
#define TIPG_IPGR2_SHIFT                20

/* RDTR Register Field */

#define RDTR_FPD_BIT                    0x80000000

/* TX Configuration Word fields */

#define TXCW_ANE_BIT                    0x80000000
#define TXCW_FD_BIT                     0x20
#define TXCW_ASM_DIR                    0x100
#define TXCW_PAUSE_BITS                 0x180

/* TX descriptor control */

#define TXDCTL_COUNT_DESC_BIT		0x00400000 /* enable counting of
						      descriptors still to be processed */

/* RXCSUM register field */

#define RXCSUM_IPOFL_BIT                0x0100
#define RXCSUM_TUOFL_BIT                0x0200

/* PHY's Registers And Initial Value*/

#define PHY_PREAMBLE                    0xFFFFFFFF
#define PHY_PREAMBLE_SIZE               32
#define PHY_WR_OP                       0x01
#define PHY_RD_OP                       0x02
#define PHY_TURNAR                      0x02
#define PHY_MARK                        0x01

/* Type define */

typedef struct devDrv_stat
    {
    UINT32 rxtxHandlerNum;      /* num of rxtx handle routine was called per sec */ 
    UINT32 rxIntCount;      /* num of rx interrupt per sec */
    UINT32 txIntCount;      /* num of tx interrupt per sec */
    UINT32 rxORunIntCount;  /* num of rx overrun interrupt per sec */
    UINT32 rxPacketNum;         /* num of rx packet per sec */
    UINT32 txPacketNum;         /* num of tx packet per sec */
    UINT32 rxPacketDrvErr;  /* num of pkt rx error on driver per sec */
    } DEVDRV_STAT;

typedef struct dev_Timer
    {
    UINT32 rdtrVal;             /* RDTR, unit of 1.024ns */
    UINT32 radvVal;             /* RADV, unit of 1.024us */
    UINT32 itrVal;              /* ITR,  unit of 256 ns  */
    UINT32 tidvVal;             /* TIDV, unit of 1.024us */
    UINT32 tadvVal;             /* TADV, unit of 1.024us */
    UINT32 watchDogIntVal;      /* interval of watchdog, unit of second */
    } DEV_TIMER;

struct adaptor_info 
    {
    UINT32 regBaseLow;      /* register PCI base address - low  */
    UINT32 regBaseHigh;     /* register PCI base address - high */
    UINT32 flashBase;       /* flash PCI base address */

    BOOL   adr64;           /* indictor of 64 bit address */
    UINT32 boardType;       /* board type */
    BOOL   dualPort;        /* indicates if device has two ports */
    UINT32 phyType;         /* PHY type (MII/GMII) */

    UINT32 delayUnit;       /* delay unit(in ns) for the delay function */
    FUNCPTR delayFunc;      /* BSP specified delay function */        

    void   (*phySpecInit)(PHY_INFO *, UINT8); /* vendor specified PHY's init */
    UINT32 (*sysLocalToBus)(int,UINT32);
    UINT32 (*sysBusToLocal)(int,UINT32);

    FUNCPTR intConnect;     /* interrupt connect function */ 
    FUNCPTR intDisConnect;  /* interrupt disconnect function */

    UINT16  eepromSize;
    UINT16  eeprom_icw1;    /* ICW1 in EEPROM */
    UINT16  eeprom_icw2;    /* ICW2 in EEPROM */

    UCHAR   enetAddr[6];    /* Ether address for this adaptor */ 
    UCHAR   reserved1[2];   /* reserved */

    FUNCPTR phyDelayRtn;    /* phy delay function */ 
    UINT32  phyMaxDelay;    /* max phy detection retry */
    UINT32  phyDelayParm;   /* delay parameter for phy delay function */
    UINT32  phyAddr;        /* phy Addr */

    BOOL   useShortCable;   /* TRUE if short cable used for 82544 */

    BOOL   (*sysGeiDynaTimerSetup)(struct adaptor_info *); /* adjust device's timer dynamically */
    BOOL   (*sysGeiInitTimerSetup)(struct adaptor_info *); /* set the device's timer initially */

    DEVDRV_STAT devDrvStat;       /* statistic data for devices */
    DEV_TIMER   devTimerUpdate;   /* timer register value for update */

    void   (*phyBspPreInit)(PHY_INFO *, UINT8); /* BSP specific PHY init for 82543-copper */
    };

typedef struct adaptor_info ADAPTOR_INFO;

/* structure for Statistic registers */

typedef struct sta_reg 
    {
    UINT32    crcerrs;       /* CRC error count */
    UINT32    algnerrc;      /* alignment err count */
    UINT32    symerrs;       /* symbol err count */
    UINT32    rxerrc;        /* rx err count */
    UINT32    mpc;           /* missed packet count */
    UINT32    scc;           /* single collision count */
    UINT32    ecol;          /* excessive collision count */
    UINT32    mcc;           /* multi collision count */
    UINT32    latecol;       /* later collision count */
    UINT32    colc;          /* collision count */
    UINT32    tuc;           /* tx underun count */
    UINT32    dc;            /* defer count */
    UINT32    tncrs;         /* tx - no crs count */
    UINT32    sec;           /* sequence err count */
    UINT32    cexteer;       /* carrier extension count */
    UINT32    rlec;          /* rx length error count */
    UINT32    xonrxc;        /* XON receive count */
    UINT32    xontxc;        /* XON transmit count */
    UINT32    xoffrxc;       /* XOFF receive count */
    UINT32    xofftxc;       /* XFF transmit count */
    UINT32    fcruc;         /* FC received unsupported count */
    UINT32    prc64;         /* packet rx (64 byte) count */
    UINT32    prc127;        /* packet rx (65 - 127 byte) count */
    UINT32    prc255;        /* packet rx (128 - 255 byte) count */
    UINT32    prc511;        /* packet rx (256 - 511 byte) count */
    UINT32    prc1023;       /* packet rx (512 - 1023 byte) count */
    UINT32    prc1522;       /* packet rx (1024 - 1522 byte) count */
    UINT32    gprc;          /* good packet received count */
    UINT32    bprc;          /* broadcast packet received count */
    UINT32    mprc;          /* Multicast packet received count */
    UINT32    gptc;          /* good packet transmit count */
    UINT32    gorl;          /* good octets receive count (low) */
    UINT32    gorh;          /* good octets received count (high) */
    UINT32    gotl;          /* good octets transmit count (lo) */
    UINT32    goth;          /* good octets transmit count (hi) */
    UINT32    rnbc;          /* receive no buffer count */
    UINT32    ruc;           /* receive undersize count */
    UINT32    rfc;           /* receive fragment count */
    UINT32    roc;           /* receive oversize count */
    UINT32    rjc;           /* receive Jabber count */
    UINT32    torl;          /* total octets received (lo) */
    UINT32    torh;          /* total octets received (hi) */
    UINT32    totl;          /* total octets transmit (lo) */
    UINT32    toth;          /* total octets transmit (hi) */
    UINT32    tpr;           /* total packet received */
    UINT32    tpt;           /* total packet transmit */
    UINT32    ptc64;         /* packet transmit (64 byte) count */
    UINT32    ptc127;        /* packet transmit (65-127 byte) count */
    UINT32    ptc255;        /* packet transmit (128-255 byte) count */  
    UINT32    ptc511;        /* packet transmit (256-511 byte) count */
    UINT32    ptc1023;       /* packet transmit (512-1023 byte) count */
    UINT32    ptc1522;       /* packet transmit (1024-1522 byte) count */
    UINT32    mptc;          /* Multicast packet transmit count */
    UINT32    bptc;          /* Broadcast packet transmit count */
    UINT32    tsctc;         /* TCP segmentation context tx count */
    UINT32    tsctfc;        /* TCP segmentation context tx fail count */      
    UINT32    rdfh;          /* rx data FIFO head */
    UINT32    rdft;          /* rx data FIFO tail */
    UINT32    rdfhs;         /* rx data FIFO head saved register */
    UINT32    rdfts;         /* rx data FIFO tail saved register */
    UINT32    rdfpc;         /* rx data FIFO packet count */
    UINT32    tdfh;          /* tx data FIFO head */
    UINT32    tdft;          /* tx data FIFO tail */
    UINT32    tdfhs;         /* tx data FIFO head saved register */
    UINT32    tdfts;         /* tx data FIFO tail saved register */
    UINT32    tdfpc;         /* tx data FIFO packet count */
    } STA_REG;

typedef struct gei_drv_ctrl
    {
    /* accessed by SL */
    HEND_DRV_CTRL hEnd;
    GEI_HEND_EVENT_REC  eventRec[GEI_NUM_EVENTS];
    HEND_ISR_REC isrRec[GEI_NUM_ISRS]; 
    int          numRxQueues;
    SL_LIST *    pRxQueueList;
    SL_LIST *    pTxQueueList;
    int          firstRxIndex;
    GEI_TX_BD *    pBdAlloc;       /* GEI BDs Alloc pointer */
    GEI_TX_BD *    pBdBase;        /* GEI BDs base */
    UINT32      bdSize;         /* GEI BDs size */
    UINT32      rxBufSize;      /* RX buffer size */
    CACHE_FUNCS  bufCacheFuncs;  /* cache descriptor */
    char *      pBufAlloc;      /* Allocated GEI memory pool base */
    char *      pBufBase;       /* Rounded GEI memory pool base */
    UINT32      bufSize;        /* GEI memory pool size */
    char       * pMBlkArea;      /* mBlock area pointer */
    UINT32       mBlkSize;       /* mBlocks area memory size */

    /* accessed by DL */
    UINT32      flags;          /* flags for device configuration */
    UINT32      usrFlags;       /* flags for user customization */ 
    ADAPTOR_INFO adaptor;       /* adaptor information */
    UINT16      devId;		/* PCI device ID */
    UINT32      devRegBase;     /* virtual base address for registers */
    int         mtu;            /* maximum transfer unit */
    int         txIntDelay;     /* delay time for TX interrupt */
    int         rxIntDelay;     /* delay time for RX interrupt */
    BOOL        dmaPriority;    /* indicator for TX/RX DMA prefer */
    UINT32      multiCastFilterType; /* indicator of multicast table type */ 
    int         flowCtrl;       /* flow control setting */
    STATUS      linkInitStatus; /* the status of first link */
    volatile UINT32 linkStatus; /* indicator for link status */
    BOOL        linkMethod;     /* indicator for link approaches */
    UINT32      txConfigureWord; /* copy of TX configuration word register */
    UINT32      devCtrlRegVal;  /* control register value */
    UINT32      speed;          /* device speed */
    UINT32      duplex;         /* device duplex mode */
    UINT32      offset;         /* offset for IP header */
    UINT32      cableType;      /* cable type (Fiber or Copper) */
    PHY_INFO *  pPhyInfo;       /* pointer to phyInfo structure */
    UINT32      phyInitFlags;   /* Initial PHY flags for phyInfo */
    volatile BOOL devStartFlag;  /* indicator for device start */
#ifdef INCLUDE_TBI_COMPATIBLE
    BOOL        tbiCompatibility; /* TBI compatibility for HW bug */
#endif
    STA_REG     staRegs;         /* statistic register structure */
    END_CAPABILITIES hwCaps;     /* interface hardware offload capabilities */
    UINT32      lastCsumCtx;
    UINT32  lastIpHdrLen;
    FUNCPTR     *pFlushRtn;
    FUNCPTR     *pInvalRtn;
    } GEI_DRV_CTRL;

/* register access */

#define GEI_VOLATILE_WRITE32(base, offset, value)		\
    vxbWrite32 (pDrvCtrl->hEnd.handle,				\
		(UINT32 *)((char *)base + offset), value)

#define GEI_VOLATILE_WRITE_REG(offset, value)			\
        GEI_VOLATILE_WRITE32(pDrvCtrl->devRegBase, offset, value)

#define GEI_READ32(base, offset, result)			\
    result = vxbRead32 (pDrvCtrl->hEnd.handle,			\
			(UINT32 *)((char *)base + offset))

#define GEI_WRITE32(base, offset, value)			\
        GEI_VOLATILE_WRITE32(pDrvCtrl->devRegBase, offset, value)

#ifdef GEI_REGISTER_READ32
#define GEI_READ_REG(offset, result)				\
        GEI_REGISTER_READ32(pDrvCtrl->hEnd.pInst, pDrvCtrl->devRegBase, \
			    offset, result)
#else
#define GEI_READ_REG(offset, result)				\
        GEI_READ32(pDrvCtrl->devRegBase, offset, result)
#endif

#ifdef GEI_REGISTER_WRITE32
#define GEI_WRITE_REG(offset, value)				\
        GEI_REGISTER_WRITE32(pDrvCtrl->hEnd.pInst, pDrvCtrl->devRegBase, \
			     offset, value)
#else
#define GEI_WRITE_REG(offset, value)				\
        GEI_WRITE32(pDrvCtrl->devRegBase, offset, value)
#endif


/* descriptor access */

#define GEI_WRITE_DESC_BYTE(pDesc, offset, value)      \
        (*(UINT8 *)((UINT32)pDesc + offset) = (UINT8) (value & 0xff))

#define GEI_READ_DESC_BYTE(pDesc, offset)              \
        ((UINT8)( *(UINT8 *)((UINT32)pDesc + offset)) & 0xff)

#if (_BYTE_ORDER == _BIG_ENDIAN)
#define GEI_WRITE_DESC_WORD(pDesc, offset, value)      \
        (*(UINT16 *)((UINT32)pDesc + offset) =    \
        (MSB(value) | LSB(value)<<8) & 0xffff)

#define GEI_WRITE_DESC_LONG(pDesc, offset, value)      \
        (*(UINT32 *)((UINT32)pDesc + offset) =    \
        (UINT32) LONGSWAP(value))

#define GEI_READ_DESC_WORD(pDesc, offset, result)             \
        do {                                                  \
           UINT16 temp;                                       \
           temp = *(UINT16 *)((UINT32)pDesc + offset);        \
           result = (MSB(temp) | (LSB(temp) << 8)) & 0xffff;  \
           } while ((0))

#define GEI_READ_DESC_LONG(pDesc, offset, result)              \
        do {                         \
           UINT32 temp;              \
           temp = *(UINT32 *)((UINT32)pDesc + offset);   \
           result = LONGSWAP(temp); /* swap the data */  \
           } while ((0))

#else /* (_BYTE_ORDER == _BIG_ENDIAN) */

#define GEI_WRITE_DESC_WORD(pDesc, offset, value)      \
        (*(UINT16 *)((UINT32)pDesc + offset) = (UINT16)(value & 0xffff))

#define GEI_WRITE_DESC_LONG(pDesc, offset, value)      \
        (*(UINT32 *)((UINT32)pDesc + offset) = (UINT32)value)

#define GEI_READ_DESC_WORD(pDesc, offset, result)              \
        result = ((UINT16)(*(UINT16 *)((UINT32)pDesc + offset)) & 0xffff)

#define GEI_READ_DESC_LONG(pDesc, offset, result)              \
        result = ((UINT32)( *(UINT32 *)((UINT32)pDesc + offset)))

#endif /* (_BYTE_ORDER == _BIG_ENDIAN) */

/* this macro should set the chip to ALLOW RX interrupts, despite confusing
 * name (this is used in hEnd).  Argument 'p' is the bus device id.  It is
 * assumed that the variable pDrvCtrl will be available for the write reg
 * macro (this may need to be changed).
 */

#define GEI_RX_INT_ENABLE(pDev) \
    do { \
        UINT32 temp; \
        GEI_WRITE_REG(INTEL_82543GC_IMS, AVAIL_RX_INT); \
        CACHE_PIPE_FLUSH(); \
	GEI_READ_REG(INTEL_82543GC_IMS, temp); \
    } while ((0))

#define GEI_TX_INT_ENABLE(pDev) \
    do { \
        UINT32 temp; \
        GEI_WRITE_REG(INTEL_82543GC_IMS, AVAIL_TX_INT); \
        CACHE_PIPE_FLUSH(); \
	GEI_READ_REG(INTEL_82543GC_IMS, temp); \
    } while ((0))

#define GEI_GET_EVENT_REC(x) ((GEI_HEND_EVENT_REC *)(x))

#define GEI_RX_EVENT_CLEAR(p)

#define GEI_TX_STALL_ENTER(pDev)

#define GEI_TX_STALL_EXIT(pDev)

#define GEI_GET_DRV_CTRL(x) \
((int)(x) - (offsetof (struct gei_drv_ctrl,eventRec[(((HEND_EVENT_REC *)(x))->index)])))

#define GEI_TX_CAPACITY(x) (x)

    
/* MIB macros */
#define GEI_HEND_M2_INIT(p)  \
    {                        \
    endM2Init(&p->hEnd.endObj, M2_ifType_ethernet_csmacd, \
              (u_char *) &p->hEnd.enetAddr, 6, p->mtu, \
              END_SPEED_1000M, \
              IFF_NOTRAILERS | IFF_MULTICAST | IFF_BROADCAST); \
    }
#define GEI_HEND_M2_RX_PKT(x,y,z)   
#define GEI_HEND_M2_PKT(p,m,z)

IMPORT STATUS cacheInvalidate (CACHE_TYPE, void *, size_t);
IMPORT STATUS cacheFlush (CACHE_TYPE, void *, size_t);
IMPORT int    intEnable (int);
IMPORT int    intDisable (int);

IMPORT struct vxbPciID geiPciDevVendList[]; 
/* INTEL 82544 INTERNAL PHY */

#define INTEL_82544PHY_OUI_ID                   (0x5043)
#define INTEL_82544PHY_MODEL                    (0x3)

#define INTEL_82544PHY_PHY_SPEC_CTRL_REG        (0x10)
#define INTEL_82544PHY_PHY_SPEC_STAT_REG        (0x11)
#define INTEL_82544PHY_INT_ENABLE_REG           (0x12)
#define INTEL_82544PHY_INT_STATUS_REG           (0x13)
#define INTEL_82544PHY_EXT_PHY_SPEC_CTRL_REG    (0x14)
#define INTEL_82544PHY_RX_ERROR_COUNTER         (0x15)
#define INTEL_82544PHY_PHY_GLOBAL_STAT          (0x17)
#define INTEL_82544PHY_LED_CTRL_REG             (0x18)

#define INTEL_82544PHY_PSCR_ASSERT_CRS_ON_TX    (0x0800)
#define INTEL_82544PHY_EPSCR_TX_CLK_25          (0x0070)

/* Alaska PHY's information */

#define MARVELL_OUI_ID                  (0x5043)
#define MARVELL_ALASKA_88E1000          (0x5)
#define MARVELL_ALASKA_88E1000S         (0x4)
#define ALASKA_PHY_SPEC_CTRL_REG        (0x10)
#define ALASKA_PHY_SPEC_STAT_REG        (0x11)
#define ALASKA_INT_ENABLE_REG           (0x12)
#define ALASKA_INT_STATUS_REG           (0x13)
#define ALASKA_EXT_PHY_SPEC_CTRL_REG    (0x14)
#define ALASKA_RX_ERROR_COUNTER         (0x15)
#define ALASKA_LED_CTRL_REG             (0x18)

#define ALASKA_PSCR_ASSERT_CRS_ON_TX    (0x0800)
#define ALASKA_EPSCR_TX_CLK_25          (0x0070)

#define ALASKA_PSCR_AUTO_X_1000T        (0x0040)
#define ALASKA_PSCR_AUTO_X_MODE         (0x0060)

#define ALASKA_PSSR_DPLX                (0x2000)
#define ALASKA_PSSR_SPEED               (0xC000)
#define ALASKA_PSSR_10MBS               (0x0000)
#define ALASKA_PSSR_100MBS              (0x4000)
#define ALASKA_PSSR_1000MBS             (0x8000)

#ifdef __cplusplus
}
#endif

#endif /* __INCgeiEndh */

