/* motTsecHEnd.h - Motorola TSEC Ethernet network interface.*/

/*
 * Copyright (c) 2005-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01l,16apr08,h_k  converted to new access method.
01k,08dec06,wap  Add jumbo frame support
01k,12sep06,dlk  Remove MOT_TSEC_RX_QUEUE support.
01j,17jul06,rcs  Added Modular hEnd includes
01i,10may06,rcs  Fixed CQ 39353 
01h,09mar06,wap  Allow up to 4 units (SPR #118829)
01g,07mar06,dlk  Disable interrupt coalescing by default (SPR #118624)
01f,02feb06,wap  Sync with latest hEnd API changes
01e,17nov05,wap  Overhaul for VxBus compliance, miiBus support, performance
01d,26sep05,dlk  Remove extra MOT_TSEC_DRV_LOGMSG definition.
01c,14sep05,rcs  Added MOT_TSEC_TX_IMASK_SET
01b,22aug05,rcs  converted macros to access method format
01a,29jul05,rcs  Initial checkin of file
*/


#ifndef __INCtsecEndh
#define __INCtsecEndh

/* includes */

#ifdef __cplusplus
extern "C" {
#endif

#include <net/ethernet.h>
#include <etherLib.h>
#include <miiLib.h>
#include "hEnd.h"

IMPORT void motTsecHEndRegister (void);

#ifndef M8260ABBREVIATIONS
#define M8260ABBREVIATIONS

#ifdef  _ASMLANGUAGE
#define CAST(x)
#else /* _ASMLANGUAGE */
typedef volatile UCHAR VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32 VINT32; /* volatile unsigned word */
typedef volatile INT16 VINT16; /* volatile unsigned halfword */
typedef volatile INT8 VINT8;   /* volatile unsigned byte */
typedef volatile UINT32 VUINT32; /* volatile unsigned word */
typedef volatile UINT16 VUINT16; /* volatile unsigned halfword */
typedef volatile UINT8 VUINT8;   /* volatile unsigned byte */
#define CAST(x) (x)
#endif  /* _ASMLANGUAGE */

#endif /* M8260ABBREVIATIONS */

#ifndef TEST_TOKEN_MERGE
    #define TEST_TOKEN_MERGE
    
    /* __(i) used for function names and variables */
    #define __(i)   motTsec ## i
    
    /* _c_(i)used for macros  and typedefs */
    #define _c_(i)  MOT_TSEC_ ## i
    
    /* _m_ used for structure name - needed for offsetof */
    #define _m_(i)  mot_tsec_ ## i
    
    /* _o_(i) used for device name string */
    #define _o_(i)  mottsec ## i
    
    /* _p_(i) used for calls to configuration stub */
    #define _p_(i)  sysMotTsec ## i
#else
    #undef __(i)
        #define __(i)    motTsec ## i
    #undef _c_(i)
        #define _c_(i)   MOT_TSEC_ ## i
    #undef _m_(i)
        #define _m_(i)   mot_tsec_ ## i
    #undef _o_(i)
        #define _o_(i)   mottsec ## i  
    #undef _p_(i)
        #define _p_(i)   sysMotTsec ## i  
#endif

#define HEND_RX_DMA
#define HEND_TX_DMA
#define HEND_INIT_TUPLES

#define MOT_TSEC_DEVICE_ON_NEXUS	TRUE
#define MOT_TSEC_HAS_MII_BUS TRUE
#define MOT_TSEC_NUM_EVENTS 3
#define MOT_TSEC_NUM_ISRS 3
#define MOT_TSEC_NUM_RX_QUEUES 1
#define MOT_TSEC_NUM_TX_QUEUES 1
#define MOT_TSEC_RX_EVENT_ID 0
#define MOT_TSEC_TX_EVENT_ID 1
#define MOT_TSEC_RX_POLL 0
#define MOT_TSEC_TX_PKT_COMP_EVENT_ID 1
#define MOT_TSEC_HEND_TX_PAD 0
#define MOT_TSEC_FRAG_NUM_LIMIT	16 
#define MOT_TSEC_TX_INT_HANDLE 0
#define MOT_TSEC_RX_INT_HANDLE 1
#define MOT_TSEC_ERR_INT_HANDLE 2
#define MOT_TSEC_TX_DESC_CAPACITY 1
#define MOT_TSEC_TX_DESC_SPAN 1
#define MOT_TSEC_ALIGNMENT_OFFSET 0

#ifndef MAC_ADDR_LEN
#define MAC_ADDR_LEN 6
#endif

/* enum declarations for argument passing */
enum motTsecParams {MOT_TSEC_UNIT_TYPE                  = 0x10,
                    MOT_TSEC_CCSBAR_TYPE                = 0x11,
                    MOT_TSEC_TSEC_NUM_TYPE              = 0x12,
                    MOT_TSEC_MAC_ADDR_TYPE              = 0x13,
                    MOT_TSEC_USR_FLAGS_TYPE             = 0x14,
                    MOT_TSEC_PHY_PARAM_TYPE             = 0x15,
                    MOT_TSEC_FUNC_TABLE_PARAM_TYPE      = 0x16,
                    MOT_TSEC_CONFIG_PARAMS_TYPE         = 0x17};


#define MOT_TSEC_CACHE_INVAL(address, len)                              \
    CACHE_DRV_INVALIDATE (&pDrvCtrl->bufCacheFuncs, (address), (len));

#define MOT_TSEC_CACHE_FLUSH(address, len)                               \
    CACHE_DRV_FLUSH (&pDrvCtrl->bufCacheFuncs, (address), (len));     


/* Debug Macros */
#define MOT_TSEC_DRV_DEBUG     0x0000
#define MOT_TSEC_DRV_DEBUG_RX         0x0001
#define MOT_TSEC_DRV_DEBUG_TX         0x0002
#define MOT_TSEC_DRV_DEBUG_POLL       0x0004
#define MOT_TSEC_DRV_DEBUG_MII        0x0008
#define MOT_TSEC_DRV_DEBUG_LOAD       0x0010
#define MOT_TSEC_DRV_DEBUG_IOCTL      0x0020
#define MOT_TSEC_DRV_DEBUG_INT        0x0040
#define MOT_TSEC_DRV_DEBUG_START      0x0080
#define MOT_TSEC_DRV_DEBUG_INT_RX_ERR 0x0100
#define MOT_TSEC_DRV_DEBUG_INT_TX_ERR 0x0200
#define MOT_TSEC_DRV_DEBUG_RX_ERR     0x0400
#define MOT_TSEC_DRV_DEBUG_TX_ERR     0x0800
#define MOT_TSEC_DRV_DEBUG_TRACE      0x1000
#define MOT_TSEC_DRV_DEBUG_TRACE_RX   0x2000
#define MOT_TSEC_DRV_DEBUG_TRACE_TX   0x4000
#define MOT_TSEC_DRV_DEBUG_MONITOR    0x8000
#define MOT_TSEC_DRV_DEBUG_ANY        0xffff
#define MOT_TSEC_DRV_DEBUG_REG      0x10000


#ifdef MOT_TSEC_DEBUG 

UINT32 tsecEndDbg = (MOT_TSEC_DRV_DEBUG_REG);

#define MOT_TSEC_DRV_LOGMSG(FLG, X0, X1, X2, X3, X4, X5, X6)    \
    {                                                           \
    if (tsecEndDbg & FLG)                                       \
        logMsg (X0, X1, X2, X3, X4, X5, X6);                    \
    }
#else /* MOT_TSEC_DEBUG */
#define MOT_TSEC_DRV_LOGMSG(FLG, X0, X1, X2, X3, X4, X5, X6)
#endif 

#define MOT_TSEC_DEV_NAME        "mottsec"
#define MOT_TSEC_DEV_NAME_LEN    8
#define MOT_TSEC_MAX_DEVS        4
#define MOT_TSEC_DEV_1           0
#define MOT_TSEC_DEV_2           1
#define MOT_TSEC_ADRS_1  0x00024000
#define MOT_TSEC_ADRS_2  0x00025000
#define MOT_TSEC_REG_CNT 25 

/* Register offsets */

#define MOT_TSEC_IEVENT				0x010
#define MOT_TSEC_IMASK				0x014
#define MOT_TSEC_EDIS				0x018
#define MOT_TSEC_ECNTRL				0x020
#define MOT_TSEC_MINFLR				0x024
#define MOT_TSEC_PTV				0x028
#define MOT_TSEC_DMACTRL			0x02c
#define MOT_TSEC_TBIPA				0x030
#define MOT_TSEC_FIFO_PAUSE_CTRL		0x04c
#define MOT_TSEC_FIFO_TX_THR			0x08c
#define MOT_TSEC_FIFO_STRV			0x098
#define MOT_TSEC_FIFO_STRV_SHUT			0x09c
#define MOT_TSEC_TCTRL				0x100
#define MOT_TSEC_TSTAT				0x104
#define MOT_TSEC_TBDLEN				0x10c
#define MOT_TSEC_TXIC				0x110
#define MOT_TSEC_CTBPTR				0x124
#define MOT_TSEC_TBPTR				0x184
#define MOT_TSEC_TBASE				0x204
#define MOT_TSEC_OSTBD				0x2b0
#define MOT_TSEC_OSTBDP				0x2b4
#define MOT_TSEC_RCTRL				0x300
#define MOT_TSEC_RSTAT				0x304
#define MOT_TSEC_RBDLEN				0x30c
#define MOT_TSEC_RXIC				0x310
#define MOT_TSEC_CRBPTR				0x324
#define MOT_TSEC_MRBLR				0x340
#define MOT_TSEC_RBPTR				0x384
#define MOT_TSEC_RBASE				0x404
#define MOT_TSEC_MACCFG1			0x500
#define MOT_TSEC_MACCFG2			0x504
#define MOT_TSEC_IPGIFG				0x508
#define MOT_TSEC_HAFDUP				0x50c
#define MOT_TSEC_MAXFRM				0x510
#define MOT_TSEC_MIIMCFG			0x520
#define MOT_TSEC_MIIMCOM			0x524
#define MOT_TSEC_MIIMADD			0x528
#define MOT_TSEC_MIIMCON			0x52c
#define MOT_TSEC_MIIMSTAT			0x530
#define MOT_TSEC_MIIMIND			0x534
#define MOT_TSEC_IFSTAT				0x53c
#define MOT_TSEC_MACSTNADDR1			0x540
#define MOT_TSEC_MACSTNADDR2			0x544
#define MOT_TSEC_TR64				0x680
#define MOT_TSEC_TR127				0x684
#define MOT_TSEC_TR255				0x688
#define MOT_TSEC_TR511				0x68c
#define MOT_TSEC_TR1K				0x690
#define MOT_TSEC_TRMAX				0x694
#define MOT_TSEC_TRMGV				0x698
#define MOT_TSEC_RBYT				0x69c
#define MOT_TSEC_RPKT				0x6a0
#define MOT_TSEC_RFCS				0x6a4
#define MOT_TSEC_RMCA				0x6a8
#define MOT_TSEC_RBCA				0x6ac
#define MOT_TSEC_RXCF				0x6b0
#define MOT_TSEC_RXPF				0x6b4
#define MOT_TSEC_RXUO				0x6b8
#define MOT_TSEC_RALN				0x6bc
#define MOT_TSEC_RFLR				0x6c0
#define MOT_TSEC_RCDE				0x6c4
#define MOT_TSEC_RCSE				0x6c8
#define MOT_TSEC_RUND				0x6cc
#define MOT_TSEC_ROVR				0x6d0
#define MOT_TSEC_RFRG				0x6d4
#define MOT_TSEC_RJBR				0x6d8
#define MOT_TSEC_RDRP				0x6dc
#define MOT_TSEC_TBYT				0x6e0
#define MOT_TSEC_TPKT				0x6e4
#define MOT_TSEC_TMCA				0x6e8
#define MOT_TSEC_TBCA				0x6ec
#define MOT_TSEC_TXPF				0x6f0
#define MOT_TSEC_TDFR				0x6f4
#define MOT_TSEC_TEDF				0x6f8
#define MOT_TSEC_TSCL				0x6fc
#define MOT_TSEC_TMCL				0x700
#define MOT_TSEC_TLCL				0x704
#define MOT_TSEC_TXCL				0x708
#define MOT_TSEC_TNCL				0x70c
#define MOT_TSEC_TDRP				0x714
#define MOT_TSEC_TJBR				0x718
#define MOT_TSEC_TFCS				0x71c
#define MOT_TSEC_TXCF				0x720
#define MOT_TSEC_TOVR				0x724
#define MOT_TSEC_TUND				0x728
#define MOT_TSEC_TFRG				0x72c
#define MOT_TSEC_CAR1				0x730
#define MOT_TSEC_CAR2				0x734
#define MOT_TSEC_CAM1				0x738
#define MOT_TSEC_CAM2				0x73c
#define MOT_TSEC_IADDR0				0x800
#define MOT_TSEC_IADDR1				0x804
#define MOT_TSEC_IADDR2				0x808
#define MOT_TSEC_IADDR3				0x80c
#define MOT_TSEC_IADDR4				0x810
#define MOT_TSEC_IADDR5				0x814
#define MOT_TSEC_IADDR6				0x818
#define MOT_TSEC_IADDR7				0x81c
#define MOT_TSEC_GADDR0				0x880
#define MOT_TSEC_GADDR1				0x884
#define MOT_TSEC_GADDR2				0x888
#define MOT_TSEC_GADDR3				0x88c
#define MOT_TSEC_GADDR4				0x890
#define MOT_TSEC_GADDR5				0x894
#define MOT_TSEC_GADDR6				0x898
#define MOT_TSEC_GADDR7				0x89c
#define MOT_TSEC_ATTR				0xbf8
#define MOT_TSEC_ATTRELI			0xbfc

/*
 * vxBus access macros
 * Note that ideally, we should use the vxbAccess macros hung off
 * the VXB_DEVICE, but that means incurring an extra function pointer
 * call for each register access. We take a shortcut, based on the
 * fact that we know the TSEC is a local bus device and that it
 * doesn't require any byte swapping.
 *
 * Note the use of the volatile keyword here. This is required or else
 * the compiler might optimize some of the register accesses away. The
 * Diab compiler doesn't seem to do this, but the GNU compiler will
 * produce non-functional code if the volatile keyword is removed.
 * Caveat utilitor.
 */

#ifdef MOT_TSEC_VXB_ACCESS
#include <../src/hwif/h/vxbus/vxbAccess.h>

#define MOT_TSEC_HANDLE(p) ((MOT_TSEC_DRV_CTRL *)(p)->pDrvCtrl)->hEnd.handle

#define MOT_TSEC_READ(pDev, addr)					\
    vxbRead32 (MOT_TSEC_HANDLE(pDev),					\
		(UINT32  *)((char *)pDev->pRegBase[0] + addr))

#define MOT_TSEC_WRITE(pDev, addr, data)
    vxbWrite32 (MOT_TSEC_HANDLE(pDev),					\
		(UINT32  *)((char *)pDev->pRegBase[0] + addr), data)

#else

#define MOT_TSEC_READ(pDev, offset)		\
	*(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + offset)

#define MOT_TSEC_WRITE(pDev, offset, val)				\
    do {                                                        	\
	    volatile UINT32 *pReg;					\
            pReg = (UINT32 *)((UINT32)pDev->pRegBase[0] + offset);	\
	    *(pReg) = (UINT32)(val);					\
    } while ((0))

#endif


#define TSEC_SETBIT(pDev, offset, val)		\
	MOT_TSEC_WRITE(pDev, offset, MOT_TSEC_READ(pDev, offset) | (val))

#define TSEC_CLRBIT(pDev, offset, val)		\
	MOT_TSEC_WRITE(pDev, offset, MOT_TSEC_READ(pDev, offset) & ~(val))

/* IEVENT and IMASK Register definitions */
#define MOT_TSEC_IEVENT_BABR    0x80000000
#define MOT_TSEC_IEVENT_RXC     0x40000000
#define MOT_TSEC_IEVENT_BSY     0x20000000
#define MOT_TSEC_IEVENT_EBERR   0x10000000
#define MOT_TSEC_IEVENT_MSRO    0x04000000
#define MOT_TSEC_IEVENT_GTSC    0x02000000
#define MOT_TSEC_IEVENT_BABT    0x01000000
#define MOT_TSEC_IEVENT_TXC     0x00800000
#define MOT_TSEC_IEVENT_TXE     0x00400000
#define MOT_TSEC_IEVENT_TXB     0x00200000
#define MOT_TSEC_IEVENT_TXF     0x00100000
#define MOT_TSEC_IEVENT_LC      0x00040000
#define MOT_TSEC_IEVENT_CRL     0x00020000
#define MOT_TSEC_IEVENT_XFUN    0x00010000
#define MOT_TSEC_IEVENT_RXB0    0x00008000
#define MOT_TSEC_IEVENT_MMRD	0x00000400
#define MOT_TSEC_IEVENT_MMWR	0x00000200
#define MOT_TSEC_IEVENT_GRSC    0x00000100
#define MOT_TSEC_IEVENT_RXF0    0x00000080

#define MOT_TSEC_IEVENTS_ALL   (MOT_TSEC_IEVENT_BABR | MOT_TSEC_IEVENT_RXC   | \
							MOT_TSEC_IEVENT_BSY  | MOT_TSEC_IEVENT_EBERR | \
							MOT_TSEC_IEVENT_MSRO | MOT_TSEC_IEVENT_GTSC  | \
							MOT_TSEC_IEVENT_BABT | MOT_TSEC_IEVENT_TXC   | \
							MOT_TSEC_IEVENT_TXE  | MOT_TSEC_IEVENT_TXB   | \
							MOT_TSEC_IEVENT_TXF  | MOT_TSEC_IEVENT_LC    | \
							MOT_TSEC_IEVENT_CRL  | MOT_TSEC_IEVENT_XFUN  | \
							MOT_TSEC_IEVENT_RXB0 | MOT_TSEC_IEVENT_GRSC  | \
							MOT_TSEC_IEVENT_RXF0)   

/* Error Disable Registers */
#define MOT_TSEC_EDIS_BSYDIS    0x20000000
#define MOT_TSEC_EDIS_EBERRDIS  0x10000000
#define MOT_TSEC_EDIS_TXEDIS    0x00400000
#define MOT_TSEC_EDIS_LCDIS     0x00040000
#define MOT_TSEC_EDIS_CRLDIS    0x00020000
#define MOT_TSEC_EDIS_XFUNDIS   0x00010000

/* Ethernet Control Register */
#define MOT_TSEC_ECNTRL_CLRCNT  0x00004000
#define MOT_TSEC_ECNTRL_AUTOZ   0x00002000
#define MOT_TSEC_ECNTRL_STEN    0x00001000
#define MOT_TSEC_ECNTRL_TBIM    0x00000020
#define MOT_TSEC_ECNTRL_RPM     0x00000010
#define MOT_TSEC_ECNTRL_R100M   0x00000008

/* Minimum Frame Register Length */
#define MOT_TSEC_MINFLR_MASK(l) (l&0x0000007f)

/* PTV Register Definition */
#define MOT_TSEC_PTV_PTE(t)     (t<<16)
#define MOT_TSEC_PTV_PT(t)      (t)

/* DMA Control Register */
#define MOT_TSEC_DMACTRL_TDSEN  0x00000080
#define MOT_TSEC_DMACTRL_TBDSEN 0x00000040
#define MOT_TSEC_DMACTRL_GRS    0x00000010
#define MOT_TSEC_DMACTRL_GTS    0x00000008
#define MOT_TSEC_DMACTRL_TOD    0x00000004
#define MOT_TSEC_DMACTRL_WWR    0x00000002
#define MOT_TSEC_DMACTRL_WOP    0x00000001

/* TBI Physical Address Registers */
#define MOT_TSEC_TBIPA_MASK(a)  (a&0x0000001f)

/* FIFO Transmit Threshold Registers */
#define MOT_TSEC_FIFO_TX_THR_MASK(a)     (a&0x000001ff)

/* FIFO Transmit Starve Registers */
#define MOT_TSEC_FIFO_TX_STARVE(a)  (a&0x000001ff)

/* FIFO Transmit Starve Shutoff Registers */
#define MOT_TSEC_FIFO_TX_STARVE_SHUTOFF(a) (a&0x000001ff)

/* Transmit Control Register */
#define MOT_TSEC_TCTRL_THDF         0x00000800
#define MOT_TSEC_TCTRL_RFC_PAUSE    0x00000010
#define MOT_TSEC_TCTRL_TFC_PAUSE    0x00000008

#define MOT_TSEC_TSTAT_THLT     0x80000000

/* Transmit Interrupt Coalescing */ 
#define MOT_TSEC_IC_ICEN	0x80000000
#define MOT_TSEC_IC_ICFCT(a)  ((a&0x000000ff)<<21)
#define MOT_TSEC_IC_ICTT(a)   (a&0x0000ffff)

#define MOT_TSEC_OSTBD_R        0x80000000
#define MOT_TSEC_OSTBD_PAD      0x40000000
#define MOT_TSEC_OSTBD_W        0x20000000
#define MOT_TSEC_OSTBD_I        0x10000000
#define MOT_TSEC_OSTBD_L        0x08000000
#define MOT_TSEC_OSTBD_TC       0x04000000
#define MOT_TSEC_OSTBD_DEF      0x02000000
#define MOT_TSEC_OSTBD_LC       0x00800000
#define MOT_TSEC_OSTBD_RL       0x00400000
#define MOT_TSEC_OSTBD_RC(r)    ((r&0x0000003c)>>18)
#define MOT_TSEC_OSTBD_UN       0x00020000
#define MOT_TSEC_OSTBD_LEN(l)   (l&0x0000ffff)

#define MOT_TSEC_RCTRL_BC_REJ   0x00000010
#define MOT_TSEC_RCTRL_PROM     0x00000008
#define MOT_TSEC_RCTRL_RSF      0x00000004

#define MOT_TSEC_RSTAT_QHLT     0x00800000
#define MOT_TSEC_TDLEN_MASK(l)       (l&0x0000ffff)
#define MOT_TSEC_RBDLEN_MASK(l)      (l&0x0000ffff)
#define MOT_TSEC_CRBPTR_MASK(p)      (p&0xfffffff8)
#define MOT_TSEC_MRBLR_MASK(l)       (l&0x0000ffc0)
#define MOT_TSEC_RBASE_MASK(p)       (p&0xfffffff8)

#define MOT_TSEC_MACCFG1_SOFT_RESET     0x80000000
#define MOT_TSEC_MACCFG1_RESET_RX_MC    0x00080000
#define MOT_TSEC_MACCFG1_RESET_TX_MC    0x00040000
#define MOT_TSEC_MACCFG1_RESET_RX_FUN   0x00020000
#define MOT_TSEC_MACCFG1_RESET_TX_FUN   0x00010000
#define MOT_TSEC_MACCFG1_LOOPBACK       0x00000100
#define MOT_TSEC_MACCFG1_RX_FLOW        0x00000020
#define MOT_TSEC_MACCFG1_TX_FLOW        0x00000010
#define MOT_TSEC_MACCFG1_SYNCD_RX_EN    0x00000008
#define MOT_TSEC_MACCFG1_RX_EN          0x00000004
#define MOT_TSEC_MACCFG1_SYNCD_TX_EN    0x00000002
#define MOT_TSEC_MACCFG1_TX_EN          0x00000001

#define MOT_TSEC_MACCFG2_PRE_LEN(l)         ((l<<12) & 0xf000)
#define MOT_TSEC_MACCFG2_PRE_LEN_GET(r)     ((r&0xf000)>>12)
#define MOT_TSEC_MACCFG2_IF_MODE(m)         ((m<<8) & 0x0300)
#define MOT_TSEC_MACCFG2_IF_MODE_GET(r)     ((r&0x0300)>>8)

#define MOT_TSEC_MACCFG2_IF_MODE_MASK       0x00000003
#define MOT_TSEC_MACCFG2_IF_MODE_MII        0x00000001
#define MOT_TSEC_MACCFG2_IF_MODE_GMII_TBI   0x00000002
#define MOT_TSEC_MACCFG2_HUGE_FRAME         0x00000020
#define MOT_TSEC_MACCFG2_LENGTH_CHECK       0x00000010
#define MOT_TSEC_MACCFG2_PADCRC             0x00000004
#define MOT_TSEC_MACCFG2_CRC_EN             0x00000002
#define MOT_TSEC_MACCFG2_FULL_DUPLEX        0x00000001

#define MOT_TSEC_IPGIFG_NBBIPG1(l)      ((l&0x0000007f)<<24)
#define MOT_TSEC_IPGIFG_NBBIPG2(l)      ( (l&0x0000007f)<<16)
#define MOT_TSEC_IPGIFG_MIFGE(l)        ((l&0x000000ff)<<8)
#define MOT_TSEC_IPGIFG_BBIPG(l)        (l&0x0000007f)

#define MOT_TSEC_HALDUP_ALTBEB_TRUNC(l) ((l&0x0000000f)<<20)
#define MOT_TSEC_HALFDUP_BEB            0x00080000
#define MOT_TSEC_HALFDUP_BPNBO          0x00040000
#define MOT_TSEC_HALFDUP_NBO            0x00020000
#define MOT_TSEC_HALFDUP_EXCESS_DEF     0x00010000
#define MOT_TSEC_HALDUP_RETRY(v)        ((v&0x0000000F)<<12)
#define MOT_TSEC_HALDUP_COL_WINDOW(w)   (w&0x003f)

#define MOT_TSEC_MAXFRM_MASK(l)         (l&0x0000ffff)
#define MOT_TSEC_MIIMCFG_RESET          0x80000000
#define MOT_TSEC_MIIMCFG_NO_PRE         0x00000010
#define MOT_TSEC_MIIMCFG_MCS(l)         (l&0x00000007)
#define MOT_TSEC_MIIMCFG_MCS_2          0x00000000
#define MOT_TSEC_MIIMCFG_MCS_4          0x00000001
#define MOT_TSEC_MIIMCFG_MCS_6          0x00000002
#define MOT_TSEC_MIIMCFG_MCS_8          0x00000003
#define MOT_TSEC_MIIMCFG_MCS_10         0x00000004
#define MOT_TSEC_MIIMCFG_MCS_14         0x00000005
#define MOT_TSEC_MIIMCFG_MCS_20         0x00000006
#define MOT_TSEC_MIIMCFG_MCS_28         0x00000007

#define MOT_TSEC_MIIMCOM_SCAN_CYCLE     0x00000002
#define MOT_TSEC_MIIMCOM_READ_CYCLE     0x00000001

#define MOT_TSEC_MIIMADD_PHYADRS(a)     ((a&0x0000001f)<<8)
#define MOT_TSEC_MIIMADD_REGADRS(a)     (a&0x0000001f)

#define MOT_TSEC_MIIMCON_PHY_CTRL(a)    (a&0x0000ffff)

#define MOT_TSEC_MIIMSTAT_PHY(a)        (a&0x0000ffff)

#define MOT_TSEC_MIIMIND_NOT_VALID      0x00000004
#define MOT_TSEC_MIIMIND_SCAN           0x00000002
#define MOT_TSEC_MIIMIND_BUSY           0x00000001

#define MOT_TSEC_IFSTAT_EXCESS_DEF      0x00000200

#define MOT_TSEC_MACSTNADDR1_SA_1       0xff000000
#define MOT_TSEC_MACSTNADDR1_SA_2       0x00ff0000
#define MOT_TSEC_MACSTNADDR1_SA_3       0x0000ff00
#define MOT_TSEC_MACSTNADDR1_SA_4       0x000000ff
#define MOT_TSEC_MACSTNADDR2_SA_5       0xff000000
#define MOT_TSEC_MACSTNADDR2_SA_6       0x00ff0000

/* Transmit Buffer Descriptor bit definitions */
#define MOT_TSEC_TBD_R          0x8000
#define MOT_TSEC_TBD_PADCRC     0x4000
#define MOT_TSEC_TBD_W          0x2000
#define MOT_TSEC_TBD_I          0x1000
#define MOT_TSEC_TBD_L          0x0800
#define MOT_TSEC_TBD_TC         0x0400
#define MOT_TSEC_TBD_DEF        0x0200
#define MOT_TSEC_TBD_HFE_LC     0x0080
#define MOT_TSEC_TBD_RL         0x0040
#define MOT_TSEC_TBD_RC         0x003c
#define MOT_TSEC_TBD_UN         0x0002

/* Receive Buffer Descriptors bit definitions */
#define MOT_TSEC_RBD_E          0x8000
#define MOT_TSEC_RBD_RO1        0x4000
#define MOT_TSEC_RBD_W          0x2000
#define MOT_TSEC_RBD_I          0x1000
#define MOT_TSEC_RBD_L          0x0800
#define MOT_TSEC_RBD_F          0x0400
#define MOT_TSEC_RBD_M          0x0200
#define MOT_TSEC_RBD_BC         0x0080
#define MOT_TSEC_RBD_MC         0x0040
#define MOT_TSEC_RBD_LG         0x0020
#define MOT_TSEC_RBD_NO         0x0010
#define MOT_TSEC_RBD_SH         0x0008
#define MOT_TSEC_RBD_CR         0x0004
#define MOT_TSEC_RBD_OV         0x0002
#define MOT_TSEC_RBD_TR         0x0001

#define MOT_TSEC_RBD_ERR  (MOT_TSEC_RBD_TR | MOT_TSEC_RBD_OV |  \
                           MOT_TSEC_RBD_CR | MOT_TSEC_RBD_SH |  \
                           MOT_TSEC_RBD_NO | MOT_TSEC_RBD_LG)


#define MOT_TSEC_ATTR_ELCWT_NA       0x0
#define MOT_TSEC_ATTR_ELCWT_RSVD     0x1
#define MOT_TSEC_ATTR_ELCWT_L2       0x2
#define MOT_TSEC_ATTR_ELCWT_L2_LOCK  0x3
#define MOT_TSEC_ATTR_ELCWT(v)       ((v&0x00000003)<<13)

#define MOT_TSEC_ATTR_BDLWT_NA       0x0
#define MOT_TSEC_ATTR_BDLWT_RSVD     0x1
#define MOT_TSEC_ATTR_BDLWT_L2       0x2
#define MOT_TSEC_ATTR_BDLWT_L2_LOCK  0x3
#define MOT_TSEC_ATTR_BDLWT(v)       ((v&0x00000003)<<10)

#define MOT_TSEC_ATTR_RDSEN          0x00000080
#define MOT_TSEC_ATTR_RBDSEN         0x00000040

#define MOT_TSEC_ATTRELI_EL(v)       ((v&0x00003fff)<<16)
#define MOT_TSEC_ATTRELI_EI(v)       (v&0x00003fff)

/* MOT_TSEC init string parameters */
/* "MMBASE:MOT_TSEC_PORT:MAC_ADRS:PHY_DEF_MODES:USER_FLAGS:FUNC_TABLE:EXT_PARMS"  */
/*        MMBASE - 85xx local base address. Used as base for driver memory space.
 *      MAC_ADRS - Mac address in 12 digit format eg. 00-A0-1E-11-22-33
 *  PHY_DEF_MODE - Default Attributes passed to the MII driver.
 * USER_DEF_MODE - Mandatory initialization user parameters.
 *    FUNC_TABLE - Table of BSP and Driver callbacks
 *         PARMS - Address of a structure that contains required initialization
 *                 parameters and driver specific tuning parameters.
 *     EXT_PARMS - Address of a structure that contains optional initialization
 *                 parameters and driver specific tuning parameters.
 */

/* MII/PHY PHY_DEF_MODE flags to init the phy driver */
#define MOT_TSEC_USR_MODE_DEFAULT         0
#define MOT_TSEC_USR_PHY_NO_AN   0x00000001  /* do not auto-negotiate */
#define MOT_TSEC_USR_PHY_TBL     0x00000002  /* use negotiation table */
#define MOT_TSEC_USR_PHY_NO_FD   0x00000004  /* do not use full duplex */
#define MOT_TSEC_USR_PHY_NO_100  0x00000008  /* do not use 100Mbit speed */
#define MOT_TSEC_USR_PHY_NO_HD   0x00000010  /* do not use half duplex */
#define MOT_TSEC_USR_PHY_NO_10   0x00000020  /* do not use 10Mbit speed */
#define MOT_TSEC_USR_PHY_MON     0x00000040  /* use PHY Monitor */
#define MOT_TSEC_USR_PHY_ISO     0x00000080  /* isolate a PHY */


/* ECNTRL Ethernet Control */
#define MOT_TSEC_USR_STAT_CLEAR     0x00000100 /* init + runtime clear mstats*/
#define MOT_TSEC_USR_STAT_AUTOZ     0x00000200 /* init */
#define MOT_TSEC_USR_STAT_ENABLE    0x00000400 /* init */

/* PHY bus configuration selections */
#define MOT_TSEC_USR_MODE_MASK      0x0003f800
#define MOT_TSEC_USR_MODE_TBI       0x00000800
#define MOT_TSEC_USR_MODE_RTBI      0x00001000
#define MOT_TSEC_USR_MODE_MII       0x00002000
#define MOT_TSEC_USR_MODE_GMII      0x00004000
#define MOT_TSEC_USR_MODE_RGMII     0x00008000
#define MOT_TSEC_USR_MODE_RGMII_10  0x00010000
#define MOT_TSEC_USR_MODE_RGMII_100 0x00020000

/* MOT_TSEC extended initialization parameters */

/* Bit flags */
/* DMACTRL - Configure the DMA block */
#define MOT_TSEC_TX_SNOOP_EN          0x00000001 /* snoop Tx Clusters */
#define MOT_TSEC_TX_BD_SNOOP_EN       0x00000002 /* snoop txbds */
#define MOT_TSEC_TX_WWR               0x00000004 /* init */
#define MOT_TSEC_TXBD_WOP             0x00000008 /* init */

/* RCTRL - Receive Control flags */
#define MOT_TSEC_BROADCAST_REJECT     0x00000010 /* Broadcast Reject */
#define MOT_TSEC_PROMISCUOUS_MODE     0x00000008 /* Promiscuous Mode*/
#define MOT_TSEC_RX_SHORT_FRAME       0x00000004 /* Rx Shorter Frames */
                                                 /* if (frame<MINFLR)*/
/* MACCFG1 - Mac Configuration */
#define MOT_TSEC_MAC_LOOPBACK         0x00000080  /* init + runtime */
#define MOT_TSEC_MAC_RX_FLOW          0x00000100  /* enable Rx Flow */
#define MOT_TSEC_MAC_TX_FLOW          0x00000200  /* enable Tx Flow */
/* MACCFG2 Mac Configuration */
#define MOT_TSEC_MAC_HUGE_FRAME       0x00000400  /* enable huge frame support*/
#define MOT_TSEC_MAC_PADCRC           0x00000800  /* MAC pads short frames */
                                                  /* and appends CRC */
#define MOT_TSEC_MAC_CRC_ENABLE       0x00001000  /* MAC appends CRC */
#define MOT_TSEC_MAC_DUPLEX           0x00002000  /* MAC duplex mode */
/* ATTR */
#define MOT_TSEC_RX_SNOOP_ENABLE      0x00004000  /* snoop Rx cluster */
#define MOT_TSEC_RXBD_SNOOP_ENABLE    0x00008000  /* snoop rxbds */
/* MII flags */
#define MOT_TSEC_MII_NOPRE            0x00010000  /* suppress preamble */
#define MOT_TSEC_MII_RESET            0x00020000  /* runtime reset MII MGMT */
#define MOT_TSEC_MII_SCAN_CYCLE       0x00040000  /* Continuous read */
#define MOT_TSEC_MII_READ_CYCLE       0x00080000  /* Preform single read */
/* HALDUP */
#define MOT_TSEC_HALDUP_ALTBEB        0x00100000  /* use alternate backoff */ 
                                                  /* algorithm */
#define MOT_TSEC_HALDUP_BACK_PRESSURE 0x00200000  /* immediate retrans back */
                                                  /* pressure */
#define MOT_TSEC_HALDUP_EX_DEFERENCE  0x00400000
#define MOT_TSEC_HALDUP_NOBACKOFF     0x00800000

/* Driver Runtime Attributes */
#define MOT_TSEC_POLLING_MODE         0x01000000    /* polling mode */
#define MOT_TSEC_MULTICAST_MODE       0x02000000    /* multicast addressing */

#define FLAG_POLLING_MODE               0x0001
#define FLAG_PROMISC_MODE               0x0002
#define FLAG_ALLMULTI_MODE              0x0004
#define FLAG_MULTICAST_MODE             0x0008
#define FLAG_BROADCAST_MODE             0x0010


/* MOT_TSEC Attribute Default Values */
#define MOT_TSEC_IMASK_DEFAULT (MOT_TSEC_IEVENT_RXC  | MOT_TSEC_IEVENT_TXC   | \
                            MOT_TSEC_IEVENT_MSRO | MOT_TSEC_IEVENT_GTSC  | \
                            MOT_TSEC_IEVENT_TXB  | MOT_TSEC_IEVENT_RXB0  | \
                            MOT_TSEC_IEVENT_TXF  | MOT_TSEC_IEVENT_GRSC  | \
                            MOT_TSEC_IEVENT_RXF0 )

#if 0
#define MOT_TSEC_IEVENT_ERROR  (MOT_TSEC_IEVENT_BSY  | MOT_TSEC_IEVENT_EBERR | \
                            MOT_TSEC_IEVENT_TXE  | MOT_TSEC_IEVENT_LC    | \
                            MOT_TSEC_IEVENT_CRL  | MOT_TSEC_IEVENT_XFUN  | \
                            MOT_TSEC_IEVENT_BABT | MOT_TSEC_IEVENT_BABR )
#endif
#define MOT_TSEC_MACCFG1_DEFAULT (0x0)


/* IF_MODE = 1 ; MII mode 10/100 only */
#define MOT_TSEC_MACCFG2_DEFAULT  \
                (MOT_TSEC_MACCFG2_PRE_LEN(7)  | \
                 MOT_TSEC_MACCFG2_FULL_DUPLEX | \
                 MOT_TSEC_MACCFG2_PADCRC      | \
                 MOT_TSEC_MACCFG2_CRC_EN      | \
                 MOT_TSEC_MACCFG2_IF_MODE(MOT_TSEC_MACCFG2_IF_MODE_GMII_TBI))

/* CLRCNT = YES,  = NO,  STEN = YES,  TBIM = NO, RPM = NO, R100M = 10 */

#define MOT_TSEC_ECNTRL_DEFAULT  (MOT_TSEC_ECNTRL_STEN | MOT_TSEC_ECNTRL_AUTOZ)

#define MOT_TSEC_TBIPA_DEFAULT  (30)

#define MOT_TSEC_EDIS_DEFAULT (MOT_TSEC_EDIS_EBERRDIS 	| 	\
                               MOT_TSEC_EDIS_TXEDIS 	| 	\
			       MOT_TSEC_EDIS_LCDIS    	| 	\
                               MOT_TSEC_EDIS_CRLDIS 	| 	\
			       MOT_TSEC_EDIS_XFUNDIS )

/* Minimum Frame Receive Length */
#define MOT_TSEC_MINFLR_DEFAULT (64)
/* Tx Flow Control PAUSE Time Default  */
#define MOT_TSEC_PVT_DEFAULT  (MOT_TSEC_PTV_PTE(0)+ MOT_TSEC_PTV_PTE(0))

/* DMA Control Register Default */
#define MOT_TSEC_DMACTRL_DEFAULT  (MOT_TSEC_DMACTRL_TDSEN  | \
                                   MOT_TSEC_DMACTRL_TBDSEN | \
                                   MOT_TSEC_DMACTRL_WWR | \
                                   MOT_TSEC_DMACTRL_WOP)

#define MOT_TSEC_FIFO_TX_THR_DEFAULT           (32) 
#define MOT_TSEC_FIFO_TX_STARVE_DEFAULT        (16) 
#define MOT_TSEC_FIFO_TX_STARVE_OFF_DEFAULT    (32) 

/* Interrupt Coalescing */

#undef MOT_TSEC_ENABLE_TXIC
#undef MOT_TSEC_ENABLE_RXIC

#ifdef MOT_TSEC_ENABLE_TXIC
/* These values are suspect, 16ct/300clks might be more reasonable */
#define MOT_TSEC_TXIC_DEFAULT	(MOT_TSEC_IC_ICEN |		\
				 MOT_TSEC_IC_ICFCT(128) |	\
				 MOT_TSEC_IC_ICTT(64))
#else
#define MOT_TSEC_TXIC_DEFAULT	(MOT_TSEC_IC_ICFCT(128) |	\
				 MOT_TSEC_IC_ICTT(64))
#endif /* MOT_TSEC_ENABLE_TXIC */

#ifdef MOT_TSEC_ENABLE_RXIC
#define MOT_TSEC_RXIC_DEFAULT	(MOT_TSEC_IC_ICEN |		\
				 MOT_TSEC_IC_ICFCT(32) |	\
				 MOT_TSEC_IC_ICTT(400))
#else
#define MOT_TSEC_RXIC_DEFAULT	(MOT_TSEC_IC_ICFCT(32) |	\
				 MOT_TSEC_IC_ICTT(400))
#endif /* MOT_TSEC_ENABLE_RXIC */


/* BACK PRESSURE = NO, RFC_PAUSE = NO, TFC_PAUSE = NO */
#define MOT_TSEC_TCTRL_DEFAULT      (0)
#define MOT_TSEC_TSTAT_DEFAULT      (MOT_TSEC_TSTAT_THLT)

/* PROMISCUOUS = NO, BROAD_REJECT = NO, RX SHORT FRAMES = NO */
#define MOT_TSEC_RCTRL_DEFAULT      (0)
#define MOT_TSEC_RSTAT_DEFAULT      (0)
#define MOT_TSEC_MRBLR_DEFAULT      MOT_TSEC_MRBLR_MASK(0x600)
#define MOT_TSEC_MRBLR_JUMBO        MOT_TSEC_MRBLR_MASK(MOT_TSEC_JUMBO_CL_SIZE)

#define MOT_TSEC_IPGIFG_DEFAULT     (MOT_TSEC_IPGIFG_NBBIPG1(0x40) | \
                                     MOT_TSEC_IPGIFG_NBBIPG2(0x60) | \
                                     MOT_TSEC_IPGIFG_MIFGE(0x50)   | \
                                     MOT_TSEC_IPGIFG_BBIPG(0x60))

#define MOT_TSEC_HAFDUP_DEFAULT     (MOT_TSEC_HALDUP_ALTBEB_TRUNC(0x0a) | \
                                     MOT_TSEC_HALDUP_RETRY(0x0f)        | \
                                     MOT_TSEC_HALDUP_COL_WINDOW(0x37)     )

#define MOT_TSEC_MAXFRM_DEFAULT      MOT_TSEC_MAXFRM_MASK(1522)
#define MOT_TSEC_MAXFRM_JUMBO        MOT_TSEC_MAXFRM_MASK(9022)
#define MOT_TSEC_MIICFG_DEFAULT     (MOT_TSEC_MIIMCFG_MCS(MOT_TSEC_MIIMCFG_MCS_14))
#define MOT_TSEC_MIICOM_DEFAULT      0
#define MOT_TSEC_IFSTAT_DEFAULT      0

#define MOT_TSEC_ATTR_DEFAULT_NO_L2  (MOT_TSEC_ATTR_RDSEN	| \
                                      MOT_TSEC_ATTR_RBDSEN	| \
                                MOT_TSEC_ATTR_BDLWT(MOT_TSEC_ATTR_BDLWT_NA)) 

#define MOT_TSEC_ATTR_DEFAULT_L2				\
	(MOT_TSEC_ATTR_RDSEN | MOT_TSEC_ATTR_RBDSEN  |		\
	MOT_TSEC_ATTR_BDLWT(MOT_TSEC_ATTR_BDLWT_L2)  |		\
        MOT_TSEC_ATTR_ELCWT(MOT_TSEC_ATTR_ELCWT_L2))

/*
 * Set extraction length to 64 bytes. This should be enough
 * to cover the ethernet frame header, plus the IP header, which
 * theoretically should give us a performance boost when doing
 * IP forwarding.
 */
#define MOT_TSEC_ATTRELI_EL_DEFAULT (MOT_TSEC_ATTRELI_EL(128) | \
                                     MOT_TSEC_ATTRELI_EI(0))


/* MOT_TSEC Register flags */
#define MOT_TSEC_FLAG_MINFLR      0x00000001
#define MOT_TSEC_FLAG_MAXFRM      0x00000002
#define MOT_TSEC_FLAG_PVT         0x00000004
#define MOT_TSEC_FLAG_TBIPA       0x00000008
#define MOT_TSEC_FLAG_FIFO_TX     0x00000010
#define MOT_TSEC_FLAG_IADDR       0x00000020
#define MOT_TSEC_FLAG_GADDR       0x00000040
#define MOT_TSEC_FLAG_MACCFG2     0x00000080
#define MOT_TSEC_FLAG_IPGIFGI     0x00000100
#define MOT_TSEC_FLAG_HAFDUP      0x00000200
#define MOT_TSEC_FLAG_MIICFG      0x00000400
#define MOT_TSEC_FLAG_ATTR        0x00000800

typedef struct
    {
    /* function pointers for BSP and Driver call backs */
    FUNCPTR miiPhyInit;     /* BSP Phy driver init callback */
    FUNCPTR miiPhyInt;      /* BSP call back to process PHY status change interrupt */
    FUNCPTR miiPhyStatusGet;   /* Driver call back to get duplex status  */
    FUNCPTR miiPhyRead;     /* BSP call back to read MII/PHY registers */
    FUNCPTR miiPhyWrite;    /* BSP call back to write MII/PHY registers */
    FUNCPTR enetAddrGet;    /* Driver call back to get the Ethernet address */
    FUNCPTR enetAddrSet;    /* Driver call back to set the Ethernet address */
    FUNCPTR enetEnable;     /* Driver call back to enable the ENET interface */
    FUNCPTR enetDisable;    /* Driver call back to disable the ENET interface */
    FUNCPTR extWriteL2AllocFunc;    /* Driver call back to put tx bd in L2 */
    } MOT_TSEC_FUNC_TABLE;

typedef struct
    {
    UINT32   phyAddr;        /* phy physical address */
    UINT32   phyDefMode;     /* default mode */
    UINT32   phyMaxDelay;    /* max delay */
    UINT32   phyDelayParm;   /* poll interval if in poll mode */
    MII_AN_ORDER_TBL * phyAnOrderTbl;  /* autonegotiation table */
    } MOT_TSEC_PHY_PARAMS;

typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_TSEC_RX_BD;
    
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_TSEC_TX_BD;
 
 /* RCS Remove later */   
typedef struct
    {
    VUINT16 bdStat;
    VUINT16 bdLen;
    VUINT32 bdAddr;
    } MOT_TSEC_BD;
/* RCS Remove later */ 
typedef struct
    {
    UINT8   *memBufPtr;    /* Buffer pointer for allocated buffer space */
    UINT32   memBufSize;   /* Buffer pool size */
    MOT_TSEC_BD *bdBasePtr;    /* Descriptor Base Address */
    UINT32   bdSize;       /* Descriptor Size */
    UINT32   rbdNum;       /* Number of Receive Buffer Descriptors  */
    UINT32   tbdNum;       /* Number of Transmit Buffer Descriptors */
    UINT32   unit0TxVec;
    UINT32   unit0RxVec;
    UINT32   unit0ErrVec;
    UINT32   unit1TxVec;
    UINT32   unit1RxVec;
    UINT32   unit1ErrVec;
    } MOT_TSEC_PARAMS;

typedef struct
    {
    UINT32   inum_tsecTx;    /* Transmit Interrupt */
    UINT32   inum_tsecRx;    /* Receive Interrupt */
    UINT32   inum_tsecErr;    /* Error Interrupt */
    FUNCPTR  inumToIvec;     /* function to convert INUM to IVEC */
    FUNCPTR  ivecToInum;     /* function to convert IVEC to INUM */
    } MOT_TSEC_INT_CTRL;


typedef struct
    {
    UINT32  numInts;
    UINT32  numZcopySends;
    UINT32  numNonZcopySends;
    UINT32  numTXBInts;
    UINT32  numBSYInts;
    UINT32  numRXFInts;
    UINT32  numRXBInts;
    UINT32  numGRAInts;
    UINT32  numRXCInts;
    UINT32  numTXCInts;
    UINT32  numTXEInts;
    UINT32  numOTHERInts;

    UINT32  numRXFHandlerEntries;
    UINT32  numRXFHandlerErrQuits;
    UINT32  numRXFHandlerFramesProcessed;
    UINT32  numRXFHandlerFramesRejected;
    UINT32  numRXFHandlerNetBufAllocErrors;
    UINT32  numRXFHandlerNetCblkAllocErrors;
    UINT32  numRXFHandlerNetMblkAllocErrors;
    UINT32  numRXFHandlerFramesCollisions;
    UINT32  numRXFHandlerFramesCrcErrors;
    UINT32  numRXFHandlerFramesLong;
    UINT32  numRXFExceedBurstLimit;

    UINT32  numNetJobAddErrors;

    UINT32  numRxStallsEntered;
    UINT32  numRxStallsCleared;

    UINT32  numTxStallsEntered;
    UINT32  numTxStallsCleared;
    UINT32  numTxStallErrors;

    UINT32  numLSCHandlerEntries;
    UINT32  numLSCHandlerExits;

    UINT32  txErr;
    UINT32  HbFailErr;
    UINT32  txLcErr;
    UINT32  txUrErr;
    UINT32  txCslErr;
    UINT32  txRlErr;
    UINT32  txDefErr;

    UINT32  rxBsyErr;
    UINT32  rxLgErr;
    UINT32  rxNoErr;
    UINT32  rxCrcErr;
    UINT32  rxOvErr;
    UINT32  rxShErr;
    UINT32  rxLcErr;
    UINT32  rxMemErr;
    } MOT_TSEC_DRIVER_STATS;





/* Driver State Variables */

#define MOT_TSEC_STATE_INIT          0x00
#define MOT_TSEC_STATE_NOT_LOADED    0x00
#define MOT_TSEC_STATE_LOADED        0x01
#define MOT_TSEC_STATE_NOT_RUNNING   0x00
#define MOT_TSEC_STATE_RUNNING       0x02

/* Internal driver flags */

#define MOT_TSEC_OWN_BUF_MEM    0x01    /* internally provided memory for data*/
#define MOT_TSEC_INV_TBD_NUM    0x02    /* invalid tbdNum provided */
#define MOT_TSEC_INV_RBD_NUM    0x04    /* invalid rbdNum provided */
#define MOT_TSEC_POLLING        0x08    /* polling mode */
#define MOT_TSEC_PROM           0x20    /* promiscuous mode */
#define MOT_TSEC_MCAST          0x40    /* multicast addressing mode */
#define MOT_TSEC_FD             0x80    /* full duplex mode */
#define MOT_TSEC_OWN_BD_MEM     0x10    /* internally provided memory for BDs */
#define MOT_TSEC_MIN_TX_PKT_SZ   100     /* the smallest packet we send */

#define MOT_TSEC_CL_NUM_DEFAULT   128   /* number of tx clusters */
#define MOT_TSEC_CL_MULTIPLE       11    /* ratio of clusters to RBDs */
#define MOT_TSEC_TBD_NUM_DEFAULT  128    /* default number of TBDs */
#define MOT_TSEC_RBD_NUM_DEFAULT  128    /* default number of RBDs */
#define MOT_TSEC_TX_POLL_NUM      1     /* one TBD for poll operation */

#define MOT_TSEC_CL_OVERHEAD      4     /* prepended cluster overhead */
#define MOT_TSEC_CL_ALIGNMENT     64    /* cluster required alignment */
#define MOT_TSEC_CL_SIZE          1536  /* cluster size */
#define MOT_TSEC_JUMBO_CL_SIZE    9056  /* jumbo cluster size */
#define MOT_TSEC_MTU              1500
#define MOT_TSEC_JUMBO_MTU        9000
#define HEND_JUMBO_FRAMES		/* enable jumbo frame support */
#define MOT_TSEC_MBLK_ALIGNMENT   64    /* mBlks required alignment */
#define MOT_TSEC_RX_BD_SIZE          0x8   /* size of MOT_TSEC BD */
#define MOT_TSEC_TX_BD_SIZE          0x8   /* size of MOT_TSEC BD */
#define MOT_TSEC_BD_ALIGN         64   /* required alignment for BDs */
#define MOT_TSEC_BUF_ALIGN        64   /* required alignment for data buffer */
/*
 * the total is 0x630 and it accounts for the required alignment
 * of receive data buffers, and the cluster overhead.
 */
#define XXX_MOT_TSEC_MAX_CL_LEN ((MII_ETH_MAX_PCK_SZ             \
                            + (MOT_TSEC_BUF_ALIGN - 1)       \
                            + MOT_TSEC_BUF_ALIGN             \
                            + (MOT_TSEC_CL_OVERHEAD - 1))    \
                            & (~ (MOT_TSEC_CL_OVERHEAD - 1)))

#define MOT_TSEC_MAX_CL_LEN     ROUND_UP(XXX_MOT_TSEC_MAX_CL_LEN,MOT_TSEC_BUF_ALIGN)

#define MOT_TSEC_RX_CL_SZ       (MOT_TSEC_MAX_CL_LEN)
#define MOT_TSEC_TX_CL_SZ       (MOT_TSEC_MAX_CL_LEN)

/* BIT mask defines for hardware specific PHY events. */
#define MOT_TSEC_PHY_EVENT_AUTONEG_ERROR    0x0001
#define MOT_TSEC_PHY_EVENT_SPEED            0x0002
#define MOT_TSEC_PHY_EVENT_DUPLEX           0x0004
#define MOT_TSEC_PHY_EVENT_AUTONEG_COMPLETE 0x0008
#define MOT_TSEC_PHY_EVENT_LINK             0x0010
#define MOT_TSEC_PHY_EVENT_SYMBOL_ERROR     0x0020
#define MOT_TSEC_PHY_EVENT_FALSE_CARRIER    0x0040
#define MOT_TSEC_PHY_EVENT_FIFO_ERROR       0x0080
#define MOT_TSEC_PHY_EVENT_XOVER            0x0100
#define MOT_TSEC_PHY_EVENT_DOWNSHIFT        0x0200
#define MOT_TSEC_PHY_EVENT_POLARITY         0x0400
#define MOT_TSEC_PHY_EVENT_JABBER           0x0800

/* PHY Access definitions */
#define MOT_TSEC_PHY_GIG_STATUS_REG	0xa
#define MOT_TSEC_PHY_1000_M_LINK_FD     0x0800
#define MOT_TSEC_PHY_1000_M_LINK_OK     0x1000

#define MOT_TSEC_PHY_LINK_STATUS        0x5
#define MOT_TSEC_PHY_10_M_LINK_FD       0x0040
#define MOT_TSEC_PHY_100_M_LINK_FD      0x0100

typedef struct
    {
    UINT8  autonegError;            /* 0-N/A, 0 - none,   1- error */
    UINT8  duplex;                  /* 1 - half,   2- full */
#define MOT_TSEC_PHY_DUPLEX_HALF    (1)
#define MOT_TSEC_PHY_DUPLEX_FULL    (2)
    UINT8  speed;                   /* 0-N/A, 1 - 10, 2- 100, 3 -1G */
#define MOT_TSEC_PHY_SPEED_10       (1)
#define MOT_TSEC_PHY_SPEED_100      (2)
#define MOT_TSEC_PHY_SPEED_1000     (3)
    UINT8  link;                    /* 0-N/A, 1 - down, 2- up */
    UINT8  symbolError;             /* 0-N/A, 1 - none, 2- error */
    UINT8  autoNegComplete;         /* 0-N/A, 1 - no, 2- completed */
    UINT8  energyDetect;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  falseCarrier;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  downShift;               /* 0-N/A, 1 - no, 2- detected */
    UINT8  fifoError;               /* 0-N/A, 1 - none, 2- error */
    UINT8  xover;                   /* 0-N/A, 1 - MDI, 2- MDIX */
    UINT8  polarity;                /* 0-N/A, 1 - normal, 2- reversed */
    UINT8  jabber;                  /* 0-N/A, 1 - no, 2- detected */
    UINT8  pageReceived;            /* 0-N/A, 1 - no, 2- detected */
    UINT8  cableLength;             /* 0-N/A */
    UINT8  txPause;                 /* 0-N/A, 1 - no, 2- detected */
    UINT8  rxPause;                 /* 0-N/A, 1 - no, 2- detected */
    UINT8  farEndFault;             /* 0-N/A, 1 - no, 2- detected */
    UINT32 rxErrorCntr;             /* 0-N/A, number of rx errors */
    UINT32 reserved[4];             /* future use */
    } MOT_TSEC_PHY_STATUS;

typedef struct mot_tsec_dl_callbacks
    {
    FUNCPTR extWriteL2AllocFunc;  /* Driver call back to put tx bd in L2 */   
    } MOT_TSEC_DL_CALLBACKS;

typedef union mot_tsec_dl_event_rec
        {
        HEND_RX_CONTROL rx;
        HEND_TX_CONTROL tx;
        }MOT_TSEC_DL_EVENT_REC;
        
typedef struct mot_tsec_hend_event_rec
    {
    HEND_EVENT_REC hEnd;
    MOT_TSEC_DL_EVENT_REC dl;
    }MOT_TSEC_HEND_EVENT_REC;
    
#if 0    
typedef struct mot_tsec_hend_qrec
    {
    MOT_TSEC_HEND_EVENT_REC eRec;
    HEND_QREC  qRec;
    } MOT_TSEC_HEND_QREC;
#endif

/* The definition of the driver control structure */
    
typedef struct mot_tsec_drv_ctrl
    {
    HEND_DRV_CTRL hEnd;  
    MOT_TSEC_HEND_EVENT_REC  eventRec[MOT_TSEC_NUM_EVENTS];
    HEND_ISR_REC isrRec[MOT_TSEC_NUM_ISRS]; 
    int          numRxQueues;
    SL_LIST *    pRxQueueList;
    SL_LIST *    pTxQueueList;
    
    int          firstRxIndex;
    int          firstTxIndex;

    UINT32       inum_tsecTx;  /* MOT_TSEC Tx interrupt num */
    UINT32       inum_tsecRx;  /* MOT_TSEC Rx interrupt num */
    UINT32       inum_tsecErr;  /*MOT_TSEC Err interrupt num */
    FUNCPTR      inumToIvec;  /* Conversion utility */
    FUNCPTR      ivecToInum;  /* Conversion utility */
    UINT32       initFlags;   /* user init flags */
    UINT32       userFlags;
    UINT32       retries;
    UINT32       maxRetries;

    /* Bsp specific functions and call backs */
    MOT_TSEC_FUNC_TABLE * initFuncs;

    /* Driver specific init parameters */
    MOT_TSEC_PARAMS     * initParms;

    /* Interrupt Controller Specific info */
    MOT_TSEC_INT_CTRL *intCtrl;

    UINT32      tsecNum;        /* physical MOT_TSEC 0 or 1 */

    UINT32      fifoTxBase;     /* address of Tx FIFO in internal RAM */
    UINT32      fifoRxBase;     /* address of Rx FIFO in internal RAM */

    char      * pBufAlloc;      /* Allocated MOT_TSEC memory pool base */
    char      * pBufBase;       /* Rounded MOT_TSEC memory pool base */
    UINT32      bufSize;        /* MOT_TSEC memory pool size */

    MOT_TSEC_BD   * pBdAlloc;       /* MOT_TSEC BDs Alloc pointer */
    MOT_TSEC_BD   * pBdBase;        /* MOT_TSEC BDs base */
    UINT32      bdSize;         /* MOT_TSEC BDs size */
   
    MOT_TSEC_BD   * pTbdBase;   

    volatile BOOL rxStall;       /* rx handler stalled - no Tbd */
    PHY_INFO   * phyInfo;        /* info on a MII-compliant PHY */
    /* transmit buffer descriptor management */

    volatile MOT_TSEC_BD   * pTbdNext;        /* TBD index */

    UINT16      tbiAdr;         /* tbi interface address */
    UINT32      phyFlags;       /* Phy flags */
    UINT32      flags;          /* driver flags */
    UINT32      state;          /* driver state including load flag */
    UINT32      intMask;        /* interrupt mask register */
    UINT32      intErrorMask;   /* interrupt error mask register */
                                 /* transmit command */
    char       * pClBlkArea;     /* cluster block pointer */
    UINT32       clBlkSize;      /* clusters block memory size */
    char       * pMBlkArea;      /* mBlock area pointer */
    UINT32       mBlkSize;       /* mBlocks area memory size */
    CACHE_FUNCS  bufCacheFuncs;  /* cache descriptor */
    CL_POOL_ID   pClPoolId;      /* cluster pool identifier */

    BOOL         lscHandling;

    /* function pointers to support unit testing */

    FUNCPTR     netJobAdd;
    FUNCPTR     muxTxRestart;
    FUNCPTR     muxError;

    /* Bsp specific functions and call backs */

    FUNCPTR     phyInitFunc;     /* BSP Phy Init */
    FUNCPTR     phyStatusFunc;   /* Status Get function */
    FUNCPTR     miiPhyRead;      /* mii Read */
    FUNCPTR     miiPhyWrite;     /* mii Write */
    FUNCPTR     enetEnable;      /* enable ethernet */
    FUNCPTR     enetDisable;     /* disable ethernet */
    FUNCPTR     enetAddrGet;     /* get ethernet address */
    FUNCPTR     enetAddrSet;     /* set ethernet Address */
    FUNCPTR     extWriteL2AllocFunc; /* Use ext write alloc L2 for Tx BD */ 

    END_CAPABILITIES hwCaps;     /* interface hardware offload capabilities */

#ifdef MOT_TSEC_DBG
    MOT_TSEC_DRIVER_STATS *stats;
#endif
    UINT32 missedCnt;
    UINT32 passCnt;
    UINT32 packetCnt;
    UINT32 starveCnt;
    UINT32 busyMissed;
    BOOL   busyState;
    END_MEDIALIST	*mediaList;
    END_ERR lastError;
    UINT32 curMedia;
    UINT32 curStatus;
    VXB_DEVICE_ID pMiiBus;

    int maxMtu;
    } MOT_TSEC_DRV_CTRL;

#define MOT_TSEC_EVENT_CLEAR(pDev)					\
    MOT_TSEC_WRITE((pDev), MOT_TSEC_IEVENT, MOT_TSEC_READ((pDev), MOT_TSEC_IEVENT))

#define MOT_TSEC_RX_EVENT_CLEAR(pDev)					\
    MOT_TSEC_WRITE((pDev),MOT_TSEC_IEVENT,					\
        MOT_TSEC_IEVENT_RXF0|MOT_TSEC_IEVENT_BSY)

#define MOT_TSEC_RX_INT_ENABLE(pDev)					\
    do {								\
        TSEC_SETBIT((pDev),MOT_TSEC_IMASK,				\
            MOT_TSEC_IEVENT_RXF0|MOT_TSEC_IEVENT_BSY);			\
        MOT_TSEC_WRITE((pDev), MOT_TSEC_RSTAT, MOT_TSEC_RSTAT_QHLT);	\
    } while ((0))

#define MOT_TSEC_TX_STALL_ENTER(pDev)

#define MOT_TSEC_TX_STALL_EXIT(pDev)					\
    TSEC_SETBIT((pDev), MOT_TSEC_TSTAT, MOT_TSEC_TSTAT_THLT);


#define MOT_TSEC_TX_INT_DISABLE(pDev)					\
    TSEC_CLRBIT((pDev), MOT_TSEC_IMASK, MOT_TSEC_IEVENT_TXF);

#define MOT_TSEC_TX_INT_ENABLE(pDev)					\
    do {								\
        pDrvCtrl->intMask |= MOT_TSEC_IEVENT_TXF;			\
        MOT_TSEC_WRITE((pDev), MOT_TSEC_IMASK, pDrvCtrl->intMask);		\
    } while ((0))

#define MOT_TSEC_GET_EVENT_REC(x) ((MOT_TSEC_HEND_EVENT_REC *)(x))

#define MOT_TSEC_GET_DRV_CTRL(x) \
((int)(x) - (offsetof (struct mot_tsec_drv_ctrl,eventRec[(((HEND_EVENT_REC *)(x))->index)])))

#define MOT_TSEC_TX_CAPACITY(x) (x)
    
/* MIB macros */
#define MOT_TSEC_HEND_M2_INIT(p)  \
    {                        \
    endM2Init(&(p)->hEnd.endObj, M2_ifType_ethernet_csmacd, \
              (u_char *) &(p)->hEnd.enetAddr, 6, ETHERMTU, \
              1000000000, \
              IFF_NOTRAILERS | IFF_MULTICAST | IFF_BROADCAST); \
    }
#define MOT_TSEC_HEND_M2_PKT(p,m,z)


IMPORT STATUS cacheInvalidate (CACHE_TYPE, void *, size_t);
IMPORT STATUS cacheFlush (CACHE_TYPE, void *, size_t);
IMPORT int    intEnable (int);
IMPORT int    intDisable (int);

#ifdef __cplusplus
}
#endif

#endif /* __INCtsecEndh */

