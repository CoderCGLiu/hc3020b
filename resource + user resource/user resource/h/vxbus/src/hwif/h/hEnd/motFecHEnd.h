/* motFec.h - Motorola Fast Ethernet Controller driver */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,16apr08,h_k  converted to new access method.
01f,31jul06,pdg  added additional members to mot_fec_drv_ctrl for coldfire
                 arch
01e,17jul06,rcs  Added Modular hEnd includes
01d,10may06,rcs  Fixed CQ 39353 
01c,01may06,wap  Fix compiler warnings in project build (SPR #119332)
01b,02feb06,wap  Merge in changes to hEnd API
01a,23jan06,wap  written
*/


#ifndef __INCmotFecHEndh
#define __INCmotFecHEndh

#ifdef __cplusplus
extern "C" {
#endif

#include <net/ethernet.h>
#include <etherLib.h>
#include "hEnd.h"

#if (CPU_FAMILY == COLDFIRE)
#include <hwif/util/vxbDmaLib.h>
#endif

IMPORT void motFecHEndRegister (void);

#ifndef TEST_TOKEN_MERGE
    #define TEST_TOKEN_MERGE
    
    /* __(i) used for function names and variables */
    #define __(i)   motFec ## i
    
    /* _c_(i)used for macros  and typedefs */
    #define _c_(i)  MOT_FEC_ ## i
    
    /* _m_ used for structure name - needed for offsetof */
    #define _m_(i)  mot_fec_ ## i
    
    /* _o_(i) used for device name string */
    #define _o_(i)  motfec ## i
    
    /* _p_(i) used for calls to configuration stub */
    #define _p_(i)  sysMotFec ## i
#else
    #undef __(i)
        #define __(i)    motFec ## i
    #undef _c_(i)
        #define _c_(i)   MOT_FEC_ ## i
    #undef _m_(i)
        #define _m_(i)   mot_fec_ ## i
    #undef _o_(i)
        #define _o_(i)   motfec ## i  
    #undef _p_(i)
        #define _p_(i)   sysMotFec ## i  
#endif

#define HEND_RX_DMA
#define HEND_TX_DMA
#define HEND_INIT_TUPLES

#define MOT_FEC_DEV_NAME        "motfec"
#define MOT_FEC_DEV_NAME_LEN    7

/* This is dumb. */
#define MOT_FEC_MAX_DEVS        2

#define MOT_FEC_DEVICE_ON_NEXUS        TRUE
#define MOT_FEC_HAS_MII_BUS TRUE
#define MOT_FEC_NUM_EVENTS 2
#define MOT_FEC_NUM_ISRS 1
#define MOT_FEC_NUM_RX_QUEUES 1
#define MOT_FEC_NUM_TX_QUEUES 1
#define MOT_FEC_RX_EVENT_ID 0
#define MOT_FEC_TX_EVENT_ID 1
#define MOT_FEC_RX_POLL 0
#define MOT_FEC_TX_PKT_COMP_EVENT_ID 1
#define MOT_FEC_HEND_TX_PAD 0
#define MOT_FEC_FRAG_NUM_LIMIT 16 
#define MOT_FEC_INT_HANDLE 0
#define MOT_FEC_TX_DESC_CAPACITY 1
#define MOT_FEC_TX_DESC_SPAN 1
#define MOT_FEC_ALIGNMENT_OFFSET 0

#if (CPU_FAMILY == COLDFIRE)
#define MOT_FEC_RX_BUF_SZ 2048
#else
#define MOT_FEC_RX_BUF_SZ 1536
#endif

#define MOT_FEC_CL_NUM_DEFAULT   128   /* number of tx clusters */
#define MOT_FEC_CL_MULTIPLE      1    /* ratio of clusters to RBDs */
#define MOT_FEC_TBD_NUM_DEFAULT  128    /* default number of TBDs */
#define MOT_FEC_RBD_NUM_DEFAULT  128    /* default number of RBDs */
#define MOT_FEC_TX_POLL_NUM      1     /* one TBD for poll operation */
#define MOT_FEC_CL_ALIGNMENT     64    /* cluster required alignment */
#define MOT_FEC_RX_BD_SIZE       0x8   /* size of MOT_FEC BD */
#define MOT_FEC_TX_BD_SIZE       0x8   /* size of MOT_FEC BD */
#define MOT_FEC_CL_SIZE          MOT_FEC_RX_BUF_SZ  /* cluster size */

#define MOT_FEC_MAX_CL_LEN	(MOT_FEC_CL_SIZE + MOT_FEC_CL_ALIGNMENT)

#define END_FLAGS_ISSET(setBits)             \
    ((&pDrvCtrl->hEnd.endObj)->flags & (setBits))

#define MOT_FEC_CACHE_FLUSH(address, len)                                   \
        CACHE_DRV_FLUSH (&cacheDmaFuncs, (address), (len))

/*
 * vxBus access macros
 */

#ifdef MOT_FEC_VXB_MACROS

#define MOT_FEC_HANDLE(p) ((MOT_FEC_DRV_CTRL *)(p)->pDrvCtrl)->hEnd.handle

#define MOT_FEC_READ(pDev, addr)					\
    vxbRead32 (MOT_FEC_HANDLE(pDev),					\
		(UINT32  *)((char *)pDev->pRegBase[0] + addr))

#define MOT_FEC_WRITE(pDev, addr, data)					\
    vxbWrite32 (MOT_FEC_HANDLE(pDev),					\
		(UINT32  *)((char *)pDev->pRegBase[0] + addr), (UINT32)data)

#define FEC_SETBIT(pDev, offset, val)          \
        MOT_FEC_WRITE(pDev, offset, MOT_FEC_READ(pDev, offset) | (val))

#define FEC_CLRBIT(pDev, offset, val)          \
        MOT_FEC_WRITE(pDev, offset, MOT_FEC_READ(pDev, offset) & ~(val))

#endif

/* revision D.3 and greater processors require special FEC initialization */

#define REV_D_4 0x0502
#define REV_D_3 0x0501

/*
 * There are two sets of these, one for PPC and another for Coldfire.
 * Names are kept consistent whereever possible.
 */

#if (CPU_FAMILY == COLDFIRE)

#define MOT_FEC0_CSR        0x9000  /* CSRs offset in the MCF5475 RAM */
#define MOT_FEC1_CSR        0x9800  /* CSRs offset in the MCF5475 RAM */
#define MOT_FEC_CSR         MOT_FEC0_CSR

#define MOT_FEC_EVENT       0x0004  /* interrupt event register */
#define MOT_FEC_MASK        0x0008  /* interrupt mask register */
#define MOT_FEC_CTRL        0x0024  /* FEC control register */
#define MOT_FEC_MII_DATA    0x0040  /* MII data register */
#define MOT_FEC_MII_SPEED   0x0044  /* MII speed register */
#define MOT_FEC_MIB_CSR     0x0064  /* MII speed register */
#define MOT_FEC_RX_CTRL     0x0084  /* rx control register */
#define MOT_FEC_RX_HASH     0x0088  /* rx hash register */
#define MOT_FEC_TX_CTRL     0x00c4  /* tx control register */
#define MOT_FEC_ADDR_L      0x00e4  /* lower 32-bits of MAC address */
#define MOT_FEC_ADDR_H      0x00e8  /* upper 16-bits of MAC address */
#define MOT_FEC_OPC         0x00eC  /* Opcode pause duration reg address */
#define MOT_FEC_IAHASH_H    0x0118  /* upper 32-bits of IA hash table */
#define MOT_FEC_IAHASH_L    0x011c  /* lower 32-bits of IA hash table */
#define MOT_FEC_HASH_H      0x0120  /* upper 32-bits of group address */
#define MOT_FEC_HASH_L      0x0124  /* lower 32-bits of group address */
#define MOT_FEC_TFWR        0x0144  /* FEC Transmit FIFO Watermark */
#define MOT_FEC_RFDR        0x0184  /* FEC RX FIFO Data Register*/
#define MOT_FEC_RFSR        0x0188  /* FEC RX FIFO Status Register*/
#define MOT_FEC_RFCR        0x018C  /* FEC RX FIFO Control Register*/
#define MOT_FEC_RLRFP       0x0190  /* FEC RX FIFO Last Read Frame Ptr */
#define MOT_FEC_RLWFP       0x0194  /* FEC RX FIFO Last Write Frame Ptr*/
#define MOT_FEC_RFAR        0x0198  /* FEC RX FIFO Alarm Register*/
#define MOT_FEC_RFRP        0x019C  /* FEC RX FIFO Read Pointer Register*/
#define MOT_FEC_RFWP        0x01A0  /* FEC RX FIFO Write Pointer Register*/
#define MOT_FEC_TFDR        0x01A4  /* FEC TX FIFO Data Register*/
#define MOT_FEC_TFSR        0x01A8  /* FEC TX FIFO Status Register*/
#define MOT_FEC_TFCR        0x01AC  /* FEC TX FIFO Control Register*/
#define MOT_FEC_TLRFP       0x01B0  /* FEC TX FIFO Last Read Frame Ptr */
#define MOT_FEC_TLWFP       0x01B4  /* FEC TX FIFO Last Write Frame Ptr */
#define MOT_FEC_TFAR        0x01B8  /* FEC TX FIFO Alarm Register*/
#define MOT_FEC_TFRP        0x01BC  /* FEC TX FIFO Read Pointer Register*/
#define MOT_FEC_TFWP        0x01C0  /* FEC TX FIFO Write Pointer Register*/
#define MOT_FEC_FRST        0x01C4  /* FEC FIFO Reset Register*/
#define MOT_FEC_CTCWR       0x01C8  /* FEC CRC and Transmit Frame Control Re
gister*/
#define MOT_FEC_RX_ACT      0x018c  /* rx FIFO control register*/
#define MOT_FEC_TX_ACT      0x01ac  /* tx FIFO control register*/

/*
 * The Coldfire has RMON/IEEE stats registers. Each
 * register is 32 bits wide.
 */

#define MOT_FEC_RMON_TX_DROPS		0x0200
#define MOT_FEC_RMON_TX_UCASTS		0x0204
#define MOT_FEC_RMON_TX_BCASTS		0x0208
#define MOT_FEC_RMON_TX_MCASTS		0x020C
#define MOT_FEC_RMON_TX_CRCALGN		0x0210	/* CRC/alignment errors */
#define MOT_FEC_RMON_TX_RUNTS		0x0214
#define MOT_FEC_RMON_TX_GIANTS		0x0218
#define MOT_FEC_RMON_TX_FRAG		0x021C
#define MOT_FEC_RMON_TX_JABBER		0x0220
#define MOT_FEC_RMON_TX_COLLS		0x0224
#define MOT_FEC_RMON_TX_P64		0x0228
#define MOT_FEC_RMON_TX_P65TO128	0x022C
#define MOT_FEC_RMON_TX_P128TO255	0x0230
#define MOT_FEC_RMON_TX_P256TO511	0x0234
#define MOT_FEC_RMON_TX_P512TO1023	0x0238
#define MOT_FEC_RMON_TX_P1024TO2047	0x023C
#define MOT_FEC_RMON_TX_PGTE2048	0x0240
#define MOT_FEC_RMON_TX_OCTETS		0x0244
#define MOT_FEC_IEEE_TX_DROPS		0x0248
#define MOT_FEC_IEEE_TX_OK		0x024C
#define MOT_FEC_IEEE_TX_1COL		0x0250
#define MOT_FEC_IEEE_TX_MCOL		0x0254
#define MOT_FEC_IEEE_TX_DEFERED		0x0258
#define MOT_FEC_IEEE_TX_LATECOL		0x025C
#define MOT_FEC_IEEE_TX_EXCOL		0x0260
#define MOT_FEC_IEEE_TX_UFLOW		0x0264
#define MOT_FEC_IEEE_TX_CARRLOSS	0x0268
#define MOT_FEC_IEEE_TX_SQE		0x026C
#define MOT_FEC_IEEE_TX_FDXFC		0x0270
#define MOT_FEC_IEEE_TX_OCTETS_OK	0x0274

#define MOT_FEC_RMON_RX_DROPS		0x0280
#define MOT_FEC_RMON_RX_UCASTS		0x0284
#define MOT_FEC_RMON_RX_BCASTS		0x0288
#define MOT_FEC_RMON_RX_MCASTS		0x028C
#define MOT_FEC_RMON_RX_CRCALGN		0x0290	/* CRC/alignment errors */
#define MOT_FEC_RMON_RX_RUNTS		0x0294
#define MOT_FEC_RMON_RX_GIANTS		0x0298
#define MOT_FEC_RMON_RX_FRAG		0x029C
#define MOT_FEC_RMON_RX_JABBER		0x02A0
#define MOT_FEC_RMON_RX_RSVD		0x02A4
#define MOT_FEC_RMON_RX_P64		0x02A8
#define MOT_FEC_RMON_RX_P65TO128	0x02AC
#define MOT_FEC_RMON_RX_P128TO255	0x02B0
#define MOT_FEC_RMON_RX_P256TO511	0x02B4
#define MOT_FEC_RMON_RX_P512TO1023	0x02B8
#define MOT_FEC_RMON_RX_P1024TO2047	0x02BC
#define MOT_FEC_RMON_RX_PGTE2048	0x02C0
#define MOT_FEC_RMON_RX_OCTETS		0x02C4
#define MOT_FEC_IEEE_RX_DROPS		0x02C8
#define MOT_FEC_IEEE_RX_OK		0x02CC
#define MOT_FEC_IEEE_RX_CRC		0x02D0
#define MOT_FEC_IEEE_RX_ALIGN		0x02D4
#define MOT_FEC_IEEE_RX_OFLOW		0x02D8
#define MOT_FEC_IEEE_RX_FDXFC		0x02DC
#define MOT_FEC_IEEE_RX_OCTETS_OK	0x02E0

#else /* CPU_FAMILY == COLDFIRE */

#define MOT_FEC_CSR         0x0e00  /* CSRs offset in the 860T RAM */
#define MOT_FEC_ADDR_L      0x0000  /* lower 32-bits of MAC address */
#define MOT_FEC_ADDR_H      0x0004  /* upper 16-bits of MAC address */
#define MOT_FEC_HASH_H      0x0008  /* upper 32-bits of hash table */
#define MOT_FEC_HASH_L      0x000c  /* lower 32-bits of hash table */
#define MOT_FEC_RX_START    0x0010  /* rx ring start address */
#define MOT_FEC_TX_START    0x0014  /* tx ring start address */
#define MOT_FEC_RX_BUF      0x0018  /* max rx buf length */
#define MOT_FEC_CTRL        0x0040  /* FEC control register */
#define MOT_FEC_EVENT       0x0044  /* interrupt event register */
#define MOT_FEC_MASK        0x0048  /* interrupt mask register */
#define MOT_FEC_VEC         0x004c  /* interrupt level/vector register */
#define MOT_FEC_RX_ACT      0x0050  /* rx ring has been updated */
#define MOT_FEC_TX_ACT      0x0054  /* tx ring has been updated */
#define MOT_FEC_MII_DATA    0x0080  /* MII data register */
#define MOT_FEC_MII_SPEED   0x0084  /* MII speed register */
#define MOT_FEC_RX_BOUND    0x00cc  /* rx fifo limit in the 860T ram */
#define MOT_FEC_RX_FIFO     0x00d0  /* rx fifo base in the 860T ram */
#define MOT_FEC_TX_FIFO_WMK 0x00e4  /* rx fifo watermark */
#define MOT_FEC_TX_FIFO     0x00ec  /* tx fifo base in the 860T ram */
#define MOT_FEC_SDMA        0x0134  /* function code to SDMA */
#define MOT_FEC_RX_CTRL     0x0144  /* rx control register */
#define MOT_FEC_RX_FR       0x0148  /* max rx frame length */
#define MOT_FEC_TX_CTRL     0x0184  /* tx control register */

#endif /* CPU_FAMILY == COLDFIRE */

/* Control/Status Registers (CSR) bit definitions */

#define MOT_FEC_RX_START_MSK    0xfffffffc      /* quad-word alignment */
                                                /* required for rx BDs */

#define MOT_FEC_TX_START_MSK    0xfffffffc      /* quad-word alignment */
                                                /* required for tx BDs */
/* Ethernet CSR bit definitions */

#define MOT_FEC_ETH_PINMUX      0x00000004      /* select FEC for port D pins */
#define MOT_FEC_ETH_EN          0x00000002      /* enable Ethernet operation */
#define MOT_FEC_ETH_DIS         0x00000000      /* disable Ethernet operation */
#define MOT_FEC_ETH_RES         0x00000001      /* reset the FEC */
#define MOT_FEC_CTRL_MASK       0x00000007      /* FEC control register mask */

/*
 * interrupt bits definitions: these are common to both the
 * mask and the event register.
 */

#define MOT_FEC_EVENT_HB        0x80000000      /* heartbeat error */
#define MOT_FEC_EVENT_BABR      0x40000000      /* babbling rx error */
#define MOT_FEC_EVENT_BABT      0x20000000      /* babbling tx error */
#define MOT_FEC_EVENT_GRA       0x10000000      /* graceful stop complete */
#define MOT_FEC_EVENT_TXF       0x08000000      /* tx frame */
#define MOT_FEC_EVENT_TXB       0x04000000      /* tx buffer */
#define MOT_FEC_EVENT_RXF       0x02000000      /* rx frame */
#define MOT_FEC_EVENT_RXB       0x01000000      /* rx buffer */
#define MOT_FEC_EVENT_MII       0x00800000      /* MII transfer */
#define MOT_FEC_EVENT_BERR      0x00400000      /* U-bus access error */
#define MOT_FEC_EVENT_MSK       0xffc00000      /* clear all interrupts */
#define MOT_FEC_MASK_ALL        MOT_FEC_EVENT_MSK    /* mask all interrupts */

/* bit masks for the interrupt level/vector CSR */

#define MOT_FEC_LVL_MSK         0xe0000000      /* intr level */
#define MOT_FEC_TYPE_MSK        0x0000000c      /* highest pending intr */
#define MOT_FEC_VEC_MSK         0xe000000c      /* this register mask */
#define MOT_FEC_RES_MSK         0x1ffffff3      /* reserved bits */
#define MOT_FEC_LVL_SHIFT       0x1d            /* intr level bits location */

/* transmit and receive active registers definitions */

#define MOT_FEC_TX_ACTIVE       0x01000000      /* tx active bit */
#define MOT_FEC_RX_ACTIVE       0x01000000      /* rx active bit */


#if (CPU_FAMILY == COLDFIRE)
/* bit settings for the transmit watermark register */
#define MOT_FEC_TFWR_X_WMRK_64  0x00000000      /* watermark = 64 bytes */
#define MOT_FEC_TFWR_X_WMRK_128 0x00000001      /* watermark = 128 bytes */
#define MOT_FEC_TFWR_X_WMRK_192 0x00000002      /* watermark = 192 bytes */
#define MOT_FEC_TFWR_X_WMRK_256 0x00000003      /* watermark = 256 bytes */
#define MOT_FEC_TFWR_X_WMRK_320 0x00000004      /* watermark = 320 bytes */
#define MOT_FEC_TFWR_X_WMRK_384 0x00000005      /* watermark = 384 bytes */
#define MOT_FEC_TFWR_X_WMRK_448 0x00000006      /* watermark = 448 bytes */
#define MOT_FEC_TFWR_X_WMRK_512 0x00000007      /* watermark = 512 bytes */

#define MOT_FEC_TFWR_X_WMRK_576 0x00000008      /* watermark = 576 bytes */
#define MOT_FEC_TFWR_X_WMRK_640 0x00000009      /* watermark = 640 bytes */
#define MOT_FEC_TFWR_X_WMRK_704 0x0000000a      /* watermark = 704 bytes */
#define MOT_FEC_TFWR_X_WMRK_768 0x0000000b      /* watermark = 768 bytes */
#define MOT_FEC_TFWR_X_WMRK_832 0x0000000c      /* watermark = 832 bytes */
#define MOT_FEC_TFWR_X_WMRK_896 0x0000000d      /* watermark = 896 bytes */
#define MOT_FEC_TFWR_X_WMRK_960 0x0000000e      /* watermark = 960 bytes */
#define MOT_FEC_TFWR_X_WMRK_1024 0x0000000f      /* watermark = 1024 bytes */

/* bit settings for the transmit FIFO alarm register */
#define MOT_FEC_TX_ALARM(x)       (((x)&0x000003FF)<<0)

/* bit settings for the transmit FIFO control register */
#define MOT_FEC_TFCR_WFR        0x20000000
#define MOT_FEC_TFCR_FRMEN      0x08000000
#define MOT_FEC_TFCR_TXWMSK     0x00040000
#define MOT_FEC_TFCR_GR(x)      (((x)&0x00000007)<<24|0x00200000)

/* bit settings for the receive FIFO control register */
#define MOT_FEC_RFCR_FRMEN      0x08000000
#define MOT_FEC_RFCR_RXWMSK     0x00000000
#define MOT_FEC_RFCR_GR(x)      (((x)&0x00000007)<<24)

/* bit settings for the receive FIFO alarm register */
#define MOT_FEC_RX_ALARM(x)       (((x)&0x000003FF)<<0)


/* bit settings for the CTCWR register */
#define MOT_FEC_CTCWR_TFCW      0x01000000
#define MOT_FEC_CTCWR_CRC       0x02000000

#define MOT_FEC_TYPE_MSK        0x0000000c      /* highest pending intr */
#define MOT_FEC_VEC_MSK         0xe000000c      /* this register mask */
#define MOT_FEC_RES_MSK         0x1ffffff3      /* reserved bits */
#define MOT_FEC_LVL_SHIFT       0x1d            /* intr level bits location */

#endif /* CPU_FAMILY == COLDFIRE */

/* MII management frame CSRs */

#define MOT_FEC_MII_ST          0x40000000      /* start of frame delimiter */
#define MOT_FEC_MII_OP_RD       0x20000000      /* perform a read operation */
#define MOT_FEC_MII_OP_WR       0x10000000      /* perform a write operation */
#define MOT_FEC_MII_ADDR_MSK    0x0f800000      /* PHY address field mask */
#define MOT_FEC_MII_REG_MSK     0x007c0000      /* PHY register field mask */
#define MOT_FEC_MII_TA          0x00020000      /* turnaround */
#define MOT_FEC_MII_DATA_MSK    0x0000ffff      /* PHY data field */
#define MOT_FEC_MII_RA_SHIFT    0x12            /* mii reg address bits */
#define MOT_FEC_MII_PA_SHIFT    0x17            /* mii PHY address bits */

#define MOT_FEC_MII_PRE_DIS     0x00000080      /* desable preamble */

#if (CPU_FAMILY == COLDFIRE)

/*
 * For the Coldfire: MDCfreq = sys-freq/(4*MII_SPEED)
 * So MII_SPEED = sys-freq/(4*MDCfreq)
 * The max MDCfreq (compliant with IEEE MII spec) is 2.5 MHz
 * These are the recommended values of MII_SPEED for various
 * CPU frequencies. The calculation we use is
 * ((sysCpuSpeed()+9999999)/10000000)<<1
 */

#define MOT_FEC_MII_SPEED_25    (3 << 1)        /* recommended for 25Mhz CPU */
#define MOT_FEC_MII_SPEED_33    (4 << 1)        /* recommended for 33Mhz CPU */
#define MOT_FEC_MII_SPEED_50    (5 << 1)        /* recommended for 50Mhz CPU */
#define MOT_FEC_MII_SPEED_66    (7 << 1)        /* recommended for 66Mhz CPU */
#define MOT_FEC_MII_CPUSPEED   (((sysCpuSpeed()+9999999)/10000000)<<1)

IMPORT unsigned long sysCpuSpeed(void);

#else /* CPU_FAMILY == COLDFIRE */

#define MOT_FEC_MII_SPEED_25    0x00000005      /* recommended for 25Mhz CPU */
#define MOT_FEC_MII_SPEED_33    0x00000007      /* recommended for 33Mhz CPU */
#define MOT_FEC_MII_SPEED_40    0x00000008      /* recommended for 40Mhz CPU */
#define MOT_FEC_MII_SPEED_50    0x0000000a      /* recommended for 50Mhz CPU */
#define MOT_FEC_MII_SPEED_SHIFT 1               /* MII_SPEED bits location */
#define MOT_FEC_MII_CLOCK_MAX   2500000         /* max freq of MII clock (Hz) */

#endif /* CPU_FAMILY == COLDFIRE */

#define MOT_FEC_MII_MAN_DIS     0x00000000      /* disable the MII management */
                                                /* interface */
#define MOT_FEC_MII_SPEED_MSK   0xffffff81      /* speed field mask */

/* FIFO transmit and receive CSRs definitions */

#define MOT_FEC_FIFO_MSK        0x000003ff      /* FIFO rx/tx/bound mask */

/* SDMA function code CSR */

#define MOT_FEC_SDMA_DATA_BE    0x60000000      /* big-endian byte-ordering */
                                                /* for SDMA data transfer */

#define MOT_FEC_SDMA_DATA_PPC   0x20000000      /* PPC byte-ordering */
                                                /* for SDMA data transfer */

#define MOT_FEC_SDMA_DATA_RES   0x00000000      /* reserved value */

#define MOT_FEC_SDMA_BD_BE      0x18000000      /* big-endian byte-ordering */
                                                /* for SDMA BDs transfer */

#define MOT_FEC_SDMA_BD_PPC     0x08000000      /* PPC byte-ordering */
                                                /* for SDMA BDs transfer */


#define MOT_FEC_SDMA_BD_RES     0x00000000      /* reserved value */
#define MOT_FEC_SDMA_FUNC_0     0x00000000      /* no function code */

/* receive control/hash registers bit definitions */

#define MOT_FEC_RX_CTRL_PROM    0x00000008      /* promiscous mode */
#define MOT_FEC_RX_CTRL_MII     0x00000004      /* select MII interface */
#define MOT_FEC_RX_CTRL_DRT     0x00000002      /* disable rx on transmit */
#define MOT_FEC_RX_CTRL_LOOP    0x00000001      /* loopback mode */

#if (CPU_FAMILY == COLDFIRE)

#define MOT_FEC_RX_FR_MSK       0x07ff0000      /* rx frame length mask */
#define MOT_FEC_RX_FCE      0x00000020      /* rx flow control enable */

#else /* CPU_FAMILY == COLDFIRE */

#define MOT_FEC_RX_FR_MSK       0x000007ff      /* rx frame length mask */

#endif /* CPU_FAMILY == COLDFIRE */


/* transmit control register bit definitions */

#define MOT_FEC_TX_CTRL_FD      0x00000004      /* enable full duplex mode */
#define MOT_FEC_TX_CTRL_HBC     0x00000002      /* HB check is performed */
#define MOT_FEC_TX_CTRL_GRA     0x00000001      /* issue a graceful tx stop */

/* rx/tx buffer descriptors definitions */

typedef struct mot_fec_bd {
	volatile UINT16		bdSts;
	volatile UINT16		bdLen;
	volatile UINT32		bdPtr;
} MOT_FEC_BD;

typedef MOT_FEC_BD MOT_FEC_RX_BD;
typedef MOT_FEC_BD MOT_FEC_TX_BD;

#define MOT_FEC_RBD_SZ          8       /* RBD size in byte */
#define MOT_FEC_TBD_SZ          8       /* TBD size in byte */
#define MOT_FEC_TBD_MIN         6       /* min number of TBDs */
#define MOT_FEC_RBD_MIN         4       /* min number of RBDs */
#define MOT_FEC_TBD_POLL_NUM    1       /* one TBD for poll operation */
#define CL_OVERHEAD             4       /* prepended cluster overhead */
#define CL_ALIGNMENT            4       /* cluster required alignment */
#define MBLK_ALIGNMENT          4       /* mBlks required alignment */
#define MOT_FEC_BD_ALIGN        0x10    /* required alignment for RBDs */
#define MOT_FEC_MAX_PCK_SZ      (ETHERMTU + SIZEOF_ETHERHEADER          \
                                 + ETHER_CRC_LEN)

#define MOT_FEC_BD_STAT     0       /* offset of the status word */
#define MOT_FEC_BD_LEN      2       /* offset of the data length word */
#define MOT_FEC_BD_ADDR     4       /* offset of the data pointer word */

/* TBD bits definitions */

#define MOT_FEC_TBD_RDY         0x8000          /* ready for transmission */
#define MOT_FEC_TBD_TO1         0x4000          /* transmit ownership bit 1 */
#define MOT_FEC_TBD_WRAP        0x2000          /* look at CSR5 for next bd */
#define MOT_FEC_TBD_TO2         0x1000          /* transmit ownership bit 2 */
#define MOT_FEC_TBD_INTERRUPT   0x1000          /* interrupt on transmit MCF547x
/8x only */
#define MOT_FEC_TBD_LAST        0x0800          /* last bd in this frame */
#define MOT_FEC_TBD_CRC         0x0400          /* transmit the CRC sequence */
#define MOT_FEC_TBD_DEF         0x0200          /* deferred transmission */
#define MOT_FEC_TBD_HB          0x0100          /* heartbeat error */
#define MOT_FEC_TBD_LC          0x0080          /* late collision */
#define MOT_FEC_TBD_RL          0x0040          /* retransmission limit */
#define MOT_FEC_TBD_UN          0x0002          /* underrun error */
#define MOT_FEC_TBD_CSL         0x0001          /* carrier sense lost */
#define MOT_FEC_TBD_RC_MASK     0x003c          /* retransmission count mask */

/* RBD bits definitions */

#define MOT_FEC_RBD_EMPTY       0x8000          /* ready for reception */
#define MOT_FEC_RBD_RO1         0x4000          /* receive ownership bit 1 */
#define MOT_FEC_RBD_WRAP        0x2000          /* look at CSR4 for next bd */
#define MOT_FEC_RBD_RO2         0x1000          /* receive ownership bit 2 */
#define MOT_FEC_RBD_INTERRUPT   0x1000          /* interrupt on receive MCF547x/8x only */
#define MOT_FEC_RBD_LAST        0x0800          /* last bd in this frame */
#define MOT_FEC_RBD_RES1        0x0400          /* reserved bit 1 */
#define MOT_FEC_RBD_RES2        0x0200          /* reserved bit 2 */
#define MOT_FEC_RBD_MISS        0x0100          /* address recognition miss */
#define MOT_FEC_RBD_BC          0x0080          /* broadcast frame */
#define MOT_FEC_RBD_MC          0x0040          /* multicast frame */
#define MOT_FEC_RBD_LG          0x0020          /* frame length violation */
#define MOT_FEC_RBD_NO          0x0010          /* nonoctet aligned frame */
#define MOT_FEC_RBD_SH          0x0008          /* short frame error */
                                                /* not supported by the 860T */
#define MOT_FEC_RBD_CRC         0x0004          /* CRC error */
#define MOT_FEC_RBD_OV          0x0002          /* overrun error */
#define MOT_FEC_RBD_TR          0x0001          /* truncated frame (>2KB) */
#define MOT_FEC_RBD_ERR         (MOT_FEC_RBD_LG  |                      \
                                 MOT_FEC_RBD_NO  |                      \
                                 MOT_FEC_RBD_CRC |                      \
                                 MOT_FEC_RBD_OV  |                      \
                                 MOT_FEC_RBD_TR)

#define MOT_FEC_CRC_POLY        0x04c11db7      /* CRC polynomium: */
                                                /* x^32 + x^26 + x^23 + */
                                                /* x^22 + x^16 + x^12 + */
                                                /* x^11 + x^10 + x^8  + */
                                                /* x^7  + x^5  + x^4  + */
                                                /* x^2  + x^1  + x^0  + */


#define MOT_FEC_HASH_MASK       0x7c000000      /* bits 27-31 */
#define MOT_FEC_HASH_SHIFT      0x1a            /* to get the index */


#if (CPU_FAMILY == COLDFIRE)

/* FIFO reset register bit definitions */

#define MOT_FEC_FIFO_SW_RST     0x02000000
#define MOT_FEC_FIFO_RST_CTL    0x01000000

#endif /* CPU_FAMILY == COLDFIRE */

#if (CPU_FAMILY == COLDFIRE)

/*
 * DMA Task Priorities
 */
#define FEC0RX_DMA_PRI      5
#define FEC1RX_DMA_PRI      5
#define FECRX_DMA_PRI(x)    ((x == 0) ? FEC0RX_DMA_PRI : FEC1RX_DMA_PRI)
#define FEC0TX_DMA_PRI      6
#define FEC1TX_DMA_PRI      6
#define FECTX_DMA_PRI(x)    ((x == 0) ? FEC0TX_DMA_PRI : FEC1TX_DMA_PRI)

#endif /* CPU_FAMILY == COLDFIRE */

typedef union mot_fec_dl_event_rec
    {
    HEND_RX_CONTROL rx;
    HEND_TX_CONTROL tx;
    } MOT_FEC_DL_EVENT_REC;

typedef struct mot_fec_hend_event_rec
    {
    HEND_EVENT_REC hEnd;
    MOT_FEC_DL_EVENT_REC dl;
    } MOT_FEC_HEND_EVENT_REC;


typedef struct mot_fec_drv_ctrl
    {

    /* Begin hEnd required fields. */

    HEND_DRV_CTRL hEnd;
    MOT_FEC_HEND_EVENT_REC  eventRec[MOT_FEC_NUM_EVENTS];
    HEND_ISR_REC isrRec[MOT_FEC_NUM_ISRS];

    int		numRxQueues;
    SL_LIST *	pRxQueueList;
    SL_LIST *	pTxQueueList;

    int		firstRxIndex;
    int		firstTxIndex;

    char      * pBufAlloc;      /* Allocated MOT_TSEC memory pool base */
    char      * pBufBase;       /* Rounded MOT_TSEC memory pool base */
    UINT32      bufSize;        /* MOT_TSEC memory pool size */

    MOT_FEC_BD	* pBdAlloc;	/* MOT_FEC BDs Alloc pointer */
    MOT_FEC_BD	* pBdBase;	/* MOT_FEC BDs base */
    UINT32	bdSize;		/* MOT_FEC BDs size */

    MOT_FEC_BD	* pTbdBase;

    char	* pClBlkArea;	/* cluster block pointer */
    UINT32	clBlkSize;	/* clusters block memory size */
    char	* pMBlkArea;	/* mBlock area pointer */
    UINT32	mBlkSize;	/* mBlocks area memory size */
    CACHE_FUNCS	bufCacheFuncs;	/* cache descriptor */
    CL_POOL_ID	pClPoolId;	/* cluster pool identifier */

    END_CAPABILITIES hwCaps;     /* interface hardware offload capabilities */

    UINT32      intMask;        /* interrupt mask register */

    /* End hEnd required fields. */

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST       *mediaList;
    END_ERR lastError;
    UINT32 curMedia;
    UINT32 curStatus;
    VXB_DEVICE_ID pMiiBus;
    /* End MII/ifmedia required fields */

    /* FEC private fields */
    int pinMux;

#if (CPU_FAMILY == COLDFIRE)
    /* dma specifics */
    VXB_DMA_RESOURCE_ID	dmaRxId;
    VXB_DMA_RESOURCE_ID	dmaTxId;
    VOIDFUNCPTR		pIsr;
#endif
#if (CPU_FAMILY == PPC)

    /* The PPC FEC has no stats counters, so we maintain them manually. */

    UINT32 rxUcasts;
    UINT32 rxMcasts;
    UINT32 rxBcasts;
    UINT32 rxOctets;

    UINT32 txUcasts;
    UINT32 txMcasts;
    UINT32 txBcasts;
    UINT32 txOctets;
#endif

    UINT32 cpuSpeed;

    } MOT_FEC_DRV_CTRL;


IMPORT STATUS   sysFecEnetAddrGet (UINT32 unit, UINT8 * address);

#if (CPU_FAMILY == COLDFIRE)
IMPORT STATUS   sysFecEnetEnable (MOT_FEC_DRV_CTRL *pDrvCtrl, VOIDFUNCPTR isr);
IMPORT STATUS   sysFecEnetDisable (MOT_FEC_DRV_CTRL *pDrvCtrl, VOIDFUNCPTR isr);
IMPORT void     sysFecEnetTxResume (MOT_FEC_DRV_CTRL *pDrvCtrl);
IMPORT void     sysFecEnetRxResume (MOT_FEC_DRV_CTRL *pDrvCtrl);
#endif /* CPU_FAMILY == COLDFIRE */

#define MOT_FEC_EVENT_CLEAR(pDev)

#if (CPU_FAMILY == COLDFIRE)
#define MOT_FEC_RX_EVENT_CLEAR(pDev)				\
	sysFecEnetRxResume ((MOT_FEC_DRV_CTRL *)pDev->pDrvCtrl);
#else
#define MOT_FEC_RX_EVENT_CLEAR(pDev)					\
    do {								\
        if (!(MOT_FEC_READ (pDev, MOT_FEC_RX_ACT) & MOT_FEC_RX_ACTIVE))	\
            MOT_FEC_WRITE (pDev, MOT_FEC_RX_ACT, MOT_FEC_RX_ACTIVE);	\
    } while ((0))
#endif

#if (CPU_FAMILY == COLDFIRE)
#define MOT_FEC_RX_INT_ENABLE(pDev)				\
	sysFecEnetRxResume ((MOT_FEC_DRV_CTRL *)pDev->pDrvCtrl);
#else
#define MOT_FEC_RX_INT_ENABLE(pDev)				\
    do {							\
        pDrvCtrl->intMask |= MOT_FEC_EVENT_RXF;			\
        MOT_FEC_WRITE (pDev, MOT_FEC_MASK, pDrvCtrl->intMask);	\
    } while ((0))
#endif

#define MOT_FEC_TX_STALL_ENTER(pDev)
#define MOT_FEC_TX_STALL_EXIT(pDev)

#define MOT_FEC_TX_INT_DISABLE(pDev)

#if (CPU_FAMILY == COLDFIRE)
#define MOT_FEC_TX_INT_ENABLE(pDev)				\
	sysFecEnetTxResume ((MOT_FEC_DRV_CTRL *)pDev->pDrvCtrl);
#else
#define MOT_FEC_TX_INT_ENABLE(pDev)				\
    do {							\
        pDrvCtrl->intMask |= MOT_FEC_EVENT_TXF;			\
        MOT_FEC_WRITE (pDev, MOT_FEC_MASK, pDrvCtrl->intMask);	\
    } while ((0))
#endif

#define MOT_FEC_GET_EVENT_REC(x) ((MOT_FEC_HEND_EVENT_REC *)(x))

#define MOT_FEC_GET_DRV_CTRL(x) \
((int)(x) - (offsetof (struct mot_fec_drv_ctrl,eventRec[(((HEND_EVENT_REC *)(x))->index)])))

#define MOT_FEC_TX_CAPACITY(x) (x)

/* MIB macros */
#define MOT_FEC_HEND_M2_INIT(p)  \
    do {                        \
    endM2Init(&(p)->hEnd.endObj, M2_ifType_ethernet_csmacd, \
              (u_char *) &(p)->hEnd.enetAddr, 6, ETHERMTU, \
              100000000, \
              IFF_NOTRAILERS | IFF_MULTICAST | IFF_BROADCAST); \
    } while ((0))

#define MOT_FEC_HEND_M2_PKT(p,m,z)

#ifdef __cplusplus
}
#endif

#endif /* __INCmotFecHEndh */
