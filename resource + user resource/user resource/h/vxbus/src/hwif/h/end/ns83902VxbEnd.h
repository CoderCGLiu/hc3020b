/* ns83902VxbEnd.h - header file for NatSemi ST-NIC  VxBus END driver */

/*
 * Copyright (c) 2006-2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,08aug07,h_k  fixed byte ordering for big endian mode. (CQ:100753)
01d,16jul07,wap  Convert to new register access API
01c,26feb07,wap  Add nicLastError
01b,05jan07,wap  Fix endian check macro
01a,28dec06,wap  written
*/

#ifndef __INCns83902VxbEndh
#define __INCns83902VxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void nicRegister (void);

#ifndef BSP_VERSION

/*
 * The 83902A register space is broken up into pages. There are
 * three available pages, selectable via two bits in the command register,
 * which is always mapped in the same place in all three pages.
 * Page 0 contains the main runtime registers. Page 1 contains the
 * physical address and multicast hash table registers. Page 2
 * contains diagnostic registers which aren't needed in normal operation.
 * Page 3 is invalid and should never be modified.
 */

/* Page 0 registers, read */

#define NIC_CR		0x00	/* Command register */
#define NIC_CLDA0	0x01	/* Current local DMA address 0 */
#define NIC_CLDA1	0x02	/* Current local DMA address 1 */
#define NIC_BNRY	0x03	/* Boundary pointer */
#define NIC_TSR		0x04	/* TX status register */
#define NIC_NCR		0x05	/* Collision count */
#define NIC_FIFO	0x06	/* FIFO */
#define NIC_ISR		0x07	/* Interrupt status register */
#define NIC_CRDA0	0x08	/* Current remode DMA address 0 */
#define NIC_CRDA1	0x09	/* Current remode DMA address 1 */
#define NIC_RSVD0	0x0A
#define NIC_RSVD1	0x0B
#define NIC_RSR		0x0C	/* RX status register */
#define NIC_FRAC	0x0D	/* Frame alignment error counter */
#define NIC_CRCC	0x0E	/* CRC error counter */
#define NIC_MISC	0x0F	/* Missed packet counter */

/* Page 0 registers, write */

#define NIC_PSTART	0x01	/* Page start */
#define NIC_PSTOP	0x02	/* Page stop */
/* #define NIC_BNRY	0x03	   Boundary pointer */
#define NIC_TPSR	0x04	/* TX page start address */
#define NIC_TBCR0	0x05	/* TX byte count 0 */
#define NIC_TBCR1	0x06	/* TX byte count 1 */
/* #define NIC_ISR	0x07	   Interrupt status register */
#define NIC_RSAR0	0x08	/* Remote start address 0 */
#define NIC_RSAR1	0x09	/* Remote start address 1 */
#define NIC_RBCR0	0x0A	/* Remote byte count 0 */
#define NIC_RBCR1	0x0B	/* Remote byte count 1 */
#define NIC_RCR		0x0C	/* RX configuration register */
#define NIC_TCR		0x0D	/* TX configuration register */
#define NIC_DCR		0x0E	/* data configuration register */
#define NIC_IMR		0x0F	/* Interrupt mask register */


/* Command register */

#define NIC_CR_STOP	0x01	/* Software reset */
#define NIC_CR_START	0x02	/* Start ST-NIC */
#define NIC_CR_TXP	0x04	/* Initiate packet transmission */
#define NIC_CR_RDMA_CMD	0x38	/* RDMA command */
#define NIC_CR_PAGESEL	0xC0	/* Page select */

#define NIC_RDMA_READ	0x08	/* Remote read */
#define NIC_RDMA_WRITE	0x10	/* Remote write */
#define NIC_RDMA_SEND	0x18	/* Send packet */
#define NIC_RDMA_ABORT	0x20	/* Abort/complete RDMA */

#define NIC_PAGESEL_0	0x00	/* Select page 0 */
#define NIC_PAGESEL_1	0x40	/* Select page 1 */
#define NIC_PAGESEL_2	0x80	/* Select page 2 */
#define NIC_PAGESEL_3	0xC0	/* reserved */


/* ISR register */

#define NIC_ISR_PRX	0x01	/* Packet received */
#define NIC_ISR_PTX	0x02	/* Packet sent */
#define NIC_ISR_RXE	0x04	/* Receive error */
#define NIC_ISR_TXE	0x08	/* Transmit error */
#define NIC_ISR_OVW	0x10	/* RX overrun */
#define NIC_ISR_CNT	0x20	/* Counter overflow */
#define NIC_ISR_RDC	0x40	/* Remote DMA complete */
#define NIC_ISR_RST	0x80	/* Reset complete */


/* IMR register */

#define NIC_IMR_PRX	0x01	/* Packet received */
#define NIC_IMR_PTX	0x02	/* Packet sent */
#define NIC_IMR_RXE	0x04	/* Receive error */
#define NIC_IMR_TXE	0x08	/* Transmit error */
#define NIC_IMR_OVW	0x10	/* RX overrun */
#define NIC_IMR_CNT	0x20	/* Counter overflow */
#define NIC_IMR_RDC	0x40	/* Remote DMA complete */
#define NIC_IMR_RST	0x80	/* Reset complete */


/* Data configuration register */

#define NIC_DCR_WTS	0x01	/* 0 = byte transfers, 1 = word transfers */
#define NIC_DCR_BOS	0x02	/* 0 = little endian, 1 = big endian */
#define NIC_DCR_LAS	0x04	/* 0 = dual 16 bit DMA, 1 = single 32 bit DMA*/
#define NIC_DCR_LS	0x08	/* 0 = loopback, 1 = normal */
#define NIC_DCR_ARM	0x10	/* autoinit remote DMA */
#define NIC_DCR_FT	0x60	/* FIFO threshold select */

#define NIC_FIFOTHR_1WORD	0x00
#define NIC_FIFOTHR_2WORDS	0x20
#define NIC_FIFOTHR_4WORDS	0x40
#define NIC_FIFOTHR_6WORDS	0x60


/* TX configuration register */

#define NIC_TCR_CRC	0x01	/* 1 = inhibit CRC generation */
#define NIC_TCR_LB	0x06	/* loopback control */
#define NIC_TCR_ATD	0x08	/* Auto transmit disable (flow control) */
#define NIC_TCR_OFST	0x10	/* Collision offset enable */

#define NIC_LOOP_OFF	0x00	/* Normal operation, loopback off */
#define NIC_LOOP_NIC	0x02	/* NIC/MAC loopback */
#define NIC_LOOP_ENDEC	0x04	/* ENDEC loopback */
#define NIC_LOOP_EXT	0x06	/* External loopback */


/* TX status register */

#define NIC_TSR_PTX	0x01	/* Frame transmitted */
#define NIC_TSR_COL	0x04	/* Collision detected */
#define NIC_TSR_ABT	0x08	/* Abort due to excess collisions */
#define NIC_TSR_CRS	0x10	/* Carrier sense lost */
#define NIC_TSR_FU	0x20	/* FIFO underrun */
#define NIC_TSR_CDH	0x40	/* CD heartbeat failure */
#define NIC_TSR_OWC	0x80	/* Out of window (late) collision */


/* RX configuration register */

#define NIC_RCR_SEP	0x01	/* Save bad frames */
#define NIC_RCR_AR	0x02	/* Accept runt frames */
#define NIC_RCR_AB	0x04	/* Accept broadcast frames */
#define NIC_RCR_AM	0x08	/* Accept multicast frames */
#define NIC_RCR_PRO	0x10	/* Accept all unicasts */
#define NIC_RCR_MON	0x20	/* Monitor mode */


/* RX status register */

#define NIC_RSR_PRX	0x01	/* Frame received */
#define NIC_RSR_CRC	0x02	/* CRC error */
#define NIC_RSR_FAE	0x04	/* Frame alignment error */
#define NIC_RSR_FO	0x08	/* FIFO overrun */
#define NIC_RSR_MPA	0x10	/* Missed frame */
#define NIC_RSR_PHY	0x20	/* 0 = unicast, 1 = multicast */
#define NIC_RSR_DIS	0x40	/* Receiver disabled (in monitor mode) */
#define NIC_RSR_DFR	0x80	/* Deferring, jabber */


/* Page 1 registers, read/write */

#define NIC_PAR0	0x01	/* Station address 0 */
#define NIC_PAR1	0x02	/* Station address 1 */
#define NIC_PAR2	0x03	/* Station address 2 */
#define NIC_PAR3	0x04	/* Station address 3 */
#define NIC_PAR4	0x05	/* Station address 4 */
#define NIC_PAR5	0x06	/* Station address 5 */
#define NIC_CURR	0x07	/* Current page */
#define NIC_MAR0	0x08	/* Multicast hash table 0 */
#define NIC_MAR1	0x09	/* Multicast hash table 1 */
#define NIC_MAR2	0x0A	/* Multicast hash table 2 */
#define NIC_MAR3	0x0B	/* Multicast hash table 3 */
#define NIC_MAR4	0x0C	/* Multicast hash table 4 */
#define NIC_MAR5	0x0D	/* Multicast hash table 5 */
#define NIC_MAR6	0x0E	/* Multicast hash table 6 */
#define NIC_MAR7	0x0F	/* Multicast hash table 7 */


/*
 * Frames in the RX DMA buffer will have a special header prepended
 * to them which tells us the RX status and the frame size, as well
 * as the next page pointer.
 */

typedef struct rx_hdr
    {
#if (_BYTE_ORDER == _BIG_ENDIAN)
    UINT8		nextPage;	/* page next pkt starts at */
    UINT8		rxSts;		/* status of packet */
#else
    UINT8		rxSts;		/* status of packet */
    UINT8		nextPage;	/* page next pkt starts at */
#endif
    UINT16		len;		/* frame length */
    } NIC_RX_HDR;


#define NIC_MTU		1500
#define NIC_JUMBO_MTU	9000
#define NIC_CLSIZE	1536
#define NIC_NAME	"nic"
#define NIC_TIMEOUT	10000
#define NIC_RXINTRS	(NIC_IMR_PRX|NIC_IMR_RXE)
#define NIC_TXINTRS	(NIC_IMR_PTX|NIC_IMR_TXE)
#define NIC_INTRS	(NIC_RXINTRS|NIC_TXINTRS|NIC_ISR_OVW)

#define NIC_ETHER_ALIGN	2

#define NIC_TX_CNT	2
#define NIC_TX_INC(x, y)	(x) = (((x) + 1) % y)
#define NIC_TXPAGES	12
#define NIC_MAXPAGES	255
#define NIC_MEMSIZE	65536
#define NIC_MOFF(x)	((x) << 8)
#define NIC_MAX_RX	16
#define NIC_DELAY	16

/*
 * Private adapter context structure.
 */

typedef struct nic_drv_ctrl
    {
    END_OBJ		nicEndObj;
    VXB_DEVICE_ID	nicDev;
    void *		nicBar;
    void *		nicHandle;
    void *		nicMuxDevCookie;

    JOB_QUEUE_ID	nicJobQueue;
    QJOB		nicIntJob;
    volatile BOOL	nicIntPending;

    QJOB		nicRxJob;
    volatile BOOL	nicRxPending;

    QJOB		nicTxJob;
    volatile BOOL	nicTxPending;

    int			nicTxFree;
    BOOL		nicTxStall;
    int			nicTxProd;
    int			nicTxCons;

    M_BLK_ID		nicTxMblk[NIC_TX_CNT];

    int			nicRxPage;
    BOOL		nicRxRecover;

    BOOL		nicPolling;
    M_BLK_ID		nicPollBuf;
    UINT8		nicIntMask;

    UINT8		nicAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	nicCaps;

    int			nicRegWidth;
    int			nicDelay;

    END_ERR		nicLastError;
    } NIC_DRV_CTRL;

LOCAL UINT16 csr_read_2 (VXB_DEVICE_ID, UINT32);
LOCAL void csr_write_2 (VXB_DEVICE_ID, UINT32, UINT16);
LOCAL UINT8 csr_read_1 (VXB_DEVICE_ID, UINT32);
LOCAL void csr_write_1 (VXB_DEVICE_ID, UINT32, UINT8);

LOCAL UINT16 csr_read_2
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    UINT16 data;
    NIC_DRV_CTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;
    data = vxbRead16 (pDrvCtrl->nicHandle,
        (UINT16 *)((UINT32)pDev->pRegBase[1] + addr));
    vxbRead8 (pDrvCtrl->nicHandle, pDev->pRegBase[0]);
    return (data);
    }

LOCAL void csr_write_2
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT16 data
    )
    {
    NIC_DRV_CTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;
    vxbWrite16 (pDrvCtrl->nicHandle,
        (UINT16 *)((UINT32)pDev->pRegBase[1] + addr), data);
    vxbRead8 (pDrvCtrl->nicHandle, pDev->pRegBase[0]);
    return;
    }

LOCAL UINT8 csr_read_1
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    UINT32 flags = 0;
    UINT8 data;
    NIC_DRV_CTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;
    data = vxbRead8 (pDrvCtrl->nicHandle, (UINT8 *)((UINT32)pDev->pRegBase[0] +
#if (_BYTE_ORDER == _LITTLE_ENDIAN)
        (pDrvCtrl->nicRegWidth == 1 ? 0 : 1) +
#endif
        (addr * pDrvCtrl->nicRegWidth)));
    vxbRead8 (pDrvCtrl->nicHandle, pDev->pRegBase[0]);
    return (data);
    }

LOCAL void csr_write_1
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT8 data
    )
    {
    NIC_DRV_CTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;
    vxbWrite8 (pDrvCtrl->nicHandle, (UINT8 *)((UINT32)pDev->pRegBase[0] +
#if (_BYTE_ORDER == _LITTLE_ENDIAN)
        (pDrvCtrl->nicRegWidth == 1 ? 0 : 1) +
#endif
        (addr * pDrvCtrl->nicRegWidth)), data);
    vxbRead8 (pDrvCtrl->nicHandle, pDev->pRegBase[0]);
    return;
    }

#define CSR_READ_4 csr_read_4
#define CSR_READ_2 csr_read_2
#define CSR_READ_1 csr_read_1
#define CSR_WRITE_4 csr_write_4
#define CSR_WRITE_2 csr_write_2
#define CSR_WRITE_1 csr_write_1

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCns83902VxbEndh */
