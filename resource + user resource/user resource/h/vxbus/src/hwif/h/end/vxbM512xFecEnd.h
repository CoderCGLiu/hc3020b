/* vxbM512xFecEnd.h - header file for Freescale MPC512x FEC VxBus END driver */

/*
 * Copyright (c) 2007-2009, 2012, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,16jul13,mpc  Add support for K70 ENET
01h,23oct12,c_l  Set/Clear FRCONT bit to determine the RMII speed.(WIND00383742)
01g,08aug12,c_l  add RMII support for i.MX53. (WIND00344131)
01f,25may12,hcl  add i.MX 6 support
01e,31aug09,d_l  add little endian support for i.MX51.
01d,07jul08,l_g  modify FEC_RX_DESC_CNT and FEC_TX_DESC_CNT to support the
                 package longer than 47000.
01c,10mar08,b_m  add support to MCF5445x FEC (RMII, multiIrq & txFixup).
01b,25dec07,b_m  update to use new vxbus access routines.
01a,18sep07,b_m  written from version 01a of fecVxbEnd.h.
*/

#ifndef __INCvxbM512xFecEndh
#define __INCvxbM512xFecEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void m512xFecRegister(void);

/* FEC register offsets */

#define FEC_ID          0x0000  /* FEC ID register */
#define FEC_EVENT       0x0004  /* interrupt event register */
#define FEC_MASK        0x0008  /* interrupt mask register */
#define FEC_RX_ACT      0x0010  /* receive ring updated flag register */
#define FEC_TX_ACT      0x0014  /* transmit ring updated flag register */
#define FEC_CTRL        0x0024  /* FEC control register */
#define FEC_MII_DATA    0x0040  /* MII data register */
#define FEC_MII_SPEED   0x0044  /* MII speed register */
#define FEC_MIB_CSR     0x0064  /* MIB control register */
#define FEC_RX_CTRL     0x0084  /* rx control register */
#define FEC_RX_HASH     0x0088  /* rx hash register */
#define FEC_TX_CTRL     0x00C4  /* tx control register */
#define FEC_ADDR_L      0x00E4  /* lower 32-bits of MAC address */
#define FEC_ADDR_H      0x00E8  /* upper 16-bits of MAC address */
#define FEC_OPC         0x00EC  /* opcode pause duration reg address */
#define FEC_IAHASH_H    0x0118  /* upper 32-bits of IA hash table */
#define FEC_IAHASH_L    0x011C  /* lower 32-bits of IA hash table */
#define FEC_HASH_H      0x0120  /* upper 32-bits of group address */
#define FEC_HASH_L      0x0124  /* lower 32-bits of group address */
#define FEC_FIFO_ID     0x0140  /* FIFO ID register */
#define FEC_TFWR        0x0144  /* tx FIFO watermark */
#define FEC_RX_BOUND    0x014C  /* rx FIFO end of ram register */
#define FEC_RX_START    0x0150  /* rx FIFO start address register */
#define FEC_RBD_START   0x0180  /* rx BD start address register */
#define FEC_TBD_START   0x0184  /* tx BD start address register */
#define FEC_RX_BUF      0x0188  /* rx buffer size register */
#define FEC_DMA_CTRL    0x01F4  /* DMA control register */

/*
 * The MPC5121e FEC has RMON/IEEE stats registers.
 * Each register is 32 bits wide.
 */

#define FEC_RMON_TX_DROPS           0x0200
#define FEC_RMON_TX_UCASTS          0x0204
#define FEC_RMON_TX_BCASTS          0x0208
#define FEC_RMON_TX_MCASTS          0x020C
#define FEC_RMON_TX_CRCALGN         0x0210
#define FEC_RMON_TX_RUNTS           0x0214
#define FEC_RMON_TX_GIANTS          0x0218
#define FEC_RMON_TX_FRAG            0x021C
#define FEC_RMON_TX_JABBER          0x0220
#define FEC_RMON_TX_COLLS           0x0224
#define FEC_RMON_TX_P64             0x0228
#define FEC_RMON_TX_P65TO128        0x022C
#define FEC_RMON_TX_P128TO255       0x0230
#define FEC_RMON_TX_P256TO511       0x0234
#define FEC_RMON_TX_P512TO1023      0x0238
#define FEC_RMON_TX_P1024TO2047     0x023C
#define FEC_RMON_TX_PGTE2048        0x0240
#define FEC_RMON_TX_OCTETS          0x0244
#define FEC_IEEE_TX_DROPS           0x0248
#define FEC_IEEE_TX_OK              0x024C
#define FEC_IEEE_TX_1COL            0x0250
#define FEC_IEEE_TX_MCOL            0x0254
#define FEC_IEEE_TX_DEFERED         0x0258
#define FEC_IEEE_TX_LATECOL         0x025C
#define FEC_IEEE_TX_EXCOL           0x0260
#define FEC_IEEE_TX_UFLOW           0x0264
#define FEC_IEEE_TX_CARRLOSS        0x0268
#define FEC_IEEE_TX_SQE             0x026C
#define FEC_IEEE_TX_FDXFC           0x0270
#define FEC_IEEE_TX_OCTETS_OK       0x0274

#define FEC_RMON_RX_DROPS           0x0280
#define FEC_RMON_RX_UCASTS          0x0284
#define FEC_RMON_RX_BCASTS          0x0288
#define FEC_RMON_RX_MCASTS          0x028C
#define FEC_RMON_RX_CRCALGN         0x0290
#define FEC_RMON_RX_RUNTS           0x0294
#define FEC_RMON_RX_GIANTS          0x0298
#define FEC_RMON_RX_FRAG            0x029C
#define FEC_RMON_RX_JABBER          0x02A0
#define FEC_RMON_RX_RSVD            0x02A4
#define FEC_RMON_RX_P64             0x02A8
#define FEC_RMON_RX_P65TO128        0x02AC
#define FEC_RMON_RX_P128TO255       0x02B0
#define FEC_RMON_RX_P256TO511       0x02B4
#define FEC_RMON_RX_P512TO1023      0x02B8
#define FEC_RMON_RX_P1024TO2047     0x02BC
#define FEC_RMON_RX_PGTE2048        0x02C0
#define FEC_RMON_RX_OCTETS          0x02C4
#define FEC_IEEE_RX_DROPS           0x02C8
#define FEC_IEEE_RX_OK              0x02CC
#define FEC_IEEE_RX_CRC             0x02D0
#define FEC_IEEE_RX_ALIGN           0x02D4
#define FEC_IEEE_RX_OFLOW           0x02D8
#define FEC_IEEE_RX_FDXFC           0x02DC
#define FEC_IEEE_RX_OCTETS_OK       0x02E0

/* MIB base address & size */

#define FEC_MIB_BASE                0x0200
#define FEC_MIB_SIZE                0x0100

/* Ethernet CSR bit definitions */

#define FEC_ETH_EN          0x00000002      /* enable ethernet operation */
#define FEC_ETH_DIS         0x00000000      /* disable ethernet operation */
#define FEC_ETH_RES         0x00000001      /* reset the FEC */
#define FEC_CTRL_MASK       0x00000007      /* FEC control register mask */
#define FEC_CTRL_DBSWP      0x00000100      /* buffer descriptor byte swap */
#define FEC_CTRL_SPEED      0x00000020      /* 100Mb or 1000Mb mode selection */

/*
 * interrupt bits definitions
 * These are common to both the mask and the event register.
 */

#define FEC_EVENT_HB        0x80000000      /* heartbeat error */
#define FEC_EVENT_BABR      0x40000000      /* babbling rx error */
#define FEC_EVENT_BABT      0x20000000      /* babbling tx error */
#define FEC_EVENT_GRA       0x10000000      /* graceful stop complete */
#define FEC_EVENT_TXF       0x08000000      /* tx frame */
#define FEC_EVENT_TXB       0x04000000      /* tx buffer */
#define FEC_EVENT_RXF       0x02000000      /* rx frame */
#define FEC_EVENT_RXB       0x01000000      /* rx buffer */
#define FEC_EVENT_MII       0x00800000      /* MII transfer */
#define FEC_EVENT_BERR      0x00400000      /* ethernet bus error */
#define FEC_EVENT_LATE_COL  0x00200000      /* late collision */
#define FEC_EVENT_COL_LIM   0x00100000      /* collision retry limit */
#define FEC_EVENT_TXUN      0x00080000      /* tx FIFO underrun */
#define FEC_EVENT_MSK       0xfff80000      /* clear all interrupts */
#define FEC_MASK_ALL        FEC_EVENT_MSK   /* mask all interrupts */

/* receive and transmit active registers definitions */

#define FEC_RX_ACTIVE       0x01000000      /* rx active bit */
#define FEC_TX_ACTIVE       0x01000000      /* tx active bit */

/* RMII mode definitions */

#define FEC_IMX53_END                 (0x63FEC000)
#define FEC_MIIGSK_CFGR_FRCONT        (1 << 6)
#define FEC_MIIGSK_CFGR_LBMODE        (1 << 4)
#define FEC_MIIGSK_CFGR_EMODE         (1 << 3)
#define FEC_MIIGSK_CFGR_IF_MODE_MASK  (3 << 0)
#define FEC_MIIGSK_CFGR_IF_MODE_MII   (0 << 0)
#define FEC_MIIGSK_CFGR_IF_MODE_RMII  (1 << 0)
#define FEC_MIIGSK_ENR_READY          (1 << 2)
#define FEC_MIIGSK_ENR_EN             (1 << 1)
#define FEC_MIIGSK_CFGR               (0x0300)
#define FEC_MIIGSK_ENR                (0x0308)

/* MII management frame CSRs */

#define FEC_MII_ST          0x40000000      /* start of frame delimiter */
#define FEC_MII_OP_RD       0x20000000      /* perform a read operation */
#define FEC_MII_OP_WR       0x10000000      /* perform a write operation */
#define FEC_MII_TA          0x00020000      /* turnaround */
#define FEC_MII_DATA_MSK    0x0000ffff      /* PHY data field */
#define FEC_MII_RA_SHIFT    18              /* mii reg address bits */
#define FEC_MII_PA_SHIFT    23              /* mii phy address bits */

#define FEC_MII_PRE_DIS     0x00000080      /* desable preamble */
#define FEC_MII_SPEED_2MHZ  2000000         /* MDC 2MHz */
#define FEC_MII_SPEED_SHIFT 1               /* MII_SPEED bits location */

/* MIB control register */

#define FEC_MIB_ENABLE      0x00000000      /* enable MIB block */
#define FEC_MIB_DISABLE     0x80000000      /* disable MIB block */

/* receive control registers bit definitions */

#define FEC_RX_CTRL_RMII_ECHO   0x00000800  /* RMII echo mode */
#define FEC_RX_CTRL_RMII_LOOP   0x00000400  /* RMII loopback mode */
#define FEC_RX_CTRL_RMII_10T    0x00000200  /* RMII 10Mbps */
#define FEC_RX_CTRL_RMII_MODE   0x00000100  /* RMII mode (read-only) */

#define FEC_RX_CTRL_PROM    0x00000008      /* promiscous mode */
#define FEC_RX_CTRL_MII     0x00000004      /* select MII interface */
#define FEC_RX_CTRL_DRT     0x00000002      /* disable rx on transmit */
#define FEC_RX_CTRL_LOOP    0x00000001      /* loopback mode */
#define FEC_RX_RGMII_EN     0x00000040      /* RGMII mode enable */
#define FEC_RX_RMII_EN      0x00000100      /* RMII mode enable */

/* transmit control register bit definitions */

#define FEC_TX_CTRL_FD      0x00000004      /* enable full duplex mode */
#define FEC_TX_CTRL_HBC     0x00000002      /* HB check is performed */
#define FEC_TX_CTRL_GRA     0x00000001      /* issue a graceful tx stop */

/* DMA function code CSR */

#define FEC_DMA_DATA_BE     0x80000000      /* big-endian for data transfer */
#define FEC_DMA_BD_BE       0x40000000      /* big-endian for BDs transfer */
#define FEC_DMA_FUNC_0      0x00000000      /* no function code */

/* rx/tx buffer descriptors definitions */

typedef struct fec_desc
    {
#if (_BYTE_ORDER == _BIG_ENDIAN)
    volatile UINT16 bdSts;
    volatile UINT16 bdLen;
#else
    volatile UINT16 bdLen;
    volatile UINT16 bdSts;
#endif
    volatile UINT32 bdPtr;
    } FEC_DESC;

/* RBD bits definitions */

#define FEC_RBD_EMPTY       0x8000          /* ready for reception */
#define FEC_RBD_RO1         0x4000          /* receive ownership bit 1 */
#define FEC_RBD_WRAP        0x2000          /* look at CSR4 for next bd */
#define FEC_RBD_RO2         0x1000          /* receive ownership bit 2 */
#define FEC_RBD_LAST        0x0800          /* last bd in this frame */
#define FEC_RBD_RES1        0x0400          /* reserved bit 1 */
#define FEC_RBD_RES2        0x0200          /* reserved bit 2 */
#define FEC_RBD_MISS        0x0100          /* address recognition miss */
#define FEC_RBD_BC          0x0080          /* broadcast frame */
#define FEC_RBD_MC          0x0040          /* multicast frame */
#define FEC_RBD_LG          0x0020          /* frame length violation */
#define FEC_RBD_NO          0x0010          /* nonoctet aligned frame */
#define FEC_RBD_SH          0x0008          /* short frame error */
#define FEC_RBD_CRC         0x0004          /* CRC error */
#define FEC_RBD_OV          0x0002          /* overrun error */
#define FEC_RBD_TR          0x0001          /* truncated frame (>2KB) */

#define FEC_RBD_ERR         \
        (FEC_RBD_LG | FEC_RBD_NO | FEC_RBD_CRC | FEC_RBD_OV | FEC_RBD_TR)

/* TBD bits definitions */

#define FEC_TBD_RDY         0x8000          /* ready for transmission */
#define FEC_TBD_TO1         0x4000          /* transmit ownership bit 1 */
#define FEC_TBD_WRAP        0x2000          /* look at CSR5 for next bd */
#define FEC_TBD_TO2         0x1000          /* transmit ownership bit 2 */
#define FEC_TBD_LAST        0x0800          /* last bd in this frame */
#define FEC_TBD_CRC         0x0400          /* transmit the CRC sequence */
#define FEC_TBD_ABC         0x0200          /* append bad CRC */

#define FEC_MTU             1500
#define FEC_JUMBO_MTU       9000
#define FEC_CLSIZE          1536
#define FEC_NAME            "motfec"
#define FEC_TIMEOUT         10000
#define FEC_INTRS           (FEC_RXINTRS | FEC_TXINTRS)
#define FEC_RXINTRS         (FEC_EVENT_RXF | FEC_EVENT_RXB | FEC_EVENT_BABR)
#define FEC_TXINTRS         (FEC_EVENT_TXF | FEC_EVENT_TXB | FEC_EVENT_BABT)
#define FEC_RX_DESC_CNT     64
#define FEC_TX_DESC_CNT     64
#define FEC_INC_DESC(x, y)  (x) = (((x) + 1) % y)
#define FEC_TUPLE_CNT       384
#define FEC_MAXFRAG         16
#define FEC_MAX_RX          16

#define FEC_IRQ_NUM         6

/* supported SoC type */

#define FEC_UNKNOWN         0x0
#define FEC_IMX53_QSB       0x1
#define FEC_K70             0x2

/* media interface mode */

#define FEC_MII             0x0
#define FEC_RMII            0x1
#define FEC_RGMII           0x2

/* combine multi-buffer into single one */

#define FEC_COMBINE_BUF_FALSE   0x0
#define FEC_COMBINE_BUF_TRUE    0x1

/* private adapter context structure */

typedef struct fec_drv_ctrl
    {
    END_OBJ         fecEndObj;
    VXB_DEVICE_ID   fecDev;
    void *          fecMuxDevCookie;

    JOB_QUEUE_ID    fecJobQueue;
    QJOB            fecIntJob;
    volatile BOOL   fecIntPending;
    QJOB            fecRxJob;
    volatile BOOL   fecRxPending;
    QJOB            fecTxJob;
    volatile BOOL   fecTxPending;

    UINT8           fecTxCur;
    UINT8           fecTxLast;
    volatile BOOL   fecTxStall;
    UINT16          fecTxThresh;

    BOOL            fecPolling;
    M_BLK_ID        fecPollBuf;
    UINT32          fecIntMask;
    UINT32          fecIntrs;

    UINT8           fecAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES fecCaps;

    END_IFDRVCONF   fecEndStatsConf;
    END_IFCOUNTERS  fecEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST * fecMediaList;
    END_ERR         fecLastError;
    UINT32          fecCurMedia;
    UINT32          fecCurStatus;
    VXB_DEVICE_ID   fecMiiBus;
    VXB_DEVICE_ID   fecMiiDev;
    FUNCPTR         fecMiiPhyRead;
    FUNCPTR         fecMiiPhyWrite;
    int             fecMiiPhyAddr;

    /* end MII/ifmedia required fields */

    VXB_DMA_TAG_ID  fecParentTag;

    VXB_DMA_TAG_ID  fecRxDescTag;
    VXB_DMA_TAG_ID  fecTxDescTag;
    VXB_DMA_MAP_ID  fecRxDescMap;
    VXB_DMA_MAP_ID  fecTxDescMap;
    FEC_DESC *      fecRxDescMem;
    FEC_DESC *      fecTxDescMem;

    VXB_DMA_TAG_ID  fecMblkTag;
    VXB_DMA_MAP_ID  fecRxMblkMap[FEC_RX_DESC_CNT];
    VXB_DMA_MAP_ID  fecTxMblkMap[FEC_TX_DESC_CNT];
    M_BLK_ID        fecRxMblk[FEC_RX_DESC_CNT];
    M_BLK_ID        fecTxMblk[FEC_TX_DESC_CNT];

    UINT32          fecTxProd;
    UINT32          fecTxCons;
    UINT32          fecTxFree;
    UINT32          fecRxIdx;

    SEM_ID          fecMiiSem;

    int             fecMaxMtu;

    void *          regBase;
    void *          handle;
    UINT32          fecFreq;
    int             multiIrq;
    int             txFixup;
    int             fecSocType;
    int             mediaIfMode;
    int             combinBuf;
    } FEC_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif
