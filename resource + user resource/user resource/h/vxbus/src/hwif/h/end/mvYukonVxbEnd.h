/* mvYukonVxbEnd.h - header file for Marvell Yukon VxBus END driver */

/*
 * Copyright (c) 2006, 2007, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,26feb13,wyt  Fix coverity error (WIND00401412)
01e,11jul07,wap  Make this driver SMP safe, convert to new register access API
01d,29jan07,wap  Add additional PCI IDs
01c,05jan07,wap  Remove Yukon II PCI IDs
01b,07dec06,wap  Add jumbo frame support
01a,18aug06,wap  written
*/

#ifndef __INCmvYukonVxbEndh
#define __INCmvYukonVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void ykRegister (void);

#ifndef BSP_VERSION


#define YK_VENDORID_MARVELL	0x11AB

#define YK_DEVICEID_YUKON8001	0x4320

#define YK_VENDORID_DLINK	0x1186
#define YK_DEVICEID_DGE530T	0x4c00
#define YK_DEVICEID_DGE530T2	0x4b01

#define YK_VENDORID_3COM	0x10b7
#define YK_DEVICEID_3C940	0x1700
#define YK_DEVICEID_3C940B	0x80Eb

#define YK_VENDORID_BELKIN	0x5005

#define YK_VENDORID_LINKSYS	0x1737
#define YK_DEVICEID_EG1032	0x1032
#define YK_DEVICEID_EG1064	0x1064
#define LINKSYS_SUBDEVICE_EG1032_REV2   0x0015

#define YK_VENDORID_SYSKONNECT	0x1148

#define YK_CHIP_ID_YUKON	0xB0    /* Yukon */
#define YK_CHIP_ID_YUKON_LITE	0xB1    /* Yukon-Lite (Rev. A1-A3) */
#define YK_CHIP_ID_YUKON_LP	0xB2    /* Yukon-LP */
#define YK_CHIP_ID_YUKON_XL	0xB3    /* Yukon-2 XL */
#define YK_CHIP_ID_YUKON_EC_U	0xB4    /* Yukon-2 EC Ultra */
#define YK_CHIP_ID_YUKON_EC	0xB6    /* Yukon-2 EC */
#define YK_CHIP_ID_YUKON_FE	0xB7    /* Yukon-2 FE */

#define YK_CHIP_REV_YU_LITE_A1	3	/* Yukon-Lite A1,A2 */
#define YK_CHIP_REV_YU_LITE_A3	7	/* Yukon-Lite A3 */

#define YK_CHIP_REV_YU_XL_A0	0	/* Yukon-2 A0 */
#define YK_CHIP_REV_YU_XL_A1	1	/* Yukon-2 A1 */
#define YK_CHIP_REV_YU_XL_A2	2	/* Yukon-2 A2 */
#define YK_CHIP_REV_YU_XL_A3	3	/* Yukon-2 A3 */

#define YK_CHIP_REV_YU_EC_A1	0	/* Yukon-EC A0,A1 */
#define YK_CHIP_REV_YU_EC_A2	1	/* Yukon-EC A2 */
#define YK_CHIP_REV_YU_EC_A3	2	/* Yukon-EC A3 */

#define YK_CHIP_REV_YU_EC_U_A0	1	/* Yukon-EC Ultra A0 */
#define YK_CHIP_REV_YU_EC_U_A1	2	/* Yukon-EC Ultra A1 */

/*
 * The Yukon has a large number of registers, divided into
 * 128 byte blocks. When accessing the device via the memory mapped
 * BAR, all registers are directly addressible. When using the
 * I/O space BAR, only 256 bytes are available, and the register
 * blocks are accessed indirectly by mapping the various blocks
 * in and out using the RAP register.
 *
 * Note that there are really two sets of registers. The first
 * large batch is for the PCI and DMA handling, which corresponds
 * to the functionality of the GEnesis controller. The second batch
 * are the Marvell GMAC registers. Unlike the GEnesis however,
 * the Marvell GMAC registers are mapped into the same space with
 * everything else (the GEnesis required you to access the XMAC's
 * register via an intirect I/O port).
 */

/* Block 0 */

#define YK_RAP		0x0000
#define YK_CSR		0x0004
#define YK_PWR		0x0006
#define YK_ISR		0x0008  /* interrupt source */
#define YK_IMR		0x000C  /* interrupt mask */
#define YK_IESR		0x0010  /* interrupt hardware error source */
#define YK_IEMR		0x0014  /* interrupt hardware error mask */
#define YK_ISSR		0x0018  /* special interrupt source 0 */
#define YK_BMU_RX_CSR	0x0060
#define YK_BMU_TXS_CSR	0x0068
#define YK_BMU_TXA_CSR	0x006C

/* YK_CSR register */
#define YK_CSR_SW_RESET                 0x0001
#define YK_CSR_SW_UNRESET               0x0002
#define YK_CSR_MASTER_RESET             0x0004
#define YK_CSR_MASTER_UNRESET           0x0008
#define YK_CSR_MASTER_STOP              0x0010
#define YK_CSR_MASTER_DONE              0x0020
#define YK_CSR_SW_IRQ_CLEAR             0x0040
#define YK_CSR_SW_IRQ_SET               0x0080
#define YK_CSR_SLOTSIZE                 0x0100 /* 1 == 64 bits, 0 == 32 */
#define YK_CSR_BUSCLOCK                 0x0200 /* 1 == 33/66 Mhz, = 33 */

/* YK_LED register */
#define YK_LED_GREEN_OFF                0x01
#define YK_LED_GREEN_ON                 0x02

/* YK_ISR register */
#define YK_ISR_TX2_AS_CHECK             0x00000001
#define YK_ISR_TX2_AS_EOF               0x00000002
#define YK_ISR_TX2_AS_EOB               0x00000004
#define YK_ISR_TX2_S_CHECK              0x00000008
#define YK_ISR_TX2_S_EOF                0x00000010
#define YK_ISR_TX2_S_EOB                0x00000020
#define YK_ISR_TX1_AS_CHECK             0x00000040
#define YK_ISR_TX1_AS_EOF               0x00000080
#define YK_ISR_TX1_AS_EOB               0x00000100
#define YK_ISR_TX1_S_CHECK              0x00000200
#define YK_ISR_TX1_S_EOF                0x00000400
#define YK_ISR_TX1_S_EOB                0x00000800
#define YK_ISR_RX2_CHECK                0x00001000
#define YK_ISR_RX2_EOF                  0x00002000
#define YK_ISR_RX2_EOB                  0x00004000
#define YK_ISR_RX1_CHECK                0x00008000
#define YK_ISR_RX1_EOF                  0x00010000
#define YK_ISR_RX1_EOB                  0x00020000
#define YK_ISR_LINK2_OFLOW              0x00040000
#define YK_ISR_MAC2                     0x00080000
#define YK_ISR_LINK1_OFLOW              0x00100000
#define YK_ISR_MAC1                     0x00200000
#define YK_ISR_TIMER                    0x00400000
#define YK_ISR_EXTERNAL_REG             0x00800000
#define YK_ISR_SW                       0x01000000
#define YK_ISR_I2C_RDY                  0x02000000
#define YK_ISR_TX2_TIMEO                0x04000000
#define YK_ISR_TX1_TIMEO                0x08000000
#define YK_ISR_RX2_TIMEO                0x10000000
#define YK_ISR_RX1_TIMEO                0x20000000
#define YK_ISR_RSVD                     0x40000000
#define YK_ISR_HWERR                    0x80000000

/* YK_IMR register */
#define YK_IMR_TX2_AS_CHECK             0x00000001
#define YK_IMR_TX2_AS_EOF               0x00000002
#define YK_IMR_TX2_AS_EOB               0x00000004
#define YK_IMR_TX2_S_CHECK              0x00000008
#define YK_IMR_TX2_S_EOF                0x00000010
#define YK_IMR_TX2_S_EOB                0x00000020
#define YK_IMR_TX1_AS_CHECK             0x00000040
#define YK_IMR_TX1_AS_EOF               0x00000080
#define YK_IMR_TX1_AS_EOB               0x00000100
#define YK_IMR_TX1_S_CHECK              0x00000200
#define YK_IMR_TX1_S_EOF                0x00000400
#define YK_IMR_TX1_S_EOB                0x00000800
#define YK_IMR_RX2_CHECK                0x00001000
#define YK_IMR_RX2_EOF                  0x00002000
#define YK_IMR_RX2_EOB                  0x00004000
#define YK_IMR_RX1_CHECK                0x00008000
#define YK_IMR_RX1_EOF                  0x00010000
#define YK_IMR_RX1_EOB                  0x00020000
#define YK_IMR_LINK2_OFLOW              0x00040000
#define YK_IMR_MAC2                     0x00080000
#define YK_IMR_LINK1_OFLOW              0x00100000
#define YK_IMR_MAC1                     0x00200000
#define YK_IMR_TIMER                    0x00400000
#define YK_IMR_EXTERNAL_REG             0x00800000
#define YK_IMR_SW                       0x01000000
#define YK_IMR_I2C_RDY                  0x02000000
#define YK_IMR_TX2_TIMEO                0x04000000
#define YK_IMR_TX1_TIMEO                0x08000000
#define YK_IMR_RX2_TIMEO                0x10000000
#define YK_IMR_RX1_TIMEO                0x20000000
#define YK_IMR_RSVD                     0x40000000
#define YK_IMR_HWERR                    0x80000000

/* YK_IESR register */
#define YK_IESR_PAR_RX2                 0x00000001
#define YK_IESR_PAR_RX1                 0x00000002
#define YK_IESR_PAR_MAC2                0x00000004
#define YK_IESR_PAR_MAC1                0x00000008
#define YK_IESR_PAR_WR_RAM              0x00000010
#define YK_IESR_PAR_RD_RAM              0x00000020
#define YK_IESR_NO_TSTAMP_MAC2          0x00000040
#define YK_IESR_NO_TSTAMO_MAC1          0x00000080
#define YK_IESR_NO_STS_MAC2             0x00000100
#define YK_IESR_NO_STS_MAC1             0x00000200
#define YK_IESR_IRQ_STS                 0x00000400
#define YK_IESR_MASTERERR               0x00000800

/* YK_IEMR register */
#define YK_IEMR_PAR_RX2                 0x00000001
#define YK_IEMR_PAR_RX1                 0x00000002
#define YK_IEMR_PAR_MAC2                0x00000004
#define YK_IEMR_PAR_MAC1                0x00000008
#define YK_IEMR_PAR_WR_RAM              0x00000010
#define YK_IEMR_PAR_RD_RAM              0x00000020
#define YK_IEMR_NO_TSTAMP_MAC2          0x00000040
#define YK_IEMR_NO_TSTAMO_MAC1          0x00000080
#define YK_IEMR_NO_STS_MAC2             0x00000100
#define YK_IEMR_NO_STS_MAC1             0x00000200
#define YK_IEMR_IRQ_STS                 0x00000400
#define YK_IEMR_MASTERERR               0x00000800

/* Block 1 unused */

/* Block 2 */

#define YK_MAC0_0       0x0100
#define YK_MAC0_1       0x0104
#define YK_MAC1_0       0x0108
#define YK_MAC1_1       0x010C
#define YK_MAC2_0       0x0110
#define YK_MAC2_1       0x0114
#define YK_CONNTYPE     0x0118
#define YK_PMDTYPE      0x0119
#define YK_CONFIG       0x011A
#define YK_CHIPVER      0x011B
#define YK_EPROM0       0x011C	/* RAM size */
#define YK_EPROM1       0x011D	/* Clock gating */
#define YK_EPROM2       0x011E	/* application info */
#define YK_EPROM3	0x011F
#define YK_EP_ADDR      0x0120
#define YK_EP_DATA      0x0124
#define YK_EP_LOADCTL   0x0128
#define YK_EP_LOADTST   0x0129
#define YK_TIMERINIT    0x0130
#define YK_TIMER        0x0134
#define YK_TIMERCTL     0x0138
#define YK_TIMERTST     0x0139
#define YK_IMTIMERINIT  0x0140
#define YK_IMTIMER      0x0144
#define YK_IMTIMERCTL   0x0148
#define YK_IMTIMERTST   0x0149
#define YK_IMMR         0x014C
#define YK_IHWEMR       0x0150
#define YK_TESTCTL1     0x0158
#define YK_TESTCTL2     0x0159
#define YK_GPIO         0x015C
#define YK_I2CHWCTL	0x0160
#define YK_I2CHWDATA	0x0164
#define YK_I2CHWIRQ	0x0168
#define YK_I2CSW	0x016C
#define YK_BLNKINIT     0x0170
#define YK_BLNKCOUNT    0x0174
#define YK_BLNKCTL      0x0178
#define YK_BLNKSTS      0x0179
#define YK_BLNKTST      0x017A

#define YK_IMCTL_STOP   0x02
#define YK_IMCTL_START  0x04

#define YK_IMTIMER_TICKS        54
#define YK_IM_USECS(x)          ((x) * YK_IMTIMER_TICKS)

#define YK_IM_MIN       10
#define YK_IM_DEFAULT   100
#define YK_IM_MAX       10000

/*
 * The YK_EPROM0 register contains a byte that describes the
 * amount of SRAM mounted on the NIC. The value also tells if
 * the chips are 64K or 128K. This affects the RAMbuffer address
 * offset that we need to use.
 */
#define YK_RAMSIZE_512K_64      0x1
#define YK_RAMSIZE_1024K_128    0x2
#define YK_RAMSIZE_1024K_64     0x3
#define YK_RAMSIZE_2048K_128    0x4

/*
 * Block 3 Ram interface
 */

#define YK_RAMADDR      0x0180
#define YK_RAMDATA0     0x0184
#define YK_RAMDATA1     0x0188
#define YK_TO0          0x0190
#define YK_TO1          0x0191
#define YK_TO2          0x0192
#define YK_TO3          0x0193
#define YK_TO4          0x0194
#define YK_TO5          0x0195
#define YK_TO6          0x0196
#define YK_TO7          0x0197
#define YK_TO8          0x0198
#define YK_TO9          0x0199
#define YK_TO10         0x019A
#define YK_TO11         0x019B
#define YK_RITIMEO_TMR  0x019C
#define YK_RAMCTL       0x01A0
#define YK_RITIMER_TST  0x01A2

#define YK_RAMCTL_RESET         0x0001
#define YK_RAMCTL_UNRESET       0x0002
#define YK_RAMCTL_CLR_IRQ_WPAR  0x0100
#define YK_RAMCTL_CLR_IRQ_RPAR  0x0200

#define YK_RAM_LINK_OFFSET	0x40

#define YK_RAM(p, r)	(((p)->ykLinkNum * YK_RAM_LINK_OFFSET) + (r))

/* Block 4 and 5 Transmit arbiter and RSS */

#define YK_MINIT_RX1    0x01B0
#define YK_MINIT_RX2    0x01B1
#define YK_MINIT_TX1    0x01B2
#define YK_MINIT_TX2    0x01B3
#define YK_MTIMEO_RX1   0x01B4
#define YK_MTIMEO_RX2   0x01B5
#define YK_MTIMEO_TX1   0x01B6
#define YK_MTIEMO_TX2   0x01B7
#define YK_MACARB_CTL   0x01B8
#define YK_MTIMER_TST   0x01BA
#define YK_RCINIT_RX1   0x01C0
#define YK_RCINIT_RX2   0x01C1
#define YK_RCINIT_TX1   0x01C2
#define YK_RCINIT_TX2   0x01C3
#define YK_RCTIMEO_RX1  0x01C4
#define YK_RCTIMEO_RX2  0x01C5
#define YK_RCTIMEO_TX1  0x01C6
#define YK_RCTIMEO_TX2  0x01C7
#define YK_RECOVERY_CTL 0x01C8
#define YK_RCTIMER_TST  0x01CA

/* Packet arbiter registers */
#define YK_RXPA1_TINIT  0x01D0
#define YK_RXPA2_TINIT  0x01D4
#define YK_TXPA1_TINIT  0x01D8
#define YK_TXPA2_TINIT  0x01DC
#define YK_RXPA1_TIMEO  0x01E0
#define YK_RXPA2_TIMEO  0x01E4
#define YK_TXPA1_TIMEO  0x01E8
#define YK_TXPA2_TIMEO  0x01EC
#define YK_PKTARB_CTL   0x01F0
#define YK_PKTATB_TST   0x01F2

#define YK_PKTARB_TIMEOUT       0x2000

#define YK_PKTARBCTL_RESET              0x0001
#define YK_PKTARBCTL_UNRESET            0x0002
#define YK_PKTARBCTL_RXTO1_OFF          0x0004
#define YK_PKTARBCTL_RXTO1_ON           0x0008
#define YK_PKTARBCTL_RXTO2_OFF          0x0010
#define YK_PKTARBCTL_RXTO2_ON           0x0020
#define YK_PKTARBCTL_TXTO1_OFF          0x0040
#define YK_PKTARBCTL_TXTO1_ON           0x0080
#define YK_PKTARBCTL_TXTO2_OFF          0x0100
#define YK_PKTARBCTL_TXTO2_ON           0x0200
#define YK_PKTARBCTL_CLR_IRQ_RXTO1      0x0400
#define YK_PKTARBCTL_CLR_IRQ_RXTO2      0x0800
#define YK_PKTARBCTL_CLR_IRQ_TXTO1      0x1000
#define YK_PKTARBCTL_CLR_IRQ_TXTO2      0x2000

#define YK_MINIT_XMAC_B2        54
#define YK_MINIT_XMAC_C1        63

#define YK_MACARBCTL_RESET      0x0001
#define YK_MACARBCTL_UNRESET    0x0002
#define YK_MACARBCTL_FASTOE_OFF 0x0004
#define YK_MACARBCRL_FASTOE_ON  0x0008

#define YK_RCINIT_XMAC_B2       54
#define YK_RCINIT_XMAC_C1       0

#define YK_RECOVERYCTL_RX1_OFF  0x0001
#define YK_RECOVERYCTL_RX1_ON   0x0002
#define YK_RECOVERYCTL_RX2_OFF  0x0004
#define YK_RECOVERYCTL_RX2_ON   0x0008
#define YK_RECOVERYCTL_TX1_OFF  0x0010
#define YK_RECOVERYCTL_TX1_ON   0x0020
#define YK_RECOVERYCTL_TX2_OFF  0x0040
#define YK_RECOVERYCTL_TX2_ON   0x0080

#define YK_RECOVERY_XMAC_B2                             \
        (YK_RECOVERYCTL_RX1_ON|YK_RECOVERYCTL_RX2_ON|   \
        YK_RECOVERYCTL_TX1_ON|YK_RECOVERYCTL_TX2_ON)

#define YK_RECOVERY_XMAC_C1                             \
        (YK_RECOVERYCTL_RX1_OFF|YK_RECOVERYCTL_RX2_OFF| \
        YK_RECOVERYCTL_TX1_OFF|YK_RECOVERYCTL_TX2_OFF)

/* Block 4 -- TX Arbiter MAC 1 */
#define YK_TXAR_TIMERINIT      0x0200
#define YK_TXAR_TIMERVAL       0x0204
#define YK_TXAR_LIMITINIT      0x0208
#define YK_TXAR_LIMITCNT       0x020C
#define YK_TXAR_COUNTERCTL     0x0210
#define YK_TXAR_COUNTERTST     0x0212
#define YK_TXAR_COUNTERSTS     0x0212

#define YK_TXARCTL_OFF          0x01
#define YK_TXARCTL_ON           0x02
#define YK_TXARCTL_RATECTL_OFF  0x04
#define YK_TXARCTL_RATECTL_ON   0x08
#define YK_TXARCTL_ALLOC_OFF    0x10
#define YK_TXARCTL_ALLOC_ON     0x20
#define YK_TXARCTL_FSYNC_OFF    0x40
#define YK_TXARCTL_FSYNC_ON     0x80

/* Block 7 -- PCI config registers */
#define YK_PCI_BASE     0x0380
#define YK_PCI_END      0x03FC

/* Block 8 and 9 RX queues */

#define YK_RXQ_BUFCNT          0x0400
#define YK_RXQ_BUFCTL          0x0402
#define YK_RXQ_NEXTDESC        0x0404
#define YK_RXQ_RXBUF_LO        0x0408
#define YK_RXQ_RXBUF_HI        0x040C
#define YK_RXQ_RXSTAT          0x0410
#define YK_RXQ_TIMESTAMP       0x0414
#define YK_RXQ_CSUM1           0x0418
#define YK_RXQ_CSUM2           0x041A
#define YK_RXQ_CSUM1_START     0x041C
#define YK_RXQ_CSUM2_START     0x041E
#define YK_RXQ_CURADDR_LO      0x0420
#define YK_RXQ_CURADDR_HI      0x0424
#define YK_RXQ_CURCNT_LO       0x0428
#define YK_RXQ_CURCNT_HI       0x042C
#define YK_RXQ_CURBYTES        0x0430
#define YK_RXQ_BMU_CSR         0x0434
#define YK_RXQ_WATERMARK       0x0438
#define YK_RXQ_FLAG            0x043A
#define YK_RXQ_TEST1           0x043C
#define YK_RXQ_TEST2           0x0440
#define YK_RXQ_TEST3           0x0444

#define YK_RXBMU_CLR_IRQ_ERR            0x00000001
#define YK_RXBMU_CLR_IRQ_EOF            0x00000002
#define YK_RXBMU_CLR_IRQ_EOB            0x00000004
#define YK_RXBMU_CLR_IRQ_PAR            0x00000008
#define YK_RXBMU_RX_START               0x00000010
#define YK_RXBMU_RX_STOP                0x00000020
#define YK_RXBMU_POLL_OFF               0x00000040
#define YK_RXBMU_POLL_ON                0x00000080
#define YK_RXBMU_TRANSFER_SM_RESET      0x00000100
#define YK_RXBMU_TRANSFER_SM_UNRESET    0x00000200
#define YK_RXBMU_DESCWR_SM_RESET        0x00000400
#define YK_RXBMU_DESCWR_SM_UNRESET      0x00000800
#define YK_RXBMU_DESCRD_SM_RESET        0x00001000
#define YK_RXBMU_DESCRD_SM_UNRESET      0x00002000
#define YK_RXBMU_SUPERVISOR_SM_RESET    0x00004000
#define YK_RXBMU_SUPERVISOR_SM_UNRESET  0x00008000
#define YK_RXBMU_PFI_SM_RESET           0x00010000
#define YK_RXBMU_PFI_SM_UNRESET         0x00020000
#define YK_RXBMU_FIFO_RESET             0x00040000
#define YK_RXBMU_FIFO_UNRESET           0x00080000
#define YK_RXBMU_DESC_RESET             0x00100000
#define YK_RXBMU_DESC_UNRESET           0x00200000
#define YK_RXBMU_SUPERVISOR_IDLE        0x01000000

#define YK_RXBMU_ONLINE         \
        (YK_RXBMU_TRANSFER_SM_UNRESET|YK_RXBMU_DESCWR_SM_UNRESET|       \
        YK_RXBMU_DESCRD_SM_UNRESET|YK_RXBMU_SUPERVISOR_SM_UNRESET|      \
        YK_RXBMU_PFI_SM_UNRESET|YK_RXBMU_FIFO_UNRESET|                  \
        YK_RXBMU_DESC_UNRESET)

#define YK_RXBMU_OFFLINE                \
        (YK_RXBMU_TRANSFER_SM_RESET|YK_RXBMU_DESCWR_SM_RESET|   \
        YK_RXBMU_DESCRD_SM_RESET|YK_RXBMU_SUPERVISOR_SM_RESET|  \
        YK_RXBMU_PFI_SM_RESET|YK_RXBMU_FIFO_RESET|              \
        YK_RXBMU_DESC_RESET)

/* Block 12 -- TX sync queue 1 */
/* Block 13 -- TX async queue 1 */
/* Block 14 -- TX sync queue 2 */
/* Block 15 -- TX async queue 2 */

/* Block 12 -- TX sync queue 1 */
#define YK_TXQS_BUFCNT         0x0600
#define YK_TXQS_BUFCTL         0x0602
#define YK_TXQS_NEXTDESC       0x0604
#define YK_TXQS_RXBUF_LO       0x0608
#define YK_TXQS_RXBUF_HI       0x060C
#define YK_TXQS_RXSTAT         0x0610
#define YK_TXQS_CSUM_STARTVAL  0x0614
#define YK_TXQS_CSUM_STARTPOS  0x0618
#define YK_TXQS_CSUM_WRITEPOS  0x061A
#define YK_TXQS_CURADDR_LO     0x0620
#define YK_TXQS_CURADDR_HI     0x0624
#define YK_TXQS_CURCNT_LO      0x0628
#define YK_TXQS_CURCNT_HI      0x062C
#define YK_TXQS_CURBYTES       0x0630
#define YK_TXQS_BMU_CSR        0x0634
#define YK_TXQS_WATERMARK      0x0638
#define YK_TXQS_FLAG           0x063A
#define YK_TXQS_TEST1          0x063C
#define YK_TXQS_TEST2          0x0640
#define YK_TXQS_TEST3          0x0644

/* Block 13 -- TX async queue 1 */
#define YK_TXQA_BUFCNT         0x0680
#define YK_TXQA_BUFCTL         0x0682
#define YK_TXQA_NEXTDESC       0x0684
#define YK_TXQA_RXBUF_LO       0x0688
#define YK_TXQA_RXBUF_HI       0x068C
#define YK_TXQA_RXSTAT         0x0690
#define YK_TXQA_CSUM_STARTVAL  0x0694
#define YK_TXQA_CSUM_STARTPOS  0x0698
#define YK_TXQA_CSUM_WRITEPOS  0x069A
#define YK_TXQA_CURADDR_LO     0x06A0
#define YK_TXQA_CURADDR_HI     0x06A4
#define YK_TXQA_CURCNT_LO      0x06A8
#define YK_TXQA_CURCNT_HI      0x06AC
#define YK_TXQA_CURBYTES       0x06B0
#define YK_TXQA_BMU_CSR        0x06B4
#define YK_TXQA_WATERMARK      0x06B8
#define YK_TXQA_FLAG           0x06BA
#define YK_TXQA_TEST1          0x06BC
#define YK_TXQA_TEST2          0x06C0
#define YK_TXQA_TEST3          0x06C4

#define YK_TXBMU_CLR_IRQ_ERR            0x00000001
#define YK_TXBMU_CLR_IRQ_EOF            0x00000002
#define YK_TXBMU_CLR_IRQ_EOB            0x00000004
#define YK_TXBMU_TX_START               0x00000010
#define YK_TXBMU_TX_STOP                0x00000020
#define YK_TXBMU_POLL_OFF               0x00000040
#define YK_TXBMU_POLL_ON                0x00000080
#define YK_TXBMU_TRANSFER_SM_RESET      0x00000100
#define YK_TXBMU_TRANSFER_SM_UNRESET    0x00000200
#define YK_TXBMU_DESCWR_SM_RESET        0x00000400
#define YK_TXBMU_DESCWR_SM_UNRESET      0x00000800
#define YK_TXBMU_DESCRD_SM_RESET        0x00001000
#define YK_TXBMU_DESCRD_SM_UNRESET      0x00002000
#define YK_TXBMU_SUPERVISOR_SM_RESET    0x00004000
#define YK_TXBMU_SUPERVISOR_SM_UNRESET  0x00008000
#define YK_TXBMU_PFI_SM_RESET           0x00010000
#define YK_TXBMU_PFI_SM_UNRESET         0x00020000
#define YK_TXBMU_FIFO_RESET             0x00040000
#define YK_TXBMU_FIFO_UNRESET           0x00080000
#define YK_TXBMU_DESC_RESET             0x00100000
#define YK_TXBMU_DESC_UNRESET           0x00200000
#define YK_TXBMU_SUPERVISOR_IDLE        0x01000000

#define YK_TXBMU_ONLINE         \
        (YK_TXBMU_TRANSFER_SM_UNRESET|YK_TXBMU_DESCWR_SM_UNRESET|       \
        YK_TXBMU_DESCRD_SM_UNRESET|YK_TXBMU_SUPERVISOR_SM_UNRESET|      \
        YK_TXBMU_PFI_SM_UNRESET|YK_TXBMU_FIFO_UNRESET|                  \
        YK_TXBMU_DESC_UNRESET)

#define YK_TXBMU_OFFLINE                \
        (YK_TXBMU_TRANSFER_SM_RESET|YK_TXBMU_DESCWR_SM_RESET|   \
        YK_TXBMU_DESCRD_SM_RESET|YK_TXBMU_SUPERVISOR_SM_RESET|  \
        YK_TXBMU_PFI_SM_RESET|YK_TXBMU_FIFO_RESET|              \
        YK_TXBMU_DESC_RESET)

/* Block 16 -- Receive RAMbuffer 1 */
/* Block 17 -- Receive RAMbuffer 2 */

#define YK_RXRB_START		0x0800
#define YK_RXRB_END		0x0804
#define YK_RXRB_WR_PTR		0x0808
#define YK_RXRB_RD_PTR		0x080C
#define YK_RXRB_UTHR_PAUSE	0x0810
#define YK_RXRB_LTHR_PAUSE	0x0814
#define YK_RXRB_UTHR_HIPRIO	0x0818
#define YK_RXRB_UTHR_LOPRIO	0x081C
#define YK_RXRB_PKTCNT		0x0820
#define YK_RXRB_LVL		0x0824
#define YK_RXRB_CTLTST		0x0828

#define YK_RXRB_LINK_OFFSET	0x80

#define YK_RXRB(p, r)	(((p)->ykLinkNum * YK_RXRB_LINK_OFFSET) + (r))

/* Block 20 -- Sync. Transmit RAMbuffer 1 */
/* Block 21 -- Async. Transmit RAMbuffer 1 */
/* Block 22 -- Sync. Transmit RAMbuffer 2 */
/* Block 23 -- Async. Transmit RAMbuffer 2 */

#define YK_TXRB_START		0x0A00
#define YK_TXRB_END		0x0A04
#define YK_TXRB_WR_PTR		0x0A08
#define YK_TXRB_RD_PTR		0x0A0C
#define YK_TXRB_PKTCNT		0x0A20
#define YK_TXRB_LVL		0x0A24
#define YK_TXRB_CTLTST		0x0A28

#define YK_TXRB_LINK_OFFSET	0x80

#define YK_TXRBS(p, r)	(((p)->ykLinkNum * YK_TXRM_LINK_OFFSET) + (r))
#define YK_TXRBA(p, r)	\
	(((p)->ykLinkNum * YK_TXRB_LINK_OFFSET) + (r) + YK_TXASYKC_OFFSET)

#define YK_RBCTL_RESET		0x00000001
#define YK_RBCTL_UNRESET	0x00000002
#define YK_RBCTL_OFFLINE	0x00000004
#define YK_RBCTL_ONLINE		0x00000008
#define YK_RBCTL_STORENFWD_OFF	0x00000010
#define YK_RBCTL_STORENFWD_ON	0x00000020

/* Block 24 RX MAC FIFO 1 */
/* Block 25 RX MAC FIFO 2 */
/* Block 26 TX MAC FIFO 1 */
/* Block 27 TX MAC FIFO 2 */

#define YK_RXMF_END		0x0C40
#define YK_RXMF_THRESHOLD	0x0C44
#define YK_RXMF_CTRL_TEST	0x0C48
#define YK_RXMF_WRITE_PTR	0x0C60
#define YK_RXMF_WRITE_LEVEL	0x0C68
#define YK_RXMF_READ_PTR	0x0C70
#define YK_RXMF_READ_LEVEL	0x0C78


#define YK_RFCTL_WR_PTR_TST_ON  0x00004000      /* Write pointer test on*/
#define YK_RFCTL_WR_PTR_TST_OFF 0x00002000      /* Write pointer test off */
#define YK_RFCTL_WR_PTR_STEP    0x00001000      /* Write pointer increment */
#define YK_RFCTL_RD_PTR_TST_ON  0x00000400      /* Read pointer test on */
#define YK_RFCTL_RD_PTR_TST_OFF 0x00000200      /* Read pointer test off */
#define YK_RFCTL_RD_PTR_STEP    0x00000100      /* Read pointer increment */
#define YK_RFCTL_RX_FIFO_OVER   0x00000040      /* Clear IRQ RX FIFO Overrun */
#define YK_RFCTL_FRAME_RX_DONE  0x00000010      /* Clear IRQ Frame RX Done */
#define YK_RFCTL_ONLINE		0x00000008      /* Operational mode on */
#define YK_RFCTL_OFFLINE	0x00000004      /* Operational mode off */
#define YK_RFCTL_UNRESET        0x00000002      /* MAC FIFO Reset Clear */
#define YK_RFCTL_RESET          0x00000001      /* MAC FIFO Reset Set */

#define YK_TXMF_END		0x0D40
#define YK_TXMF_THRESHOLD	0x0D44
#define YK_TXMF_CTRL_TEST	0x0D48
#define YK_TXMF_FIFO_FLUSHMSK	0x0D4C
#define YK_TXMF_WRITE_PTR	0x0D60
#define YK_TXMF_WRITE_SHADOW	0x0D64
#define YK_TXMF_WRITE_LEVEL	0x0D68
#define YK_TXMF_READ_PTR	0x0D70
#define YK_TXMF_READ_LEVEL	0x0D78

#define YK_TFCTL_WR_PTR_TST_ON  0x00004000      /* Write pointer test on*/
#define YK_TFCTL_WR_PTR_TST_OFF 0x00002000      /* Write pointer test off */
#define YK_TFCTL_WR_PTR_STEP    0x00001000      /* Write pointer increment */
#define YK_TFCTL_RD_PTR_TST_ON  0x00000400      /* Read pointer test on */
#define YK_TFCTL_RD_PTR_TST_OFF 0x00000200      /* Read pointer test off */
#define YK_TFCTL_RD_PTR_STEP    0x00000100      /* Read pointer increment */
#define YK_TFCTL_TX_FIFO_UNDER  0x00000040      /* Clear IRQ TX FIFO Under */
#define YK_TFCTL_FRAME_TX_DONE  0x00000020      /* Clear IRQ Frame TX Done */
#define YK_TFCTL_IRQ_PARITY_ER  0x00000010      /* Clear IRQ Parity Error */
#define YK_TFCTL_ONLINE		0x00000008      /* Operational mode on */
#define YK_TFCTL_OFFLINE	0x00000004      /* Operational mode off */
#define YK_TFCTL_UNRESET        0x00000002      /* MAC FIFO Reset Clear */
#define YK_TFCTL_RESET          0x00000001      /* MAC FIFO Reset Set */

#define YK_MF_LINK_OFFSET	0x80
#define YK_MF(p, r)	(((p)->ykLinkNum * YK_MF_LINK_OFFSET) + (r))

/* Block 28 -- Descriptor Poll Timer */
#define YK_DPT_INIT             0x0e00  /* Initial value 24 bits */
#define YK_DPT_TIMER            0x0e04  /* Mul of 78.12MHz clk (24b) */

#define YK_DPT_TIMER_CTRL       0x0e08  /* Timer Control 16 bits */
#define YK_DPT_TCTL_STOP        0x0001  /* Stop Timer */
#define YK_DPT_TCTL_START       0x0002  /* Start Timer */

#define YK_DPT_TIMER_TEST       0x0e0a  /* Timer Test 16 bits */
#define YK_DPT_TTEST_STEP       0x0001  /* Timer Decrement */
#define YK_DPT_TTEST_OFF        0x0002  /* Test Mode Off */
#define YK_DPT_TTEST_ON         0x0004  /* Test Mode On */

/* Block 30 GMAC and GPHY control registers */

#define YK_GMACCTL		0x0F00
#define YK_GPHYCTL		0x0F04
#define YK_MISCCTL		0x0F06
#define YK_GMACISR		0x0F08
#define YK_GMACIMR		0x0F0C
#define YK_LINKCTL		0x0F10

/* WOL registers */

#define YK_WOL_CTLSTS		0x0F20
#define YK_WOL_MATCHCTL		0x0F22
#define YK_WOL_MATCHRES		0x0F23
#define YK_WOL_MACADDR_LO	0x0F24
#define YK_WOL_MACADDR_HI	0x0F28
#define YK_WOL_PMEMATCH_ENB	0x0F2A
#define YK_WOL_ASFMATCH_ENB	0x0F2B
#define YK_WOL_PATTERNRD_PTR	0x0F2C
#define YK_WOL_PATTERN_LEN0	0x0F30
#define YK_WOL_PATTERN_LEN1	0x0F31
#define YK_WOL_PATTERN_LEN2	0x0F32
#define YK_WOL_PATTERN_LEN3	0x0F33
#define YK_WOL_PATTERN_LEN4	0x0F34
#define YK_WOL_PATTERN_LEN5	0x0F35
#define YK_WOL_PATTERN_LEN6	0x0F36
#define YK_WOL_PATTERN_CNT0	0x0F38
#define YK_WOL_PATTERN_CNT1	0x0F39
#define YK_WOL_PATTERN_CNT2	0x0F3A
#define YK_WOL_PATTERN_CNT3	0x0F3B
#define YK_WOL_PATTERN_CNT4	0x0F3C
#define YK_WOL_PATTERN_CNT5	0x0F3D
#define YK_WOL_PATTERN_CNT6	0x0F3E

#define YK_GMAC_LINK_OFFSET	0x80

#define YK_GMAC(p, r)	(((p)->ykLinkNum * YK_GMAC_LINK_OFFSET) + (r))

/* GMAC control register */

#define YK_GMACCTL_RESET	0x00000001
#define YK_GMACCTL_UNRESET	0x00000002
#define YK_GMACCTL_PAUSE_OFF	0x00000004
#define YK_GMACCTL_PAUSE_ON	0x00000008
#define YK_GMACCTL_LOOP_OFF	0x00000010
#define YK_GMACCTL_LOOP_ON	0x00000020

/* GPHY control register */

#define YK_GPHYCTL_SEL_BDT         0x10000000      /* Select Bidirectional xfer */
#define YK_GPHYCTL_INT_POL_HI      0x08000000      /* IRQ Polarity Active */
#define YK_GPHYCTL_75_OHM          0x04000000      /* Use 75 Ohm Termination */
#define YK_GPHYCTL_DIS_FC          0x02000000      /* Disable Auto Fiber/Copper */
#define YK_GPHYCTL_DIS_SLEEP       0x01000000      /* Disable Energy Detect */
#define YK_GPHYCTL_HWCFG_M_3       0x00800000      /* HWCFG_MODE[3] */  
#define YK_GPHYCTL_HWCFG_M_2       0x00400000      /* HWCFG_MODE[2] */  
#define YK_GPHYCTL_HWCFG_M_1       0x00200000      /* HWCFG_MODE[1] */
#define YK_GPHYCTL_HWCFG_M_0       0x00100000      /* HWCFG_MODE[0] */ 
#define YK_GPHYCTL_ANEG_0          0x00080000      /* ANEG[0] */
#define YK_GPHYCTL_ENA_XC          0x00040000      /* Enable MDI Crossover */
#define YK_GPHYCTL_DIS_125         0x00020000      /* Disable 125MHz Clock */
#define YK_GPHYCTL_ANEG_3          0x00010000      /* ANEG[3] */
#define YK_GPHYCTL_ANEG_2          0x00008000      /* ANEG[2] */
#define YK_GPHYCTL_ANEG_1          0x00004000      /* ANEG[1] */
#define YK_GPHYCTL_ENA_PAUSE       0x00002000      /* Enable Pause */
#define YK_GPHYCTL_PHYADDR_4       0x00001000      /* Bit 4 of Phy Addr */  
#define YK_GPHYCTL_PHYADDR_3       0x00000800      /* Bit 3 of Phy Addr */
#define YK_GPHYCTL_PHYADDR_2       0x00000400      /* Bit 2 of Phy Addr */
#define YK_GPHYCTL_PHYADDR_1       0x00000200      /* Bit 1 of Phy Addr */
#define YK_GPHYCTL_PHYADDR_0       0x00000100      /* Bit 0 of Phy Addr */ 
#define YK_GPHYCTL_UNRESET         0x00000002      /* Clear GPHY Reset */  
#define YK_GPHYCTL_RESET           0x00000001      /* Set GPHY Reset */

#define YK_GPHYCTL_COPPER	(YK_GPHYCTL_HWCFG_M_0 | YK_GPHYCTL_HWCFG_M_1 | \
				 YK_GPHYCTL_HWCFG_M_2 | YK_GPHYCTL_HWCFG_M_3 )
#define YK_GPHYCTL_FIBER	YK_GPHYCTL_HWCFG_M_0 | YK_GPHYCTL_HWCFG_M_1 | \
				YK_GPHYCTL_HWCFG_M_2 )

#define YK_GPHYCTL_ANEG_ALL	(YK_GPHYCTL_ANEG_0 | YK_GPHYCTL_ANEG_1 | \
				 YK_GPHYCTL_ANEG_2 | YK_GPHYCTL_ANEG_3 )

#define YK_GPHYCTL_10_HALF	0
#define YK_GPHYCTL_10_FULL	YK_GPHYCTL_ANEG_0
#define YK_GPHYCTL_100_HALF	YK_GPHYCTL_ANEG_1
#define YK_GPHYCTL_100_FULL	(YK_GPHYCTL_ANEG_0|YK_GPHYCTL_ANEG_1)

#define YK_GPHYCTL_1000_HALF	YK_GPHYCTL_ANEG_2
#define YK_GPHYCTL_1000_FULL	YK_GPHYCTL_ANEG_3

#define YK_GPHYCTL_FORCE_MASTER	0
#define YK_GPHYCTL_FORCE_SLAVE	YK_GPHYCTL_ANEG_0
#define YK_GPHYCTL_PREF_MASTER	YK_GPHYCTL_ANEG_1
#define YK_GPHYCTL_PREF_SLAVE	(YK_GPHYCTL_ANEG_0|YK_GPHYCTL_ANEG_1)

/* Misc control register */

#define YK_MISCCTL_PHYBURNIN	0x00C0
#define YK_MISCCTL_SPI_LOCK	0x0100

/* GMAC ISR (and IMR) register */

#define YK_GMACISR_RXOK		0x01
#define YK_GMACISR_RXOFLOW	0x02
#define YK_GMACISR_TXOK		0x04
#define YK_GMACISR_TXUFLOW	0x08
#define YK_GMACISR_TXCNT_OFLOW	0x10
#define YK_GMACISR_RXCNT_OFLOW	0x20

/* Link control register */

#define YK_LINKCTL_RESET	0x0001
#define YK_LINKCTL_UNRESET	0x0002

/* PCI config space mirror */

#define YK_PCI_CFG		0x1c00

/*
 * DMA Data structures.
 * There are two kinds of DMA descriptor structures; TX and RX. Both
 * are 32 bytes in size and contain 64-bit wide buffer address fields.
 * The 'next' pointers are all 32 bits however, which implies that
 * the descriptors should all be allocated in the same 4GB range.
 */

/* RX queue descriptor data structure */
typedef struct yk_rx_desc
    {
    volatile UINT32	yk_ctl;
    volatile UINT32	yk_next;
    volatile UINT32	yk_data_lo;
    volatile UINT32	yk_data_hi;
    volatile UINT32	yk_xmac_rxstat;
    volatile UINT32	yk_timestamp;
    volatile UINT16	yk_csum2;
    volatile UINT16	yk_csum1;
    volatile UINT16	yk_csum2_start;
    volatile UINT16	yk_csum1_start;
    } YK_RX_DESC;

#define YK_OPCODE_DEFAULT       0x00550000
#define YK_OPCODE_CSUM          0x00560000
 
#define YK_RXCTL_LEN            0x0000FFFF
#define YK_RXCTL_OPCODE         0x00FF0000
#define YK_RXCTL_TSTAMP_VALID   0x01000000
#define YK_RXCTL_STATUS_VALID   0x02000000
#define YK_RXCTL_DEV0           0x04000000
#define YK_RXCTL_EOF_INTR       0x08000000
#define YK_RXCTL_EOB_INTR       0x10000000
#define YK_RXCTL_LASTFRAG       0x20000000
#define YK_RXCTL_FIRSTFRAG      0x40000000
#define YK_RXCTL_OWN            0x80000000

#define YK_RXSTAT       \
        (YK_OPCODE_DEFAULT|YK_RXCTL_EOF_INTR|YK_RXCTL_LASTFRAG| \
         YK_RXCTL_FIRSTFRAG|YK_RXCTL_OWN)

typedef struct yk_tx_desc
    {
    volatile UINT32	yk_ctl;
    volatile UINT32	yk_next;
    volatile UINT32	yk_data_lo;
    volatile UINT32	yk_data_hi;
    volatile UINT32	yk_xmac_txstat;
    volatile UINT16	yk_rsvd0;
    volatile UINT16	yk_csum_startval;
    volatile UINT16	yk_csum_startpos;
    volatile UINT16	yk_csum_writepos;
    volatile UINT32	yk_rsvd1;
    } YK_TX_DESC;

#define YK_TXCTL_LEN            0x0000FFFF
#define YK_TXCTL_OPCODE         0x00FF0000
#define YK_TXCTL_SW             0x01000000
#define YK_TXCTL_NOCRC          0x02000000
#define YK_TXCTL_STORENFWD      0x04000000
#define YK_TXCTL_EOF_INTR       0x08000000
#define YK_TXCTL_EOB_INTR       0x10000000
#define YK_TXCTL_LASTFRAG       0x20000000
#define YK_TXCTL_FIRSTFRAG      0x40000000
#define YK_TXCTL_OWN            0x80000000

#define YK_TXSTAT       \
        (YK_OPCODE_DEFAULT|YK_TXCTL_EOF_INTR|YK_TXCTL_LASTFRAG|YK_TXCTL_OWN)

#define YK_RXBYTES(x)           (x) & 0x0000FFFF;
#define YK_TXBYTES              YK_RXBYTES


#define YK_TX_LIST_CNT	128
#define YK_RX_LIST_CNT	128

#define YK_INC_DESC(x, y)	(x) = (((x) + 1) % y)

#define YK_ADJ(x)	m_adj((x), 2)

#define YK_MTU          1500
#define YK_JUMBO_MTU    9000
#define YK_CLSIZE	1536
#define YK_MAXFRAG	16
#define YK_MAX_RX	16
#define YK_NAME		"yk"
#define YK_TIMEOUT	10000
#define YK_INTRS	(YK_ISR_RX1_EOF|YK_ISR_TX1_S_EOF|YK_ISR_MAC1)

#define YK_ADDR_LO(y)  ((UINT32)((UINT64)(y) & 0xFFFFFFFF))
#define YK_ADDR_HI(y)  ((UINT32)(((UINT64)(y) >> 32) & 0xFFFFFFFF))

#define YK_RXINTRS	YK_ISR_RX1_EOF
#define YK_TXINTRS	YK_ISR_TX1_S_EOF
#define YK_LINKINTRS	YK_ISR_MAC1

/*
 * Private adapter context structure.
 */

typedef struct yk_drv_ctrl
    {
    END_OBJ		ykEndObj;
    VXB_DEVICE_ID	ykDev;
    VXB_DEVICE_ID	ykMiiParent;
    void *		ykBar;
    void *		ykMacBar;
    void *		ykHandle;
    void *		ykMuxDevCookie;

    JOB_QUEUE_ID	ykJobQueue;
    QJOB		ykIntJob;
    atomicVal_t		ykIntPending;

    QJOB		ykRxJob;
    atomicVal_t		ykRxPending;

    QJOB		ykTxJob;
    atomicVal_t		ykTxPending;
    UINT8		ykTxCur;
    UINT8		ykTxLast;
    volatile BOOL	ykTxStall;

    BOOL		ykPolling;
    M_BLK_ID		ykPollBuf;
    UINT32		ykIntMask;
    UINT32		ykIntrs;

    UINT8		ykAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	ykCaps;

    END_IFDRVCONF	ykEndStatsConf;
    END_IFCOUNTERS	ykEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*ykMediaList;
    END_ERR		ykLastError;
    UINT32		ykCurMedia;
    UINT32		ykCurStatus;
    VXB_DEVICE_ID	ykMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	ykParentTag;

    VXB_DMA_TAG_ID	ykRxDescTag;
    VXB_DMA_MAP_ID	ykRxDescMap;
    YK_RX_DESC		*ykRxDescMem;

    VXB_DMA_TAG_ID	ykTxDescTag;
    VXB_DMA_MAP_ID	ykTxDescMap;
    YK_TX_DESC		*ykTxDescMem;

    UINT32              ykTxProd;
    UINT32              ykTxCons;
    UINT32              ykTxFree;
    UINT32		ykRxIdx;
    UINT32		ykRxProdIdx;

    VXB_DMA_TAG_ID	ykMblkTag;

    VXB_DMA_MAP_ID	ykRxMblkMap[YK_RX_LIST_CNT];
    VXB_DMA_MAP_ID	ykTxMblkMap[YK_TX_LIST_CNT];

    M_BLK_ID		ykRxMblk[YK_RX_LIST_CNT];
    M_BLK_ID		ykTxMblk[YK_TX_LIST_CNT];

    SEM_ID		ykDevSem;

    UINT32		ykDiscards;

    UINT8		ykChipId;
    UINT8		ykChipVer;
    UINT32		ykRamSize;
    UINT32		ykRxRamStart;
    UINT32		ykRxRamEnd;
    UINT32		ykTxRamStart;
    UINT32		ykTxRamEnd;

    int			ykMaxMtu;
    } YK_DRV_CTRL;

#define YK_BAR(p)   ((YK_DRV_CTRL *)(p)->pDrvCtrl)->ykBar
#define YK_HANDLE(p)   ((YK_DRV_CTRL *)(p)->pDrvCtrl)->ykHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (YK_HANDLE(pDev), (UINT32 *)((char *)YK_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (YK_HANDLE(pDev),                             \
        (UINT32 *)((char *)YK_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (YK_HANDLE(pDev), (UINT16 *)((char *)YK_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (YK_HANDLE(pDev),                             \
        (UINT16 *)((char *)YK_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (YK_HANDLE(pDev), (UINT8 *)((char *)YK_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (YK_HANDLE(pDev),                              \
        (UINT8 *)((char *)YK_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define GM_BAR(p)   ((YK_DRV_CTRL *)(p)->pDrvCtrl)->ykMacBar

#define GMAC_READ_4(pDev, addr)                                  \
    vxbRead32 (YK_HANDLE(pDev), (UINT32 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (YK_HANDLE(pDev),                             \
        (UINT32 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_READ_2(pDev, addr)                                  \
    vxbRead16 (YK_HANDLE(pDev), (UINT16 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (YK_HANDLE(pDev),                             \
        (UINT16 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_READ_1(pDev, addr)                                  \
    vxbRead8 (YK_HANDLE(pDev), (UINT8 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (YK_HANDLE(pDev),                              \
        (UINT8 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_SETBIT_1(pDev, offset, val)          \
        GMAC_WRITE_1(pDev, offset, GMAC_READ_1(pDev, offset) | (val))

#define GMAC_CLRBIT_1(pDev, offset, val)          \
        GMAC_WRITE_1(pDev, offset, GMAC_READ_1(pDev, offset) & ~(val))

#define GMAC_SETBIT_2(pDev, offset, val)          \
        GMAC_WRITE_2(pDev, offset, GMAC_READ_2(pDev, offset) | (val))

#define GMAC_CLRBIT_2(pDev, offset, val)          \
        GMAC_WRITE_2(pDev, offset, GMAC_READ_2(pDev, offset) & ~(val))

#define GMAC_SETBIT_4(pDev, offset, val)          \
        GMAC_WRITE_4(pDev, offset, GMAC_READ_4(pDev, offset) | (val))

#define GMAC_CLRBIT_4(pDev, offset, val)          \
        GMAC_WRITE_4(pDev, offset, GMAC_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCmvYukonIIVxbEndh */
