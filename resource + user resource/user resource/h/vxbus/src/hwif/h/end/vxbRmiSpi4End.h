/* vxbRmiSpi4End.h - header file for SPI-4.2 to GE VxBus END driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,10nov08,l_z  written
*/

#ifndef __INCvxbRmiSpi4Endh
#define __INCvxbRmiSpi4Endh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void xlrSpi4Register (void);

#ifndef BSP_VERSION

#define XLR_INC_DESC(x, y) (x) = ((x + 1) & (y - 1))

#define XLR_MAXFRAG     16
#define XLR_MAX_RX      96

#define SPI4_MAX_CHANNEL 16
#define XLR_NUM_PORTS    10

#define XLR_MTU          1500
#define XLR_JUMBO_MTU    9000
#define XLR_CLSIZE       1536

#define XLR_NAME        "spi"
#define XLR_TIMEOUT     100000

#define XLR_INTRS       (XLR_RXINTRS|XLR_TXINTRS)

#define XLR_RXINTRS \
            (XLRA_ISR_C0EARLYFULL|XLRA_ISR_C1EARLYFULL| \
             XLRA_ISR_C2EARLYFULL|XLRA_ISR_C3EARLYFULL)

#define XLR_TXINTRS         (XLRA_ISR_FREEOEARLYFULL)

#define XLR_ETHER_ALIGN     2

#define XLR_RX_DESC_CNT     256
#define XLR_TX_DESC_CNT     256

#define XLR_RX_BUCKET       (3 + (pDev->unitNumber * 2))
#define XLR_TX_BUCKET       (4 + (pDev->unitNumber * 2))
#define XLR_ERR_BUCKET      (4 + (pDev->unitNumber * 2))

#define XLR_RXBMASK         (1 << XLR_RX_BUCKET)
#define XLR_TXBMASK         ((1 << XLR_TX_BUCKET) | (1 << XLR_ERR_BUCKET))
#define XLR_BMASK           (XLR_TXBMASK|XLR_RXBMASK)

/* SPI4.2 register */

#define SPI4_TX_CAL_LEN_X       (0x00 * 4)  /* Length of the transmit calendar sequence */
#define SPI4_TX_CAL_M_X         (0x04 * 4)  /* Number repetitions of the transmit FIFO status */
#define SPI4_TX_CAL_X           (0x08 * 4)  /* Port address at transmit calendar */
#define SPI4_RX_CAL_LEN_X       (0x18 * 4)  /* Length of the receive calendar sequence */
#define SPI4_RX_CAL_M_X         (0x1c * 4)  /* Number repetitions of the receive FIFO status */
#define SPI4_RX_CAL_X           (0x20 * 4)  /* Port address at receive calendar */
#define SPI4_TX_MAXBURST1       (0x30 * 4)  /* Maximum number of 16-byte blocks */
#define SPI4_TX_MAXBURST2       (0x34 * 4)  /* Maximum number of 16-byte blocks */
#define SPI4_RX_MAXBURST1       (0x38 * 4)  /* Maximum number of 16-byte blocks */
#define SPI4_RX_MAXBURST2       (0x3c * 4)  /* Maximum number of 16-byte blocks */
#define SPI4_CNTRL_REG          (0x6c * 4)  /* Control register for the SPI-4.2 protocol manager.*/
#define SPI4_F_ALPHA            (0x50 * 4)  /* Number of repetitions of the status path training sequences */
#define SPI4_RX_FIFO_DEPTH_I    (0x88 * 4)
#define SPI4_RX_FIFO_BASE_I     (0x90 * 4)
#define SPI4_TX_FIFO_DEPTH_I    (0x8c * 4)
#define SPI4_TX_FIFO_BASE_I     (0x94 * 4)
#define SPI4_BE_BURS            (0x78 * 4)   /* The maximum length of the burst on the SPI-4.2 interface */

/* glue logic registers */

#define DESC_PKT_CTRL_1         (0xa9 * 4)
#define DESC_PKT_CTRL_2         (0xaa * 4)

#define TX_RESET                0x00000001
#define TX_ENABLE               0x00000002
#define RX_RESET                0x00000004
#define RX_ENABLE               0x00000008
#define RX_CAL_Y                0x00000010
#define USR_SHAL_LPB            0x00000020
#define USR_DEEP_LPB            0x00000040
#define SPI_SHAL_LPB            0x00000080
#define SPI_DEP_LPB             0x00000100
#define DDL_ENABLE              0x00000200
#define FIFO_SW_RESET           0x00000400
#define RX_TRAIN_RESET          0x00000800
#define RX_TRAIN_LOS            0x00001000
#define TX_FRM_ERR_EN           0x00002000
#define MORE_TRN_EN             0x00004000
#define ERL_TRN_EN              0x00008000
#define SYNC_PAT_EN             0x00010000

#define TX_RX_CAL_LEN           XLR_NUM_PORTS
#define TX_RX_CAL_M_X           0x1

#define TX_MAXBURST1            0x8
#define TX_MAXBURST2            0x8
#define RX_MAXBURST1            0x8
#define RX_MAXBURST2            0x8

#define TX_FIFO_SIZE            0x8
#define RX_FIFO_SIZE            0x20

#define BE_BURS                 0x4

#define SPI4_TX_STATUS_TX_SYNC  0x4
#define SPI4_RX_STATUS_RX_SYNC  0x2

#define SPI4_CNTRL_RX_TRAIN_RESET 0x00000800 /* Receive SPI4 sends training */
#define SPI4_CNTRL_FIFO_SW_RESET  0x00000400 /* Reset the FIFO */
#define SPI4_CNTRL_DDL_ENABLE     0x00000200 /* Dynamic deskew logic enable */
#define SPI4_CNTRL_USR_DEEP_LPB   0x00000040 /* User side deep loop back */
#define SPI4_CNTRL_RX_ENABLE      0x00000008 /* Receive enable */
#define SPI4_CNTRL_RX_SW_RESET    0x00000004 /* Rx S/W reset */
#define SPI4_CNTRL_TX_ENABLE      0x00000002 /* Tx enable */
#define SPI4_CNTRL_TX_SW_RESET    0x00000001 /* Tx S/W reset */

/*
 * The SPI4A/B have the following bucket IDs allocated to them in
 * the fast messaging network (FNM).
 */

#define SPI4_MSG_STID_XGMIIA    64  /* TX */
#define SPI4_MSG_STID_XGMIIB    80  /* TX */
#define SPI4_MSG_STID_XSA_FREE  113 /* Free descriptors */
#define SPI4_MSG_STID_XSB_FREE  115 /* Free descriptors */

/*
 * Message are allowed to have a software code field. We currently
 * don't use this, but we initialize it for completeness.
 */

#define SPI4_MSG_CODE_RX        0xAB
#define SPI4_MSG_CODE_TX        0xCD

/* Meigs-II Ethernet MAC defines */

/* Meigs-II Block IDs */

#define MDL_MAC                 1   /* Tri-speed Ethernet MACs and 10-GbE MAC */
#define MDL_FIFO                2   /* Block FIFOS */
#define MDL_MIIM                3   /* MII Management interfaces */
#define MDL_STAT                4   /* Statistics */
#define MDL_SPI4                5   /* SPI-4.2 interface */
#define MDL_SYSTEM              7   /* System */

/* Meigs-II Sub Block IDs */

/* System Block Registers (Block ID: SYSTEM) */

#define SMDL_SYS_CTRL           0x0F  /* Systme register and CPU interface */
#define SMDL_SYS_AGGREGATOR     0x01  /* Aggregator */
#define SMDL_SYS_BIST           0x02  /* RAM BIST */

#define SMDL_SPI4               0x0  /* SPI4 host interface */

#define SMDL_FIFO_INGRESS       0x0  /* Ingress FIFO */
#define SMDL_FIFO_EGRESS        0x1  /* Egress FIFO */

/* MII Management Block Registers (Block ID: MIIM) */

#define MII_M_STAUS             0x00 /* MII Management status register */
#define MII_M_CMD               0x01 /* MII Management command register */
#define MII_M_DATA              0x02 /* MII Management data register */
#define MII_M_PRESCALE          0x03 /* MII Management MIIM_PRESCALE register */

#define MII_M_STAUS_MIIM_BUSY   0x10 /* MII interface busy */
#define MII_M_STAUS_OPR_PEND    0x08 /* MII operation pending */
#define MII_M_STAUS_OPR_PROG    0x04 /* MII operation in progress */

#define MII_M_CMD_READ          0x02 /* MII read phy */
#define MII_M_CMD_WRITE         0x01 /* MII write phy */

#define MII_M_MII_MODE          0x01 /* MII mode */
#define MII_M_MDIO_MODE         0x00 /* MDIO mode */

#define CHIP_ID                 0x00 /* CHIP Id address */
#define SW_RESET                0x02 /* Global soft reset */
#define IFACE_MODE              0x07 /* Interface mode */
#define CRC_CFG                 0x0B /* crc config */
#define SI_INSERT_BYTES         0x0F /* SI insert bytes */
#define SI_TRANSFER_SEL         0x18 /* SI transfer select */
#define PLL_CLK_SPEED           0x19 /* Clock speed selection */
#define CPU_TRANSFER_SEL        0x20 /* CPU transfer select */
#define LOCAL_DATA              0xFE /* Local CPU Data */
#define LOCAL_STATUS            0xFF /* Local CPU Status */

#define SW_RESET_RST0           0x00000001 /* Global reset0 */
#define SW_RESET_RST1           0x80000000 /* Global reset1 */

#define TRANSFER_SEL_LE         0x81818181
#define TRANSFER_SEL_BE         0x99999999

/* SPI-4.2 Host Interface Block Registers (Block ID: SPI4) */

#define SPI4_MISC               0x00 /* SPI-4.2 Miscellaneous Register */
#define SPI4_ING_SETUP0         0x02 /* Ingress status channel setup */
#define SPI4_ING_SETUP1         0x03 /* Ingress data training setup */
#define SPI4_ING_SETUP2         0x04 /* Ingress Data Burst Sizes Setup */
#define SPI4_ERG_SETUP0         0x05 /* Egress status channel setup */

#define SPI4_MISC_CML_EN        0x00000001 /* CML enable */
#define SPI4_MISC_CML_RES       0x00000008 /* CML reset */
#define SPI4_MISC_SD            0x00040000 /* swap bits in each byte */

#define SPI4_MISC_EGRCLK        0x20000000 /* Egress clock clear */
#define SPI4_MISC_INGCLK        0x40000000 /* Ingress clock clear */
#define SPI4_MISC_RSCLK         0x80000000 /* Ingress status clock clear */

#define SPI4_ING_SETUP0_PORT_MSK 0x3FF   /* All port mask */
#define SPI4_INT_SETUP0_CAL_M    0x1000  /* Calender M */

/* FIFO Block Registers (Block ID: FIFO) */

#define FIFO_MODE_TEST          0x00 /* Mode and Test */
#define FIFO_BUF_TOP_BOTTOM     0x10 /* FIFO buffer top and bottrom */
#define FIFO_WRITE_PONITER      0x20 /* FIFO write pointer */
#define FIFO_READ_POINTER       0x30 /* FIFO read pointer */
#define FIFO_FLO_WATERMARK      0x40 /* FIFO flow control watermarks */
#define FIFO_THESHOLD           0x50 /* FIFO cut through threshold */
#define FIFO_DROP_CNT           0x60 /* FIFO Drop & CRC Error Counter */
#define FIFO_DEBUG_BUF_CNT      0x70 /* Input Side Debug Counter */

#define FIFO_ING_CONTROL        0x0F /* Ingress control */
#define FIFO_EGR_CONTROL        0x0F /* Egress control */

#define FIFO_ING_CONTROL_OUT_PORT_OFFSET    0x0 /* Transmitting to SPI-4.2 */
#define FIFO_ING_CONTROL_OUT_PORT_OFFSET_LA 0xA0000000 /* In link aggregation mode */
#define FIFO_ING_CONTROL_INP_PORT_OFFSET 0x0A000000 /* Receiveing from tri-speed MACs */
#define FIFO_ING_CONTROL_FAIL_IGN 0x00040000 /* Fail ignore */
#define FIFO_ING_CONTROL_MUX      0x00010000 /* Aggregation mode (1G <-> 10G) */
#define FIFO_ING_CONTROL_TC       0x00004000 /* Turbo change */
#define FIFO_ING_CONTROL_LE       0x00002000 /* Length enqueue mode */
#define FIFO_ING_CONTROL_IGI      0x00001000 /* Ignore inhibit */
#define FIFO_ING_CONTROL_IPT      0x00000800 /* Inhibit pass through */
#define FIFO_ING_CONTROL_RT       0x00000400 /* Replay timer enable */
#define FIFO_ING_CONTROL_PH       0x00000200 /* Preamble header */
#define FIFO_ING_CONTROL_NH       0x00000100 /* Normalized header */
#define FIFO_ING_CONTROL_SS       0x00000080 /* Suppress slow enable */
#define FIFO_ING_CONTROL_CLR      0x00000040 /* Clear all FIFO buffers */
#define FIFO_ING_CONTROL_M10G     0x00000020 /* Mode 10GbE <-> SPI-4.2 */
#define FIFO_ING_CONTROL_CM       0x00000010 /* Burst interleaving */
#define FIFO_ING_CONTROL_BURST_LEN 0x00000004 /* Number of 32B entries */

#define FIFO_EGR_CONTROL_OUT_PORT_OFFSET_SPI_10G  0x0        /* SPI4.2 to 10GbE MAC */
#define FIFO_EGR_CONTROL_OUT_PORT_OFFSET_SPI_1G   0xA0000000 /* SPI4.2 to 1GbE MAC */
#define FIFO_EGR_CONTROL_INP_PORT_OFFSET_LA 0x0A000000 /* In link aggregation mode */
#define FIFO_EGR_CONTROL_FAIL_IGN 0x00040000 /* Fail ignore */
#define FIFO_EGR_CONTROL_MUX_10G  0x00000000 /* 10GbE mode */
#define FIFO_EGR_CONTROL_MUX_1G   0x00010000 /* 1GbE mode */
#define FIFO_EGR_CONTROL_MUX_A    0x00020000 /* Aggregation mode */
#define FIFO_EGR_CONTROL_MFE      0x00008000 /* 1GbE external flow control pass through enable */
#define FIFO_EGR_CONTROL_TC       0x00004000 /* Turbo change */
#define FIFO_EGR_CONTROL_LE       0x00002000 /* Length enqueue mode */
#define FIFO_EGR_CONTROL_IGI      0x00001000 /* Ignore inhibit */
#define FIFO_EGR_CONTROL_RT       0x00000400 /* Replay timer enable */
#define FIFO_EGR_CONTROL_PH       0x00000200 /* Preamble header */
#define FIFO_EGR_CONTROL_NH       0x00000100 /* Normalized header */
#define FIFO_EGR_CONTROL_SS       0x00000080 /* Suppress slow enable */
#define FIFO_EGR_CONTROL_CLR      0x00000040 /* Clear all FIFO buffers */
#define FIFO_EGR_CONTROL_M10G     0x00000020 /* Only output from FIFO buffer 0 */
#define FIFO_EGR_CONTROL_CM       0x00000010 /* Burst interleaving */
#define FIFO_EGR_CONTROL_BURST_LEN 0x00000001 /* Number of 32B entries */

#define FIFO_MODE_TEST_NORMAL              0x0
#define FIFO_MODE_TEST_RX_STOP             0x1
#define FIFO_MODE_TEST_CLR                 0x2
#define FIFO_MODE_TEST_TX_STOP             0x3
#define FIFO_MODE_TEST_MASK                0xF

/* Tri-Speed Ethernet MAC Block Registers (Block ID: MAC) */

#define MAC_MODE_CFG            0x00 /* Miscellaneous 10G */
#define MAC_PAUSE               0x01 /* Pause register */
#define MAC_MAX_LEN             0x02 /* Max length */
#define MAC_HIGH_ADDR           0x03 /* Mac address high */
#define MAC_LOW_ADDR            0x04 /* Mac address low */
#define MAC_NORMALIZER          0x05 /* Normalizer */
#define MAC_PCS_STATUS          0x06 /* PCS status */
#define MAC_PCS_STATUS_DBG      0x07 /* PCS status debug*/
#define MAC_PCS_CTRL            0x08 /* PCS control */
#define MAC_PCS_CONFIG          0x09 /* PCS config */
#define MAC_DEV_SETUP           0x0B /* DEV setup */
#define MAC_DENORMALIZER        0x15 /* Fame denormalization */
#define MAC_TX_IFG              0x18 /* Tx Inter Frame Gap configuration */
#define MAC_ADV_HDX_CFG         0x19 /* Advance Half Duplex configuration */

#define MAC_MODE_CFG_LEN_DROP   0x8000 /* Length drop bit */
#define MAC_MODE_CFG_VLAN_AWR   0x0010 /* VLAN aware bit */
#define MAC_MODE_CFG_MODE_10M_100M_FDX 0x0004 /* 10/100 Mbps full-deplex mode */
#define MAC_MODE_CFG_MODE_1G    0x000C /* 1 Gbps full-deplex mode */
#define MAC_MODE_CFG_RX_EN      0x0002 /* Enable recevier */
#define MAC_MODE_CFG_TX_EN      0x0001 /* Enable transmitter */
#define MAC_MODE_CFG_VALUE      0x0544

/* Recommended values for 1st part of Rx to Tx inter-frame gap */

#define MAC_MODE_CFG_IFG1_MASK                   (0xF << 6)
#define MAC_MODE_CFG_IFG1_1G                     (5 << 6)
#define MAC_MODE_CFG_IFG1_100M_HDX               (6 << 6)
#define MAC_MODE_CFG_IFG1_100M_FDX               (7 << 6)
#define MAC_MODE_CFG_IFG1_10M_HDX                (6 << 6)
#define MAC_MODE_CFG_IFG1_10M_FDX                (7 << 6)

/* Recommended values for 2nd part of Rx to Tx inter-frame gap */

#define MAC_MODE_CFG_IFG2_MASK                   (0xF << 10)
#define MAC_MODE_CFG_IFG2_1G                     (1 << 10)
#define MAC_MODE_CFG_IFG2_100M_HDX               (8 << 10)
#define MAC_MODE_CFG_IFG2_100M_FDX               (11 << 10)
#define MAC_MODE_CFG_IFG2_10M_HDX                (8 << 10)
#define MAC_MODE_CFG_IFG2_10M_FDX                (11 << 10)

#define MAC_PAUSE_TX_PAUSE_EN     0x00080000 /* Tx pause enabled */
#define MAC_PAUSE_RX_PAUSE_EN     0x00040000 /* Rx pause enabled */
#define MAC_PAUSE_TX_PAUSE_XONOFF 0x00020000 /* Tx pause zero on de-assert */
#define MAC_PAUSE_PAUSE_EN        0x00010000 /* Enables flow control in both directions */

#define MAC_DEV_SETUP_LI_DISEN    0x0080     /* LI bit is disabled */
#define MAC_DEV_SETUP_RGMII       0x0010     /* RGMII enabled */
#define MAC_DEV_SETUP_MODE_MASK   0x00E0     /* mode mask */
#define MAC_DEV_SETUP_MODE_10M    0x0002     /* 10 Mbps (RGMII/MII) */
#define MAC_DEV_SETUP_MODE_100M   0x0004     /* 100 Mbps (RGMII/MII) */
#define MAC_DEV_SETUP_MODE_1G     0x0006     /* 1 Gbps (RGMII/MII) */
#define MAC_DEV_SETUP_MODE_1G_SD  0x0002     /* 1 Gbps (SerDes) */
#define MAC_DEV_SETUP_RST         0x0001     /* Master reset for MAC */

#define MAC_NORMALIZER_NO_CRC     0x0200     /* CRC updateing disabled */
#define MAC_NORMALIZER_MPLS_NORM  0x0100     /* MPLS normilize enabled */
#define MAC_NORMALIZER_DROP_PAUSE 0x0040     /* Drop pause */
#define MAC_NORMALIZER_DROP_CTLR  0x0020     /* Drop control */
#define MAC_NORMALIZER_IPV6       0x0010     /* IPv6 after MPLS aware */
#define MAC_NORMALIZER_IPV4       0x0008     /* IPv4 after MPLS aware */
#define MAC_NORMALIZER_NLE        0x0004     /* Non-Length enqueue mode */
#define MAC_NORMALIZER_INS_HEAD   0x0002     /* Insert normalized header */
#define MAC_NORMALIZER_PRE        0x0001     /* Insert preamble header */

#define MAC_DENORMALIZER_CRC_UPD  0x0020     /* Update CRC (egress only) */
#define MAC_DENORMALIZER_CRC_ADD  0x0010     /* Add CRC (egress only) */

/* Statistic Counters Block Registers (Block ID: STAT) */

#define STAT_RX_IN_BYTES          0x00
#define STAT_RX_SYMBOL_CARRIER    0x01
#define STAT_RX_PAUSE             0x02
#define STAT_RX_UNSUP_OPCODE      0x03
#define STAT_RX_OK_BYTES          0x04
#define STAT_RX_BAD_BYTES         0x05
#define STAT_RX_UNICAST           0x06
#define STAT_RX_MULTICAST         0x07
#define STAT_RX_BROADCAST         0x08

#define STAT_TX_OUT_BYTES         0x18
#define STAT_TX_PAUSE             0x19
#define STAT_TX_OK_BYTES          0x1A
#define STAT_TX_UNICAST           0x1B
#define STAT_TX_MULTICAST         0x1C
#define STAT_TX_BROADCAST         0x1D

#define VSC_TIMEOUT               1000000

#define MAX_FRAME_LENGTH           1518  /* 16-bit value */

#define MAX_INGRESS_FIFO_SIZE      (3 * 128 * 1024)
#define MAX_EGRESS_FIFO_SIZE       (128 * 1024)

#define FIFO_INGRESS_STEP_SIZE     ((MAX_INGRESS_FIFO_SIZE >> 11) / XLR_NUM_PORTS)
#define FIFO_EGRESS_STEP_SIZE      ((MAX_EGRESS_FIFO_SIZE >> 11) / XLR_NUM_PORTS)

#define MDL_OFFSET              12   /* Module offset bit */
#define SMDL_OFFSET             8    /* Sub module offset bit */
#define REG_OFFSET              1    /* Register offset bit */

#define VSC_REG_WIDTH           4    /* Register width 4 bytes */

#define MK_REG_OFFSET(mdl, smdl, reg) (((mdl << MDL_OFFSET) +   \
                                        (smdl << SMDL_OFFSET) + \
                                        (reg)) * VSC_REG_WIDTH)

#define WORD_SWAP(data)               (((data & 0xFF00FF00) >> 8) | \
                                       ((data & 0x00FF00FF) << 8))

typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define xlrEndPoolTupleFree(x) netMblkClChainFree(x)
#define xlrEndPoolTupleGet(x)      \
        netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

/*
 * We use TX descriptor arrays, and they must be cache aligned. We
 * combine 8 of them together. This should be enough to handle
 * most scatter/gather combinations without making the descriptor
 * clusters too big.
 */

typedef struct spi4_tx_desc {
    XLRA_TX_DESC xlr_frag[XLR_MAXFRAG];
} SPI4_TX_DESC;

/* Private adapter context structure. */

typedef struct xlr_drv_ctrl
    {
    END_OBJ     xlrEndObj;
    VXB_DEVICE_ID xlrEndDev;
    VXB_DEVICE_ID xlrPrivDev;
    int         xlrUnit;
    void *      xlrBar;
    void *      xlrHandle;

    void *      vscBar;
    void *      vscHandle;

    void *      xlrEndMuxDevCookie;

    int			xlrRxStId;
    int			xlrTxStId;

    void *      xlrPool;

    JOB_QUEUE_ID xlrJobQueue;
    QJOB        xlrEndIntJob;
    atomic_t    xlrEndIntPending;

    int         xlrTxProd;
    int         xlrTxCons;
    int         xlrTxFree;
    volatile BOOL xlrTxStall;
    UINT16      xlrTxThresh;

    QJOB        xlrEndLinkJob;
    volatile BOOL xlrEndLinkPending;

    BOOL        xlrEndPolling;
    M_BLK_ID    xlrEndPollBuf;
    UINT32      xlrEndIntMask;
    UINT32      xlrEndIntrs;
    UINT32		xlrEndRxIntrs;

    UINT8       xlrAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES xlrEndCaps;

    END_IFDRVCONF xlrEndStatsConf;
    END_IFCOUNTERS xlrEndStatsCounters;

    SPI4_TX_DESC * xlrTxDescMem;

    M_BLK_ID    xlrTxMblk[XLR_TX_DESC_CNT];

    UINT64 *    xlrFreeInSpill;
    UINT64 *    xlrFreeIn1Spill;
    UINT64 *    xlrFreeOutSpill;
    UINT64 *    xlrC0Spill;
    UINT64 *    xlrC1Spill;
    UINT64 *    xlrC2Spill;
    UINT64 *    xlrC3Spill;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST * xlrMediaList;
    END_ERR     xlrLastError;
    UINT32      xlrCurMedia;
    UINT32      xlrCurStatus;
    VXB_DEVICE_ID xlrMiiBus;
    VXB_DEVICE_ID xlrMiiDev;
    FUNCPTR     xlrMiiPhyRead;
    FUNCPTR     xlrMiiPhyWrite;
    UINT32      xlrMiiPhyAddr;
    UINT32      miiChannel;
    /* End MII/ifmedia required fields */

    int         xlrMaxMtu;

    SEM_ID      xlrDevSem;
    } XLR_DRV_CTRL;

#define XLR_BAR(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrBar
#define XLR_HANDLE(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (XLR_HANDLE(pDev), (UINT32 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (XLR_HANDLE(pDev),                               \
        (UINT32 *)((char *)XLR_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (XLR_HANDLE(pDev), (UINT16 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (XLR_HANDLE(pDev),                               \
        (UINT16 *)((char *)XLR_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (XLR_HANDLE(pDev), (UINT8 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (XLR_HANDLE(pDev),                                \
        (UINT8 *)((char *)XLR_BAR(pDev) + addr), data)


#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

/* read & write low level access routines */

#define VSC_BAR(p)      ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->vscBar
#define VSC_HANDLE(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->vscHandle


#define CSR0_READ_4(pDev, addr)                                  \
    vxbRead32(VSC_HANDLE(pDev), (UINT32 *)((char *)VSC_BAR(pDev) + addr))

#define CSR0_WRITE_4(pDev, addr, data)                           \
    vxbWrite32(VSC_HANDLE(pDev), (UINT32 *)((char *)VSC_BAR(pDev) + addr), data)

IMPORT int vxbUsDelay (int);

static __inline__ UINT32 VSC_READ_REG
    (
    VXB_DEVICE_ID pDev,
    UINT32 mdl,
    UINT32 smdl,
    UINT32 reg
    )
    {
    UINT32 regOff = MK_REG_OFFSET(mdl, smdl, reg);

    if ((mdl == MDL_SYSTEM) && (smdl == SMDL_SYS_CTRL) &&
        ((reg == LOCAL_DATA)||(reg == LOCAL_STATUS))) 
        return WORD_SWAP(CSR0_READ_4(pDev, regOff));
    else
        {
        /* Perform a dummy read to activate read request. */

        CSR0_READ_4(pDev, regOff);

        /* Wait for data ready. */

        vxbUsDelay(1);

        return WORD_SWAP (CSR0_READ_4(pDev, \
            MK_REG_OFFSET(MDL_SYSTEM, SMDL_SYS_CTRL, LOCAL_DATA)));
        }
    }

#define VSC_WRITE_REG(pDev, mdl, smdl, reg, data)          \
    do                                                     \
        {                                                  \
        UINT32 regOff = MK_REG_OFFSET(mdl, smdl, reg);     \
        vxbUsDelay(1);                                     \
        CSR0_WRITE_4(pDev, regOff, WORD_SWAP(data));       \
        } while ((0))

#define VSC_SETBIT_REG(pDev, mdl, smdl, reg, val) \
        VSC_WRITE_REG(pDev, mdl, smdl, reg,       \
                      VSC_READ_REG(pDev, mdl, smdl, reg) | (val))

#define VSC_CLRBIT_REG(pDev, mdl, smdl, reg, val) \
        VSC_WRITE_REG(pDev, mdl, smdl, reg,       \
                      VSC_READ_REG(pDev, mdl, smdl, reg) & ~(val))
#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbRmiSpi4Endh */
