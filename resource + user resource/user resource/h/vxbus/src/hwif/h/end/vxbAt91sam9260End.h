/* vxbAt91sam9260End.h - AT91SAM9260 VxBus End header file */

/*
 * Copyright (c) 2008, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,15feb13,wap  increased RX/TX descriptor and net pool tuple count;
                 added macros and adjusted EMAC_DRV_CTRL structure. 
                 (WIND00399074)
01b,02nov11,c_t  rewrite the RX logic.(WIND00251506)
01a,14mar08,l_z  written.
*/

/*
DESCRIPTION
This module contains constants and defines for the AT91SAM9260 End.
*/

#ifndef __INCvxbAt91sam9260Endh
#define __INCvxbAt91sam9260Endh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void at91EmacRegister (void);

#ifndef BSP_VERSION

/* Emac register offsets */

#define EMAC_CTL      0x00    /* Network Control Register              */
#define EMAC_CFG      0x04    /* Network Configuration Register        */
#define EMAC_SR       0x08    /* Network Status Register               */
#define EMAC_TSR      0x14    /* Transmit Status Register              */
#define EMAC_RBQP     0x18    /* Receive Buffer Queue Pointer          */
#define EMAC_TBQP     0x1C    /* Transmit Address Register             */
#define EMAC_RSR      0x20    /* Receive Status Register               */
#define EMAC_ISR      0x24    /* Interrupt Status Register             */
#define EMAC_IER      0x28    /* Interrupt Enable Register             */
#define EMAC_IDR      0x2C    /* Interrupt Disable Register            */
#define EMAC_IMR      0x30    /* Interrupt Mask Register               */
#define EMAC_MAN      0x34    /* PHY Maintenance Register              */
#define EMAC_FRA      0x40    /* Frames Transmitted OK Register        */
#define EMAC_SCOL     0x44    /* Single Collision Frame Register       */
#define EMAC_MCOL     0x48    /* Multiple Collision Frame Register     */
#define EMAC_OK       0x4C    /* Frames Received OK Register           */
#define EMAC_SEQE     0x50    /* Frame Check Sequence Error Register   */
#define EMAC_ALE      0x54    /* Alignment Error Register              */
#define EMAC_DTE      0x58    /* Deferred Transmission Frame Register  */
#define EMAC_LCOL     0x5C    /* Late Collision Register               */
#define EMAC_ECOL     0x60    /* Excessive Collision Register          */
#define EMAC_TUND     0x64    /* Transmit Underrun Register            */
#define EMAC_CSE      0x68    /* Carrier Sense Error Register          */
#define EMAC_RRE      0x6C    /* Receive Resource  Error Register      */
#define EMAC_ROV      0x70    /* Receive Overrun Error Register        */
#define EMAC_RSE      0x74    /* Receive Symbol Error Register         */
#define EMAC_ELE      0x78    /* Excessive Lenght Error Register       */
#define EMAC_RJA      0x7C    /* Receive Jabbers Error Register        */
#define EMAC_USF      0x80    /* Undersize Frames Error Register       */
#define EMAC_STE      0x84    /* SQE Test Error Register               */
#define EMAC_RLE      0x88    /* Receive Length Field Error Register   */

#define EMAC_HSL      0x90    /* Hash Address Low[31:0]                */
#define EMAC_HSH      0x94    /* Hash Address High[63:32]              */
#define EMAC_SA1L     0x98    /* Specific Address 1 Low, First 4 bytes */
#define EMAC_SA1H     0x9C    /* Specific Address 1 High, Last 2 bytes */
#define EMAC_SA2L     0xA0    /* Specific Address 2 Low, First 4 bytes */
#define EMAC_SA2H     0xA4    /* Specific Address 2 High, Last 2 bytes */
#define EMAC_SA3L     0xA8    /* Specific Address 3 Low, First 4 bytes */
#define EMAC_SA3H     0xAC    /* Specific Address 3 High, Last 2 bytes */
#define EMAC_SA4L     0xB0    /* Specific Address 4 Low, First 4 bytes */
#define EMAC_SA4H     0xB4    /* Specific Address 4 High, Last 2 bytes */
#define EMAC_TID      0xB8    /* Type ID Checking Register             */
#define EMAC_USRIO    0xC0    /* User IO - CLKEN and RMII/MII          */

#define FIRST_FRAME_IN_PACKAGE   0
#define COMPOSE_FRAME_TO_PACKAGE 1

/* rx/tx buffer descriptors definitions */

#define CL_OVERHEAD             4          /* prepended cluster overhead */
#define CL_ALIGNMENT            4          /* cluster required alignment */
#define MBLK_ALIGNMENT          4          /* mBlks required alignment */
#define EMAC_END_BD_ALIGN       0x40       /* required alignment for RBDs */
#define TX_UNDERRUN             0x10000000 /* transmite under run */



/* Bit assignments for Receive Buffer Descriptor */

/* Address - Word 0 */

#define RXBUF_ADD_BASE_MASK         0xfffffffc /* Base address */
#define RXBUF_ADD_WRAP              0x00000002 /* The last buffer in the ring */
#define RXBUF_ADD_OWNED             0x00000001 /* SW or MAC owns the pointer */

/* Status - Word 1 */

#define RXBUF_STAT_BCAST            0x80000000 /* Global broadcast address */
#define RXBUF_STAT_MULTI            0x40000000 /* Multicast hash match*/
#define RXBUF_STAT_UNI              0x20000000 /* Unicast hash match */ 
#define RXBUF_STAT_EXT              0x10000000 /* External address */ 
#define RXBUF_STAT_UNK              0x08000000 /* Unknown source address */ 
#define RXBUF_STAT_LOC1             0x04000000 /* Specific address 1 match */ 
#define RXBUF_STAT_LOC2             0x02000000 /* Specific address 2 match */ 
#define RXBUF_STAT_LOC3             0x01000000 /* Specific address 3 match */ 
#define RXBUF_STAT_LOC4             0x00800000 /* Specific address 4 match */ 
#define RXBUF_STAT_TYPEID           0x00400000 /* Type ID Match */
#define RXBUF_STAT_VLAN             0x00200000 /* VLAN Detected */
#define RXBUF_STAT_PRIOR            0x00100000 /* Priority Tag Detected */
#define RXBUF_STAT_PRIOR_MASK       0x000e0000 /* VLAN Priority Field */
#define RXBUF_STAT_CFI              0x00010000 /* Concatenation indicator */
#define RXBUF_STAT_EOF              0x00008000 /* Buffer is end of frame */
#define RXBUF_STAT_SOF              0x00004000 /* Buffer is start of frame */
#define RXBUF_STAT_LEN_MASK         0x000007ff /* Length of frame */

/* Bit assignments for Transmit Buffer Descriptor */

/* Status - Word 1 */

#define TXBUF_STAT_USED             0x80000000 /* Successfully transmited */
#define TXBUF_STAT_WRAP             0x40000000 /* Wrap (last buffer in list) */
#define TXBUF_STAT_RETRY_LIMIT      0x20000000 
#define TXBUF_STAT_UNDERRUN         0x10000000 /* Transmit Underrun */
#define TXBUF_STAT_BUFF_EXHAUSTED   0x08000000 
#define TXBUF_STAT_NO_CRC           0x00010000 /* Do not transmit CRC */
#define TXBUF_STAT_LAST_BUFF        0x00008000 
#define TXBUF_STAT_LEN_MASK         0x000007ff /* Length of frame */

/* EMAC register definitions */

/* Control Register, EMAC_CTL, Offset 0x0  */

#define EMAC_CTL_LB             0x00000001     /* Set Loopback output signal */
#define EMAC_CTL_LBL            0x00000002     /* Loopback local.*/
#define EMAC_CTL_RE             0x00000004     /* Receive enable.*/
#define EMAC_CTL_TE             0x00000008     /* Transmit enable.*/
#define EMAC_CTL_MPE            0x00000010     /* Management port enable.*/
#define EMAC_CTL_CSR            0x00000020     /* Clear statistics registers.*/
#define EMAC_CTL_ISR            0x00000040     /* Open statistics registers */
#define EMAC_CTL_WES            0x00000080     /* Enable statistics registers */
#define EMAC_CTL_BP             0x00000100     /* Force collision */
#define EMAC_CTL_TSTART         0x00000200
#define EMAC_CTL_THALT          0x00000400

/* Configuration Register, EMAC_CFG, Offset 0x4 */

#define EMAC_CFG_SPD            0x00000001    /* 10/100 Speed */
#define EMAC_CFG_FD             0x00000002    /* Full duplex.*/
#define EMAC_CFG_JFRAME         0x00000008    /* Enable Jumbo Frames */
#define EMAC_CFG_CAF            0x00000010    /* Accept all frames */
#define EMAC_CFG_NBC            0x00000020    /* No recept broadcast frames */
#define EMAC_CFG_MTI            0x00000040    /* Multicast hash enable */
#define EMAC_CFG_UNI            0x00000080    /* Unicast hash enable. */
#define EMAC_CFG_BIG            0x00000100    /* Open 802.3 1522 byte frames */
#define EMAC_CFG_CLK_8          0x00000000    /* HCLK divided by 8 */
#define EMAC_CFG_CLK_16         0x00000400    /* HCLK divided by 16 */
#define EMAC_CFG_CLK_32         0x00000800    /* HCLK divided by 32 */
#define EMAC_CFG_CLK_64         0x00000c00    /* HCLK divided by 64 */
#define EMAC_CFG_RTY            0x00001000    /* Retry Test Mode */
#define EMAC_CFG_PAE            0x00002000    /* Pause Enable */
#define EMAC_CFG_RBOF_0         0x00000000    /* No offset */
#define EMAC_CFG_RBOF_1         0x00004000    /* One Byte offset */
#define EMAC_CFG_RBOF_2         0x00008000    /* Two Byte offset */
#define EMAC_CFG_RBOF_3         0x0000c000    /* Three Byte offset */
#define EMAC_CFG_RLCE           0x00010000    /* Checking Enable */
#define EMAC_CFG_DRFCS          0x00020000    /* Discard Receive FCS */
#define EMAC_CFG_EFRHD          0x00040000    /* Enable frames in Half Duplex */
#define EMAC_CFG_IRXFCS         0x00080000    /* Ignore Receive FCS */

/* Status Register, EMAC_SR, Offset 0x8 */

#define EMAC_LINK               0x00000001    /* Link pin */
#define EMAC_MDIO               0x00000002    /* Real Time state of MDIO pin */
#define EMAC_IDLE               0x00000004    /* Logic is idle or running */

/* Transmit Status Register, EMAC_TSR, Offset 0x14 */

#define EMAC_TSR_OVR            0x00000001    /* Transmit buffer overrun */
#define EMAC_TSR_COL            0x00000002    /* Collision occured */
#define EMAC_TSR_RLE            0x00000004    /* Retry lmimt exceeded */
#define EMAC_TSR_TXGO           0x00000008    /* Transmitter is idle */
#define EMAC_TSR_BNQ            0x00000010    /* Transmit buffer not queued */
#define EMAC_TSR_COMP           0x00000020    /* Transmit complete */
#define EMAC_TSR_UND            0x00000040    /* Transmit underrun */

/* Receive Status Register, EMAC_RSR, Offset 0x20  */

#define EMAC_RSR_BNA            0x00000001    /* Buffer not available */
#define EMAC_RSR_REC            0x00000002    /* Frame received */
#define EMAC_RSR_OVR            0x00000004    /* Receive overrun */

/*
 * Interrupt Status Register, EMAC_ISR, Offsen 0x24
 * Interrupt Enable Register, EMAC_IER, Offset 0x28
 * Interrupt Disable Register, EMAC_IDR, Offset 0x2c
 * Interrupt Mask Register, EMAC_IMR, Offset 0x30
 */

#define EMAC_INT_DONE           0x00000001    /* Phy management done */
#define EMAC_INT_RCOM           0x00000002    /* Receive complete */
#define EMAC_INT_RBNA           0x00000004    /* Receive buffer not available */
#define EMAC_INT_TBNA           0x00000008
#define EMAC_INT_TUND           0x00000010    /* Transmit buffer underrun */
#define EMAC_INT_RTRY           0x00000020    /* Transmit Retry limt */
#define EMAC_INT_TBRE           0x00000040
#define EMAC_INT_TCOM           0x00000080    /* Transmit complete */
#define EMAC_INT_ROVR           0x00000400    /* Receive overrun */
#define EMAC_INT_ABT            0x00000800    /* Abort on DMA transfer */

/* PHY Maintenance Register, EMAC_MAN, Offset 0x34 */

#define EMAC_MAN_DATA(x)        ((x & 0xFFFF) <<  0) /* PHY data register */
#define EMAC_MAN_CODE           (0x2 << 16)          /* IEEE Code */
#define EMAC_MAN_REGA(x)        ((x & 0x1F) << 18)   /* PHY register address */
#define EMAC_MAN_PHYA(x)        ((x & 0x1F) << 23)   /* PHY address */
#define EMAC_MAN_WRITE          (0x1 << 28)          /* Transfer is a write */
#define EMAC_MAN_READ           (0x2 << 28)          /* Transfer is a read */
#define EMAC_MAN_SOF             0x40000000          /* Must be set */

/* User IO Register, offset 0xc0 */

#define EMAC_USRIO_RMII          0x00000001          /* Enable RMII Mode */
#define EMAC_USRIO_CLKEN         0x00000002          /* Transmit Clock Enable */

#define EMAC_MTU                1500
#define EMAC_JUMBO_MTU          9000
#define EMAC_CLSIZE             1536
#define EMAC_NAME               "emac"
#define EMAC_TIMEOUT            10000
#define EMAC_INTRS              (EMAC_INT_RCOM | EMAC_INT_RBNA \
                                | EMAC_INT_TUND | EMAC_INT_RTRY | EMAC_INT_TCOM \
                                | EMAC_INT_ROVR | EMAC_INT_ABT)

#define EMAC_RX_INTRS           (EMAC_INT_RCOM | EMAC_INT_RBNA \
                                 | EMAC_INT_ROVR )

#define EMAC_TX_INTRS           (EMAC_INT_TUND | EMAC_INT_RTRY | EMAC_INT_TCOM)

#define EMAC_RX_STS		(EMAC_RSR_BNA|EMAC_RSR_REC|EMAC_RSR_OVR)
#define EMAC_TX_STS		(EMAC_TSR_COMP|EMAC_TSR_RLE|EMAC_TSR_UND|EMAC_TSR_OVR|EMAC_TSR_BNQ)

#define EMAC_TUPLE_CNT          1536
#define EMAC_ADJ(x)             (x)->m_data += 2

#define EMAC_INC_DESC(x,y)      (x) = (((x)+1) & ((y)-1))
#define EMAC_MAXFRAG            16

#define EMAC_MAX_RX             64
#define EMAC_RX_DESC_CNT        512  /* must be power of 2 */
#define EMAC_TX_DESC_CNT        256  /* must be power of 2 */
#define EMAC_RX_MAXPROC_DESC    256
#define EMAC_RX_BUFF_SIZE       128

#define EMAC_RX_PAUSE_THRESHOLD 180 
#define INIT_REFLECTED          0xFFFFFFFF
#define XOROT                   INIT_REFLECTED

#define AT91C_ISRAM0        (0x00200000)  /* Internal SRAM 0 base address */
#define AT91C_ISRAM0_SIZE   (0x00001000)  /* Internal SRAM 0 size in byte */
#define AT91C_ISRAM1        (0x00300000)  /* Internal SRAM 1 base address */
#define AT91C_ISRAM1_SIZE   (0x00001000)  /* Internal SRAM 1 size in byte */
#define AT91C_IROM          (0x00100000)  /* Internal ROM base address */
#define AT91C_IROM_SIZE     (0x00008000)  /* Internal ROM size in byte */

/* CRC for logical address filter */

#define EMAC_CRC_TO_FILTER_INDEX(crc)   ((crc) >> 26)   /* get 6 MSBits */

typedef struct emac_desc
    {
    volatile UINT32    bdAddr;
    volatile UINT32    bdSts;
    } EMAC_DESC;

/*
 * Private adapter context structure.
 */

typedef struct emac_drv_ctrl
    {
    END_OBJ          emacEndObj;
    VXB_DEVICE_ID    emacDev;
    void *           emacBar;
    void *           emacHandle;
    void *           emacMuxDevCookie;

    JOB_QUEUE_ID     emacJobQueue;
    QJOB             emacIntJob;
    atomic_t         emacIntPending;
    QJOB             emacRxJob;
    atomic_t         emacRxPending;
    QJOB             emacTxJob;
    atomic_t         emacTxPending;

    BOOL             emacPolling;
    M_BLK_ID         emacPollBuf;
    UINT32           emacIntrs;
    UINT32           emacMoreRx;
    UINT8            emacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES emacCaps;
    END_IFDRVCONF    emacEndStatsConf;
    END_IFCOUNTERS   emacEndStatsCounters;

    UINT32           emacInErrors;
    UINT32           emacInDiscards;
    UINT32           emacInUcasts;
    UINT32           emacInMcasts;
    UINT32           emacInBcasts;
    UINT32           emacInOctets;
    UINT32           emacOutErrors;
    UINT32           emacOutUcasts;
    UINT32           emacOutMcasts;
    UINT32           emacOutBcasts;
    UINT32           emacOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *  emacMediaList;
    END_ERR          emacLastError;
    UINT32           emacCurMedia;
    UINT32           emacCurStatus;
    VXB_DEVICE_ID    emacMiiBus;
    VXB_DEVICE_ID    emacMiiDev;
    FUNCPTR          emacMiiPhyRead;
    FUNCPTR          emacMiiPhyWrite;
    int              emacMiiPhyAddr;
    
    /* End MII/ifmedia required fields */

   /* DMA tags and maps. */

    EMAC_DESC *      emacRxDescMem;
    EMAC_DESC *      emacTxDescMem;
    M_BLK_ID         emacTxMblk[EMAC_TX_DESC_CNT];
    UINT32           emacRxBuff;

    UINT32           emacTxProd;
    UINT32           emacTxCons;
    UINT32           emacTxFree;
    UINT32           emacRxIdx;
    UINT32           emacTxLast;
    UINT32           emacTxStall;

    SEM_ID           emacMiiSem;
    int              emacMaxMtu;

    } EMAC_DRV_CTRL;

/* EMAC control module register low level access routines */

#define EMAC_BAR(p)        ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->emacBar
#define EMAC_HANDLE(p)     ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->emacHandle

#define CSR_READ_4(pDev, addr)             \
        vxbRead32(EMAC_HANDLE(pDev),       \
                 (UINT32 *)((char *)EMAC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)      \
        vxbWrite32(EMAC_HANDLE(pDev),      \
                  (UINT32 *)((char *)EMAC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)    \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)    \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbAt91sam9260Endh */
