/* vxbRmiXgmacEnd.h - header file for RMI XGMAC XGMII VxBus END driver */

/*
 * Copyright (c) 2007-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,01may08,wap  written
*/

#ifndef __INCvxbRmiXgmacEndh
#define __INCvxbRmiXgmacEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void xlrXgmacRegister (void);


#ifndef BSP_VERSION

#define XLR_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))
#define XLR_MAXFRAG		8
#define XLR_MAX_RX		32

#define XLR_NUM_PORTS		4

#define XLR_MTU                 1500
#define XLR_JUMBO_MTU           9000

#define XLR_CLSIZE		1536

#define XLR_NAME		"xgmac"
#define XLR_TIMEOUT		10000
#define XLR_INTRS (XLR_RXINTRS|XLR_TXINTRS)
#define XLR_RXINTRS	\
	(XLRA_ISR_C0EARLYFULL|XLRA_ISR_C1EARLYFULL|	\
	 XLRA_ISR_C2EARLYFULL|XLRA_ISR_C3EARLYFULL)
#define XLR_TXINTRS	XLRA_ISR_FREEOEARLYFULL

#define XLR_ETHER_ALIGN		2

#define XLR_RX_DESC_CNT		256
#define XLR_TX_DESC_CNT		256

#define XLR_RX_BUCKET		(3 + (pDev->unitNumber * 2))
#define XLR_TX_BUCKET		(4 + (pDev->unitNumber * 2))
#define XLR_ERR_BUCKET		(4 + (pDev->unitNumber * 2))

#define XLR_RXBMASK (1 << XLR_RX_BUCKET)
#define XLR_TXBMASK ((1 << XLR_TX_BUCKET) | (1 << XLR_ERR_BUCKET))
#define XLR_BMASK	(XLR_TXBMASK|XLR_RXBMASK)

/* XGMII XGMAC registers */

#define XGMAC_MAC_CONF0         0x0000 /* MAC Configuration #0 0x0000_8000 */
#define XGMAC_MAC_CONF1         0x0004 /* MAC Configuration #1 0x0000_003D */
#define XGMAC_MAC_CONF2         0x0008 /* MAC Configuration #2 0x40D0_0000 */
#define XGMAC_MAC_CONF3         0x000C /* MAC Configuration #3 0x0000_FFFF */
#define XGMAC_MAX_FRM           0x0020 /* Maximum Frame 0x0180_0600 */
#define XGMAC_REVISION          0x002C /* Revision Level 0x0000_0608 */
#define XGMAC_MIIM_CMD          0x0040 /* MII Mgmt: Command 0x0000_0000 */
#define XGMAC_MIIM_FIELD        0x0044 /* MII Mgmt: Field 0x0208_6000 */
#define XGMAC_MIIM_CONF         0x0048 /* MII Mgmt: Configuration 0x0000_003E */
#define XGMAC_MIIM_LF_VEC       0x004C /* MII Mgmt: Link Fail Vec 0x0000_0000 */
#define XGMAC_MIIM_IND          0x0050 /* MII Mgmt: Indicators 0x0000_0000 */

/* RX filter registers */

#define XGMAC_MAC_ADDR0_LO      0x0140 /* MAC_ADDR0 */
#define XGMAC_MAC_ADDR0_HI      0x0144 /* MAC_ADDR0 */
#define XGMAC_MAC_ADDR1_LO      0x0148 /* MAC_ADDR1 */
#define XGMAC_MAC_ADDR1_HI      0x014C /* MAC_ADDR1 */
#define XGMAC_MAC_ADDR2_LO      0x0150 /* MAC_ADDR2 */
#define XGMAC_MAC_ADDR2_HI      0x0154 /* MAC_ADDR2 */
#define XGMAC_MAC_ADDR3_LO      0x0158 /* MAC_ADDR3 */
#define XGMAC_MAC_ADDR3_HI      0x015C /* MAC_ADDR3 */
#define XGMAC_MAC_ADDR_MASK0_LO 0x0160 /* MAC_ADDR_MASK0 */
#define XGMAC_MAC_ADDR_MASK0_HI 0x0164 /* MAC_ADDR_MASK0 */
#define XGMAC_MAC_ADDR_MASK1_LO 0x0168 /* MAC_ADDR_MASK1 */
#define XGMAC_MAC_ADDR_MASK1_HI 0x016C /* MAC_ADDR_MASK1 */
#define XGMAC_MAC_FILTER_CONFIG 0x0170 /* MAC Filter Configuration */

/* Hash table registers */

#define XGMAC_HASH_TABLE_0	0x0180
#define XGMAC_HASH_TABLE_1	0x0184
#define XGMAC_HASH_TABLE_2	0x0188
#define XGMAC_HASH_TABLE_3	0x018C
#define XGMAC_HASH_TABLE_4	0x0190
#define XGMAC_HASH_TABLE_5	0x0194
#define XGMAC_HASH_TABLE_6	0x0198
#define XGMAC_HASH_TABLE_7	0x019C
#define XGMAC_HASH_TABLE_8	0x01A0
#define XGMAC_HASH_TABLE_9	0x01A4
#define XGMAC_HASH_TABLE_10	0x01A8
#define XGMAC_HASH_TABLE_11	0x01AC
#define XGMAC_HASH_TABLE_12	0x01B0
#define XGMAC_HASH_TABLE_13	0x01B4
#define XGMAC_HASH_TABLE_14	0x01B8
#define XGMAC_HASH_TABLE_15	0x01BC

/* MAC configuration register 0 */

#define XGMAC_MACCONF0_SRST	0x80000000	/* Soft reset */
#define XGMAC_MACCONF0_RSTRCTL	0x00400000	/* Reset RX control */
#define XGMAC_MACCONF0_RSTRFM	0x00200000	/* Reset RX function */
#define XGMAC_MACCONF0_RSTTCTL	0x00040000	/* Reset TX control */
#define XGMAC_MACCONF0_RSTTFN	0x00020000	/* Reset TX functuon */
#define XGMAC_MACCONF0_RSTMIIM	0x00010000	/* Reset MII */

/* MAC configuration register 1 */

#define XGMAC_MACCONF1_TCTLEN	0x80000000	/* enable pause frames */
#define XGMAC_MACCONF1_TFEN	0x40000000	/* enable TX */
#define XGMAC_MACCONF1_RCTLEN	0x20000000	/* enable pause frames */
#define XGMAC_MACCONF1_RFEN	0x10000000	/* enable RX */
#define XGMAC_MACCONF1_TFSTS	0x04000000	/* TX enable status */
#define XGMAC_MACCONF1_RFSTS	0x01000000	/* RX enable status */
#define XGMAC_MACCONF1_RCTLSTP	0x00001000	/* enable short pause */
#define XGMAC_MACCONF1_DLYFCSTX	0x00000C00	/* TX FCS delay */
#define XGMAC_MACCONF1_DLYFCSRX	0x00000300	/* RX FCS delay */
#define XGMAC_MACCONF1_PPEN	0x00000080	/* per-packet settings */
#define XGMAC_MACCONF1_BYTSWP	0x00000040	/* byte swap (test only)*/
#define XGMAC_MACCONF1_DRPLT64	0x00000020	/* Discard runts */
#define XGMAC_MACCONF1_PRMSCRX	0x00000010	/* Promisc RX */
#define XGMAC_MACCONF1_GENFCS	0x00000008	/* Generate FCS */
#define XGMAC_MACCONF1_PADEN	0x00000004	/* PAD TX frames */
#define XGMAC_MACCONF1_PADMODE	0x00000003	/* padding mode */

#define XGMAC_DLYFCSTX_NONE	0x00000000
#define XGMAC_DLYFCSTX_1WORD	0x00000400
#define XGMAC_DLYFCSTX_2WORD	0x00000800
#define XGMAC_DLYFCSTX_3WORD	0x00000C00

#define XGMAC_DLYFCSRX_NONE	0x00000000
#define XGMAC_DLYFCSRX_1WORD	0x00000100
#define XGMAC_DLYFCSRX_2WORD	0x00000200
#define XGMAC_DLYFCSRX_3WORD	0x00000300

#define XGMAC_PADMODE_NONE	0x00000000
#define XGMAC_PADMODE_PAD64	0x00000001	/* Pad to 64 bytes */
#define XGMAC_PADMODE_PAD64_68	0x00000002	/* Pad to 64 or 68 bytes*/
#define XGMAC_PADMODE_68	0x00000003	/* Pad to 68 bytes */


/* MAC configuration register 2 */

#define XGMAC_MACCONF2_TXPAUSE	0x80000000	/* Force TX pause xmit (testing) */
#define XGMAC_MACCONF2_MILNKFLT	0x08000000	/* MI link fault handler */
#define XGMAC_MACCONF2_ALNKFLT	0x04000000	/* auto link fault handler */
#define XGMAC_MACCONF2_RLNKFLT	0x03000000	/* link fault */
#define XGMAC_MACCONF2_IPGETXMD	0x001F0000	/* IPG extension modulus */
#define XGMAC_MACCONF2_RXPAUSE	0x00001000	/* Force TX pause (testing) */
#define XGMAC_MACCONF2_IPGEXTEN	0x00000020	/* IPG extension enable*/
#define XGMAC_MACCONF2_MIPGEXT	0x0000001F	/* Minimum IPG extension */

#define XGMAC_FAULT_NONE	0x00000000
#define XGMAC_FAULT_LOCAL	0x01000000
#define XGMAC_FAULT_REMOTE	0x02000000

/* MAC configuration register 3 */

#define XGMAC_MACCONF3_FLTRFRM	0xFFFF0000	/* framedrop filter status */
#define XGMAC_MACCONF3_FLTRFCFG	0x0000FFFF	/* framedrop filter config */

/* Max frame length register */

#define XGMAC_MAXFRM_MAXTX	0x3FFF0000	/* Max TX frame (words) */
#define XGMAC_MAXFRM_MAXRX	0x0000FFFF	/* Max RX frame (bytes) */

#define XGMAC_MAXRX(x)		((x) & XGMAC_MAXFRM_MAXRX)
#define XGMAC_MAXTX(x)		((((x) >> 2) << 16) & XGMAC_MAXFRM_MAXTX)

/* MII Management command register */

#define XGMAC_MIICMD_GO		0x00000008	/* Execute command */
#define XGMAC_MIICMD_CMD	0x00000007	/* Command to run */

#define XGMAC_MIICMD_IDLE	0x00000000	/* no-op */
#define XGMAC_MIICMD_LREAD	0x00000001	/* legacy read */
#define XGMAC_MIICMD_LWRITE	0x00000002	/* legacy write */
#define XGMAC_MIICMD_SMONITOR	0x00000003	/* monitor single reg */
#define XGMAC_MIICMD_MMONITOR	0x00000004	/* monitor multi reg */
#define XGMAC_MIICMD_MMD	0x00000005	/* 10GbE MMD op */
#define XGMAC_MIICMD_CLRLNKFL	0x00000006	/* Clear link fail */

/* MII Management Field register */

#define XGMAC_MIIFLD_TYPE	0xC0000000	/* device type */
#define XGMAC_MIIFLD_OP		0x30000000	/* operation */
#define XGMAC_MIIFLD_PHYADDR	0x0FE00000	/* PHY address */
#define XGMAC_MIIFLD_REGADDR	0x007C0000	/* register offset */
#define XGMAC_MIIFLD_TURNAROUND	0x00030000	/* turnaround field */
#define XGMAC_MIIFLD_DATA	0x0000FFFF	/* read/write data */

#define XGMAC_TURNAROUND	0x00020000

#define XGMAC_MIITYPE_10GB	0x00000000	/* 10GbE device */
#define XGMAC_MIITYPE_LEGACY	0x40000000	/* 10/100/1000 legacy */

#define XGMAC_MIIOP_IND		0x00000000	/* indirect MMD access */
#define XGMAC_MIIOP_WRITE	0x10000000	/* PHY write */
#define XGMAC_MIIOP_READ	0x20000000	/* PHY read */
#define XGMAC_MIIOP_READINC	0x30000000	/* read with increment */

#define XGMAC_PHYADDR(x)	(((x) << 23) & XGMAC_MIIFLD_PHYADDR)
#define XGMAC_REGADDR(x)	(((x) << 18) & XGMAC_MIIFLD_REGADDR)

/* MII Management configuration register */

#define XGMAC_MIICONF_NOPRAM	0x00000080	/* bypass preamble */
#define XGMAC_MIICONF_CLKDIV	0x0000003F	/* MDC clock divisor */

/* MII Management Indication register */

#define XGMAC_MIIIND_PHYLF	0x00000010	/* link fail */
#define XGMAC_MIIIND_MONCPLT	0x00000008	/* monitor op complete */
#define XGMAC_MIIINT_MONVLD	0x00000004	/* monitor results valid */
#define XGMAC_MIIINT_MON	0x00000002	/* monitor in progress */
#define XGMAC_MIIINT_BUSY	0x00000001	/* command in progress */

/* RX filter configuration register */

#define XGMAC_FILTCFG_BCAST_EN	0x00000400	/* broadcast RX enable */
#define XGMAC_FILTCFG_PAUSE_EN	0x00000200	/* pause RX enable */
#define XGMAC_FILTCFG_ALLMULTI	0x00000100	/* RX all multicast */
#define XGMAC_FILTCFG_ALLUCAST	0x00000080	/* RX all unicast */
#define XGMAC_FILTCFG_HASH_MCAST	0x00000040	/* hash filter multicast */
#define XGMAC_FILTCFG_HASH_UCAST	0x00000020	/* hash filter unicast */
#define XGMAC_FILTCFG_MATCH_DISC	0x00000010	/* discard on mac addr match */
#define XGMAC_FILTCFG_MAC3_VALID	0x00000008	/* MAC_ADDR3 valid */
#define XGMAC_FILTCFG_MAC2_VALID	0x00000004	/* MAC_ADDR2 valid */
#define XGMAC_FILTCFG_MAC1_VALID	0x00000002	/* MAC_ADDR1 valid */
#define XGMAC_FILTCFG_MAC0_VALID	0x00000001	/* MAC_ADDR0 valid */

#define GAMC_TR64              0x0080 /* Transmit and Receive 64 Byte Frame Counter   */
#define GAMC_TR127             0x0084 /* Transmit and Receive 65 to 127 Byte Frame Counter*/
#define GAMC_TR255             0x0088 /* Transmit and Receive 128 to 255 Byte Frame Counter   */
#define GAMC_TR511             0x008C /* Transmit and Receive 256 to 511 Byte Frame Counter   */
#define GAMC_TR1K              0x0090 /* Transmit and Receive 512 to 1023 Byte Frame Counter  */
#define GAMC_TRMAX             0x0094 /* Transmit and Receive 1024 to 1518 Byte Frame Counter */
#define GAMC_TRMGV             0x0098 /* Transmit and Receive 1519 to 1522 Byte Good VLAN Frame Cnt   */
                                 
#define XGMAC_RBYT              0x009C /* Receive Byte Counter */
#define XGMAC_RPKT              0x00A0 /* Receive Packet Counter   */
#define XGMAC_RFCS              0x00A4 /* Receive FCS Error Counter*/
#define XGMAC_RMCA              0x00A8 /* Receive Multicast Packet Counter */
#define XGMAC_RBCA              0x00AC /* Receive Broadcast Packet Counter   */
#define XGMAC_RXCF              0x00B0 /* Receive Control Frame Packet Counter*/
#define XGMAC_RXPF              0x00B4 /* Receive PAUSE Frame Packet Counter */
#define XGMAC_RXUO              0x00B8 /* Receive Unknown OP code Counter  */
#define XGMAC_RALN              0x00BC /* Receive Alignment Error Counter */
#define XGMAC_RFLR              0x00C0 /* Receive Frame Length Error Counter */
#define XGMAC_RCDE              0x00C4 /* Receive Code Error Counter */
#define XGMAC_RCSE              0x00C8 /* Receive Carrier Sense Error Counter*/
#define XGMAC_RUND              0x00CC /* Receive Undersize Packet Counter */
#define XGMAC_ROVR              0x00D0 /* Receive Oversize Packet Counter  */
#define XGMAC_RFRG              0x00D4 /* Receive Fragments Counter*/
#define XGMAC_RJBR              0x00D8 /* Receive Jabber Counter */
#define XGMAC_RDRP              0x00DC /* Receive Drop 0   */
                                 
#define XGMAC_TBYT              0x00E0 /* Transmit Byte Counter*/
#define XGMAC_TPKT              0x00E4 /* Transmit Packet Counter  */
#define XGMAC_TMCA              0x00E8 /* Transmit Multicast Packet Counter*/
#define XGMAC_TBCA              0x00EC /* Transmit Broadcast Packet Counter*/
#define XGMAC_TXPF              0x00F0 /* Transmit PAUSE Control Frame Counter*/
#define XGMAC_TDFR              0x00F4 /* Transmit Deferral Packet Counter */
#define XGMAC_TEDF              0x00F8 /* Transmit Excessive Deferral Packet Counter   */
#define XGMAC_TSCL              0x00FC /* Transmit Single Collision Packet Counter */
#define XGMAC_TMCL              0x0100 /* Transmit Multiple Collision Packet Counter   */
#define XGMAC_TLCL              0x0104 /* Transmit Late Collision Packet Counter   */
#define XGMAC_TXCL              0x0108 /* Transmit Excessive Collision Packet Counter  */
#define XGMAC_TNCL              0x010C /* Transmit Total Collision Counter */
#define XGMAC_TPFH              0x0110 /* Transmit PAUSE Frames Honored Counter*/
#define XGMAC_TDRP              0x0114 /* Transmit Drop Frame Counter  */
#define XGMAC_TJBR              0x0118 /* Transmit Jabber Frame Counter*/
#define XGMAC_TFCS              0x011C /* Transmit FCS Error Counter   */
#define XGMAC_TXCF              0x0120 /* Transmit Control Frame Counter   */
#define XGMAC_TOVR              0x0124 /* Transmit Oversize Frame Counter  */
#define XGMAC_TUND              0x0128 /* Transmit Undersize Frame Counter   */
#define XGMAC_TFRG              0x012C /* Transmit Fragments Frame Counter   */
        
#define XGMAC_TIMEOUT           100000 /* XGMAC operation timeout value */


/*
 * The XGMII XGMACs have the following bucket IDs allocated to them in
 * the fast messaging network (FNM).
 */

#define XGMAC_MSG_STID_XGMIIA	64	/* TX */
#define XGMAC_MSG_STID_XGMIIB	80	/* TX */
#define XGMAC_MSG_STID_XSA_RX	112	/* RX */
#define XGMAC_MSG_STID_XSB_RX	114	/* RX */
#define XGMAC_MSG_STID_XSA_JFREE	112	/* Free jumbo descriptors */
#define XGMAC_MSG_STID_XSA_FREE	113	/* Free descriptors */
#define XGMAC_MSG_STID_XSB_JFREE	114	/* Free jumbo descriptors */
#define XGMAC_MSG_STID_XSB_FREE	115	/* Free descriptors */

/*
 * Message are allowed to have a software code field. We currently
 * don't use this, but we initialize it for completeness.
 */

#define XGMAC_MSG_CODE_RX	0xAB
#define XGMAC_MSG_CODE_TX	0xCD

typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define xlrEndPoolTupleFree(x) netMblkClChainFree(x)
#define xlrEndPoolTupleGet(x)      \
        netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

/*
 * We use TX descriptor arrays, and they must be cache aligned. We
 * combine 8 of them together. This should be enough to handle
 * most scatter/gather combinations without making the descriptor
 * clusters too big.
 */

typedef struct gmac_tx_desc {
	XLRA_TX_DESC	xlr_frag[XLR_MAXFRAG];
} XGMAC_TX_DESC;

/*
 * Private adapter context structure.
 */

typedef struct xlr_drv_ctrl
    {
    END_OBJ		xlrEndObj;
    VXB_DEVICE_ID	xlrEndDev;
    void *		xlrBar;
    void *		xlrHandle;
    void *		xlrEndMuxDevCookie;

    int			xlrRxBucket;
    int			xlrTxBucket;

    void *		xlrPool;

    JOB_QUEUE_ID	xlrJobQueue;
    QJOB		xlrEndIntJob;
    atomic_t		xlrEndIntPending;

    int			xlrTxProd;
    int			xlrTxCons;
    int			xlrTxFree; 
    volatile BOOL	xlrTxStall;
    UINT16		xlrTxThresh;

    QJOB		xlrEndLinkJob;
    volatile BOOL	xlrEndLinkPending;

    BOOL		xlrEndPolling;
    M_BLK_ID		xlrEndPollBuf;
    UINT32		xlrEndIntMask;
    UINT32		xlrEndIntrs;

    UINT8		xlrAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	xlrEndCaps;

    END_IFDRVCONF	xlrEndStatsConf;
    END_IFCOUNTERS	xlrEndStatsCounters;

    XGMAC_TX_DESC *	xlrTxDescMem;

    M_BLK_ID		xlrTxMblk[XLR_TX_DESC_CNT];

    UINT64 *		xlrFreeInSpill;
    UINT64 *		xlrFreeIn1Spill;
    UINT64 *		xlrFreeOutSpill;
    UINT64 *		xlrC0Spill;
    UINT64 *		xlrC1Spill;
    UINT64 *		xlrC2Spill;
    UINT64 *		xlrC3Spill;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	xlrMediaList;
    END_ERR		xlrLastError;
    UINT32		xlrCurMedia;
    UINT32		xlrCurStatus;
    VXB_DEVICE_ID	xlrMiiBus;
    VXB_DEVICE_ID	xlrMiiDev;
    FUNCPTR		xlrMiiPhyRead;
    FUNCPTR		xlrMiiPhyWrite;
    UINT32		xlrMiiPhyAddr;
    /* End MII/ifmedia required fields */

    int			xlrMaxMtu;

    SEM_ID		xlrDevSem;
    } XLR_DRV_CTRL;

#define XLR_BAR(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrBar
#define XLR_HANDLE(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (XLR_HANDLE(pDev), (UINT32 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (XLR_HANDLE(pDev),                             \
        (UINT32 *)((char *)XLR_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (XLR_HANDLE(pDev), (UINT16 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (XLR_HANDLE(pDev),                             \
        (UINT16 *)((char *)XLR_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (XLR_HANDLE(pDev), (UINT8 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (XLR_HANDLE(pDev),                              \
        (UINT8 *)((char *)XLR_BAR(pDev) + addr), data)


#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbRmiXgmacEndh */
