/* vxbIntelTopcliffEnd.h - header file for tcei VxBus ENd driver */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,02dec10,j_z  modified TCEI_CLSIZE. (WIND00243326)
01a,28jul10,wap  written
*/

#ifndef __INCvxbIntelTopcliffEndh
#define __INCvxbIntelTopcliffEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbIntelTopcliffEndRegister (void);

#ifndef BSP_VERSION

#define INTEL_VENDORID		0x8086

#define INTEL_DEVICEID_TOPCLIFF	0x8802

#define TCEI_ISR		0x000	/* interrupt status */
#define TCEI_IMR		0x004	/* interrupt mask */
#define TCEI_MODE		0x008 	/* mode */
#define TCEI_RESET		0x00C	/* reset */
#define TCEI_TCPIPACC		0x010	/* TCP/IP accelerator */
#define TCEI_EXLIST		0x014	/* external options list */
#define TCEI_ISRH		0x018	/* interrupt status hold */
#define TCEI_PHYINTCTL		0x01C	/* PHY interrupt control */
#define TCEI_RXEN		0x020	/* MAX RX enable */
#define TCEI_RXFC		0x024	/* RX flow control */
#define TCEI_PAUSEREQ		0x028	/* pause packet request */
#define TCEI_RXMODE		0x02C	/* RX mode */
#define TCEI_TXMODE		0x030	/* TX mode */
#define TCEI_RXFIFOSTS		0x034	/* RX FIFO status */
#define TCEI_TXFIFOSTS		0x038	/* TX FIFO status */
#define TCEI_PAUSE_PKT0		0x044	/* Pause packet 0 */
#define TCEI_PAUSE_PKT1		0x048	/* Pause packet 1 */
#define TCEI_PAUSE_PKT2		0x04C	/* Pause packet 2 */
#define TCEI_PAUSE_PKT3		0x050	/* Pause packet 3 */
#define TCEI_PAUSE_PKT4		0x054	/* Pause packet 4 */
#define TCEI_PARLO_0		0x060	/* MAC address 0 low */
#define TCEI_PARHI_0		0x064	/* MAC address 0 high */
#define TCEI_PARLO_15		0x0D8	/* MAC address 15 low */
#define TCEI_PARHI_15		0x0DC	/* MAC address 15 high */
#define TCEI_PARMASK		0x0E0	/* MAC addr. activation mask */
#define TCEI_MIIM		0x0E4	/* MDIO management */
#define TCEI_WOLLOAD		0x0E8	/* address 0 WOL load */
#define TCEI_RGMIISTS		0x0EC	/* RGMII status */
#define TCEI_RGMIICTL		0x0F0	/* RGMII control */
#define TCEI_DMACTL		0x100	/* DMA control */
#define TCEI_RXD_BASE		0x110	/* RX DMA ring base */
#define TCEI_RXD_SIZE		0x114	/* RX DMA ring size */
#define TCEI_RXD_HPTR		0x118	/* RX consumer index */
#define TCEI_RXD_HPTR_HOLD	0x11C	/* RX consumer index hold */
#define TCEI_RXD_SPTR		0x120	/* RX producer index */
#define TCEI_TXD_BASE		0x130	/* TX DMA ring base */
#define TCEI_TXD_SIZE		0x134	/* TX DMA ring size */
#define TCEI_TXD_HPTR		0x138	/* TX consumer index */
#define TCEI_TXD_HPTR_HOLD	0x13C	/* TX consumer index hold */
#define TCEI_TXD_SPTR		0x140	/* TX producer index */
#define TCEI_WOLSTS		0x160	/* wake on lan status */
#define TCEI_WOLCTL		0x164	/* wake on lan control */
#define TCEI_WOLMASK		0x168	/* WOL address mask */
#define TCEI_SRST		0x1FC	/* soft reset */

/* Interrupt status register (clear on read) */

#define TCEI_ISR_TCPIPERR	0x10000000	/* TCP/IP acc. error */
#define TCEI_ISR_WOLDET		0x01000000	/* wake-on-lan detect */
#define TCEI_ISR_PHYINT		0x00100000	/* PHY interrupt */
#define TCEI_ISR_MDIODONE	0x00010000	/* MDIO read complete */
#define TCEI_ISR_PAUSEDONE	0x00001000	/* pause frame TX done */
#define TCEI_ISR_TXDMAERR	0x00000800	/* TX DMA error */
#define TCEI_ISR_TXFIFOERR	0x00000400	/* TX FIFO error */
#define TCEI_ISR_TXDMADONE	0x00000200	/* TX DMA done */
#define TCEI_ISR_TXDONE		0x00000100	/* TX done */
#define TCEI_ISR_RXNODESC	0x00000020	/* RX ring full */
#define TCEI_ISR_RXDMAERR	0x00000010	/* RX DMA error */
#define TCEI_ISR_RXFIFOERR	0x00000008	/* RX FIFO error */
#define TCEI_ISR_RXFRAMEERR	0x00000004	/* RX frame error */
#define TCEI_ISR_RXDONE		0x00000002	/* RX done */
#define TCEI_ISR_RXDMADONE	0x00000001	/* RX DMA done */

/* Interrupt mask register (write 1 to enable) */

#define TCEI_IMR_TCPIPERR	0x10000000	/* TCP/IP acc. error */
#define TCEI_IMR_WOLDET		0x01000000	/* wake-on-lan detect */
#define TCEI_IMR_PHYINT		0x00100000	/* PHY interrupt */
#define TCEI_IMR_MDIODONE	0x00010000	/* MDIO read complete */
#define TCEI_IMR_PAUSEDONE	0x00001000	/* pause frame TX done */
#define TCEI_IMR_TXDMAERR	0x00000800	/* TX DMA error */
#define TCEI_IMR_TXFIFOERR	0x00000400	/* TX FIFO error */
#define TCEI_IMR_TXDMADONE	0x00000200	/* TX DMA done */
#define TCEI_IMR_TXDONE		0x00000100	/* TX done */
#define TCEI_IMR_RXNODESC	0x00000020	/* RX ring full */
#define TCEI_IMR_RXDMAERR	0x00000010	/* RX DMA error */
#define TCEI_IMR_RXFIFOERR	0x00000008	/* RX FIFO error */
#define TCEI_IMR_RXFRAMEERR	0x00000004	/* RX frame error */
#define TCEI_IMR_RXDONE		0x00000002	/* RX done */
#define TCEI_IMR_RXDMADONE	0x00000001	/* RX DMA done */

/* mode register */

#define TCEI_MODE_WIDTH		0x80000000	/* nibble or byte */
#define TCEI_MODE_FDX		0x40000000	/* full duplex */
#define TCEI_MODE_BURST		0x02000000	/* frame bursting (gigE HDX) */

#define TCEI_WIDTH_NIBBLE	0x00000000	/* 10/100 Mbps */
#define TCEI_WIDTH_BYTE		0x80000000	/* 1000Mbps */

/* reset register  */

#define TCEI_RESET_ALL		0x80000000	/* reset whole MAC */
#define TCEI_RESET_TX		0x00008000	/* reset TX only */
#define TCEI_RESET_RX		0x00004000	/* reset RX only */

/* TCP/IP acceleration control  */

#define TCEI_TCPIPACC_LIST_EN	0x00000008	/* external list enable */
#define TCEI_TCPIPACC_RX_OFF	0x00000004	/* RX acceleration disable */
#define TCEI_TCPIPACC_TX_EN	0x00000002	/* TX acceleration enable */
#define TCEI_TCPIPACC_RX_EN	0x00000001	/* RX acceleration enable */

/* IPv6 option header list */

#define TCEI_EXLIST_OPT4	0xFF000000	/* option header 4 */
#define TCEI_EXLIST_OPT3	0x00FF0000	/* option header 4 */
#define TCEI_EXLIST_OPT2	0x0000FF00	/* option header 4 */
#define TCEI_EXLIST_OPT1	0x000000FF	/* option header 4 */

/* Interrupt status (read only) */

#define TCEI_ISRH_TCPIPERR	0x10000000	/* TCP/IP acc. error */
#define TCEI_ISRH_WOLDET	0x01000000	/* wake-on-lan detect */
#define TCEI_ISRH_PHYINT	0x00100000	/* PHY interrupt */
#define TCEI_ISRH_MDIODONE	0x00010000	/* MDIO read complete */
#define TCEI_ISRH_PAUSEDONE	0x00001000	/* pause frame TX done */
#define TCEI_ISRH_TXDMAERR	0x00000800	/* TX DMA error */
#define TCEI_ISRH_TXFIFOERR	0x00000400	/* TX FIFO error */
#define TCEI_ISRH_TXDMADONE	0x00000200	/* TX DMA done */
#define TCEI_ISRH_TXDONE	0x00000100	/* TX done */
#define TCEI_ISRH_RXNODESC	0x00000020	/* RX ring full */
#define TCEI_ISRH_RXDMAERR	0x00000010	/* RX DMA error */
#define TCEI_ISRH_RXFIFOERR	0x00000008	/* RX FIFO error */
#define TCEI_ISRH_RXFRAMEERR	0x00000004	/* RX frame error */
#define TCEI_ISRH_RXDONE	0x00000002	/* RX done */
#define TCEI_ISRH_RXDMADONE	0x00000001	/* RX DMA done */

/* PHY interrupt control */

#define TCEI_PHYINTCTL_EN	0x00010000	/* PHYINT enabled */
#define TCEI_PHYINTCTL_MODE	0x00000003	/* interrupt mode */

#define TCEI_PHYINT_LEVEL_L	0x00000000
#define TCEI_PHYINT_LEVEL_H	0x00000001
#define TCEI_PHYINT_EDGE_FALL	0x00000002
#define TCEI_PHYINT_EDGE_RISE	0x00000003

/* MAC RX enable register */

#define TCEI_RXEN_EN		0x00000001	/* MAC RX enable */

/* RX flow control register */

#define TCEI_RXFC_EN		0x80000000	/* flow control enable */

/* Pause packet request register */

#define TCEI_PAUSEREQ_TX	0x80000000	/* send a pause frame */

/* RX mode register */

#define TCEI_RXMODE_UCAST_FILT	0x80000000	/* 0 = promisc */
#define TCEI_RXMODE_MCAST_FILT	0x40000000	/* 0 = receive all multicasts */
#define TCEI_RXMODE_FIFO_EMPTY	0x0000C000	/* FIFO full alarm */
#define TCEI_RXMODE_FIFO_FULL	0x00003000	/* FIFO empty alarm */
#define TCEI_RXMODE_FIFO_READ	0x00000E00	/* read trigger */

#define TCIE_RX_EMPTY_16	0x00000000
#define TCIE_RX_EMPTY_32	0x00004000
#define TCIE_RX_EMPTY_64	0x00008000
#define TCIE_RX_EMPTY_128	0x0000C000

#define TCIE_RX_FULL_16		0x00000000
#define TCIE_RX_FULL_32		0x00001000
#define TCIE_RX_FULL_64		0x00002000
#define TCIE_RX_FULL_128	0x00003000

#define TCEI_RX_READ_16		0x00000000
#define TCEI_RX_READ_32		0x00000200
#define TCEI_RX_READ_64		0x00000400
#define TCEI_RX_READ_128	0x00000600
#define TCEI_RX_READ_256	0x00000800
#define TCEI_RX_READ_512	0x00000A00
#define TCEI_RX_READ_1024	0x00000C00
#define TCEI_RX_READ_2048	0x00000E00

/* TX mode register */

#define TCEI_TXMODE_NOCOLLRTRY	0x80000000	/* nocollision retry */
#define TCEI_TXMODE_JUMBO	0x40000000	/* jumbo frame enable */
#define TCEI_TXMODE_STORENFWD	0x20000000	/* store and forward mode */
#define TCEI_TXMODE_RUNT	0x10000000	/* runt frame enable */
#define TCEI_TXMODE_LCOLLRTRY	0x08000000	/* late coll. retry */
#define TCEI_TXMODE_FIFO_START	0x0000C000	/* TX start threshold */
#define TCEI_TXMODE_FIFO_EMPTY	0x00003800	/* FIFO empty alarm */
#define TCEI_TXMODE_FIFO_FULL	0x00000600	/* FIFO full alarm */

#define TCEI_TX_START_16	0x00000000
#define TCEI_TX_START_32	0x00004000
#define TCEI_TX_START_64	0x0000C000
#define TCEI_TX_START_128	0x00008000

#define TCEI_TX_EMPTY_16	0x00000000
#define TCEI_TX_EMPTY_32	0x00000800
#define TCEI_TX_EMPTY_64	0x00001000
#define TCEI_TX_EMPTY_128	0x00001800
#define TCEI_TX_EMPTY_256	0x00002000
#define TCEI_TX_EMPTY_512	0x00002800
#define TCEI_TX_EMPTY_1024	0x00003000
#define TCEI_TX_EMPTY_2048	0x00003800

#define TCEI_TX_FULL_16		0x00000000
#define TCEI_TX_FULL_32		0x00000200
#define TCEI_TX_FULL_64 	0x00000400
#define TCEI_TX_FULL_128	0x00000600

/* RX FIFO status register */

#define TCEI_RXFIFOSTS_EMPTY	0x80000000	/* almost empty */
#define TCEI_RXFIFOSTS_READ	0x40000000	/* read trigger */
#define TCEI_RXFIFOSTS_WORDCNT	0x0FFC0000	/* FIFO word count */
#define TCEI_RXFIFOSTS_RXING	0x00010000	/* RX in progress */

/* TX FIFO status register */

#define TCEI_TXFIFOSTS_FULL	0x80000000	/* almost full */
#define TCEI_TXFIFOSTS_EMPTY	0x40000000	/* almost empty */
#define TCEI_TXFIFOSTS_STS	0x38000000	/* status */
#define TCEI_TXFIFOSTS_FRMCNT	0x07000000	/* frame count */

#define TCEI_TXFIFO_FULL	0x38000000
#define TCEI_TXFIFO_WRITE_EN	0x28000000
#define TCEI_TXFIFO_COMPLETE	0x30000000
#define TCEI_TXFIFO_ACCNEW	0x20000000
#define TCEI_TXFIFO_STOP	0x00000000

/* MAC address mask register */

#define TCEI_PARMASK_BUSY	0x80000000	/* Mask update in progress */
#define TCEI_PARMASK_15		0x00008000	/* Addr 15 masked (disabled) */
#define TCEI_PARMASK_14		0x00004000	/* Addr 14 masked (disabled) */
#define TCEI_PARMASK_13		0x00002000	/* Addr 13 masked (disabled) */
#define TCEI_PARMASK_12		0x00001000	/* Addr 12 masked (disabled) */
#define TCEI_PARMASK_11		0x00000800	/* Addr 11 masked (disabled) */
#define TCEI_PARMASK_10		0x00000400	/* Addr 10 masked (disabled) */
#define TCEI_PARMASK_09		0x00000200	/* Addr 9 masked (disabled) */
#define TCEI_PARMASK_08		0x00000100	/* Addr 8 masked (disabled) */
#define TCEI_PARMASK_07		0x00000080	/* Addr 7 masked (disabled) */
#define TCEI_PARMASK_06		0x00000040	/* Addr 6 masked (disabled) */
#define TCEI_PARMASK_05		0x00000020	/* Addr 5 masked (disabled) */
#define TCEI_PARMASK_04		0x00000010	/* Addr 4 masked (disabled) */
#define TCEI_PARMASK_03		0x00000008	/* Addr 3 masked (disabled) */
#define TCEI_PARMASK_02		0x00000004	/* Addr 2 masked (disabled) */
#define TCEI_PARMASK_01		0x00000002	/* Addr 1 masked (disabled) */
#define TCEI_PARMASK_00		0x00000001	/* Addr 0 masked (disabled) */

#define TCEI_PARMASK_ALL	0x0000FFFF

#define TCEI_PARSEL(x)		(1 << (x))

/* MDIO register */

#define TCEI_MIIM_OP		0x04000000	/* MDIO operation  */
#define TCEI_MIIM_PHYADDR	0x03E00000	/* PHY address */
#define TCEI_MIIM_REGADDR	0x001F0000	/* register address */
#define TCEI_MIIM_DATA		0x0000FFFF	/* data */

#define TCEI_MIIM_READ		0x00000000
#define TCEI_MIIM_WRITE		0x04000000
#define TCEI_MIIM_IDLE		0x04000000

#define TCEI_PHYADDR(x)		(((x) << 21) & TCEI_MIIM_PHYADDR)
#define TCEI_REGADDR(x)		(((x) << 16) & TCEI_MIIM_REGADDR)

/* Wake on lan address load register */

#define TCEI_WOLLOAD_LOAD	0x00000001

/* RGMII status register */

#define TCEI_RGMIISTS_LINK	0x00000008	/* 1 = up, 0 = down */
#define TCEI_RGMIISTS_SPEED	0x00000006	/* clock speed */
#define TCEI_RGMIISTS_FDX	0x00000001	/* 1 = fdx, 0 = hdx */

#define TCEI_RGMIISTS_2_5MHZ	0x00000000
#define TCEI_RGMIISTS_25MHZ	0x00000002
#define TCEI_RGMIISTS_125MHZ	0x00000004

/* RGMII control register */

#define TCEI_RGMIICTL_CRS	0x00000010	/* carrier sense error */
#define TCEI_RGMIICTL_SPEED	0x0000000C	/* clock speed */
#define TCEI_RGMIICTL_RPM	0x00000002	/* reduced pin mode */
#define TCEI_RGMIICTL_CHIPTYPE	0x00000001	/* internal/external input */

#define TCEI_RGMIICTL_2_5MHZ	0x0000000C
#define TCEI_RGMIICTL_25MHZ	0x00000008
#define TCEI_RGMIICTL_125MHZ	0x00000000

/* DMA control register */

#define TCEI_DMACTL_RX		0x00000002	/* RX DMA enable */
#define TCEI_DMACTL_TX		0x00000001	/* TX DMA enable */

/* Software reset register */

#define TCEI_SRST_RESET		0x00000001	/* Assert reset */

/*
 * DMA descriptor formats
 * RX and TX descriptors are 16 bytes in size and the base address
 * of the descriptor rings must be 16-byte aligned. RX and TX buffer
 * addresses must be 64-byte aligned (last 6 bits not decoded).
 * Support for scatter/gather is conspicously absent: all frames
 * must reside in a single buffer (there are no start-of-frame or
 * end-of-frame marker bits).
 */

/* RX descriptor format */

typedef struct tcei_rx_desc
    {
    volatile UINT32	tcei_bufaddr;
    volatile UINT32	tcei_tcpipsts;
    volatile UINT16	tcei_rxwords;
    volatile UINT16	tcei_macsts;
    volatile UINT8	tcei_dmasts;
    volatile UINT8	tcei_rsvd1;
    volatile UINT16	tcei_rsvd0;
    } TCEI_RX_DESC;

/* DMA status field */

#define TCEI_RXDESC_DMASTS_BUSERR	0x01

/* GMAC status field */

#define TCEI_RXDESC_MACSTS_CRCERR	0x0001
#define TCEI_RXDESC_MACSTS_NBLERR	0x0002
#define TCEI_RXDESC_MACSTS_ALIGN	0x0004
#define TCEI_RXDESC_MACSTS_RUNT		0x0008
#define TCEI_RXDESC_MACSTS_GIANT	0x0010
#define TCEI_RXDESC_MACSTS_PROMISC	0x0020
#define TCEI_RXDESC_MACSTS_UCAST	0x0040
#define TCEI_RXDESC_MACSTS_MCAST	0x0080
#define TCEI_RXDESC_MACSTS_BCAST	0x0100
#define TCEI_RXDESC_MACSTS_PAUSE	0x0200

/* TCP/IP statis field */

#define TCEI_RXDESC_TCPIPSTS_VTAG	0x00000001
#define TCEI_RXDESC_TCPIPSTS_PPPOE	0x00000002
#define TCEI_RXDESC_TCPIPSTS_8023	0x00000004
#define TCEI_RXDESC_TCPIPSTS_IPTYPE	0x00000008 /* v4 vs v6 */
#define TCEI_RXDESC_TCPIPSTS_OUTLIST	0x00000010 /* v6 not in list */
#define TCEI_RXDESC_TCPIPSTS_IPV6HERR	0x00000020 /* v6 header error */
#define TCEI_RXDESC_TCPIPSTS_TCPCSUMERR	0x00000040 /* TCP/UDP csum error */
#define TCEI_RXDESC_TCPIPSTS_IPCSUMERR	0x00000080 /* IPv4 csum error */
#define TCEI_RXDESC_TCPIPSTS_UCAST	0x00000100 /* IP unicast */
#define TCEI_RXDESC_TCPIPSTS_MCAST	0x00000200 /* IP multicast */
#define TCEI_RXDESC_TCPIPSTS_BCAST	0x00000400 /* IP broadcast */
#define TCEI_RXDESC_TCPIPSTS_PPOEID	0xFFFF0000 /* PPPoE session ID */

/* RX length field */

#define TCEI_RXWORDS_WORDS		0xFFFC /* # of 32-bit words */
#define TCEI_RXWORDS_BYTES		0x0003 /* # of extra bytes */

#define TCEI_RXLEN(x)	\
	(((x) & TCEI_RXLEN_WORDS) >> 2) + ((x) & TCEI_RXLEN_BYTES))

/* TX descriptor format */

typedef struct tcei_tx_desc
    {
    volatile UINT32	tcei_bufaddr;
    volatile UINT16	tcei_length;
    volatile UINT16	tcei_rsvd1;
    volatile UINT16	tcei_txwords;
    volatile UINT16	tcei_frmctl;
    volatile UINT8	tcei_dmasts;
    volatile UINT8	tcei_rsvd0;
    volatile UINT16	tcei_macsts;
    } TCEI_TX_DESC;


/* GMAC status field */

#define TCEI_TXDESC_MACSTS_RTRYCNT	0x000F /* collision retries */
#define TCEI_TXDESC_MACSTS_UFLOW	0x0010 /* TX underrun */
#define TCEI_TXDESC_MACSTS_LATECOL	0x0020 /* late collision */
#define TCEI_TXDESC_MACSTS_RUNT		0x0040
#define TCEI_TXDESC_MACSTS_GIANT	0x0080
#define TCEI_TXDESC_MACSTS_NOCARR	0x0100 /* carrier sense err */
#define TCEI_TXDESC_MACSTS_MULTICOLL	0x0200 /* multiple collisions */
#define TCEI_TXDESC_MACSTS_ONECOLL	0x0400 /* single collision */
#define TCEI_TXDESC_MACSTS_EXCOLL	0x0800 /* retry count exceeded */
#define TCEI_TXDESC_MACSTS_ABORT	0x1000 /* TX aborted */
#define TCEI_TXDESC_MACSTS_DONE		0x2000 /* TX done */

#define TCEI_TXDESC_MACSTS_ERRSUM	0x10F0

/* DMA status field */

#define TCEI_TXDESC_DMASTS_BUSERR	0x01

/* Frame control field */

#define TCEI_TXDESC_FRMCTL_PAD		0x0001 /* autopad */
#define TCEI_TXDESC_FRMCTL_HASCRC	0x0002 /* already has CRC */
#define TCEI_TXDESC_FRMCTL_HASVTAG	0x0004 /* has VLAN tag */
#define TCEI_TXDESC_FRMCTL_NOACC	0x0008 /* TCP/IP accel disable */


#define TCEI_MTU        1500
#define TCEI_JUMBO_MTU  9000
#define TCEI_CLSIZE     10500
#define TCEI_NAME	"tcei"
#define TCEI_TIMEOUT    10000

#define TCEI_RXINTRS				\
    (TCEI_ISR_RXDONE|TCEI_ISR_RXDMADONE|TCEI_ISR_RXFIFOERR|	\
    TCEI_ISR_RXNODESC|TCEI_ISR_RXFRAMEERR|TCEI_ISR_RXDMAERR)
#define TCEI_TXINTRS				\
    (TCEI_ISR_TXDONE|TCEI_ISR_TXDMADONE|TCEI_ISR_TXFIFOERR|	\
    TCEI_ISR_TXDMAERR|TCEI_ISR_PAUSEDONE) 

#define TCEI_INTRS (TCEI_RXINTRS|TCEI_TXINTRS)


#define TCEI_ALIGN(x)    (char *)(((UINT32)(x) + 63) & ~63)

#define TCEI_ADJ(x)      \
        (x)->m_data = TCEI_ALIGN((x)->m_data)

#define TCEI_INC_DESC(x, y)       (x) = ((x) + 1) % (y)

#define TCEI_PAR_CNT	16

#define TCEI_MAXFRAG		1
#define TCEI_MAX_RX		16
#define TCEI_RX_DESC_CNT	128
#define TCEI_TX_DESC_CNT	128

/*
 * Private adapter context structure.
 */

typedef struct tcei_drv_ctrl
    {
    END_OBJ		tceiEndObj;
    VXB_DEVICE_ID	tceiDev;
    void *		tceiHandle;
    void *		tceiBar;
    void *		tceiMuxDevCookie;

    JOB_QUEUE_ID	tceiJobQueue;
    QJOB		tceiIntJob;
    atomic_t		tceiIntPending;

    QJOB		tceiRxJob;
    atomic_t		tceiRxPending;

    QJOB		tceiTxJob;
    atomic_t		tceiTxPending;
    UINT8		tceiTxCur;
    UINT8		tceiTxLast;
    volatile BOOL	tceiTxStall;
    UINT16		tceiTxThresh;

    BOOL		tceiPolling;
    M_BLK_ID		tceiPollBuf;
    UINT32		tceiIntrs;
    UINT32		tceiIntMask;

    UINT8		tceiAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	tceiCaps;

    SEM_ID		tceiDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	tceiMediaList;
    END_ERR		tceiLastError;
    UINT32		tceiCurMedia;
    UINT32		tceiCurStatus;
    VXB_DEVICE_ID	tceiMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	tceiParentTag;

    VXB_DMA_TAG_ID	tceiRxDescTag;
    VXB_DMA_MAP_ID	tceiRxDescMap;
    TCEI_RX_DESC *	tceiRxDescMem;

    VXB_DMA_TAG_ID	tceiTxDescTag;
    VXB_DMA_MAP_ID	tceiTxDescMap;
    TCEI_TX_DESC *	tceiTxDescMem;

    VXB_DMA_TAG_ID	tceiMblkTag;

    VXB_DMA_MAP_ID	tceiRxMblkMap[TCEI_RX_DESC_CNT];
    VXB_DMA_MAP_ID	tceiTxMblkMap[TCEI_TX_DESC_CNT];

    M_BLK_ID		tceiRxMblk[TCEI_RX_DESC_CNT];
    M_BLK_ID		tceiTxMblk[TCEI_TX_DESC_CNT];

    UINT32		tceiTxProd;
    UINT32		tceiTxCons;
    UINT32		tceiTxFree;
    UINT32		tceiRxIdx;

    int			tceiMaxMtu;
    } TCEI_DRV_CTRL;

#undef CSR_READ_4
#undef CSR_WRITE_4

#define TCEI_BAR(p)	((TCEI_DRV_CTRL *)(p)->pDrvCtrl)->tceiBar
#define TCEI_HANDLE(p)	((TCEI_DRV_CTRL *)(p)->pDrvCtrl)->tceiHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (TCEI_HANDLE(pDev), (UINT32 *)((char *)TCEI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (TCEI_HANDLE(pDev),                             \
        (UINT32 *)((char *)TCEI_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIntelTopcliffEndh */
