/* mvYukonIIVxbEnd.h - header file for Marvell Yukon II VxBus END driver */

/*
 * Copyright (c) 2006-2007, 2010, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01k,16sep13,xms  fix CONSTANT_EXPRESSION_RESULT error. (WIND00414265)
01j,12may10,c_t  Modify YN_ADDR_HI define to 32bit safe(WIND00114514)
01i,19jun07,wap  Make this driver SMP safe
01h,07jan07,wap  Add support for additional Marvell and D-Link adapters
01g,05jan07,wap  Add additional Yukon II device IDs
01f,04jan07,wap  Add connector type to control structure
01e,04jan07,wap  Correct YN_TXRB_LINK_OFFSET and YN_TXQ_LINK_OFFSET
01d,07dec06,wap  Add jumbo frame support
01c,21aug06,wap  Harden polled mode support
01c,16nov06,wap  Merge in updates from development branch
01b,29jul06,wap  Add TCP/IP checksum offload and VLAN tagging support
01a,23jun06,wap  written
*/

#ifndef __INCmvYukonIIVxbEndh
#define __INCmvYukonIIVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void ynRegister (void);

#ifndef BSP_VERSION


#define MV_VENDORID		0x11AB

/* Yukon EC PCIe */

#define MV_DEVICEID_8050	0x4361
#define MV_DEVICEID_8052	0x4360
#define MV_DEVICEID_8053	0x4362
#define MV_DEVICEID_8055	0x4363
#define MV_DEVICEID_8056	0x4364
#define MV_DEVICEID_8058	0x436A

/* Yukon Extreme */

#define MV_DEVICEID_8070	0x4365
#define MV_DEVIDEID_8071	0x436B
#define MV_DEVICEID_C032	0x4367
#define MV_DEVICEID_C033	0x4356
#define MV_DEVICEID_C034	0x4368
#define MV_DEVICEID_C036	0x4366
#define MV_DEVICEID_C042	0x4369

/*
 * 802x is Yukon II PCI-X (Yukon XL)
 * 806x is Yukon II PCIe
 */

#define MV_DEVICEID_8021CU	0x4340
#define MV_DEVICEID_8022CU	0x4341
#define MV_DEVICEID_8061CU	0x4342
#define MV_DEVICEID_8062CU	0x4343
#define MV_DEVICEID_8021SX	0x4344
#define MV_DEVICEID_8022SX	0x4345
#define MV_DEVICEID_8061SX	0x4346
#define MV_DEVICEID_8062SX	0x4347

/* Yukon FE (10/100) */

#define MV_DEVICEID_8035	0x4350
#define MV_DEVICEID_8036	0x4351
#define MV_DEVICEID_8038	0x4352
#define MV_DEVICEID_8039	0x4353

#define SK_VENDORID		0x1148

#define SK_DEVICEID_YK2		0x9000
#define SK_DEVICEID_YK2EXPR	0x9e00

#define DLINK_VENDORID		0x1186

#define DLINK_DEVICEID_DGE550SX	0x4001
#define DLINK_DEVICEID_DGE560T	0x4b00

#define YN_CHIP_ID_YUKON	0xB0    /* Yukon */
#define YN_CHIP_ID_YUKON_LITE	0xB1    /* Yukon-Lite (Rev. A1-A3) */
#define YN_CHIP_ID_YUKON_LP	0xB2    /* Yukon-LP */
#define YN_CHIP_ID_YUKON_XL	0xB3    /* Yukon-2 XL */
#define YN_CHIP_ID_YUKON_EC_U	0xB4    /* Yukon-2 EC Ultra */
#define YN_CHIP_ID_YUKON_EX	0xB5    /* Yukon-2 Extreme */
#define YN_CHIP_ID_YUKON_EC	0xB6    /* Yukon-2 EC */
#define YN_CHIP_ID_YUKON_FE	0xB7    /* Yukon-2 FE */

#define YN_CHIP_REV_YU_LITE_A1	3	/* Yukon-Lite A1,A2 */
#define YN_CHIP_REV_YU_LITE_A3	7	/* Yukon-Lite A3 */

#define YN_CHIP_REV_YU_XL_A0	0	/* Yukon-2 A0 */
#define YN_CHIP_REV_YU_XL_A1	1	/* Yukon-2 A1 */
#define YN_CHIP_REV_YU_XL_A2	2	/* Yukon-2 A2 */
#define YN_CHIP_REV_YU_XL_A3	3	/* Yukon-2 A3 */

#define YN_CHIP_REV_YU_EC_A1	0	/* Yukon-EC A0,A1 */
#define YN_CHIP_REV_YU_EC_A2	1	/* Yukon-EC A2 */
#define YN_CHIP_REV_YU_EC_A3	2	/* Yukon-EC A3 */

#define YN_CHIP_REV_YU_EC_U_A0	1	/* Yukon-EC Ultra A0 */
#define YN_CHIP_REV_YU_EC_U_A1	2	/* Yukon-EC Ultra A1 */

#define YN_PMD_1000BASELX	0x4C
#define YN_PMD_1000BASESX	0x53
#define YN_PMD_1000BASECX	0x43
#define YN_PMD_1000BASET	0x54

/*
 * The Yukon II has a large number of registers, divided into
 * 128 byte blocks. When accessing the device via the memory mapped
 * BAR, all registers are directly addressible. When using the
 * I/O space BAR, only 256 bytes are available, and the register
 * blocks are accessed indirectly by mapping the various blocks
 * in and out using the RAP register.
 *
 * Note that there are really two sets of registers. The first
 * large batch is for the PCI and DMA handling, which corresponds
 * to the functionality of the GEnesis controller. The second batch
 * are the Marvell GMAC registers. Unlike the GEnesis however,
 * the Marvell GMAC registers are mapped into the same space with
 * everything else (the GEnesis required you to access the XMAC's
 * register via an intirect I/O port).
 */

/* Block 0 */

#define YN_RAP		0x0000
#define YN_CSR		0x0004
#define YN_PWR		0x0006
#define YN_ISR		0x0008  /* interrupt source */
#define YN_IMR		0x000C  /* interrupt mask */
#define YN_IESR		0x0010  /* interrupt hardware error source */
#define YN_IEMR		0x0014  /* interrupt hardware error mask */
#define YN_ISSR0	0x0018  /* special interrupt source 0 */
#define YN_ISSR1	0x001C  /* special interrupt source 1 */
#define YN_ISSR2	0x0020  /* special interrupt source 2 */
#define YN_ISRENTER	0x0024
#define YN_ISRLEAVE	0x0028
#define YN_INTCTL	0x002C
#define YN_SPI_CSR	0x0060
#define YN_SPI_ADDR	0x0064
#define YN_SPI_DATA	0x0068
#define YN_SPI_VIDDID	0x0070
#define YN_SPI_LDCFG	0x0074
#define YN_SPI_OPCODE1	0x0078
#define YN_SPI_OPCODE2	0x007C

#define YN_CSR_SW_RESET                 0x0001
#define YN_CSR_SW_UNRESET               0x0002
#define YN_CSR_MASTER_RESET             0x0004
#define YN_CSR_MASTER_UNRESET           0x0008
#define YN_CSR_MASTER_STOP              0x0010
#define YN_CSR_MASTER_DONE              0x0020
#define YN_CSR_SW_IRQ_CLEAR             0x0040
#define YN_CSR_SW_IRQ_SET               0x0080
#define YN_CSR_LED0_OFF			0x0100
#define YN_CSR_LED1_ON			0x0200
#define YN_CSR_ASF_ENB_CLR		0x1000
#define YN_CSR_ASF_ENB_SET		0x2000
#define YN_CSR_VAUX_AVAIL		0x4000
#define YN_CSR_VMAIN_AVAIL		0x8000

#define YN_ISR_TX1_AS_CHECK		0x00000001
#define YN_ISR_TX1_S_CHECK		0x00000002
#define YN_ISR_RX1_CHECK		0x00000004
#define YN_ISR_GMAC1			0x00000008
#define YN_ISR_GPHY1			0x00000010
#define YN_ISR_TX2_AS_CHECK		0x00000100
#define YN_ISR_TX2_S_CHECK		0x00000200
#define YN_ISR_RX2_CHECK		0x00000400
#define YN_ISR_GMAC2			0x00000800
#define YN_ISR_GPHY2			0x00001000
#define YN_ISR_TIMER			0x01000000
#define YN_ISR_SW			0x02000000
#define YN_ISR_TWSI_RDY			0x04000000
#define YN_ISR_POLLING_CHECK		0x08000000
#define YN_ISR_RSVD			0x10000000
#define YN_ISR_ASF			0x20000000
#define YN_ISR_STBURST			0x40000000
#define YN_ISR_HWERR			0x80000000

#define YN_ISRHW_TX1_A_TCPLEN		0x00000001
#define YN_ISRHW_TX1_S_TCPLEN		0x00000002
#define YN_ISRHW_RX1_PARITY		0x00000004
#define YN_ISRHW_GMAC1_PARITY		0x00000008
#define YN_ISRHW_RAMWR1_PARITY		0x00000010
#define YN_ISRHW_RAMRD1_PARITY		0x00000020
#define YN_ISRHW_TX2_A_TCPLEN		0x00000100
#define YN_ISRHW_TX2_S_TCPLEN		0x00000200
#define YN_ISRHW_RX2_PARITY		0x00000400
#define YN_ISRHW_GMAC2_PARITY		0x00000800
#define YN_ISRHW_RAMWR2_PARITY		0x00001000
#define YN_ISRHW_RAMRD2_PARITY		0x00002000
#define YN_ISRHW_NO_PE			0x01000000
#define YN_ISRHW_PE			0x02000000
#define YN_ISRHW_STATUS			0x04000000
#define YN_ISRHW_MASTER			0x08000000
#define YN_ISRHW_SENSOR			0x10000000
#define YN_ISRHW_TSTAMP_OFLOW		0x20000000

#define YN_INTCTL_ISR_ENTER		0x00000001
#define YN_INTCTL_ISR_LEAVE		0x00000002
#define YN_INTCTL_ISR_STS		0x00000004
#define YN_INTCTL_ISR_MASK		0x00000008

/* Block 1 unused */

/* Block 2 */

#define YN_MAC0_0       0x0100
#define YN_MAC0_1       0x0104
#define YN_MAC1_0       0x0108
#define YN_MAC1_1       0x010C
#define YN_MAC2_0       0x0110
#define YN_MAC2_1       0x0114
#define YN_CONNTYPE     0x0118
#define YN_PMDTYPE      0x0119
#define YN_CONFIG       0x011A
#define YN_CHIPVER      0x011B
#define YN_EPROM0       0x011C	/* RAM size */
#define YN_EPROM1       0x011D	/* Clock gating */
#define YN_EPROM2       0x011E	/* application info */
#define YN_CORECLK	0x0120
#define YN_CLKDIV_CTL	0x0121
#define YN_CLKDIV_VAL	0x0122
#define YN_RSVD1	0x0123
#define YN_EP_DATA      0x0124
#define YN_EP_LOADCTL   0x0128
#define YN_EP_LOADTST   0x0129
#define YN_TIMERINIT    0x0130
#define YN_TIMER        0x0134
#define YN_TIMERCTL     0x0138
#define YN_TIMERTST     0x0139
#define YN_IMTIMERINIT  0x0140
#define YN_IMTIMER      0x0144
#define YN_IMTIMERCTL   0x0148
#define YN_IMTIMERTST   0x0149
#define YN_IMMR         0x014C
#define YN_IHWEMR       0x0150
#define YN_TESTCTL1     0x0158
#define YN_TESTCTL2     0x0159
#define YN_GPIO         0x015C
#define YN_TWSIHWCTL	0x0160
#define YN_TWSIHWDATA	0x0164
#define YN_TWSIHWIRQ	0x0168
#define YN_TWSISW	0x016C
#define YN_PEXPHY_DATA	0x0170	/* PCI express PHY data */
#define YN_PEXPHY_ADDR	0x0172	/* PCI express PHY address */

/* EPROM0 (ram size) register, in 4K blocks */

#define YN_EPROM0_RAMSIZE	0xFF

/* EPROM1 (clock gating) register */

#define YN_EPROM1_BONDSTS_1	0x80
#define YN_EPROM1_LINK_DIS_1	0x40
#define YN_EPROM1_CORECLK_DIS_1	0x20
#define YN_EPROM1_BIUCLK_DIS_1	0x10
#define YN_EPROM1_BONDSTS_0	0x08
#define YN_EPROM1_LINK_DIS_0	0x04
#define YN_EPROM1_CORECLK_DIS_0	0x02
#define YN_EPROM1_BIUCLK_DIS_0	0x01

/* EPROM2 (application information) register */

#define YN_EPROM2_LINK0		0x0001	/* link 0 available */
#define YN_EPROM2_LINK1		0x0002	/* link 1 available */
#define YN_EPROM2_LEDINFO	0x001C

/* Core clock control register */

#define YN_CORECLK_CLKDIV_DIS	0x00000001
#define YN_CORECLK_CLKDIV_ENB	0x00000002
#define YN_CORECLK_CLKSEL	0x001F0000
#define YN_CORECLK_CLKDIV	0x00E00000

/* Timer control register */

#define YN_TIMER_CLR_IRQ	0x00000001
#define YN_TIMER_STOP		0x00000002
#define YN_TIMER_START		0x00000004
#define YN_TIMER_STEP		0x00000010
#define YN_TIMER_TST_OFFLINE	0x00000020
#define YN_TIMER_TST_ONLINE	0x00000040

/* Test control register 1 */

#define YN_TESTCTL1_CFGWR_OFF		0x01
#define YN_TESTCTL1_CFGWR_ON		0x02
#define YN_TESTCTL1_ADDRPERR_TGT	0x04
#define YN_TESTCTL1_ADDRPERR_MST	0x08
#define YN_TESTCTL1_DATAWRPERR_TGT	0x10
#define YN_TESTCTL1_DATARDPERR_TGT	0x20
#define YN_TESTCTL1_DATAWRPERR_MST	0x40
#define YN_TESTCTL1_DATARDPERR_MST	0x80

/*
 * Block 3 Ram interface
 */

#define YN_RAMADDR      0x0180
#define YN_RAMDATA0     0x0184
#define YN_RAMDATA1     0x0188
#define YN_TO0          0x0190
#define YN_TO1          0x0191
#define YN_TO2          0x0192
#define YN_TO3          0x0193
#define YN_TO4          0x0194
#define YN_TO5          0x0195
#define YN_TO6          0x0196
#define YN_TO7          0x0197
#define YN_TO8          0x0198
#define YN_TO9          0x0199
#define YN_TO10         0x019A
#define YN_TO11         0x019B
#define YN_RITIMEO_TMR  0x019C
#define YN_RAMCTL       0x01A0
#define YN_RITIMER_TST  0x01A2

#define YN_RAMCTL_RESET         0x0001
#define YN_RAMCTL_UNRESET       0x0002
#define YN_RAMCTL_CLR_IRQ_WPAR  0x0100
#define YN_RAMCTL_CLR_IRQ_RPAR  0x0200

#define YN_RAM_LINK_OFFSET	0x40

#define YN_RAM(p, r)	(((p)->ynLinkNum * YN_RAM_LINK_OFFSET) + (r))

/* Block 4 and 5 Transmit arbiter and RSS */

#define YN_TXA_TIMERINIT	0x0200
#define YN_TXA_TIMERVAL		0x0204
#define YN_TXA_LIMITINIT	0x0208
#define YN_TXA_LIMITCNT		0x020C
#define YN_TXA_CTLTST		0x0210
#define YN_RSS_KEY0		0x0220
#define YN_RSS_KEY1		0x0224
#define YN_RSS_KEY2		0x0228
#define YN_RSS_KEY3		0x022C

#define YN_TXACTLTST_OFFLINE		0x00000001
#define YN_TXACTLTST_ONLINE		0x00000002
#define YN_TXACTLTST_RATECTL_OFFLINE	0x00000004
#define YN_TXACTLTST_RATECTL_ONLINE	0x00000008
#define YN_TXACTLTST_ALLOC_OFFLINE	0x00000010
#define YN_TXACTLTST_ALLOC_ONLINE	0x00000020
#define YN_TXACTLTST_FSYNC_OFFLINE	0x00000040
#define YN_TXACTLTST_FSYNC_ONLINE	0x00000080
#define YN_TXACTLTST_LIMCNT_STEP	0x00000100
#define YN_TXACTLTST_LIMCNTTST_ONLINE	0x00000200
#define YN_TXACTLTST_LIMCNTTST_OFFLINE	0x00000400
#define YN_TXACTLTST_INTCNT_STEP	0x00000800
#define YN_TXACTLTST_INTCNTTST_ONLINE	0x00001000
#define YN_TXACTLTST_INTCNTTST_OFFLINE	0x00002000
#define YN_TXACTLTST_PRIO_SYNC_RAMBUFF	0x00008000

#define YN_TXA_LINK_OFFSET	0x80

#define YN_TXA(p, r)	(((p)->ynLinkNum * YN_TXA_LINK_OFFSET) + (r))

/* Block 7 PCI express register mirror, lower half */

#define YN_PCIE_CFG	0x0300

/* Block 8 and 9 RX queues */

#define YN_RXQ_BUFCNT		0x0400
#define YN_RXQ_BUFCTL		0x0402
#define YN_RXQ_RSSHASH		0x0404
#define YN_RXQ_RXBUF_LO		0x0408
#define YN_RXQ_RXBUF_HI		0x040C
#define YN_RXQ_RXSTAT		0x0410
#define YN_RXQ_TIMESTAMP	0x0414
#define YN_RXQ_CSUM1		0x0418
#define YN_RXQ_CSUM2		0x041A
#define YN_RXQ_CSUM1_START	0x041C
#define YN_RXQ_CSUM2_START	0x041E
#define YN_RXQ_VLAN		0x0420
#define YN_RXQ_RXLEN		0x0422
#define YN_RXQ_DONEIDX		0x0424
#define YN_RXQ_REQADDR_LO	0x0428
#define YN_RXQ_REQADDR_HI	0x042C
#define YN_RXQ_CURBYTES		0x0430
#define YN_RXQ_BMU_CSR		0x0434
#define YN_RXQ_BMU_TST		0x0438
#define YN_RXQ_BMU_STATE	0x043C
#define YN_RXQ_FIFO_WMRK	0x0440
#define YN_RXQ_FIFO_ALIGN	0x0442
#define YN_RXQ_FIFO_RDPTR_SHD	0x0444
#define YN_RXQ_FIFO_RDLVL_SHD	0x0446
#define YN_RXQ_FIFO_RDPTR	0x0448
#define YN_RXQ_FIFO_RDLVL	0x0446
#define YN_RXQ_FIFO_WRPTR	0x044C
#define YN_RXQ_FIFO_WRPTR_SHD	0x044D
#define YN_RXQ_FIFO_WRLVL_SHD	0x044F

#define YN_RXQBMUCSR_RESET		0x00000001
#define YN_RXQBMUCSR_UNRESET		0x00000002
#define YN_RXQBMUCSR_OFFLINE		0x00000004
#define YN_RXQBMUCSR_ONLINE		0x00000008
#define YN_RXQBMUCSR_FIFO_RESET		0x00000010
#define YN_RXQBMUCSR_FIFO_UNRESET	0x00000020
#define YN_RXQBMUCSR_FIFO_OFFLINE	0x00000040
#define YN_RXQBMUCSR_FIFO_ONLINE	0x00000080
#define YN_RXQBMUCSR_START		0x00000100
#define YN_RXQBMUCSR_STOP		0x00000200
#define YN_RXQBMUCSR_CLR_IRQ_BADOP	0x00000400
#define YN_RXQBMUCSR_CLR_IRQ_PERR	0x00000800
#define YN_RXQBMUCSR_RXCSUM_OFFLINE	0x00001000
#define YN_RXQBMUCSR_RXCSUM_ONLINE	0x00002000
#define YN_RXQBMUCSR_RSS_OFFLINE	0x00004000
#define YN_RXQBMUCSR_RSS_ONLINE		0x00008000
#define YN_RXQBMUCSR_PKT_IS_IP		0x20000000
#define YN_RXQBMUCSR_PKT_IS_TCP		0x40000000
#define YN_RXQBMUCSR_BMU_IDLE		0x80000000

#define YN_RXQBMU_UNRESET					\
	(YN_RXQBMUCSR_UNRESET|YN_RXQBMUCSR_FIFO_RESET|YN_RXQBMUCSR_OFFLINE)
#define YN_RXQBMU_INIT						\
	(YN_RXQBMUCSR_CLR_IRQ_BADOP|YN_RXQBMUCSR_CLR_IRQ_PERR|	\
	 YN_RXQBMUCSR_START|YN_RXQBMUCSR_FIFO_UNRESET|		\
	 YN_RXQBMUCSR_ONLINE)
#define YN_RXQBMU_ONLINE	YN_RXQBMUCSR_FIFO_ONLINE

/* RX Prefetch unit control */

#define YN_RXQP_PREFETCH_CTL	0x0450
#define YN_RXQP_LASTIDX		0x0454
#define YN_RXQP_LISTADDR_LO	0x0458
#define YN_RXQP_LISTADDR_HI	0x045C
#define YN_RXQP_GETIDX		0x0460
#define YN_RXQP_PUTIDX		0x0464
#define YN_RXQP_FIFO_WRPTR	0x0470
#define YN_RXQP_FIFO_WRPTR_SHD	0x0472
#define YN_RXQP_FIFO_RDPTR	0x0474
#define YN_RXQP_FIFO_WMRK	0x0478
#define YN_RXQP_MASTER_RQBYTES	0x047A
#define YN_RXQP_FIFO_LVL	0x047C
#define YN_RXQP_FIFO_LVL_SHD	0x047E

#define YN_RXPRFCTL_RESET		0x00000001
#define YN_RXPRFCTL_UNRESET		0x00000002
#define YN_RXPRFCTL_OFFLINE		0x00000004
#define YN_RXPRFCTL_ONLINE		0x00000008
#define YN_RXPRFCTL_WRFIFO_STP		0x00000010
#define YN_RXPRFCTL_WRFIFO_TST_OFFLINE	0x00000020
#define YN_RXPRFCTL_WRFIFO_TST_ONLINE	0x00000040
#define YN_RXPRFCTL_RDFIFO_STP		0x00000100
#define YN_RXPRFCTL_RDFIFO_TST_OFFLINE	0x00000200
#define YN_RXPRFCTL_RDFIFO_TST_ONLINE	0x00000400
#define YN_RXPRFCTL_MSTREQ_STEP		0x00001000
#define YN_RXPRFCTL_MSTREQ_TST_OFFLINE	0x00002000
#define YN_RXPRFCTL_MSTREQ_TST_ONLINE	0x00004000

#define YN_RXQ_LINK_OFFSET	0x80

#define YN_RXQ(p, r)	(((p)->ynLinkNum * YN_RXQ_LINK_OFFSET) + (r))

/* Block 12 -- TX sync queue 1 */
/* Block 13 -- TX async queue 1 */
/* Block 14 -- TX sync queue 2 */
/* Block 15 -- TX async queue 2 */

#define YN_TXQ_BUFCNT		0x0600
#define YN_TXQ_BUFCTL		0x0602
#define YN_TXQ_TXBUF_LO		0x0608
#define YN_TXQ_TXBUF_HI		0x060C
#define YN_TXQ_TCP_LGSEND_LEN	0x0610
#define YN_TXQ_TCPSUM_INIT	0x0614
#define YN_TXQ_TCPSUM_LOCK	0x0616
#define YN_TXQ_TCPSUM_WRITE	0x0618
#define YN_TXQ_TCPSUM_START	0x061A
#define YN_TXQ_VLAN		0x0620
#define YN_TXQ_RXLEN		0x0622
#define YN_TXQ_DONEIDX		0x0624
#define YN_TXQ_REQADDR_LO	0x0628
#define YN_TXQ_REQADDR_HI	0x062C
#define YN_TXQ_CURBYTES		0x0630
#define YN_TXQ_BMU_CSR		0x0634
#define YN_TXQ_BMU_TST		0x0638
#define YN_TXQ_BMU_STATE	0x063C
#define YN_TXQ_FIFO_WMRK	0x0640
#define YN_TXQ_FIFO_ALIGN	0x0642
#define YN_TXQ_FIFO_RDPTR_SHD	0x0644
#define YN_TXQ_FIFO_RDLVL_SHD	0x0646
#define YN_TXQ_FIFO_RDPTR	0x0648
#define YN_TXQ_FIFO_RDLVL	0x0646
#define YN_TXQ_FIFO_WRPTR	0x064C
#define YN_TXQ_FIFO_WRPTR_SHD	0x064D
#define YN_TXQ_FIFO_WRLVL_SHD	0x064F

#define YN_TXQBMUCSR_RESET		0x00000001
#define YN_TXQBMUCSR_UNRESET		0x00000002
#define YN_TXQBMUCSR_OFFLINE		0x00000004
#define YN_TXQBMUCSR_ONLINE		0x00000008
#define YN_TXQBMUCSR_FIFO_RESET		0x00000010
#define YN_TXQBMUCSR_FIFO_UNRESET	0x00000020
#define YN_TXQBMUCSR_FIFO_OFFLINE	0x00000040
#define YN_TXQBMUCSR_FIFO_ONLINE	0x00000080
#define YN_TXQBMUCSR_START		0x00000100
#define YN_TXQBMUCSR_STOP		0x00000200
#define YN_TXQBMUCSR_CLR_IRQ_CHECK	0x00000400
#define YN_TXQBMUCSR_CLR_IRQ_TCP	0x00000800
#define YN_TXQBMUCSR_IP_IDINC_ONLINE	0x00001000
#define YN_TXQBMUCSR_IP_IDINC_OFFLINE	0x00002000
#define YN_TXQBMUCSR_BMU_IDLE		0x80000000

#define YN_TXQBMU_UNRESET					\
	(YN_TXQBMUCSR_UNRESET|YN_TXQBMUCSR_FIFO_RESET|YN_TXQBMUCSR_OFFLINE)
#define YN_TXQBMU_INIT						\
	(YN_TXQBMUCSR_CLR_IRQ_CHECK|YN_TXQBMUCSR_CLR_IRQ_TCP|	\
	 YN_TXQBMUCSR_START|YN_TXQBMUCSR_FIFO_UNRESET|		\
	 YN_TXQBMUCSR_ONLINE)
#define YN_TXQBMU_ONLINE	YN_TXQBMUCSR_FIFO_ONLINE

/* TX Prefetch unit control */

#define YN_TXQP_PREFETCH_CTL	0x0650
#define YN_TXQP_LASTIDX		0x0654
#define YN_TXQP_LISTADDR_LO	0x0658
#define YN_TXQP_LISTADDR_HI	0x065C
#define YN_TXQP_GETIDX		0x0660
#define YN_TXQP_PUTIDX		0x0664
#define YN_TXQP_FIFO_WRPTR	0x0670
#define YN_TXQP_FIFO_WRPTR_SHD	0x0672
#define YN_TXQP_FIFO_RDPTR	0x0674
#define YN_TXQP_FIFO_WMRK	0x0678
#define YN_TXQP_MASTER_RQBYTES	0x067A
#define YN_TXQP_FIFO_LVL	0x067C
#define YN_TXQP_FIFO_LVL_SHD	0x067E

#define YN_TXPRFCTL_RESET		0x00000001
#define YN_TXPRFCTL_UNRESET		0x00000002
#define YN_TXPRFCTL_OFFLINE		0x00000004
#define YN_TXPRFCTL_ONLINE		0x00000008
#define YN_TXPRFCTL_WRFIFO_STP		0x00000010
#define YN_TXPRFCTL_WRFIFO_TST_OFFLINE	0x00000020
#define YN_TXPRFCTL_WRFIFO_TST_ONLINE	0x00000040
#define YN_TXPRFCTL_RDFIFO_STP		0x00000100
#define YN_TXPRFCTL_RDFIFO_TST_OFFLINE	0x00000200
#define YN_TXPRFCTL_RDFIFO_TST_ONLINE	0x00000400
#define YN_TXPRFCTL_MSTREQ_STEP		0x00001000
#define YN_TXPRFCTL_MSTREQ_TST_OFFLINE	0x00002000
#define YN_TXPRFCTL_MSTREQ_TST_ONLINE	0x00004000

#define YN_TXQ_LINK_OFFSET	0x100
#define YN_TXASYNC_OFFSET	0x80

#define YN_TXQS(p, r)	(((p)->ynLinkNum * YN_TXQ_LINK_OFFSET) + (r))
#define YN_TXQA(p, r)	\
	(((p)->ynLinkNum * YN_TXQ_LINK_OFFSET) + (r) + YN_TXASYNC_OFFSET)

/* Block 16 -- Receive RAMbuffer 1 */
/* Block 17 -- Receive RAMbuffer 2 */

#define YN_RXRB_START		0x0800
#define YN_RXRB_END		0x0804
#define YN_RXRB_WR_PTR		0x0808
#define YN_RXRB_RD_PTR		0x080C
#define YN_RXRB_UTHR_PAUSE	0x0810
#define YN_RXRB_LTHR_PAUSE	0x0814
#define YN_RXRB_UTHR_HIPRIO	0x0818
#define YN_RXRB_UTHR_LOPRIO	0x081C
#define YN_RXRB_PKTCNT		0x0820
#define YN_RXRB_LVL		0x0824
#define YN_RXRB_CTLTST		0x0828

#define YN_RXRB_LINK_OFFSET	0x80

#define YN_RXRB(p, r)	(((p)->ynLinkNum * YN_RXRB_LINK_OFFSET) + (r))

/* Block 20 -- Sync. Transmit RAMbuffer 1 */
/* Block 21 -- Async. Transmit RAMbuffer 1 */
/* Block 22 -- Sync. Transmit RAMbuffer 2 */
/* Block 23 -- Async. Transmit RAMbuffer 2 */

#define YN_TXRB_START		0x0A00
#define YN_TXRB_END		0x0A04
#define YN_TXRB_WR_PTR		0x0A08
#define YN_TXRB_RD_PTR		0x0A0C
#define YN_TXRB_PKTCNT		0x0A20
#define YN_TXRB_LVL		0x0A24
#define YN_TXRB_CTLTST		0x0A28

#define YN_TXRB_LINK_OFFSET	0x100

#define YN_TXRBS(p, r)	(((p)->ynLinkNum * YN_TXRM_LINK_OFFSET) + (r))
#define YN_TXRBA(p, r)	\
	(((p)->ynLinkNum * YN_TXRB_LINK_OFFSET) + (r) + YN_TXASYNC_OFFSET)

#define YN_RBCTL_RESET		0x00000001
#define YN_RBCTL_UNRESET	0x00000002
#define YN_RBCTL_OFFLINE	0x00000004
#define YN_RBCTL_ONLINE		0x00000008
#define YN_RBCTL_STORENFWD_OFF	0x00000010
#define YN_RBCTL_STORENFWD_ON	0x00000020

/* Block 24 RX MAC FIFO 1 */
/* Block 25 RX MAC FIFO 2 */
/* Block 26 TX MAC FIFO 1 */
/* Block 27 TX MAC FIFO 2 */

#define YN_RXMF_END		0x0C40
#define YN_RXMF_THRESHOLD	0x0C44
#define YN_RXMF_CTRL_TEST	0x0C48
#define YN_RXMF_FIFO_FLUSHMSK	0x0C4C
#define YN_RXMF_FIFO_FLUSHTHR	0x0C50
#define YN_RXMF_FIFO_TRUNCTHR	0x0C54
#define YN_RXMF_VLAN_TAGTYPE	0x0C5C
#define YN_RXMF_WRITE_PTR	0x0C60
#define YN_RXMF_WRITE_LEVEL	0x0C68
#define YN_RXMF_READ_PTR	0x0C70
#define YN_RXMF_READ_LEVEL	0x0C78


#define YN_RFCTL_TRUNC_ONLINE	0x02000000	/* truncation on */
#define YN_RFCTL_TRUNC_OFFLINE	0x01000000	/* truncation off */
#define YN_RFCTL_VLAN_ONLINE	0x02000000	/* VLAN stripping on */
#define YN_RFCTL_VLAN_OFFLINE	0x01000000	/* VLAN stripping off */
#define YN_RFCTL_WR_PTR_TST_ON	0x00004000      /* Write pointer test on*/
#define YN_RFCTL_WR_PTR_TST_OFF	0x00002000      /* Write pointer test off */
#define YN_RFCTL_WR_PTR_STEP	0x00001000      /* Write pointer increment */
#define YN_RFCTL_RD_PTR_TST_ON	0x00000400      /* Read pointer test on */
#define YN_RFCTL_RD_PTR_TST_OFF	0x00000200      /* Read pointer test off */
#define YN_RFCTL_RD_PTR_STEP	0x00000100      /* Read pointer increment */
#define YN_RFCTL_FIFOFL_ONLINE	0x00000080	/* Fifo flush on */
#define YN_RFCTL_FIFOFL_OFFLINE	0x00000040	/* Fifo flush off */
#define YN_RFCTL_CLR_RX_OFLOW	0x00000020      /* Clear IRQ RX FIFO Overrun */
#define YN_RFCTL_CLR_RX_DONE	0x00000010      /* Clear IRQ Frame RX Done */
#define YN_RFCTL_ONLINE		0x00000008      /* Operational mode on */
#define YN_RFCTL_OFFLINE	0x00000004      /* Operational mode off */
#define YN_RFCTL_UNRESET	0x00000002      /* MAC FIFO Reset Clear */
#define YN_RFCTL_RESET		0x00000001      /* MAC FIFO Reset Set */

#define YN_TXMF_END		0x0D40
#define YN_TXMF_THRESHOLD	0x0D44
#define YN_TXMF_CTRL_TEST	0x0D48
#define YN_TXMF_FIFO_FLUSHMSK	0x0D4C
#define YN_TXMF_FIFO_FLUSHTHR	0x0D50
#define YN_TXMF_FIFO_TRUNCTHR	0x0D54
#define YN_TXMF_VLAN_TAGTYPE	0x0D5C
#define YN_TXMF_WRITE_PTR	0x0D60
#define YN_TXMF_WRITE_LEVEL	0x0D68
#define YN_TXMF_READ_PTR	0x0D70
#define YN_TXMF_READ_LEVEL	0x0D78

#define YN_TFCTL_VLAN_ONLINE	0x02000000
#define YN_TFCTL_VLAN_OFFLINE	0x01000000
#define YN_TFCTL_WRPTR_TST_ON	0x00040000
#define YN_TFCTL_WRPTR_TST_OFF	0x00020000
#define YN_TFCTL_WRPTR_INC	0x00010000
#define YN_TFCTL_WR_PTR_TST_ON	0x00004000      /* Write pointer test on*/
#define YN_TFCTL_WR_PTR_TST_OFF	0x00002000      /* Write pointer test off */
#define YN_TFCTL_WR_PTR_STEP	0x00001000      /* Write pointer increment */
#define YN_TFCTL_RD_PTR_TST_ON	0x00000400      /* Read pointer test on */
#define YN_TFCTL_RD_PTR_TST_OFF	0x00000200      /* Read pointer test off */
#define YN_TFCTL_RD_PTR_STEP	0x00000100      /* Read pointer increment */
#define YN_TFCTL_CLR_TX_UFLOW	0x00000080	/* Clear IRQ TX underrun */
#define YN_TFCTL_CLR_TXDONE	0x00000020      /* Clear IRQ TX done */
#define YN_TFCTL_CLR_PERR	0x00000010      /* Clear IRQ TX parity error */
#define YN_TFCTL_ONLINE		0x00000008      /* Operational mode on */
#define YN_TFCTL_OFFLINE	0x00000004      /* Operational mode off */
#define YN_TFCTL_UNRESET	0x00000002      /* MAC FIFO Reset Clear */
#define YN_TFCTL_RESET		0x00000001      /* MAC FIFO Reset Set */

#define YN_MF_LINK_OFFSET	0x80
#define YN_MF(p, r)	(((p)->ynLinkNum * YN_MF_LINK_OFFSET) + (r))

/* Block 28 timestamp timer */

#define YN_TSTAMP_TIMER		0x0E14
#define YN_TSTAMP_CTLTST	0x0E18

#define YN_TSCTLTST_CLR_IRQ	0x00000001
#define YN_TSCTLTST_OFFLINE	0x00000002
#define YN_TSCTLTST_ONLINE	0x00000004
#define YN_TSCTLTST_STEP	0x00000010
#define YN_TSCTLTST_TST_OFFLNE	0x00000020
#define YN_TSCTLTST_TST_ONLINE	0x00000040

/* Block 29 status BMU */

#define YN_STBMU_CTL		0x0E80
#define YN_STBMU_LASTIDX	0x0E84
#define YN_STBMU_LISTADDR_LO	0x0E88
#define YN_STBMU_LISTADDR_HI	0x0E8C
#define YN_STBMU_TXA0_REPIDX	0x0E90
#define YN_STBMU_TXS0_REPIDX	0x0E92
#define YN_STBMU_TXA1_REPIDX	0x0E94
#define YN_STBMU_TXS1_REPIDX	0x0E96
#define YN_STBMU_TXIDX_THRESH	0x0E98
#define YN_STBMU_PUTIDX		0x0E9C
#define YN_STBMU_FIFO_WRPTR	0x0EA0
#define YN_STBMU_FIFO_RDPTR	0x0EA4
#define YN_STBMU_FIFO_RFPTR_SHD	0x0EA6
#define YN_STBMU_FIFO_LVL	0x0EA8
#define YN_STBMU_FIFO_LVL_SHD	0x0EAA
#define YN_STBMU_FIFO_WMRK	0x0EAC
#define YN_STBMU_FIFO_ISR_WMRK	0x0EAD

#define YN_STBMUCTL_RESET	0x00000001
#define YN_STBMUCTL_UNRESET	0x00000002
#define YN_STBMUCTL_OFFLINE	0x00000004
#define YN_STBMUCTL_ONLINE	0x00000008
#define YN_STBMUCTL_CLR_IRQ	0x00000010
#define YN_STBMUCTL_WFIFO_INC	0x00000100
#define YN_STBMUCTL_WFIFO_TON	0x00000200
#define YN_STBMUCTL_WFIFO_TOFF	0x00000400
#define YN_STBMUCTL_RFIFO_INC	0x00001000
#define YN_STBMUCTL_RFIFO_TON	0x00002000
#define YN_STBMUCTL_RFIFO_TOFF	0x00004000
#define YN_STBMUCTL_MREQ_INC	0x00010000
#define YN_STBMUCTL_MREQ_TON	0x00020000
#define YN_STBMUCTL_MREQ_TOFF	0x00040000

/* Level timer */

#define YN_LVTIMER_INIT		0x0EB0
#define YN_LVTIMER_COUNTER	0x0EB4
#define YN_LVTIMER_CTL		0x0EB8
#define YN_LVTIMER_TST		0x0EB9
#define YN_TXTIMER_INIT		0x0EC0
#define YN_TXTIMER_COUNTER	0x0EC4
#define YN_TXTIMER_CTL		0x0EC8
#define YN_TXTIMER_TST		0x0EC9
#define YN_ISRTIMER_INIT	0x0ED0
#define YN_ISRTIMER_COUNTER	0x0ED4
#define YN_ISRTIMER_CTL		0x0ED8
#define YN_ISRTIMER_TST		0x0ED9

#define YN_STBMU_LINK_OFFSET	0x80

#define YN_STMBU(p, r)	(((p)->ynLinkNum * YN_STBMU_LINK_OFFSET) + (r))

/* Block 30 GMAC and GPHY control registers */

#define YN_GMACCTL		0x0F00
#define YN_GPHYCTL		0x0F04
#define YN_MISCCTL		0x0F06
#define YN_GMACISR		0x0F08
#define YN_GMACIMR		0x0F0C
#define YN_LINKCTL		0x0F10

/* WOL registers */

#define YN_WOL_CTLSTS		0x0F20
#define YN_WOL_MATCHCTL		0x0F22
#define YN_WOL_MATCHRES		0x0F23
#define YN_WOL_MACADDR_LO	0x0F24
#define YN_WOL_MACADDR_HI	0x0F28
#define YN_WOL_PMEMATCH_ENB	0x0F2A
#define YN_WOL_ASFMATCH_ENB	0x0F2B
#define YN_WOL_PATTERNRD_PTR	0x0F2C
#define YN_WOL_PATTERN_LEN0	0x0F30
#define YN_WOL_PATTERN_LEN1	0x0F31
#define YN_WOL_PATTERN_LEN2	0x0F32
#define YN_WOL_PATTERN_LEN3	0x0F33
#define YN_WOL_PATTERN_LEN4	0x0F34
#define YN_WOL_PATTERN_LEN5	0x0F35
#define YN_WOL_PATTERN_LEN6	0x0F36
#define YN_WOL_PATTERN_CNT0	0x0F38
#define YN_WOL_PATTERN_CNT1	0x0F39
#define YN_WOL_PATTERN_CNT2	0x0F3A
#define YN_WOL_PATTERN_CNT3	0x0F3B
#define YN_WOL_PATTERN_CNT4	0x0F3C
#define YN_WOL_PATTERN_CNT5	0x0F3D
#define YN_WOL_PATTERN_CNT6	0x0F3E

#define YN_GMAC_LINK_OFFSET	0x80

#define YN_GMAC(p, r)	(((p)->ynLinkNum * YN_GMAC_LINK_OFFSET) + (r))

/* GMAC control register */

#define YN_GMACCTL_RESET	0x0001
#define YN_GMACCTL_UNRESET	0x0002
#define YN_GMACCTL_PAUSE_OFF	0x0004
#define YN_GMACCTL_PAUSE_ON	0x0008
#define YN_GMACCTL_LOOP_OFF	0x0010
#define YN_GMACCTL_LOOP_ON	0x0020
#define YN_GMACCTL_BURST_OFF	0x0040
#define YN_GMACCTL_BURST_ON	0x0080

/* GPHY control register */

#define YN_GPHYCTL_RESET	0x01
#define YN_GPHYCTL_UNRESET	0x02

/* Misc control register */

#define YN_MISCCTL_PHYBURNIN	0x00C0
#define YN_MISCCTL_SPI_LOCK	0x0100

/* GMAC ISR (and IMR) register */

#define YN_GMACISR_RXOK		0x01
#define YN_GMACISR_RXOFLOW	0x02
#define YN_GMACISR_TXOK		0x04
#define YN_GMACISR_TXUFLOW	0x08
#define YN_GMACISR_TXCNT_OFLOW	0x10
#define YN_GMACISR_RXCNT_OFLOW	0x20

/* Link control register */

#define YN_LINKCTL_RESET	0x0001
#define YN_LINKCTL_UNRESET	0x0002

/* PCI config space mirror */

#define YN_PCI_CFG		0x1c00

/*
 * DMA Data structures.
 * There are three sets of DMA descriptor structures: TX list,
 * RX list and status list. The RX and TX lists contain buffers supplied
 * by the host, which will be consumed by the NIC. The status list
 * is filled in by the controller with information that tells us which
 * entries in the RX and TX lists have been consumed. For the RX case,
 * it also contains RX status for received packets.
 *
 * Each link will have at least one TX DMA list, and just one RX DMA
 * list. There is only one status DMA list, even with NICs that have two
 * links: all events are multiplexed into the DMA status list and
 * must be demultiplexed and processed by the host.
 *
 * The original SysKonnect GEnesis and Yukon I adapters used a 32-byte
 * structure for both RX and TX lists. The Yukon II uses a different
 * mechanism whereby the descriptors are effectively broken up into
 * 8 byte chunks, each prefixed with a special opcode identifier.
 * The idea is that in some cases, some of the fields present in the
 * original 32-byte descriptor layout may have frequently been unused
 * or redundant. The new scheme allows the host to consume just enough
 * descriptor memory for a given transmission and no more. This may
 * mean that a packet might need as little as 8 bytes of descriptor
 * memory.
 */

/* RX opcodes */

#define YN_RXOP_TCPPAR	0x12	/* TCP RX start sum, end sum */
#define YN_RXOP_ADDR64	0x21	/* 64-bit addressing update */
#define YN_RXOP_BUFFER	0x40	/* middle or last buffer in packet */
#define YN_RXOP_PACKET	0x41	/* first buffer in packet */

/* TX opcodes */

#define YN_TXOP_TCP_S	0x12	/* Load csum start */
#define YN_TXOP_TCP_IS	0x16	/* Load csum init and start */
#define YN_TXOP_TCP_L	0x18	/* Lock this number of packets */
#define YN_TXOP_TCP_LW	0x19	/* Lock packets, and load csum write offset */
#define YN_TXOP_TCP_LSW	0x1B	/* Lock packets, load csum start and write */
#define YN_TXOP_TCP_LISW 0x1F	/* Lock and load everything */
#define YN_TXOP_ADDR64	0x21	/* 64-bit addressing update */
#define YN_TXOP_VLAN	0x22	/* insert this vlan tag */
#define YN_TXOP_A64VLAN	0x23	/* 64-bit addr update and vlan combined */
#define YN_TXOP_LRGLEN	0x24	/* TCP large MTU length */
#define YN_TXOP_LRGLENV	0x26	/* TCP large MTU and vlan combined */
#define YN_TXOP_BUFFER	0x40	/* middle or last buffer in packet */
#define YN_TXOP_PACKET	0x41	/* first buffer in packet */
#define YN_TXOP_TCPLRG	0x43	/* TCP large send */

/* Status opcodes */

#define YN_STOP_RXSTS	0x60	/* RX status */
#define YN_STOP_RXTSTMP	0x61	/* RX timestamp */
#define YN_STOP_RXVLAN	0x62	/* RX VLAN tag */
#define YN_STOP_RXTV	0x63	/* RX timestamp plus vlan */
#define YN_STOP_RXSUM	0x64	/* RX TCP checksum */
#define YN_STOP_RXRSS	0x65	/* RX RSS hash */
#define YN_STOP_RXSUMV	0x66	/* RX TCP checksum plus vlan */
#define YN_STOP_TXIDX	0x68	/* TX done index */

/* Ownership bit, part of opcode byte  */

#define YN_DESC_OWN	0x80

/* TX descriptor control codes */

#define YN_TXCTL_UDPTCP	0x01	/* 1 == UDP packet, 0 == TCP packet */
#define YN_TXCTL_CSUM	0x02	/* calculate checksum for this packet */
#define YN_TXCTL_WSUM	0x04	/* write checksum to packet */
#define YN_TXCTL_ISUM	0x08	/* use init value from init register */
#define YN_TXCTL_LSUM	0x10	/* use lock value from lock register */
#define YN_TXCTL_VLAN	0x20	/* insert vlan tag */
#define YN_TXCTL_BURST	0x40	/* force burst to status FIFO */
#define YN_TXCTL_EOP	0x80	/* last buffer in packet */

/* RX descriptor control codes */

#define YN_RXCTL_CSUM	0x02	/* calculate checksum for this packet */
#define YN_RXCTL_BURST	0x40	/* last element forces burst of status FIFO */

typedef struct yn_desc
    {
    union
        {
	struct
            {
            volatile UINT16 yn_arg0;
            volatile UINT16 yn_arg1;
            } y_arg;
        volatile UINT32 yn_buf;
        } y;
    volatile UINT16	yn_arg2;
    volatile UINT8	yn_ctl;
    volatile UINT8	yn_opcode;
    } YN_DESC;

#define yn_arg0		y.y_arg.yn_arg0
#define yn_arg1		y.y_arg.yn_arg1
#define yn_buf		y.yn_buf

typedef struct yn_rx_desc
    {
    YN_DESC		yn_rx[2];
    } YN_RX_DESC;

/*
 * For each RX packet buffer we supply to the chip, it may return
 * up to 3 status descriptors. So we have to supply a large enough
 * status element list to hold all possible descriptors.
 */

#define YN_TX_LIST_CNT	(128 * 3)
#define YN_RX_LIST_CNT	128
#define YN_ST_LIST_CNT	(((YN_TX_LIST_CNT / 3) * 2) + (YN_RX_LIST_CNT * 4))

#define YN_INC_DESC(x, y)	(x) = (((x) + 1) % y)

#define YN_ADJ(x)	m_adj((x), 2)

#define YN_MTU		1500
#define YN_JUMBO_MTU	9000
#define YN_CLSIZE	1536
#define YN_MAXFRAG	16
#define YN_MAX_STS	32
#define YN_NAME		"yn"
#define YN_TIMEOUT	10000
#define YN_INTRS	\
	(YN_ISR_RX1_CHECK|YN_ISR_TX1_AS_CHECK|YN_ISR_RX2_CHECK|	\
	YN_ISR_TX2_AS_CHECK|YN_ISR_STBURST)

#define YN_ADDR_LO(y)		((UINT64)((UINT32) (y)) & 0xFFFFFFFF)
#ifdef _WRS_CONFIG_LP64
#define YN_ADDR_HI(y)		(((UINT64)((y)) >> 32) & 0xFFFFFFFF)
#else
#define YN_ADDR_HI(y)       (0)
#endif

/*
 * Private adapter context structure.
 */

typedef struct yn_drv_ctrl
    {
    END_OBJ		ynEndObj;
    VXB_DEVICE_ID	ynDev;
    void *		ynBar;
    void *		ynMacBar;
    void *		ynHandle;
    VXB_DEVICE_ID	ynMiiParent;
    void *		ynMuxDevCookie;

    JOB_QUEUE_ID	ynJobQueue;
    QJOB		ynIntJob;
    atomicVal_t		ynIntPending;

    volatile BOOL	ynTxStall;

    BOOL		ynPolling;
    M_BLK_ID		ynPollBuf;
    UINT32		ynIntMask;

    UINT8		ynAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	ynCaps;

    END_IFDRVCONF	ynEndStatsConf;
    END_IFCOUNTERS	ynEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	ynMediaList;
    END_ERR		ynLastError;
    UINT32		ynCurMedia;
    UINT32		ynCurStatus;
    VXB_DEVICE_ID	ynMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	ynParentTag;

    VXB_DMA_TAG_ID	ynRxDescTag;
    VXB_DMA_MAP_ID	ynRxDescMap;
    YN_RX_DESC *	ynRxDescMem;

    VXB_DMA_TAG_ID	ynTxDescTag;
    VXB_DMA_MAP_ID	ynTxDescMap;
    YN_DESC *		ynTxDescMem;

    VXB_DMA_TAG_ID	ynStDescTag;
    VXB_DMA_MAP_ID	ynStDescMap;
    YN_DESC *		ynStDescMem;

    UINT32		ynStIdx;

    UINT32              ynTxProd;
    UINT32              ynTxCons;
    UINT32              ynTxFree;
    UINT32		ynRxIdx;
    UINT32		ynRxProdIdx;

    VXB_DMA_TAG_ID	ynMblkTag;

    VXB_DMA_MAP_ID	ynRxMblkMap[YN_RX_LIST_CNT];
    VXB_DMA_MAP_ID	ynTxMblkMap[YN_TX_LIST_CNT];

    M_BLK_ID		ynRxMblk[YN_RX_LIST_CNT];
    M_BLK_ID		ynTxMblk[YN_TX_LIST_CNT];

    SEM_ID		ynDevSem;
    BOOL		ynIsDualLink;
    BOOL		ynLinkNum;
    struct yn_drv_ctrl *ynOther;

    UINT32		ynDiscards;

    UINT8		ynChipId;
    UINT8		ynChipVer;
    UINT8		ynConnType;
    UINT8		ynPmdType;
    UINT32		ynRamSize;
    UINT32		ynRxRamStart;
    UINT32		ynRxRamEnd;
    UINT32		ynTxRamStart;
    UINT32		ynTxRamEnd;

    int			ynMaxMtu;
    } YN_DRV_CTRL;

#define YN_BAR(p)   ((YN_DRV_CTRL *)(p)->pDrvCtrl)->ynBar
#define YN_HANDLE(p)   ((YN_DRV_CTRL *)(p)->pDrvCtrl)->ynHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (YN_HANDLE(pDev), (UINT32 *)((char *)YN_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (YN_HANDLE(pDev),                             \
        (UINT32 *)((char *)YN_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (YN_HANDLE(pDev), (UINT16 *)((char *)YN_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (YN_HANDLE(pDev),                             \
        (UINT16 *)((char *)YN_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (YN_HANDLE(pDev), (UINT8 *)((char *)YN_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (YN_HANDLE(pDev),                              \
        (UINT8 *)((char *)YN_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define GM_BAR(p)   ((YN_DRV_CTRL *)(p)->pDrvCtrl)->ynMacBar

#define GMAC_READ_4(pDev, addr)                                  \
    vxbRead32 (YN_HANDLE(pDev), (UINT32 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (YN_HANDLE(pDev),                             \
        (UINT32 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_READ_2(pDev, addr)                                  \
    vxbRead16 (YN_HANDLE(pDev), (UINT16 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (YN_HANDLE(pDev),                             \
        (UINT16 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_READ_1(pDev, addr)                                  \
    vxbRead8 (YN_HANDLE(pDev), (UINT8 *)((char *)GM_BAR(pDev) + addr))

#define GMAC_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (YN_HANDLE(pDev),                              \
        (UINT8 *)((char *)GM_BAR(pDev) + addr), data)

#define GMAC_SETBIT_1(pDev, offset, val)          \
        GMAC_WRITE_1(pDev, offset, GMAC_READ_1(pDev, offset) | (val))

#define GMAC_CLRBIT_1(pDev, offset, val)          \
        GMAC_WRITE_1(pDev, offset, GMAC_READ_1(pDev, offset) & ~(val))

#define GMAC_SETBIT_2(pDev, offset, val)          \
        GMAC_WRITE_2(pDev, offset, GMAC_READ_2(pDev, offset) | (val))

#define GMAC_CLRBIT_2(pDev, offset, val)          \
        GMAC_WRITE_2(pDev, offset, GMAC_READ_2(pDev, offset) & ~(val))

#define GMAC_SETBIT_4(pDev, offset, val)          \
        GMAC_WRITE_4(pDev, offset, GMAC_READ_4(pDev, offset) | (val))

#define GMAC_CLRBIT_4(pDev, offset, val)          \
        GMAC_WRITE_4(pDev, offset, GMAC_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCmvYukonIIVxbEndh */
