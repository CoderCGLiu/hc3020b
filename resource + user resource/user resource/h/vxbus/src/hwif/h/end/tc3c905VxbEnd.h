/* tc3c905VxbEnd.h - header file for 3Com 3c905/B/C VxBus ENd driver */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be liceelPcid only pursuant to the terms
 * of an applicable Wind River liceelPci agreement.
 */

/*
modification history
--------------------
01d,18jun07,wap  Make this driver SMP safe
01c,29jan07,wap  Add more PCI IDs
01b,28aug06,wap  Add reset mask bits
01a,25aug06,wap  written
*/

#ifndef __INCtc3c905VxbEndh
#define __INCtc3c905VxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void elPciRegister (void);

#ifndef BSP_VERSION

#define ELPCI_VENDORID_3COM	0x10b7

#define ELPCI_DEVICEID_BOOMERANG_10BT              0x9000
#define ELPCI_DEVICEID_BOOMERANG_10BT_COMBO        0x9001
#define ELPCI_DEVICEID_BOOMERANG_10_100BT          0x9050
#define ELPCI_DEVICEID_BOOMERANG_100BT4            0x9051
#define ELPCI_DEVICEID_KRAKATOA_10BT               0x9004
#define ELPCI_DEVICEID_KRAKATOA_10BT_COMBO         0x9005
#define ELPCI_DEVICEID_KRAKATOA_10BT_TPC           0x9006
#define ELPCI_DEVICEID_CYCLONE_10FL                0x900A
#define ELPCI_DEVICEID_HURRICANE_10_100BT          0x9055
#define ELPCI_DEVICEID_CYCLONE_10_100BT4           0x9056
#define ELPCI_DEVICEID_CYCLONE_10_100_COMBO        0x9058
#define ELPCI_DEVICEID_CYCLONE_10_100FX            0x905A
#define ELPCI_DEVICEID_TORNADO_10_100BT            0x9200
#define ELPCI_DEVICEID_TORNADO_10_100BT_920B       0x9201
#define ELPCI_DEVICEID_TORNADO_10_100BT_920B_WNM   0x9202
#define ELPCI_DEVICEID_TORNADO_10_100BT_920B_INT   0x9210 /* integrated */
#define ELPCI_DEVICEID_HURRICANE_10_100BT_SERV     0x9800
#define ELPCI_DEVICEID_PYTHON_10_100BT_SERV        0x9805
#define ELPCI_DEVICEID_HURRICANE_SOHO100TX         0x7646
#define ELPCI_DEVICEID_TORNADO_HOMECONNECT         0x4500

/* The following are all cardbus adapters */

#define ELPCI_DEVICEID_HURRICANE_555               0x5055
#define ELPCI_DEVICEID_HURRICANE_556               0x6055
#define ELPCI_DEVICEID_HURRICANE_556B              0x6056
#define ELPCI_DEVICEID_HURRICANE_575A              0x5057
#define ELPCI_DEVICEID_HURRICANE_575B              0x5157
#define ELPCI_DEVICEID_HURRICANE_575C              0x5257
#define ELPCI_DEVICEID_HURRICANE_656               0x6560
#define ELPCI_DEVICEID_HURRICANE_656B              0x6562
#define ELPCI_DEVICEID_TORNADO_656C                0x6564

/*
 * 3c905 registers. The register space is 64 bytes in size for
 * original 3c905 boomerang adapters and 128 bytes for 3c905B
 * and later adapters. The register are is divided into two
 * main blocks. The first block of 16 bytes is a register
 * window controlled by the 'window select' command.
 * There are a total of 8 register windows which can be overlaid
 * into this space. The remaining space contains the list
 * pointer registers, the DMA control and status registers,
 * the interrupt status register and the timer registers. The
 * command and status registers are always mapped at the same
 * location in all 8 register windows.
 */

#define ELPCI_COMMAND		0x0E
#define ELPCI_STATUS		0x0E

#define ELPCI_TXPKT_ID		0x18 /* 3c905B only */
#define ELPCI_TIMER		0x1A
#define ELPCI_TX_STATUS		0x1B
#define ELPCI_ISR		0x1E /* 3c905, mirror of 0x0E */
#define ELPCI_ISRAUTO		0x1E /* 3c905B only */
#define ELPCI_DMACTL		0x20
#define ELPCI_DOWNLIST_PTR	0x24
#define ELPCI_DOWN_BURST_THRESH	0x28 /* 3c905B only */
#define ELPCI_DOWN_PRIO_THRESH	0x2C /* 3c905B only */
#define ELPCI_DOWN_POLL		0x2D /* 3c90xB only */
#define ELPCI_TX_FREETHRESH	0x2F /* 3c905 only */
#define ELPCI_UPLIST_STATUS	0x30
#define ELPCI_FREETIMER		0x34
#define ELPCI_COUNTDOWN		0x36
#define ELPCI_UPLIST_PTR	0x38
#define ELPCI_UP_PRIO_THRESH	0x3C /* 3c905B only */
#define ELPCI_UP_POLL		0x3D /* 3c905B only */
#define ELPCI_UP_BURST_THRESH	0x3E /* 3c905B only */
#define ELPCI_UP_POLL		0x3D /* 3c90xB only */

#define ELPCI_REALTIME_COUNTER	0x40 /* 3c905B only */
#define ELPCI_DEBUGDATA		0x70 /* 3c905B only */
#define ELPCI_DOWN_MAXBURST	0x78 /* 3c905B only */
#define ELPCI_UP_MAXBURST	0x7A /* 3c905B only */
#define ELPCI_PWRMGMT_CTL	0x7C /* 3c905B only */

#define ELPCI_PKTSTAT_UP_STALLED	0x00002000
#define ELPCI_PKTSTAT_UP_ERROR		0x00004000
#define ELPCI_PKTSTAT_UP_CMPLT		0x00008000
 
#define ELPCI_DMACTL_DN_CMPLT_REQ	0x00000002
#define ELPCI_DMACTL_DOWN_STALLED	0x00000004
#define ELPCI_DMACTL_UP_CMPLT		0x00000008
#define ELPCI_DMACTL_DOWN_CMPLT		0x00000010
#define ELPCI_DMACTL_UP_RX_EARLY	0x00000020
#define ELPCI_DMACTL_ARM_COUNTDOWN	0x00000040
#define ELPCI_DMACTL_DOWN_INPROG	0x00000080
#define ELPCI_DMACTL_COUNTER_SPEED	0x00000100
#define ELPCI_DMACTL_DOWNDOWN_MODE	0x00000200
#define ELPCI_DMACTL_TARGET_ABORT	0x40000000
#define ELPCI_DMACTL_MASTER_ABORT	0x80000000

/* Select a register window */

#define ELPCI_SEL_WIN(x)	\
    CSR_WRITE_2(pDev, ELPCI_COMMAND, ELPCI_CMD_WINSEL | x)

/*
 * Command codes. Some command codes require that we wait for
 * the CMD_BUSY flag to clear. Those codes are marked as 'mustwait.'
 */

#define ELPCI_CMD_RESET		0x0000  /* mustwait */
#define ELPCI_CMD_WINSEL	0x0800
#define ELPCI_CMD_COAX_START	0x1000
#define ELPCI_CMD_RX_DISABLE	0x1800
#define ELPCI_CMD_RX_ENABLE	0x2000
#define ELPCI_CMD_RX_RESET	0x2800  /* mustwait */
#define ELPCI_CMD_UP_STALL	0x3000  /* mustwait */
#define ELPCI_CMD_UP_UNSTALL	0x3001
#define ELPCI_CMD_DOWN_STALL	0x3002  /* mustwait */
#define ELPCI_CMD_DOWN_UNSTALL	0x3003
#define ELPCI_CMD_RX_DISCARD	0x4000
#define ELPCI_CMD_TX_ENABLE	0x4800
#define ELPCI_CMD_TX_DISABLE	0x5000
#define ELPCI_CMD_TX_RESET	0x5800  /* mustwait */
#define ELPCI_CMD_INTR_FAKE	0x6000
#define ELPCI_CMD_INTR_ACK	0x6800
#define ELPCI_CMD_INTR_MASK	0x7000
#define ELPCI_CMD_INTR_ENB	0x7800
#define ELPCI_CMD_RX_SET_FILT	0x8000
#define ELPCI_CMD_RX_SET_THRESH	0x8800
#define ELPCI_CMD_TX_SET_THRESH	0x9000
#define ELPCI_CMD_TX_SET_START	0x9800
#define ELPCI_CMD_DMA_UP	0xA000
#define ELPCI_CMD_DMA_STOP	0xA001
#define ELPCI_CMD_STATS_ENABLE	0xA800
#define ELPCI_CMD_STATS_DISABLE	0xB000
#define ELPCI_CMD_COAX_STOP	0xB800
#define ELPCI_CMD_SET_TX_RECLAIM 0xC000 /* 3c905B only */
#define ELPCI_CMD_RX_HASH_CLR	0xC800 /* 3c905B only */
#define ELPCI_CMD_RX_HASH_SET	0xCC00 /* 3c905B only */

#define ELPCI_HASH_SET		0x0400
#define ELPCI_HASHFILT_SIZE	256

/*
 * Reset mask bits. These control what blocks within the NIC
 * will be initialized by a global reset. Setting the bit
 * prevents the block from being reinitialized.
 */

#define ELPCI_RESET_TPAUI	0x01	/* 10baseT and AUI transeiver */
#define ELPCI_RESET_ENDEC	0x02	/* 10Mbps encoder/decoder */
#define ELPCI_RESET_NETWORK	0x04	/* NIC, CSMA/CD, stats register */
#define ELPCI_RESET_FIFO	0x08	/* FIFO control logic */
#define ELPCI_RESET_AISM	0x10	/* auto-initialize state machine */
#define ELPCI_RESET_HOST	0x20	/* host bus interface logic */
#define ELPCI_RESET_VCO		0x40	/* 10Mbps VCO */
#define ELPCI_RESET_UPDOWN	0x80	/* RX/TX DMA engine logic */

/*
 * status codes
 * Note that bits 15 to 13 indicate the currently visible register window
 * which may be anything from 0 to 7.
 */
#define ELPCI_STAT_INTLATCH	0x0001  /* 0 */
#define ELPCI_STAT_ADFAIL	0x0002  /* 1 */
#define ELPCI_STAT_TX_COMPLETE	0x0004  /* 2 */
#define ELPCI_STAT_RX_COMPLETE	0x0010  /* 4 */
#define ELPCI_STAT_RX_EARLY	0x0020  /* 5 */
#define ELPCI_STAT_INTREQ	0x0040  /* 6 */
#define ELPCI_STAT_STATSOFLOW	0x0080  /* 7 */
#define ELPCI_STAT_LINKSTAT	0x0100  /* 8 3c509B only */
#define ELPCI_STAT_DOWN_COMPLETE 0x0200  /* 9 */
#define ELPCI_STAT_UP_COMPLETE	0x0400  /* 10 */
#define ELPCI_STAT_CMDBUSY	0x1000  /* 12 */

/*
 * Window 0 registers
 */

#define ELPCI_W0_EE_DATA	0x0C
#define ELPCI_W0_EE_CMD		0x0A
#define ELPCI_W0_RSRC_CFG	0x08
#define ELPCI_W0_ADDR_CFG	0x06
#define ELPCI_W0_CFG_CTRL	0x04
#define ELPCI_W0_PROD_ID	0x02
#define ELPCI_W0_MFG_ID		0x00

/* EEPROM commands */

#define ELPCI_EE_READ		0x0080  /* read, 5 bit address */
#define ELPCI_EE_WRITE		0x0040  /* write, 5 bit address */
#define ELPCI_EE_ERASE		0x00c0  /* erase, 5 bit address */
#define ELPCI_EE_EWEN		0x0030  /* erase, no data needed */
#define ELPCI_EE_8BIT_READ	0x0200  /* read, 8 bit address */
#define ELPCI_EE_BUSY		0x8000

/* EEPROM contents */

#define ELPCI_EE_EADDR0		0x00    /* station address, first word */
#define ELPCI_EE_EADDR1		0x01    /* station address, next word, */
#define ELPCI_EE_EADDR2		0x02    /* station address, last word */
#define ELPCI_EE_PRODID		0x03    /* product ID code */
#define ELPCI_EE_MDATA_DATE	0x04    /* manufacturing data, date */
#define ELPCI_EE_MDATA_DIV	0x05    /* manufacturing data, division */
#define ELPCI_EE_MDATA_PCODE	0x06    /* manufacturing data, product code */
#define ELPCI_EE_MFG_ID		0x07
#define ELPCI_EE_PCI_PARM	0x08
#define ELPCI_EE_ROM_ONFO	0x09
#define ELPCI_EE_OEM_ADR0	0x0A
#define ELPCI_EE_OEM_ADR1	0x0B
#define ELPCI_EE_OEM_ADR2	0x0C
#define ELPCI_EE_SOFTINFO1	0x0D
#define ELPCI_EE_COMPAT		0x0E
#define ELPCI_EE_SOFTINFO2	0x0F
#define ELPCI_EE_CAPS		0x10    /* capabilities word */
#define ELPCI_EE_RSVD0		0x11
#define ELPCI_EE_ICFG_0		0x12
#define ELPCI_EE_ICFG_1		0x13
#define ELPCI_EE_RSVD1		0x14
#define ELPCI_EE_SOFTINFO3	0x15
#define ELPCI_EE_RSVD_2		0x16

#define EEPROM_5BIT_OFFSET(x)	((((x) << 2) & 0x7F00) | ((x) & 0x003F))
#define EEPROM_8BIT_OFFSET(x)	((x) & 0x003F)

/*
 * Bits in the capabilities word
 */

#define ELPCI_CAPS_PNP		0x0001
#define ELPCI_CAPS_FULL_DUPLEX	0x0002
#define ELPCI_CAPS_LARGE_PKTS	0x0004
#define ELPCI_CAPS_SLAVE_DMA	0x0008
#define ELPCI_CAPS_SECOND_DMA	0x0010
#define ELPCI_CAPS_FULL_BM	0x0020
#define ELPCI_CAPS_FRAG_BM	0x0040
#define ELPCI_CAPS_CRC_PASSTHRU	0x0080
#define ELPCI_CAPS_TXDONE	0x0100
#define ELPCI_CAPS_NO_TXLENGTH	0x0200
#define ELPCI_CAPS_RX_REPEAT	0x0400
#define ELPCI_CAPS_SNOOPING	0x0800
#define ELPCI_CAPS_100MBPS	0x1000
#define ELPCI_CAPS_PWRMGMT	0x2000

/*
 * Window 1 registers (3c905 only)
 */

#define ELPCI_W1_TX_FIFO	0x10
#define ELPCI_W1_FREE_TX	0x0C
#define ELPCI_W1_TX_STATUS	0x0B
#define ELPCI_W1_TX_TIMER	0x0A
#define ELPCI_W1_RX_STATUS	0x08
#define ELPCI_W1_RX_FIFO	0x00

/*
 * RX status codes
 */

#define ELPCI_RXSTATUS_OVERRUN	0x01
#define ELPCI_RXSTATUS_RUNT	0x02
#define ELPCI_RXSTATUS_ALIGN	0x04
#define ELPCI_RXSTATUS_CRC	0x08
#define ELPCI_RXSTATUS_OVERSIZE	0x10
#define ELPCI_RXSTATUS_DRIBBLE	0x20

/*
 * TX status codes
 */

#define ELPCI_TXSTATUS_RECLAIM	0x02    /* 3c905B only */
#define ELPCI_TXSTATUS_OVERFLOW	0x04
#define ELPCI_TXSTATUS_MAXCOLS	0x08
#define ELPCI_TXSTATUS_UNDERRUN	0x10
#define ELPCI_TXSTATUS_JABBER	0x20
#define ELPCI_TXSTATUS_INTREQ	0x40
#define ELPCI_TXSTATUS_COMPLETE	0x80

#define ELPCI_TXSTATUS_ERRSUM				\
    (ELPCI_TXSTATUS_OVERFLOW|ELPCI_TXSTATUS_MAXCOLS|	\
     ELPCI_TXSTATUS_UNDERRUN|ELPCI_TXSTATUS_JABBER)

/*
 * Window 2 registers
 */

#define ELPCI_W2_RESET_OPTIONS		0x0C    /* 3c905B only */
#define ELPCI_W2_STATION_MASK_HI	0x0A
#define ELPCI_W2_STATION_MASK_MID	0x08
#define ELPCI_W2_STATION_MASK_LO	0x06
#define ELPCI_W2_STATION_ADDR_HI	0x04
#define ELPCI_W2_STATION_ADDR_MID	0x02
#define ELPCI_W2_STATION_ADDR_LO	0x00

#define ELPCI_RESETOPT_FEATUREMASK	(0x0001 | 0x0002 | 0x004)
#define ELPCI_RESETOPT_D3RESETDIS	0x0008
#define ELPCI_RESETOPT_DISADVFD		0x0010
#define ELPCI_RESETOPT_DISADV100	0x0020
#define ELPCI_RESETOPT_DISAUTONEG	0x0040
#define ELPCI_RESETOPT_DEBUGMODE	0x0080
#define ELPCI_RESETOPT_FASTAUTO		0x0100
#define ELPCI_RESETOPT_FASTEE		0x0200
#define ELPCI_RESETOPT_FORCEDCONF	0x0400
#define ELPCI_RESETOPT_TESTPDTPDR	0x0800
#define ELPCI_RESETOPT_TEST100TX	0x1000
#define ELPCI_RESETOPT_TEST100RX	0x2000

#define ELPCI_RESETOPT_INVERT_LED	0x0010
#define ELPCI_RESETOPT_INVERT_MII	0x4000

/*
 * Window 3 (fifo management)
 */
#define ELPCI_W3_INTERNAL_CFG	0x00
#define ELPCI_W3_MAXPKTSIZE	0x04    /* 3c905B only */
#define ELPCI_W3_RESET_OPT	0x08
#define ELPCI_W3_MEDIA_OP	0x08
#define ELPCI_W3_FREE_TX	0x0C
#define ELPCI_W3_FREE_RX	0x0A
#define ELPCI_W3_MAC_CTRL	0x06

#define ELPCI_ICFG_CONNECTOR_MASK	0x00F00000
#define ELPCI_ICFG_CONNECTOR_BITS	20

#define ELPCI_ICFG_RAMSIZE_MASK	0x00000007
#define ELPCI_ICFG_RAMWIDTH	0x00000008
#define ELPCI_ICFG_ROMSIZE_MASK	0x000000C0  
#define ELPCI_ICFG_DISABLE_BASSD 0x00000100
#define ELPCI_ICFG_RAMLOC	0x00000200
#define ELPCI_ICFG_RAMPART	0x00030000
#define ELPCI_ICFG_XCVRSEL	0x00F00000
#define ELPCI_ICFG_AUTOSEL	0x01000000

#define ELPCI_XCVR_10BT		0x00000000
#define ELPCI_XCVR_AUI		0x00100000
#define ELPCI_XCVR_RSVD_0	0x00200000
#define ELPCI_XCVR_COAX		0x00300000
#define ELPCI_XCVR_100BTX	0x00400000
#define ELPCI_XCVR_100BFX	0x00500000
#define ELPCI_XCVR_MII		0x00600000
#define ELPCI_XCVR_RSVD_1	0x00700000
#define ELPCI_XCVR_AUTO		0x00800000	/* 3c905B only */

#define ELPCI_MACCTRL_DEFER_EXT_END	0x0001
#define ELPCI_MACCTRL_DEFER_0		0x0002
#define ELPCI_MACCTRL_DEFER_1		0x0004
#define ELPCI_MACCTRL_DEFER_2		0x0008
#define ELPCI_MACCTRL_DEFER_3		0x0010
#define ELPCI_MACCTRL_DUPLEX		0x0020
#define ELPCI_MACCTRL_ALLOW_LARGE_PACK	0x0040
#define ELPCI_MACCTRL_EXTEND_AFTER_COL	0x0080  /* 3c905B only */
#define ELPCI_MACCTRL_FLOW_CONTROL_ENB	0x0100  /* 3c905B only */
#define ELPCI_MACCTRL_VLT_END		0x0200  /* 3c905B only */

/*
 * The 'reset options' register contains power-on reset values
 * loaded from the EEPROM. This includes the supported media
 * types on the card. It is also known as the media options register.
 */

#define ELPCI_MEDIAOPT_BT4         0x0001  /* MII */
#define ELPCI_MEDIAOPT_BTX         0x0002  /* on-chip */
#define ELPCI_MEDIAOPT_BFX         0x0004  /* on-chip */
#define ELPCI_MEDIAOPT_BT          0x0008  /* on-chip */
#define ELPCI_MEDIAOPT_BNC         0x0010  /* on-chip */
#define ELPCI_MEDIAOPT_AUI         0x0020  /* on-chip */
#define ELPCI_MEDIAOPT_MII         0x0040  /* MII */
#define ELPCI_MEDIAOPT_VCO         0x0100  /* 1st gen chip only */

#define ELPCI_MEDIAOPT_10FL        0x0100  /* 3x905B only, on-chip */
#define ELPCI_MEDIAOPT_MASK        0x01FF

/*
 * Window 4 registers (diagnostics)
 */

#define ELPCI_W4_UPPERBYTESOK	0x0D
#define ELPCI_W4_BADSSD		0x0C
#define ELPCI_W4_MEDIA_STATUS	0x0A
#define ELPCI_W4_PHY_MGMT	0x08
#define ELPCI_W4_NET_DIAG	0x06
#define ELPCI_W4_FIFO_DIAG	0x04
#define ELPCI_W4_VCO_DIAG	0x02

#define ELPCI_MII_CLK		0x01
#define ELPCI_MII_DATA		0x02
#define ELPCI_MII_DIR		0x04

#define ELPCI_MEDIA_SQE		0x0008
#define ELPCI_MEDIA_10TP	0x00C0
#define ELPCI_MEDIA_LNK		0x0080
#define ELPCI_MEDIA_LNKBEAT	0x0800

#define ELPCI_MEDIASTAT_CRCSTRIP	0x0004
#define ELPCI_MEDIASTAT_SQEENB		0x0008
#define ELPCI_MEDIASTAT_COLDET		0x0010
#define ELPCI_MEDIASTAT_CARRIER		0x0020
#define ELPCI_MEDIASTAT_JABGUARD	0x0040
#define ELPCI_MEDIASTAT_LINKBEAT	0x0080
#define ELPCI_MEDIASTAT_JABDETECT	0x0200
#define ELPCI_MEDIASTAT_POLREVERS	0x0400
#define ELPCI_MEDIASTAT_LINKDETECT	0x0800
#define ELPCI_MEDIASTAT_TXINPROG	0x1000
#define ELPCI_MEDIASTAT_DCENB		0x4000
#define ELPCI_MEDIASTAT_AUIDIS		0x8000

#define ELPCI_NETDIAG_TEST_LOWVOLT	0x0001
#define ELPCI_NETDIAG_ASIC_REVMASK	0x003E
#define ELPCI_NETDIAG_UPPER_BYTES_ENB	0x0040
#define ELPCI_NETDIAG_STATS_ENABLED	0x0080
#define ELPCI_NETDIAG_TX_FATALERR	0x0100
#define ELPCI_NETDIAG_TRANSMITTING	0x0200
#define ELPCI_NETDIAG_RX_ENABLED	0x0400
#define ELPCI_NETDIAG_TX_ENABLED	0x0800
#define ELPCI_NETDIAG_FIFO_LOOPBACK	0x1000
#define ELPCI_NETDIAG_MAC_LOOPBACK	0x2000
#define ELPCI_NETDIAG_ENDEC_LOOPBACK	0x4000
#define ELPCI_NETDIAG_EXTERNAL_LOOP	0x8000

/*
 * Window 5 registers
 */

#define ELPCI_W5_INTR_ENB	0x0C
#define ELPCI_W5_INTR_MASK	0x0A
#define ELPCI_W5_RECLAIM_THRESH	0x09    /* 3c905B only */
#define ELPCI_W5_RX_FILTER	0x08
#define ELPCI_W5_RX_EARLYTHRESH	0x06
#define ELPCI_W5_TX_AVAILTHRESH	0x02
#define ELPCI_W5_TX_STARTTHRESH	0x00

/*
 * RX filter bits
 */
#define ELPCI_RXFILTER_UCAST		0x01
#define ELPCI_RXFILTER_ALLMULTI		0x02
#define ELPCI_RXFILTER_BCAST		0x04
#define ELPCI_RXFILTER_PROMISC		0x08
#define ELPCI_RXFILTER_MULTIHASH	0x10    /* 3c905B only */

/*
 * Window 6 registers (stats)
 */

#define ELPCI_W6_TX_BYTES_OK	0x0C
#define ELPCI_W6_RX_BYTES_OK	0x0A
#define ELPCI_W6_UPPER_FRAMES_OK	0x09
#define ELPCI_W6_DEFERRED		0x08
#define ELPCI_W6_RX_OK		0x07
#define ELPCI_W6_TX_OK		0x06
#define ELPCI_W6_RX_OVERRUN	0x05
#define ELPCI_W6_COL_LATE		0x04
#define ELPCI_W6_COL_SINGLE	0x03
#define ELPCI_W6_COL_MULTIPLE	0x02
#define ELPCI_W6_SQE_ERRORS	0x01
#define ELPCI_W6_CARRIER_LOST	0x00

/*
 * Window 7 (bus master control)
 */

#define ELPCI_W7_BM_STATUS		0x0B	/* 3c905 only */
#define ELPCI_W7_BM_TIMER		0x0A	/* 3c905 only */
#define ELPCI_W7_VLAN_MASK		0x00	/* 3c905B only */
#define ELPCI_W7_VLAN_ETHERTYPE	0x04	/* 3c905B only */


/*
 * DMA descriptor structures.
 * The 3Com descriptors contain a 63-entry fragment list, which allows
 * us to always use a single descriptor per packet. For RX however,
 * we always use just one fragment, so to save space we define a
 * special cut down descriptor format with a single fragment entry
 * specifically for RX use.
 *
 * Descriptors are joined together in a linked list.
 */

#define ELPCI_MAXFRAG	63
#define ELPCI_LASTFRAG	0x80000000
#define ELPCI_RX_DESC_CNT	32
#define ELPCI_TX_DESC_CNT	32

typedef struct elPci_frag
    {
    volatile UINT32	elPci_addr;
    volatile UINT32	elPci_len;
    } ELPCI_FRAG;

typedef struct elPci_rx_desc
    {
    volatile UINT32	elPci_next;
    volatile UINT32	elPci_status;
    volatile UINT32     elPci_addr;
    volatile UINT32     elPci_len;
    } ELPCI_RX_DESC;

typedef struct elPci_tx_desc
    {
    volatile UINT32     elPci_next;
    volatile UINT32     elPci_status;
    volatile ELPCI_FRAG	elPci_frags[ELPCI_MAXFRAG];
    } ELPCI_TX_DESC;

#define ELPCI_RXSTAT_LENMASK	0x00001FFF
#define ELPCI_RXSTAT_ERROR	0x00004000
#define ELPCI_RXSTAT_UL_DONE	0x00008000
#define ELPCI_RXSTAT_OVERRUN	0x00010000
#define ELPCI_RXSTAT_RUNT	0x00020000
#define ELPCI_RXSTAT_ALIGN	0x00040000
#define ELPCI_RXSTAT_CRC	0x00080000
#define ELPCI_RXSTAT_OVERSIZE	0x00100000
#define ELPCI_RXSTAT_DRIBBLE	0x00800000
#define ELPCI_RXSTAT_UP_OFLOW	0x01000000
#define ELPCI_RXSTAT_IPCKERR	0x02000000      /* 3c905B only */
#define ELPCI_RXSTAT_TCPCKERR	0x04000000      /* 3c905B only */
#define ELPCI_RXSTAT_UDPCKERR	0x08000000      /* 3c905B only */
#define ELPCI_RXSTAT_BUFEN	0x10000000      /* 3c905B only */
#define ELPCI_RXSTAT_IPCKOK	0x20000000      /* 3c905B only */
#define ELPCI_RXSTAT_TCPCOK	0x40000000      /* 3c905B only */
#define ELPCI_RXSTAT_UDPCKOK	0x80000000      /* 3c905B only */

#define ELPCI_TXSTAT_LENMASK	0x00001FFF
#define ELPCI_TXSTAT_CRCDIS	0x00002000
#define ELPCI_TXSTAT_TX_INTR	0x00008000
#define ELPCI_TXSTAT_DL_DONE	0x00010000
#define ELPCI_TXSTAT_IPCKSUM	0x02000000      /* 3c905B only */
#define ELPCI_TXSTAT_TCPCKSUM	0x04000000      /* 3c905B only */
#define ELPCI_TXSTAT_UDPCKSUM	0x08000000      /* 3c905B only */
#define ELPCI_TXSTAT_RND_DEFEAT	0x10000000      /* 3c905B only */
#define ELPCI_TXSTAT_EMPTY	0x20000000      /* 3c905B only */
#define ELPCI_TXSTAT_DL_INTR	0x80000000


#define ELPCI_CLSIZE	1536
#define ELPCI_NAME	"elPci"
#define ELPCI_TIMEOUT 10000

#define ELPCI_MAX_RX	16
#define ELPCI_INC_DESC(x, y)	(x) = ((x) + 1) % y

#define ELPCI_INTRS		\
    (ELPCI_STAT_UP_COMPLETE | ELPCI_STAT_DOWN_COMPLETE|	\
     ELPCI_STAT_TX_COMPLETE | ELPCI_STAT_TX_COMPLETE | ELPCI_STAT_INTLATCH)
#define ELPCI_RXINTRS	(ELPCI_STAT_UP_COMPLETE | ELPCI_STAT_RX_COMPLETE)
#define ELPCI_TXINTRS	(ELPCI_STAT_DOWN_COMPLETE | ELPCI_STAT_TX_COMPLETE)

#define ELPCI_ADJ(x)	 m_adj((x), 2)
#define ELPCI_TYPE_905		1
#define ELPCI_TYPE_905B		2

#define ELPCI_INT_PENDING	0x80000000

/*
 * Private adapter context structure.
 */

typedef struct elPci_drv_ctrl
    {
    END_OBJ		elPciEndObj;
    VXB_DEVICE_ID	elPciDev;
    void *		elPciBar;
    void *		elPciHandle;
    void *		elPciMuxDevCookie;

    JOB_QUEUE_ID	elPciJobQueue;
    QJOB		elPciIntJob;
    atomicVal_t		elPciIntPending;
    atomicVal_t		elPciIntStatus;

    QJOB		elPciRxJob;
    atomicVal_t		elPciRxPending;

    QJOB		elPciTxJob;
    atomicVal_t		elPciTxPending;
    UINT8		elPciTxCur;
    UINT8		elPciTxLast;
    volatile BOOL	elPciTxStall;
    UINT16		elPciTxThresh;

    QJOB		elPciLinkJob;
    volatile BOOL	elPciLinkPending;

    BOOL		elPciPolling;
    M_BLK_ID		elPciPollBuf;
    UINT16		elPciIntMask;
    UINT16		elPciIntrs;

    UINT8		elPciAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	elPciCaps;

    END_IFDRVCONF	elPciEndStatsConf;
    END_IFCOUNTERS	elPciEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*elPciMediaList;
    END_ERR		elPciLastError;
    UINT32		elPciCurMedia;
    UINT32		elPciCurStatus;
    VXB_DEVICE_ID	elPciMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	elPciParentTag;

    VXB_DMA_TAG_ID	elPciRxDescTag;
    VXB_DMA_MAP_ID	elPciRxDescMap;
    ELPCI_RX_DESC	*elPciRxDescMem;

    VXB_DMA_TAG_ID	elPciTxDescTag;
    VXB_DMA_MAP_ID	elPciTxDescMap;
    ELPCI_TX_DESC	*elPciTxDescMem;
    ELPCI_TX_DESC	*elPciTxPrev[ELPCI_TX_DESC_CNT];
    UINT32		elPciTxPhys[ELPCI_TX_DESC_CNT];

    VXB_DMA_TAG_ID	elPciMblkTag;

    VXB_DMA_MAP_ID	elPciRxMblkMap[ELPCI_RX_DESC_CNT];
    VXB_DMA_MAP_ID	elPciTxMblkMap[ELPCI_TX_DESC_CNT];

    M_BLK_ID		elPciRxMblk[ELPCI_RX_DESC_CNT];
    M_BLK_ID		elPciTxMblk[ELPCI_TX_DESC_CNT];

    UINT32		elPciTxProd;
    UINT32		elPciTxCons;
    UINT32		elPciTxFree;
    UINT32		elPciRxIdx;

    UINT8		elPciEeWidth;
    SEM_ID		elPciDevSem;
    spinlockIsr_t	elPciLock;
    UINT8		elPciType;
    UINT32		elPciXcvr;

    } ELPCI_DRV_CTRL;

#define ELPCI_BAR(p)   ((ELPCI_DRV_CTRL *)(p)->pDrvCtrl)->elPciBar
#define ELPCI_HANDLE(p)   ((ELPCI_DRV_CTRL *)(p)->pDrvCtrl)->elPciHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (ELPCI_HANDLE(pDev), (UINT32 *)((char *)ELPCI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (ELPCI_HANDLE(pDev),                             \
        (UINT32 *)((char *)ELPCI_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (ELPCI_HANDLE(pDev), (UINT16 *)((char *)ELPCI_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (ELPCI_HANDLE(pDev),                             \
        (UINT16 *)((char *)ELPCI_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (ELPCI_HANDLE(pDev), (UINT8 *)((char *)ELPCI_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (ELPCI_HANDLE(pDev),                              \
        (UINT8 *)((char *)ELPCI_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCtc3c905VxbEndh */
