/* vxbTiEmacEnd.h - TI EMAC VxBus END driver header */

/*
 * Copyright (c) 2008-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,06sep11,clx  add GMII support and optimize Rx BD List.
01d,08aug11,hcl  add AM3517 emac support
01c,11oct10,hcl  change emac driver file name.
01b,09feb09,b_m  add OMAP-L137 EMAC support.
01a,08jan08,b_m  written.
*/

#ifndef __INCvxbTiEmacEndh
#define __INCvxbTiEmacEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void tiEmacRegister(void);

/* EMAC Types */

#define EMAC_TYPE_DM644X        0
#define EMAC_TYPE_L137          1
#define EMAC_TYPE_AM35XX        2

/* AM3517 EMAC Memory Address */

#define AM3517_EMAC_HW_RAM      0x01E20000
#define AM3517_EMAC_RAM         0x5C020000

/* EMAC Media */

#define EMAC_MEDIA_MII          0
#define EMAC_MEDIA_RMII         1
#define EMAC_MEDIA_GMII         2

#ifndef BSP_VERSION

/* EMAC modules */

#define EMAC_MOD_NUM            3

/* EMAC control module register offsets */

/* the following register offsets applies to DM644x EMAC */

#define EMAC_INT_CTRL           0x04    /* interrupt control register */
#define EMAC_INT_TIMER_CNT      0x08    /* interrupt timer count register */

/* the following register offsets applies to L137 EMAC */

#define EMAC_L137_REVID         0x00    /* revision ID register */
#define EMAC_L137_SOFT_RESET    0x04    /* software reset register */
#define EMAC_L137_INT_CTRL      0x0C    /* interrupt control register */
#define EMAC_L137_C0RXTHR_EN    0x10    /* interrupt core 0 rx threshold en */
#define EMAC_L137_C0RX_EN       0x14    /* interrupt core 0 rx en */
#define EMAC_L137_C0TX_EN       0x18    /* interrupt core 0 tx en */
#define EMAC_L137_C0MISC_EN     0x1C    /* interrupt core 0 misc en */
#define EMAC_L137_C1RXTHR_EN    0x20    /* interrupt core 1 rx threshold en */
#define EMAC_L137_C1RX_EN       0x24    /* interrupt core 1 rx en */
#define EMAC_L137_C1TX_EN       0x28    /* interrupt core 1 tx en */
#define EMAC_L137_C1MISC_EN     0x2C    /* interrupt core 1 misc en */
#define EMAC_L137_C2RXTHR_EN    0x30    /* interrupt core 2 rx threshold en */
#define EMAC_L137_C2RX_EN       0x34    /* interrupt core 2 rx en */
#define EMAC_L137_C2TX_EN       0x38    /* interrupt core 2 tx en */
#define EMAC_L137_C2MISC_EN     0x3C    /* interrupt core 2 misc en */
#define EMAC_L137_C0RXTHR_STAT  0x40    /* interrupt core 0 rx threshold status */
#define EMAC_L137_C0RX_STAT     0x44    /* interrupt core 0 rx status */
#define EMAC_L137_C0TX_STAT     0x48    /* interrupt core 0 tx status */
#define EMAC_L137_C0MISC_STAT   0x4C    /* interrupt core 0 misc status */
#define EMAC_L137_C1RXTHR_STAT  0x50    /* interrupt core 1 rx threshold status */
#define EMAC_L137_C1RX_STAT     0x54    /* interrupt core 1 rx status */
#define EMAC_L137_C1TX_STAT     0x58    /* interrupt core 1 tx status */
#define EMAC_L137_C1MISC_STAT   0x5C    /* interrupt core 1 misc status */
#define EMAC_L137_C2RXTHR_STAT  0x60    /* interrupt core 2 rx threshold status */
#define EMAC_L137_C2RX_STAT     0x64    /* interrupt core 2 rx status */
#define EMAC_L137_C2TX_STAT     0x68    /* interrupt core 2 tx status */
#define EMAC_L137_C2MISC_STAT   0x6C    /* interrupt core 2 misc status */
#define EMAC_L137_C0RX_IMAX     0x70    /* interrupt core 0 rx int per ms */
#define EMAC_L137_C0TX_IMAX     0x74    /* interrupt core 0 tx int per ms */
#define EMAC_L137_C1RX_IMAX     0x78    /* interrupt core 1 rx int per ms */
#define EMAC_L137_C1TX_IMAX     0x7C    /* interrupt core 1 tx int per ms */
#define EMAC_L137_C2RX_IMAX     0x80    /* interrupt core 2 rx int per ms */
#define EMAC_L137_C2TX_IMAX     0x84    /* interrupt core 2 tx int per ms */

/* EMAC MDIO module register offsets */

#define MDIO_VER                0x00    /* MDIO version register */
#define MDIO_CTRL               0x04    /* MDIO control register */
#define MDIO_ALIVE              0x08    /* PHY alive status register */
#define MDIO_LINK               0x0C    /* PHY link status register */
#define MDIO_LINK_INT_RAW       0x10    /* link status change int raw */
#define MDIO_LINK_INT_MASKED    0x14    /* link status change int masked */
#define MDIO_USER_INT_RAW       0x20    /* user cmd complete int raw */
#define MDIO_USER_INT_MASKED    0x24    /* user cmd complete int masked */
#define MDIO_USER_INT_MASK_SET  0x28    /* user cmd complete int mask set */
#define MDIO_USER_INT_MASK_CLR  0x2C    /* user cmd complete int mask clear */
#define MDIO_USER_ACCESS_0      0x80    /* user access register 0 */
#define MDIO_PHY_SEL_0          0x84    /* user PHY select register 0 */
#define MDIO_USER_ACCESS_1      0x88    /* user access register 1 */
#define MDIO_PHY_SEL_1          0x8C    /* user PHY select register 1 */

/* EMAC module register offsets */

#define EMAC_TX_IDVER           0x0000  /* Tx ID & version register */
#define EMAC_TX_CTRL            0x0004  /* Tx control register */
#define EMAC_TX_TEARDOWN        0x0008  /* Tx teardown register */
#define EMAC_RX_IDVER           0x0010  /* Rx ID & version register */
#define EMAC_RX_CTRL            0x0014  /* Rx control register */
#define EMAC_RX_TEARDOWN        0x0018  /* Rx teardown register */
#define EMAC_TX_INT_RAW         0x0080  /* Tx interrupt status raw */
#define EMAC_TX_INT_MASKED      0x0084  /* Tx interrupt status masked */
#define EMAC_TX_INT_MASK_SET    0x0088  /* Tx interrupt status mask set */
#define EMAC_TX_INT_MASK_CLR    0x008C  /* Tx interrupt status mask clear */
#define EMAC_MAC_IN_VECTOR      0x0090  /* MAC input vector register */
#define EMAC_MAC_EOI_VECTOR     0x0094  /* MAC end of interrupt vector register */
#define EMAC_RX_INT_RAW         0x00A0  /* Rx interrupt status raw */
#define EMAC_RX_INT_MASKED      0x00A4  /* Rx interrupt status masked */
#define EMAC_RX_INT_MASK_SET    0x00A8  /* Rx interrupt status mask set */
#define EMAC_RX_INT_MASK_CLR    0x00AC  /* Rx interrupt status mask clear */
#define EMAC_MAC_INT_RAW        0x00B0  /* MAC interrupt status raw */
#define EMAC_MAC_INT_MASKED     0x00B4  /* MAC interrupt status masked */
#define EMAC_MAC_INT_MASK_SET   0x00B8  /* MAC interrupt status mask set */
#define EMAC_MAC_INT_MASK_CLR   0x00BC  /* MAC interrupt status mask clear */
#define EMAC_RX_MBP_EN          0x0100  /* Rx mcast/bcast/promiscuout enable */
#define EMAC_RX_UCAST_SET       0x0104  /* Rx unicast set register */
#define EMAC_RX_UCAST_CLR       0x0108  /* Rx unicast clear register */
#define EMAC_RX_MAX_LEN         0x010C  /* Rx maximum length register */
#define EMAC_RX_BUF_OFF         0x0110  /* Rx buffer offset register */
#define EMAC_RX_FLT_LOW_THRESH  0x0114  /* Rx filter low frame threshold */
#define EMAC_RX0_FLOW_THRESH    0x0120  /* Rx channel 0 flow ctrl threshold */
#define EMAC_RX1_FLOW_THRESH    0x0124  /* Rx channel 1 flow ctrl threshold */
#define EMAC_RX2_FLOW_THRESH    0x0128  /* Rx channel 2 flow ctrl threshold */
#define EMAC_RX3_FLOW_THRESH    0x012C  /* Rx channel 3 flow ctrl threshold */
#define EMAC_RX4_FLOW_THRESH    0x0130  /* Rx channel 4 flow ctrl threshold */
#define EMAC_RX5_FLOW_THRESH    0x0134  /* Rx channel 5 flow ctrl threshold */
#define EMAC_RX6_FLOW_THRESH    0x0138  /* Rx channel 6 flow ctrl threshold */
#define EMAC_RX7_FLOW_THRESH    0x013C  /* Rx channel 7 flow ctrl threshold */
#define EMAC_RX0_FREE_BUFFER    0x0140  /* Rx channel 0 free buffer count */
#define EMAC_RX1_FREE_BUFFER    0x0144  /* Rx channel 1 free buffer count */
#define EMAC_RX2_FREE_BUFFER    0x0148  /* Rx channel 2 free buffer count */
#define EMAC_RX3_FREE_BUFFER    0x014C  /* Rx channel 3 free buffer count */
#define EMAC_RX4_FREE_BUFFER    0x0150  /* Rx channel 4 free buffer count */
#define EMAC_RX5_FREE_BUFFER    0x0154  /* Rx channel 5 free buffer count */
#define EMAC_RX6_FREE_BUFFER    0x0158  /* Rx channel 6 free buffer count */
#define EMAC_RX7_FREE_BUFFER    0x015C  /* Rx channel 7 free buffer count */
#define EMAC_MAC_CTRL           0x0160  /* MAC control register */
#define EMAC_MAC_STATUS         0x0164  /* MAC status register */
#define EMAC_EM_CTRL            0x0168  /* emulation control register */
#define EMAC_FIFO_CTRL          0x016C  /* FIFO control register */
#define EMAC_MAC_CFG            0x0170  /* MAC configuration register */
#define EMAC_SOFT_RESET         0x0174  /* soft reset register */
#define EMAC_MAC_SRC_ADDR_LO    0x01D0  /* MAC source address low */
#define EMAC_MAC_SRC_ADDR_HI    0x01D4  /* MAC source address high */
#define EMAC_MAC_HASH1          0x01D8  /* MAC hash address register 1 */
#define EMAC_MAC_HASH2          0x01DC  /* MAC hash address register 2 */
#define EMAC_BACK_OFF_TEST      0x01E0  /* back off test register */
#define EMAC_TX_PACE_TEST       0x01E4  /* Tx pacing algorithm test register */
#define EMAC_RX_PAUSE           0x01E8  /* Rx pause timer register */
#define EMAC_TX_PAUSE           0x01EC  /* Tx pause timer register */
#define EMAC_MAC_ADDR_LO        0x0500  /* MAC address low (Rx matching) */
#define EMAC_MAC_ADDR_HI        0x0504  /* MAC address high (Rx matching) */
#define EMAC_MAC_INDEX          0x0508  /* MAC index register */
#define EMAC_TX0_HDP            0x0600  /* Tx channel 0 DMA head descriptor */
#define EMAC_TX1_HDP            0x0604  /* Tx channel 1 DMA head descriptor */
#define EMAC_TX2_HDP            0x0608  /* Tx channel 2 DMA head descriptor */
#define EMAC_TX3_HDP            0x060C  /* Tx channel 3 DMA head descriptor */
#define EMAC_TX4_HDP            0x0610  /* Tx channel 4 DMA head descriptor */
#define EMAC_TX5_HDP            0x0614  /* Tx channel 5 DMA head descriptor */
#define EMAC_TX6_HDP            0x0618  /* Tx channel 6 DMA head descriptor */
#define EMAC_TX7_HDP            0x061C  /* Tx channel 7 DMA head descriptor */
#define EMAC_RX0_HDP            0x0620  /* Rx channel 0 DMA head descriptor */
#define EMAC_RX1_HDP            0x0624  /* Rx channel 1 DMA head descriptor */
#define EMAC_RX2_HDP            0x0628  /* Rx channel 2 DMA head descriptor */
#define EMAC_RX3_HDP            0x062C  /* Rx channel 3 DMA head descriptor */
#define EMAC_RX4_HDP            0x0630  /* Rx channel 4 DMA head descriptor */
#define EMAC_RX5_HDP            0x0634  /* Rx channel 5 DMA head descriptor */
#define EMAC_RX6_HDP            0x0638  /* Rx channel 6 DMA head descriptor */
#define EMAC_RX7_HDP            0x063C  /* Rx channel 7 DMA head descriptor */
#define EMAC_TX0_CP             0x0640  /* Tx channel 0 completion pointer */
#define EMAC_TX1_CP             0x0644  /* Tx channel 1 completion pointer */
#define EMAC_TX2_CP             0x0648  /* Tx channel 2 completion pointer */
#define EMAC_TX3_CP             0x064C  /* Tx channel 3 completion pointer */
#define EMAC_TX4_CP             0x0650  /* Tx channel 4 completion pointer */
#define EMAC_TX5_CP             0x0654  /* Tx channel 5 completion pointer */
#define EMAC_TX6_CP             0x0658  /* Tx channel 6 completion pointer */
#define EMAC_TX7_CP             0x065C  /* Tx channel 7 completion pointer */
#define EMAC_RX0_CP             0x0660  /* Rx channel 0 completion pointer */
#define EMAC_RX1_CP             0x0664  /* Rx channel 1 completion pointer */
#define EMAC_RX2_CP             0x0668  /* Rx channel 2 completion pointer */
#define EMAC_RX3_CP             0x066C  /* Rx channel 3 completion pointer */
#define EMAC_RX4_CP             0x0670  /* Rx channel 4 completion pointer */
#define EMAC_RX5_CP             0x0674  /* Rx channel 5 completion pointer */
#define EMAC_RX6_CP             0x0678  /* Rx channel 6 completion pointer */
#define EMAC_RX7_CP             0x067C  /* Rx channel 7 completion pointer */

/* EMAC network statistics registers */

#define EMAC_RX_GOOD_FRAMES     0x0200
#define EMAC_RX_BCAST_FRAMES    0x0204
#define EMAC_RX_MCAST_FRAMES    0x0208
#define EMAC_RX_PAUSE_FRAMES    0x020C
#define EMAC_RX_CRC_ERRORS      0x0210
#define EMAC_RX_ALIGN_ERRORS    0x0214
#define EMAC_RX_OVERSIZED       0x0218
#define EMAC_RX_JABBER          0x021C
#define EMAC_RX_UNDERSIZED      0x0220
#define EMAC_RX_FRAGMENTS       0x0224
#define EMAC_RX_FILTERED        0x0228
#define EMAC_RX_QOS_FILTERED    0x022C
#define EMAC_RX_OCTETS          0x0230
#define EMAC_TX_GOOD_FRAMES     0x0234
#define EMAC_TX_BCAST_FRAMES    0x0238
#define EMAC_TX_MCAST_FRAMES    0x023C
#define EMAC_TX_PAUSE_FRAMES    0x0240
#define EMAC_TX_DEFERRED        0x0244
#define EMAC_TX_COLLISION       0x0248
#define EMAC_TX_SINGLE_COLL     0x024C
#define EMAC_TX_MULTI_COLL      0x0250
#define EMAC_TX_EXCESSIVE_COLL  0x0254
#define EMAC_TX_LATE_COLL       0x0258
#define EMAC_TX_UNDERRUN        0x025C
#define EMAC_TX_CARRIER_SENSE   0x0260
#define EMAC_TX_OCTETS          0x0264
#define EMAC_FRAME64            0x0268
#define EMAC_FRAME65T127        0x026C
#define EMAC_FRAME128T255       0x0270
#define EMAC_FRAME256T511       0x0274
#define EMAC_FRAME512T1023      0x0278
#define EMAC_FRAME1024TUP       0x027C
#define EMAC_NET_OCTETS         0x0280
#define EMAC_RX_SOF_OVERRUNS    0x0284
#define EMAC_RX_MOF_OVERRUNS    0x0288
#define EMAC_RX_DMA_OVERRUNS    0x028C

/* MIB base address & size */

#define EMAC_MIB_BASE           0x0200
#define EMAC_MIB_SIZE           0x0090

/* CSR bit definitions */

/* EMAC control module interrupt control register */

#define EMAC_INT_CTRL_EN        0x00000001

/* MDIO control register */

#define MDIO_CTRL_IDLE          0x80000000
#define MDIO_CTRL_EN            0x40000000
#define MDIO_CTRL_CLKDIV_MASK   0x0000ffff
#define MDIO_CTRL_CLK_STD       2000000

/* MDIO user access register */

#define MDIO_ACCESS_GO          0x80000000
#define MDIO_ACCESS_READ        0x00000000
#define MDIO_ACCESS_WRITE       0x40000000
#define MDIO_ACCESS_ACK         0x20000000
#define MDIO_ACCESS_REG_MASK    0x03e00000
#define MDIO_ACCESS_PHY_MASK    0x001f0000
#define MDIO_ACCESS_DATA_MASK   0x0000ffff
#define MDIO_ACCESS_REG_SHIFT   21
#define MDIO_ACCESS_PHY_SHIFT   16

/* transmit control register */

#define EMAC_TX_CTRL_EN         0x00000001

/* receive control register */

#define EMAC_RX_CTRL_EN         0x00000001

/* channel teardown register */

#define EMAC_CH_TEARDOWN_VALUE  0xfffffffc

/* MAC input vector register */

#define EMAC_INT_USER           0x80000000
#define EMAC_INT_LINK           0x40000000
#define EMAC_INT_HOST           0x00020000
#define EMAC_INT_STAT           0x00010000
#define EMAC_INT_RX_MASK        0x0000ff00
#define EMAC_INT_TX_MASK        0x000000ff
#define EMAC_INT_MASK           (EMAC_INT_RX_MASK | EMAC_INT_TX_MASK)

/* MAC end of interrupt vector register */

#define EMAC_MAC_VEC_C0RX       0x01
#define EMAC_MAC_VEC_C0TX       0x02
#define EMAC_MAC_VEC_C0MISC     0x03

/* EMAC channel numbers */

#define EMAC_CH_0               0
#define EMAC_CH_1               1
#define EMAC_CH_2               2
#define EMAC_CH_3               3
#define EMAC_CH_4               4
#define EMAC_CH_5               5
#define EMAC_CH_6               6
#define EMAC_CH_7               7
#define EMAC_CH_NUM             8

/* channel interrupt enable */

#define EMAC_CH0_INT_EN         0x00000001
#define EMAC_CH1_INT_EN         0x00000002
#define EMAC_CH2_INT_EN         0x00000004
#define EMAC_CH3_INT_EN         0x00000008
#define EMAC_CH4_INT_EN         0x00000010
#define EMAC_CH5_INT_EN         0x00000020
#define EMAC_CH6_INT_EN         0x00000040
#define EMAC_CH7_INT_EN         0x00000080

/* channel interrupt disable */

#define EMAC_CH0_INT_DIS        0x00000001
#define EMAC_CH1_INT_DIS        0x00000002
#define EMAC_CH2_INT_DIS        0x00000004
#define EMAC_CH3_INT_DIS        0x00000008
#define EMAC_CH4_INT_DIS        0x00000010
#define EMAC_CH5_INT_DIS        0x00000020
#define EMAC_CH6_INT_DIS        0x00000040
#define EMAC_CH7_INT_DIS        0x00000080

/* MAC control register */

#define EMAC_MAC_CTRL_GMII_1000 0x00000080 /* bit7 */
#define EMAC_MAC_CTRL_GMII_EN   0x00000020 /* bit5 */
#define EMAC_MAC_CTRL_GIGFORCE  0x00020000 /* bit17 */

#define EMAC_MAC_CTRL_RMII_100  0x00008000
#define EMAC_MAC_CTRL_PRI_FIX   0x00000200
#define EMAC_MAC_CTRL_TXPACE    0x00000040
#define EMAC_MAC_CTRL_MII_EN    0x00000020
#define EMAC_MAC_CTRL_LOOPBACK  0x00000002
#define EMAC_MAC_CTRL_FD        0x00000001

/* rx multicast/broadcast/promiscuous channel enable register */

#define EMAC_RX_PASSCRC         0x40000000
#define EMAC_RX_QOS_EN          0x20000000
#define EMAC_RX_NO_CHAIN        0x10000000
#define EMAC_RX_CMF_EN          0x01000000
#define EMAC_RX_CSF_EN          0x00800000
#define EMAC_RX_CEF_EN          0x00400000
#define EMAC_RX_CAF_EN          0x00200000
#define EMAC_RX_PROM_CH_MASK    0x00070000
#define EMAC_RX_BCAST_EN        0x00002000
#define EMAC_RX_BCAST_CH_MASK   0x00000700
#define EMAC_RX_MCAST_EN        0x00000020
#define EMAC_RX_MCAST_CH_MASK   0x00000007

/* rx unicast enable set register */

#define EMAC_RX_CH0_EN          0x00000001
#define EMAC_RX_CH1_EN          0x00000002
#define EMAC_RX_CH2_EN          0x00000004
#define EMAC_RX_CH3_EN          0x00000008
#define EMAC_RX_CH4_EN          0x00000010
#define EMAC_RX_CH5_EN          0x00000020
#define EMAC_RX_CH6_EN          0x00000040
#define EMAC_RX_CH7_EN          0x00000080

/* rx unicast clear register */

#define EMAC_RX_CH0_DIS         0x00000001
#define EMAC_RX_CH1_DIS         0x00000002
#define EMAC_RX_CH2_DIS         0x00000004
#define EMAC_RX_CH3_DIS         0x00000008
#define EMAC_RX_CH4_DIS         0x00000010
#define EMAC_RX_CH5_DIS         0x00000020
#define EMAC_RX_CH6_DIS         0x00000040
#define EMAC_RX_CH7_DIS         0x00000080

/* soft reset register */

#define EMAC_SOFT_RESET_EN      0x00000001
#define EMAC_SOFT_RESET_MASK    0x00000001

/* MAC address register */

#define EMAC_MAC_ADDR_VALID     0x00100000
#define EMAC_MAC_ADDR_MATCH     0x00080000

/* packet buffer descriptors definitions */

typedef struct emac_desc
    {
    volatile UINT32 next;
    volatile UINT32 buffer;
    volatile UINT32 buflen_off;
    volatile UINT32 pktlen_flags;
    } EMAC_DESC;

/* TBD bits definitions */

#define EMAC_TBD_SOP            0x80000000  /* start of packet */
#define EMAC_TBD_EOP            0x40000000  /* end of packet */
#define EMAC_TBD_OWNER          0x20000000  /* ownership */
#define EMAC_TBD_EOQ            0x10000000  /* end of queue */
#define EMAC_TBD_TDOWN_DONE     0x08000000  /* teardown complete */
#define EMAC_TBD_PASSCRC        0x04000000  /* pass crc */

/* RBD bits definitions */

#define EMAC_RBD_SOP            0x80000000  /* start of packet */
#define EMAC_RBD_EOP            0x40000000  /* end of packet */
#define EMAC_RBD_OWNER          0x20000000  /* ownership */
#define EMAC_RBD_EOQ            0x10000000  /* end of queue */
#define EMAC_RBD_TDOWN_DONE     0x08000000  /* teardown complete */
#define EMAC_RBD_PASSCRC        0x04000000  /* pass crc */
#define EMAC_RBD_JABBER         0x02000000  /* jabber */
#define EMAC_RBD_OVERSIZE       0x01000000  /* oversize */
#define EMAC_RBD_FRAGMENT       0x00800000  /* fragment */
#define EMAC_RBD_UNDERSIZE      0x00400000  /* undersize */
#define EMAC_RBD_CONTROL        0x00200000  /* control */
#define EMAC_RBD_OVERRUN        0x00100000  /* overrun */
#define EMAC_RBD_CODE_ERR       0x00080000  /* code error */
#define EMAC_RBD_ALIGN_ERR      0x00040000  /* align error */
#define EMAC_RBD_CRC_ERR        0x00020000  /* crc error */
#define EMAC_RBD_NO_MATCH       0x00010000  /* no match (promiscuous mode) */

#define EMAC_RBD_ERR         \
        (EMAC_RBD_JABBER | EMAC_RBD_OVERSIZE | EMAC_RBD_FRAGMENT |  \
         EMAC_RBD_UNDERSIZE | EMAC_RBD_CONTROL | EMAC_RBD_OVERRUN | \
         EMAC_RBD_CODE_ERR | EMAC_RBD_ALIGN_ERR | EMAC_RBD_CRC_ERR)

#define EMAC_MTU                1500
#define EMAC_JUMBO_MTU          9000
#define EMAC_CLSIZE             1536
#define EMAC_NAME               "tiemac"
#define EMAC_TIMEOUT            10000
#define EMAC_MIN_PKT_SIZE       60

#define EMAC_TUPLE_CNT          768

/* internal memory 8K = 256 * 16 * 2 */

#define EMAC_RX_DESC_CNT        256
#define EMAC_TX_DESC_CNT        256
#define EMAC_INC_DESC(x, y)     (x) = (((x) + 1) % y)
#define EMAC_DEC_DESC(x, y)     (x) = (((x) == 0) ? y : ((x) - 1))
#define EMAC_MAXFRAG            16
#define EMAC_MAX_RX             16

#define EMAC_ETHER_ALIGN        2
#define EMAC_ADJ(x)             (x)->m_data += 2

/* private adapter context structure */

typedef struct emacDrvCtrl
    {
    END_OBJ         emacEndObj;
    VXB_DEVICE_ID   emacDev;
    void *          emacMuxDevCookie;

    JOB_QUEUE_ID    emacJobQueue;
    QJOB            emacIntJob;
    volatile BOOL   emacIntPending;
    QJOB            emacRxJob;
    volatile BOOL   emacRxPending;
    QJOB            emacTxJob;
    volatile BOOL   emacTxPending;

    volatile BOOL   emacTxStall;
    volatile BOOL   emacRxStall;

    BOOL            emacPolling;
    M_BLK_ID        emacPollBuf;
    UINT32          emacIntMask;
    UINT32          emacIntrs;

    UINT8           emacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    emacCaps;

    END_IFDRVCONF   emacEndStatsConf;
    END_IFCOUNTERS  emacEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST * emacMediaList;
    END_ERR         emacLastError;
    UINT32          emacCurMedia;
    UINT32          emacCurStatus;
    VXB_DEVICE_ID   emacMiiBus;
    VXB_DEVICE_ID   emacMiiDev;
    FUNCPTR         emacMiiPhyRead;
    FUNCPTR         emacMiiPhyWrite;
    int             emacMiiPhyAddr;

    /* end MII/ifmedia required fields */

    EMAC_DESC *     emacRxDescMem;
    EMAC_DESC *     emacTxDescMem;

    EMAC_DESC *     emacRxCpDesc;   /* store the latest processed Rx Desc */
    EMAC_DESC *     emacRxHdpDesc;  /* write to HDP register to restart Rx */

    M_BLK_ID        emacRxMblk[EMAC_RX_DESC_CNT];
    M_BLK_ID        emacTxMblk[EMAC_TX_DESC_CNT];

    UINT32          emacTxProd;
    UINT32          emacTxCons;
    UINT32          emacTxFree;
    UINT32          emacRxIdx;
    UINT32          emacRxTail;
    BOOL            emacRwHdp;

    UINT32          emacTxPend;
    UINT32          emacTxCnt;

    SEM_ID          emacMiiSem;

    int             emacMaxMtu;

    void *          regBase[EMAC_MOD_NUM];
    void *          handle[EMAC_MOD_NUM];

    UINT32          emacDescMem;
    UINT32          emacFreq;
    int             emacType;
    int             emacMedia;
    } EMAC_DRV_CTRL;

/* EMAC control module register low level access routines */

#define EMAC_BAR0(p)        ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[0]
#define EMAC_HANDLE0(p)     ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[0]

#define CSR0_READ_4(pDev, addr)             \
        vxbRead32(EMAC_HANDLE0(pDev),       \
                (UINT32 *)((char *)EMAC_BAR0(pDev) + addr))

#define CSR0_WRITE_4(pDev, addr, data)      \
        vxbWrite32(EMAC_HANDLE0(pDev),      \
                (UINT32 *)((char *)EMAC_BAR0(pDev) + addr), data)

#define CSR0_SETBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) | (val))

#define CSR0_CLRBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) & ~(val))

/* EMAC MDIO module register low level access routines */

#define EMAC_BAR1(p)        ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[1]
#define EMAC_HANDLE1(p)     ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[1]

#define CSR1_READ_4(pDev, addr)             \
        vxbRead32(EMAC_HANDLE1(pDev),       \
                (UINT32 *)((char *)EMAC_BAR1(pDev) + addr))

#define CSR1_WRITE_4(pDev, addr, data)      \
        vxbWrite32(EMAC_HANDLE1(pDev),      \
                (UINT32 *)((char *)EMAC_BAR1(pDev) + addr), data)

#define CSR1_SETBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) | (val))

#define CSR1_CLRBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) & ~(val))

/* EMAC module register low level access routines */

#define EMAC_BAR2(p)        ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[2]
#define EMAC_HANDLE2(p)     ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[2]

#define CSR2_READ_4(pDev, addr)             \
        vxbRead32(EMAC_HANDLE2(pDev),       \
                (UINT32 *)((char *)EMAC_BAR2(pDev) + addr))

#define CSR2_WRITE_4(pDev, addr, data)      \
        vxbWrite32(EMAC_HANDLE2(pDev),      \
                (UINT32 *)((char *)EMAC_BAR2(pDev) + addr), data)

#define CSR2_SETBIT_4(pDev, offset, val)    \
        CSR2_WRITE_4(pDev, offset, CSR2_READ_4(pDev, offset) | (val))

#define CSR2_CLRBIT_4(pDev, offset, val)    \
        CSR2_WRITE_4(pDev, offset, CSR2_READ_4(pDev, offset) & ~(val))


#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbTiEmacEndh */
