/* vxbXlpGmacEnd.h - header file for XLP GMAC VxBus END driver */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,16apr13,c_t  do not use & to compute desc increase when
                 desc count is not power of 2 (WIND00412484)
01a,12mar11,x_f  written
*/

#ifndef __INCvxbXlpGmacEndh
#define __INCvxbXlpGmacEndh

#ifdef __cplusplus
extern "C" {
#endif

#include <../src/hwif/h/resource/vxbXlpFmn.h>


#define XLP_GMAC16_ONLINE_MASK  0x10000
#define XLP_GMAC17_ONLINE_MASK  0x20000

#define XLP_INC_DESC(x, y)      (x) = (((x) + 1) % (y))

#define XLP_MAXFRAG             16
#define XLP_MAX_RX              96

#define XLP_NUM_PORTS           18

#define XLP_MTU                 1500
#define XLP_JUMBO_MTU           9000
#define XLP_CLSIZE              1536

#define XLP_GMAC_NAME           "xlp"
#define XLP_TIMEOUT             10000

#define XLP_NAE_TX_QID(port)    (476 + port)
#define XLP_NAE_RX_QID(port)    (1000 + port)
#define XLP_POE_RX_QID          384
#define XLP_POE_TX_QID          476

#define XLP_RX_DESC_CNT         50
#define XLP_TX_DESC_CNT         50 

#define XLP_RX_BUCKET           0 
#define XLP_TX_BUCKET           3

#define XLP_RXBMASK             (1 << XLP_RX_BUCKET)
#define XLP_TXBMASK             (1 << XLP_TX_BUCKET)

#define XLP_GMAC_MAX_CONTEXT    524   /* XLP support 524 context */
#define XLP_GMAC_MAP_WIDTH      4     /* mapped memory use 4 byte per context */

/*
 * Message are allowed to have a software code field. We currently
 * don't use this, but we initialize it for completeness.
 */

#define GMAC_MSG_CODE_RX	    0xAB
#define GMAC_MSG_CODE_TX	    0xCD

/* General Descriptor Defines */

#define	XLPA_P2D_NEOP           0
#define	XLPA_P2P                1
#define	XLPA_P2D_EOP            2
#define	XLPA_MSC                3

/* Rx Packet Descriptor Defines */

#define XLPA_RXD_BUFLEN		    0x003FFF00
#define XLPA_RXD_BUFADDR_HI	    0x000000FF
#define XLPA_RXD_BUFADDR_LO	    0xFFFFFFC0	/* 64 byte aligned */
#define XLPA_RXD_UP             0x00000020
#define XLPA_RXD_ERR            0x00000010
#define XLPA_RXD_IC             0x00000008
#define XLPA_RXLEN(x)		    (((x) & XLPA_RXD_BUFLEN) >> 8)
#define XLPA_RXADDR(x)		    ((x) & XLPA_RXD_BUFADDR_LO)
#define XLPA_RX_CONTEXT(x)		(((x) >> 22) & 0x3FF)

/* Tx Packet Descriptor Defines */

#define XLPA_TXD_EOP		    0x80000000
#define XLPA_TXD_P2P		    0x40000000
#define XLPA_TXD_RDEX		    0x20000000
#define XLPA_TXD_FBID		    0x1FC00000
#define XLPA_TXD_BUFLEN		    0x003FFF00
#define XLPA_TXD_BUFADDRHI	    0x000000FF
#define XLPA_TXD_BUFADDRLO	    0xFFFFFFFF  /* any alignment */
#define XLPA_TXLEN(x)		    (((x) << 8) & XLPA_TXD_BUFLEN)
#define XLPA_TXFBID(x)		    (((x) << 22) & XLPA_TXD_FBID)
#define XLPA_TXADDRLO(x)	    ((x) & XLPA_TXD_BUFADDRLO)

/* Tx Free Back Descriptor Defines */

#define XLPA_TXFB_TYPE_MASK		0xC0000000
#define S_XLPA_TXFB_TYPE        30 
#define XLPA_TXFB_P2D		    (0 << S_XLPA_TXFB_TYPE)
#define XLPA_TXFB_P2P		    (1 << S_XLPA_TXFB_TYPE)
#define XLPA_TXFB_P2DE		    (2 << S_XLPA_TXFB_TYPE)
#define XLPA_TXFB_RDX		    0x20000000
#define XLPA_TXFB_UNDERRUN		0x10000000
#define XLPA_TXFB_ABORT         0x08000000
#define XLPA_TXFB_TX_DONE       0x04000000
#define XLPA_TXFB_COLL_CNT      (0xF << 22))
#define XLPA_TXFB_CONTEXT(x)    (((x) >> 8) & 0x3FF)
#define XLPA_TXFB_BUFADDR_HI	0x000000FF
#define XLPA_TXFB_BUFADDR_LO	0xFFFFFFFF

/* NAE Interface Defines */

#define BLOCK_0                                 0
#define BLOCK_1                                 1
#define BLOCK_2                                 2
#define BLOCK_3                                 3
#define BLOCK_4                                 4
#define BLOCK_7                                 7
#define BLOCK_NAE                               BLOCK_7
#define BLOCK_UCORE                             8

#define IFACE_UCORE                             0x0
#define IFACE_NAE                               0x0
#define IFACE_GMAC_0                            0x0      
#define IFACE_GMAC_1                            0x1
#define IFACE_GMAC_2                            0x2
#define IFACE_GMAC_3                            0x3
#define IFACE_XGMAC                             0x4
#define IFACE_INTERLAKEN                        0x5
#define IFACE_PHY                               0xE
#define IFACE_LANE_CFG                          0xF

#define XLP_NAE_BLOCK_SIZE                      0x2000    /* 8KB */
#define XLP_NAE_IFACE_SIZE                      0x200     /* 512B */

#define XLP_NAE_OFFSET(iface)                   (((iface) & 0xF) << 9)

#define XLP_MAC_OFFSET(blk, iface)              \
    (((blk) * XLP_NAE_BLOCK_SIZE) + ((iface) * XLP_NAE_IFACE_SIZE))

#define XLP_MAC_BLOCK(port)                     (((port) & 0xFF) >> 2)
#define XLP_MAC_IFACE(port)                     ((port) & 0x3)

/* Ingress/Egress Path Registers */

#define R_NAE_RX_CONFIG                         (0x10 << 2)
#define R_NAE_TX_CONFIG                         (0x11 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_0               (0x12 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_1               (0x13 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_2               (0x14 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_3               (0x15 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_4               (0x16 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_5               (0x17 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_6               (0x18 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_7               (0x19 << 2)
#define R_NAE_RX_IF_BASE_CONFIG_8               (0x1a << 2)
#define R_NAE_RX_IF_BASE_CONFIG_9               (0x1b << 2)
#define R_NAE_RX_IFACE_VEC_VALID                (0x1c << 2)
#define R_NAE_RX_IFACE_SLOT_CAL                 (0x1d << 2)
#define R_NAE_XLP_PARSER_CONFIG                 (0x1e << 2)
#define R_NAE_PARSER_SEQ_FIFO_CFG               (0x1f << 2)
#define R_NAE_FREE_IN_FIFO_CFG                  (0x20 << 2)
#define R_NAE_RX_BUFFER_BASE_DEPTH_ADDR_REG     (0x21 << 2)
#define R_NAE_RX_BUFFER_BASE_DEPTH_REG          (0x22 << 2)
#define R_NAE_RX_UCORE_CFG                      (0x23 << 2)
#define R_NAE_RX_UCORE_CAM_MASK0_CFG            (0x24 << 2)
#define R_NAE_RX_UCORE_CAM_MASK1_CFG            (0x25 << 2)
#define R_NAE_RX_UCORE_CAM_MASK2_CFG            (0x26 << 2)
#define R_NAE_RX_UCORE_CAM_MASK3_CFG            (0x27 << 2)
#define R_NAE_FREE_IN_FIFO_UNIQ_SZ_CFG          (0x28 << 2)
#define R_NAE_CRC_POLY0_CFG                     (0x2a << 2)
#define R_NAE_CRC_POLY1_CFG                     (0x2b << 2)
#define R_NAE_FREE_SPILL0_MEM_CFG               (0x2c << 2)
#define R_NAE_FREE_SPILL1_MEM_CFG               (0x2d << 2)
#define R_NAE_FREE_FIFO_THRESHOLDS_CFG          (0x87 << 2)
#define R_NAE_FLOW_CRC16_POLY_CFG               (0x2f << 2)
#define R_NAE_DMA_TX_CREDIT_TH                  (0x29 << 2)
#define R_NAE_STG1_STG2CRDT_CMD                 (0x30 << 2)
#define R_NAE_STG1_STG2CRDT_STATUS              (0x31 << 2)
#define R_NAE_STG2_EHCRDT_CMD                   (0x32 << 2)
#define R_NAE_STG2_EHCRDT_STATUS                (0x33 << 2)
#define R_NAE_STG2_FREECRDT_CMD                 (0x34 << 2)
#define R_NAE_STG2_FREECRDT_STATUS              (0x35 << 2)
#define R_NAE_STG2_STRCRDT_CMD                  (0x36 << 2)
#define R_NAE_STG2_STRCRDT_STATUS               (0x37 << 2)
#define R_NAE_TXFIFO_IFACE_MAP_CMD              (0x38 << 2)
#define R_NAE_TXFIFO_IFACE_MAP_STATUS           (0x39 << 2)
#define R_NAE_VFBID_TO_DEST_MAP_CMD             (0x3a << 2)
#define R_NAE_STG1_PMEM_PROG                    (0x3c << 2)
#define R_NAE_STG1_PMEM_STATUS                  (0x3d << 2)
#define R_NAE_STG2_PMEM_PROG                    (0x3e << 2)
#define R_NAE_STG2_PMEM_STATUS                  (0x3f << 2)
#define R_NAE_EH_PMEM_PROG                      (0x40 << 2)
#define R_NAE_EH_PMEM_STATUS                    (0x41 << 2)
#define R_NAE_FREE_PMEM_PROG                    (0x42 << 2)
#define R_NAE_FREE_PMEM_STATUS                  (0x43 << 2)
#define R_NAE_TX_DRR_ACTVLIST_CMD               (0x44 << 2)
#define R_NAE_TX_DRR_ACTVLIST_STATUS            (0x45 << 2)
#define R_NAE_TX_IFACE_BURSTMAX_CMD             (0x46 << 2)
#define R_NAE_TX_IFACE_BURSTMAX_STATUS          (0x47 << 2)
#define R_NAE_TX_IFACE_ENBL_CMD                 (0x48 << 2)
#define R_NAE_TX_IFACE_ENBL_STATUS              (0x49 << 2)
#define R_NAE_TX_PKTLEN_PMEM_CMD                (0x4a << 2)
#define R_NAE_TX_PKTLEN_PMEM_STATUS             (0x4b << 2)
#define R_NAE_TX_SCHED_CTXTMAP_CMD0             (0x4c << 2)
#define R_NAE_TX_SCHED_CTXTMAP_CMD1             (0x4d << 2)
#define R_NAE_TX_SCHED_CTXTMAP_STATUS0          (0x4e << 2)
#define R_NAE_TX_SCHED_CTXTMAP_STATUS1          (0x4f << 2)
#define R_NAE_TX_PKT_PMEM_CMD0                  (0x50 << 2)
#define R_NAE_TX_PKT_PMEM_CMD1                  (0x51 << 2)
#define R_NAE_TX_PKT_PMEM_STATUS                (0x52 << 2)
#define R_NAE_TX_SCHED_CTRL                     (0x53 << 2)
#define R_NAE_STR_PMEM_CMD                      (0x58 << 2)
#define R_NAE_TX_IORCRDT_INIT                   (0x59 << 2)
#define R_NAE_RX_FREE_FIFO_POP                  (0x62 << 2)
#define R_NAE_FLOW_BASE_MASK_CFG                (0x80 << 2)
#define R_NAE_POE_CLASS_SETUP_CFG               (0x81 << 2)
#define R_NAE_UCORE_IFACE_MASK_CFG              (0x82 << 2)
#define R_NAE_FLOW_TABLE1_CFG                   (0x84 << 2)
#define R_NAE_IFACE_FIFO_CFG                    (0x8A << 2)
#define R_NAE_L2_TYPE_0                         (0x210 << 2)
#define R_NAE_L3_CTABLE_MASK_0                  (0x22c << 2)
#define R_NAE_L3_CTABLE_0_0                     (0x230 << 2)
#define R_NAE_L3_CTABLE_0_1                     (0x231 << 2)
#define R_NAE_L4_CTABLE_0_0                     (0x250 << 2)
#define R_NAE_L4_CTABLE_0_1                     (0x251 << 2)
#define R_NAE_NET_IF_INTR_STAT(i)               ((0x280 + (i) * 2) << 2)
#define R_NAE_NET_IF_INTR_MASK(i)               ((0x281 + (i) * 2) << 2)
#define R_NAE_NET_COMMON0_INTR_STAT             (0x2A8 << 2)
#define R_NAE_NET_COMMON0_INTR_MASK             (0x2A9 << 2)
#define R_NAE_NET_COMMON1_INTR_STAT             (0x2AA << 2)
#define R_NAE_NET_COMMON1_INTR_MASK             (0x2AB << 2)
#define R_NAE_VFBID_TO_DEST_MAP_STATUS          (0x380 << 2)

#define V_NAE_TX_IORCRDT_INIT                   0
#define V_NAE_RX_ENABLE                         0x1
#define V_NAE_TX_ENABLE                         0x1
#define V_NAE_TX_ACE                            0x2
#define V_NAE_XLP_PARSER_THRESHOLD              384
#define V_NAE_RX_CONFIG_DESC_SIZE               2048

#define V_NAE_MAX_MESSAGE_SIZE(x)               (((x) & 0x3) <<1)
#define V_RESET_MAX_MESSAGE_SIZE                (~(0x3 << 1))
#define V_NAE_FRINDESCCLSIZE(x)                 (((x)  & 0xFF) << 4)
#define V_RESET_FRINDESCCLSIZE                  (~((0xff)<< 4))
#define M_NAE_RX_STATUS_MASK(x)                 (((x) & 0x7F) << 24)
#define M_RESET_RX_STATUS_MASK                   (~((0x3F) << 24))

#define V_PARSER_THRESHOLD(x)                   ((x)  & 0x3FF)
#define V_PARSER_THRESHOLD_DIV_DESCSIZE(x)      (((x) & 0xFF) << 12)
#define V_PARSER_THRESHOLD_MOD_DESCSIZE_CL(x)   (((x) & 0xFF) << 20)
    
/* Network Interface Lane Configuration Registers */

#define R_NAE_LANE_CFG_CPLX_0_1                 (0x00 << 2)
#define R_NAE_LANE_CFG_CPLX_2_3                 (0x01 << 2)
#define R_NAE_LANE_CFG_CPLX_4                   (0x02 << 2)
#define R_NAE_NET_INTF_SOFT_RST                 (0x03 << 2)
#define R_NAE_INT_MDIO_CTRL                     (0x19 << 2)
#define R_NAE_INT_MDIO_CTRL_DATA                (0x1A << 2)
#define R_NAE_INT_MDIO_RD_STAT                  (0x1B << 2)
#define R_NAE_INT_MDIO_LINK_STAT                (0x1C << 2)
#define R_NAE_EXT_XG0_MDIO_CTRL                 (0x25 << 2)
#define R_NAE_EXT_XG1_MDIO_CTRL                 (0x29 << 2)
#define R_NAE_EXT_G0_MDIO_CTRL                  (0x1D << 2)
#define R_NAE_EXT_G1_MDIO_CTRL                  (0x21 << 2)
#define R_NAE_EXT_G0_MDIO_CTRL_DATA             (0x1E << 2)
#define R_NAE_EXT_G1_MDIO_CTRL_DATA             (0x22 << 2)
#define R_NAE_EXT_G0_MDIO_LINK_STAT             (0x20 << 2)
#define R_NAE_EXT_G1_MDIO_LINK_STAT             (0x24 << 2)
#define R_NAE_EXT_G0_MDIO_RD_STAT               (0x1F << 2)
#define R_NAE_EXT_G1_MDIO_RD_STAT               (0x23 << 2)
#define R_NAE_EXT_XG1_MDIO_CTRL                 (0x29 << 2)
#define R_NAE_EXT_XG0_MDIO_CTRL_DATA            (0x26 << 2)
#define R_NAE_EXT_XG1_MDIO_CTRL_DATA            (0x2A << 2)
#define R_NAE_EXT_XG0_MDIO_LINK_STAT            (0x28 << 2)
#define R_NAE_EXT_XG1_MDIO_LINK_STAT            (0x2C << 2)
#define R_NAE_EXT_XG0_MDIO_RD_STAT              (0x27 << 2)
#define R_NAE_EXT_XG1_MDIO_RD_STAT              (0x2B << 2)
#define R_NAE_GMAC_FC_SLOT0                     (0x2D << 2)
#define R_NAE_GMAC_FC_SLOT1                     (0x2E << 2)
#define R_NAE_GMAC_FC_SLOT2                     (0x2F << 2)
#define R_NAE_GMAC_FC_SLOT3                     (0x30 << 2)
#define R_NAE_NETIOR_MISC_CTLR1                 (0x39 << 2)
#define R_NAE_NETIOR_MISC_CTLR2                 (0x3A << 2)
#define R_NAE_NETIOR_MISC_CTLR3                 (0x3D << 2)

#define S_NAE_INT_MDIO_CTRL_ST                  0
#define S_NAE_INT_MDIO_CTRL_OP                  2
#define S_NAE_INT_MDIO_CTRL_PHYADDR             4
#define S_NAE_INT_MDIO_CTRL_DEVTYPE             9
#define S_NAE_INT_MDIO_CTRL_TA                  14
#define S_NAE_INT_MDIO_CTRL_MIIM                16
#define S_NAE_INT_MDIO_CTRL_LOAD                19
#define S_NAE_INT_MDIO_CTRL_XDIV                21
#define S_NAE_INT_MDIO_CTRL_MCDIV               28
#define S_NAE_EXT_G_MDIO_CTRL_ADDDIV            2
#define S_NAE_EXT_G_MDIO_CTRL_REGADDR           5
#define S_NAE_EXT_G_MDIO_CTRL_PHYADDR           10
#define S_NAE_EXT_G_MDIO_CTRL_RDS               18

#define M_NAE_INT_MDIO_CTRL_RST                 0x40000000
#define M_NAE_INT_MDIO_CTRL_SMP                 0x00100000
#define M_NAE_INT_MDIO_CTRL_CMD_LOAD            0x00080000
#define M_NAE_INT_MDIO_RD_STAT_MASK             0x0000FFFF
#define M_NAE_INT_MDIO_STAT_LFV                 0x00010000
#define M_NAE_INT_MDIO_STAT_SC                  0x00020000
#define M_NAE_INT_MDIO_STAT_SM                  0x00040000
#define M_NAE_INT_MDIO_STAT_MIILFS              0x00080000
#define M_NAE_INT_MDIO_STAT_MBSY                0x00100000
#define M_NAE_EXT_G_MDIO_CMD_SP                 0x00008000
#define M_NAE_EXT_G_MDIO_CMD_PSIA 		        0x00010000
#define M_NAE_EXT_G_MDIO_CMD_LCD                0x00020000
#define M_NAE_EXT_G_MDIO_CMD_RDS                0x00040000
#define M_NAE_EXT_G_MDIO_CMD_SC                 0x00080000
#define M_NAE_EXT_G_MDIO_MMRST                  0x00100000
#define M_NAE_EXT_G_MDIO_RD_STAT_MASK           0x0000FFFF
#define M_NAE_EXT_G_MDIO_STAT_LFV               0x00010000
#define M_NAE_EXT_G_MDIO_STAT_SC                0x00020000
#define M_NAE_EXT_G_MDIO_STAT_SM                0x00040000
#define M_NAE_EXT_G_MDIO_STAT_MIILFS            0x00080000
#define M_NAE_EXT_G_MDIO_STAT_MBSY              0x80000000

#define V_NAE_EXT_G_MDIO_CTRL_ADDDIV            7

#define V_NAE_LANE_CFG_UNCONNECTED              0
#define V_NAE_LANE_CFG_SGMII                    1
#define V_NAE_LANE_CFG_XAUI                     2
#define V_NAE_LANE_CFG_IL                       3

/* GMAC Registers */

#define R_GMAC_MAC_CONF1                        (0x00 << 2)
#define R_GMAC_MAC_CONF2                        (0x01 << 2)
#define R_GMAC_MAX_FRM                          (0x04 << 2)
#define R_GMAC_IO_CTRL                          (0x0E << 2)
#define R_GMAC_RX_BYTE                          (0x27 << 2)
#define R_GMAC_RX_PKT                           (0x28 << 2)
#define R_GMAC_RX_MULTICAST                     (0x2A << 2)
#define R_GMAC_RX_BROADCAST                     (0x2B << 2)
#define R_GMAC_TX_BYTE                          (0x38 << 2)
#define R_GMAC_TX_PKT                           (0x39 << 2)
#define R_GMAC_TX_MULTICAST                     (0x3A << 2)
#define R_GMAC_TX_BROADCAST                     (0x3B << 2)
#define R_GMAC_MAC_ADDR0_LO                     (0x50 << 2)
#define R_GMAC_MAC_ADDR0_HI                     (0x51 << 2)
#define R_GMAC_MAC_ADDR1_LO                     (0x52 << 2)
#define R_GMAC_MAC_ADDR1_HI                     (0x53 << 2)
#define R_GMAC_MAC_ADDR2_LO                     (0x54 << 2)
#define R_GMAC_MAC_ADDR2_HI                     (0x55 << 2)
#define R_GMAC_MAC_ADDR3_LO                     (0x56 << 2)
#define R_GMAC_MAC_ADDR3_HI                     (0x57 << 2)
#define R_GMAC_MAC_ADDR_MASK0_LO                (0X58 << 2)
#define R_GMAC_MAC_ADDR_MASK0_HI                (0x59 << 2)
#define R_GMAC_MAC_ADDR_MASK1_LO                (0X5A << 2)
#define R_GMAC_MAC_ADDR_MASK1_HI                (0x5B << 2)
#define R_GMAC_MAC_FILTER_CONFIG                (0x5C << 2)
#define R_GMAC_MAC_HASH_TABLE0                  (0x60 << 2)
#define R_GMAC_NETIOR_GMAC_CTRL1                (0x7F << 2)
#define R_GMAC_NETIOR_GMAC_CTRL3                (0x7C << 2)

#define M_GMAC_NETIOR_GMAC_CTRL1_GSR            (0x1 << 11)

#define V_GMAC_MAC_HASH_SIZE                    16

/* Network Interface Ctrl Registers */

#define V_GMAC_CTRL1_SOFTRESET(x)               ((x) << 11)
#define V_GMAC_CTRL1_STATS_EN(x)                ((x) << 16)
#define V_GMAC_CTRL1_TX_EN                      (1 << 2)
#define V_GMAC_CTRL1_SPEED(x)                   ((x) & 0x3)
#define V_GMAC_CTRL1_SPEED_125MHZ	            0x00000002
#define V_GMAC_CTRL1_SPEED_25MHZ	            0x00000001
#define V_GMAC_CTRL1_SPEED_2_5MHZ	            0x00000000

#define V_GMAC_MAC_CONF1_SOFTRESET              (1UL << 31)
#define V_GMAC_MAC_CONF1_LOOP_BACK(x)           ((x) << 8)
#define V_GMAC_MAC_CONF1_RX_ENABLE(x)           ((x) << 2)
#define V_GMAC_MAC_CONF1_TX_ENABLE(x)           (x)

#define V_GMAC_MAC_CONF2_PREMBL_LEN(x)          (((x) & 0xf) << 12)
#define V_GMAC_MAC_CONF2_IFMODE(x)              (((x) & 0x3) << 8)
#define V_GMAC_MAC_CONF2_LENCHK(x)              ((((x) & 0x1)) << 4)
#define V_GMAC_MAC_CONF2_PADCRCEN               (0x1 << 2)
#define V_GMAC_MAC_CONF2_PADCRC                 (0x1 << 1)
#define V_GMAC_MAC_CONF2_FDX	                (0x1 << 0)
#define V_GMAC_MAC_CONF2_IFMODE_NIBBLE	        0x00000100	/* 10/100 */
#define V_GMAC_MAC_CONF2_IFMODE_BYTE	        0x00000200	/* gigabit */

#define V_GMAC_IOCTRL_TBIMODE	                0x08000000	/* TBI mode enable */
#define V_GMAC_IOCTRL_SGMII_SPD_1	            0x04000000	/* Gbps Select */
#define V_GMAC_IOCTRL_SGMII_SPD_0	            0x02000000	/* 100Mbps Select */
#define V_GMAC_IOCTRL_LHDMODE	                0x02000000	/* 10/100 HDX MII enable */
#define V_GMAC_IOCTRL_PHYMODE	                0x01000000	/* PHY mode */
#define V_GMAC_IOCTRL_EN_JAB_PROT	            0x00000001	/* Enable jabber protection */

#define V_GMAC_FILTER_CONFIG_BREN               0x00000400  /* All Broadcast Enable */
#define V_GMAC_FILTER_CONFIG_PFE                0x00000200  /* ALL Pause frame Enable */
#define V_GMAC_FILTER_CONFIG_AME                0x00000100  /* All multicast */
#define V_GMAC_FILTER_CONFIG_AUE                0x00000080  /* All Unicast Enable */
#define V_GMAC_FILTER_CONFIG_HME                0x00000040  /* Hash Multicast Enable */
#define V_GMAC_FILTER_CONFIG_HUE                0x00000020  /* Hash Unicast Enable */
#define V_GMAC_FILTER_CONFIG_AMD                0x00000010  /* Address Match Discard */
#define V_GMAC_FILTER_CONFIG_MA3V               0x00000008  /* MAC Address 3 Valid */
#define V_GMAC_FILTER_CONFIG_MA2V               0x00000004  /* MAC Address 2 Valid */
#define V_GMAC_FILTER_CONFIG_MA1V               0x00000002  /* MAC Address 1 Valid */
#define V_GMAC_FILTER_CONFIG_MA0V               0x00000001  /* MAC Address 0 Valid */


#define V_NAE_TX_CONFIG_TXINITIORCR(x)          (((x) & 0x7FFFF) << 8)

/* Network Interface PHY/PMA Registers */

#define R_NAE_PHY_LANE_0_STATUS                 (0x00 << 2)
#define R_NAE_PHY_LANE_1_STATUS                 (0x01 << 2)
#define R_NAE_PHY_LANE_2_STATUS                 (0x02 << 2)
#define R_NAE_PHY_LANE_3_STATUS                 (0x03 << 2)
#define R_NAE_PHY_LANE_0_CTRL                   (0x04 << 2)
#define R_NAE_PHY_LANE_1_CTRL                   (0x05 << 2)
#define R_NAE_PHY_LANE_2_CTRL                   (0x06 << 2)
#define R_NAE_PHY_LANE_3_CTRL                   (0x07 << 2)
#define V_NAE_PHY_LANE_STAT_PCR                 0x00000800
#define S_NAE_PHY_LANE_CTRL_DATA_POS            0
#define S_NAE_PHY_LANE_CTRL_ADDR_POS            8
#define V_NAE_PHY_LANE_CTRL_CMD_READ            0x00010000
#define V_NAE_PHY_LANE_CTRL_CMD_WRITE           0x00000000
#define V_NAE_PHY_LANE_CTRL_CMD_START           0x00020000
#define M_NAE_PHY_LANE_CTRL_CMD_PENDING         0x00040000
#define S_NAE_PHY_LANE_CTRL_PHYMODE_POS         25
#define V_NAE_PHY_LANE_CTRL_PWRDOWN             0x20000000
#define V_NAE_PHY_LANE_CTRL_RST                 0x40000000

#define V_NAE_UCORES_MAX                        16
#define V_NAE_UCORE_MASK                        0xFFFF
#define V_NAE_UCORE_CODE_SIZE                   (4 << 10)
#define V_NAE_UCORE_SIZE                        58 /* Should be even number */
#define V_NAE_UCORE_RESET(x)                    ((x & 0xffff) << 8)
#define V_NAE_UCORE_IRAM                        (0x1 << 7) /* Instruction RAM access */

#define UCORE_SPRAY_CONFIG(port, mask, cmd) \
            (((cmd & 0x1) << 31) | ((mask & 0xffff) << 8) | (port & 0x1f))

typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define xlpEndPoolTupleFree(x) netMblkClChainFree(x)
#define xlpEndPoolTupleGet(x)      \
        netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

typedef struct xlpa_rx_desc
    {
    volatile UINT32	xlpa_sts;
    volatile UINT32	xlpa_bufaddr_lo;
    } XLPA_RX_DESC;

typedef struct xlpa_tx_desc
    {
    volatile UINT32	xlpa_cmd;
    volatile UINT32	xlpa_bufaddr_lo;
    } XLPA_TX_DESC;

typedef struct xlpa_txfb_desc
    {
    volatile UINT32	xlpa_sts;
    volatile UINT32	xlpa_bufaddr_lo;
    } XLPA_TXFB_DESC;

/*
 * We use TX descriptor arrays, and they must be cache aligned. We
 * combine 8 of them together. This should be enough to handle
 * most scatter/gather combinations without making the descriptor
 * clusters too big.
 */

typedef struct gmac_tx_desc {
    XLPA_TX_DESC xlp_frag[XLP_MAXFRAG];
} GMAC_TX_DESC;

/* Private adapter context structure. */

typedef struct xlp_drv_ctrl
    {
    END_OBJ          xlpEndObj;
    VXB_DEVICE_ID    xlpEndDev;
    VXB_DEVICE_ID    xlpPrivDev;
    int              xlpUnit;
    void *           xlpBar;
    void *           xlpHandle;

    SEM_ID           xlpDevSem;

    int			     xlpRxQueueId;
    int			     xlpTxQueueId;

    M_BLK_ID         xlpTxMblk[XLP_TX_DESC_CNT];
    GMAC_TX_DESC *   xlpTxDescMem;
    void *           xlpPool;

    JOB_QUEUE_ID     xlpJobQueue;

    int              xlpTxProd;
    int              xlpTxCons;
    int              xlpTxFree;
    volatile BOOL    xlpTxStall;

    BOOL             xlpEndPolling;
    M_BLK_ID         xlpEndPollBuf;

    UINT8            xlpAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES xlpEndCaps;

    END_IFDRVCONF    xlpEndStatsConf;
    END_IFCOUNTERS   xlpEndStatsCounters;

    void *           xlpEndMuxDevCookie;
    
    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *  xlpMediaList;
    END_ERR          xlpLastError;
    UINT32           xlpCurMedia;
    UINT32           xlpCurStatus;
    VXB_DEVICE_ID    xlpMiiBus;
    VXB_DEVICE_ID    xlpMiiDev;
    FUNCPTR          xlpMiiPhyRead;
    FUNCPTR          xlpMiiPhyWrite;
    UINT32           xlpMiiPhyAddr;
    UINT32           miiChannel;
    
    /* End MII/ifmedia required fields. */
    
    int              xlpMaxMtu;

    VXB_DEVICE_ID       xlpFmnInst;
    VXB_XLP_FMN_FUNC *  xlpFmnFuncs;
    } XLP_GMAC_DRV_CTRL;

#define XLP_BAR(p)      ((XLP_GMAC_DRV_CTRL *)(p)->pDrvCtrl)->xlpBar
#define XLP_HANDLE(p)   ((XLP_GMAC_DRV_CTRL *)(p)->pDrvCtrl)->xlpHandle

#define CSR_WRITE_4(pDev, block, iface, reg, data)                        \
        vxbWrite32(XLP_HANDLE(pDev),                                      \
                   (UINT32 *)((char *)XLP_BAR(pDev) +                     \
                   (XLP_MAC_OFFSET(block, iface) + reg)), data)

#define CSR_READ_4(pDev, block, iface, reg)                               \
        vxbRead32(XLP_HANDLE(pDev),                                       \
                  (UINT32 *)((char *)XLP_BAR(pDev) +                      \
                  (XLP_MAC_OFFSET(block, iface) + reg)))

#define CSR_SETBIT_4(pDev, block, iface, reg, data)                       \
        CSR_WRITE_4(pDev, block, iface, reg,                              \
                    CSR_READ_4(pDev, block, iface, reg) | (data))

#define CSR_CLRBIT_4(pDev, block, iface, reg, data)                       \
        CSR_WRITE_4(pDev, block, iface, reg,                              \
                    CSR_READ_4(pDev, block, iface, reg) & ~(data))

#define NAE_TX_DESC(type, rdex, fbid, len, addr)                          \
        (((UINT64)(type & 0x3) << 62) | ((UINT64)(rdex & 0x1) << 61) |    \
         ((UINT64)(fbid & 0x7f) << 54) | ((UINT64)(len & 0x3fff) << 40) | \
         addr)

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbXlpGmacEndh */
