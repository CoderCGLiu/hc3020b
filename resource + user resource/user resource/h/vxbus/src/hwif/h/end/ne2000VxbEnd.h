/* ne2000VxbEnd.h - header file for NE2000 compatible VxBus END driver */

/*
 * Copyright (c) 2007-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,25jul08,wap  Correct PCI ID for Winbond W89C940 chip
01c,18jul07,wap  Convert to new register access API
01b,26feb07,wap  Add eneLastError
01a,11jan07,wap  written
*/

#ifndef __INCne2000VxbEndh
#define __INCne2000VxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void eneRegister (void);

#ifndef BSP_VERSION

#define ENE_VENDORID_REALTEK	0x10ec
#define ENE_DEVICEID_RTL8029	0x8029

#define ENE_VENDORID_VIA	0x1106
#define ENE_DEVICEID_VT86C926	0x0926

#define ENE_VENDORID_WINBOND	0x1050
#define ENE_DEVICEID_W89C940	0x0940

/*
 * The NE2000 register space is broken up into pages. There are
 * three available pages, selectable via two bits in the command register,
 * which is always mapped in the same place in all three pages.
 * Page 0 contains the main runtime registers. Page 1 contains the
 * physical address and multicast hash table registers. Page 2
 * contains diagnostic registers which aren't needed in normal operation.
 * Page 3 is invalid and should never be modified.
 */

/* Page 0 registers, read */

#define ENE_CR		0x00	/* Command register */
#define ENE_CLDA0	0x01	/* Current local DMA address 0 */
#define ENE_CLDA1	0x02	/* Current local DMA address 1 */
#define ENE_BNRY	0x03	/* Boundary pointer */
#define ENE_TSR		0x04	/* TX status register */
#define ENE_NCR		0x05	/* Collision count */
#define ENE_FIFO	0x06	/* FIFO */
#define ENE_ISR		0x07	/* Interrupt status register */
#define ENE_CRDA0	0x08	/* Current remode DMA address 0 */
#define ENE_CRDA1	0x09	/* Current remode DMA address 1 */
#define ENE_RSVD0	0x0A
#define ENE_RSVD1	0x0B
#define ENE_RSR		0x0C	/* RX status register */
#define ENE_FRAC	0x0D	/* Frame alignment error counter */
#define ENE_CRCC	0x0E	/* CRC error counter */
#define ENE_MISC	0x0F	/* Missed packet counter */

/* Page 0 registers, write */

#define ENE_PSTART	0x01	/* Page start */
#define ENE_PSTOP	0x02	/* Page stop */
/* #define ENE_BNRY	0x03	   Boundary pointer */
#define ENE_TPSR	0x04	/* TX page start address */
#define ENE_TBCR0	0x05	/* TX byte count 0 */
#define ENE_TBCR1	0x06	/* TX byte count 1 */
/* #define ENE_ISR	0x07	   Interrupt status register */
#define ENE_RSAR0	0x08	/* Remote start address 0 */
#define ENE_RSAR1	0x09	/* Remote start address 1 */
#define ENE_RBCR0	0x0A	/* Remote byte count 0 */
#define ENE_RBCR1	0x0B	/* Remote byte count 1 */
#define ENE_RCR		0x0C	/* RX configuration register */
#define ENE_TCR		0x0D	/* TX configuration register */
#define ENE_DCR		0x0E	/* data configuration register */
#define ENE_IMR		0x0F	/* Interrupt mask register */


/* Command register */

#define ENE_CR_STOP	0x01	/* Software reset */
#define ENE_CR_START	0x02	/* Start ST-NIC */
#define ENE_CR_TXP	0x04	/* Initiate packet transmission */
#define ENE_CR_RDMA_CMD	0x38	/* RDMA command */
#define ENE_CR_PAGESEL	0xC0	/* Page select */

#define ENE_RDMA_READ	0x08	/* Remote read */
#define ENE_RDMA_WRITE	0x10	/* Remote write */
#define ENE_RDMA_SEND	0x18	/* Send packet */
#define ENE_RDMA_ABORT	0x20	/* Abort/complete RDMA */

#define ENE_PAGESEL_0	0x00	/* Select page 0 */
#define ENE_PAGESEL_1	0x40	/* Select page 1 */
#define ENE_PAGESEL_2	0x80	/* Select page 2 */
#define ENE_PAGESEL_3	0xC0	/* reserved */


/* ISR register */

#define ENE_ISR_PRX	0x01	/* Packet received */
#define ENE_ISR_PTX	0x02	/* Packet sent */
#define ENE_ISR_RXE	0x04	/* Receive error */
#define ENE_ISR_TXE	0x08	/* Transmit error */
#define ENE_ISR_OVW	0x10	/* RX overrun */
#define ENE_ISR_CNT	0x20	/* Counter overflow */
#define ENE_ISR_RDC	0x40	/* Remote DMA complete */
#define ENE_ISR_RST	0x80	/* Reset complete */


/* IMR register */

#define ENE_IMR_PRX	0x01	/* Packet received */
#define ENE_IMR_PTX	0x02	/* Packet sent */
#define ENE_IMR_RXE	0x04	/* Receive error */
#define ENE_IMR_TXE	0x08	/* Transmit error */
#define ENE_IMR_OVW	0x10	/* RX overrun */
#define ENE_IMR_CNT	0x20	/* Counter overflow */
#define ENE_IMR_RDC	0x40	/* Remote DMA complete */
#define ENE_IMR_RST	0x80	/* Reset complete */


/* Data configuration register */

#define ENE_DCR_WTS	0x01	/* 0 = byte transfers, 1 = word transfers */
#define ENE_DCR_BOS	0x02	/* 0 = little endian, 1 = big endian */
#define ENE_DCR_LAS	0x04	/* 0 = dual 16 bit DMA, 1 = single 32 bit DMA*/
#define ENE_DCR_LS	0x08	/* 0 = loopback, 1 = normal */
#define ENE_DCR_ARM	0x10	/* autoinit remote DMA */
#define ENE_DCR_FT	0x60	/* FIFO threshold select */

#define ENE_FIFOTHR_1WORD	0x00
#define ENE_FIFOTHR_2WORDS	0x20
#define ENE_FIFOTHR_4WORDS	0x40
#define ENE_FIFOTHR_6WORDS	0x60


/* TX configuration register */

#define ENE_TCR_CRC	0x01	/* 1 = inhibit CRC generation */
#define ENE_TCR_LB	0x06	/* loopback control */
#define ENE_TCR_ATD	0x08	/* Auto transmit disable (flow control) */
#define ENE_TCR_OFST	0x10	/* Collision offset enable */

#define ENE_LOOP_OFF	0x00	/* Normal operation, loopback off */
#define ENE_LOOP_NIC	0x02	/* NIC/MAC loopback */
#define ENE_LOOP_ENDEC	0x04	/* ENDEC loopback */
#define ENE_LOOP_EXT	0x06	/* External loopback */


/* TX status register */

#define ENE_TSR_PTX	0x01	/* Frame transmitted */
#define ENE_TSR_COL	0x04	/* Collision detected */
#define ENE_TSR_ABT	0x08	/* Abort due to excess collisions */
#define ENE_TSR_CRS	0x10	/* Carrier sense lost */
#define ENE_TSR_FU	0x20	/* FIFO underrun */
#define ENE_TSR_CDH	0x40	/* CD heartbeat failure */
#define ENE_TSR_OWC	0x80	/* Out of window (late) collision */


/* RX configuration register */

#define ENE_RCR_SEP	0x01	/* Save bad frames */
#define ENE_RCR_AR	0x02	/* Accept runt frames */
#define ENE_RCR_AB	0x04	/* Accept broadcast frames */
#define ENE_RCR_AM	0x08	/* Accept multicast frames */
#define ENE_RCR_PRO	0x10	/* Accept all unicasts */
#define ENE_RCR_MON	0x20	/* Monitor mode */


/* RX status register */

#define ENE_RSR_PRX	0x01	/* Frame received */
#define ENE_RSR_CRC	0x02	/* CRC error */
#define ENE_RSR_FAE	0x04	/* Frame alignment error */
#define ENE_RSR_FO	0x08	/* FIFO overrun */
#define ENE_RSR_MPA	0x10	/* Missed frame */
#define ENE_RSR_PHY	0x20	/* 0 = unicast, 1 = multicast */
#define ENE_RSR_DIS	0x40	/* Receiver disabled (in monitor mode) */
#define ENE_RSR_DFR	0x80	/* Deferring, jabber */


/* Page 1 registers, read/write */

#define ENE_PAR0	0x01	/* Station address 0 */
#define ENE_PAR1	0x02	/* Station address 1 */
#define ENE_PAR2	0x03	/* Station address 2 */
#define ENE_PAR3	0x04	/* Station address 3 */
#define ENE_PAR4	0x05	/* Station address 4 */
#define ENE_PAR5	0x06	/* Station address 5 */
#define ENE_CURR	0x07	/* Current page */
#define ENE_MAR0	0x08	/* Multicast hash table 0 */
#define ENE_MAR1	0x09	/* Multicast hash table 1 */
#define ENE_MAR2	0x0A	/* Multicast hash table 2 */
#define ENE_MAR3	0x0B	/* Multicast hash table 3 */
#define ENE_MAR4	0x0C	/* Multicast hash table 4 */
#define ENE_MAR5	0x0D	/* Multicast hash table 5 */
#define ENE_MAR6	0x0E	/* Multicast hash table 6 */
#define ENE_MAR7	0x0F	/* Multicast hash table 7 */

/* NE2000 data port offset */

#define ENE_IOPORT	0x10

/* NE2000 reset port */

#define ENE_RESET	0x1F

/*
 * Frames in the RX DMA buffer will have a special header prepended
 * to them which tells us the RX status and the frame size, as well
 * as the next page pointer.
 */

typedef struct rx_hdr
    {
    UINT8		rxSts;		/* status of packet */
    UINT8		nextPage;	/* page next pkt starts at */
    UINT16		len;		/* frame length */
    } ENE_RX_HDR;


#define ENE_CLSIZE	1536
#define ENE_NAME	"ene"
#define ENE_TIMEOUT	10000
#define ENE_RXINTRS	(ENE_IMR_PRX|ENE_IMR_RXE|ENE_ISR_OVW)
#define ENE_TXINTRS	(ENE_IMR_PTX|ENE_IMR_TXE)
#define ENE_INTRS	(ENE_RXINTRS|ENE_TXINTRS)

#define ENE_ETHER_ALIGN	2

#define ENE_TX_CNT	2
#define ENE_TX_INC(x, y)	(x) = (((x) + 1) % y)

#define ENE_CONFIG_PAGE	0x00    /* where the Ethernet address is */
#define ENE_EADDR_LOC	0x00    /* location within config. page */
#define ENE_TXPAGES	0x06
#define ENE_TXPAGE	0x40
#define ENE_MAXPAGES	0x80
#define ENE_MAXPAGES8	0x60
#define ENE_MEMSIZE	32768
#define ENE_MOFF(x)	((x) << 8)
#define ENE_MAX_RX	16

/*
 * Private adapter context structure.
 */

typedef struct ene_drv_ctrl
    {
    END_OBJ		eneEndObj;
    VXB_DEVICE_ID	eneDev;
    void *		eneBar;
    void *		eneHandle;
    void *		eneMuxDevCookie;

    int                 eneMaxPages;
    int                 eneByteAccess;

    JOB_QUEUE_ID	eneJobQueue;
    QJOB		eneIntJob;
    volatile BOOL	eneIntPending;

    QJOB		eneRxJob;
    volatile BOOL	eneRxPending;

    QJOB		eneTxJob;
    volatile BOOL	eneTxPending;

    int			eneTxFree;
    BOOL		eneTxStall;
    int			eneTxProd;
    int			eneTxCons;

    M_BLK_ID		eneTxMblk[ENE_TX_CNT];

    int			eneRxPage;
    BOOL		eneRxRecover;

    BOOL		enePolling;
    M_BLK_ID		enePollBuf;
    UINT8		eneIntMask;

    UINT8		eneAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	eneCaps;

    int			eneRegWidth;
    int			eneDelay;

    END_ERR		eneLastError;
    } ENE_DRV_CTRL;

#define ENE_BAR(p)   ((ENE_DRV_CTRL *)(p)->pDrvCtrl)->eneBar
#define ENE_HANDLE(p)   ((ENE_DRV_CTRL *)(p)->pDrvCtrl)->eneHandle

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (ENE_HANDLE(pDev), (UINT16 *)((char *)ENE_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (ENE_HANDLE(pDev),                             \
        (UINT16 *)((char *)ENE_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (ENE_HANDLE(pDev), (UINT8 *)((char *)ENE_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (ENE_HANDLE(pDev),                              \
        (UINT8 *)((char *)ENE_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCne2000VxbEndh */
