/* sbeVxbEnd.h - header file for sbe VxBus END driver */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,26sep07,h_k  converted to new reg access methods.
01d,03feb07,wap  Make this driver SMP safe
01c,07dec06,wap  Add jumbo frame support
01b,21aug06,wap  Harden polled mode support
01b,22dec06,wap  Add SMP safety
01b,16nov06,wap  Merge in updates from development branch
01a,28jun06,wap  written
*/

#ifndef __INCsbeVxbEndh
#define __INCsbeVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void sbeRegister (void);

#ifndef BSP_VERSION

#define	MAC_FC_DISABLED 	0
#define MAC_FC_FRAME		1
#define MAC_FC_COLLISION 	2
#define MAC_FC_CARRIER		3

#ifndef END_JUMBO_CLSIZE
#define END_JUMBO_CLSIZE	9056
#endif

#define SBE_MTU         1500
#define SBE_JUMBO_MTU   9000
#define SBE_CLSIZE	1536
#define SBE_NAME	"sbe"
#define SBE_TIMEOUT	10000
#define SBE_MAXFRAG	16
#define SBE_MAX_RX	16
#define SBE_RXINTRS	(M_MAC_INT_CHANNEL << S_MAC_RX_CH0)
#define SBE_TXINTRS	(M_MAC_INT_CHANNEL << S_MAC_TX_CH0)
#define SBE_INTRS	(SBE_RXINTRS|SBE_TXINTRS)
#define SBE_LINKINTRS	0
#define SBE_ETHER_ALIGN	2

#define SBE_RX_DESC_CNT	64
#define SBE_TX_DESC_CNT	64

#define SBE_INC_DESC(x, y)	(x) = (((x) + 1) % y)

#define CACHELINESIZE   32
#define NUMCACHEBLKS(x) (((x) + (CACHELINESIZE - 1)) / CACHELINESIZE)

#define SBE_RXCHAN(chan,reg)						\
	(R_MAC_DMA_CHANNELS + ((chan) * MAC_DMA_CHANNEL_SPACING) + reg)

#define SBE_TXCHAN(chan,reg)						\
	(R_MAC_DMA_CHANNELS + ((chan) * MAC_DMA_CHANNEL_SPACING) +	\
	MAC_DMA_TXRX_SPACING + reg)

typedef struct ethDmaDscr
    {
    UINT64  dscr_a;     /* DMA descriptor first doubleword */
    UINT64  dscr_b;     /* DMA descriptor second doubleword */
    } SBE_DESC;

/*
 * Private adapter context structure.
 */

typedef struct sbe_drv_ctrl
    {
    END_OBJ		sbeEndObj;
    VXB_DEVICE_ID	sbeDev;
    void		*sbeHandle;
    void		*sbeMuxDevCookie;

    JOB_QUEUE_ID	sbeJobQueue;
    QJOB		sbeIntJob;
    atomicVal_t		sbeIntPending;

    QJOB		sbeRxJob;
    atomicVal_t		sbeRxPending;

    QJOB		sbeTxJob;
    atomicVal_t		sbeTxPending;
    UINT8		sbeTxCur;
    UINT8		sbeTxLast;
    volatile BOOL	sbeTxStall;

    BOOL		sbePolling;
    M_BLK_ID		sbePollBuf;
    UINT64		sbeIntMask;
    UINT64		sbeIntrs;

    UINT8		sbeAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	sbeCaps;

    END_IFDRVCONF	sbeEndStatsConf;
    END_IFCOUNTERS	sbeEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*sbeMediaList;
    END_ERR		sbeLastError;
    UINT32		sbeCurMedia;
    UINT32		sbeCurStatus;
    VXB_DEVICE_ID	sbeMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	sbeParentTag;

    VXB_DMA_TAG_ID	sbeRxDescTag;
    VXB_DMA_MAP_ID	sbeRxDescMap;
    SBE_DESC		*sbeRxDescMem;

    VXB_DMA_TAG_ID	sbeTxDescTag;
    VXB_DMA_MAP_ID	sbeTxDescMap;
    SBE_DESC		*sbeTxDescMem;

    VXB_DMA_TAG_ID	sbeMblkTag;

    VXB_DMA_MAP_ID	sbeRxMblkMap[SBE_RX_DESC_CNT];
    VXB_DMA_MAP_ID	sbeTxMblkMap[SBE_TX_DESC_CNT];

    M_BLK_ID		sbeTxMblk[SBE_RX_DESC_CNT];
    M_BLK_ID		sbeRxMblk[SBE_TX_DESC_CNT];

    UINT32		sbeTxProd;
    UINT32		sbeTxCons;
    UINT32		sbeTxFree;
    UINT32		sbeRxIdx;

    UINT32		sbeInDiscards;
    UINT32		sbeInMcasts;
    UINT32		sbeInBcasts;
    UINT32		sbeInUcasts;
    UINT32		sbeInOctets;
    UINT32		sbeOutMcasts;
    UINT32		sbeOutBcasts;

    SEM_ID		sbeDevSem;

    int			sbeMaxMtu;
    BOOL		sbeCksumEnable;
    BOOL		sbeBrokenPass1;
    FUNCPTR		sbeEncapFunc;
    UINT32		sbeTxThresh;
    } SBE_DRV_CTRL;

#define SBE_BAR(p)	(p)->pRegBase[0]
#define SBE_HANDLE(p)	((SBE_DRV_CTRL *)(p)->pDrvCtrl)->sbeHandle

LOCAL UINT64 csr_read (VXB_DEVICE_ID, UINT64);
LOCAL void csr_write (VXB_DEVICE_ID, UINT64, UINT64);

LOCAL UINT64 csr_read
    ( 
    VXB_DEVICE_ID pDev,
    UINT64 addr
    )
    {
    return (vxbRead64 (SBE_HANDLE(pDev),
		       (UINT64 *)((char *)SBE_BAR(pDev) + addr)));
    }

LOCAL void csr_write
    (
    VXB_DEVICE_ID pDev,
    UINT64 addr,
    UINT64 data
    )
    {
    vxbWrite64 (SBE_HANDLE(pDev),
		(UINT64 *)((char *)SBE_BAR(pDev) + addr), data);
    }

#define CSR_READ csr_read
#define CSR_WRITE csr_write

#define CSR_SETBIT(pDev, offset, val)          \
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) | (val))

#define CSR_CLRBIT(pDev, offset, val)          \
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCsbeVxbEndh */
