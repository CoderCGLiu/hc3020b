/* vxbComcertoGemacEnd.h - Mindspeed Comcerto 100 GEMAC VxBus END driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,11jun08,b_m  written.
*/

#ifndef __INCvxbComcertoGemacEndh
#define __INCvxbComcertoGemacEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void davinciEmacRegister (void);

#define GEMAC_MEDIA_MII         0
#define GEMAC_MEDIA_GMII        1
#define GEMAC_MEDIA_RMII        2
#define GEMAC_MEDIA_RGMII       3

#ifndef BSP_VERSION

/* GEMAC driver generic defines */

#define GEMAC_NAME              "gemac"
#define GEMAC_MTU               1500
#define GEMAC_JUMBO_MTU         9000
#define GEMAC_CLSIZE            1536
#define GEMAC_TIMEOUT           10000
#define GEMAC_TUPLE_CNT         384
#define GEMAC_ADJ(x)            (x)->m_data += 2

/* Comcerto AHB clock frequency */

#define COMCERTO_AHB_20MHZ      20000000    /* AHB upto 20MHz */
#define COMCERTO_AHB_40MHZ      40000000    /* AHB upto 40MHz */
#define COMCERTO_AHB_80MHZ      80000000    /* AHB upto 80MHz */
#define COMCERTO_AHB_120MHZ     120000000   /* AHB upto 120MHz */
#define COMCERTO_AHB_160MHZ     160000000   /* AHB upto 160MHz */
#define COMCERTO_AHB_240MHZ     240000000   /* AHB upto 240MHz */
#define COMCERTO_AHB_320MHZ     320000000   /* AHB upto 320MHz */
#define COMCERTO_AHB_540MHZ     540000000   /* AHB upto 540MHz */

#define COMCERTO_AHB_NUM        8

/* GEMAC number of modules */

#define GEMAC_MOD_NUM           2

/* GEMAC register offsets (from regBase[0]) */

/* GEM external FIFO interface registers */

#define HCSM_FIFO_CTRL          0x0000  /* FIFO control register */
#define HCSM_FIFO_STATUS        0x0004  /* FIFO interrupt status register */
#define HCSM_RX_FIFO_DEPTH      0x0014  /* RX FIFO depth */
#define HCSM_TX_FIFO_DEPTH      0x0024  /* TX FIFO depth */
#define HCSM_TX_FIFO_HIGH       0x0028  /* TX FIFO high threshold */
#define HCSM_TX_FIFO_LOW        0x002C  /* TX FIFO low threshold */

/* HICORE packet FIFO registers */

#define GEM_FIFO_CTRL           0xD000  /* FIFO control register */
#define GEM_TX_FIFO_HIGH        0xD024  /* TX FIFO high threshold */

/* GMAC registers */

#define GMAC_NET_CTRL           0xE000  /* network control register */
#define GMAC_NET_CFG            0xE004  /* network config register */
#define GMAC_NET_STATUS         0xE008  /* network status register */
#define GMAC_TX_STATUS          0xE014  /* TX status register */
#define GMAC_RX_STATUS          0xE020  /* RX status register */
#define GMAC_INT_STATUS         0xE024  /* interrupt status register */
#define GMAC_INT_EN             0xE028  /* interrupt enable register */
#define GMAC_INT_DIS            0xE02C  /* interrupt disable register */
#define GMAC_INT_MASK           0xE030  /* interrupt mask register */
#define GMAC_PHY_MAINT          0xE034  /* PHY maintenance register */
#define GMAC_RX_PAUSE_NUM       0xE038  /* RX pause number register */
#define GMAC_TX_PAUSE_NUM       0xE03C  /* TX pause number register */
#define GMAC_HASH_L             0xE080  /* hash [31:0] */
#define GMAC_HASH_H             0xE084  /* hash [63:32] */
#define GMAC_SPE_ADDR1_L        0xE088  /* special address 1 [31:0] */
#define GMAC_SPE_ADDR1_H        0xE08C  /* special address 1 [63:32] */
#define GMAC_SPE_ADDR2_L        0xE090  /* special address 2 [31:0] */
#define GMAC_SPE_ADDR2_H        0xE094  /* special address 2 [63:32] */
#define GMAC_SPE_ADDR3_L        0xE098  /* special address 3 [31:0] */
#define GMAC_SPE_ADDR3_H        0xE09C  /* special address 3 [63:32] */
#define GMAC_SPE_ADDR4_L        0xE0A0  /* special address 4 [31:0] */
#define GMAC_SPE_ADDR4_H        0xE0A4  /* special address 4 [63:32] */
#define GMAC_TYPEID_MATCH1      0xE0A8  /* type ID match 1 register */
#define GMAC_TYPEID_MATCH2      0xE0AC  /* type ID match 2 register */
#define GMAC_TYPEID_MATCH3      0xE0B0  /* type ID match 3 register */
#define GMAC_TYPEID_MATCH4      0xE0B4  /* type ID match 4 register */
#define GMAC_IPG_STRETCH        0xE0BC  /* IPG stretch register */
#define GMAC_MODULE_ID          0xE0FC  /* module ID register */

/* statistics registers */

#define GMAC_TX_OCTETS_L        0xE100  /* TX bytes [31:0] */
#define GMAC_TX_OCTETS_H        0xE104  /* TX bytes [47:32] */
#define GMAC_TX_FRAMES          0xE108  /* TX frames */
#define GMAC_TX_BCAST_FRAMES    0xE10C  /* TX broadcast frames */
#define GMAC_TX_MCAST_FRAMES    0xE110  /* TX multicast frames */
#define GMAC_TX_PAUSE_FRAMES    0xE114  /* TX pause frames */
#define GMAC_TX_FRAME64         0xE118  /* TX 64 bytes frames */
#define GMAC_TX_FRAME65T127     0xE11C  /* TX 65-127 bytes frames */
#define GMAC_TX_FRAME128T255    0xE120  /* TX 128-255 bytes frames */
#define GMAC_TX_FRAME256T511    0xE124  /* TX 256-511 bytes frames */
#define GMAC_TX_FRAME512T1023   0xE128  /* TX 512-1023 bytes frames */
#define GMAC_TX_FRAME1024T1518  0xE12C  /* TX 1024-1518 bytes frames */
#define GMAC_TX_FRAME1519TUP    0xE130  /* TX 1519+ bytes frames */
#define GMAC_TX_UNDERRUN        0xE134  /* TX underrun */
#define GMAC_TX_SINGLE_COLL     0xE138  /* TX single collisions frames */
#define GMAC_TX_MULTI_COLL      0xE13C  /* TX multiple collisions frames */
#define GMAC_TX_EXCESSIVE_COLL  0xE140  /* TX excessive collisions frames */
#define GMAC_TX_LATE_COLL       0xE144  /* TX late collisions frames */
#define GMAC_TX_DEFERRED        0xE148  /* TX deferred frames */
#define GMAC_TX_CARRIER_SENSE   0xE14C  /* TX carrier sense error */
#define GMAC_RX_OCTETS_L        0xE150  /* RX bytes [31:0] */
#define GMAC_RX_OCTETS_H        0xE154  /* RX bytes [47:32] */
#define GMAC_RX_FRAMES          0xE158  /* RX frames */
#define GMAC_RX_BCAST_FRAMES    0xE15C  /* RX broadcast frames */
#define GMAC_RX_MCAST_FRAMES    0xE160  /* RX multicast frames */
#define GMAC_RX_PAUSE_FRAMES    0xE164  /* RX pause frames */
#define GMAC_RX_FRAME64         0xE168  /* RX 64 bytes frames */
#define GMAC_RX_FRAME65T127     0xE16C  /* RX 65-127 bytes frames */
#define GMAC_RX_FRAME128T255    0xE170  /* RX 128-255 bytes frames */
#define GMAC_RX_FRAME256T511    0xE174  /* RX 256-511 bytes frames */
#define GMAC_RX_FRAME512T1023   0xE178  /* RX 512-1023 bytes frames */
#define GMAC_RX_FRAME1024T1518  0xE17C  /* RX 1024-1518 bytes frames */
#define GMAC_RX_FRAME1519TUP    0xE180  /* RX 1519+ bytes frames */
#define GMAC_RX_UNDERSIZED      0xE184  /* RX undersized frames */
#define GMAC_RX_OVERSIZED       0xE188  /* RX oversized frames */
#define GMAC_RX_JABBER          0xE18C  /* RX jabber frames */
#define GMAC_RX_CRC_ERRORS      0xE190  /* RX CRC error frames */
#define GMAC_RX_LENGTH_ERRORS   0xE194  /* RX length error frames */
#define GMAC_RX_SYMBOL_ERRORS   0xE198  /* RX symbol error frames */
#define GMAC_RX_ALIGN_ERRORS    0xE19C  /* RX alignment error frames */
#define GMAC_RX_OVERRUN         0xE1A4  /* RX overrun */
#define GMAC_RX_IP_ERRORS       0xE1A8  /* RX IP checksum error */
#define GMAC_RX_TCP_ERRORS      0xE1AC  /* RX TCP checksum error */
#define GMAC_RX_UDP_ERRORS      0xE1B0  /* RX UDP checksum error */

/* GEM IF registers */

#define GEM_CONFIG              0xF000  /* GEM configuration register */
#define GEM_TX_CTRL             0xF004  /* TX control register */
#define GEM_TX_COL_FIFO_DEP     0xF008  /* TX collision FIFO depth */
#define GEM_RX_CTRL             0xF010  /* RX control register */
#define GEM_RX_PACK_SIZE        0xF014  /* RX status pack size */
#define GEM_RX_FIFO_DEP         0xF018  /* RX status FIFO depth */
#define GEM_RX_FIFO_DATA        0xF01C  /* RX status FIFO data */

/* IODMA register offsets (from regBase[1]) */

#define IODMA_TX_START          0x0000  /* TX start */
#define IODMA_TX_HDP            0x0004  /* TX head pointer */
#define IODMA_TX_LOCKED_SIZE    0x0008  /* TX locked transfer size */
#define IODMA_TX_CTRL           0x0010  /* TX control */
#define IODMA_TX_STATUS         0x0014  /* TX interrupt status */
#define IODMA_TX_RESET          0x0020  /* TX software reset */

#define IODMA_RX_START          0x0080  /* RX start */
#define IODMA_RX_HDP            0x0084  /* RX head pointer */
#define IODMA_RX_LOCKED_SIZE    0x0088  /* RX locked transfer size */
#define IODMA_RX_CTRL           0x0090  /* RX control */
#define IODMA_RX_STATUS         0x0094  /* RX interrupt status */
#define IODMA_RX_RESET          0x00A0  /* RX software reset */


/* HCSM FIFO control register */

#define HCSM_FIFO_CTRL_TX_EN        0x00000004  /* TX FIFO write enable */
#define HCSM_FIFO_CTRL_RX_EN        0x00000008  /* RX FIFO read enable */
#define HCSM_FIFO_CTRL_RX_RESET     0x00001000  /* RX FIFO reset */
#define HCSM_FIFO_CTRL_TX_RESET     0x00002000  /* TX FIFO reset */

/* HCSM FIFO interrupt status register */

#define HCSM_FIFO_STATUS_RXF        0x00000001  /* RX FIFO full */
#define HCSM_FIFO_STATUS_RXTH       0x00000002  /* RX FIFO threshold */
#define HCSM_FIFO_STATUS_TXE        0x00000004  /* TX FIFO empty */
#define HCSM_FIFO_STATUS_TXTH       0x00000008  /* TX FIFO threshold */
#define HCSM_FIFO_STATUS_FLUSH_TX   0x00000080  /* flush TX FIFO */

/* HCSM TX FIFO high threshold */

#define HCSM_TX_FIFO_HIGH_10M       0x000001C0  /* high threshold for 10M */
#define HCSM_TX_FIFO_HIGH_100M      0x000001C0  /* high threshold for 100M */
#define HCSM_TX_FIFO_HIGH_1000M     0x000001D0  /* high threshold for 1000M */

/* HCSM TX FIFO low threshold */

#define HCSM_TX_FIFO_LOW_DEF        0x00000180  /* low threshold default */

/* GEM FIFO control register */

#define GEM_FIFO_CTRL_RX_EN         0x00000001  /* RX enable */
#define GEM_FIFO_CTRL_HBRXRQ_EN     0x00000002  /* HBRXRQ enable */
#define GEM_FIFO_CTRL_TX_EN         0x00000008  /* TX enable */
#define GEM_FIFO_CTRL_HBTXRQ_EN     0x00000010  /* HBTXRQ enable */

/* GEM TX FIFO high threshold */

#define GEM_TX_FIFO_HIGH_10M        0x00000080  /* high threshold for 10M */
#define GEM_TX_FIFO_HIGH_100M       0x00000080  /* high threshold for 100M */
#define GEM_TX_FIFO_HIGH_1000M      0x00000100  /* high threshold for 1000M */

/* GMAC network contorl register */

#define GMAC_NET_CTRL_LOOPBACK      0x00000002  /* local loopback */
#define GMAC_NET_CTRL_RX_EN         0x00000004  /* RX enable */
#define GMAC_NET_CTRL_TX_EN         0x00000008  /* TX enable */
#define GMAC_NET_CTRL_MDIO_EN       0x00000010  /* mdio enable */
#define GMAC_NET_CTRL_MIB_CLR       0x00000020  /* clear mib counters */
#define GMAC_NET_CTRL_TX_START      0x00000200  /* TX start */
#define GMAC_NET_CTRL_TX_HALT       0x00000400  /* TX halt */

/* GMAC network config register */

#define GMAC_NET_CFG_VLAN_ONLY      0x00000004  /* discard non-VLAN frames */
#define GMAC_NET_CFG_JUMBO          0x00000008  /* jumbo frames upto 10240 */
#define GMAC_NET_CFG_COPYALL        0x00000010  /* copy all frames */
#define GMAC_NET_CFG_BC_DIS         0x00000020  /* broadcast disable */
#define GMAC_NET_CFG_MC_EN          0x00000040  /* multicast enable */
#define GMAC_NET_CFG_UC_EN          0x00000080  /* unicast enable */
#define GMAC_NET_CFG_RX_1536        0x00000100  /* RX upto 1536 */
#define GMAC_NET_CFG_PAUSE          0x00002000  /* pause enable */
#define GMAC_NET_CFG_LENGTH_ERR     0x00010000  /* length error discard */
#define GMAC_NET_CFG_RX_NO_FCS      0x00020000  /* RX FCS remove */
#define GMAC_NET_CFG_MDC_MASK       0x001c0000  /* MDC mask */
#define GMAC_MDC_AHB_20MHZ          0x00000000  /* AHB upto 20MHz */
#define GMAC_MDC_AHB_40MHZ          0x00040000  /* AHB upto 40MHz */
#define GMAC_MDC_AHB_80MHZ          0x00080000  /* AHB upto 80MHz */
#define GMAC_MDC_AHB_120MHZ         0x000c0000  /* AHB upto 120MHz */
#define GMAC_MDC_AHB_160MHZ         0x00100000  /* AHB upto 160MHz */
#define GMAC_MDC_AHB_240MHZ         0x00140000  /* AHB upto 240MHz */
#define GMAC_MDC_AHB_320MHZ         0x00180000  /* AHB upto 320MHz */
#define GMAC_MDC_AHB_540MHZ         0x001c0000  /* AHB upto 540MHz */
#define GMAC_NET_CFG_RX_CSUM        0x01000000  /* RX checksum offload enable */

/* GMAC network status register */

#define GMAC_STATUS_MDIO_IDLE       0x00000004  /* PHY maintenance idle */

/* GMAC TX status register */

#define GMAC_TX_STS_COLL            0x00000002  /* collision occurred */
#define GMAC_TX_STS_RETRY_EXCEED    0x00000004  /* retry limit exceeded*/
#define GMAC_TX_STS_GO              0x00000008  /* transmit go */
#define GMAC_TX_STS_CP              0x00000020  /* transmit complete */
#define GMAC_TX_STS_UNDERRUN        0x00000040  /* transmit underrun */
#define GMAC_TX_STS_LATE_COLL       0x00000080  /* late collision occured */

/* GMAC RX status register */

#define GMAC_RX_STS_CP              0x00000002  /* receive complete */
#define GMAC_RX_STS_OVERRUN         0x00000004  /* receive overrun */

/* GMAC interrupt status register */

#define GMAC_INT_RX                 0x00000002  /* RX complete */
#define GMAC_INT_TX_UR              0x00000010  /* TX underrun */
#define GMAC_INT_TX                 0x00000080  /* TX complete */
#define GMAC_INT_RX_OR              0x00000400  /* RX overrun */

/* GMAC clear status */

#define GMAC_STATUS_CLEAR_ALL       0xffffffff  /* clear interrupt status */

/* GMAC PHY maintenance register */

#define GMAC_PHY_MAINT_RSVD         0x00020000  /* reserved */
#define GMAC_PHY_MAINT_WR           0x10000000  /* write op */
#define GMAC_PHY_MAINT_RD           0x20000000  /* read op */
#define GMAC_PHY_MAINT_CLAUSE22     0x40000000  /* clause 22 PHY */
#define GMAC_PHY_MAINT_REG_SHIFT    18          /* register address shift */
#define GMAC_PHY_MAINT_PHY_SHIFT    23          /* phy address shift */
#define GMAC_PHY_MAINT_DATA_MASK    0x0000ffff  /* data mask */

/* GEM configuration register */

#define GEM_CFG_MODE_GEM            0x00000001  /* GEM mode select */
#define GEM_CFG_GEM_MII             0x00000000  /* GEM MII mode */
#define GEM_CFG_GEM_GMII            0x00000002  /* GEM GMII mode */
#define GEM_CFG_GEM_RMII            0x00000004  /* GEM RMII mode */
#define GEM_CFG_GEM_RGMII           0x00000006  /* GEM RGMII mode */
#define GEM_CFG_GEM_MODE_MASK       0x0000000e  /* mode mask */
#define GEM_CFG_PIN_MODE_MASK       0x00000070  /* pin strap mode */
#define GEM_CFG_DUPLEX_GEM          0x00000100  /* GEM duplex select */
#define GEM_CFG_GEM_FD              0x00000200  /* GEM full duplex */
#define GEM_CFG_SPEED_GEM           0x00000800  /* GEM speed select */
#define GEM_CFG_GEM_10M             0x00000000  /* GEM 10M */
#define GEM_CFG_GEM_100M            0x00001000  /* GEM 100M */
#define GEM_CFG_GEM_1000M           0x00002000  /* GEM 1000M */
#define GEM_CFG_SPEED_MASK          0x00003000  /* speed mask */

/* GEM TX control register */

#define GEM_TX_CTRL_DMA_EN          0x00000001  /* DMA enable */
#define GEM_TX_CTRL_CRC_EN          0x00000002  /* CRC enable */
#define GEM_TX_CTRL_RETRY_EN        0x00000004  /* retry enable */

/* GEM RX control register */

#define GEM_RX_CTRL_DMA_EN          0x00000001  /* DMA enable */

/* GEM RX status pack size */

#define GEM_RX_PACK_SIZE_DEF        0x00000100  /* RX status pack size */


/* IODMA registers */

#define IODMA_START_CMD             0x00000001  /* start DMA */
#define IODMA_RESET_CMD             0x00000001  /* reset DMA */

/* IODMA locked transfer size */

#define IODMA_LOCKED_SIZE_DEF       0x00000008  /* locked transfer size */


/* packet buffer descriptors definitions */

#define GEMAC_TX_DESC_CNT           256
#define GEMAC_RX_DESC_CNT           256
#define GEMAC_INC_DESC(x, y)        (x) = (((x) + 1) % y)
#define GEMAC_MAXFRAG               6
#define GEMAC_MAX_RX                16

#define FSTATUS_FDONE               0x80000000  /* frame done */
#define FCTRL_FREADY                0x00000001  /* frame ready */
#define FCTRL_FLAST                 0x00000002  /* last frame */
#define FCTRL_IRQ_EN                0x00000004  /* interrupt enable */
#define FCTRL_SCATTER               0x00000008  /* scatter */
#define BCTRL_BDONE                 0x80000000  /* buffer done */
#define BCTRL_BLAST                 0x00010000  /* last block */
#define BCTRL_BLEN_MASK             0x0000ffff  /* length mask */

#define FSTATUS_RX_ERR              0x00000001  /* RX frame error */
#define FSTATUS_RX_CSUM_IP          0x00400000  /* RX IP checksum */
#define FSTATUS_RX_CSUM_TCP         0x00800000  /* RX TCP checksum */
#define FSTATUS_RX_CSUM_UDP         0x01000000  /* RX UDP checksum */

typedef struct gemacFraDesc
    {
    volatile UINT32 next;
    volatile UINT32 system;
    volatile UINT32 status;
    volatile UINT32 control;
    } GEMAC_FRA_DESC;

typedef struct gemacBufDesc
    {
    volatile UINT32 pointer;
    volatile UINT32 control;
    } GEMAC_BUF_DESC;

typedef struct gemacTxDesc
    {
    GEMAC_FRA_DESC  frame;
    GEMAC_BUF_DESC  buffer[GEMAC_MAXFRAG];
    } GEMAC_TX_DESC;

typedef struct gemacRxDesc
    {
    GEMAC_FRA_DESC  frame;
    GEMAC_BUF_DESC  buffer;
    } GEMAC_RX_DESC;

typedef struct gemacMdcCtrl
    {
    UINT32  ahbClk;
    UINT32  mdcDiv;
    } GEMAC_MDC_CTRL;

/* private adapter context structure */

typedef struct gemacDrvCtrl
    {
    END_OBJ             gemacEndObj;
    VXB_DEVICE_ID       gemacDev;
    void *              gemacMuxDevCookie;

    JOB_QUEUE_ID        gemacJobQueue;
    QJOB                gemacIntJob;
    volatile BOOL       gemacIntPending;
    QJOB                gemacTxJob;
    volatile BOOL       gemacTxPending;
    QJOB                gemacRxJob;
    volatile BOOL       gemacRxPending;

    volatile BOOL       gemacTxStall;

    BOOL                gemacPolling;
    M_BLK_ID            gemacPollBuf;
    UINT32              gemacIntrs;

    UINT8               gemacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    gemacCaps;

    END_IFDRVCONF       gemacEndStatsConf;
    END_IFCOUNTERS      gemacEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST *     gemacMediaList;
    END_ERR             gemacLastError;
    UINT32              gemacCurMedia;
    UINT32              gemacCurStatus;
    VXB_DEVICE_ID       gemacMiiBus;
    VXB_DEVICE_ID       gemacMiiDev;
    FUNCPTR             gemacMiiPhyRead;
    FUNCPTR             gemacMiiPhyWrite;
    int                 gemacMiiPhyAddr;

    /* end MII/ifmedia required fields */

    GEMAC_TX_DESC *     gemacTxDescMem;
    GEMAC_RX_DESC *     gemacRxDescMem;

    UINT32              gemacTxProd;
    UINT32              gemacTxCons;
    UINT32              gemacTxFree;
    UINT32              gemacRxIdx;

    SEM_ID              gemacMiiSem;

    int                 gemacMaxMtu;

    void *              regBase[GEMAC_MOD_NUM];
    void *              handle[GEMAC_MOD_NUM];

    UINT32              gemacDescMem;
    UINT32              gemacMdcDiv;
    UINT32              gemacMedia;
    } GEMAC_DRV_CTRL;

/* GEMAC control module register low level access routines */

#define GEMAC_BAR0(p)       ((GEMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[0]
#define GEMAC_HANDLE0(p)    ((GEMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[0]

#define CSR0_READ_4(pDev, addr)             \
        vxbRead32(GEMAC_HANDLE0(pDev),      \
                  (UINT32 *)((char *)GEMAC_BAR0(pDev) + addr))

#define CSR0_WRITE_4(pDev, addr, data)      \
        vxbWrite32(GEMAC_HANDLE0(pDev),     \
                   (UINT32 *)((char *)GEMAC_BAR0(pDev) + addr), data)

#define CSR0_SETBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) | (val))

#define CSR0_CLRBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) & ~(val))

/* GEMAC IODMA module register low level access routines */

#define GEMAC_BAR1(p)       ((GEMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[1]
#define GEMAC_HANDLE1(p)    ((GEMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[1]

#define CSR1_READ_4(pDev, addr)             \
        vxbRead32(GEMAC_HANDLE1(pDev),      \
                  (UINT32 *)((char *)GEMAC_BAR1(pDev) + addr))

#define CSR1_WRITE_4(pDev, addr, data)      \
        vxbWrite32(GEMAC_HANDLE1(pDev),     \
                   (UINT32 *)((char *)GEMAC_BAR1(pDev) + addr), data)

#define CSR1_SETBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) | (val))

#define CSR1_CLRBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbComcertoGemacEndh */
