/* rtl8139VxbEnd.h - register definitions for the RealTek 8139 ethernet chip */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,09jul07,wap  Make this driver SMP safe, convert to new register access API
01c,21aug06,wap  Fix TX error handling
01c,17nov06,wap  Merge in updates from development branch
01b,24jul06,wap  Change RTL_ADJ macro
01a,16mar06,wap  written
*/

#ifndef __INCrtl8139VxbEndh
#define __INCrtl8139VxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void rtlRegister (void);

#ifndef BSP_VERSION

/* Supported vendor and device IDs */

#define RTL_VENDORID		0x10EC

#define RTL_DEVICEID_8138	0x8138
#define RTL_DEVICEID_8139	0x8139
#define RTL_DEVICEID_8100	0x8100
/*
 * Accton PCI vendor ID
 */
#define ACCTON_VENDORID                         0x1113

/*
 * Accton MPX 5030/5038 device ID.
 */
#define ACCTON_DEVICEID_5030                    0x1211

/*
 * Nortel PCI vendor ID
 */
#define NORTEL_VENDORID                         0x126C

/*
 * Delta Electronics Vendor ID.
 */
#define DELTA_VENDORID                          0x1500

/*
 * Delta device IDs.
 */
#define DELTA_DEVICEID_8139                     0x1360

/*
 * Addtron vendor ID.
 */
#define ADDTRON_VENDORID                        0x4033

/*
 * Addtron device IDs.
 */
#define ADDTRON_DEVICEID_8139                   0x1360

/*
 * D-Link vendor ID.
 */
#define DLINK_VENDORID                          0x1186

/*
 * D-Link DFE-530TX+ device ID
 */
#define DLINK_DEVICEID_530TXPLUS                0x1300

/*
 * D-Link DFE-5280T device ID
 */
#define DLINK_DEVICEID_528T                     0x4300

/*
 * D-Link DFE-690TXD device ID
 */
#define DLINK_DEVICEID_690TXD                   0x1340

/*
 * Corega K.K vendor ID
 */
#define COREGA_VENDORID                         0x1259

/*
 * Corega FEther CB-TXD device ID
 */
#define COREGA_DEVICEID_FETHERCBTXD             0xa117

/*
 * Corega FEtherII CB-TXD device ID
 */
#define COREGA_DEVICEID_FETHERIICBTXD           0xa11e

/*
 * Corega CG-LAPCIGT device ID
 */
#define COREGA_DEVICEID_CGLAPCIGT               0xc107

/*
 * Peppercon vendor ID
 */
#define PEPPERCON_VENDORID                      0x1743

/*
 * Peppercon ROL-F device ID
 */
#define PEPPERCON_DEVICEID_ROLF                 0x8139


/*
 * Planex Communications, Inc. vendor ID
 */
#define PLANEX_VENDORID                         0x14ea

/*
 * Planex FNW-3800-TX device ID
 */

#define PLANEX_DEVICEID_FNW3800TX               0xab07

/*
 * LevelOne vendor ID
 */
#define LEVEL1_VENDORID                         0x018A

/*
 * LevelOne FPC-0106TX devide ID
 */
#define LEVEL1_DEVICEID_FPC0106TX               0x0106

/*
 * Compaq vendor ID
 */
#define CP_VENDORID                             0x021B

/*
 * Edimax vendor ID
 */
#define EDIMAX_VENDORID                         0x13D1

/*
 * Edimax EP-4103DL cardbus device ID
 */
#define EDIMAX_DEVICEID_EP4103DL                0xAB06




/*
 * Station ID registers. The RealTek manual documents these
 * as if there are six of them, each a byte wide, but in reality
 * you have to access them using 32 bit reads/writes. So we
 * define them as 2 offsets here.
 */

#define RTL_IDR0		0x0000
#define RTL_IDR1		0x0004

/*
 * Multicast hash registers. The 8139 has a 64-bit hash table.
 * This must also be accesed using 32 bit reads/writes.
 */

#define RTL_MAR0		0x0008
#define RTL_MAR1		0x000C

/* Transmit status registers */

#define RTL_TXSTAT0		0x0010
#define RTL_TXSTAT1		0x0014
#define RTL_TXSTAT2		0x0018
#define RTL_TXSTAT3		0x001C

#define RTL_TXSTAT_SIZE		0x00001FFF
#define RTL_TXSTAT_OWN		0x00002000
#define RTL_TXSTAT_UFLOW	0x00004000	/* underrun error */
#define RTL_TXSTAT_OK		0x00008000	/* transmission OK */
#define RTL_TXSTAT_THRESH	0x003F0000	/* TX start threshold */
#define RTL_TXSTAT_COLLCNT	0x0F000000	/* collision count */
#define RTL_TXSTAT_HBEAT	0x10000000	/* hearbeat error (10Mbps) */
#define RTL_TXSTAT_LATECOLL	0x20000000	/* late collision */
#define RTL_TXSTAT_TXABRT	0x40000000	/* TX aborted */
#define RTL_TXSTAT_CARRLOSS	0x80000000	/* carrier loss error */

#define RTL_TXTHRESH(x)		((((x) >> 3) << 16) & 0x003F0000)

/* Transmit buffer pointer registers */

#define RTL_TXBASE0		0x0020
#define RTL_TXBASE1		0x0024
#define RTL_TXBASE2		0x0028
#define RTL_TXBASE3		0x002C

/* RX window base address */

#define RTL_RXBASE		0x0030

/* Early receive byte count (read only) */

#define RTL_RXEARLYCNT		0x0034

/* Early receive status (read only) */

#define RTL_RXEARLYSTS		0x0036

#define RTL_RXEARLYSTS_OK	0x01
#define RTL_RXEARLYSTS_OVW	0x02	/* Overwrite */
#define RTL_RXEARLYSTS_BAD	0x04	/* bad frame */
#define RTL_RXEARLYSTS_GOOD	0x08	/* good frame */

/* Command register */

#define RTL_CMD			0x0037

#define RTL_CMD_RX_EMPTY	0x01	/* RX window is empty (read only) */
#define RTL_CMD_TX_ENABLE	0x04	/* enable transmitter */
#define RTL_CMD_RX_ENABLE	0x08	/* enable receiver */
#define RTL_CMD_RESET		0x10	/* reset the controller */

/*
 * Current RX window offset (16 bits).
 * This register indicates the total number of bytes in the RX
 * window waiting to be read. The initial value is 0xFFF0.
 * This register can be updated by software.
 */

#define RTL_CURRXOFF		0x0038

#define RTL_CURRXOFF_INIT	0xFFF0

/*
 * Current RX buffer address. This reflects the total number
 * of bytes waiting in the RX window. This is read only.
 */

#define RTL_CURRXLEN		0x003A

/*
 * Interrupt mask register (16 bits)
 * See ISR register definition below for bit definitions.
 */

#define RTL_IMR			0x003C

/* Interrupt status register (16 bits) */

#define RTL_ISR			0x003E

/* Interrupt status register (16 bits) */

#define RTL_ISR_RX_OK		0x0001	/* good frame received */
#define RTL_ISR_RX_ERR		0x0002	/* bad frame received */
#define RTL_ISR_TX_OK		0x0004	/* transmit completed successfully */
#define RTL_ISR_TX_ERR		0x0008	/* transmit completed with error */
#define RTL_ISR_RX_FULL		0x0010	/* RX window is full */
#define RTL_ISR_PKT_UNDERRUN	0x0020	/* CURRXOFF written, but window empty*/
#define RTL_ISR_LINKCHG		0x0020  /* link change */
#define RTL_ISR_RX_OFLOW	0x0040  /* RX FIFO overflow  */
#define RTL_ISR_CABLE_LEN_CHGD	0x2000	/* cable length changed */
#define RTL_ISR_TIMER_EXPIRED	0x4000	/* timer ran out */
#define RTL_ISR_SYSTEM_ERR	0x8000	/* PCI system error */

#define RTL_INTRS (RTL_ISR_RX_OK|RTL_ISR_RX_ERR|RTL_ISR_TX_OK|	\
		   RTL_ISR_TX_ERR|RTL_ISR_RX_FULL|RTL_ISR_PKT_UNDERRUN| \
		   RTL_ISR_LINKCHG|RTL_ISR_RX_OFLOW|RTL_ISR_TIMER_EXPIRED| \
		   RTL_ISR_SYSTEM_ERR)

#define RTL_RXINTRS (RTL_ISR_RX_OK|RTL_ISR_RX_ERR| \
		     RTL_ISR_RX_FULL|RTL_ISR_RX_OFLOW)
#define RTL_TXINTRS (RTL_ISR_TX_OK|RTL_ISR_TX_ERR)

/* TX config register */

#define RTL_TXCFG		0x0040

#define RTL_TXCFG_CLRABRT	0x00000001      /* retransmit aborted pkt */
#define RTL_TXCFG_MAXDMA	0x00000700      /* max DMA burst size */
#define RTL_TXCFG_CRCAPPEND	0x00010000      /* CRC append (0 = yes) */
#define RTL_TXCFG_LOOPBKTST	0x00060000      /* loopback test */
#define RTL_TXCFG_IFG2		0x00080000      /* 8169 only */
#define RTL_TXCFG_IFG		0x03000000      /* interframe gap */
#define RTL_TXCFG_HWREV		0x7CC00000

#define RTL_TXDMA_16BYTES	0x00000000
#define RTL_TXDMA_32BYTES	0x00000100
#define RTL_TXDMA_64BYTES	0x00000200
#define RTL_TXDMA_128BYTES	0x00000300
#define RTL_TXDMA_256BYTES	0x00000400
#define RTL_TXDMA_512BYTES	0x00000500
#define RTL_TXDMA_1024BYTES	0x00000600
#define RTL_TXDMA_2048BYTES	0x00000700

#define RTL_LOOPTEST_OFF	0x00000000
#define RTL_LOOPTEST_ON		0x00020000

/* Known revision codes. */

#define RTL_HWREV_8169          0x00000000
#define RTL_HWREV_8110S         0x00800000
#define RTL_HWREV_8169S         0x04000000
#define RTL_HWREV_8169_8110SB   0x10000000
#define RTL_HWREV_8169_8110SC   0x18000000
#define RTL_HWREV_8100E         0x30800000
#define RTL_HWREV_8101E         0x34000000
#define RTL_HWREV_8168          0x38000000
#define RTL_HWREV_8139          0x60000000
#define RTL_HWREV_8139A         0x70000000
#define RTL_HWREV_8139AG        0x70800000
#define RTL_HWREV_8139B         0x78000000
#define RTL_HWREV_8130          0x7C000000
#define RTL_HWREV_8139C         0x74000000
#define RTL_HWREV_8139D         0x74400000
#define RTL_HWREV_8139CPLUS     0x74800000
#define RTL_HWREV_8101          0x74c00000
#define RTL_HWREV_8100          0x78800000

/* RX config register */

#define RTL_RXCFG		0x0044

#define RTL_RXCFG_RX_PROMISC	0x00000001	/* accept all unicast */
#define RTL_RXCFG_RX_INDIV	0x00000002	/* accept filter match */
#define RTL_RXCFG_RX_MULTI	0x00000004      /* accept all multicast */
#define RTL_RXCFG_RX_BROAD	0x00000008      /* accept all broadcast */
#define RTL_RXCFG_RX_RUNT	0x00000010	/* accept runts */
#define RTL_RXCFG_RX_ERRPKT	0x00000020	/* accept error frames */
#define RTL_RXCFG_WRAP		0x00000080	/* RX window wrapping */
#define RTL_RXCFG_MAXDMA	0x00000700
#define RTL_RXCFG_WINSZ		0x00001800	/* RX window size */
#define RTL_RXCFG_FIFOTHRESH	0x0000E000
#define RTL_RXCFG_RER8		0x00010000
#define RTL_RXCFG_EARLYINT	0x00020000
#define RTL_RXCFG_EARLYTHRESH	0x07000000

#define RTL_RXDMA_16BYTES	0x00000000
#define RTL_RXDMA_32BYTES	0x00000100
#define RTL_RXDMA_64BYTES	0x00000200
#define RTL_RXDMA_128BYTES	0x00000300
#define RTL_RXDMA_256BYTES	0x00000400
#define RTL_RXDMA_512BYTES	0x00000500
#define RTL_RXDMA_1024BYTES	0x00000600
#define RTL_RXDMA_UNLIMITED	0x00000700

#define RTL_RXWIN_8K		0x00000000
#define RTL_RXWIN_16K		0x00000800
#define RTL_RXWIN_32K		0x00001000
#define RTL_RXWIN_64K		0x00001800

#define RTL_RXFIFO_16BYTES	0x00000000
#define RTL_RXFIFO_32BYTES	0x00002000
#define RTL_RXFIFO_64BYTES	0x00004000
#define RTL_RXFIFO_128BYTES	0x00006000
#define RTL_RXFIFO_256BYTES	0x00008000
#define RTL_RXFIFO_512BYTES	0x0000A000
#define RTL_RXFIFO_1024BYTES	0x0000C000
#define RTL_RXFIFO_NOTHRESH	0x0000E000

/* Countdown timer register (32 bits) */

#define RTL_TIMER		0x0048

/* Missed packet counter */

#define RTL_MISSEDPKT		0x004C

/* EEPROM access register */

#define RTL_EECMD		0x0050

#define RTL_EECMD_DATAOUT	0x01    /* Data out */
#define RTL_EECMD_DATAIN	0x02    /* Data in */
#define RTL_EECMD_CLK		0x04    /* clock */
#define RTL_EECMD_SEL		0x08    /* chip select */
#define RTL_EECMD_MODE		0xC0

#define RTL_EEMODE_OFF		0x00
#define RTL_EEMODE_AUTOLOAD	0x40
#define RTL_EEMODE_PROGRAM	0x80
#define RTL_EEMODE_WRITECFG	0xC0

/* 9346 EEPROM commands */
#define RTL_9346_WRITE		0x140
#define RTL_9346_READ_6BIT	0x180
#define RTL_9346_READ_8BIT	0x600
#define RTL_9346_ERASE		0x1c0

#define RTL_EE_ID		0x00
#define RTL_EE_PCI_VID		0x01
#define RTL_EE_PCI_DID		0x02
/* Location of station address inside EEPROM */
#define RTL_EE_EADDR		0x07

#define RTL_EE_SIGNATURE	0x8129

/* Strapping config 1 */

#define RTL_CFG1		0x0051

/* Strapping config 2 */

#define RTL_CFG2		0x0052

/* Media status */

#define RTL_MEDIASTAT		0x0058

#define RTL_MEDIASTAT_RXPAUSE	0x01
#define RTL_MEDIASTAT_TXPAUSE	0x02
#define RTL_MEDIASTAT_LINK	0x04	/* 0 = link ok, 1 = link down */
#define RTL_MEDIASTAT_SPEED10	0x08	/* 1 = 10Mbps, 0 = 100Mbps */
#define RTL_MEDIASTAT_AUXPRW	0x10
#define RTL_MEDIASTAT_RXFLOWCTL	0x40    /* duplex mode */
#define RTL_MEDIASTAT_TXFLOWCTL	0x80    /* duplex mode */

/* Strapping config 3 */

#define RTL_CFG3		0x0059

/* Strapping config 4 */

#define RTL_CFG4		0x005A

/* Multiple interrupt select */

#define RTL_MULTIINT		0x005C

/* PCI revision ID */

#define RTL_PCIREV		0x005E

/* Status of all TX descriptors (16 bits) */

#define RTL_TXALLSTS		0x0060

#define RTL_TXALLSTS_OWN	0x000F
#define RTL_TXALLSTS_ABRT	0x00F0
#define RTL_TXALLSTS_UFLOW	0x0F00
#define RTL_TXALLSTS_OK		0xF000
#define RTL_TXALLSTS_DESC0	0x1111
#define RTL_TXALLSTS_DESC1	0x2222
#define RTL_TXALLSTS_DESC2	0x4444
#define RTL_TXALLSTS_DESC3	0x8888

#define RTL_TXSTS_DESC(x)	(RTL_TXALLSTS_DESC0 << (x))

/* PHY registers */

#define RTL_BMCR		0x0062	/* basic mode control register */
#define RTL_BMSR		0x0064	/* basic mode status register */
#define RTL_ANAR		0x0066	/* autoneg advertisement */
#define RTL_ANLPAR		0x0068	/* autoneg partner ability */
#define RTL_ANER		0x006A	/* autoneg expansion */

/* Disconnect counter */

#define RTL_DISCCNT		0x006C

/* False carrier sense counter */

#define RTL_FALSCARRCNT		0x006E

/* NWAY test register */

#define RTL_NWAYTST		0x0070

#define RTL_NWAYTST_LSC		0x01	/* autoneg link status check */
#define RTL_NWAYTST_PDF		0x02	/* autoneg parallel detection fault */
#define RTL_NWAYTST_ABD		0x04	/* autoneg ability detect state */
#define RTL_NWAYTST_LED0	0x08	/* LED0 indicates link */
#define RTL_NWAYTST_LOOP	0x80	/* enable NWAY loopback mode */

/* RX error count register */

#define RTL_RXERRCNT		0x0072

/* CS configuration register */

#define RTL_CSCFG		0x0074

#define RTL_CSCFG_BYPASS_SCR	0x0001	/* bypass scrambler */
#define RTL_CSCFG_LED1_LINKSTS	0x0004	/* set LED1 to show link status */
#define RTL_CSCFG_LINKSTS	0x0008	/* 1 == link, 0 == no link */
#define RTL_CSCFG_FORCE_LINK	0x0020	/* force good link */
#define RTL_CSCFG_FORCE_100	0x0040	/* force 100Mbps link */
#define RTL_CSCFG_JABBER	0x0080	/* enable jabber detect */
#define RTL_CSCFG_HBEAT		0x0100	/* enable heartbeat detect */
#define RTL_CSCFG_LD		0x0200	/* Link disable */
#define RTL_CSCFG_TESTFUN	0x8000	/* auto-neg speeds up internal timer */

/* Flash access */

#define RTL_FLASH		0x00D4

#define RTL_FLASH_ADDR		0x0000FFFF	/* address bus */
#define RTL_FLASH_SW_ENABLE	0x00010000	/* enable software read/write */
#define RTL_FLASH_WRITE_ENABLE	0x00020000	/* set WEB pin */
#define RTL_FLASH_OUTPUT_ENABLE	0x00040000	/* set OEB pin */
#define RTL_FLASH_CHIPSEL	0x00080000	/* set ROMCSB pin */
#define RTL_FLASH_DATA		0xFF000000	/* data bus */

/*
 * Every received frame in the RX window will have a 32-bit header
 * prepended to it containing the frame length and RX status. This
 * header is defined below.
 */

typedef struct rtl_rxstat {
	volatile UINT16		rtlRxSts;
	volatile UINT16		rtlRxLen;
} rtlRxStat;

#define RTL_RXSTS_OK		0x0001	/* good frame received */
#define RTL_RXSTS_FAE		0x0002	/* frame alignment error */
#define RTL_RXSTS_CRC		0x0004	/* CRC error */
#define RTL_RXSTS_GIANT		0x0008	/* giant packet error */
#define RTL_RXSTS_RUNT		0x0010	/* runt packet error */
#define RTL_RXSTS_ISE		0x0020	/* invalid symbol error */
#define RTL_RXSTS_BCAST		0x2000	/* broadcast frame received */
#define RTL_RXSTS_UCAST		0x4000	/* unicast frame received */
#define RTL_RXSTS_MCAST		0x8000	/* multicast frame received */

#define RTL_RXSTS_UNFINISHED	0xFFF0

/*
 * The RealTek can only have 4 TX packets in flight
 * at any given time.
 */
#define RTL_TX_CNT	4

#define RTL_RX_BUFLEN	65536
#define RTL_RX_LIMIT(x)	((x) + (RTL_RX_BUFLEN))
#define RTL_RX_ROUND(x)	(((x) + 3) & ~3)
#define RTL_TIMEOUT	10000

#define RTL_TX_THRESH_INIT	96
#define RTL_RX_FIFOTHRESH	RTL_RXFIFO_NOTHRESH
#define RTL_RX_MAXDMA		RTL_RXDMA_UNLIMITED
#define RTL_TX_MAXDMA		RTL_TXDMA_2048BYTES

#define RTL_INC(x)		(x = (x + 1) % RTL_TX_CNT)
#define RTL_CUR_TXBASE(x)	((x->rtlTxCur * 4) + RTL_TXBASE0)
#define RTL_CUR_TXSTAT(x)	((x->rtlTxCur * 4) + RTL_TXSTAT0)
#define RTL_CUR_TXMBLK(x)	(x->rtlTxMblks[x->rtlTxCur])
#define RTL_CUR_DMAMAP(x)	(x->rtlTxMaps[x->rtlTxCur])
#define RTL_LAST_TXBASE(x)	((x->rtlTxLast * 4) + RTL_TXBASE0)
#define RTL_LAST_TXSTAT(x)	((x->rtlTxLast * 4) + RTL_TXSTAT0)
#define RTL_LAST_TXMBLK(x)	(x->rtlTxMblks[x->rtlTxLast])
#define RTL_LAST_DMAMAP(x)	(x->rtlTxMaps[x->rtlTxLast])

#define RTL_ADJ(x)	(x)->m_data += 2

#define RTL_CLSIZE	1536
#define RTL_MAX_RX	16

#define RTL_NAME	"rtl"

/*
 * Private adapter context structure.
 */

typedef struct rtl_drv_ctrl
    {
    END_OBJ		rtlEndObj;
    VXB_DEVICE_ID	rtlDev;
    void *		rtlBar;
    void *		rtlHandle;
    void *		rtlMuxDevCookie;

    JOB_QUEUE_ID	rtlJobQueue;
    QJOB		rtlRxJob;
    atomicVal_t		rtlRxPending;

    QJOB		rtlTxJob;
    atomicVal_t		rtlTxPending;
    UINT8		rtlTxCur;
    UINT8		rtlTxLast;
    UINT8		rtlTxFree;
    volatile BOOL	rtlTxStall;
    UINT16		rtlTxThresh;

    QJOB		rtlIntJob;
    atomicVal_t		rtlIntPending;

    BOOL		rtlPolling;
    M_BLK_ID		rtlPollBuf;
    UINT16		rtlIntMask;
    UINT16		rtlIntrs;

    UINT8		rtlAddr[ETHER_ADDR_LEN];

    UINT16		rtlEeReadCmd;

    END_CAPABILITIES	rtlCaps;

    END_IFDRVCONF	rtlEndStatsConf;
    END_IFCOUNTERS	rtlEndStatsCounters;
    UINT32		rtlInErrors;
    UINT32		rtlInDiscards;
    UINT32		rtlInUcasts;
    UINT32		rtlInMcasts;
    UINT32		rtlInBcasts;
    UINT32		rtlInOctets;
    UINT32		rtlOutErrors;
    UINT32		rtlOutUcasts;
    UINT32		rtlOutMcasts;
    UINT32		rtlOutBcasts;
    UINT32		rtlOutOctets;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*rtlMediaList;
    END_ERR		rtlLastError;
    UINT32		rtlCurMedia;
    UINT32		rtlCurStatus;
    VXB_DEVICE_ID	rtlMiiBus;
    /* End MII/ifmedia required fields */

    /* RX DMA tags and maps. */
    VXB_DMA_TAG_ID	rtlParentTag;
    VXB_DMA_TAG_ID	rtlRxWindowTag;
    VXB_DMA_MAP_ID	rtlRxWindowMap;
    UINT8		*rtlRxWindow;

    /* TX DMA tags and maps. */
    VXB_DMA_TAG_ID	rtlTxTag;
    VXB_DMA_MAP_ID	rtlTxMaps[RTL_TX_CNT];
    M_BLK_ID		rtlTxMblks[RTL_TX_CNT];

    SEM_ID		rtlDevSem;
    } RTL_DRV_CTRL;

#define RTL_BAR(p)   ((RTL_DRV_CTRL *)(p)->pDrvCtrl)->rtlBar
#define RTL_HANDLE(p)   ((RTL_DRV_CTRL *)(p)->pDrvCtrl)->rtlHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (RTL_HANDLE(pDev), (UINT32 *)((char *)RTL_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (RTL_HANDLE(pDev),                             \
        (UINT32 *)((char *)RTL_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (RTL_HANDLE(pDev), (UINT16 *)((char *)RTL_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (RTL_HANDLE(pDev),                             \
        (UINT16 *)((char *)RTL_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (RTL_HANDLE(pDev), (UINT8 *)((char *)RTL_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (RTL_HANDLE(pDev),                              \
        (UINT8 *)((char *)RTL_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCrtl8139VxbEndh */
