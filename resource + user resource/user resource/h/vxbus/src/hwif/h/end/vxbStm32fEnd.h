/* vxbStm32fEnd.h - header file for STM32F20x/STM32F21x VxBus END driver */

/*
 * Copyright (c) 2012, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,31jul13,mpc  change STM_RX_DESC_CNT/STM_TX_DESC_CNT to 64
01d,15oct12,rec  process interrupts in job queue
01c,02aug12,rec  change stm_desc field names.  Device name changed to 'stm'.
01b,11jul12,rec  change STM_NAME
01a,04mar12,rec  written
*/

#ifndef __INCvxbStmEndh
#define __INCvxbStmEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbStmEndRegister (void);

#ifndef BSP_VERSION

#define STM_MTU			1500
#define STM_JUMBO_MTU		9000
#define STM_CLSIZE		1536
#define STM_NAME		"stm"
#define STM_TIMEOUT 		10000

#define STM_MAX_RX 		16
#define STM_MAXFRAG 		16
#define STM_TX_INC(x, y)     	(x) = (((x) + 1) % y)
#define STM_CLSIZE		1536

#define STM_RX_DESC_CNT 	64
#define STM_TX_DESC_CNT 	64

#define PHY_TIMEOUT 		10000

/*****************************************************************************
 *
 * DMA descriptor definition
 *
 *****************************************************************************/

/* 
 * Normal DMA descriptors (both transmit and receive) 
 * Enhanced are not supported.
 */

typedef struct stm_desc
    {
    volatile UINT32	word0;  /* See RDES0/TDES0 bit definition */
    volatile UINT32	word1;  /* See RDES1/TDES1 bit definition */
    volatile UINT32	buf0;   /* Bufer address */
    volatile UINT32	buf1;   /* Next buffer pointer */
    } STM_DESC;
/* TDES0 bit definition */

#define TDES0_DB     0x00000001  /* Deferred */
#define TDES0_UF     0x00000002  /* Undeflow */
#define TDES0_ED     0x00000004  /* Excessive deferral */
#define TDES0_CCMSK  0x00000078  /* Collision count mask */
#define TDES0_CCSHFT          3  /* Collision count shift */
#define TDES0_VF     0x00000080  /* VLAN frame */
#define TDES0_EC     0x00000100  /* Excessive collision */
#define TDES0_LCO    0x00000200  /* Late collision */
#define TDES0_NC     0x00000400  /* No carrier */
#define TDES0_LCA    0x00000800  /* Loss of carrier */
#define TDES0_IPE    0x00001000  /* IP payload error */
#define TDES0_FF     0x00002000  /* Frame flushed */
#define TDES0_JT     0x00004000  /* Jabber timeout */
#define TDES0_ES     0x00008000  /* Error summary */
#define TDES0_IHE    0x00010000  /* IP header error */
#define TDES0_TTSS   0x00020000  /* Transmit timestamp status */
#define TDES0_TCH    0x00100000  /* Second address chained */
#define TDES0_TER    0x00200000  /* Transmitter end of ring */
#define TDES0_CICMSK 0x00C00000  /* Checksum insertion control mask */
#define TDES0_CICSHFT        22  /* Checksum insertion control shift */
#define TDES0_TTSE   0x02000000  /* Transmit time stamp enable */
#define TDES0_DP     0x04000000  /* Disable pad */
#define TDES0_DC     0x08000000  /* Disable CRC */
#define TDES0_FS     0x10000000  /* First segment */
#define TDES0_LS     0x20000000  /* Last segment */
#define TDES0_IC     0x40000000  /* Interrupt completion */
#define TDES0_OWN    0x80000000  /* Own bit */

/* TDES1 bit definition */

#define TDES1_TBS1MSK 0x00001FFF  /* Transmit buffer1 size mask */
#define TDES1_TBS1SHFT         0  /* Transmit buffer1 shift */
#define TDES1_TBS2MSK 0x1FFF0000  /* Transmit buffer2 size mask */
#define TDES1_TBS2SHFT        16  /* Transmit buffer2 shift */

/* RDES0 bit definition */

#define RDES0_PCE     0x00000001  /* payload checksum error/ext status avail*/
#define RDES0_CE      0x00000002  /* CRC error */
#define RDES0_DRE     0x00000004  /* Dribble-bit error */
#define RDES0_RE      0x00000008  /* Receiver error */
#define RDES0_RWT     0x00000010  /* RCV watchdog timeout */
#define RDES0_FT      0x00000020  /* Fream-type (1=ethernet) */
#define RDES0_LCO     0x00000040  /* Late Collision */
#define RDES0_IPC     0x00000080  /* IPv checksum error/time stamp valid */
#define RDES0_LS      0x00000100  /* Last descriptor */
#define RDES0_FS      0x00000200  /* First descriptor */
#define RDES0_VLAN    0x00000400  /* VLAN tag */
#define RDES0_OE      0x00000800  /* Overflow error */
#define RDES0_LE      0x00001000  /* Length error */
#define RDES0_SAF     0x00002000  /* Source address filter fail */
#define RDES0_DE      0x00004000  /* Descriptor error */
#define RDES0_ES      0x00008000  /* Error summary */
#define RDES0_FLMSK   0x3FFF0000  /* Frame length mask */
#define RDES0_FLSHFT          16  /* Frame length shift */
#define RDES0_AFM     0x40000000  /* Destination address filter fail */
#define RDES0_OWN     0x80000000  /* Own bit */
#define STM_RXBYTES(x)           ((x & RDES0_FLMSK) >> RDES0_FLSHFT)

/* RDES1 bit definition */

#define RDES1_RBS1MSK 0x00001FFF  /* Receive buffer1 size mask */
#define RDES1_RBS1SHFT         0  /* Receive buffer1 size shift */
#define RDES1_RCH     0x00004000  /* Second address chained flag */
#define RDES1_RER     0x00008000  /* End of ring */
#define RDES1_RBS2MSK 0x1FFF0000  /* Receive buffer2 size mask */
#define RDES1_RBS2SHFT        16  /* Receive buffer2 size shift */
#define RDES1_DIC     0x80000000  /* Disable interrupt on completion */

#define STM32F_ADJ(x)      (x)->m_data += 8
#define STM32F_INC_DESC(x, y)	(x) = ((x) + 1) % (y)

/* private adapter context structure */

typedef struct stm32f_drv_ctrl
    {
    END_OBJ		stmEndObj;
    VXB_DEVICE_ID	stmDev;
    void *		stmHandle;
    void *		stmBar;
    void *		stmMuxDevCookie;

    JOB_QUEUE_ID	stmJobQueue;
    QJOB		stmIntJob;
    volatile BOOL	stmIntPending;

#ifdef _STM_SPLIT_INTERRUPT_HANDLING
    QJOB		stmRxJob;
    atomic_t		stmRxPending;

    QJOB		stmTxJob;
    atomic_t		stmTxPending;
#endif /* _STM_SPLIT_INTERRUPT_HANDLING */

    UINT8		stmTxCur;
    UINT8		stmTxLast;
    volatile BOOL	stmTxStall;
    UINT16		stmTxThresh;

    BOOL		stmPolling;
    M_BLK_ID		stmPollBuf;
    UINT16		stmIntMask;

    UINT8		stmAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	stmCaps;

    END_IFDRVCONF	stmEndStatsConf;
    END_IFCOUNTERS	stmEndStatsCounters;

    VXB_DMA_MAP_ID	stmRxMblkMap[STM_RX_DESC_CNT];
    VXB_DMA_MAP_ID	stmTxMblkMap[STM_TX_DESC_CNT];

    M_BLK_ID		stmTxMblk[STM_RX_DESC_CNT];
    M_BLK_ID		stmRxMblk[STM_TX_DESC_CNT];

    UINT32		stmTxProd;
    UINT32		stmTxCons;
    UINT32		stmTxFree;
    UINT32		stmRxIdx;

    /* Begin MII/ifmedia required fields. */
    
    END_MEDIALIST *	stmMediaList;
    END_ERR		stmLastError;
    UINT32		stmCurMedia;
    UINT32		stmCurStatus;
    VXB_DEVICE_ID	stmMiiBus;
    
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    
    VXB_DMA_TAG_ID	stmParentTag;
    STM_DESC          * stmRxDescMem;
    VXB_DMA_MAP_ID	stmRxDescMap;
    VXB_DMA_TAG_ID	stmRxDescTag;
    STM_DESC          * stmTxDescMem;
    VXB_DMA_MAP_ID	stmTxDescMap;
    VXB_DMA_TAG_ID	stmTxDescTag;

    VXB_DMA_TAG_ID	stmMblkTag;

    SEM_ID		stmDevSem;

    /* Currently enabled interrupt flags */
    
    UINT32              macInterruptsEnabled;
    UINT32              mmcRcvInterruptsEnabled;
    UINT32              mmcXmtInterruptsEnabled;
    UINT32              dmaInterruptsEnabled;

    int			stmMaxMtu;
    } STM32F_DRV_CTRL;

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbStmEndh */
