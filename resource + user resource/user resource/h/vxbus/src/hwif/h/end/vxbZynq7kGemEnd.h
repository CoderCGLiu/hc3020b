/* vxbZynq7kGemEnd.h - Xilinx Zynq-7000 GEM VxBus End header file */

/*
 * Copyright (c) 2011-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
30nov15,m_w  added reset to the receiver when detecting the RX overrun
             or HRESP not OK error conditions.
             fixed hung issue when switch frequently between interrupt
             mode and poll mode. (VXW6-85040)
24mar14,xms  add gemExPhy to gem_drv_ctrl. (VXW6-70152)
09Jan13,fao  add ifCached to gem_drv_ctrl. (WIND00397943)
15jun12,fao  add clock define.
22jun11,rab  written.
*/

/*
DESCRIPTION
This module contains constants and defines for the Xilinx Zynq-7000 GEM End.
*/

#ifndef __INCvxbZynq7kGemEndh
#define __INCvxbZynq7kGemEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void zynq7kGemRegister (void);

#define GEM_1G_CLK             1 
#define GEM_10M_CLK            2 
#define GEM_100M_CLK           3 

#ifndef BSP_VERSION

/* Gem register offsets */

#define GEM_CTL      0x00    /* Network Control Register              */
#define GEM_CFG      0x04    /* Network Configuration Register        */
#define GEM_SR       0x08    /* Network Status Register               */
#define GEM_USRIO    0x0C    /* User IO                               */
#define GEM_DCFG     0x10    /* DMA Configuration                     */
#define GEM_TSR      0x14    /* Transmit Status Register              */
#define GEM_RBQP     0x18    /* Receive Buffer Queue Pointer          */
#define GEM_TBQP     0x1C    /* Transmit Address Register             */
#define GEM_RSR      0x20    /* Receive Status Register               */
#define GEM_ISR      0x24    /* Interrupt Status Register             */
#define GEM_IER      0x28    /* Interrupt Enable Register             */
#define GEM_IDR      0x2C    /* Interrupt Disable Register            */
#define GEM_IMR      0x30    /* Interrupt Mask Register               */
#define GEM_MAN      0x34    /* PHY Maintenance Register              */

#define GEM_HSL      0x80    /* Hash Address Low[31:0]                */
#define GEM_HSH      0x84    /* Hash Address High[63:32]              */
#define GEM_SA1L     0x88    /* Specific Address 1 Low, First 4 bytes */
#define GEM_SA1H     0x8C    /* Specific Address 1 High, Last 2 bytes */
#define GEM_SA2L     0x90    /* Specific Address 2 Low, First 4 bytes */
#define GEM_SA2H     0x94    /* Specific Address 2 High, Last 2 bytes */
#define GEM_SA3L     0x98    /* Specific Address 3 Low, First 4 bytes */
#define GEM_SA3H     0x9C    /* Specific Address 3 High, Last 2 bytes */
#define GEM_SA4L     0xA0    /* Specific Address 4 Low, First 4 bytes */
#define GEM_SA4H     0xA4    /* Specific Address 4 High, Last 2 bytes */
#define GEM_TID1     0xA8    /* Type ID Checking Register             */
#define GEM_TID2     0xAC    /* Type ID Checking Register             */
#define GEM_TID3     0xB0    /* Type ID Checking Register             */
#define GEM_TID4     0xB4    /* Type ID Checking Register             */
#define GEM_WAKE     0xB8    /* Wake on LAN                           */
#define GEM_SAML     0xC8    /* Specific Address Mask Low, 4 bytes    */
#define GEM_SAMH     0xCC    /* Specific Address Mask High, 2 bytes   */

#define GEM_FRA      0x108   /* Frames Transmitted OK Register        */
#define GEM_TUND     0x134   /* Transmit Underrun Register            */
#define GEM_SCOL     0x138   /* Single Collision Frame Register       */
#define GEM_MCOL     0x13C   /* Multiple Collision Frame Register     */
#define GEM_ECOL     0x140   /* Excessive Collision Register          */
#define GEM_LCOL     0x144   /* Late Collision Register               */
#define GEM_DTE      0x148   /* Deferred Transmission Frame Register  */
#define GEM_CSE      0x14C   /* Carrier Sense Error Register          */
#define GEM_OK       0x158   /* Frames Received OK Register           */
#define GEM_USF      0x184   /* Undersize Frames Error Register       */
#define GEM_RJA      0x18C   /* Receive Jabbers Error Register        */
#define GEM_SEQE     0x190   /* Frame Check Sequence Error Register   */
#define GEM_ELE      0x194   /* Excessive Lenght Error Register       */
#define GEM_RSE      0x198   /* Receive Symbol Error Register         */
#define GEM_ALE      0x19C   /* Alignment Error Register              */
#define GEM_RRE      0x1A0   /* Receive Resource  Error Register      */
#define GEM_ROV      0x1A4   /* Receive Overrun Error Register        */

#define FIRST_FRAME_IN_PACKAGE   0
#define COMPOSE_FRAME_TO_PACKAGE 1

/* rx/tx buffer descriptors definitions */

#define CL_OVERHEAD             4          /* prepended cluster overhead */
#define CL_ALIGNMENT            4          /* cluster required alignment */
#define MBLK_ALIGNMENT          4          /* mBlks required alignment */
#define GEM_END_BD_ALIGN        0x40       /* required alignment for RBDs */
#define TX_UNDERRUN             0x10000000 /* transmite under run */

/* Bit assignments for Receive Buffer Descriptor */

/* Address - Word 0 */

#define RXBUF_ADD_BASE_MASK         0xfffffffc /* Base address */
#define RXBUF_ADD_WRAP              0x00000002 /* The last buffer in the ring */
#define RXBUF_ADD_OWNED             0x00000001 /* SW or MAC owns the pointer */

/* Status - Word 1 */

#define RXBUF_STAT_BCAST            0x80000000 /* Global broadcast address */
#define RXBUF_STAT_MULTI            0x40000000 /* Multicast hash match*/
#define RXBUF_STAT_UNI              0x20000000 /* Unicast hash match */
#define RXBUF_STAT_EXT              0x10000000 /* External address */
#define RXBUF_STAT_UNK              0x08000000 /* Unknown source address */
#define RXBUF_STAT_LOC1             0x04000000 /* Specific address 1 match */
#define RXBUF_STAT_LOC2             0x02000000 /* Specific address 2 match */
#define RXBUF_STAT_LOC3             0x01000000 /* Specific address 3 match */
#define RXBUF_STAT_CHECKSUM_BIT1    0x00800000 /* checksum indicate 1 */
#define RXBUF_STAT_CHECKSUM_BIT0    0x00400000 /* checksum indicate 0 */
#define RXBUF_STAT_VLAN             0x00200000 /* VLAN Detected */
#define RXBUF_STAT_PRIOR            0x00100000 /* Priority Tag Detected */
#define RXBUF_STAT_PRIOR_MASK       0x000e0000 /* VLAN Priority Field */
#define RXBUF_STAT_CFI              0x00010000 /* Concatenation indicator */
#define RXBUF_STAT_EOF              0x00008000 /* Buffer is end of frame */
#define RXBUF_STAT_SOF              0x00004000 /* Buffer is start of frame */
#define RXBUF_STAT_FCS              0x00002000 /* Bad FCS */
#define RXBUF_STAT_LEN_MASK         0x00001fff /* Length of frame */

/* Bit assignments for Transmit Buffer Descriptor */

/* Status - Word 1 */

#define TXBUF_STAT_USED             0x80000000 /* Successfully transmited */
#define TXBUF_STAT_WRAP             0x40000000 /* Wrap (last buffer in list) */
#define TXBUF_STAT_RETRY_LIMIT      0x20000000
#define TXBUF_STAT_UNDERRUN         0x10000000 /* Transmit Underrun */
#define TXBUF_STAT_BUFF_EXHAUSTED   0x08000000
#define TXBUF_STAT_LATE_COLLISION   0x04000000
#define TXBUF_STAT_CKSUM_ERR_MASK   0x00700000
#define TXBUF_STAT_NO_CRC           0x00010000 /* Do not transmit CRC */
#define TXBUF_STAT_LAST_BUFF        0x00008000
#define TXBUF_STAT_LEN_MASK         0x00003fff /* Length of frame */

/* GEM register definitions */

/* Control Register, GEM_CTL, Offset 0x0  */

#define GEM_CTL_LB             0x00000001     /* Set Loopback output signal */
#define GEM_CTL_LBL            0x00000002     /* Loopback local.*/
#define GEM_CTL_RE             0x00000004     /* Receive enable.*/
#define GEM_CTL_TE             0x00000008     /* Transmit enable.*/
#define GEM_CTL_MPE            0x00000010     /* Management port enable.*/
#define GEM_CTL_CSR            0x00000020     /* Clear statistics registers.*/
#define GEM_CTL_ISR            0x00000040     /* Open statistics registers */
#define GEM_CTL_WES            0x00000080     /* Enable statistics registers */
#define GEM_CTL_BP             0x00000100     /* Force collision */
#define GEM_CTL_TSTART         0x00000200     /* TX Start */
#define GEM_CTL_THALT          0x00000400     /* TX Stop */
#define GEM_CTL_TPAU           0x00000800     /* TX Pause Frame */
#define GEM_CTL_TZQPAU         0x00001000     /* TX Pause ZQ Frame */
#define GEM_CTL_RXSTMP         0x00008000     /* RX Timestamp */
#define GEM_CTL_PFCEN          0x00010000     /* PFC Enable */

/* Configuration Register, GEM_CFG, Offset 0x4 */

#define GEM_CFG_SPD            0x00000001    /* 10/100 Speed */
#define GEM_CFG_FD             0x00000002    /* Full duplex.*/
#define GEM_CFG_JFRAME         0x00000008    /* Jumbo Frames UNSUPPORTED */
#define GEM_CFG_CAF            0x00000010    /* Accept all frames */
#define GEM_CFG_NBC            0x00000020    /* No recept broadcast frames */
#define GEM_CFG_MTI            0x00000040    /* Multicast hash enable */
#define GEM_CFG_UNI            0x00000080    /* Unicast hash enable. */
#define GEM_CFG_BIG            0x00000100    /* Open 802.3 1522 byte frames */
#define GEM_CFG_GIG            0x00000400    /* Gigabit mode enable */
#define GEM_CFG_RTY            0x00001000    /* Retry Test Mode */
#define GEM_CFG_PAE            0x00002000    /* Pause Enable */
#define GEM_CFG_RBOF_0         0x00000000    /* No offset */
#define GEM_CFG_RBOF_1         0x00004000    /* One Byte offset */
#define GEM_CFG_RBOF_2         0x00008000    /* Two Byte offset */
#define GEM_CFG_RBOF_3         0x0000c000    /* Three Byte offset */
#define GEM_CFG_RLCE           0x00010000    /* Checking Enable */
#define GEM_CFG_DRFCS          0x00020000    /* Discard Receive FCS */
#define GEM_CFG_CLK_8          0x00000000    /* CLK divided by 8 */
#define GEM_CFG_CLK_16         0x00040000    /* CLK divided by 16 */
#define GEM_CFG_CLK_32         0x00080000    /* CLK divided by 32 */
#define GEM_CFG_CLK_48         0x000C0000    /* CLK divided by 48 */
#define GEM_CFG_CLK_64         0x00100000    /* CLK divided by 64 */
#define GEM_CFG_CLK_96         0x00140000    /* CLK divided by 96 */
#define GEM_CFG_CLK_128        0x00180000    /* CLK divided by 128 */
#define GEM_CFG_CLK_224        0x001C0000    /* CLK divided by 224 */
#define GEM_CFG_DBUS_32        0x00000000    /* 32 bit Data Bus Width */
#define GEM_CFG_DBUS_64        0x00200000    /* 64 bit Data Bus Width */
#define GEM_CFG_DBUS_128       0x00400000    /* 128 bit Data Bus Width */
#define GEM_CFG_CAFDIS         0x00800000    /* Disable Copy Paused Frames */
#define GEM_CFG_RXOL           0x01000000    /* RX Checksum Offload */
#define GEM_CFG_EFRHD          0x02000000    /* Allow Half Duplex RX */
#define GEM_CFG_IRXFCS         0x04000000    /* Ignore RX FCS */
#define GEM_CFG_SGMII          0x08000000    /* SGMII Enable */
#define GEM_CFG_IPGST          0x10000000    /* IPG Stretch Enable */
#define GEM_CFG_BDPRE          0x20000000    /* RX Bad Preamble */
#define GEM_CFG_IPGIG          0x40000000    /* Ignore IPG RX Error */
#define GEM_CFG_UNIDIR         0x80000000    /* Uni-direction Enable */

/* Status Register, GEM_SR, Offset 0x8 */

#define GEM_LINK               0x00000001    /* Link pin */
#define GEM_MDIO               0x00000002    /* Real Time state of MDIO pin */
#define GEM_IDLE               0x00000004    /* Logic is idle or running */
#define GEM_PCSDUP             0x00000008    /* PCS Auto-neg duplex */
#define GEM_PCSRX              0x00000010    /* PCS Auto-neg pause RX */
#define GEM_PCSTX              0x00000020    /* PCS Auto-neg pause TX */
#define GEM_PCSPRI             0x00000040    /* PCS Auto-neg priority based  */

/* DMA Configuration Register, GEM_DCFG, Offset 0x10 */

#define GEM_DMA_BLEN_1         0x00000001    /* Single AHB Burst */
#define GEM_DMA_BLEN_2         0x00000002    /* Single AHB Burst */
#define GEM_DMA_BLEN_4         0x00000004    /* INCR4 AHB Burst */
#define GEM_DMA_BLEN_8         0x00000008    /* INCR8 AHB Burst */
#define GEM_DMA_BLEN_16        0x00000010    /* INCR16 AHB Burst */
#define GEM_DMA_SWAP           0x00000040    /* Mngmt Endian Swap mode */
#define GEM_DMA_ENDIAN         0x00000080    /* Packet Endian Swap mode */
#define GEM_DMA_RBUF           0x00000300    /* RX Packet Buffer Size */
#define GEM_DMA_TBUF           0x00000400    /* TX Packet Buffer Size */
#define GEM_DMA_TCKSUM         0x00000800    /* TX Checksum Offload Enable */
#define GEM_DMA_RDMABUF        0x00FF0000    /* RX DMA Buffer Size mask */
#define GEM_DMA_RDMALEN        0x00180000    /* RX DMA Buffer Size=1536 */
#define GEM_DMA_RDISC          0x01000000    /* RX Discard Pkts if no AHB */

/* Transmit Status Register, GEM_TSR, Offset 0x14 */

#define GEM_TSR_OVR            0x00000001    /* Transmit buffer overrun */
#define GEM_TSR_COL            0x00000002    /* Collision occured */
#define GEM_TSR_RLE            0x00000004    /* Retry lmimt exceeded */
#define GEM_TSR_TXGO           0x00000008    /* Transmitter is idle */
#define GEM_TSR_BNQ            0x00000010    /* Transmit buffer not queued */
#define GEM_TSR_COMP           0x00000020    /* Transmit complete */
#define GEM_TSR_UND            0x00000040    /* Transmit underrun */
#define GEM_TSR_LCOL           0x00000080    /* Transmit late collision */
#define GEM_TSR_HRSP           0x00000100    /* Transmit HRESP not Ok */

/* Receive Status Register, GEM_RSR, Offset 0x20  */

#define GEM_RSR_BNA            0x00000001    /* Buffer not available */
#define GEM_RSR_REC            0x00000002    /* Frame received */
#define GEM_RSR_OVR            0x00000004    /* Receive overrun */
#define GEM_RSR_HRSP           0x00000008    /* Receive HRESP not Ok */

/*
 * Interrupt Status Register, GEM_ISR, Offsen 0x24
 * Interrupt Enable Register, GEM_IER, Offset 0x28
 * Interrupt Disable Register, GEM_IDR, Offset 0x2c
 * Interrupt Mask Register, GEM_IMR, Offset 0x30
 */

#define GEM_INT_DONE           0x00000001    /* Phy management done */
#define GEM_INT_RCOM           0x00000002    /* Receive complete */
#define GEM_INT_RBNA           0x00000004    /* Receive buffer not available */
#define GEM_INT_TBNA           0x00000008
#define GEM_INT_TUND           0x00000010    /* Transmit buffer underrun */
#define GEM_INT_RTRY           0x00000020    /* Transmit Retry limt */
#define GEM_INT_TBRE           0x00000040
#define GEM_INT_TCOM           0x00000080    /* Transmit complete */
#define GEM_INT_LCHG           0x00000200    /* Link Change */
#define GEM_INT_ROVR           0x00000400    /* Receive overrun */
#define GEM_INT_ABT            0x00000800    /* Abort on DMA transfer */
#define GEM_INT_PFRRX          0x00001000    /* Pause Non-zero Quantum RX */
#define GEM_INT_PTZ            0x00002000    /* Pause Time zero */
#define GEM_INT_PTX            0x00004000    /* Pause Frame TX */
#define GEM_INT_ETX            0x00008000    /* External Interrupt */
#define GEM_INT_ANEG           0x00010000    /* Auto-neg complete */
#define GEM_INT_PRX            0x00020000    /* Link Partner RX */
#define GEM_INT_RDEL           0x00040000    /* PTP RX Delay */
#define GEM_INT_RSYNC          0x00080000    /* PTP RX Sync */
#define GEM_INT_TRQDEL         0x00100000    /* PTP Delay Req TX */
#define GEM_INT_TSYNC          0x00200000    /* PTP Sync Frame TX */
#define GEM_INT_RPDEL_REQ      0x00400000    /* PTP RX PDelay request */
#define GEM_INT_RPDEL_RSP      0x00800000    /* PTP RX PDelay response */
#define GEM_INT_TPDEL_REQ      0x01000000    /* PTP TX PDelay request */
#define GEM_INT_TPDEL_RSP      0x02000000    /* PTP TX PDelay response */
#define GEM_INT_TSU            0x04000000    /* TSU incriment */

/* PHY Maintenance Register, GEM_MAN, Offset 0x34 */

#define GEM_MAN_DATA(x)        ((x & 0xFFFF) <<  0) /* PHY data register */
#define GEM_MAN_CODE           (0x2 << 16)          /* IEEE Code */
#define GEM_MAN_REGA(x)        ((x & 0x1F) << 18)   /* PHY register address */
#define GEM_MAN_PHYA(x)        ((x & 0x1F) << 23)   /* PHY address */
#define GEM_MAN_WRITE          (0x1 << 28)          /* Transfer is a write */
#define GEM_MAN_READ           (0x2 << 28)          /* Transfer is a read */
#define GEM_MAN_SOF             0x40000000          /* Must be set RAB?? */

#define GEM_MTU                1500
#define GEM_JUMBO_MTU          9000
#define GEM_CLSIZE             1536
#define GEM_NAME               "gem"
#define GEM_TIMEOUT            10000

#define GEM_INTRS              (GEM_INT_RCOM | GEM_INT_RBNA | GEM_INT_TUND |\
                                GEM_INT_RTRY | GEM_INT_TCOM | GEM_INT_ROVR |\
                                GEM_INT_ABT  | GEM_INT_TBRE)
#define GEM_RX_INTRS           (GEM_INT_RCOM | GEM_INT_RBNA | GEM_INT_ROVR |\
                                GEM_INT_ABT)
#define GEM_TX_INTRS           (GEM_INT_TUND | GEM_INT_RTRY | GEM_INT_TCOM |\
                                GEM_INT_TBRE | GEM_INT_ABT)

#define GEM_TUPLE_CNT          1152
#define GEM_ADJ(x)             (x)->m_data += 2

#define GEM_INC_DESC(x, y)     (x) = (((x) + 1) % y)
#define GEM_MAXFRAG            32
#define GEM_MAX_RX             32

#define GEM_RX_DESC_CNT        192
#define GEM_TX_DESC_CNT        192

#define GEM_RX_BUFF_SIZE       1536

/* CRC for logical address filter */

#define GEM_CRC_TO_FILTER_INDEX(crc)   ((crc) >> 26)   /* get 6 MSBits */

typedef struct gem_desc
    {
    volatile UINT32    bdAddr;
    volatile UINT32    bdSts;
    } GEM_DESC;

/*
 * Private adapter context structure.
 */

typedef struct gem_drv_ctrl
    {
    END_OBJ          gemEndObj;
    VXB_DEVICE_ID    gemDev;
    void *           gemBar;
    void *           gemHandle;
    FUNCPTR          clkSetup;
    FUNCPTR          ifCached;
    void *           gemMuxDevCookie;

    JOB_QUEUE_ID     gemJobQueue;
    QJOB             gemIntJob;
    atomicVal_t      gemIntPending;
    QJOB             gemRxJob;
    atomicVal_t      gemRxPending;
    QJOB             gemTxJob;
    atomicVal_t      gemTxPending;

    BOOL             gemPolling;
    M_BLK_ID         gemPollBuf;
    UINT32           gemIntrs;
    UINT8            gemAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES gemCaps;
    END_IFDRVCONF    gemEndStatsConf;
    END_IFCOUNTERS   gemEndStatsCounters;

    UINT32           gemInErrors;
    UINT32           gemInDiscards;
    UINT32           gemInUcasts;
    UINT32           gemInMcasts;
    UINT32           gemInBcasts;
    UINT32           gemInOctets;
    UINT32           gemOutErrors;
    UINT32           gemOutUcasts;
    UINT32           gemOutMcasts;
    UINT32           gemOutBcasts;
    UINT32           gemOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *  gemMediaList;
    END_ERR          gemLastError;
    UINT32           gemCurMedia;
    UINT32           gemCurStatus;
    VXB_DEVICE_ID    gemMiiBus;
    VXB_DEVICE_ID    gemMiiDev;
    FUNCPTR          gemMiiPhyRead;
    FUNCPTR          gemMiiPhyWrite;
    int              gemMiiPhyAddr;

    /* End MII/ifmedia required fields */

   /* DMA tags and maps. */

    VXB_DMA_TAG_ID   gemParentTag;
    VXB_DMA_TAG_ID   gemRxDescTag;
    VXB_DMA_MAP_ID   gemRxDescMap;
    GEM_DESC *       gemRxDescMem;
    VXB_DMA_TAG_ID   gemTxDescTag;
    VXB_DMA_MAP_ID   gemTxDescMap;
    GEM_DESC *       gemTxDescMem;
    VXB_DMA_TAG_ID   gemMblkTag;
    VXB_DMA_MAP_ID   gemTxMblkMap[GEM_TX_DESC_CNT];
    M_BLK_ID         gemTxMblk[GEM_TX_DESC_CNT];
    VXB_DMA_MAP_ID   gemRxMblkMap[GEM_RX_DESC_CNT];
    M_BLK_ID         gemRxMblk[GEM_RX_DESC_CNT];

    UINT32           gemTxProd;
    UINT32           gemTxCons;
    UINT32           gemTxFree;
    UINT32           gemRxIdx;
    UINT32           gemTxLast;
    UINT32           gemTxStall;
    UINT32           gemRxStall;

    SEM_ID           gemDevSem;
    int              gemMaxMtu;
	BOOL			 gemExPhy; /* if true,  indicates it's using external PHY */
    } GEM_DRV_CTRL;

/* GEM control module register low level access routines */

#define GEM_BAR(p)        ((GEM_DRV_CTRL *)(p)->pDrvCtrl)->gemBar
#define GEM_HANDLE(p)     ((GEM_DRV_CTRL *)(p)->pDrvCtrl)->gemHandle

#define CSR_READ_4(pDev, addr)            \
        vxbRead32(GEM_HANDLE(pDev),       \
                 (UINT32 *)((char *)GEM_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)     \
        vxbWrite32(GEM_HANDLE(pDev),      \
                  (UINT32 *)((char *)GEM_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbZynq7kGemEndh */
