/* vxbTgecEnd.h - header file for Freescale QorIQ TGEC VxBus END driver */

/*
 * Copyright (C) 2009-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,07mar11,kxb  NAP 2.0 merge: Change tx/rx/comp queue id's
01e,15jun10,wap  Document the MAC loopback bit
01d,11jun10,wap  Add support for fiber media on Freescale XAUI cards
01c,31mar10,wap  Clean up compiler warnings
01b,17nov09,wap  Handle 36-bit addressing
01a,02sep09,wap  written
*/

#ifndef __INCvxbTgecEndh
#define __INCvxbTgecEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbTgecEndRegister (void);

#ifndef BSP_VERSION

#define TGEC_ID			0x000	/* Controller ID/vers */
#define TGEC_CMDCFG		0x008	/* Command/config */
#define TGEC_MACADDR0_LO	0x00C	/* Lower 32 bits of station address */
#define TGEC_MACADDR0_HI	0x010	/* Upper 16 bits of station address */
#define TGEC_MAXFRM		0x014	/* Max frame size */
#define TGEC_PAUSEQUANT		0x018	/* Pause quanta */
#define TGEC_HASHCTL		0x02C	/* Hash table control */
#define TGEC_MACSTS		0x040	/* MAC status register */
#define TGEC_TX_IPGLEN		0x044	/* TX interframe gap */
#define TGEC_MACADDR1_LO	0x048	/* Lower 32 bits of station address */
#define TGEC_MACADDR1_HI	0x04C	/* Upper 16 bits of station address */
#define TGEC_IMASK		0x060	/* Interrupt mask register */
#define TGEC_IEVENT		0x064	/* Interrupt event register */

/* Statistics registers */

#define TGEC_TFRM_U		0x080
#define TGEC_TFRM_L		0x084
#define TGEC_RFRM_U		0x088
#define TGEC_RFRM_L		0x08C
#define TGEC_RFCS_U		0x090
#define TGEC_RFCS_L		0x094
#define TGEC_RALN_U		0x098
#define TGEC_RALN_L		0x09C
#define TGEC_TXPF_U		0x0A0
#define TGEC_TXPF_L		0x0A4
#define TGEC_RXPF_U		0x0A8
#define TGEC_RXPF_L		0x0AC
#define TGEC_RLONG_U		0x0B0
#define TGEC_RLONG_L		0x0B4
#define TGEC_RFLR_U		0x0B8
#define TGEC_RFLR_L		0x0BC
#define TGEC_TVLAN_U		0x0C0
#define TGEC_TVLAN_L		0x0C4
#define TGEC_RVLAN_U		0x0C8
#define TGEC_RVLAN_L		0x0CC
#define TGEC_TOCT_U		0x0D0
#define TGEC_TOCT_L		0x0D4
#define TGEC_ROCT_U		0x0D8
#define TGEC_ROCT_L		0x0DC
#define TGEC_RUCA_U		0x0E0
#define TGEC_RUCA_L		0x0E4
#define TGEC_RMCA_U		0x0E8
#define TGEC_RMCA_L		0x0EC
#define TGEC_RBCA_U		0x0F0
#define TGEC_RBCA_L		0x0F4
#define TGEC_TERR_U		0x0F8
#define TGEC_TERR_L		0x0FC
#define TGEC_TUCA_U		0x108
#define TGEC_TUCA_L		0x10C
#define TGEC_TMCA_U		0x110
#define TGEC_TMCA_L		0x114
#define TGEC_TBCA_U		0x118
#define TGEC_TBCA_L		0x11C
#define TGEC_RDRP_U		0x120
#define TGEC_RDRP_L		0x124
#define TGEC_REOCT_U		0x128
#define TGEC_REOCT_L		0x12C
#define TGEC_RPKT_U		0x130
#define TGEC_RPKT_L		0x134
#define TGEC_TRUND_U		0x138
#define TGEC_TRUND_L		0x13C
#define TGEC_R64_U		0x140
#define TGEC_R64_L		0x144
#define TGEC_R127_U		0x148
#define TGEC_R127_L		0x14C
#define TGEC_R255_U		0x150
#define TGEC_R255_L		0x154
#define TGEC_R511_U		0x158
#define TGEC_R511_L		0x15C
#define TGEC_R1023_U		0x160
#define TGEC_R1023_L		0x164
#define TGEC_R1518_U		0x168
#define TGEC_R1518_L		0x16C
#define TGEC_R1519X_U		0x170
#define TGEC_R1519X_L		0x174
#define TGEC_TROVR_U		0x178
#define TGEC_TROVR_L		0x17C
#define TGEC_TRJBR_U		0x180
#define TGEC_TRJBR_L		0x184
#define TGEC_TRFRG_U		0x188
#define TGEC_TRFRG_L		0x18C
#define TGEG_RERR_U		0x190
#define TGEG_RERR_L		0x194


/* Command and configuration register */

#define TGEC_CMDCFG_TSTAMP_EN		0x00100000 /* Enable 1588 timestamps */
#define TGEC_CMDCFG_MACSEL		0x00080000 /* MAC address select */
#define TGEC_CMDCFG_NO_LENCHK		0x00020000 /* Disable payload len chk */
#define TGEC_CMDCFG_TX_IDLE		0x00010000 /* Force idle generation */
#define TGEC_CMDCFG_RX_ERRDISC_EN	0x00004000 /* Discard error frames */
#define TGEC_CMDCFG_CMDFRM_EN		0x00002000 /* Accept all cmd frames */
#define TGEC_CMDCFG_STS_CLR		0x00001000 /* Clear stats registers */
#define TGEC_CMDCFG_MAC_LOOPBACK	0x00000400 /* MAC loopback enable */
#define TGEC_CMDCFG_TX_ADDRINS		0x00000200 /* TX MAC addr insertion */
#define TGEC_CMDCFG_PAUSE_IGNORE	0x00000100 /* Ignore pause frames */
#define TGEC_CMDCFG_PAUSE_FWD		0x00000080 /* Fwd pause frames to app */
#define TGEC_CMDCFG_PROMISC_EN		0x00000010 /* Promiscuous mode */
#define TGEC_CMDCFG_WANMODE		0x00000008 /* WAN mode */
#define TGEC_CMDCFG_RX_EN		0x00000002 /* RX enable */
#define TGEC_CMDCFG_TX_EN		0x00000001 /* TX enable */

/* IEVENT and IMASK Register definitions */

#define TGEC_IEVENT_MDIOSCAN		0x00010000 /* MDIO scan event */
#define TGEC_IEVENT_MDIOCMDDONE		0x00008000 /* MDIO command complete */
#define TGEC_IEVENT_RFAULT		0x00004000 /* Remote fault */
#define TGEC_IEVENT_LFAULT		0x00002000 /* Local fault */
#define TGEC_IEVENT_TX_ECC		0x00001000 /* TX ECC Error */
#define TGEC_IEVENT_TX_UFLOW		0x00000800 /* TX FIFO underflow */
#define TGEC_IEVENT_TX_OFLOW		0x00000400 /* TX FIFO overflow */
#define TGEC_IEVENT_TX_ERR		0x00000200 /* TX frame error */
#define TGEC_IEVENT_RX_OFLOW		0x00000100 /* RX FIFO overflow */
#define TGEC_IEVENT_RX_ECC		0x00000080 /* RX ECC error */
#define TGEC_IEVENT_RX_JAB		0x00000040 /* RX jabber error */
#define TGEC_IEVENT_RX_GIANT		0x00000020 /* RX oversized frame */
#define TGEC_IEVENT_RX_RUNT		0x00000010 /* RX runt frame */
#define TGEC_IEVENT_RX_FRAG		0x00000008 /* RX fragment */
#define TGEC_IEVENT_RX_LEN		0x00000004 /* RX payload length error */
#define TGEC_IEVENT_RX_CRC		0x00000002 /* RX CRC error */
#define TGEC_IEVENT_RX_ALIGN		0x00000001 /* RX alignment error */

/* Hash table control register */

#define TGEC_HASHCTL_MC_EN		0x00000200 /* Mulicast enable/disable */
#define TGEC_HASHCTL_OFFSET		0x000001FF /* Hash table offset */

/* MAC status register */

#define TGEC_MACSTS_PHY_STAT1		0x00000008 /* TX status busy/idle */
#define TGEC_MACSTS_PHY_STAT0		0x00000004 /* RX status busy/idle */


typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define tgecEndPoolTupleFree(x) netMblkClChainFree(x)
#define tgecEndPoolTupleGet(x)      \
	netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

#define TGEC_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))
#define TGEC_MAXFRAG		8
#define TGEC_MAX_UNITS		8
#define TGEC_MAX_RX		32
#define TGEC_ALIGN		2

#define TGEC_MTU	1500
#define TGEC_JUMBO_MTU	9000
#define TGEC_CLSIZE	1536 + FMAN_PKT_OFF
#define TGEC_JUMBO_CLSIZE	9036 + FMAN_PKT_OFF
#define TGEC_NAME	"tgec"
#define TGEC_TIMEOUT	100000
#define TGEC_INTRS	(TGEC_RXINTRS|TGEC_TXINTRS|TGEC_ERRINTRS)
#define TGEC_RXINTRS	(TGEC_IEVENT_RXC|TGEC_IEVENT_BABR)
#define TGEC_TXINTRS	(TGEC_IEVENT_TXC|TGEC_IEVENT_TXE)
#define TGEC_ERRINTRS	(TGEC_IEVENT_XFUN)
#define TGEC_LINKINTRS	0

#define TGEC_ETHER_ALIGN	(TGEC_ALIGN + FMAN_PKT_OFF)

/* Conform to IPFORWARDER layout */
#define TGEC_TX_FQID		0x20
#define TGEC_RX_FQID		0x40
#define TGEC_TXDONE_FQID	0x60
#define TGEC_RX_DESC_CNT	256
#define TGEC_TX_DESC_CNT	256


typedef struct tgec_tx_desc
    {
    QPORTAL_SGLIST	tgec_frag[TGEC_MAXFRAG];
    } TGEC_TX_DESC;

typedef UINT64 (*tgecVirtToPhysFunc)(void *);
typedef void * (*tgecPhysToVirtFunc)(UINT64);

/*
 * Private adapter context structure.
 */

typedef struct tgec_drv_ctrl
    {
    END_OBJ		tgecEndObj;
    VXB_DEVICE_ID	tgecDev;
    void *		tgecBar;
    void *		tgecHandle;
    void *		tgecMuxDevCookie;
    VXB_DEVICE_ID	tgecFman;
    BOOL		tgecFiler;
    UINT32		tgecMask;

    BPORTAL *		tgecBportal;
    QPORTAL *		tgecQportal;

    UINT8		tgecBpid;
    UINT32		tgecRxFqId;
    UINT32		tgecTxFqId;
    UINT32		tgecTxDoneFqId;

    UINT32		tgecRxCtx;
    UINT32		tgecTxDoneCtx;

    UINT32		tgecRxPort;
    UINT32		tgecTxPort;

    UINT32		tgecFmanChan;

    UINT32		tgecFmanUnit;
    UINT32		tgecNum;
    UINT32		tgecCpu;

    JOB_QUEUE_ID	tgecJobQueue;
    QJOB		tgecIntJob;
    atomic_t		tgecIntPending;

    BOOL		tgecPolling;
    M_BLK_ID		tgecPollBuf;
    UINT32		tgecIntMask;

    UINT8		tgecAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	tgecCaps;

    END_IFDRVCONF	tgecEndStatsConf;
    END_IFCOUNTERS	tgecEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*tgecMediaList;
    END_ERR		tgecLastError;
    UINT32		tgecCurMedia;
    UINT32		tgecCurStatus;
    VXB_DEVICE_ID	tgecMiiBus;
    VXB_DEVICE_ID	tgecMiiDev;
    FUNCPTR		tgecMiiPhyRead;
    FUNCPTR		tgecMiiPhyWrite;
    int			tgecMiiPhyAddr;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */

    VXB_DMA_TAG *	tgecParentTag;

    UINT32		tgecTxProd;
    UINT32		tgecTxCons;
    UINT32		tgecTxFree;
    volatile BOOL	tgecTxStall;
    TGEC_TX_DESC *	tgecTxDescMem;
    M_BLK_ID		tgecTxMblk[TGEC_TX_DESC_CNT];

    SEM_ID		tgecDevSem;
    spinlockIsrNd_t	tgecLock;

    int			tgecMaxMtu;

    tgecVirtToPhysFunc	tgecVirtToPhys;
    tgecPhysToVirtFunc	tgecPhysToVirt;

    BOOL		tgecFiber;
    } TGEC_DRV_CTRL;


#define TGEC_ADDR_LO(y)  ((UINT64)((UINT64) (y)) & 0xFFFFFFFF)
#define TGEC_ADDR_HI(y)  (((UINT64)((UINT64) (y)) >> 32) & 0xFFFFFFFF)

#define TGEC_VTOP(p, x) \
	p->tgecVirtToPhys == NULL ? (unsigned long)(x) : (p->tgecVirtToPhys)(x)

#define TGEC_PTOV(p, x) \
	p->tgecPhysToVirt == NULL ? (void *)(unsigned long)(x) : (p->tgecPhysToVirt)(x)


#define CSR_READ_4(pDev, addr)					\
	*(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)				\
	do {							\
	    volatile UINT32 *pReg =				\
		(UINT32 *)((UINT32)pDev->pRegBase[0] + addr);	\
	    *(pReg) = (UINT32)(data);				\
	    WRS_ASM("eieio");					\
	} while ((0))

#define CSR_SETBIT_4(pDev, offset, val)          \
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbTgecEndh */
