/* vxbDm9000aEnd.h - header file for DAVICOM DM9000A VxBus END driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,17nov08,z_l  written
*/

#ifndef __INCvxbDm9000aEndh
#define __INCvxbDm9000aEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void dmRegister (void);

#ifndef BSP_VERSION

#define DM_NCR              0x00
#define DM_NSR              0x01
#define DM_TCR              0x02
#define DM_RCR              0x05
#define DM_BPTR             0x08
#define DM_EPCR             0x0b
#define DM_EPAR             0x0c
#define DM_EPRRL            0x0d
#define DM_EPDRH            0x0e
#define DM_MAC_ADDR0        0x10
#define DM_MAC_ADDR1        0x11
#define DM_MAC_ADDR2        0x12
#define DM_MAC_ADDR3        0x13
#define DM_MAC_ADDR4        0x14
#define DM_MAC_ADDR5        0x15
#define DM_MULTIADDR        0x16
#define DM_GRP              0x1f
#define DM_TCR2             0x2d
#define DM_SMCR             0x2f
#define DM_MRCMDX           0xf0
#define DM_MRCMD            0xf2
#define DM_MRRL             0xf4
#define DM_MRRH             0xf5
#define DM_MWCMD            0xf8
#define DM_TXPLL            0xfc
#define DM_TXPLH            0xfd
#define DM_ISR              0xfe
#define DM_IMR              0xff
                            
#define DM_NSR_TX1END       0x04
#define DM_NSR_TX2END       0x08

#define DM_EPCR_PHY_READ    0x0c
#define DM_EPCR_PHY_WRITE   0x0a
#define DM_EPAR_PHY_ADR     0x40

#define DM_RCR_ALLMULTI     0x08
#define DM_RCR_PRMSC        0x02
#define DM_RCR_RXEN         0x31

#define DM_TCR_TXREQ        0x01

#define DM_NCR_RST          0x01

#define DM_IMR_DEFAULT      0x83

#define DM_ISR_PT           0x02
#define DM_ISR_PR           0x01

#define DM_PHY_BMCR         0
#define DM_PHY_ANAR         4

#define DM_PHY_BMCR_RST     0x8000

#define DM_PHY_ANAR_DEFAULT 0x01e1

#define DM_PKT_READY        0x01

#define DM_TX_CNT           2

#define DM_MAX_RX           4

#define DM_MTU              1500
#define DM_NAME             "dme"
#define DM_TIMEOUT          100000

#define DM_TX_INC(x, y)     (x) = (((x) + 1) % y)

/* Private adapter context structure. */

typedef struct dm_drv_ctrl
    {
    END_OBJ         dmEndObj;
    VXB_DEVICE_ID   dmDev;
    void *          dmMuxDevCookie;
    void *          dmBar;
    void *          dmHandle;
    JOB_QUEUE_ID    dmJobQueue;
    QJOB            dmIntJob;
    atomicVal_t     dmIntPending;

    QJOB            dmRxJob;
    atomicVal_t     dmRxPending;
    atomicVal_t     dmJobPending;

    QJOB            dmTxJob;
    atomicVal_t     dmTxPending;
    UINT8           dmTxCur;
    UINT8           dmTxLast;
    volatile BOOL   dmTxStall;
    UINT16          dmTxThresh;

    BOOL            dmPolling;
    M_BLK_ID        dmPollBuf;
    UINT8           dmIntMask;
    UINT8           dmIntrs;

    UINT8           dmAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    dmCaps;

    /* Begin MII/ifmedia required fields. */
    
    END_MEDIALIST * dmMediaList;
    END_ERR         dmLastError;
    UINT32          dmCurMedia;
    UINT32          dmCurStatus;
    VXB_DEVICE_ID   dmMiiBus;
    
    /* End MII/ifmedia required fields */

    M_BLK_ID        dmTxMblk[DM_TX_CNT];

    UINT32          dmTxProd;
    UINT32          dmTxCons;
    UINT32          dmTxFree;

    int             dmMaxMtu;

    SEM_ID          dmDevSem;
    int             dmTxLen;
    int             dmTxQCount;
    } DM_DRV_CTRL;

typedef struct _RX_DESC 
    { 
    UINT8           rxbyte; 
    UINT8           status; 
    UINT16          length; 
    }RX_DESC; 

typedef union{ 
    UINT8           buf[4]; 
    RX_DESC         desc; 
    } RX_T; 

#define DM_BAR(p)       ((DM_DRV_CTRL *)(p)->pDrvCtrl)->dmBar
#define DM_HANDLE(p)    ((DM_DRV_CTRL *)(p)->pDrvCtrl)->dmHandle

#define CSR_READ_2(pDev, addr)              \
    vxbRead16 (DM_HANDLE(pDev), (UINT16 *)((char *)DM_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)           \
    vxbWrite16 (DM_HANDLE(pDev), (UINT16 *)((char *)DM_BAR(pDev) + addr), data)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbDm9000aEndh */
