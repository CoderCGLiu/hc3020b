/* vxbAlteraTseEnd.h - Altera Triple Speed Ethernet VxBus END driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,21nov08,l_z  created
*/

#ifndef __INCvxbAlteraTseEndh
#define __INCvxbAlteraTseEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void alteraTseRegister (void);

#define ATSE_MEDIA_MII         0
#define ATSE_MEDIA_GMII        1
#define ATSE_MEDIA_RGMII       2
#define ATSE_MEDIA_SGMII       3

#ifndef BSP_VERSION

/* ATSE driver generic defines */

#define ATSE_NAME              "atse"
#define ATSE_MTU               1522
#define ATSE_JUMBO_MTU         9000
#define ATSE_CLSIZE            1536
#define ATSE_TIMEOUT           10000
#define ATSE_TUPLE_CNT         384
#define ATSE_ADJ(x)            ((x)->m_data += 2)
#define ATSE_ADJ_LEN           2

/* ATSE number of modules */

#define ATSE_MOD_NUM           3

/* ASTE register width */

#define ATSE_REG_WIDTH         4

/* ATSE register offsets (from regBase[0]) */

#define ATSE_REV            0x000 /* MegaCore function revision */
#define ATSE_SCRATCH        0x004 /* Scratch register */
#define ATSE_CMD_CFG        0x008 /* MAC command register */
#define ATSE_MAC_0          0x00c /* MAC address low */
#define ATSE_MAC_1          0x010 /* MAC address high */
#define ATSE_FRM_LEN        0x014 /* 14-bit maximum frame length in bytes */
#define ATSE_PAUSE_QUANT    0x018 /* 16-bit pause quanta */
#define ATSE_RX_SEC_EMP     0x01c /* variable-length receive FIFO section-empty threshold */
#define ATSE_RX_SEC_FUL     0x020 /* variable-length receive FIFO section-full threshold */
#define ATSE_TX_SEC_EMP     0x024 /* variable-length transmit FIFO section-empty threshold */
#define ATSE_TX_SEC_FUL     0x028 /* variable-length transmit FIFO section-full threshold */
#define ATSE_RX_ALM_EMP     0x02c /* variable-length receive FIFO almost-empty threshold */
#define ATSE_RX_ALM_FUL     0x030 /* variable-length receive FIFO almost-full threshold */
#define ATSE_TX_ALM_EMP     0x034 /* variable-length transmit FIFO almost-empty threshold */
#define ATSE_TX_ALM_FUL     0x038 /* variable-length transmit FIFO almost-full threshold */
#define ATSE_MDIO_ADDR0     0x03c /* MDIO address of PHY Device 0 */
#define ATSE_MDIO_ADDR1     0x040 /* MDIO address of PHY Device 1 */
#define ATSE_REG_STATUS     0x058 /* Register read access status */
#define ATSE_TX_IPG_LEN     0x05c /* TX ipg length */

#define ATSE_OCT_TRAN_OK    0x078 /* transmit octets bytes */
#define ATSE_OCT_RECV_OK    0x07c /* receive octets byes */
#define ATSE_FRM_RX_UCAST   0x090 /* receive unicast packet count */
#define ATSE_FRM_RX_MULTI   0x094 /* receive multicast packet count */
#define ATSE_FRM_RX_BROAD   0x098 /* receive broadcast packet count */
#define ATSE_FRM_TX_UCAST   0x0a0 /* transmit unicast packet count */
#define ATSE_FRM_TX_MULTI   0x0a4 /* transmit multicast packet count */
#define ATSE_FRM_TX_BROAD   0x0a8 /* transmit broadcast packet count */

#define ATSE_TX_CMD_STAT    0x0e8 /* Transmit FIFO control register */
#define ATSE_RX_CMD_STAT    0x0ec /* Receive FIFO control register */
#define ATSE_HASH_TABLE     0x100 /* Multicast address resolution table */
#define ATSE_PHY_DEV0_REG   0x200 /* Registers 0 to 31 within PHY device 0 */
#define ATSE_PHY_DEV1_REG   0x280 /* Registers 0 to 31 within PHY device 1 */

#define CMD_CFG_TX_EN           0x00000001 /* transmit enable bit */
#define CMD_CFG_RX_EN           0x00000002 /* receive enable bit */
#define CMD_CFG_XON_GEN         0x00000004 /* pause frame generate bit */
#define CMD_CFG_ETH_SPEED       0x00000008 /* speed select */
#define CMD_CFG_PROMIS_EN       0x00000010 /* promiscuous */
#define CMD_CFG_PAD_EN          0x00000020 /* pad enable */
#define CMD_CFG_CRC_FWD         0x00000040 /* CRC fowarding */
#define CMD_CFG_PAUSE_FWD       0x00000080 /* pause fowarding */
#define CMD_CFG_PAUSE_IGO       0x00001000 /* pause ignore */
#define CMD_CFG_SET_MAC         0x00002000 /* Set MAC address on transmit */
#define CMD_CFG_HD_ENA          0x00004000 /* enable half duplex */
#define CMD_CFG_EXCESS_COL      0x00008000 /* Excessive collision */
#define CMD_CFG_LATE_COL        0x00001000 /* Late collision condition */
#define CMD_CFG_SW_RESET        0x00002000 /* soft reset */
#define CMD_CFG_MHASH_SEL       0x00004000 /* Select multicast */
#define CMD_CFG_LOOP_ENA        0x00008000 /* Enables local loopback */
#define CMD_CFG_TX_ADR_SEL      0x00070000 /* Tx select mask */
#define CMD_CFG_MAGIC_EN        0x00080000 /* Enable magic packet detection */
#define CMD_CFG_SLEEP           0x00100000 /* Sleep enable */
#define CMD_CFG_WEAKUP          0x00200000 /* Wake up mode enable */
#define CMD_CFG_XOFF_GEN        0x00400000 /* Pause frame generate */
#define CMD_CFG_CTRL_FRM_EN     0x00800000 /* MAC control frame enable */
#define CMD_CFG_NO_LGTH_CHECK   0x01000000 /* payload length check disable */
#define CMD_CFG_ENA_10          0x02000000 /* 10Mbps enable */
#define CMD_CFG_RX_ERR_DISC     0x04000000 /* Rx error frame discard enalbe */
#define CMD_CFG_CNT_RESET       0x80000000 /* Clear the statistic */

#define REG_STATUS_READ_TIMEOUT 0x00000001 /* Read access timeout indication */

#define TX_CMD_STAT_OMIT_CRC    0x00020000 /* Generate CRC */
#define TX_CMD_STAT_SHIFT16     0x00040000 /* Removes the first two bytes */
#define RX_CMD_STAT_SHIFT16     0x02000000 /* Removes the first two bytes */

/* SGDMA register offsets (from regBase[1/2]) */

#define SGDMA_STATUS        0x000  /* status register */
#define SGDMA_CONTROL       0x010  /* control register */
#define SGDMA_NEXT_DES      0x020  /* next descriptor register */

#define SGDMA_CONTROL_IE_ERR              0x00000001 /* generate interrupt when occurs error */
#define SGDMA_CONTROL_IE_EOP              0x00000002 /* generate interrupt when encounter EOP */
#define SGDMA_CONTROL_IE_DES_COMP         0x00000004 /* each descriptor is processed */
#define SGDMA_CONTROL_IE_CHAIN_COMP       0x00000008 /* last descriptor is processed */
#define SGDMA_CONTROL_IE_GLOBAL           0x00000010 /* Global signal to enable all interrupts */
#define SGDMA_CONTROL_CTL_RUN             0x00000020 /* start process */
#define SGDMA_CONTROL_STOP_DMA_ER         0x00000040 /* stop DMA when occur error */
#define SGDMA_CONTROL_IE_MAX_DESC_PROC    0x00000080 /* interrupt after the number of descriptors specified by MAX_DESC_PROCESSED are processed.*/
#define SGDMA_CONTROL_MAX_DESC_PROCESSED  0x0000ff00 /* Specifies the number of descriptors to process */
#define SGDMA_CONTROL_SW_RESET            0x00010000 /* soft reset */
#define SGDMA_CONTROL_PARK                0x00020000 /* auto clear own bit */
#define SGDMA_CONTROL_POLL_ENABLE         0x00040000 /* hardware polling rx descriptor */
#define SGDMA_CONTROL_CLEAR_INTERRUPT     0x80000000 /* clear pending interrupt */
#define SGDMA_CONTROL_POLL_TIME(a)        ((a) << 18) /* RX polling timeout value */
#define SGDMA_INTRS                 (SGDMA_CONTROL_IE_DES_COMP   | \
                                     SGDMA_CONTROL_IE_CHAIN_COMP | \
                                     SGDMA_CONTROL_IE_GLOBAL)

#define SGDMA_STATUS_ERROR          0x00000001 /* tansfer error */
#define SGDMA_STATUS_EOP            0x00000002 /* EOP encounter */
#define SGDMA_STATUS_DESC_COMP      0x00000004 /* a descriptor is processed */
#define SGDMA_STATUS_CHAIN_COMP     0x00000008 /* all descriptors are processed */
#define SGDMA_STATUS_BUSY           0x00000010 /* descriptors are being processed */

#define DMA_DESC_GEN_EOP            0x00000001 /* generate EOP */
#define DMA_DESC_READ_FIX_ADR       0x00000002 /* read fixed address */
#define DMA_DESC_WRITE_FIX_ADR      0x00000004 /* write fixed address */
#define DMA_DESC_OWNED_BY_HW        0x00000080 /* owned by hardware flag */

#define DMA_DESC_STATUS_RX_ERR      0x3F       /* signal rx_err bit */

/* PCS register offsets (from ATSE_PHY_DEV0_REG of regBase[0])*/

#define PCS_CONTROL                 0x00       /* PCS Control Register */
#define PCS_IF_MODE                 0x50       /* Interface mode */

#define PCS_CONTROL_RST             0x8000     /* reset bit of control register */
#define PCS_SGMII_ENA               0x0001     /* SGMII mode or 1000BASE-X mode */
#define PCS_USE_SGMII_AN            0x0002     /* auto-negotiation */

/* packet buffer descriptors definitions */

#define ATSE_TX_DESC_CNT            128
#define ATSE_RX_DESC_CNT            128
#define ATSE_MAX_RX                 32
#define ATSE_INC_DESC(x, y)         (x) = (((x) + 1) % y)

typedef struct dmaDesc
    {
    volatile UINT32 source;
    volatile UINT32 reserve1;
    volatile UINT32 destination;
    volatile UINT32 reserve2;
    volatile UINT32 nextDescPtr;
    volatile UINT32 reserve3;
    volatile UINT16 reserve4;
    volatile UINT16 bytesToXfer;
    volatile UINT8  descCtl;
    volatile UINT8  descStatus;
    volatile UINT16 actualXfer;
    } DMA_DESC;

#define DMS_DESC_SIZE             (sizeof(DMA_DESC))

/* private adapter context structure */

typedef struct atseDrvCtrl
    {
    END_OBJ             atseEndObj;
    VXB_DEVICE_ID       atseDev;
    void *              atseMuxDevCookie;

    UINT32              atseIfMode;

    JOB_QUEUE_ID        atseJobQueue;
    QJOB                atseTxJob;
    volatile BOOL       atseTxPending;
    QJOB                atseRxJob;
    volatile BOOL       atseRxPending;

    volatile BOOL       atseTxStall;

    BOOL                atsePolling;
    M_BLK_ID            atsePollBuf;
    UINT32              atseIntrs;

    UINT8               atseAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    atseCaps;

    END_IFDRVCONF       atseEndStatsConf;
    END_IFCOUNTERS      atseEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST *     atseMediaList;
    END_ERR             atseLastError;
    UINT32              atseCurMedia;
    UINT32              atseCurStatus;
    VXB_DEVICE_ID       atseMiiBus;
    VXB_DEVICE_ID       atseMiiDev;
    FUNCPTR             atseMiiPhyRead;
    FUNCPTR             atseMiiPhyWrite;
    int                 atseMiiPhyAddr;

    /* end MII/ifmedia required fields */

    DMA_DESC *          atseTxDescMem;
    DMA_DESC *          atseRxDescMem;

    UINT32              atseTxProd;
    UINT32              atseTxCons;
    UINT32              atseTxFree;
    UINT32              atseRxIdx;

    SEM_ID              atseDevSem;

    int                 atseMaxMtu;

    void *              regBase[ATSE_MOD_NUM];
    void *              handle[ATSE_MOD_NUM];

    M_BLK_ID            atseRxMblk[ATSE_RX_DESC_CNT];
    M_BLK_ID            atseTxMblk[ATSE_TX_DESC_CNT];

    UINT32              atseDescMem;
    UINT32              atseMdcDiv;
    UINT32              atseMedia;
    } ATSE_DRV_CTRL;

/* ATSE control module register low level access routines */

#define ATSE_BAR(p)       ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->regBase[0]
#define ATSE_HANDLE(p)    ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->handle[0]

#define CSR0_READ_4(pDev, addr)             \
        vxbRead32(ATSE_HANDLE(pDev),      \
                  (UINT32 *)((char *)ATSE_BAR(pDev) + addr))

#define CSR0_WRITE_4(pDev, addr, data)      \
        vxbWrite32(ATSE_HANDLE(pDev),     \
                   (UINT32 *)((char *)ATSE_BAR(pDev) + addr), data)

#define CSR0_SETBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) | (val))

#define CSR0_CLRBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) & ~(val))

/* SGDMA TX module register low level access routines */

#define SGDMA_TX_BAR(p)       ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->regBase[1]
#define SGDMA_TX_HANDLE(p)    ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->handle[1]

#define CSR1_READ_4(pDev, addr)             \
        vxbRead32(SGDMA_TX_HANDLE(pDev),      \
                  (UINT32 *)((char *)SGDMA_TX_BAR(pDev) + addr))

#define CSR1_WRITE_4(pDev, addr, data)      \
        vxbWrite32(SGDMA_TX_HANDLE(pDev),     \
                   (UINT32 *)((char *)SGDMA_TX_BAR(pDev) + addr), data)

#define CSR1_SETBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) | (val))

#define CSR1_CLRBIT_4(pDev, offset, val)    \
        CSR1_WRITE_4(pDev, offset, CSR1_READ_4(pDev, offset) & ~(val))

/* SGDMA RX module register low level access routines */

#define SGDMA_RX_BAR(p)       ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->regBase[2]
#define SGDMA_RX_HANDLE(p)    ((ATSE_DRV_CTRL *)(p)->pDrvCtrl)->handle[2]

#define CSR2_READ_4(pDev, addr)             \
        vxbRead32(SGDMA_RX_HANDLE(pDev),      \
                  (UINT32 *)((char *)SGDMA_RX_BAR(pDev) + addr))

#define CSR2_WRITE_4(pDev, addr, data)      \
        vxbWrite32(SGDMA_RX_HANDLE(pDev),     \
                   (UINT32 *)((char *)SGDMA_RX_BAR(pDev) + addr), data)

#define CSR2_SETBIT_4(pDev, offset, val)    \
        CSR2_WRITE_4(pDev, offset, CSR2_READ_4(pDev, offset) | (val))

#define CSR2_CLRBIT_4(pDev, offset, val)    \
        CSR2_WRITE_4(pDev, offset, CSR2_READ_4(pDev, offset) & ~(val))


#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbAlteraTseEndh */

