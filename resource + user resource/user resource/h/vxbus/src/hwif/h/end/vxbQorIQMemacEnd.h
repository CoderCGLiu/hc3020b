/* vxbQorIQMemacEnd.h - header file for Freescale QorIQ mEMAC VxBus END driver */

/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
07jul14,wyt  Add XFI support (VXW6-83113)
01f,31jan13,wap  Update copyright
01e,18jan13,wap  Add SGMII management register definitions
01d,09jan13,wap  Add XAUI support
01c,11oct12,wap  Add SGMII support
01b,08oct12,wap  Fill in more register definitions
01a,13aug12,wap  written
*/

#ifndef __INCvxbQorIQMemacEndh
#define __INCvxbQorIQMemacEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbQorIQMemacEndRegister (void);

#define MEMAC_MEDIA_RMII        0x1
#define MEMAC_MEDIA_GMII        0x2
#define MEMAC_MEDIA_RGMII       0x3
#define MEMAC_MEDIA_SGMII       0x4
#define MEMAC_MEDIA_TBI         0x5
#define MEMAC_MEDIA_XAUI	0x6
#define MEMAC_MEDIA_XFI         0x7

#ifndef BSP_VERSION

#define MEMAC_CMDCFG		0x008	/* Command/config */
#define MEMAC_MACADDR0_LO	0x00C	/* Lower 32 bits of station address */
#define MEMAC_MACADDR0_HI	0x010	/* Upper 16 bits of station address */
#define MEMAC_MAXFRM		0x014	/* Max frame size */
#define MEMAC_HASHCTL		0x02C	/* Hash table control */
#define MEMAC_IEVENT		0x040	/* Interrupt event register */
#define MEMAC_TX_IPGLEN		0x044	/* TX interframe gap */
#define MEMAC_IMASK		0x04C	/* Interrupt mask register */
#define MEMAC_PAUSEQUANT_CL01	0x054	/* CL0,1 pause quanta register */
#define MEMAC_PAUSEQUANT_CL23	0x058	/* CL0,1 pause quanta register */
#define MEMAC_PAUSEQUANT_CL45	0x05C	/* CL0,1 pause quanta register */
#define MEMAC_PAUSEQUANT_CL67	0x060	/* CL0,1 pause quanta register */
#define MEMAC_PAUSETHRESH_CL01	0x064	/* CL0,1 pause threshold register */
#define MEMAC_PAUSETHRESH_CL23	0x068	/* CL0,1 pause threshold register */
#define MEMAC_PAUSETHRESH_CL45	0x06C	/* CL0,1 pause threshold register */
#define MEMAC_PAUSETHRESH_CL67	0x070	/* CL0,1 pause threshold register */
#define MEMAC_RXPAUSE_STS	0x074	/* RX pause status register */
#define MEMAC_MACADDR1_LO	0x080	/* Lower 32 bits of address 1 */
#define MEMAC_MACADDR1_HI	0x084	/* Upper 32 bits of address 1 */
#define MEMAC_MACADDR2_LO	0x088	/* Lower 32 bits of address 2 */
#define MEMAC_MACADDR2_HI	0x08C	/* Upper 32 bits of address 2 */
#define MEMAC_MACADDR3_LO	0x090	/* Lower 32 bits of address 3 */
#define MEMAC_MACADDR3_HI	0x094	/* Upper 32 bits of address 3 */
#define MEMAC_MACADDR4_LO	0x098	/* Lower 32 bits of address 4 */
#define MEMAC_MACADDR4_HI	0x09C	/* Upper 32 bits of address 4 */
#define MEMAC_MACADDR5_LO	0x0A0	/* Lower 32 bits of address 5 */
#define MEMAC_MACADDR5_HI	0x0A4	/* Upper 32 bits of address 5 */
#define MEMAC_MACADDR6_LO	0x0A8	/* Lower 32 bits of address 6 */
#define MEMAC_MACADDR6_HI	0x0AC	/* Upper 32 bits of address 6 */
#define MEMAC_MACADDR7_LO	0x0B0	/* Lower 32 bits of address 7 */
#define MEMAC_MACADDR7_HI	0x0B4	/* Upper 32 bits of address 7 */

/* Statistics registers */

#define MEMAC_REOCT_L		0x100
#define MEMAC_REOCT_U		0x104
#define MEMAC_ROCT_L		0x108
#define MEMAC_ROCT_U		0x10C
#define MEMAC_RALN_L		0x110
#define MEMAC_RALN_U		0x114
#define MEMAC_RXPF_L		0x118
#define MEMAC_RXPF_U		0x11C
#define MEMAC_RFRM_L		0x120
#define MEMAC_RFRM_U		0x124
#define MEMAC_RFCS_L		0x128
#define MEMAC_RFCS_U		0x12C
#define MEMAC_RVLAN_L		0x130
#define MEMAC_RVLAN_U		0x134
#define MEMAC_RERR_L		0x138
#define MEMAC_RERR_U		0x13C
#define MEMAC_RUCA_L		0x140
#define MEMAC_RUCA_U		0x144
#define MEMAC_RMCA_L		0x148
#define MEMAC_RMCA_U		0x14C
#define MEMAC_RBCA_L		0x150
#define MEMAC_RBCA_U		0x154
#define MEMAC_RDRP_L		0x158
#define MEMAC_RDRP_U		0x15C
#define MEMAC_RPKT_L		0x160
#define MEMAC_RPKT_U		0x164
#define MEMAC_TRUND_L		0x168
#define MEMAC_TRUND_U		0x16C
#define MEMAC_R64_L		0x170
#define MEMAC_R64_U		0x174
#define MEMAC_R127_L		0x178
#define MEMAC_R127_U		0x17C
#define MEMAC_R255_L		0x180
#define MEMAC_R255_U		0x184
#define MEMAC_R511_L		0x188
#define MEMAC_R511_U		0x18C
#define MEMAC_R1023_L		0x190
#define MEMAC_R1023_U		0x194
#define MEMAC_R1518_L		0x198
#define MEMAC_R1518_U		0x19C
#define MEMAC_R1519X_L		0x1A0
#define MEMAC_R1519X_U		0x1A4
#define MEMAC_ROVR_L		0x1A8
#define MEMAC_ROVR_U		0x1AC
#define MEMAC_RJBR_L		0x1B0
#define MEMAC_RJBR_U		0x1B4
#define MEMAC_RFRG_L		0x1B8
#define MEMAC_RFRG_U		0x1BC
#define MEMAC_RCNP_L		0x1C0
#define MEMAC_RCNP_U		0x1C4
#define MEMAC_RDRNTP_L		0x1C8
#define MEMAC_RDRNTP_U		0x1CC

#define MEMAC_TEOCT_L		0x200
#define MEMAC_TEOCT_U		0x204
#define MEMAC_TOCT_L		0x208
#define MEMAC_TOCT_U		0x20C
#define MEMAC_TXPF_L		0x218
#define MEMAC_TXPF_U		0x21C
#define MEMAC_TFRM_L		0x220
#define MEMAC_TFRM_U		0x224
#define MEMAC_TFCS_L		0x228
#define MEMAC_TFCS_U		0x22C
#define MEMAC_TVLAN_L		0x230
#define MEMAC_TVLAN_U		0x234
#define MEMAC_TERR_L		0x238
#define MEMAC_TERR_U		0x23C
#define MEMAC_TUCA_L		0x240
#define MEMAC_TUCA_U		0x244
#define MEMAC_TMCA_L		0x248
#define MEMAC_TMCA_U		0x24C
#define MEMAC_TBCA_L		0x250
#define MEMAC_TBCA_U		0x254
#define MEMAC_TPKT_L		0x260
#define MEMAC_TPKT_U		0x264
#define MEMAC_TUND_L		0x268
#define MEMAC_TUND_U		0x26C
#define MEMAC_T64_L		0x270
#define MEMAC_T64_U		0x274
#define MEMAC_T127_L		0x278
#define MEMAC_T127_U		0x27C
#define MEMAC_T255_L		0x280
#define MEMAC_T255_U		0x284
#define MEMAC_T511_L		0x288
#define MEMAC_T511_U		0x28C
#define MEMAC_T1023_L		0x290
#define MEMAC_T1023_U		0x294
#define MEMAC_T1518_L		0x298
#define MEMAC_T1518_U		0x29C
#define MEMAC_T1519X_L		0x2A0
#define MEMAC_T1519X_U		0x2A4
#define MEMAC_TCNP_L		0x2C0
#define MEMAC_TCNP_U		0x2CC

/* Line interface control */

#define MEMAC_IFMODE		0x300	/* interface mode control register */
#define MEMAC_IFSTS		0x304	/* interface status register */

/* Broadcom HiGig/2 registers */

#define MEMAC_HG_CFG		0x340	/* HiGig/2 control/config register */
#define MEMAC_HG_PAUSEQUANT	0x350
#define MEMAC_HG_PAUSETHRESH	0x360
#define MEMAC_HG_RX_PAUSESTS	0x370
#define MEMAC_HG_RX_FIFOSTS	0x374
#define MEMAC_HG_RHM		0x378	/* Rx HiGig/2 message count */
#define MEMAC_HG_THM		0x37C	/* Rx HiGig/2 message count */

/* Command and configuration register */

#define MEMAC_CMDCFG_MG_EN		0x80000000 /* Magic packet detection enable */
#define MEMAC_CMDCFG_RX_LOWPW		0x01000000 /* RX low power PCS assert */
#define MEMAC_CMDCFG_TX_LOWPW		0x00800000 /* TX low power idle enable */
#define MEMAC_CMDCFG_SFD_ANY		0x00200000 /* SFD check disable */
#define MEMAC_CMDCFG_PFC_EN		0x00080000 /* Priority flow control enable */
#define MEMAC_CMDCFG_NO_LENCHK		0x00020000 /* Disable payload len chk */
#define MEMAC_CMDCFG_TX_IDLE		0x00010000 /* Force idle generation */
#define MEMAC_CMDCFG_RX_ERRDISC_EN	0x00004000 /* Discard error frames */
#define MEMAC_CMDCFG_CMDFRM_EN		0x00002000 /* Accept all cmd frames */
#define MEMAC_CMDCFG_SW_RESET		0x00001000 /* Software reset */
#define MEMAC_CMDCFG_TXPAD_EN		0x00000800 /* TX autopad enable */
#define MEMAC_CMDCFG_MAC_LOOPBACK	0x00000400 /* MAC loopback enable */
#define MEMAC_CMDCFG_TX_ADDRINS		0x00000200 /* TX MAC addr insertion */
#define MEMAC_CMDCFG_PAUSE_IGNORE	0x00000100 /* Ignore pause frames */
#define MEMAC_CMDCFG_PAUSE_FWD		0x00000080 /* Fwd pause frames to app */
#define MEMAC_CMDCFG_CRCSTRIP_EN	0x00000040 /* RX CRC strip enable */
#define MEMAC_CMDCFG_PADSTRIP_EN	0x00000020 /* RX PAD strip enable */
#define MEMAC_CMDCFG_PROMISC_EN		0x00000010 /* Promiscuous mode */
#define MEMAC_CMDCFG_WANMODE		0x00000008 /* WAN mode */
#define MEMAC_CMDCFG_RX_EN		0x00000002 /* RX enable */
#define MEMAC_CMDCFG_TX_EN		0x00000001 /* TX enable */

/* IEVENT and IMASK Register definitions */

#define MEMAC_IEVENT_PCS		0x80000000 /* PCS event */
#define MEMAC_IEVENT_AN			0x40000000 /* autoneg event */
#define MEMAC_IEVENT_LT			0x20000000 /* link training event */
#define MEMAC_IEVENT_MGI		0x00004000 /* magic packet detected */
#define MEMAC_IEVENT_RX_OFLOW		0x00001000 /* RX FIFO overflow */
#define MEMAC_IEVENT_TX_UFLOW		0x00000800 /* TX FIFO underflow */
#define MEMAC_IEVENT_TX_OFLOW		0x00000400 /* TX FIFO overflow */
#define MEMAC_IEVENT_TX_ECC		0x00000200 /* TX frame error */
#define MEMAC_IEVENT_RX_ECC		0x00000100 /* RX ECC error */
#define MEMAC_IEVENT_LI_FAULT		0x00000080 /* Link interruption fault */
#define MEMAC_IEVENT_RX_FIFOEMPTY	0x00000040 /* RX FIFO empty */
#define MEMAC_IEVENT_TX_FIFOEMPTY	0x00000020 /* TX FIFO empty */
#define MEMAC_IEVENT_RX_LOWP		0x00000010 /* RX low power  */
#define MEMAC_IEVENT_PHY_LOS		0x00000004 /* PHY loss of signal */
#define MEMAC_IEVENT_REM_FAULT		0x00000002 /* Remove fault event */
#define MEMAC_IEVENT_LOC_FAULT		0x00000001 /* Local fault event */

/* Hash table control register */

#define MEMAC_HASHCTL_MC_EN		0x00000200 /* Mulicast enable/disable */
#define MEMAC_HASHCTL_OFFSET		0x000001FF /* Hash table offset */

/* IF mode control register */

#define MEMAC_IFMODE_EN_AUTO		0x00008000 /* Enable auto speed select */
#define MEMAC_IFMODE_SETSP		0x00006000 /* manual speed select */
#define MEMAC_IFMODE_MSG		0x00000200 /* Allow HiGig/2 messages */
#define MEMAC_IFMODE_HG			0x00000100 /* HigGig/2 enable */
#define MEMAC_IFMODE_RLP		0x00000020 /* Reduced pin internal loopback */
#define MEMAC_IFMODE_RM10		0x00000010 /* 0 == 10Mbps RMII, 1 == 100Mbps RMII */
#define MEMAC_IFMODE_RM			0x00000008 /* RMII mode (10/100) */
#define MEMAC_IFMODE_GM			0x00000004 /* RGMII Mode (1000) */
#define MEMAC_IFMODE_MODE		0x00000003 /* XGMII/GMII select */

#define MEMAC_SETSP_100MBPS		0x00000000
#define MEMAC_SETSP_10MBPS		0x00002000
#define MEMAC_SETSP_1000MBPS		0x00004000

#define MEMAC_MODE_XGMII		0x00000000
#define MEMAC_MODE_GMII			0x00000002

/* TBI registers */

#define MEMAC_TBICR		0x00	/* Control register */
#define MEMAC_TBISR		0x01	/* Status register */
#define MEMAC_TBIANAR		0x04	/* Autoneg advertisement */
#define MEMAC_TBILPAR		0x05	/* Link partner ability */
#define MEMAC_TBIANEX		0x06	/* Autoneg adv. expansion */
#define MEMAC_TBIANNP		0x07	/* AN next page */
#define MEMAC_TBIANLPNP		0x08	/* Link partner next page */
#define MEMAC_TBIEXST		0x0F	/* Extended status */
#define MEMAC_TBIJD		0x10	/* Jitter diagnostics */
#define MEMAC_TBICON		0x11	/* TBI control */

/*
 * Most TBI registers are defined in genericTbiPhy.h, but
 * the TBICON register is specific to the MEMAC hardware
 */

#define MEMAC_TBICON_RESET	0x8000  /* Soft reset */
#define MEMAC_TBICON_RXDIS_DIS	0x2000  /* Disable RX disparity */
#define MEMAC_TBICON_TXDIS_DIS	0x1000  /* Disable TX disparity */
#define MEMAC_TBICON_AN_SENSE	0x0100  /* 802.3z */
#define MEMAC_TBICON_CLKSEL	0x0020  /* 62.5Mhz/125Mhz clock */
#define MEMAC_TBICON_MIMODE	0x0010  /* TBI/GMII mode */

#define MEMAC_SGMII_CTL		0x00
#define MEMAC_SGMII_STS		0x01
#define MEMAC_SGMII_ID1		0x02
#define MEMAC_SGMII_ID2		0x03
#define MEMAC_SGMII_ANAR	0x04
#define MEMAC_SGMII_LPAR	0x05
#define MEMAC_SGMII_ANEXP	0x06
#define MEMAC_SGMII_NPTX	0x07
#define MEMAC_SGMII_NPRX	0x08
#define MEMAC_SGMII_ESTS	0x0F
#define MEMAC_SGMII_SCRATCH	0x10
#define MEMAC_SGMII_REV		0x11
#define MEMAC_SGMII_LT_L	0x12
#define MEMAC_SGMII_LT_U	0x13
#define MEMAC_SGMII_IFMODE	0x14

/* control register */

#define MEMAC_SGMIICTL_RST	0x8000
#define MEMAC_SGMIICTL_LPBK	0x4000
#define MEMAC_SGMIICTL_SPDSEL0	0x2000
#define MEMAC_SGMIICTL_AN_EN	0x1000
#define MEMAC_SGMIICTL_PD	0x0800
#define MEMAC_SGMIICTL_ISO	0x0400
#define MEMAC_SGMIICTL_ANRSTRT	0x0200
#define MEMAC_SGMIICTL_FD	0x0100
#define MEMAC_SGMIICTL_COLTEST	0x0080
#define MEMAC_SGMIICTL_SPDSEL1	0x0040

#define MEMAC_SGMIISPDSEL_1000	0x0040

/* SGMII ability register */

#define MEMAC_SGMIIANAR_ACK	0x4000
#define MEMAC_SGMIIANAR_SGMII	0x4001

/* SGMII IFMODE register */

#define MEMAC_SGMIIIFMODE_FD	0x0010
#define MEMAC_SGMIIIFMODE_SPD	0x000C
#define MEMAC_SGMIIIFMODE_AN	0x0002
#define MEMAC_SGMIIIFMODE_EN	0x0001

#define MEMAC_SGMIISPD_10	0x0000
#define MEMAC_SGMIISPD_100	0x0004
#define MEMAC_SGMIISPD_1000	0x0008


/*
 * This address can be changed by modifying one of the
 * SerDes control registers, but typically defaults to 0.
 */

#define MEMAC_SGMII_ADDR	0
#define MEMAC_SGMII_REQUEST	0xFF

typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define memacEndPoolTupleFree(x) netMblkClChainFree(x)
#define memacEndPoolTupleGet(x)      \
	netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

#define MEMAC_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))
#define MEMAC_MAXFRAG		8
#define MEMAC_MAX_UNITS		8
#define MEMAC_MAX_RX		32
#define MEMAC_ALIGN		2

#define MEMAC_MTU	1500
#define MEMAC_JUMBO_MTU	9000
#define MEMAC_CLSIZE	1536 + FMAN_PKT_OFF
#define MEMAC_JUMBO_CLSIZE	9036 + FMAN_PKT_OFF
#define MEMAC_NAME	"memac"
#define MEMAC_TIMEOUT	100000
#define MEMAC_INTRS	(MEMAC_RXINTRS|MEMAC_TXINTRS|MEMAC_ERRINTRS)
#define MEMAC_RXINTRS	(MEMAC_IEVENT_RXC|MEMAC_IEVENT_BABR)
#define MEMAC_TXINTRS	(MEMAC_IEVENT_TXC|MEMAC_IEVENT_TXE)
#define MEMAC_ERRINTRS	(MEMAC_IEVENT_XFUN)
#define MEMAC_LINKINTRS	0

#define MEMAC_ETHER_ALIGN	(MEMAC_ALIGN + FMAN_PKT_OFF)

/* Conform to IPFORWARDER layout */
#define MEMAC_TX_FQID		0x20
#define MEMAC_RX_FQID		0x40
#define MEMAC_TXDONE_FQID	0x60
#define MEMAC_RX_DESC_CNT	256
#define MEMAC_TX_DESC_CNT	256

typedef struct memac_tx_desc
    {
    QPORTAL_SGLIST	memac_frag[MEMAC_MAXFRAG];
    } MEMAC_TX_DESC;

typedef UINT64 (*memacVirtToPhysFunc)(void *);
typedef void * (*memacPhysToVirtFunc)(UINT64);

/*
 * Private adapter context structure.
 */

typedef struct memac_drv_ctrl
    {
    END_OBJ		memacEndObj;
    VXB_DEVICE_ID	memacDev;
    void *		memacBar;
    void *		memacHandle;
    void *		memacMuxDevCookie;
    VXB_DEVICE_ID	memacFman;
    BOOL		memacFiler;
    UINT32		memacMask;

    BPORTAL *		memacBportal;
    QPORTAL *		memacQportal;

    UINT8		memacBpid;
    UINT32		memacRxFqId;
    UINT32		memacTxFqId;
    UINT32		memacTxDoneFqId;

    UINT32		memacRxCtx;
    UINT32		memacTxDoneCtx;

    UINT32		memacRxPort;
    UINT32		memacTxPort;

    UINT32		memacFmanChan;

    UINT32		memacFmanUnit;
    UINT32		memacNum;
    UINT32		memacCpu;

    JOB_QUEUE_ID	memacJobQueue;
    QJOB		memacIntJob;
    atomic_t		memacIntPending;

    BOOL		memacPolling;
    M_BLK_ID		memacPollBuf;
    UINT32		memacIntMask;

    UINT8		memacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	memacCaps;

    END_IFDRVCONF	memacEndStatsConf;
    END_IFCOUNTERS	memacEndStatsCounters;

    int			memacMedia;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*memacMediaList;
    END_ERR		memacLastError;
    UINT32		memacCurMedia;
    UINT32		memacCurStatus;
    VXB_DEVICE_ID	memacMiiBus;
    VXB_DEVICE_ID	memacMiiDev;
    FUNCPTR		memacMiiPhyRead;
    FUNCPTR		memacMiiPhyWrite;
    int			memacMiiPhyAddr;
    /* End MII/ifmedia required fields */

    int			memacTbiAddr;

    /* DMA tags and maps. */

    VXB_DMA_TAG *	memacParentTag;

    UINT32		memacTxProd;
    UINT32		memacTxCons;
    UINT32		memacTxFree;
    volatile BOOL	memacTxStall;
    MEMAC_TX_DESC *	memacTxDescMem;
    M_BLK_ID		memacTxMblk[MEMAC_TX_DESC_CNT];

    SEM_ID		memacDevSem;
    spinlockIsrNd_t	memacLock;

    int			memacMaxMtu;

    memacVirtToPhysFunc	memacVirtToPhys;
    memacPhysToVirtFunc	memacPhysToVirt;

    /* RX error statistics */

    UINT32		memacRxDmeErr;
    UINT32		memacRxFpeErr;
    UINT32		memacRxFseErr;
    UINT32		memacRxDisErr;
    UINT32		memacRxEofErr;
    UINT32		memacRxNssErr;
    UINT32		memacRxIppErr;
    UINT32		memacRxPteErr;
    UINT32		memacRxIspErr;
    UINT32		memacRxPheErr;
      
    /* TX error statistics */

    UINT32		memacTxUfdErr;
    UINT32		memacTxLgeErr;
    UINT32		memacTxDmeErr;

    } MEMAC_DRV_CTRL;

#define memacSgmiiAddr memacTbiAddr

#define MEMAC_ADDR_LO(y)  ((UINT64)((UINT64) (y)) & 0xFFFFFFFF)
#define MEMAC_ADDR_HI(y)  (((UINT64)((UINT64) (y)) >> 32) & 0xFFFFFFFF)

#define MEMAC_VTOP(p, x) \
	p->memacVirtToPhys == NULL ? (unsigned long)(x) : (p->memacVirtToPhys)(x)

#define MEMAC_PTOV(p, x) \
	p->memacPhysToVirt == NULL ? (void *)(unsigned long)(x) : (p->memacPhysToVirt)(x)


#define CSR_READ_4(pDev, addr)					\
	*(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)				\
	do {							\
	    volatile UINT32 *pReg =				\
		(UINT32 *)((UINT32)pDev->pRegBase[0] + addr);	\
	    *(pReg) = (UINT32)(data);				\
	    WRS_ASM("eieio");					\
	} while ((0))

#define CSR_SETBIT_4(pDev, offset, val)          \
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define MEMAC_MDIO_OFFSET	0x1000

#define MII_READ_4(pDev, offset)		\
	CSR_READ_4(pDev, offset + MEMAC_MDIO_OFFSET)
#define MII_WRITE_4(pDev, offset, val)		\
	CSR_WRITE_4(pDev, offset + MEMAC_MDIO_OFFSET, val)
#define MII_SETBIT_4(pDev, offset, val)		\
	CSR_SETBIT_4(pDev, offset + MEMAC_MDIO_OFFSET, val)
#define MII_CLRBIT_4(pDev, offset, val)		\
	CSR_CLRBIT_4(pDev, offset + MEMAC_MDIO_OFFSET, val)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbQorIQMemacEndh */
