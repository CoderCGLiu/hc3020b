/* mvGmac.h - header file for Marvell GMAC controller */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,29jul06,wap  Correct serial mode register definition
01a,26jun06,wap  written
*/

/*
DESCRIPTION
This file contains register definitions for the Marvell GMAC MAC
found in Marvell Yukon I and Yukon II gigabit ethernet controllers.
The same MAC is used in both chip revs, so the register information
is broken out into a separate file for use with both drivers.

The GMAC's registers are mapped into the Yukon's register space.
Some of the Yukon II devices are dual link and will have two GMACs.
The first GMAC's register set is always located starting at offset
0x2800. For dual link devices, the second GMAC's register set
is located at 0x3800.
*/

#ifndef __INCmvGmach
#define __INCmvGmach

/* GMAC register base addresses */

#define GMAC_LINK0_OFFSET	0x2800
#define GMAC_LINK1_OFFSET	0x3800

/*
 * GMAC registers
 * All register offsets are relative to the GMAC's base address
 * within the Yukon's register space.
 */

#define GMAC_GPSR	0x0000
#define GMAC_GPCR	0x0004
#define GMAC_TXCTL	0x0008
#define GMAC_RXCTL	0x000C
#define GMAC_TXFLOW	0x0010
#define GMAC_TXPARM	0x0014
#define GMAC_SMR	0x0018
#define GMAC_SADDRA0	0x001C
#define GMAC_SADDRA1	0x0020
#define GMAC_SADDRA2	0x0024
#define GMAC_SADDRB0	0x0028
#define GMAC_SADDRB1	0x002C
#define GMAC_SADDRB2	0x0030
#define GMAC_MAR0	0x0034
#define GMAC_MAR1	0x0038
#define GMAC_MAR2	0x003C
#define GMAC_MAR3	0x0040
#define GMAC_TXISR	0x0044
#define GMAC_RXISR	0x0048
#define GMAC_TXRXISR	0x004C
#define GMAC_TXIMR	0x0050
#define GMAC_RXIMR	0x0054
#define GMAC_TXRXIMR	0x0058
#define GMAC_SMICTL	0x0080
#define GMAC_SMIDATA	0x0084
#define GMAC_PHYADDR	0x0088

/* General purpose status register */

#define GMAC_GPSR_SPEED		0x8000	/* 0 = 10Mbps, 1 = 100Mbps */
#define GMAC_GPSR_DUPLEX	0x4000	/* 0 = half duplex, 1 = full */
#define GMAC_GPSR_TXFLOWCTL	0x2000	/* TX flow control enabled */
#define GMAC_GPSR_LINK		0x1000	/* 1 = link good */
#define GMAC_GPSR_PAUSE		0x0800	/* port is in pause state */
#define GMAC_GPSR_TXBUSY	0x0400	/* transmitter is active */
#define GMAC_GPSR_EXCESSCOL	0x0200	/* exesss collision occured */
#define GMAC_GPSR_LATECOLL	0x0100	/* late collision */
#define GMAC_GPSR_MIISTS	0x0020	/* MII PHY status change */
#define GMAC_GPSR_GSPEED	0x0010	/* 1 = gigabit, 0 = 10/100 */
#define GMAC_GPSR_PARTITION	0x0008	/* port is in partition mode */
#define GMAC_GPSR_RXFLOWCTL	0x0004	/* RX flow control enabled */
#define GMAC_GPSR_PROMISC	0x0002	/* promiscuous mode */

/* General purpose control register */

#define GMAC_GPCR_TXFLOWCTL	0x2000	/* TX flow control enabled */
#define GMAC_GPCR_TX_ENABLE	0x1000	/* TX enabled */
#define GMAC_GPCR_RX_ENABLE	0x0800	/* RX enabled */
#define GMAC_GPCR_LOOPBK	0x0200	/* loopback mode */
#define GMAC_GPCR_PARTITION	0x0100	/* partition enable */
#define GMAC_GPCR_GSPEED	0x0080	/* 1 = gigabit, 0 = 10/100 */
#define GMAC_GPCR_LINKFORCE	0x0040	/* force good link */
#define GMAC_GPCR_DUPLEX	0x0020	/* 1 = full duplex 0 = half */
#define GMAC_GPCR_RXFLOWCTL	0x0010	/* RX flow control enabled */
#define GMAC_GPCR_SPEED		0x0008	/* 0 = 10Mbps, 1 = 100Mbps */
#define GMAC_GPCR_AUTUDUPLEX	0x0004	/* autoneg duplex mode */
#define GMAC_GPCR_AUTOFLOWCTL	0x0002	/* autoneg flow control */
#define GMAC_GPCR_AUTOSPEED	0x0001	/* autoneg speed */

/* Transmit control register */

#define GMAC_TXCTL_FORCEJAM	0x8000	/* Use jam flowctl in half duplex */
#define GMAC_TXCTL_ADDFCS	0x4000	/* Insert FCS into outbound frames */
#define GMAC_TXCTL_AUTOPAD	0x2000	/* autopad short frames */
#define GMAC_TXCTL_COLTHRESH	0x1C00	/* Collision threshold */
#define GMAC_TXCTL_PADPATTERM	0x00FF	/* padding pattern */

/* Receive control register */

#define GMAC_RXCTL_RX_UCAST	0x8000	/* receive unicast frames */
#define GMAC_RXCTL_RX_MCAST	0x4000	/* receive multicast frames */
#define GMAC_RXCTL_STRIPFCS	0x2000	/* strip FCS from inbound frames */

/* Transmit parameter register */

#define GMAC_TXPARM_JAMLEN	0xC000
#define GMAC_TXPARM_JAMIPG	0x3E00
#define GMAC_TXPARM_JAMIPGDAT	0x01F0
#define GMAC_TXPARM_BACKOFFLIM	0x000F


/* Serial mode register */

#define GMAC_SMR_DATABLINDER	0xF800
#define GMAC_SMR_LIMIT4		0x0400
#define GMAC_SMR_VLAN_EN	0x0200	/* 0 == 1514/9014, 1 == 1518/9018 */
#define GMAC_SMR_JUMBO_EN	0x0100	/* 0 == 1500, 1 == 9000 */
#define GMAC_SMR_IPGDAT		0x001F

/* Transmit interrupt register */

#define GMAC_TXISR_LATECOL	0x8000
#define GMAC_TXISR_COL		0x4000
#define GMAC_TXISR_MAXOCTET	0x1000
#define GMAC_TXISR_1518OCTET	0x0800
#define GMAC_TXISR_1023OCTET	0x0400
#define GMAC_TXISR_511OCTET	0x0200
#define GMAC_TXISR_255OCTET	0x0100
#define GMAC_TXISR_127OCTET	0x0080
#define GMAC_TXISR_64OCTET	0x0040
#define GMAC_TXISR_OCTETS	0x0030
#define GMAC_TXISR_MCAST	0x0008
#define GMAC_TXISR_PAUSE	0x0004
#define GMAC_TXISR_BCAST	0x0002
#define GMAC_TXISR_UCAST	0x0001

/* Receive interrupt register */

#define GMAC_RXISR_511OCTET	0x8000
#define GMAC_RXISR_255OCTET	0x4000
#define GMAC_RXISR_127OCTET	0x2000
#define GMAC_RXISR_64OCTET	0x1000
#define GMAC_RXISR_FRAG		0x0800
#define GMAC_RXISR_RUNT		0x0400
#define GMAC_RXISR_BADOCTET	0x0300
#define GMAC_RXISR_GOODOCTET	0x00C0
#define GMAC_RXISR_FCSERR	0x0010
#define GMAC_RXISR_MCAST	0x0008
#define GMAC_RXISR_PAUSE	0x0004
#define GMAC_RXISR_BCAST	0x0002
#define GMAC_RXISR_UCAST	0x0001

/* Transmit an receive interrupt register */

#define GMAC_TXRXISR_UFLOW	0x0800
#define GMAC_TXRXISR_SINGLECOL	0x0400
#define GMAC_TXRXISR_MULTICOL	0x0200
#define GMAC_TXRXISR_EXCESSCOL	0x0100
#define GMAC_TXRXISR_OFLOW	0x0040
#define GMAC_TXRXISR_JABBER	0x0010
#define GMAC_TXRXISR_GIANT	0x0008
#define GMAC_TXRXISR_INMAXOCTET	0x0004
#define GMAC_TXRXISR_IN1518OCTET	0x0002
#define GMAC_TXRXISR_IN1023OCTET	0x0001

/* SMI control register */

#define GMAC_SMICTL_PHYADDR	0xF800
#define GMAC_SMICTL_REGADDR	0x07C0
#define GMAC_SMICTL_OPCODE	0x0020
#define GMAC_SMICTL_RDVALID	0x0010
#define GMAC_SMICTL_BUSY	0x0008

#define GMAC_SMIOPCODE_READ	0x0020
#define GMAC_SMIOPCODE_WRITE	0x0000

#define GMAC_SMIPHYADDR(x)	((x) << 11)
#define GMAC_SMIREGADDR(x)	((x) << 6)

/* Phy address register */

#define GMAC_PHYADDR_MIBCLRMODE	0x0020	/* 1 == reset on read */
#define GMAC_PHYADDR_LOADTSTCNT	0x0010

/* MIB RX counter registers */

#define GMAC_MIB_INUCAST_LO		0x0100
#define GMAC_MIB_INUCAST_HI		0x0104
#define GMAC_MIB_INBCAST_LO		0x0108
#define GMAC_MIB_INBCAST_HI		0x010C
#define GMAC_MIB_INPAUSE_LO		0x0110
#define GMAC_MIB_INPAUSE_HI		0x0114
#define GMAC_MIB_INMCAST_LO		0x0118
#define GMAC_MIB_INMCAST_HI		0x011C
#define GMAC_MIB_INCRCERR_LO		0x0120
#define GMAC_MIB_INCRCERR_HI		0x0124
#define GMAC_MIB_INOCTETS_0		0x0130
#define GMAC_MIB_INOCTETS_1		0x0134
#define GMAC_MIB_INOCTETS_2		0x0138
#define GMAC_MIB_INOCTETS_3		0x013C
#define GMAC_MIB_INBADOCTETS_0		0x0140
#define GMAC_MIB_INBADOCTETS_1		0x0144
#define GMAC_MIB_INBADOCTETS_2		0x0148
#define GMAC_MIB_INBADOCTETS_3		0x014C
#define GMAC_MIB_INRUNT_LO		0x0150
#define GMAC_MIB_INRUNT_HI		0x0154
#define GMAC_MIB_INFRAG_LO		0x0158
#define GMAC_MIB_INFRAG_HI		0x015C
#define GMAC_MIB_IN64OCTETS_LO		0x0160
#define GMAC_MIB_IN64OCTETS_HI		0x0164
#define GMAC_MIB_IN127OCTETS_LO		0x0168
#define GMAC_MIB_IN127OCTETS_HI		0x016C
#define GMAC_MIB_IN255OCTETS_LO		0x0170
#define GMAC_MIB_IN255OCTETS_HI		0x0174
#define GMAC_MIB_IN511OCTETS_LO		0x0178
#define GMAC_MIB_IN511OCTETS_HI		0x017C
#define GMAC_MIB_IN1023OCTETS_LO	0x0180
#define GMAC_MIB_IN1023OCTETS_HI	0x0184
#define GMAC_MIB_IN1518OCTETS_LO	0x0188
#define GMAC_MIB_IN1518OCTETS_HI	0x018C
#define GMAC_MIB_INMAXOCTETS_LO		0x0190
#define GMAC_MIB_INMAXOCTETS_HI		0x0194
#define GMAC_MIB_INGIANT_LO		0x0198
#define GMAC_MIB_INGIANT_HI		0x019C
#define GMAC_MIB_INJABBER_LO		0x01A0
#define GMAC_MIB_INJABBER_HI		0x01A4
#define GMAC_MIB_INOFLOW_LO		0x01B0
#define GMAC_MIB_INOFLOW_HI		0x01B4

/* MIB TX counter registers */

#define GMAC_MIB_OUTUCAST_LO		0x01C0
#define GMAC_MIB_OUTUCAST_HI		0x01C4
#define GMAC_MIB_OUTBCAST_LO		0x01C8
#define GMAC_MIB_OUTBCAST_HI		0x01CC
#define GMAC_MIB_OUTPAUSE_LO		0x01D0
#define GMAC_MIB_OUTPAUSE_HI		0x01D4
#define GMAC_MIB_OUTMCAST_LO		0x01D8
#define GMAC_MIB_OUTMCAST_HI		0x01DC
#define GMAC_MIB_OUTOCTETS_0		0x01E0
#define GMAC_MIB_OUTOCTETS_1		0x01E4
#define GMAC_MIB_OUTOCTETS_2		0x01E8
#define GMAC_MIB_OUTOCTETS_3		0x01EC
#define GMAC_MIB_OUT64OCTETS_LO		0x01F0
#define GMAC_MIB_OUT64OCTETS_HI		0x01F4
#define GMAC_MIB_OUT127OCTETS_LO	0x01F8
#define GMAC_MIB_OUT127OCTETS_HI	0x01FC
#define GMAC_MIB_OUT255OCTETS_LO	0x0200
#define GMAC_MIB_OUT255OCTETS_HI	0x0204
#define GMAC_MIB_OUT511OCTETS_LO	0x0208
#define GMAC_MIB_OUT511OCTETS_HI	0x020C
#define GMAC_MIB_OUT1023OCTETS_LO	0x0210
#define GMAC_MIB_OUT1023OCTETS_HI	0x0214
#define GMAC_MIB_OUT1518OCTETS_LO	0x0218
#define GMAC_MIB_OUT1518OCTETS_HI	0x021C
#define GMAC_MIB_OUTMAXOCTETS_LO	0x0220
#define GMAC_MIB_OUTMAXOCTETS_HI	0x0224
#define GMAC_MIB_OUTCOLLISION_LO	0x0230
#define GMAC_MIB_OUTCOLLISION_HI	0x0234
#define GMAC_MIB_OUTEXCESSCOL_LO	0x0240
#define GMAC_MIB_OUTEXCESSCOL_HI	0x0244
#define GMAC_MIB_OUTMULTICOL_LO		0x0248
#define GMAC_MIB_OUTMULTICOL_HI		0x024C
#define GMAC_MIB_OUTSINGLECOL_LO	0x0250
#define GMAC_MIB_OUTSINGLECOL_HI	0x0254
#define GMAC_MIB_OUTUFLOW_HI		0x0258
#define GMAC_MIB_OUTUFLOW_LO		0x025C

/* SMI control register */

#define GMACSMICTL_PHYADDR	0xF800
#define GMACSMICTL_REGADDR	0x07C0
#define GMACSMICTL_OPCODE	0x0020
#define GMACSMICTL_RDVALID	0x0010
#define GMACSMICTL_BUSY		0x0008

#define SMIOPCODE_READ		0x0020
#define SMIOPCODE_WRITE		0x0000

#define SMIPHYADDR(x)		((x) << 11)
#define SMIREGADDR(x)		((x) << 6)

/*
 * GMAC RX status word
 * This will be relayed through to the RX DMA descriptor or
 * status descriptor in the Yukon/Yukon II.
 */

#define GMAC_RXSTAT_FIFO_OFLOW	0x00000001
#define GMAC_RXSTAT_CRC_ERR	0x00000002
#define GMAC_RXSTAT_RSVD0	0x00000004
#define GMAC_RXSTAT_FRAGMENT	0x00000008
#define GMAC_RXSTAT_GIANT	0x00000010
#define GMAC_RXSTST_MII_ERR	0x00000020
#define GMAC_RXSTAT_FLOWCTL_BAD	0x00000040
#define GMAC_RXSTAT_FLOWCTL_OK	0x00000080
#define GMAC_RXSTAT_RXOK	0x00000100
#define GMAC_RXSTAT_BCAST	0x00000200
#define GMAC_RXSTAT_MCAST	0x00000400
#define GMAC_RXSTAT_RUNT	0x00000800
#define GMAC_RXSTAT_JABBER	0x00001000
#define GMAC_RXSTAT_VLAN	0x00002000
#define GMAC_RXSTAT_BYTECNT	0xFFFF0000

#endif /* __INCmvGmach */
