/* am79c97xVxbEnd.h - header file for Am79x79x 10/100 VxBus END driver */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,31oct07,wap  Increase RX and TX DMA ring sizes to improve TCP
                 performance (WIND00104927)
01d,15jun07,wap  Make this driver SMP safe
01c,24aug06,wap  Switch to I/O space BAR
01b,29jun06,wap  Correct typo in RX mBlk array initialization
01a,12jun06,wap  written
*/

#ifndef __INCam79c79xVxbEndh
#define __INCam79c79xVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void lnPciRegister (void);

#ifndef BSP_VERSION

/* AMD PCI vendor ID. */

#define AMD_VENDORID		0x1022

/*
 * AMD PCnet/PCI device ID.
 * Note that all devices in the PCnet/LANCE ethernet family
 * have the same PCI vendor/device ID. To distinguish them,
 * you need to refer to the chip ID registers.
 */

#define AMD_DEVICEID_PCNET	0x2000
#define AMD_DEVICEID_PCNET_PNA	0x2001
#define AMD_DEVICEID_PCNET974	0x2040

/*
 * Known chip ID values.
 *
 * CSR88-89: Chip ID masks
 * Note: the Am79C970 is documented to have chip ID 0x2420,
 * but evidence suggests it really has ID 0x2430 (same as
 * the ISA device that preceeded it).
 */

#define AMD_MASK		0x003
#define PART_MASK		0xffff
#define Am79C960  0x0003
#define Am79C961  0x2260
#define Am79C961A 0x2261
#define Am79C965  0x2430
#define Am79C970  0x2430
#define Am79C970A 0x2621
#define Am79C971  0x2623
#define Am79C972  0x2624
#define Am79C973  0x2625
#define Am79C974  0x2420
#define Am79C975  0x2627
#define Am79C976  0x2628
#define Am79C978  0x2626

/*
 * The AMD PCnet/LANCE chips have only a small window of
 * registers mapped into the host. Access to the internal
 * registers is made indirectly, through the RAP, RDP and
 * RBP registers.
 *
 * The layout of the visible registers varies depending on
 * the operating mode of the chip. For backwards compatibility
 * with the original LANCE, the chip can be used in 16-bit
 * mode.
 */

/*
 * I/O map in 16-bit mode. To switch to 32-bit mode,
 * you need to perform a 32-bit write to the RDP register
 * (writing a 0 is recommended).
 */

#define LNPCI_IO16_APROM00	0x00
#define LNPCI_IO16_APROM01	0x02
#define LNPCI_IO16_APROM02	0x04
#define LNPCI_IO16_APROM03	0x06
#define LNPCI_IO16_APROM04	0x08
#define LNPCI_IO16_APROM05	0x0A
#define LNPCI_IO16_APROM06	0x0C
#define LNPCI_IO16_APROM07	0x0E
#define LNPCI_IO16_RDP		0x10
#define LNPCI_IO16_RAP		0x12
#define LNPCI_IO16_RESET	0x14
#define LNPCI_IO16_BDP		0x16

/*
 * I/O map in 32-bit mode.
 */
#define LNPCI_IO32_APROM00	0x00
#define LNPCI_IO32_APROM01	0x04
#define LNPCI_IO32_APROM02	0x08
#define LNPCI_IO32_APROM03	0x0C
#define LNPCI_IO32_RDP		0x10
#define LNPCI_IO32_RAP		0x14
#define LNPCI_IO32_RESET	0x18
#define LNPCI_IO32_BDP		0x1C


/*
 * Internally, the PCnet/LANCE has two sets of registers:
 * Control and Status Registers (CSRs) and Bus Configuration
 * Registers (BCRs). Most of these registers are only 16
 * bits wide. A few are 32. We document the CSRs first,
 * then the BCRs.
 */

/* CSR registers */

/* CSR0 -- Control and status */

#define LNPCI_CSR0		0

#define LNPCI_CSR0_INIT            0x0001
#define LNPCI_CSR0_START           0x0002
#define LNPCI_CSR0_STOP            0x0004
#define LNPCI_CSR0_TDMD            0x0008
#define LNPCI_CSR0_TXON            0x0010
#define LNPCI_CSR0_RXON            0x0020
#define LNPCI_CSR0_INTEN           0x0040
#define LNPCI_CSR0_INTR            0x0080
#define LNPCI_CSR0_IDONE           0x0100
#define LNPCI_CSR0_TINT            0x0200
#define LNPCI_CSR0_RINT            0x0400
#define LNPCI_CSR0_MERR            0x0800
#define LNPCI_CSR0_MISS            0x1000
#define LNPCI_CSR0_CERR            0x2000
#define LNPCI_CSR0_ERR             0x8000

/* CSR1 -- Initialization block address, low */

#define LNPCI_CSR1		1

/* CSR2 -- Initialization block address, high */

#define LNPCI_CSR2		2

/* CSR3 -- Interrupt masks and deferral control */

#define LNPCI_CSR3		3

#define LNPCI_CSR3_BSWAP           0x0004
#define LNPCI_CSR3_ENMBA           0x0008  /* enable modified backoff alg */
#define LNPCI_CSR3_DXMT2PD         0x0010
#define LNPCI_CSR3_LAPPEN          0x0020  /* lookahead packet processing enb */
#define LNPCI_CSR3_DXSUFLO         0x0040  /* disable TX stop on underflow */
#define LNPCI_CSR3_IDONE           0x0100
#define LNPCI_CSR3_TINT            0x0200
#define LNPCI_CSR3_RINT            0x0400
#define LNPCI_CSR3_MERR            0x0800
#define LNPCI_CSR3_MISS            0x1000

/* CSR4 -- Test and features control */

#define LNPCI_CSR4		4

#define LNPCI_CSR4_TXSTRTMASK    0x0004
#define LNPCI_CSR4_TXSTRT        0x0008
#define LNPCI_CSR4_RXCCOFLOWM    0x0010  /* Rx collision counter oflow */
#define LNPCI_CSR4_RXCCOFLOW     0x0020
#define LNPCI_CSR4_UINT          0x0040
#define LNPCI_CSR4_UINTREQ       0x0080
#define LNPCI_CSR4_MISSOFLOWM    0x0100
#define LNPCI_CSR4_MISSOFLOW     0x0200
#define LNPCI_CSR4_STRIP_FCS     0x0400
#define LNPCI_CSR4_PAD_TX        0x0800
#define LNPCI_CSR4_TXDPOLL       0x1000
#define LNPCI_CSR4_DMAPLUS       0x4000

/* CSR5 -- Extented control and interrupt */

#define LNPCI_CSR5		5

#define LNPCI_CSR5_SPND		0x0001  /* suspend */
#define LNPCI_CSR5_MPMODE	0x0002  /* magic packet mode */
#define LNPCI_CSR5_MPENB	0x0004  /* magic packet enable */
#define LNPCI_CSR5_MPINTEN	0x0008  /* magic packet interrupt enable */
#define LNPCI_CSR5_MPINT	0x0010  /* magic packet interrupt */
#define LNPCI_CSR5_MPPLBA	0x0020  /* magic packet phys. logical bcast */
#define LNPCI_CSR5_EXDEFEN	0x0040  /* excessive deferral interrupt enb. */
#define LNPCI_CSR5_EXDEF	0x0080  /* excessive deferral interrupt */
#define LNPCI_CSR5_SINTEN	0x0400  /* system interrupt enable */
#define LNPCI_CSR5_SINT		0x0800  /* system interrupt */
#define LNPCI_CSR5_LTINTEN	0x4000  /* last TX interrupt enb */
#define LNPCI_CSR5_TXOKINTD	0x8000  /* TX OK interrupt disable */

/* CSR6 -- encoded RX/TX descriptor ring size */

#define LNPCI_CSR6		6

#define LNPCI_CSR6_RLEN		0x0F00
#define LNPCI_CSR6_TLEN		0xF000

/* CSR7 -- Extended control and interrupt 2 */

#define LNPCI_CSR7		7

#define LNPCI_CSR7_MIIPDTINTE  0x0001
#define LNPCI_CSR7_MIIPDTINT   0x0002
#define LNPCI_CSR7_MCCIINTE    0x0004
#define LNPCI_CSR7_MCCIINT     0x0008
#define LNPCI_CSR7_MCCINTE     0x0010
#define LNPCI_CSR7_MCCINT      0x0020
#define LNPCI_CSR7_MAPINTE     0x0040
#define LNPCI_CSR7_MAPINT      0x0080
#define LNPCI_CSR7_MREINTE     0x0100
#define LNPCI_CSR7_MREINT      0x0200
#define LNPCI_CSR7_STINTE      0x0400
#define LNPCI_CSR7_STINT       0x0800
#define LNPCI_CSR7_RXDPOLL     0x1000
#define LNPCI_CSR7_RDMD        0x2000
#define LNPCI_CSR7_RXFRTG      0x4000
#define LNPCI_CSR7_FASTSPNDE   0x8000

#define LNPCI_CSR7_INTACK	\
	(LNPCI_CSR7_MIIPDTINT|LNPCI_CSR7_MCCIINT|LNPCI_CSR7_MCCINT|	\
	 LNPCI_CSR7_MAPINT|LNPCI_CSR7_MREINT|LNPCI_CSR7_STINT|	\
	 LNPCI_CSR7_RXDPOLL|LNPCI_CSR7_FASTSPNDE)

/* CSR8-11 -- Logical address filter (multicast hash table) */

#define LNPCI_CSR8		8
#define LNPCI_CSR9		9
#define LNPCI_CSR10		10
#define LNPCI_CSR11		11

/* CSR12-14 -- Physical address */

#define LNPCI_CSR12		12
#define LNPCI_CSR13		13
#define LNPCI_CSR14		14

/* CSR15 -- Mode */

#define LNPCI_CSR15		15

#define LNPCI_CSR15_RXD		0x0001  /* RX disable */
#define LNPCI_CSR15_TXD		0x0002  /* TX disable */
#define LNPCI_CSR15_LOOP	0x0004  /* loopback enable */
#define LNPCI_CSR15_TXCRCD	0x0008
#define LNPCI_CSR15_FORCECOLL	0x0010
#define LNPCI_CSR15_RETRYD	0x0020
#define LNPCI_CSR15_INTLOOP	0x0040
#define LNPCI_CSR15_PORTSEL	0x0180
#define LNPCI_CSR15_RXNOPA	0x2000 /* Disable unicast reception */
#define LNPCI_CSR15_RXNOBROAD	0x4000 /* Disable broadcast reception */
#define LNPCI_CSR15_PROMISC	0x8000 /* Enable promiscuous mode */

/* Settings for LNPCI_CSR15_PORTSEL when ASEL (BCR2[1] is 0 */

#define LNPCI_PORT_AUI            0x0000
#define LNPCI_PORT_10BASET        0x0080
#define LNPCI_PORT_GPSI           0x0100
#define LNPCI_PORT_MII            0x0180

/* CSR24 -- RX DMA ring base address, low */

#define LNPCI_CSR24		24

/* CSR25 -- RX DMA ring base address, high */

#define LNPCI_CSR25		25

/* CSR30 -- TX DMA ring base address, low */

#define LNPCI_CSR30		30

/* CSR31 -- TX DMA ring base address, high */

#define LNPCI_CSR31		31

/* CSR76 -- RX DMA Ring length */

#define LNPCI_CSR76		76

/* CSR78 -- TX DMA Ring length */

#define LNPCI_CSR78		78

/* CSR80 -- DMA transfer counter and FIFO threshold control */

#define LNPCI_CSR80		80

#define LNPCI_CSR80_RCVFW	0x3000	/* RX FIFO watermark */
#define LNPCI_CSR80_XMTSP	0x0C00	/* TX start point */
#define LNPCI_CSR80_XMTFW	0x0300	/* TX FIFO watermark */
#define LNPCI_CSR80_DMATC	0x00FF	/* DMA transfer counter */

#define LNPCI_RCVFW_16		0x0000
#define LNPCI_RCVFW_64		0x1000
#define LNPCI_RCVFW_112		0x2000

#define LNPCI_XMTSP_20		0x0000
#define LNPCI_XMTSP_64		0x0400
#define LNPCI_XMTSP_128		0x0800
#define LNPCI_XMTSP_220		0x0C00

#define LNPCI_XMTFW_16		0x0000
#define LNPCI_XMTFW_64		0x0100
#define LNPCI_XMTFW_108		0x0200

/* CSR88 -- Chip ID register, lower */

#define LNPCI_CSR88		88

#define LNPCI_CSR88_VER		0xF0000000
#define LNPCI_CSR88_PARTID	0x0FFFF000
#define LNPCI_CSR88_MANFID	0x00000FFE
#define LNPCI_CSR88_ONE		0x00000001

#define LNPCI_PARTID(x)		(((x) & 0x0FFFF000) >> 12)

/* CSR89 -- Chip ID register, upper */

#define LNPCI_CSR89		88

#define LNPCI_CSR89_VER		0x0000F000
#define LNPCI_CSR89_PARTIDU	0x00000FFF

/* CSR122 -- Advanced feature control */

#define LNPCI_CSR122		122

#define LNPCI_CSR122_RXALIGN	0x0001

/* BCR registers */

/* BCR2 -- Miscellaneous configuration */

#define LNPCI_BCR2_SMIUEN	0x8000	/* Enable SMbus, am79c975 only */
#define LNPCI_BCR2_DISCCR_SFEX	0x4000	/* Disable scrambler/descrambler */
#define LNPCI_BCR2_PHYSELEN	0x2000	/* Enable manual PHY selection */
#define LNPCI_BCR2_LEDPE	0x1000	/* LED program enable */
#define LNPCI_BCR2_RESET_SFEX	0x0800	/* Reset internal PHY */
#define LNPCI_BCR2_APROMWE	0x0100	/* Address PROM write enable */
#define LNPCI_BCR2_INTLEVEL	0x0080	/* interrupt level (1=edge,0=level) */
#define LNPCI_BCR2_EADISEL	0x0008	/* EADI enable */
#define LNPCI_BCR2_SLEEP_SFEX	0x0004	/* internal PHY sleep mode */
#define LNPCI_BCR2_ASEL		0x0002	/* auto-select media */

/* BCR9 -- Full duplex control */

#define LNPCI_BCR9		9

#define LNPCI_BCR9_FDEN		0x0001  /* Full-duplex enable */
#define LNPCI_BCR9_AUI		0x0002  /* AUI full-duplex */
#define LNPCI_BCR9_FDRPAD	0x0004  /* Full-duplex runt pkt accept dis. */

/* BCR18 -- Burst and bus control */

#define LNPCI_BCR18		18

#define LNPCI_BCR18_ROMTMG	0xF000	/* ROM timing */
#define LNPCI_BCR18_NOUFLO	0x0800	/* No underflow mode */
#define LNPCI_BCR18_MEMCMD	0x0200	/* memory command */
#define LNPCI_BCR18_EXTREQ	0x0100	/* extended request */
#define LNPCI_BCR18_DWIO	0x0080	/* double word I/O */
#define LNPCI_BCR18_BREADE	0x0040	/* burst read enable */
#define LNPCI_BCR18_BWRITE	0x0020	/* burst write enable */
#define LNPCI_BCR18_PHYSEL	0x0018	/* PHY mode selection */

/* BCR19 -- EEPROM control and status */

#define LNPCI_BCR19		19

#define LNPCI_BCR19_PVALID	0x8000	/* Successful autoload */
#define LNPCI_BCR19_PREAD	0x4000	/* Force autoload */
#define LNPCI_BCR19_EEDET	0x2000	/* EEPROM detect */
#define LNPCI_BCR19_EEN		0x0010	/* EEPROM port enable */
#define LNPCI_BCR19_EECMD_SEL	0x0004	/* EEPROM chip select */
#define LNPCI_BCR19_EECMD_CLK	0x0002	/* EEPROM clock */
#define LNPCI_BCR19_EECMD_DATA	0x0001	/* EEPROM data in/out */

#define LNPCI_9346_WRITE	0x5
#define LNPCI_9346_READ		0x6
#define LNPCI_9346_ERASE	0x7

/* BCR20 -- Software style */

#define LNPCI_BCR20		20

#define LNPCI_BCR20_APERREN	0x0400  /* advanced parity error checking */
#define LNPCI_BCR20_SSIZE32	0x0100
#define LNPCI_BCR20_SWSTYLE	0x00FF

#define LNPCI_SWSTYLE_LANCE		0x0000
#define LNPCI_SWSTYLE_PCNETPCI		0x0102
#define LNPCI_SWSTYLE_PCNETPCI_BURST	0x0103

/* BCR25 -- SRAM size register */

#define LNPCI_BCR25		25

#define LNPCI_BCM25_SRAM_SIZE	0x00FF

/* BCR26 -- SRAM boundary register */

#define LNPCI_BCR26		26

#define LNPCI_BCM26_SRAM_BND	0x00FF

/* BCR27 -- SRAM interface control */

#define LNPCI_BCR27		27

#define LNPCI_BCR27_PTR_TST	0x8000
#define LNPCI_BCR27_LOLATRX	0x4000	/* Low latency RX */
#define LNPCI_BCR27_EBCS	0x0038	/* Expansion bus clock source */
#define LNPCI_BCR27_CLK_FAC	0x0007	/* Clock factor */

#define LNPCI_EBCS_PCI		0x0000
#define LNPCI_EBCS_TIMEBASE	0x0008
#define LNPCI_EBCS_EBCLK_PIN	0x0030

#define LNPCI_CLKFAC_ONE	0x0000
#define LNPCI_CLKFAC_HALF	0x0001
#define LNPCI_CLKFAC_QUARTER	0x0003

/* BCR32 -- MII control */

#define LNPCI_BCR32		32

#define LNPCI_BCR32_MIILP	0x0002  /* MII internal loopback */
#define LNPCI_BCR32_XPHYSP	0x0008  /* external PHY speed */
#define LNPCI_BCR32_XPHYFD	0x0010  /* external PHY full duplex */
#define LNPCI_BCR32_XPHYANE	0x0020  /* external phy auto-neg enable */
#define LNPCI_BCR32_XPHYRST	0x0040  /* external PHY reset */
#define LNPCI_BCR32_DANAS	0x0080  /* disable auto-neg auto-setup */
#define LNPCI_BCR32_APDW	0x0700  /* auto-poll dwell time */
#define LNPCI_BCR32_APEP	0x0800  /* auto-poll external PHY */
#define LNPCI_BCR32_FMDC	0x3000  /* data clock speed */
#define LNPCI_BCR32_MIIPD	0x4000  /* PHY detect */
#define LNPCI_BCR32_ANTST	0x8000  /* Manufacturing test */

#define LNPCI_APDW_CONTINUOUS	0x0000
#define LNPCI_APDW_128		0x0100
#define LNPCI_APDW_256		0x0200
#define LNPCI_APDW_512		0x0300
#define LNPCI_APDW_1024		0x0400
#define LNPCI_APDW_2048		0x0500

/* BCR33 -- MII address */

#define LNPCI_BCR33		33

#define LNPCI_BCR33_REGADDR	0x001F
#define LNPCI_BCR33_PHYADD	0x03E0

/* BCR34 -- MII data */

#define LNPCI_BCR34		34

#define LNPCI_BCR34_MIIMD	0xFFFF

/*
 * DMA descriptor definitions
 *
 * Most PCnet/PCI devices support three different 'software styles,'
 * and three different DMA descriptor layouts to go with them.
 * The first one is the legacy 16-bit layout compatible with the
 * original 7990 LANCE chip. The second is the 32-bit descriptor
 * layout which first appeared on the original 79c970 PCnet/PCI
 * device. The third is also a 32-bit layout, but with some of the
 * fields swapped.
 *
 * We want to use SWSTYLE 3 where possible, since that allows us to
 * use PCI burst mode, and achieve better performance. However we
 * also want to support the original 79c970 because that's the device
 * that VMWare emulates. We define our descriptors to use the SWSTYLE 3
 * layout by default, but we provide the SWSTYLE 2 definitions for
 * reference.
 */

/* RX DMA descriptor structures */

typedef struct lnPci_rx_desc
    {
    volatile UINT32	lnPci_rxLen;
    volatile UINT16	lnPci_bufLen;
    volatile UINT16	lnPci_rxStat;
    volatile UINT32	lnPci_rxBufAddr;
    volatile UINT32	lnPci_uSpace;
    } LNPCI_RX_DESC_SWSTYLE_3;

typedef struct lnPci_rx_desc_swstyle_2
    {
    volatile UINT32	lnPci_rxBufAddr;
    volatile UINT16	lnPci_bufLen;
    volatile UINT16	lnPci_rxStat;
    volatile UINT32	lnPci_rxLen;
    volatile UINT32	lnPci_uSpace;
    } LNPCI_RX_DESC_SWSTYLE_2;

#define LNPCI_RXSTAT_BAM	0x0010	/* broadcast address match */
#define LNPCI_RXSTAT_LFAM	0x0020	/* logical filter match (multicast) */
#define LNPCI_RXSTAT_PAM	0x0040	/* physical address match (unicast) */
#define LNPCI_RXSTAT_BPE	0x0080  /* bus parity error */
#define LNPCI_RXSTAT_ENP	0x0100  /* end of packet */
#define LNPCI_RXSTAT_STP	0x0200  /* start of packet */
#define LNPCI_RXSTAT_BUFF	0x0400  /* buffer error */
#define LNPCI_RXSTAT_CRC	0x0800  /* CRC error */
#define LNPCI_RXSTAT_OFLOW	0x1000  /* rx overrun */
#define LNPCI_RXSTAT_FRAM	0x2000  /* framing error */
#define LNPCI_RXSTAT_ERR	0x4000  /* error summary */
#define LNPCI_RXSTAT_OWN	0x8000

#define LNPCI_RXLEN_MBO		0xF000	/* Must be one */
#define LNPCI_RXLEN_BUFSZ	0x0FFF

/* TX DMA descriptor structures */

typedef struct lnPci_tx_desc
    {
    volatile UINT32	lnPci_txStat;
    volatile UINT32	lnPci_txCtl;
    volatile UINT32	lnPci_txBufAddr;
    volatile UINT32	lnPci_uSpace;
    } LNPCI_TX_DESC_SWSTYLE_3;

typedef struct lnPci_tx_desc_swstyle_2
    {
    volatile UINT32	lnPci_txBufAddr;
    volatile UINT32	lnPci_txCtl;
    volatile UINT32	lnPci_txStat;
    volatile UINT32	lnPci_uSpace;
    } LNPCI_TX_DESC_SWSTYLE_2;

#ifdef LNPCI_SWSTYLE_3
#define LNPCI_TX_DESC LNPCI_TX_DESC_SWSTYLE_3
#define LNPCI_RX_DESC LNPCI_RX_DESC_SWSTYLE_3
#else
#define LNPCI_TX_DESC LNPCI_TX_DESC_SWSTYLE_2
#define LNPCI_RX_DESC LNPCI_RX_DESC_SWSTYLE_2
#endif

#define LNPCI_TXSTAT_TRC	0x0000000F      /* transmit retries */
#define LNPCI_TXSTAT_RTRY	0x04000000      /* retry */
#define LNPCI_TXSTAT_LCAR	0x08000000      /* lost carrier */
#define LNPCI_TXSTAT_LCOL	0x10000000      /* late collision */
#define LNPCI_TXSTAT_EXDEF	0x20000000      /* excessive deferrals */
#define LNPCI_TXSTAT_UFLOW	0x40000000      /* transmit underrun */
#define LNPCI_TXSTAT_BUFF	0x80000000      /* buffer error */

#define LNPCI_TXCTL_OWN		0x80000000
#define LNPCI_TXCTL_ERR		0x40000000      /* error summary */
#define LNPCI_TXCTL_ADD_FCS	0x20000000      /* add FCS to pkt */
#define LNPCI_TXCTL_MORE_LTINT	0x10000000
#define LNPCI_TXCTL_ONE		0x08000000
#define LNPCI_TXCTL_DEF		0x04000000
#define LNPCI_TXCTL_STP		0x02000000	/* start of packet */
#define LNPCI_TXCTL_ENP		0x01000000	/* end of packet */
#define LNPCI_TXCTL_BPE		0x00800000
#define LNPCI_TXCTL_MBO		0x0000F000	/* must be one */
#define LNPCI_TXCTL_BUFSZ	0x00000FFF

#define LNPCI_RX_DESC_CNT	64
#define LNPCI_TX_DESC_CNT	64

#define LNPCI_ADJ(x)		((x)->m_data += 2)
#define LNPCI_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))

#define LNPCI_CLSIZE	1536
#define LNPCI_MAXFRAG	16
#define LNPCI_MAX_RX	16
#define LNPCI_NAME	"lnPci"
#define LNPCI_TIMEOUT 10000
#define LNPCI_INTRS	0
#define LNPCI_RXINTRS	0
#define LNPCI_TXINTRS	0
#define LNPCI_LINKINTRS 0

/*
 * Private adapter context structure.
 */

typedef struct lnPci_drv_ctrl
    {
    END_OBJ		lnPciEndObj;
    VXB_DEVICE_ID	lnPciDev;
    void		*lnPciMuxDevCookie;
    void *		lnPciBar;
    void *		lnPciHandle;

    JOB_QUEUE_ID	lnPciJobQueue;
    QJOB		lnPciIntJob;
    atomicVal_t		lnPciIntPending;

    QJOB		lnPciRxJob;
    atomicVal_t		lnPciRxPending;

    QJOB		lnPciTxJob;
    atomicVal_t		lnPciTxPending;
    UINT8		lnPciTxCur;
    UINT8		lnPciTxLast;
    volatile BOOL	lnPciTxStall;
    UINT16		lnPciTxThresh;

    BOOL		lnPciPolling;
    M_BLK_ID		lnPciPollBuf;
    UINT16		lnPciIntMask;
    UINT16		lnPciIntrs;

    UINT8		lnPciAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	lnPciCaps;

    END_IFDRVCONF	lnPciEndStatsConf;
    END_IFCOUNTERS	lnPciEndStatsCounters;
    UINT32		lnPciInErrors;
    UINT32		lnPciInDiscards;
    UINT32		lnPciInUcasts;
    UINT32		lnPciInMcasts;
    UINT32		lnPciInBcasts;
    UINT32		lnPciInOctets;
    UINT32		lnPciOutErrors;
    UINT32		lnPciOutUcasts;
    UINT32		lnPciOutMcasts;
    UINT32		lnPciOutBcasts;
    UINT32		lnPciOutOctets;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*lnPciMediaList;
    END_ERR		lnPciLastError;
    UINT32		lnPciCurMedia;
    UINT32		lnPciCurStatus;
    VXB_DEVICE_ID	lnPciMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	lnPciParentTag;

    VXB_DMA_TAG_ID	lnPciRxDescTag;
    VXB_DMA_MAP_ID	lnPciRxDescMap;
    LNPCI_RX_DESC	*lnPciRxDescMem;

    VXB_DMA_TAG_ID	lnPciTxDescTag;
    VXB_DMA_MAP_ID	lnPciTxDescMap;
    LNPCI_TX_DESC	*lnPciTxDescMem;

    VXB_DMA_TAG_ID	lnPciMblkTag;

    VXB_DMA_MAP_ID	lnPciRxMblkMap[LNPCI_RX_DESC_CNT];
    VXB_DMA_MAP_ID	lnPciTxMblkMap[LNPCI_TX_DESC_CNT];

    M_BLK_ID		lnPciRxMblk[LNPCI_RX_DESC_CNT];
    M_BLK_ID		lnPciTxMblk[LNPCI_TX_DESC_CNT];

    int			lnPciEeWidth;

    UINT32		lnPciDevType;

    UINT32		lnPciTxProd;
    UINT32		lnPciTxCons;
    UINT32		lnPciTxFree;
    UINT32		lnPciRxIdx;

    SEM_ID		lnPciDevSem;
    spinlockIsr_t	lnPciLock;
    } LNPCI_DRV_CTRL;


#define LNPCI_BAR(p)   ((LNPCI_DRV_CTRL *)(p)->pDrvCtrl)->lnPciBar
#define LNPCI_HANDLE(p)   ((LNPCI_DRV_CTRL *)(p)->pDrvCtrl)->lnPciHandle

#define CSR_READ_4(pDev, addr)					\
    vxbRead32 (LNPCI_HANDLE(pDev), (UINT32 *)((char *)LNPCI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)				\
    vxbWrite32 (LNPCI_HANDLE(pDev),				\
        (UINT32 *)((char *)LNPCI_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)					\
    vxbRead16 (LNPCI_HANDLE(pDev), (UINT16 *)((char *)LNPCI_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)				\
    vxbWrite16 (LNPCI_HANDLE(pDev),				\
        (UINT16 *)((char *)LNPCI_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)					\
    vxbRead8 (LNPCI_HANDLE(pDev), (UINT8 *)((char *)LNPCI_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)				\
    vxbWrite8 (LNPCI_HANDLE(pDev),				\
        (UINT8 *)((char *)LNPCI_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define LNPCI_CSR_SETBIT(pDef, reg, x)		\
	lnPciCsrWrite(pDev, reg, lnPciCsrRead(pDev, reg) | (x))

#define LNPCI_CSR_CLRBIT(pDev, reg, x)		\
	lnPciCsrWrite(pDev, reg, lnPciCsrRead(pDev, reg) & ~(x))

#define LNPCI_BCR_SETBIT(pDev, reg, x)		\
	lnPciBcrWrite(pDev, reg, lnPciBcrRead(pDev, reg) | (x))

#define LNPCI_BCR_CLRBIT(pDev, reg, x)		\
	lnPciBcrWrite(pDev, reg, lnPciBcrRead(pDev, reg) & ~(x))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCam79c79xVxbEndh */
