/* vxbLpc24xxEnd.h - LPC24XX EMAC VxBus END driver */

/*
 * Copyright (c) 2008, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,23may12,y_c  Modify the definition of macro MAC_PHY_MAINT_PHY_SHIFT.
                 (WIND00270768)
01a,11dec08,x_s  written.
*/

#ifndef __INCvxbLpc24xxEndh
#define __INCvxbLpc24xxEndh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* forward declarations */

IMPORT void lpc24xxEndRegister (void);

#ifndef BSP_VERSION

/* EMAC driver generic defines */

#define EMAC_NAME                      "emac"
#define EMAC_MTU                       1500
#define EMAC_JUMBO_MTU                 9000
#define EMAC_TIMEOUT                   10000
#define EMAC_TUPLE_CNT                 384
#define EMAC_ADJ(x)                    (x)->m_data += 2

/* Mac registers */

#define MAC_MAC1                       0x0000
#define MAC_MAC2                       0x0004

#define MAC_IPGT                       0x0008
#define MAC_IPGR                       0x000C
#define MAC_CLRT                       0x0010
#define MAC_MAXF                       0x0014
#define MAC_SUPP                       0x0018
#define MAC_TEST                       0x001C

#define MAC_MCFG                       0x0020
#define MAC_MCMD                       0x0024
#define MAC_MADR                       0x0028
#define MAC_MWTD                       0x002C
#define MAC_MRDD                       0x0030
#define MAC_MIND                       0x0034

#define MAC_SA0                        0x0040
#define MAC_SA1                        0x0044
#define MAC_SA2                        0x0048

/* Control registers */

#define CONTROL_COMMAND                0x0100
#define CONTROL_STATUS                 0x0104

#define CONTROL_RXDESCRIPTOR           0x0108
#define CONTROL_RXSTATUS               0x010C
#define CONTROL_RXDESCRIPTORNUMBER     0x0110
#define CONTROL_RXPRODUCEINDEX         0x0114
#define CONTROL_RXCONSUMEINDEX         0x0118

#define CONTROL_TXDESCRIPTOR           0x011C
#define CONTROL_TXSTATUS               0x0120
#define CONTROL_TXDESCRIPTORNUMBER     0x0124
#define CONTROL_TXPRODUCEINDEX         0x0128
#define CONTROL_TXCONSUMEINDEX         0x012C

#define CONTROL_TSV0                   0x0158
#define CONTROL_TSV1                   0x015C
#define CONTROL_RSV                    0x0160

#define CONTROL_FLOWCONTROLCOUNTER     0x0170
#define CONTROL_FLOWCONTROSTATUS       0x0174

/* Rx filter registers*/

#define FILTER_RXFILTERCTRL            0x0200
#define FILTER_RXFILTERWOLSTATUS       0x0204
#define FILTER_RXFILTERWOLCLEAR        0x0208
#define FILTER_HASHFILTERL             0x0210
#define FILTER_HASHFILTERH             0x0214

/* Module control registers*/

#define MODULE_INTSTATUS               0x0FE0
#define MODULE_INTENABLE               0x0FE4
#define MODULE_INTCLEAR                0x0FE8
#define MODULE_INTSET                  0x0FEC
#define MODULE_POWERDOWN               0x0FF4

/* MAC Registers definitions*/

#define MAC_RECEIVE_ENABLE             0x00000001
#define MAC_SOFT_RESET                 0x00008000
#define MAC_PHY_MAINT_PHY_SHIFT        8
#define MAC_PHY_MAINT_DATA_MASK        0x0000ffff
#define MAC_STATUS_MDIO_BUSY           0x00000001

#define MAC_FULLDUPLEX_ENABLE          0x00000001
#define MAC_HUGE_ENABLE                0x00000002
#define MAC_CRC_ENABLE                 0x00000010
#define MAC_PAD_ENABLE                 0x00000020
#define MAC_PHY_SPEED_100              0x00000100
#define MAC_PHY_CLOCK_SELECT           0x00000018
#define MAC_MAXF_PACKET_LEN            0x600
#define MAC_IPGR_VALUE                 0x12
#define MAC_IPGT_FULLVALUE             0x15
#define MAC_IPGT_HALFVALUE             0x12
#define MAC_CLRT_VALUE                 0x370F

/* Control Registers definitions */

#define CONTROL_COMMAND_RXENABLE        0x00000001
#define CONTROL_COMMAND_TXENABLE        0x00000002
#define CONTROL_COMMAND_REGRESET        0x00000008
#define CONTROL_COMMAND_RMII            0x00000200
#define CONTROL_COMMAND_FULLDUPLEX      0x00000400

#define CONTROL_STATUS_RX               0x00000001
#define CONTROL_STATUS_TX               0x00000002

/* Receive filter register definitions */

#define FILTER_ACCEPTUNICASTEN          0x00000001
#define FILTER_ACCEPTBROADCASTEN        0x00000002
#define FILTER_ACCEPTMULTICASTEN        0x00000004
#define FILTER_ACCEPTUNICAST_HASHEN     0x00000008
#define FILTER_ACCEPTMULTICAST_HASHEN   0x00000010
#define FILTER_ACCEPTPERFERFECTTEN      0x00000020

/* Module control registers definitions */

#define INTSTATUS_RXERRORINT       0x00000002
#define INTSTATUS_RXFINISHEDINT    0x00000004
#define INTSTATUS_RXDONEINT        0x00000008
#define INTSTATUS_TXERRORINT       0x00000020
#define INTSTATUS_TXFINISHEDINT    0x00000040
#define INTSTATUS_TXDONEINT        0x00000080

/* packet buffer descriptors definitions */

/*
 * Both Tx and RX descriptors and Tx pakage all located in SRAM, the size of SRAM 
 * is 16k, so the Max value of Tx and Rx descriptors are 10.
 */

#define EMAC_TX_DESC_CNT           10
#define EMAC_RX_DESC_CNT           50
#define EMAC_INC_DESC(x, y)        (x) = (((x) + 1) % y)
#define EMAC_MAX_RX                16

#define RXDES_CONTROL_INT           0x80000000
#define RXSTATUS_INFO_CRCERROR      0x00800000
#define RXSTATUS_INFO_SYMBOLERROR   0x01000000
#define RXSTATUS_INFO_LENGTHERROR   0x02000000
#define RXSTATUS_INFO_RANGEERROR    0x04000000
#define RXSTATUS_INFO_ALIGNERROR    0x08000000
#define RXSTATUS_INFO_OVERRUNERROR  0x10000000
#define RXSTATUS_INFO_ERROR         0x80000000
#define RXSTATUS_INFO_LASTFLAG      0x40000000

#define BCTRL_BLEN_MASK             0x000007ff  /* length mask */
#define RXBUF_STAT_BCAST            0x00400000  /* Global broadcast address */
#define RXBUF_STAT_MULTI            0x00200000  /* Multicast hash match*/

#define TXDES_CONTROL_PAD           0x10000000
#define TXDES_CONTROL_CRC           0x20000000
#define TXDES_CONTROL_LAST          0x40000000
#define TXDES_CONTROL_INT           0x80000000

typedef struct emacRxDesc
    {
    volatile UINT32 packet;
    volatile UINT32 control;
    } EMAC_RX_DESC;

typedef struct emacRxStatus
    {
    volatile UINT32 statusInfo;
    volatile UINT32 statusHashCRC;
    }EMAC_RX_STATUS;

typedef struct emacTxDesc
    {
    volatile UINT32 packet;
    volatile UINT32 control;
    } EMAC_TX_DESC;

/* private adapter context structure */

typedef struct emacDrvCtrl
    {
    END_OBJ             emacEndObj;
    VXB_DEVICE_ID       emacDev;
    void *              emacMuxDevCookie;

    JOB_QUEUE_ID        emacJobQueue;
    QJOB                emacTxJob;
    volatile BOOL       emacTxPending;
    QJOB                emacRxJob;
    volatile BOOL       emacRxPending;

    volatile BOOL       emacTxStall;

    BOOL                emacPolling;
    M_BLK_ID            emacPollBuf;
    UINT32              emacIntrs;

    UINT8               emacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    emacCaps;

    END_IFDRVCONF       emacEndStatsConf;
    END_IFCOUNTERS      emacEndStatsCounters;

    UINT32              emacInErrors;
    UINT32              emacInDiscards;
    UINT32              emacInUcasts;
    UINT32              emacInMcasts;
    UINT32              emacInBcasts;
    UINT32              emacInOctets;
    UINT32              emacOutErrors;
    UINT32              emacOutUcasts;
    UINT32              emacOutMcasts;
    UINT32              emacOutBcasts;
    UINT32              emacOutOctets;
    

    /* begin MII/ifmedia required fields */

    END_MEDIALIST *     emacMediaList;
    END_ERR             emacLastError;
    UINT32              emacCurMedia;
    UINT32              emacCurStatus;
    VXB_DEVICE_ID       emacMiiBus;
    VXB_DEVICE_ID       emacMiiDev;
    FUNCPTR             emacMiiPhyRead;
    FUNCPTR             emacMiiPhyWrite;
    int                 emacMiiPhyAddr;

    /* end MII/ifmedia required fields */

    EMAC_TX_DESC *      emacTxDescMem;
    UINT32 *            emacTxStatus;
    EMAC_RX_DESC *      emacRxDescMem;
    EMAC_RX_STATUS *    emacRxStatus;

    UINT16              rxProduceIndex;
    UINT16              rxConsumeIndex;
    UINT16              txProduceIndex;
    UINT16              txConsumeIndex;
        
    UINT32              emacTxProd;
    UINT32              emacTxCons;
    UINT32              emacTxFree;
    UINT32              emacRxIdx;

    SEM_ID              emacMiiSem;

    int                 emacMaxMtu;

    void *              regBase[1];
    void *              handle[1];
    
    M_BLK_ID            emacRxMblk[EMAC_RX_DESC_CNT];
    M_BLK_ID            emacTxMblk[EMAC_TX_DESC_CNT];
    
    UINT32              emacDescMem;
    UINT32              emacMdcDiv;
    UINT32              emacMedia;
    } EMAC_DRV_CTRL;

/* EMAC control module register low level access routines */

#define EMAC_BAR(p)       ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[0]
#define EMAC_HANDLE(p)    ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[0]

#define CSR_READ_4(pDev, addr)             \
        vxbRead32(EMAC_HANDLE(pDev),      \
                  (UINT32 *)((char *)EMAC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)      \
        vxbWrite32(EMAC_HANDLE(pDev),     \
                   (UINT32 *)((char *)EMAC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)    \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)    \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbLpc24xxEndh */
