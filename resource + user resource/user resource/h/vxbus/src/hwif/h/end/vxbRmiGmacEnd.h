/* vxbRmiGmacEnd.h - header file for RMI GMAC RGMII VxBus END driver */

/*
 * Copyright (c) 2007-2008, 2010-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,13jan11,slk  add rxBuffer member to driver cntrl struct
01g,08jan10,h_k  LP64 adaptation.
                 removed unused xlrFreeIn1Spill field in XLR_DRV_CTRL struct.
01f,26jun09,rlg  Change default TX bucket
01e,22may09,wap  Add support for AMP configurations, merge in changes for
                 XLS boards
01d,08may08,wap  Add jumbo frame support
01c,21dec07,wap  Add definition for number of ports
01b,20oct07,wap  Add polled mode support
01a,10oct07,wap  written
*/

#ifndef __INCvxbRmiGmacEndh
#define __INCvxbRmiGmacEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void xlrGmacRegister (void);

#define PROC_XLR		0
#define PROC_XLS		1

#define IF_MEDIA_RGMII		0
#define IF_MEDIA_SGMII		1

#ifndef BSP_VERSION

#define XLR_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))
#define XLR_MAXFRAG		8
#define XLR_MAX_RX		96

#define XLR_NUM_PORTS		8

#define XLR_MTU                 1500
#define XLR_JUMBO_MTU           9000

#define XLR_CLSIZE		1536
#define XLR_NAME		"xlr"
#define XLR_TIMEOUT		10000
#define XLR_INTRS		(XLR_RXINTRS|XLR_TXINTRS)
#define XLR_RXINTRS		(pDrvCtrl->xlrEndRxIntrs)
#ifdef notdef
#define XLR_RXINTRS		\
	(XLRA_ISR_C0EARLYFULL|XLRA_ISR_C1EARLYFULL|	\
	 XLRA_ISR_C2EARLYFULL|XLRA_ISR_C3EARLYFULL)
#endif
#define XLR_TXINTRS		XLRA_ISR_FREEOEARLYFULL

#define XLR_ETHER_ALIGN		2

#define XLR_RX_DESC_CNT		128
#define XLR_TX_DESC_CNT		128

#define XLR_RX_BUCKET		0
#define XLR_TX_BUCKET		4
#define XLR_ERR_BUCKET		XLR_TX_BUCKET /*2*/

#define XLR_RXBMASK (1 << pDrvCtrl->xlrRxBucket)
#define XLR_TXBMASK (1 << pDrvCtrl->xlrTxBucket)
#define XLR_BMASK	(XLR_TXBMASK|XLR_RXBMASK)

/* RGMII GMAC registers */

#define GMAC_MAC_CONF1         0x0000 /* MAC Configuration #1 0x8000_0000 */
#define GMAC_MAC_CONF2         0x0004 /* MAC Configuration #2 0x0000_7000 */
#define GMAC_IPG_IFG           0x0008 /* IPG / IFG 0x4060_5060 */
#define GMAC_HLF_DUP           0x000C /* Half-Duplex 0x00A1_F037 */
#define GMAC_MAX_FRM           0x0010 /* Maximum Frame 0x0000_0600 */
#define GMAC_TEST              0x001C /* Test Register 0x0000_0000 */
#define GMAC_MIIM_CONF         0x0020 /* MII Mgmt: Configuration 0x0000_0000 */
#define GMAC_MIIM_CMD          0x0024 /* MII Mgmt: Command 0x0000_0000 */
#define GMAC_MIIM_ADDR         0x0028 /* MII Mgmt: Address 0x0000_0000 */
#define GMAC_MIIM_CTRL         0x002C /* MII Mgmt: Control 0x0000_0000 */
#define GMAC_MIIM_STAT         0x0030 /* MII Mgmt: Status 0x0000_0000 */
#define GMAC_MIIM_IND          0x0034 /* MII Mgmt: Indicators 0x0000_0000 */
#define GMAC_IO_CTRL           0x0038 /* Interface Control 0x0000_0000 */
#define GMAC_IO_STAT           0x003C /* Interface Status 0x0000_0000 */

/* RX filter registers */

#define GMAC_MAC_ADDR0_LO      0x0140 /* MAC_ADDR0 */
#define GMAC_MAC_ADDR0_HI      0x0144 /* MAC_ADDR0 */
#define GMAC_MAC_ADDR1_LO      0x0148 /* MAC_ADDR1 */
#define GMAC_MAC_ADDR1_HI      0x014C /* MAC_ADDR1 */
#define GMAC_MAC_ADDR2_LO      0x0150 /* MAC_ADDR2 */
#define GMAC_MAC_ADDR2_HI      0x0154 /* MAC_ADDR2 */
#define GMAC_MAC_ADDR3_LO      0x0158 /* MAC_ADDR3 */
#define GMAC_MAC_ADDR3_HI      0x015C /* MAC_ADDR3 */
#define GMAC_MAC_ADDR_MASK0_LO 0x0160 /* MAC_ADDR_MASK0 */
#define GMAC_MAC_ADDR_MASK0_HI 0x0164 /* MAC_ADDR_MASK0 */
#define GMAC_MAC_ADDR_MASK1_LO 0x0168 /* MAC_ADDR_MASK1 */
#define GMAC_MAC_ADDR_MASK1_HI 0x016C /* MAC_ADDR_MASK1 */
#define GMAC_MAC_FILTER_CONFIG 0x0170 /* MAC Filter Configuration */

/* Hash table registers */

#define GMAC_HASH_TABLE_0	0x0180
#define GMAC_HASH_TABLE_1	0x0184
#define GMAC_HASH_TABLE_2	0x0188
#define GMAC_HASH_TABLE_3	0x018C
#define GMAC_HASH_TABLE_4	0x0190
#define GMAC_HASH_TABLE_5	0x0194
#define GMAC_HASH_TABLE_6	0x0198
#define GMAC_HASH_TABLE_7	0x019C
#define GMAC_HASH_TABLE_8	0x01A0
#define GMAC_HASH_TABLE_9	0x01A4
#define GMAC_HASH_TABLE_10	0x01A8
#define GMAC_HASH_TABLE_11	0x01AC
#define GMAC_HASH_TABLE_12	0x01B0
#define GMAC_HASH_TABLE_13	0x01B4
#define GMAC_HASH_TABLE_14	0x01B8
#define GMAC_HASH_TABLE_15	0x01BC

#define GMAC_BAR_SIZE		(GMAC_HASH_TABLE_15 + 4)

/* MAC configuration register 1 */

#define GMAC_MACCONF1_SRST	0x80000000	/* Soft reset */
#define GMAC_MACCONF1_LOOPBK	0x00000100	/* Loopback (1 = enabled) */
#define GMAC_MACCONF1_RXFLOW	0x00000020	/* RX flow control enable */
#define GMAC_MACCONF1_TXFLOW	0x00000010	/* TX flow control enable */
#define GMAC_MACCONF1_RX_SYN	0x00000008	/* Receiver is synchronized */
#define GMAC_MACCONF1_RX_ENB	0x00000004	/* Receiver enable */
#define GMAC_MACCONF1_TX_SYN	0x00000002	/* Transmitter is synchronized */
#define GMAC_MACCONF1_TX_ENB	0x00000001	/* Transmitter enable */

/* MAC configuration register 2 */

#define GMAC_MACCONF2_PREAMLEN	0x0000F000	/* Length of preamble field */
#define GMAC_MACCONF2_IFMODE	0x00000300	/* interface mode */
#define GMAC_MACCONF2_JUMBO	0x00000020	/* jumbo frames */
#define GMAC_MACCONF2_LENCHK	0x00000010	/* Enable RX length checking */
#define GMAC_MACCONF2_PADCRC	0x00000004	/* Enable short frame padding */
#define GMAC_MACCONF2_TXCRC	0x00000002	/* Enable TX CRC */
#define GMAC_MACCONF2_FDX	0x00000001	/* Enable full duplex mode */

#define GMAC_IFMODE_NIBBLE	0x00000100	/* 10/100 */
#define GMAC_IFMODE_BYTE	0x00000200	/* gigabit */

#define GMAC_PREAMLEN(x)	(((x) << 12) & GMAC_MACCONF2_PREAMLEN)

/* Interpacket gap register */

#define GMAC_IPGIFG_NBBIPG_1	0xFF000000	/* Non back-to-back IPG 1 */
#define GMAC_IPGIFG_NBBIPG_2	0x003F0000	/* Non back-to-back IPG 2 */
#define GMAC_IPGIFG_MINIFG_ENF	0x0000FF00	/* Minimum IFG enforcement */
#define GMAC_IPGIFG_BBIPG	0x0000007F	/* Back-to-back IPG */

/* Half duplex register */

#define GMAC_HLFDUP_ABEBT	0x00F00000	/* alt binary exp backoff trunc */
#define GMAC_HLFDUP_ABEBE	0x00080000	/* alt binary exp backoff enb */
#define GMAC_HLFDUP_BP_NB	0x00040000	/* backpressure no backoff */
#define GMAC_HLFDUP_NO_BACKOFF	0x00020000	/* No backoff */
#define GMAC_HLFDUP_EX_DEF	0x00010000	/* enable excess deferal rexmit */
#define GMAC_HLFDUP_RETX_MAX	0x0000F000	/* max retransmit retries */
#define GMAC_HLFDUP_COLLWIN	0x000003FF	/* collision window */

/* Test register */

#define GMAC_TEST_MAX_BACKOFF	0x00000008	/* force max backoff */
#define GMAC_TEST_REGTX_FLOW_EN	0x00000004	/* registered xmit flow enable */
#define GMAC_TEST_PAUSE		0x00000002	/* pause the MAC */
#define GMAC_TEST_SHORT_SLOT	0x00000001	/* shortcut slot time */

/* MIIM_CONF register */

#define GMAC_MIIM_RST		0x80000000	/* Reset MDIO interface */
#define GMAC_MIIM_SCAN_AUTOINC	0x00000020	/* enable auto PHY scanning */
#define GMAC_MIIM_PRE_SUPPR	0x00000010	/* Preamble suppression */
#define GMAC_MIIM_CLKSEL	0x00000007	/* MII clock seletc */

#define MIICLK_DIV_4		0x00000000
#define MIICLK_DIV_4_ALSO	0x00000001
#define MIICLK_DIV_6		0x00000002
#define MIICLK_DIV_8		0x00000003
#define MIICLK_DIV_10		0x00000004
#define MIICLK_DIV_14		0x00000005
#define MIICLK_DIV_20		0x00000006
#define MIICLK_DIV_28		0x00000007

/* MII command register */

#define GMAC_MIIM_CMD_SCAN	0x00000002	/* Initiate PHY scanning */
#define GMAC_MIIM_CMD_READ	0x00000001	/* Issue read cycle */

/* MII address register */

#define GMAC_MIIM_ADDR_PHY	0x00001F00	/* PHY address */
#define GMAC_MIIM_ADDR_REG	0x0000001F	/* register address */

#define GMII_MIIPHYADDR(x)	(((x) << 8) & GMAC_MIIM_ADDR_PHY)
#define GMII_MIIREGADDR(x)	((x) & GMAC_MIIM_ADDR_REG)

/* MII management control register */

#define GMAC_MII_CTRL_WRDATA	0x0000FFFF	/* Data to write to PHY */

/* MII management status register */

#define GMAC_MII_STAT_RDDATA	0x0000FFFF	/* Data to read from PHY */

/* MII managemnt indicator register */

#define GMAC_MII_IND_NOT_VALID	0x00000004	/* data not yet valid */
#define GMAC_MII_IND_SCANNING	0x00000002	/* scanning in progress */
#define GMAC_MII_IND_BUSY	0x00000001	/* read/write in progress */

/* IO control register */

#define GMAC_IOCTRL_TBIMODE	0x08000000	/* TBI mode enable */
#define GMAC_IOCTRL_SGMII_SPD_1	0x04000000	/* Gbps Select */
#define GMAC_IOCTRL_SGMII_SPD_2	0x02000000	/* 100Mbps Select */
#define GMAC_IOCTRL_LHDMODE	0x02000000	/* 10/100 HDX MII enable */
#define GMAC_IOCTRL_PHYMODE	0x01000000	/* PHY mode */
#define GMAC_IOCTRL_EN_JAB_PROT	0x00000001	/* Enable jabber protection */

/* IO status register */

#define GMAC_IOSTAT_EXCESS_DEF	0x00000200	/* TX excess deferral */
#define GMAC_IOSTAT_CLASH	0x00000100	/* rock the casbah */
#define GMAC_IOSTAT_PHYJABBER	0x00000080	/* jabber detected */
#define GMAC_IOSTAT_PHYLINK_OK	0x00000040	/* valid link detected */
#define GMAC_IOSTAT_PHYFDX	0x00000020	/* full duplex link detected */
#define GMAC_IOSTAT_PHYSPEED	0x00000010	/* 1 = 100Mbps, 0 = 10Mbps */
#define GMAC_IOSTAT_MIILINK_FAIL 0x00000008	/* link failure detected */
#define GMAC_IOSTAT_CARR_LOSS	0x00000004	/* loss of carier */
#define	GMAC_IOSTAT_SQE_ERR	0x00000002	/* SQE error */
#define GMAC_IOSTAT_JABBER	0x00000001	/* jabber detected */

/* RX filter configuration register */

#define GMAC_FILTCFG_BCAST_EN	0x00000400	/* broadcast RX enable */
#define GMAC_FILTCFG_PAUSE_EN	0x00000200	/* pause RX enable */
#define GMAC_FILTCFG_ALLMULTI	0x00000100	/* RX all multicast */
#define GMAC_FILTCFG_ALLUCAST	0x00000080	/* RX all unicast */
#define GMAC_FILTCFG_HASH_MCAST	0x00000040	/* hash filter multicast */
#define GMAC_FILTCFG_HASH_UCAST	0x00000020	/* hash filter unicast */
#define GMAC_FILTCFG_MATCH_DISC	0x00000010	/* discard on mac addr match */
#define GMAC_FILTCFG_MAC3_VALID	0x00000008	/* MAC_ADDR3 valid */
#define GMAC_FILTCFG_MAC2_VALID	0x00000004	/* MAC_ADDR2 valid */
#define GMAC_FILTCFG_MAC1_VALID	0x00000002	/* MAC_ADDR1 valid */
#define GMAC_FILTCFG_MAC0_VALID	0x00000001	/* MAC_ADDR0 valid */

#define GAMC_TR64              0x0080 /* Transmit and Receive 64 Byte Frame Counter   */
#define GAMC_TR127             0x0084 /* Transmit and Receive 65 to 127 Byte Frame Counter*/
#define GAMC_TR255             0x0088 /* Transmit and Receive 128 to 255 Byte Frame Counter   */
#define GAMC_TR511             0x008C /* Transmit and Receive 256 to 511 Byte Frame Counter   */
#define GAMC_TR1K              0x0090 /* Transmit and Receive 512 to 1023 Byte Frame Counter  */
#define GAMC_TRMAX             0x0094 /* Transmit and Receive 1024 to 1518 Byte Frame Counter */
#define GAMC_TRMGV             0x0098 /* Transmit and Receive 1519 to 1522 Byte Good VLAN Frame Cnt   */
                                 
#define GMAC_RBYT              0x009C /* Receive Byte Counter */
#define GMAC_RPKT              0x00A0 /* Receive Packet Counter   */
#define GMAC_RFCS              0x00A4 /* Receive FCS Error Counter*/
#define GMAC_RMCA              0x00A8 /* Receive Multicast Packet Counter */
#define GMAC_RBCA              0x00AC /* Receive Broadcast Packet Counter   */
#define GMAC_RXCF              0x00B0 /* Receive Control Frame Packet Counter */
#define GMAC_RXPF              0x00B4 /* Receive PAUSE Frame Packet Counter   */
#define GMAC_RXUO              0x00B8 /* Receive Unknown OP code Counter  */
#define GMAC_RALN              0x00BC /* Receive Alignment Error Counter */
#define GMAC_RFLR              0x00C0 /* Receive Frame Length Error Counter   */
#define GMAC_RCDE              0x00C4 /* Receive Code Error Counter   */
#define GMAC_RCSE              0x00C8 /* Receive Carrier Sense Error Counter  */
#define GMAC_RUND              0x00CC /* Receive Undersize Packet Counter */
#define GMAC_ROVR              0x00D0 /* Receive Oversize Packet Counter  */
#define GMAC_RFRG              0x00D4 /* Receive Fragments Counter*/
#define GMAC_RJBR              0x00D8 /* Receive Jabber Counter   */
#define GMAC_RDRP              0x00DC /* Receive Drop 0   */
                                 
#define GMAC_TBYT              0x00E0 /* Transmit Byte Counter*/
#define GMAC_TPKT              0x00E4 /* Transmit Packet Counter  */
#define GMAC_TMCA              0x00E8 /* Transmit Multicast Packet Counter*/
#define GMAC_TBCA              0x00EC /* Transmit Broadcast Packet Counter*/
#define GMAC_TXPF              0x00F0 /* Transmit PAUSE Control Frame Counter */
#define GMAC_TDFR              0x00F4 /* Transmit Deferral Packet Counter */
#define GMAC_TEDF              0x00F8 /* Transmit Excessive Deferral Packet Counter   */
#define GMAC_TSCL              0x00FC /* Transmit Single Collision Packet Counter */
#define GMAC_TMCL              0x0100 /* Transmit Multiple Collision Packet Counter   */
#define GMAC_TLCL              0x0104 /* Transmit Late Collision Packet Counter   */
#define GMAC_TXCL              0x0108 /* Transmit Excessive Collision Packet Counter  */
#define GMAC_TNCL              0x010C /* Transmit Total Collision Counter */
#define GMAC_TPFH              0x0110 /* Transmit PAUSE Frames Honored Counter*/
#define GMAC_TDRP              0x0114 /* Transmit Drop Frame Counter  */
#define GMAC_TJBR              0x0118 /* Transmit Jabber Frame Counter*/
#define GMAC_TFCS              0x011C /* Transmit FCS Error Counter   */
#define GMAC_TXCF              0x0120 /* Transmit Control Frame Counter   */
#define GMAC_TOVR              0x0124 /* Transmit Oversize Frame Counter  */
#define GMAC_TUND              0x0128 /* Transmit Undersize Frame Counter   */
#define GMAC_TFRG              0x012C /* Transmit Fragments Frame Counter   */
        
#define GMAC_TIMEOUT           100000        /* GMAC operation timeout value */


/*
 * The RGMII GMACs have the following bucket IDs allocated to them in
 * the fast messaging network (FNM).
 */

#define GMAC1_MSG_STID_RXJFREE	80	/* jumbo frames, not supported */
#define GMAC1_MSG_STID_FREEOUT	80	/* accel -> CPU source ID */
#define GMAC1_MSG_STID_RXFREE	81	/* populate RX free queue */
#define GMAC1_MSG_STID_TX0	82	/* TX on port 0 */
#define GMAC1_MSG_STID_TX1	83	/* TX on port 1 */
#define GMAC1_MSG_STID_TX2	84	/* TX on port 2 */
#define GMAC1_MSG_STID_TX3	85	/* TX on port 3 */
#define GMAC1_MSG_STID_RXJFREE1	86	/* jumbo frames, not supported */
#define GMAC1_MSG_STID_RXFREE1	87	/* used in split mode only */

#define GMAC0_MSG_STID_RXJFREE	96	/* jumbo frames, not supported */
#define GMAC0_MSG_STID_FREEOUT	96	/* accel -> CPU source ID */
#define GMAC0_MSG_STID_RXFREE	97	/* populate RX free queue */
#define GMAC0_MSG_STID_TX0	98	/* TX on port 0 */
#define GMAC0_MSG_STID_TX1	99	/* TX on port 1 */
#define GMAC0_MSG_STID_TX2	100	/* TX on port 2 */
#define GMAC0_MSG_STID_TX3	101	/* TX on port 3 */
#define GMAC0_MSG_STID_RXJFREE1	102	/* jumbo frames, not supported */
#define GMAC0_MSG_STID_RXFREE1	103	/* used in split mode only */

/*
 * Message are allowed to have a software code field. We currently
 * don't use this, but we initialize it for completeness.
 */

#define GMAC_MSG_CODE_RX	0xAB
#define GMAC_MSG_CODE_TX	0xCD

typedef struct endNetPool
    {
    NET_POOL            pool;
    void                * pMblkMemArea;
    void                * pClMemArea;
    } END_NET_POOL;

#define xlrEndPoolTupleFree(x) netMblkClChainFree(x)
#define xlrEndPoolTupleGet(x)      \
        netTupleGet((x), (x)->clTbl[0]->clSize, M_DONTWAIT, MT_DATA, 0)

/*
 * We use TX descriptor arrays, and they must be cache aligned. We
 * combine 8 of them together. This should be enough to handle
 * most scatter/gather combinations without making the descriptor
 * clusters too big.
 */

typedef struct gmac_tx_desc {
	XLRA_TX_DESC_64	xlr_frag[XLR_MAXFRAG];
} GMAC_TX_DESC;

/*
 * Private adapter context structure.
 */

typedef struct xlr_drv_ctrl
    {
    END_OBJ		xlrEndObj;
    VXB_DEVICE_ID	xlrEndDev;
    void *		xlrBar;
    void *		xlrHandle;
    void *		xlrEndMuxDevCookie;

    int			xlrPort;

    int			xlrRxBucket;
    int			xlrTxBucket;
    int			xlrMaster;
    int			xlrRxStId;
    int			xlrTxStId;
    int			xlrRxBuffer;
    int			xlrRxBufferSize;
    int			xlrRxBufferAmpMode;

    void *		xlrPool;

    JOB_QUEUE_ID	xlrJobQueue;
    QJOB		xlrEndIntJob;
    atomic32_t		xlrEndIntPending;

    int			xlrTxProd;
    int			xlrTxCons;
    int			xlrTxFree; 
    volatile BOOL	xlrTxStall;
    UINT16		xlrTxThresh;

    QJOB		xlrEndLinkJob;
    volatile BOOL	xlrEndLinkPending;

    BOOL		xlrEndPolling;
    M_BLK_ID		xlrEndPollBuf;
    UINT32		xlrEndIntMask;
    UINT32		xlrEndIntrs;
    UINT32		xlrEndRxIntrs;

    UINT8		xlrAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	xlrEndCaps;

    END_IFDRVCONF	xlrEndStatsConf;
    END_IFCOUNTERS	xlrEndStatsCounters;

    GMAC_TX_DESC *	xlrTxDescMem;

    M_BLK_ID		xlrTxMblk[XLR_TX_DESC_CNT];

    UINT64 *		xlrFreeInSpill;
    UINT64 *		xlrFreeOutSpill;
    UINT64 *		xlrC0Spill;
    UINT64 *		xlrC1Spill;
    UINT64 *		xlrC2Spill;
    UINT64 *		xlrC3Spill;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	xlrMediaList;
    END_ERR		xlrLastError;
    UINT32		xlrCurMedia;
    UINT32		xlrCurStatus;
    VXB_DEVICE_ID	xlrMiiBus;
    VXB_DEVICE_ID	xlrMiiDev;
    STATUS		(* xlrMiiPhyRead)(VXB_DEVICE_ID, UINT8, UINT8,
                                          UINT16 *);
    STATUS		(* xlrMiiPhyWrite)(VXB_DEVICE_ID, UINT8, UINT8,
                                           UINT16);
    UINT32		xlrMiiPhyAddr;
    /* End MII/ifmedia required fields */

    int			xlrMaxMtu;

    int			xlrPcsAddr;
    int			xlrDevType;
    int			xlrMediaType;

    SEM_ID		xlrDevSem;
    } XLR_DRV_CTRL;


#define XLR_BAR(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrBar
#define XLR_HANDLE(p)   ((XLR_DRV_CTRL *)(p)->pDrvCtrl)->xlrHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (XLR_HANDLE(pDev), (UINT32 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (XLR_HANDLE(pDev),                             \
        (UINT32 *)((char *)XLR_BAR(pDev) + addr), (UINT32)(data))

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (XLR_HANDLE(pDev), (UINT16 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (XLR_HANDLE(pDev),                             \
        (UINT16 *)((char *)XLR_BAR(pDev) + addr), (UINT16)(data))

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (XLR_HANDLE(pDev), (UINT8 *)((char *)XLR_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (XLR_HANDLE(pDev),                              \
        (UINT8 *)((char *)XLR_BAR(pDev) + addr), (UINT8)(data))


#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbRmiGmacEndh */
