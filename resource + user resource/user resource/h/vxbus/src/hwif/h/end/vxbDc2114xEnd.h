/* vxbDc2114xEnd.h - header file for DEC/Intel 2114x 10/100 ethernet driver */

/*
 * Copyright (c) 2007-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,20feb09,wap  Add support for BNC media on 21143 chips
01a,28aug07,wap  written
*/

#ifndef __INCvxbDc2114xEndh
#define __INCvxbDc2114xEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbDc2114xEndRegister (void);

#define DC_MEDIA_MII		1	/* 10/100 MII autoneg (default) */
#define DC_MEDIA_NWAY_FULL	2	/* 21143/5 internal 10/100 NWAY */
#define DC_MEDIA_NWAY_LOW	3	/* 21143/5 10Mbps NWAY only */
#define DC_MEDIA_10BASET	4	/* 10baseT, no NWAY */
#define DC_MEDIA_AUI		5	/* AUI */
#define DC_MEDIA_BNC		6	/* BNC */

#ifndef BSP_VERSION

#define DC_VENDORID_DEC		0x1011
#define DC_VENDORID_INTEL	0x8086

#define DC_DEVICEID_21041	0x0014
#define DC_DEVICEID_21140	0x0009
#define DC_DEVICEID_21143	0x0019
#define DC_DEVICEID_21145	0x0039

/* DEC 21140/21143 register definitions */

#define DC_BUSCTL               0x00    /* bus control */
#define DC_TXSTART              0x08    /* tx start demand */
#define DC_RXSTART              0x10    /* rx start demand */
#define DC_RXADDR               0x18    /* rx descriptor list start addr */
#define DC_TXADDR               0x20    /* tx descriptor list start addr */
#define DC_ISR                  0x28    /* interrupt status register */
#define DC_NETCFG               0x30    /* network config register */
#define DC_IMR                  0x38    /* interrupt mask */
#define DC_FRAMESDISCARDED      0x40    /* # of discarded frames */
#define DC_SIO                  0x48    /* MII and ROM/EEPROM access */
#define DC_ROM                  0x50    /* ROM programming address */
#define DC_TIMER                0x58    /* general timer */
#define DC_SIASTS               0x60    /* SIA status */
#define DC_SIACONN              0x68    /* SIA connectivity */
#define DC_SIATR                0x70    /* SIA transmit and receive */
#define DC_SIAGP                0x78    /* SIA and general purpose port */


/* Bus control register */

#define DC_BUSCTL_RESET         0x00000001
#define DC_BUSCTL_ARBITRATION   0x00000002
#define DC_BUSCTL_SKIPLEN       0x0000007C
#define DC_BUSCTL_BUF_BIGENDIAN 0x00000080
#define DC_BUSCTL_BURSTLEN      0x00003F00
#define DC_BUSCTL_CACHEALIGN    0x0000C000
#define DC_BUSCTL_TXPOLL        0x000E0000
#define DC_BUSCTL_DBO           0x00100000
#define DC_BUSCTL_MRME          0x00200000
#define DC_BUSCTL_MRLE          0x00800000
#define DC_BUSCTL_MWIE          0x01000000
#define DC_BUSCTL_ONNOW_ENB     0x04000000

#define DC_SKIPLEN_1LONG        0x00000004
#define DC_SKIPLEN_2LONG        0x00000008
#define DC_SKIPLEN_3LONG        0x00000010
#define DC_SKIPLEN_4LONG        0x00000020
#define DC_SKIPLEN_5LONG        0x00000040

#define DC_CACHEALIGN_NONE      0x00000000
#define DC_CACHEALIGN_8LONG     0x00004000
#define DC_CACHEALIGN_16LONG    0x00008000
#define DC_CACHEALIGN_32LONG    0x0000C000

#define DC_BURSTLEN_USECA       0x00000000
#define DC_BURSTLEN_1LONG       0x00000100
#define DC_BURSTLEN_2LONG       0x00000200
#define DC_BURSTLEN_4LONG       0x00000400
#define DC_BURSTLEN_8LONG       0x00000800
#define DC_BURSTLEN_16LONG      0x00001000
#define DC_BURSTLEN_32LONG      0x00002000

#define DC_TXPOLL_OFF           0x00000000
#define DC_TXPOLL_1             0x00020000
#define DC_TXPOLL_2             0x00040000
#define DC_TXPOLL_3             0x00060000
#define DC_TXPOLL_4             0x00080000
#define DC_TXPOLL_5             0x000A0000
#define DC_TXPOLL_6             0x000C0000
#define DC_TXPOLL_7             0x000E0000

/* Interrupt status register */

#define DC_ISR_TX_OK            0x00000001
#define DC_ISR_TX_IDLE          0x00000002
#define DC_ISR_TX_NOBUF         0x00000004
#define DC_ISR_TX_JABBERTIMEO   0x00000008
#define DC_ISR_LINKGOOD         0x00000010
#define DC_ISR_TX_UNDERRUN      0x00000020
#define DC_ISR_RX_OK            0x00000040
#define DC_ISR_RX_NOBUF         0x00000080
#define DC_ISR_RX_READ          0x00000100
#define DC_ISR_RX_WATDOGTIMEO   0x00000200
#define DC_ISR_TX_EARLY         0x00000400
#define DC_ISR_TIMER_EXPIRED    0x00000800
#define DC_ISR_LINKFAIL         0x00001000
#define DC_ISR_BUS_ERR          0x00002000
#define DC_ISR_RX_EARLY         0x00004000
#define DC_ISR_ABNORMAL         0x00008000
#define DC_ISR_NORMAL           0x00010000
#define DC_ISR_RX_STATE         0x000E0000
#define DC_ISR_TX_STATE         0x00700000
#define DC_ISR_BUSERRTYPE       0x03800000
#define DC_ISR_100MBPSLINK      0x08000000
#define DC_ISR_MAGICKPACK       0x10000000

#define DC_RXSTATE_STOPPED      0x00000000      /* 000 - Stopped */
#define DC_RXSTATE_FETCH        0x00020000      /* 001 - Fetching descriptor */
#define DC_RXSTATE_ENDCHECK     0x00040000      /* 010 - check for rx end */
#define DC_RXSTATE_WAIT         0x00060000      /* 011 - waiting for packet */
#define DC_RXSTATE_SUSPEND      0x00080000      /* 100 - suspend rx */
#define DC_RXSTATE_CLOSE        0x000A0000      /* 101 - close tx desc */
#define DC_RXSTATE_FLUSH        0x000C0000      /* 110 - flush from FIFO */
#define DC_RXSTATE_DEQUEUE      0x000E0000      /* 111 - dequeue from FIFO */

#define DC_TXSTATE_RESET        0x00000000      /* 000 - reset */
#define DC_TXSTATE_FETCH        0x00100000      /* 001 - fetching descriptor */
#define DC_TXSTATE_WAITEND      0x00200000      /* 010 - wait for tx end */
#define DC_TXSTATE_READING      0x00300000      /* 011 - read and enqueue */
#define DC_TXSTATE_RSVD         0x00400000      /* 100 - reserved */
#define DC_TXSTATE_SETUP        0x00500000      /* 101 - setup packet */
#define DC_TXSTATE_SUSPEND      0x00600000      /* 110 - suspend tx */
#define DC_TXSTATE_CLOSE        0x00700000      /* 111 - close tx desc */

/* Network configuration register */

#define DC_NETCFG_RX_HASHPERF   0x00000001
#define DC_NETCFG_RX_ON         0x00000002
#define DC_NETCFG_RX_HASHONLY   0x00000004
#define DC_NETCFG_RX_BADFRAMES  0x00000008
#define DC_NETCFG_RX_INVFILT    0x00000010
#define DC_NETCFG_BACKOFFCNT    0x00000020
#define DC_NETCFG_RX_PROMISC    0x00000040
#define DC_NETCFG_RX_ALLMULTI   0x00000080
#define DC_NETCFG_FULLDUPLEX    0x00000200
#define DC_NETCFG_LOOPBACK      0x00000C00
#define DC_NETCFG_FORCECOLL     0x00001000
#define DC_NETCFG_TX_ON         0x00002000
#define DC_NETCFG_TX_THRESH     0x0000C000
#define DC_NETCFG_TX_BACKOFF    0x00020000
#define DC_NETCFG_PORTSEL       0x00040000      /* 0 == AUI/10BT, 1 == MII */
#define DC_NETCFG_HEARTBEAT     0x00080000
#define DC_NETCFG_STORENFWD     0x00200000
#define DC_NETCFG_SPEEDSEL      0x00400000      /* 1 == 10, 0 == 100 */
#define DC_NETCFG_PCS           0x00800000
#define DC_NETCFG_SCRAMBLER     0x01000000
#define DC_NETCFG_NO_RXCRC      0x02000000
#define DC_NETCFG_RX_ALL        0x40000000
#define DC_NETCFG_CAPEFFECT     0x80000000

#define DC_OPMODE_NORM          0x00000000
#define DC_OPMODE_INTLOOP       0x00000400
#define DC_OPMODE_EXTLOOP       0x00000800

#if 0
#define DC_TXTHRESH_72BYTES     0x00000000
#define DC_TXTHRESH_96BYTES     0x00004000
#define DC_TXTHRESH_128BYTES    0x00008000
#define DC_TXTHRESH_160BYTES    0x0000C000
#endif

#define DC_TXTHRESH_MIN         0x00000000
#define DC_TXTHRESH_INC         0x00004000
#define DC_TXTHRESH_MAX         0x0000C000

#define DC_PORT_10BTAUI		0x00000000
#define DC_PORT_MIISYM		0x00040000

/* Interrupt mask register */

#define DC_IMR_TX_OK            0x00000001
#define DC_IMR_TX_IDLE          0x00000002
#define DC_IMR_TX_NOBUF         0x00000004
#define DC_IMR_TX_JABBERTIMEO   0x00000008
#define DC_IMR_LINKGOOD         0x00000010
#define DC_IMR_TX_UNDERRUN      0x00000020
#define DC_IMR_RX_OK            0x00000040
#define DC_IMR_RX_NOBUF         0x00000080
#define DC_IMR_RX_READ          0x00000100
#define DC_IMR_RX_WATDOGTIMEO   0x00000200
#define DC_IMR_TX_EARLY         0x00000400
#define DC_IMR_TIMER_EXPIRED    0x00000800
#define DC_IMR_LINKFAIL         0x00001000
#define DC_IMR_BUS_ERR          0x00002000
#define DC_IMR_RX_EARLY         0x00004000
#define DC_IMR_ABNORMAL         0x00008000
#define DC_IMR_NORMAL           0x00010000
#define DC_IMR_100MBPSLINK      0x08000000
#define DC_IMR_MAGICKPACK       0x10000000

/* Serial I/O (EEPROM/ROM) register */

#define DC_SIO_EE_CS            0x00000001      /* EEPROM chip select */
#define DC_SIO_EE_CLK           0x00000002      /* EEPROM clock */
#define DC_SIO_EE_DATAIN        0x00000004      /* EEPROM data output */
#define DC_SIO_EE_DATAOUT       0x00000008      /* EEPROM data input */
#define DC_SIO_ROMDATA4         0x00000010
#define DC_SIO_ROMDATA5         0x00000020
#define DC_SIO_ROMDATA6         0x00000040
#define DC_SIO_ROMDATA7         0x00000080
#define DC_SIO_EESEL            0x00000800
#define DC_SIO_ROMSEL           0x00001000
#define DC_SIO_ROMCTL_WRITE     0x00002000
#define DC_SIO_ROMCTL_READ      0x00004000
#define DC_SIO_MII_CLK          0x00010000      /* MDIO clock */
#define DC_SIO_MII_DATAOUT      0x00020000      /* MDIO data out */
#define DC_SIO_MII_DIR          0x00040000      /* MDIO dir */
#define DC_SIO_MII_DATAIN       0x00080000      /* MDIO data in */

/* 9346 EEPROM commands */
#define DC_9346_WRITE           0x5
#define DC_9346_READ            0x6
#define DC_9346_ERASE           0x7

#define DC_EE_NODEADDR_OFFSET   0x70
#define DC_EE_NODEADDR          10

/* General purpose timer register */

#define DC_TIMER_VALUE          0x0000FFFF
#define DC_TIMER_CONTINUOUS     0x00010000

/* SIA status register */

#define DC_SIASTS_MIIACT        0x00000001 /* MII port activity */
#define DC_SIASTS_LS100         0x00000002 /* link status of 100baseTX */
#define DC_SIASTS_LS10          0x00000004 /* link status of 10baseT */
#define DC_SIASTS_AUTOPOLARITY  0x00000008
#define DC_SIASTS_AUIACT        0x00000100 /* AUI activity */
#define DC_SIASTS_10BTACT       0x00000200 /* 10baseT activity */
#define DC_SIASTS_NSN           0x00000400 /* non-stable FLPs detected */
#define DC_SIASTS_REMFAULT      0x00000800
#define DC_SIASTS_ANEGSTAT      0x00007000
#define DC_SIASTS_LP_CDC_NWAY   0x00008000 /* link partner supports NWAY */
#define DC_SIASTS_LPCODEWORD    0xFFFF0000 /* link partner's code word */

#define DC_ASTAT_DISABLE        0x00000000
#define DC_ASTAT_TXDISABLE      0x00001000
#define DC_ASTAT_ABDETECT       0x00002000
#define DC_ASTAT_ACKDETECT      0x00003000
#define DC_ASTAT_CMPACKDETECT   0x00004000
#define DC_ASTAT_AUTONEGCMP     0x00005000
#define DC_ASTAT_LINKCHECK      0x00006000

/* SIA connectivity register */

#define DC_SIACONN_RESET	0x00000001 /* 0 = reset */
#define DC_SIACONN_CAC		0x00000004 /* CSR autoconfig (21041 only)*/
#define DC_SIACONN_AUI		0x00000008 /* AUI or 10baseT */
#define DC_SIACONN_SDM		0x0000FFF0 /* Diagnostic mode bits */

/*
 * For the 21041, the diagnostic mode bits must be set
 * to 0xEF0, per the manual. No explanation is given as to
 * what these bits actually mean.
 */

#define DC_SIADIAG_NORMAL	0x0000EF00

/* SIA transmit and receive register */

#define DC_SIATR_ENCODER_ENB    0x00000001
#define DC_SIATR_LOOPBACK       0x00000002
#define DC_SIATR_DRIVER_ENB     0x00000004
#define DC_SIATR_LNKPULSE_ENB   0x00000008
#define DC_SIATR_CPEN		0x00000030
#define DC_SIATR_HALFDUPLEX     0x00000040
#define DC_SIATR_AUTONEGENBL    0x00000080
#define DC_SIATR_RX_SQUELCH     0x00000100
#define DC_SIATR_COLL_SQUELCH   0x00000200
#define DC_SIATR_COLL_DETECT    0x00000400
#define DC_SIATR_SQE_ENB        0x00000800
#define DC_SIATR_LINKTEST       0x00001000
#define DC_SIATR_AUTOPOLARITY   0x00002000
#define DC_SIATR_SET_POL_PLUS   0x00004000
#define DC_SIATR_AUTOSENSE      0x00008000      /* 10bt/AUI autosense */
#define DC_SIATR_100BTXHALF     0x00010000
#define DC_SIATR_100BTXFULL     0x00020000
#define DC_SIATR_100BT4         0x00040000

#define DC_CPEN_DISABLE0	0x00000000
#define DC_CPEN_DISABLE1	0x00000010
#define DC_CPEN_HIGH		0x00000020
#define DC_CPEN_NORMAL		0x00000030

/* SIA and general purpose register */

#define DC_SIAGP_JABBERDIS      0x00000001
#define DC_SIAGP_HOSTUNJAB      0x00000002
#define DC_SIAGP_JABBERCLK      0x00000004
#define DC_SIAGP_ABM            0x00000008
#define DC_SIAGP_RXSIAGPDIS     0x00000010
#define DC_SIAGP_RXSIAGPCLK     0x00000020
#define DC_SIAGP_MUSTBEZERO     0x00000100
#define DC_SIAGP_GPIO0          0x00010000
#define DC_SIAGP_GPIO1          0x00020000
#define DC_SIAGP_GPIO2          0x00040000
#define DC_SIAGP_GPIO3          0x00080000
#define DC_SIAGP_AUIBNC         0x00100000
#define DC_SIAGP_LED_ACTIVITY   0x00200000
#define DC_SIAGP_LED_RX_MATCH   0x00400000
#define DC_SIAGP_LED_LINK       0x00800000
#define DC_SIAGP_CTLWREN        0x08000000

/* Size of a setup frame */

#define DC_SFRAME_LEN           192

#if _BYTE_ORDER == _BIG_ENDIAN
#define DC_SP_MAC(x)            ((x) << 16)
#else
#define DC_SP_MAC(x)            (x)
#endif

/* 2114x DMA descriptor structure */

typedef struct dc_desc
    {
    volatile UINT32	dcSts;
    volatile UINT32	dcCtl;
    volatile UINT32	dcPtr1;
    volatile UINT32	dcPtr2;
    } DC_DESC;

#define dcData dcPtr1
#define dcNext dcPtr2

/* RX status word */

#define DC_RXSTAT_FIFOOFLOW     0x00000001
#define DC_RXSTAT_CRCERR        0x00000002
#define DC_RXSTAT_DRIBBLE       0x00000004
#define DC_RXSTAT_MIIERE        0x00000008
#define DC_RXSTAT_WATCHDOG      0x00000010
#define DC_RXSTAT_FRAMETYPE     0x00000020      /* 0 == IEEE 802.3 */
#define DC_RXSTAT_COLLSEEN      0x00000040
#define DC_RXSTAT_GIANT         0x00000080
#define DC_RXSTAT_LASTFRAG      0x00000100
#define DC_RXSTAT_FIRSTFRAG     0x00000200
#define DC_RXSTAT_MULTICAST     0x00000400
#define DC_RXSTAT_RUNT          0x00000800
#define DC_RXSTAT_RXTYPE        0x00003000
#define DC_RXSTAT_DE            0x00004000
#define DC_RXSTAT_RXERR         0x00008000
#define DC_RXSTAT_RXLEN         0x3FFF0000
#define DC_RXSTAT_OWN           0x80000000

#define DC_RXBYTES(x)           ((x & DC_RXSTAT_RXLEN) >> 16)

/* RX control word */

#define DC_RXCTL_BUFLEN1        0x00000FFF
#define DC_RXCTL_BUFLEN2        0x00FFF000
#define DC_RXCTL_RLINK          0x01000000
#define DC_RXCTL_RLAST          0x02000000

/* TX status word */

#define DC_TXSTAT_DEFER         0x00000001
#define DC_TXSTAT_UNDERRUN      0x00000002
#define DC_TXSTAT_LINKFAIL      0x00000003
#define DC_TXSTAT_COLLCNT       0x00000078
#define DC_TXSTAT_SQE           0x00000080
#define DC_TXSTAT_EXCESSCOLL    0x00000100
#define DC_TXSTAT_LATECOLL      0x00000200
#define DC_TXSTAT_NOCARRIER     0x00000400
#define DC_TXSTAT_CARRLOST      0x00000800
#define DC_TXSTAT_JABTIMEO      0x00004000
#define DC_TXSTAT_ERRSUM        0x00008000
#define DC_TXSTAT_OWN           0x80000000

/* TX control word */

#define DC_TXCTL_BUFLEN1        0x000007FF
#define DC_TXCTL_BUFLEN2        0x003FF800
#define DC_TXCTL_FILTTYPE0      0x00400000
#define DC_TXCTL_PAD            0x00800000
#define DC_TXCTL_TLINK          0x01000000
#define DC_TXCTL_TLAST          0x02000000
#define DC_TXCTL_NOCRC          0x04000000
#define DC_TXCTL_SETUP          0x08000000
#define DC_TXCTL_FILTTYPE1      0x10000000
#define DC_TXCTL_FIRSTFRAG      0x20000000
#define DC_TXCTL_LASTFRAG       0x40000000
#define DC_TXCTL_FINT           0x80000000

#define DC_FILTER_PERFECT       0x00000000
#define DC_FILTER_HASHPERF      0x00400000
#define DC_FILTER_INVERSE       0x10000000
#define DC_FILTER_HASHONLY      0x10400000



#define DC_MTU 1500
#define DC_JUMBO_MTU 9000
#define DC_CLSIZE	1536
#define DC_NAME	"dc"
#define DC_TIMEOUT 10000
#define DC_INTRS	(DC_RXINTRS|DC_TXINTRS|DC_LINKINTRS|	\
                         DC_ISR_NORMAL|DC_ISR_ABNORMAL)
#define DC_RXINTRS	(DC_ISR_RX_OK|DC_ISR_RX_NOBUF)
#define DC_TXINTRS	(DC_ISR_TX_OK|DC_ISR_TX_IDLE|	\
     DC_ISR_TX_NOBUF|DC_ISR_TX_UNDERRUN)

#define DC_LINKINTRS	(DC_ISR_LINKGOOD|DC_ISR_LINKFAIL)

#define DC_MAXFRAG	16
#define DC_MAX_RX	16
#define DC_RX_DESC_CNT	32
#define DC_TX_DESC_CNT	32

#if (CPU_FAMILY == I80X86) || (CPU_FAMILY == PPC) || (CPU_FAMILY == COLDFIRE)
#define DC_ADJ(x)
#else
#define DC_RX_FIXUP
#define DC_ADJ(x)      (x)->m_data += 8
#endif

#define DC_INC_DESC(x, y)	(x) = ((x) + 1) % (y)

/*
 * PI low memory base and low I/O base register, and
 * other PCI registers.
 */
 
#define DC_PCI_CFID             0x00    /* Id */
#define DC_PCI_CFCS             0x04    /* Command and status */
#define DC_PCI_CFRV             0x08    /* Revision */
#define DC_PCI_CFLT             0x0C    /* Latency timer */
#define DC_PCI_CFBIO            0x10    /* Base I/O address */
#define DC_PCI_CFBMA            0x14    /* Base memory address */
#define DC_PCI_CCIS             0x28    /* Card info struct */
#define DC_PCI_CSID             0x2C    /* Subsystem ID */
#define DC_PCI_CBER             0x30    /* Expansion ROM base address */   
#define DC_PCI_CCAP             0x34    /* Caps pointer - PD/TD chip only */
#define DC_PCI_CFIT             0x3C    /* Interrupt */
#define DC_PCI_CFDD             0x40    /* Device and driver area */
#define DC_PCI_CWUA0            0x44    /* Wake-Up LAN addr 0 */
#define DC_PCI_CWUA1            0x48    /* Wake-Up LAN addr 1 */
#define DC_PCI_SOP0             0x4C    /* SecureON passwd 0 */
#define DC_PCI_SOP1             0x50    /* SecureON passwd 1 */
#define DC_PCI_CWUC             0x54    /* Configuration Wake-Up cmd */
#define DC_PCI_CCID             0xDC    /* Capability ID - PD/TD only */   
#define DC_PCI_CPMC             0xE0    /* Pwrmgmt ctl & sts - PD/TD only */

/* PCI ID register */
#define DC_CFID_VENDOR          0x0000FFFF
#define DC_CFID_DEVICE          0xFFFF0000
 
/* PCI command/status register */
#define DC_CFCS_IOSPACE         0x00000001 /* I/O space enable */
#define DC_CFCS_MEMSPACE        0x00000002 /* memory space enable */
#define DC_CFCS_BUSMASTER       0x00000004 /* bus master enable */
#define DC_CFCS_MWI_ENB         0x00000010 /* mem write and inval enable */
#define DC_CFCS_PARITYERR_ENB   0x00000040 /* parity error enable */
#define DC_CFCS_SYSERR_ENB      0x00000100 /* system error enable */
#define DC_CFCS_NEWCAPS         0x00100000 /* new capabilities */
#define DC_CFCS_FAST_B2B        0x00800000 /* fast back-to-back capable */
#define DC_CFCS_DATAPARITY      0x01000000 /* Parity error report */
#define DC_CFCS_DEVSELTIM       0x06000000 /* devsel timing */
#define DC_CFCS_TGTABRT         0x10000000 /* received target abort */
#define DC_CFCS_MASTERABRT      0x20000000 /* received master abort */
#define DC_CFCS_SYSERR          0x40000000 /* asserted system error */
#define DC_CFCS_PARITYERR       0x80000000 /* asserted parity error */

/* PCI revision register */
#define DC_CFRV_STEPPING        0x0000000F
#define DC_CFRV_REVISION        0x000000F0
#define DC_CFRV_SUBCLASS        0x00FF0000
#define DC_CFRV_BASECLASS       0xFF000000

#define DC_21143_PB_REV         0x00000030
#define DC_21143_TB_REV         0x00000030
#define DC_21143_PC_REV         0x00000030
#define DC_21143_TC_REV         0x00000030
#define DC_21143_PD_REV         0x00000041
#define DC_21143_TD_REV         0x00000041

/* PCI latency timer register */
#define DC_CFLT_CACHELINESIZE   0x000000FF
#define DC_CFLT_LATENCYTIMER    0x0000FF00

/* PCI subsystem ID register */
#define DC_CSID_VENDOR          0x0000FFFF
#define DC_CSID_DEVICE          0xFFFF0000

/* PCI cababilities pointer */
#define DC_CCAP_OFFSET          0x000000FF

/* PCI interrupt config register */
#define DC_CFIT_INTLINE         0x000000FF
#define DC_CFIT_INTPIN          0x0000FF00
#define DC_CFIT_MIN_GNT         0x00FF0000
#define DC_CFIT_MAX_LAT         0xFF000000

/* PCI capability register */
#define DC_CCID_CAPID           0x000000FF
#define DC_CCID_NEXTPTR         0x0000FF00
#define DC_CCID_PM_VERS         0x00070000
#define DC_CCID_PME_CLK         0x00080000
#define DC_CCID_DVSPEC_INT      0x00200000
#define DC_CCID_STATE_D1        0x02000000
#define DC_CCID_STATE_D2        0x04000000
#define DC_CCID_PME_D0          0x08000000
#define DC_CCID_PME_D1          0x10000000
#define DC_CCID_PME_D2          0x20000000
#define DC_CCID_PME_D3HOT       0x40000000
#define DC_CCID_PME_D3COLD      0x80000000

/* PCI power management control/status register */
#define DC_CPMC_STATE           0x00000003
#define DC_CPMC_PME_ENB         0x00000100
#define DC_CPMC_PME_STS         0x00008000

#define DC_PSTATE_D0            0x0
#define DC_PSTATE_D1            0x1
#define DC_PSTATE_D2            0x2
#define DC_PSTATE_D3            0x3

/* Device specific region */
/* Configuration and driver area */
#define DC_CFDD_DRVUSE          0x0000FFFF
#define DC_CFDD_SNOOZE_MODE     0x40000000
#define DC_CFDD_SLEEP_MODE      0x80000000

/* Configuration wake-up command register */
#define DC_CWUC_MUST_BE_ZERO    0x00000001
#define DC_CWUC_SECUREON_ENB    0x00000002
#define DC_CWUC_FORCE_WUL       0x00000004
#define DC_CWUC_BNC_ABILITY     0x00000008
#define DC_CWUC_AUI_ABILITY     0x00000010
#define DC_CWUC_TP10_ABILITY    0x00000020
#define DC_CWUC_MII_ABILITY     0x00000040
#define DC_CWUC_SYM_ABILITY     0x00000080
#define DC_CWUC_LOCK            0x00000100

typedef struct dc_drv_ctrl
    {
    END_OBJ		dcEndObj;
    VXB_DEVICE_ID	dcDev;
    void *		dcMuxDevCookie;
    void *		dcBar;
    void *		dcHandle;

    JOB_QUEUE_ID	dcJobQueue;
    QJOB		dcIntJob;
    atomic_t		dcIntPending;

    QJOB		dcRxJob;
    atomic_t		dcRxPending;

    QJOB		dcTxJob;
    atomic_t		dcTxPending;
    UINT8		dcTxCur;
    UINT8		dcTxLast;
    volatile BOOL	dcTxStall;
    UINT16		dcTxThresh;

    QJOB		dcLinkJob;
    atomic_t		dcLinkPending;

    BOOL		dcPolling;
    M_BLK_ID		dcPollBuf;
    UINT32		dcIntrs;
    UINT32		dcIntMask;

    UINT8		dcAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	dcCaps;

    END_IFDRVCONF	dcEndStatsConf;
    END_IFCOUNTERS	dcEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*dcMediaList;
    END_ERR		dcLastError;
    UINT32		dcCurMedia;
    UINT32		dcCurStatus;
    VXB_DEVICE_ID	dcMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	dcParentTag;

    VXB_DMA_TAG_ID	dcRxDescTag;
    VXB_DMA_MAP_ID	dcRxDescMap;
    DC_DESC *		dcRxDescMem;

    VXB_DMA_TAG_ID	dcTxDescTag;
    VXB_DMA_MAP_ID	dcTxDescMap;
    DC_DESC *		dcTxDescMem;

    VXB_DMA_TAG_ID	dcMblkTag;

    VXB_DMA_MAP_ID	dcRxMblkMap[DC_RX_DESC_CNT];
    VXB_DMA_MAP_ID	dcTxMblkMap[DC_TX_DESC_CNT];

    M_BLK_ID		dcRxMblk[DC_RX_DESC_CNT];
    M_BLK_ID		dcTxMblk[DC_TX_DESC_CNT];

    UINT32		dcTxProd;
    UINT32		dcTxCons;
    UINT32		dcTxFree;
    UINT32		dcRxIdx;

    int			dcEeWidth;
    int			dcMaxMtu;

    UINT8 *		dcSframe;
    VXB_DMA_TAG_ID	dcSframeTag;
    VXB_DMA_MAP_ID	dcSframeMap;

    UINT16		dcDevId;
    int			dcMedia;
    BOOL		dcSiaAuto;
    UINT16		dcSiaAnar;

    SEM_ID		dcDevSem;
    } DC_DRV_CTRL;

#define DC_BAR(p)   ((DC_DRV_CTRL *)(p)->pDrvCtrl)->dcBar
#define DC_HANDLE(p)   ((DC_DRV_CTRL *)(p)->pDrvCtrl)->dcHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (DC_HANDLE(pDev), (UINT32 *)((char *)DC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (DC_HANDLE(pDev),                             \
        (UINT32 *)((char *)DC_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (DC_HANDLE(pDev), (UINT16 *)((char *)DC_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (DC_HANDLE(pDev),                             \
        (UINT16 *)((char *)DC_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (DC_HANDLE(pDev), (UINT8 *)((char *)DC_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (DC_HANDLE(pDev),                              \
        (UINT8 *)((char *)DC_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbDc2114xEndh */
