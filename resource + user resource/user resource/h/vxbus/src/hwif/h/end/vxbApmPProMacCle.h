/* vxbApmPProMacCle.h - header file for APM PacketPro MAC Classifier driver */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,15nov11,x_z  fixed nextDataPtr field defination.
01b,02dec11,x_z  updated Database RAM Entry defination.
01a,29jun11,x_z  written.
*/

#ifndef __INCvxbApmPProMacCleh
#define __INCvxbApmPProMacCleh

#ifdef __cplusplus
extern "C" {
#endif

/*  Classifier resources */

/* Database RAM */

#define APM_MAC_CLE_DBRAM_ENTRY_NUM         1024

/* Used status word count */

#define APM_MAC_CLE_DBRAM_STAT_CNT      (APM_MAC_CLE_DBRAM_ENTRY_NUM / 32)
#define APM_MAC_CLE_DBRAM_STAT_WORDn(n) ((n) >> 5)
#define APM_MAC_CLE_DBRAM_STAT_BITn(n)  (1 << (31 - ((n) & 31)))

/* Patricia Tree RAM */

#define APM_MAC_CLE_PTRAM_ENTRY_NUM         128

/* AVL RAM */

#define APM_MAC_CLE_AVLRAM_ENTRY_NUM        2048

/* Clessifier Registers */

#define APM_MAC_CLE_BID                     0x000   /* Block ID */

/* bit definitions for Clessifier Block ID Register */

#define APM_MAC_CLE_BID_REV(x)     (((x) >> 8) & 0x3) /* Revision */
#define APM_MAC_CLE_BID_BUSID(x)   (((x) >> 5) & 0x7) /* Bus ID */
#define APM_MAC_CLE_BID_DEVID(x)   ((x) & 0x1F) /* Device ID */

/* Data RAM Access Registers */

#define APM_MAC_CLE_DRAM_INDADDR     0x004 /* Indirect Access Address */
#define APM_MAC_CLE_DRAM_INDCMD      0x008 /* Indirect Access Command */
#define APM_MAC_CLE_DRAM_INDCMD_STAT 0x00C /* Indirect Access Command Status */
#define APM_MAC_CLE_DRAM_DATA0       0x010 /* Indirect Access Read/Write Data */
#define APM_MAC_CLE_DRAM_DATA1       0x014
#define APM_MAC_CLE_DRAM_DATA2       0x018
#define APM_MAC_CLE_DRAM_DATA3       0x01C
#define APM_MAC_CLE_DRAM_DATA4       0x020
#define APM_MAC_CLE_DRAM_DATA5       0x024
#define APM_MAC_CLE_DRAM_DATA6       0x028
#define APM_MAC_CLE_DRAM_DATA7       0x02C
#define APM_MAC_CLE_DRAM_DATA8       0x030
#define APM_MAC_CLE_DRAM_DATA9       0x034
#define APM_MAC_CLE_DRAM_DATA10      0x038
#define APM_MAC_CLE_DRAM_DATA11      0x03C
#define APM_MAC_CLE_DRAM_DATA12      0x040
#define APM_MAC_CLE_DRAM_DATA13      0x044
#define APM_MAC_CLE_DRAM_DATA14      0x048
#define APM_MAC_CLE_DRAM_DATA15      0x04C
#define APM_MAC_CLE_DRAM_AVL_CFG     0x050 /* AVL Tree Search Config */
#define APM_MAC_CLE_DRAM_AVL_STAT    0x054 /* AVL Search Manager Status */

/* bit definitions for DRAM Indirect Access Address Register */

#define APM_MAC_CLE_DRAM_INDADDR_TYPE(x)        ((UINT32) (((x) & 0x7) << 29))
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_MASK      0xE0000000
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_SHIFT     29
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_PKTRAM0   0 /* Packet RAM 0 */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_PKTRAM1   1 /* Packet RAM 1 */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_PKTRAM2   2 /* Packet RAM 2 */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_PTRAM     3 /* Patricia Tree RAM */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_AVLRAM    4 /* AVL Tree RAM(debug only) */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_DBRAM     5 /* Database RAM */
#define APM_MAC_CLE_DRAM_INDADDR_TYPE_AVL       6 /* AVL add/del/sercach */
                                                  /* operation */

/* DRAM Address */

#define APM_MAC_CLE_DRAM_INDADDR_PKTRAM0(x)     \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (0) | ((x) & 0x3F))
#define APM_MAC_CLE_DRAM_INDADDR_PKTRAM1(x)     \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (1) | ((x) & 0x3F))
#define APM_MAC_CLE_DRAM_INDADDR_PKTRAM2(x)     \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (2) | ((x) & 0x1F))
#define APM_MAC_CLE_DRAM_INDADDR_PTRAM(x)       \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (3) | ((x) & 0x7F))
#define APM_MAC_CLE_DRAM_INDADDR_AVLRAM(x)      \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (4) | ((x) & 0x7FF))
#define APM_MAC_CLE_DRAM_INDADDR_DBRAM(x)       \
    (APM_MAC_CLE_DRAM_INDADDR_TYPE (5) | ((x) & 0x3FF))

/*
 * bit definitions for DRAM Indirect Access Command Register
 *
 * Note: All command bit is self-cleared.
 */

#define APM_MAC_CLE_DRAM_INDCMD_AVL_SRARCH  0x20 /* AVL node search */
#define APM_MAC_CLE_DRAM_INDCMD_AVL_DEL     0x10 /* AVL node add */
#define APM_MAC_CLE_DRAM_INDCMD_AVL_ADD     0x08 /* AVL node delete */
#define APM_MAC_CLE_DRAM_INDCMD_RD          0x02 /* DRAM read */
#define APM_MAC_CLE_DRAM_INDCMD_WR          0x01 /* DRAM write */

/* bit definitions for DRAM Indirect Access Command Status Register */

#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_ERR    0x80000000 /* ALV error */

/*
 * address in the AVL RAM of the data/string del/srch/add - ed into the AVL
 * Tree(valid only after AVL sarech/add/del opeartions are hit/finished)
 */

#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_ADRS(x)    (((x) >> 17) & 0x7FF)

/* address in the DB RAM(valid only after AVL sarech opeartion is hit) */

#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_SEARCH_DB_ADRS(x)  (((x) >> 7) & 0x3FF)

/* Clear on read bits */

/* AVL command status */

#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_SEARCH_MISS    0x40 /* Search missed */
#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_SEARCH_HIT     0x20 /* Search hit */
#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_DEL_DONE       0x10 /* Delete done */
#define APM_MAC_CLE_DRAM_INDCMD_STAT_AVL_ADD_DONE       0x08 /* Add done */

/* RAM read/write command status */

#define APM_MAC_CLE_DRAM_INDCMD_STAT_RD_DONE            0x02 /* Read done */
#define APM_MAC_CLE_DRAM_INDCMD_STAT_WR_DONE            0x01 /* Write done */

/*
 * Data RAM Indirect Access Read/Write Data Register
 *
 * 1. When DRAM read operation is done,
 * Packet RAM 0 - Data RAM Data 15 is to be used for programming,
 * Packet RAM 1 - Data RAM Data 15 is to be used for programming,
 * Packet RAM 2 - Data RAM Data 12-15 are to be used for programming,
 * Patricia Tree RAM - Data RAM Data 0-15 are to be used for programming,
 *
 * 2. For AVL RAM, Data RAM Data 6-15 are to be used for programming to form
 * Data RAM Data[293:0] (if it is to access AVL RAM directly):
 * data_ram[293:38] is the search string
 * data_ram[37:35] is the priority bits
 * data_ram[34:25] is the DB RAM pointer
 * data_ram[24:14] is the left address
 * data_ram[13:3] is the right address
 * data_ram[2:0] is balance bits
 *
 * 3. To add node using avl_add indirect command Data RAM Data 7-15 are used to
 * form:
 * Data RAM Data[268:13] is the search string
 * Data RAM Data[12:10] is the priority bits
 * Data RAM Data[9:0] is the DB RAM pointer
 *
 * 4. To delede node using AVL delete command or to search node using AVL search
 * command, Data RAM Data[255:0] is the string to be deleted or searched DB RAM
 * - Data RAM Data 10-15 are to be used for programming,
 */

#define APM_MAC_CLE_DRAM_DATAn(n)       (APM_MAC_CLE_DRAM_DATA0 + ((n) << 2))

#define APM_MAC_CLE_DRAM_DATA_CNT_MAX   16

/* Packet RAM usage */

#define APM_MAC_CLE_DRAM_DATA_CNT_PKTRAM0   1
#define APM_MAC_CLE_DRAM_DATA_CNT_PKTRAM1   1
#define APM_MAC_CLE_DRAM_DATA_CNT_PKTRAM2   4

/* Patricia Tree RAM usage */

#define APM_MAC_CLE_DRAM_DATA_CNT_PTRAM     16

/* AVL RAM usage - direct access mode (debug only) */

#define APM_MAC_CLE_DRAM_DATA_CNT_AVLRAM            10
#define APM_MAC_CLE_AVL_STR_MAX_LEN                 32
#define APM_MAC_CLE_AVL_STR_WORD_CNT                8

typedef struct
    {
    UINT32  resv        : 26;
    UINT32  srchStrHi   : 6; /* Saerch String[255, 0] */
    UINT32  srchStrMid[APM_MAC_CLE_AVL_STR_WORD_CNT - 1];
    UINT32  srchStrLo   : 26;
    UINT32  pri         : 3;  /* Priority */
    UINT32  dbRamPtrHi  : 3;  /* DB RAM pointer */
    UINT32  dbRamPtrLo  : 7;
    UINT32  lAdrs       : 11; /* left address */
    UINT32  rAdrs       : 11; /* right address */
    UINT32  blance      : 3;
    } _WRS_PACK_ALIGN (1) APM_MAC_CLE_AVLRAM_ENTRY;

#define APM_MAC_CLE_DRAM_AVLRAM_STR_HI_RD(x)    (((x) & 0x3F) << 26)
#define APM_MAC_CLE_DRAM_AVLRAM_STR_HI_WR(x)    (((x) >> 26) & 0x3F)

#define APM_MAC_CLE_DRAM_AVLRAM_STR_LO_RD(x)    (((x) >> 6) & 0x3FFFFFF)
#define APM_MAC_CLE_DRAM_AVLRAM_STR_LO_MASK     0x3FFFFFF
#define APM_MAC_CLE_DRAM_AVLRAM_STR_LO_WR(x)    (((x) & 0x3FFFFFF) << 6)

#define APM_MAC_CLE_DRAM_AVLRAM_DBRAMPTR_HI(x)      (((x) >> 7) & 7)
#define APM_MAC_CLE_DRAM_AVLRAM_DBRAMPTR_LO(x)      ((x) & 0x7F)
#define APM_MAC_CLE_DRAM_AVLRAM_DBRAMPTR(hi, lo)    (((hi) << 7) | (lo))

/* AVL RAM usage - add operaton */

#define APM_MAC_CLE_DRAM_DATA_CNT_AVLADD    9

typedef struct
    {
    UINT32  resv        : 19; /* used to save the AVL RAM index of AVL entry */
    UINT32  srchStrHi   : 13; /* Saerch String[255, 0] */
    UINT32  srchStrMid[APM_MAC_CLE_AVL_STR_WORD_CNT - 1];
    UINT32  srchStrLo   : 19;
    UINT32  pri         : 3;  /* Priority */
    UINT32  dbRamPtr    : 10; /* DB RAM pointer */
    } _WRS_PACK_ALIGN (1) APM_MAC_CLE_AVLADD_ENTRY;

#define APM_MAC_CLE_DRAM_AVLADD_STR_HI(x)    (((x) >> 19) & 0x1FFF)
#define APM_MAC_CLE_DRAM_AVLADD_STR_LOMASK   0x7FFFF
#define APM_MAC_CLE_DRAM_AVLADD_STR_LO(x)    (((x) & 0x7FFFF) << 13)

/* AVL RAM usage - delete/sarch operaton */

#define APM_MAC_CLE_DRAM_DATA_CNT_AVL   8

/* AVL RAM Decriptor - used for SW interface */

typedef struct
    {
    UINT32  errCode;
    UINT32  srchStr[APM_MAC_CLE_AVL_STR_WORD_CNT];
    UINT32  pri         : 3;  /* Priority */
    UINT32  dbRamPtr    : 10; /* DB RAM pointer */
    UINT32  lAdrs       : 11; /* left address */
    UINT32  rAdrs       : 11; /* right address */
    UINT32  blance      : 3;
    UINT32  index       : 11;
    } _WRS_PACK_ALIGN (1) APM_MAC_CLE_AVLRAM_INFO;

/* Database RAM usage */

#define APM_MAC_CLE_DRAM_DATA_CNT_DBRAM 6

/* bit definitions for DRAM AVL Tree Search Config Register */

/*
 * Maximum number of state transitions permitted in add/del/search.
 *
 * After this limit, add/del/search will be terminated with error interrupt.
 */

#define APM_MAC_CLE_DRAM_AVL_CFG_MAX_STEP(x)    (((x) & 0xFFF) << 14)
#define APM_MAC_CLE_DRAM_AVL_CFG_MAX_STEP_MASK  0x03FFC000
#define APM_MAC_CLE_DRAM_AVL_CFG_MAX_STEP_SHIFT 14
#define APM_MAC_CLE_DRAM_AVL_CFG_MAX_STEP_DEF   4095

/*
 * Depth of AVL search database. - default is 1535
 *
 * string size of 8 bytes - this value must be less than or equal to 1535.
 * string size of 16 bytes - this value must be less than or equal to 1023.
 * string size of 32 bytes - this value must be less than or equal to 511.
 */

#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP(x)      (((x) & 0xFFF) << 2)
#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_MASK    0x00003FFC
#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_SHIFT   2

#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_DEF     1535
#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_MAX_8B  1535
#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_MAX_16B 1023
#define APM_MAC_CLE_DRAM_AVL_CFG_SRCHDB_DEP_MAX_32B 511

/* String size in bytes */

#define APM_MAC_CLE_DRAM_AVL_CFG_STR_SIZE(x)        ((x) & 3)
#define APM_MAC_CLE_DRAM_AVL_CFG_STR_8B_CODE        0 /* 8 bytes */
#define APM_MAC_CLE_DRAM_AVL_CFG_STR_16B_CODE       1 /* 16 bytes */
#define APM_MAC_CLE_DRAM_AVL_CFG_STR_32B_CODE       2 /* 32 bytes */

#define APM_MAC_CLE_DRAM_AVL_CFG_STR_8B             8 /* 8 bytes */
#define APM_MAC_CLE_DRAM_AVL_CFG_STR_16B            16 /* 16 bytes */
#define APM_MAC_CLE_DRAM_AVL_CFG_STR_32B            32 /* 32 bytes */

/* bit definitions for DRAM AVL Search Manager Status Register */

/* Root address of the AVL Tree */

#define APM_MAC_CLE_DRAM_AVL_STAT_ROOT_ADRS(x)  (((x) >> 12) & 0x7FF)

/* Amount of nodes in the AVL Tree */

#define APM_MAC_CLE_DRAM_AVL_STAT_NDCNT(x)      ((x) & 0xFFF)

/* Paser Config Registers */

#define APM_MAC_CLE_SNPTR0              0x100 /* Parser Start Node Patricia */
                                              /* Tree Pointer */
#define APM_MAC_CLE_SPPTR0              0x104 /* Parser Start Packet Pointer */
#define APM_MAC_CLE_DEF_CLERST_DB_PTR0  0x108 /* Default Classification */
                                              /* Result/Database Pointer */
#define APM_MAC_CLE_DEF_CLERST_DB0_0    0x10C /* Default Classification */
                                              /* Result/Database */
#define APM_MAC_CLE_DEF_CLERST_DB0_1    0x110
#define APM_MAC_CLE_DEF_CLERST_DB0_2    0x114
#define APM_MAC_CLE_DEF_CLERST_DB0_3    0x118
#define APM_MAC_CLE_DEF_CLERST_DB0_4    0x11C
#define APM_MAC_CLE_DEF_CLERST_DB0_5    0x120
#define APM_MAC_CLE_PARSER_CTL0         0x124 /* Parser Control */
#define APM_MAC_CLE_TREE_MAXHOP0        0x128 /* Tree Max Hops */
#define APM_MAC_CLE_PORT_NUM0           0x12C /* Port Number 0 */

#define APM_MAC_CLE_DS_CTL              0x130 /* RAM Deep Sleep Control */

#define APM_MAC_CLE_SNPTR1              0x140 /* Parser Start Node Patricia */
                                              /* Tree Pointer */
#define APM_MAC_CLE_SPPTR1              0x144 /* Parser Start Packet Pointer */
#define APM_MAC_CLE_DEF_CLERST_DB_PTR1  0x148 /* Default Classification */
                                              /* Result/Database Pointer */
#define APM_MAC_CLE_DEF_CLERST_DB1_0    0x14C /* Default Classification */
                                              /* Result/Database */
#define APM_MAC_CLE_DEF_CLERST_DB1_1    0x150
#define APM_MAC_CLE_DEF_CLERST_DB1_2    0x154
#define APM_MAC_CLE_DEF_CLERST_DB1_3    0x158
#define APM_MAC_CLE_DEF_CLERST_DB1_4    0x15C
#define APM_MAC_CLE_DEF_CLERST_DB1_5    0x160
#define APM_MAC_CLE_PARSER_CTL1         0x164 /* Parser Control */
#define APM_MAC_CLE_TREE_MAXHOP1        0x168 /* Tree Max Hops */
#define APM_MAC_CLE_PORT_NUM1           0x16C /* Port Number 0 */

/* bit definitions for Parser Start Node Patricia Tree Pointer 0/1 Registers */

#define APM_MAC_CLE_SNPTR_WOL_MOD   0x80000000 /* WOL mode Enable(valid only */
                                               /* for Parser 0 (Eth Port 0)) */

/*
 * Ethernet place holder, which will be sent out to ethernet as part of the
 * cle_eth_db_data, there is no functionality for these signals.
 */

#define APM_MAC_CLE_SNPTR_ETH_SPARE(x)      (((x) & 0xF) << 27)
#define APM_MAC_CLE_SNPTR_ETH_SPARE_MASK    0x78000000
#define APM_MAC_CLE_SNPTR_ETH_SPARE_SHIFT   27

/* Start Node Pointer to the Patricia Tree RAM */

#define APM_MAC_CLE_SNPTR_VAL(x)    ((x) & 0xFFF)

/* bit definitions for Parser Start Packet Pointer Register */

#define APM_MAC_CLE_SPPTR_VAL(x)    ((x) & 0x1FF)

/*
 * bit definitions for Default Classification Result/Database Pointer 0/1
 * Registers
 */

/*
 * Default Priority, used when error occurs while searching AVL TREE is in
 * progress, or if  AVL TREE search results in a miss.
 */

#define APM_MAC_CLE_DEF_CLERST_DB_PTR_PRI(x)    (((x) & 7) << 10)
#define APM_MAC_CLE_DEF_CLERST_DB_PTR_PRI_MASK  0x00001c00
#define APM_MAC_CLE_DEF_CLERST_DB_PTR_PRI_SHIFT 10
#define APM_MAC_CLE_DEF_CLERST_DB_PTR_PRI_DEF   7

/*
 * Default Pointer to the Result/Database RAM, used when error occurs when
 * reading from Patricia Tree RAM(location indicated by spptr) or while
 * searching AVL TREE is in progress, or if AVL TREE search results in a miss.
 */

#define APM_MAC_CLE_DEF_CLERST_DB_PTR_VAL(x)    ((x) & 0x3FF)

/*
 * bit definitions for Default Classification Result/Database 0/1 Registers
 *
 * Default Value of Database RAM, used when error occurs while reading the
 * Database RAM.
 *
 * NOTE: 0 is the left most of the default database, 5 is the right most of
 * default database.
 */

#define APM_MAC_CLE_DEF_CLERST_DB0_n(n) \
    (APM_MAC_CLE_DEF_CLERST_DB0_0 + ((n) << 2))

#define APM_MAC_CLE_DEF_CLERST_DB1_n(n) \
    (APM_MAC_CLE_DEF_CLERST_DB0_1 + ((n) << 2))

/* bit definitions for Parser Control 0/1 Registers */

/*
 * When set, the byte to be stored as search string will be the data based on
 * "Next 2-Byte Data Pointer" with the inverted Mask field of the "winning"
 * branch.
 *
 * Otherwise the byte to be stored as search string will be the data based on
 * "Next 2-Byte Data Pointer" without the "and" operation with the Mask field.
 */

#define APM_MAC_CLE_PARSER_CTL_PKT_DATA_MASKEN  0x80000000

/*
 * When set, the "Default Result Pointers[7:0]" in the Patricia Tree Node will
 * be used as "Next Decision Pointer[7:0]" if there is no winner for that node.
 * It must be guaranteed that at Last Node, there is always a winner.
 */

#define APM_MAC_CLE_PARSER_CTL_USE_DEFPTR_AS_NXT_DECPTR 0x40000000

/* set it to stop the parser to process any packet*/

#define APM_MAC_CLE_PARSER_CTL_HALT 0x00000001

/*
 * bit definitions for Tree Max Hops 0/1 Registers
 *
 * Tree Max Hop, if the parser uses the number of nodes beyond the value
 * specified in this register, an error interrupt will be generated..
 */

#define APM_MAC_CLE_TREE_MAXHOP_VAL(x)  ((x) & 0xFF)
#define APM_MAC_CLE_TREE_MAXHOP_VAL_DEF 0x40

/*
 * bit definitions for Port Number 0/1 Registers
 *
 * used to define the port number used by parser 0/1. It is 0 for parser 0, and
 * 1 for parser 1.
 */

#define APM_MAC_CLE_PORT_NUM_VAL(x)     ((x) & 0xFF)
#define APM_MAC_CLE_PORT_NUM0_VAL_DEF   0
#define APM_MAC_CLE_PORT_NUM1_VAL_DEF   1

/* bit definitions for RAM Deep Sleep Control Register */

#define APM_MAC_CLE_DS_CTL_SLEEP_STAT(x)  (((x) >> 28) & 0xF) /* Sleep status */
#define APM_MAC_CLE_DS_CTL_WAKE_STAT(x)   (((x) >> 24) & 0xF) /* Wake status */
#define APM_MAC_CLE_DS_CTL_INPROG_STAT(x) (((x) >> 20) & 0xF) /* In progress */
                                                              /*status */
#define APM_MAC_CLE_DS_CTL_RAMDS_STAT   0x00080000 /* RAM deep sleep status */
#define APM_MAC_CLE_DS_CTL_RAMRDY_STAT  0x00040000 /* RAM ready status */

/*
 * The ds control logics normally will starts the wakeup_cnt when enet rx buffer
 * is not empty.  But if dis_enet_rx_empty_usage is set, the wakeup_cnt will be
 * started only when the packet arrives at CLE.
 */

#define APM_MAC_CLE_DS_CTL_DIS_ENET_RXEPT   0x00020000

/*
 * When this Register is valid, the DS signal to RAMs will be control by the
 * deep sleep state machine to reduce the power consumption.
 *
 * This bit should be set to 0 when in the wol_mode - bit[0] of snptr reg- is
 * set. This is required since in wol_mode the Deep Sleep state machine from
 * SLIMpro will take over.
 */

#define APM_MAC_CLE_DS_CTL_EN   0x0002000

/*
 * This counter is used by the control logics to delay the activation of the DS
 * signal to RAMS when there is no activity on the classifier.
 *
 * Default of 10 is to make sure if there is packet in rx buffer, the DS signal
 * will not be activated. This delay is needed for the synchronization delay.
 */

#define APM_MAC_CLE_DS_CTL_SLEEP_TMCNT(x)       (((x) & 0xFF) << 8)
#define APM_MAC_CLE_DS_CTL_SLEEP_TMCNT_MASK     0xFF00
#define APM_MAC_CLE_DS_CTL_SLEEP_TMCNT_SHIFT    8
#define APM_MAC_CLE_DS_CTL_SLEEP_TMCNT_DEF      10

/*
 * This counter is used to delay the access to all the RAMS.
 *
 * For 250MHz AXI clock, the value should be 11.
 * For 125MHz AXI clock, the value should be 6.
 *
 * This register is also used when the Deep Sleep state machine from IPP is
 * active in wol_mode. In wol_mode, when the rx_DSen input from SLIMpro is
 * driven from hight to low, the access to CLE RAMs will be delayed based on the
 * value in ds_wake_timeout_cnt registers.
 */

#define APM_MAC_CLE_DS_CTL_WAKE_TMCNT(x)    ((x) & 0xFF)
#define APM_MAC_CLE_DS_CTL_WAKE_TMCNT_DEF   11

/* RAM Diagnostic/Control/Status registers */

#define APM_MAC_CLE_PTRAM_ERR_DIAG      0x200 /* Patricia Tree RAM Error */
                                              /* Diagnostic */
#define APM_MAC_CLE_PKTRAM0_ERR_DIAG    0x204 /* Packet RAM 0 Error */
                                              /* Diagnostic */
#define APM_MAC_CLE_PKTRAM1_ERR_DIAG    0x208 /* Packet RAM 1 Error */
                                              /* Diagnostic */
#define APM_MAC_CLE_AVLRAM_ERR_DIAG     0x20C /* AVL RAM 0 Error */
                                              /* Diagnostic */
#define APM_MAC_CLE_DBRAM_ERR_DIAG      0x210 /* Dtatbase RAM 0 Error */
                                              /* Diagnostic */
#define APM_MAC_CLE_SNPTR_CHANGE_STAT0  0x214 /* Parser Start Node Patricia */
                                              /* Tree Pointer Change Status*/
#define APM_MAC_CLE_SNPTR_CHANGE_STAT1  0x218
#define APM_MAC_CLE_PARSER_STAT0        0x21C /* Parser Status */
#define APM_MAC_CLE_PARSER_STAT0MASK    0x220 /* Parser Status Mask */
#define APM_MAC_CLE_PARSER_STAT1        0x224
#define APM_MAC_CLE_PARSER_STAT1MASK    0x228
#define APM_MAC_CLE_RSIF_RAM_CTRL       0x230 /* RSIF RAM Control */
#define APM_MAC_CLE_DB_RAM_CTRL         0x234 /* Dtatbase RAM Control */

#define APM_MAC_CLE_AVL_RAM3_CTRL       0x238 /* AVL RAM Control */
#define APM_MAC_CLE_AVL_RAM2_CTRL       0x23C
#define APM_MAC_CLE_AVL_RAM1_CTRL       0x240
#define APM_MAC_CLE_AVL_RAM0_CTRL       0x244

#define APM_MAC_CLE_PTREE_RAM3_CTRL     0x248 /* Patricia Tree RAM Control */
#define APM_MAC_CLE_PTREE_RAM2_CTRL     0x24C
#define APM_MAC_CLE_PTREE_RAM1_CTRL     0x250
#define APM_MAC_CLE_PTREE_RAM0_CTRL     0x254

#define APM_MAC_CLE_AVL_SEARCH_INT      0x258 /* AVL/Search Manager Interrupt */
#define APM_MAC_CLE_AVL_SEARCH_INTMASK  0x25C
#define APM_MAC_CLE_CLE_INT             0x260 /* Classifier Interrupt */
#define APM_MAC_CLE_CLE_INTMASK         0x264

/* Parser 0 */

#define APM_MAC_CLE_LSTNVST0            0x300 /* Last Node of Parser Patricia */
                                              /* Tree RAM Visited */
#define APM_MAC_CLE_LSTTRCNVST0_0       0x304 /* Last Trace Node of Parser */
#define APM_MAC_CLE_LSTTRCNVST0_1       0x308 /* Patricia Tree RAM Visited */
#define APM_MAC_CLE_LSTTRCNVST0_2       0x30C
#define APM_MAC_CLE_LSTTRCNVST0_3       0x310
#define APM_MAC_CLE_LSTTRCNVST0_4       0x314
#define APM_MAC_CLE_LSTTRCNVST0_5       0x318
#define APM_MAC_CLE_LSTTRCNVST0_6       0x31C
#define APM_MAC_CLE_LSTTRCNVST0_7       0x320
#define APM_MAC_CLE_LSTTRCNVST0_8       0x324
#define APM_MAC_CLE_LSTTRCNVST0_9       0x328
#define APM_MAC_CLE_LSTTRCNVST0_10      0x32C
#define APM_MAC_CLE_LSTTRCNVST0_11      0x330
#define APM_MAC_CLE_LSTTRCNVST0_12      0x334
#define APM_MAC_CLE_LSTTRCNVST0_13      0x338
#define APM_MAC_CLE_LSTTRCNVST0_14      0x33C
#define APM_MAC_CLE_LSTTRCNVST0_15      0x340

#define APM_MAC_CLE_FTRCNVSTSTR0_0      0x350 /* First Trace Node of Parser */
                                              /* Patricia Tree RAM Visited */
                                              /* Start */
#define APM_MAC_CLE_FTRCNVST0_0         0x354 /* First Trace Node of Parser */
#define APM_MAC_CLE_FTRCNVST0_1         0x358 /* Patricia Tree RAM Visited */
#define APM_MAC_CLE_FTRCNVST0_2         0x35C
#define APM_MAC_CLE_FTRCNVST0_3         0x360
#define APM_MAC_CLE_FTRCNVST0_4         0x364
#define APM_MAC_CLE_FTRCNVST0_5         0x368
#define APM_MAC_CLE_FTRCNVST0_6         0x36C
#define APM_MAC_CLE_FTRCNVST0_7         0x370
#define APM_MAC_CLE_FTRCNVST0_8         0x374
#define APM_MAC_CLE_FTRCNVST0_9         0x378
#define APM_MAC_CLE_FTRCNVST0_10        0x37C
#define APM_MAC_CLE_FTRCNVST0_11        0x380
#define APM_MAC_CLE_FTRCNVST0_12        0x384
#define APM_MAC_CLE_FTRCNVST0_13        0x388
#define APM_MAC_CLE_FTRCNVST0_14        0x38C
#define APM_MAC_CLE_FTRCNVST0_15        0x390

#define APM_MAC_CLE_TRCNVSTMON0_0       0x394 /* Trace Node of Parser */
                                              /* Patricia Tree RAM to be Kept */
                                              /* trackeed by trcnvstmoncnt */
#define APM_MAC_CLE_TRCNVSTMONCNT0_0    0x398 /* Counter that counts how many */
                                              /* times node indicated by */
                                               /*trcnvstmon is visited*/
#define APM_MAC_CLE_TRCNVSTMON0_1       0x39C
#define APM_MAC_CLE_TRCNVSTMONCNT0_1    0x3A0
#define APM_MAC_CLE_TRCNVSTMON0_2       0x3A4
#define APM_MAC_CLE_TRCNVSTMONCNT0_2    0x3A8
#define APM_MAC_CLE_TRCNVSTMON0_3       0x3AC
#define APM_MAC_CLE_TRCNVSTMONCNT0_3    0x3B0
#define APM_MAC_CLE_TRCNVSTMON0_4       0x3B4
#define APM_MAC_CLE_TRCNVSTMONCNT0_4    0x3B8
#define APM_MAC_CLE_TRCNVSTMON0_5       0x3BC
#define APM_MAC_CLE_TRCNVSTMONCNT0_5    0x3C0
#define APM_MAC_CLE_TRCNVSTMON0_6       0x3C4
#define APM_MAC_CLE_TRCNVSTMONCNT0_6    0x3C8
#define APM_MAC_CLE_TRCNVSTMON0_7       0x3CC
#define APM_MAC_CLE_TRCNVSTMONCNT0_7    0x3D0
#define APM_MAC_CLE_TRCNVSTMON0_8       0x3D4
#define APM_MAC_CLE_TRCNVSTMONCNT0_8    0x3D8
#define APM_MAC_CLE_TRCNVSTMON0_9       0x3DC
#define APM_MAC_CLE_TRCNVSTMONCNT0_9    0x3E0
#define APM_MAC_CLE_TRCNVSTMON0_10      0x3E4
#define APM_MAC_CLE_TRCNVSTMONCNT0_10   0x3E8
#define APM_MAC_CLE_TRCNVSTMON0_11      0x3EC
#define APM_MAC_CLE_TRCNVSTMONCNT0_11   0x3F0
#define APM_MAC_CLE_TRCNVSTMON0_12      0x3F4
#define APM_MAC_CLE_TRCNVSTMONCNT0_12   0x3F8
#define APM_MAC_CLE_TRCNVSTMON0_13      0x3FC
#define APM_MAC_CLE_TRCNVSTMONCNT0_13   0x400
#define APM_MAC_CLE_TRCNVSTMON0_14      0x404
#define APM_MAC_CLE_TRCNVSTMONCNT0_14   0x408
#define APM_MAC_CLE_TRCNVSTMON0_15      0x40C
#define APM_MAC_CLE_TRCNVSTMONCNT0_15   0x410

/* Parser 1 */

#define APM_MAC_CLE_LSTNVST1            0x420 /* Last Node of Parser Patricia */
                                              /* Tree RAM Visited */
#define APM_MAC_CLE_LSTTRCNVST1_0       0x424 /* Last Trace Node of Parser */
#define APM_MAC_CLE_LSTTRCNVST1_1       0x428 /* Patricia Tree RAM Visited */
#define APM_MAC_CLE_LSTTRCNVST1_2       0x42C
#define APM_MAC_CLE_LSTTRCNVST1_3       0x430
#define APM_MAC_CLE_LSTTRCNVST1_4       0x434
#define APM_MAC_CLE_LSTTRCNVST1_5       0x438
#define APM_MAC_CLE_LSTTRCNVST1_6       0x43C
#define APM_MAC_CLE_LSTTRCNVST1_7       0x440
#define APM_MAC_CLE_LSTTRCNVST1_8       0x444
#define APM_MAC_CLE_LSTTRCNVST1_9       0x448
#define APM_MAC_CLE_LSTTRCNVST1_10      0x44C
#define APM_MAC_CLE_LSTTRCNVST1_11      0x450
#define APM_MAC_CLE_LSTTRCNVST1_12      0x454
#define APM_MAC_CLE_LSTTRCNVST1_13      0x458
#define APM_MAC_CLE_LSTTRCNVST1_14      0x45C
#define APM_MAC_CLE_LSTTRCNVST1_15      0x460

#define APM_MAC_CLE_FTRCNVSTSTR1_0      0x470 /* First Trace Node of Parser */
                                              /* Patricia Tree RAM Visited */
                                              /* Start */
                                              /* First Trace Node of Parser */
#define APM_MAC_CLE_FTRCNVST1_0         0x474 /* Patricia Tree RAM Visited */
#define APM_MAC_CLE_FTRCNVST1_1         0x478
#define APM_MAC_CLE_FTRCNVST1_2         0x47C
#define APM_MAC_CLE_FTRCNVST1_3         0x480
#define APM_MAC_CLE_FTRCNVST1_4         0x484
#define APM_MAC_CLE_FTRCNVST1_5         0x488
#define APM_MAC_CLE_FTRCNVST1_6         0x48C
#define APM_MAC_CLE_FTRCNVST1_7         0x490
#define APM_MAC_CLE_FTRCNVST1_8         0x494
#define APM_MAC_CLE_FTRCNVST1_9         0x498
#define APM_MAC_CLE_FTRCNVST1_10        0x49C
#define APM_MAC_CLE_FTRCNVST1_11        0x4A0
#define APM_MAC_CLE_FTRCNVST1_12        0x4A4
#define APM_MAC_CLE_FTRCNVST1_13        0x4A8
#define APM_MAC_CLE_FTRCNVST1_14        0x4AC
#define APM_MAC_CLE_FTRCNVST1_15        0x4B0

#define APM_MAC_CLE_TRCNVSTMON1_0       0x4B4 /* Trace Node of Parser */
                                              /* Patricia Tree RAM to be Kept */
                                              /* trackeed by trcnvstmoncnt */
#define APM_MAC_CLE_TRCNVSTMONCNT1_0    0x4B8 /* Counter that counts how many */
                                              /* times node indicated by */
                                               /*trcnvstmon is visited*/
#define APM_MAC_CLE_TRCNVSTMON1_1       0x4BC
#define APM_MAC_CLE_TRCNVSTMONCNT1_1    0x4C0
#define APM_MAC_CLE_TRCNVSTMON1_2       0x4C4
#define APM_MAC_CLE_TRCNVSTMONCNT1_2    0x4C8
#define APM_MAC_CLE_TRCNVSTMON1_3       0x4CC
#define APM_MAC_CLE_TRCNVSTMONCNT1_3    0x4D0
#define APM_MAC_CLE_TRCNVSTMON1_4       0x4D4
#define APM_MAC_CLE_TRCNVSTMONCNT1_4    0x4D8
#define APM_MAC_CLE_TRCNVSTMON1_5       0x4DC
#define APM_MAC_CLE_TRCNVSTMONCNT1_5    0x4E0
#define APM_MAC_CLE_TRCNVSTMON1_6       0x4E4
#define APM_MAC_CLE_TRCNVSTMONCNT1_6    0x4E8
#define APM_MAC_CLE_TRCNVSTMON1_7       0x4EC
#define APM_MAC_CLE_TRCNVSTMONCNT1_7    0x4F0
#define APM_MAC_CLE_TRCNVSTMON1_8       0x4F4
#define APM_MAC_CLE_TRCNVSTMONCNT1_8    0x4F8
#define APM_MAC_CLE_TRCNVSTMON1_9       0x4FC
#define APM_MAC_CLE_TRCNVSTMONCNT1_9    0x500
#define APM_MAC_CLE_TRCNVSTMON1_10      0x504
#define APM_MAC_CLE_TRCNVSTMONCNT1_10   0x508
#define APM_MAC_CLE_TRCNVSTMON1_11      0x50C
#define APM_MAC_CLE_TRCNVSTMONCNT1_11   0x510
#define APM_MAC_CLE_TRCNVSTMON1_12      0x514
#define APM_MAC_CLE_TRCNVSTMONCNT1_12   0x518
#define APM_MAC_CLE_TRCNVSTMON1_13      0x51C
#define APM_MAC_CLE_TRCNVSTMONCNT1_13   0x520
#define APM_MAC_CLE_TRCNVSTMON1_14      0x524
#define APM_MAC_CLE_TRCNVSTMONCNT1_14   0x528
#define APM_MAC_CLE_TRCNVSTMON1_15      0x52C
#define APM_MAC_CLE_TRCNVSTMONCNT1_15   0x530

#define APM_MAC_CLE_SW_INT              0x600 /* Software interrupt */

/*
 * bit definitions for Parser Patricia Tree RAM/Packet RAM 0/Packet RAM 1/ AVL
 * RAM/Database RAM Error Diagnostic Registers
 */

/*
 * Error Address - used when Force_Read_Error_Enable is set and
 * Error_Addr_Enable is set, read error will be generated when Error_Addr
 * matches with the read address of RAM.
 */

#define APM_MAC_CLE_PTRAM_ERR_DIAG_ERR_ADRS(x)      (((x) >> 2) & 0x7F)
#define APM_MAC_CLE_PTRAM_ERR_DIAG_ERR_ADRS_MASK    0x1FC
#define APM_MAC_CLE_PTRAM_ERR_DIAG_ERR_ADRS_SHIFT   2

#define APM_MAC_CLE_PKTRAM_ERR_DIAG_ERR_ADRS(x)     (((x) >> 2) & 0x3F)
#define APM_MAC_CLE_PKTRAM_ERR_DIAG_ERR_ADRS_MASK   0x0FC
#define APM_MAC_CLE_PKTRAM_ERR_DIAG_ERR_ADRS_SHIFT  2

#define APM_MAC_CLE_AVLRAM_ERR_DIAG_ERR_ADRS(x)     (((x) >> 2) & 0x1FF)
#define APM_MAC_CLE_AVLRAM_ERR_DIAG_ERR_ADRS_MASK   0x7FC
#define APM_MAC_CLE_AVLRAM_ERR_DIAG_ERR_ADRS_SHIFT  2

#define APM_MAC_CLE_DBRAM_ERR_DIAG_ERR_ADRS(x)      (((x) >> 2) & 0x3FF)
#define APM_MAC_CLE_DBRAM_ERR_DIAG_ERR_ADRS_MASK    0xFFC
#define APM_MAC_CLE_DBRAM_ERR_DIAG_ERR_ADRS_SHIFT   2

/*
 * When set, together with Force_Read_Error_Enable, it would use Error_Addr
 * field to compare the read address of RAM and an error will be generated if
 * matches.
 *
 * When clear and if Force_Read_Error_Enable is 1, error will be generated for
 * all read from RAM.
 */

#define APM_MAC_CLE_RAM_ERR_DIAG_ERR_ADRS_EN  0x00000002

/* When set, error will be generated when Patricia Tree RAM is read. */

#define APM_MAC_CLE_RAM_ERR_DIAG_RDERR_EN     0x00000001


/*
 * bit definitions for Parser Start Node Patricia Tree Pointer Change Status 0/1
 * Registers
 *
 * When set, it indicates that the snptr has been updated and it will be used by
 * parser for the next incoming packet. (clear on read)
 */

#define APM_MAC_CLE_SNPTR_CHANGE_STAT_DONE  1

/*
 * bit definitions for Parser Status/Mask 0/1 Registers
 *
 * NOTE: these satsus will be routed to Classifier Interrupt.
 */

#define APM_MAC_CLE_PARSER_STAT_PKTRAM_ERR      0x10 /* Packet RAM read error */
#define APM_MAC_CLE_PARSER_STAT_PTRAM_ERR       0x08 /* Patricia Tree RAM */
                                                     /* read error */

/* parser requested Next 2-Byte Data Pointer that is greater then packet Size */

#define APM_MAC_CLE_PARSER_STAT_PKTOFFSTE_ERR   0x04

/* More than Tree MAX hops access error */

#define APM_MAC_CLE_PARSER_STAT_MAXHOP_ERR      0x02

/* Paser halt(read only) */

#define APM_MAC_CLE_PARSER_STAT_HALT            0x01

/* bit definitions for RSIF RAM Control Register */

#define APM_MAC_CLE_RSIF_RAM_CTRL_RMBE      0x20 /* Connected to RMBE pin */
                                                /* of RAM */

#define APM_MAC_CLE_RSIF_RAM_CTRL_RMB(x)    (((x) & 3) << 3) /* Connected to */
#define APM_MAC_CLE_RSIF_RAM_CTRL_RMB_MASK  3 /*RMB pin of RAM */
#define APM_MAC_CLE_RSIF_RAM_CTRL_RMB_SHIFt 3

#define APM_MAC_CLE_RSIF_RAM_CTRL_RMAE      0x04 /* Connected to RMAE pin of */
                                                 /* RAM */
#define APM_MAC_CLE_RSIF_RAM_CTRL_RMA       ((x) & 3) /* Connected to RMA pin*/
                                                      /* of RAM */

/*
 * bit definitions for Database RAM/AVL RAM 3/AVL RAM 2/AVL RAM 1/AVL RAM 0/
 * Patricia Tree RAM 3/Patricia Tree RAM 2/Patricia Tree RAM 1/Patricia Tree RAM
 * 0 Control Registers
 */

#define APM_MAC_CLE_RAM_CTRL_RME    0x04 /* Connected to RME pin of RAM */
#define APM_MAC_CLE_RAM_CTRL_RM     ((x) & 3) /* Connected to RM pin of RAM */

/* bit definitions for AVL/Search Manager Interrupt/Mask Registers */

/* AVL tree fatal interrupts */

#define APM_MAC_CLE_AVL_SEARCH_INT_FWRPTR   0x400 /* write node points to */
                                                  /* itself */
#define APM_MAC_CLE_AVL_SEARCH_INT_FRDPTR   0x200 /* Read node points to */
                                                  /* itself */
#define APM_MAC_CLE_AVL_SEARCH_INT_FWRBAL   0x100 /* write node has incorrect */
                                                  /* balance and address */
#define APM_MAC_CLE_AVL_SEARCH_INT_FRDBAL   0x080 /* read node has incorrect */
                                                  /* balance and address */
#define APM_MAC_CLE_AVL_SEARCH_INT_FMAXSTEP 0x040 /* max number of steps */
                                                  /* exceeded */
#define APM_MAC_CLE_AVL_SEARCH_INT_FRDRAM   0x020 /* read error from RAM */

/* AVL tre normal interrupts */

#define APM_MAC_CLE_AVL_SEARCH_INT_RDRAM    0x10 /* read error from RAM */
#define APM_MAC_CLE_AVL_SEARCH_INT_DELNF    0x08 /* node to be deleted */
                                                 /* unfounded  */
#define APM_MAC_CLE_AVL_SEARCH_INT_DEL      0x04 /* tree empty for delete */
#define APM_MAC_CLE_AVL_SEARCH_INT_ADDDUP   0x02 /* node to be added existed */
#define APM_MAC_CLE_AVL_SEARCH_INT_ADD      0x01 /* tree full for add */

/* bit definitions for classifier Interrupt/Mask Registers */

#define APM_MAC_CLE_CLE_INT_BIT             1

/*
 * bit definitions for Last Node of Parser/Patricia Tree RAM Visited 0/1
 * Registers
 */

#define APM_MAC_CLE_LSTNVST_VAL(x)  ((x) & 0xFFF)

/*
 * bit definitions for Last Trace Node of Parser/Patricia Tree RAM Visited
 * Registers
 *
 * NOTE: Register 0 indicates the latest node visited, Register 15 indicates the
 * earlier node visited. It may cross multiple packets
 */

#define APM_MAC_CLE_LSTTRCNVST0_n(n)   (APM_MAC_CLE_LSTTRCNVST0_0 + ((n) << 2))
#define APM_MAC_CLE_LSTTRCNVST1_n(n)   (APM_MAC_CLE_LSTTRCNVST1_0 + ((n) << 2))
#define APM_MAC_CLE_LSTTRCNVS_VAL(x)   ((x) & 0xFFF)

/*
 * bit definitions for First Trace Node of Parser/Patricia Tree RAM Visited 0/1
 * Registers
 */

#define APM_MAC_CLE_FTRCNVSTSTR_VAL(x)  ((x) & 0xFFF)

/*
 * bit definitions for First Trace Node of Parser/Patricia Tree RAM Visited
 * Registers
 *
 * NOTE: Register 0 indicates the latest node visited, Register 15 indicates the
 * earlier node visited.
 */

#define APM_MAC_CLE_FTRCNVST0_n(n)  (APM_MAC_CLE_FTRCNVST0_0 + ((n) << 2))
#define APM_MAC_CLE_FTRCNVST1_n(n)  (APM_MAC_CLE_FTRCNVST1_0 + ((n) << 2))
#define APM_MAC_CLE_FTRCNVST_VAL(x) ((x) & 0xFFF)

/*
 * bit definitions for Trace Node of Parser/Patricia Tree RAM to be Kept
 * trackeed Registers
 */

#define APM_MAC_CLE_TRCNVSTMON0_n(n)   (APM_MAC_CLE_TRCNVSTMON0_0 + ((n) << 3))
#define APM_MAC_CLE_TRCNVSTMON1_n(n)    APM_MAC_CLE_TRCNVSTMON1_0 + ((n) << 3))
#define APM_MAC_CLE_TRCNVSTMON_VAL(x)   (x) & 0xFFF)

/*
 * bit definitions for Trace Node of Parser/Patricia Tree RAM to be Kept
 * trackeed counter Registers
 *
 * Counter that counts how many times node indicated by trcnvstmon is visited
 */

#define APM_MAC_CLE_TRCNVSTMONCNT0_n(n) \
    (APM_MAC_CLE_TRCNVSTMONCNT0_0 + ((n) << 3))
#define APM_MAC_CLE_TRCNVSTMONCNT1_n(n) \
    (APM_MAC_CLE_TRCNVSTMONCNT1_0 + ((n) << 3))

/* bit definitions for Software interrupt Register */

#define APM_MAC_CLE_SW_INT_BIT  1   /* set it to raise one interrupt */

/* Database RAM Entry defination */

typedef struct
    {
    UINT32  resv            : 19;
    UINT32  insTimeStamp    : 1; /* Enable Timestamp insertion by Ethernet */
    UINT32  perFlow         : 6; /* PerFlow Policer IDs */
    UINT32  flowGroup       : 4; /* FlowGroup Policer IDs */
    UINT32  pri             : 3; /* Classifier priority bits */
    UINT32  dstqid          : 8; /* Destination Queue ID to which Ethernet */
                                 /* forwards the packet */
    /*
     * the following fileds are corresponding to the fields of the Qman message
     */

    UINT32  FPSel           : 5;
    UINT32  nxtFPSel        : 5;
    UINT32  h0FPSel         : 4;
    UINT32  h0HR            : 1;
    UINT32  h0HE            : 1;
    UINT32  h0DR            : 1;
    UINT32  h0SZ            : 1;
    UINT32  h0EnqNum        : 8;
    UINT64  h0Info          : 40;

    UINT32  h1FPSel         : 4;
    UINT32  h1HR            : 1;
    UINT32  h1HE            : 1;
    UINT32  h1DR            : 1;
    UINT32  h1SZ            : 1;
    UINT32  h1EnqNum        : 8;
    UINT64  h1Info          : 40;

    /*
     * Header data split when set ingress packet header and payload are stored
     * in separate buffers.
     *
     * For example, when hdrDataSplit is 1 and splitBoundary is 56, the first 56
     * Bytes of the packet are stored in one buffer, while the remainder of the
     * packet is stored in a separate buffer.
     */

    UINT32  hdrDataSplit    : 1;

    /*
     * Mirror the data to a specified Queue. Ethernet forwards data to a
     * specified Queue and can also copy the data for mirroring purpose to a
     * different Queue.
     */

    UINT32  mirror          : 1;

    UINT32  drop            : 1; /* Drop the packet */

    /* used when mirroring is enabled */

    UINT32  mirrorDstqid    : 8;
    UINT32  mirrorFPSel     : 5;
    UINT32  mirrorNxtFPSel  : 5;

    /* used when hdrDataSplit is 1 */

    UINT32  splitBoundary   : 8;

    /* used by software */

    UINT32  index; /* index to be configured */
    UINT32  realIndex; /* index to be allocted in fact */
} _WRS_PACK_ALIGN (1) APM_MAC_CLE_DBRAM_ENTRY;

/* Patricia Tree node definition */

/* 4-way/8-way decision node header definition (used to config) */

typedef struct
    {
    UINT32  defRstPtr   : 10; /* Default Result Pointer when there is no any */
                              /* matched branch at current decision node */
    UINT32  srchByteSt  : 2;  /* flag to store aside 1/or 2 bytes of the 2B */
                              /* data packets to format 32-byte Search String */
    UINT32  byteSt      : 2;  /* flag to store aside one byte of the 2B data */
                              /* packets to format Key Byte for Last Node */
    UINT32  hdrLenStore : 1;  /* flag to overwrite Mac and IP header length */
                              /* provided by Ethernet module */
    UINT32  lastNode    : 1;  /* flag for last node in the paser tree */
    UINT32  chainComp   : 1;  /* chain current node to next node for */
                              /* comparison */

    /* used by software */

    UINT32  index;
    UINT32  realIndex;
    UINT8   fwdnIndex; /* used for 4-way node */
    UINT8   realFwdnIndex;
    UINT8   nodeType;
    UINT8   branchKeyNum;
    void *  branchKey;
    } APM_MAC_CLE_PTREE_NODE_HDR;

/* srchByteSt/byteSt field defination */

#define APM_MAC_CLE_PTREE_NODE_TYPE_SRCHBYTE_STNONE 0 /* don't store */
#define APM_MAC_CLE_PTREE_NODE_TYPE_SRCHBYTE_ST0    1 /* store 1st byte */
#define APM_MAC_CLE_PTREE_NODE_TYPE_SRCHBYTE_ST1    2 /* store 2nd byte */
#define APM_MAC_CLE_PTREE_NODE_TYPE_SRCHBYTE_STBOTH 3 /* store 2 bytes */

/* srchByteSt/byteSt field defination */

#define APM_MAC_CLE_PTREE_NODE_TYPE_BYTE_STNONE 0 /* don't care */
#define APM_MAC_CLE_PTREE_NODE_TYPE_BYTE_ST0    1 /* store 1st byte */
#define APM_MAC_CLE_PTREE_NODE_TYPE_BYTE_ST1    2 /* store 2nd byte */

/* Branch definition (used to config) */

typedef struct
    {

    UINT32  valid       : 1;  /* if not set, then the current decision is the */
                              /* last comparison to be made at that node; if */
                              /* set, comparison to the next branch should be */
                              /* made */
    UINT32  mask        : 16; /* inverted mask(1's complement) for comparison */
                              /* data */
    UINT32  compData    : 16; /* comparison data for 2-byte packet data */
    UINT32  op          : 3;  /* operation for comparison */
    UINT32  nextDecPtr  : 12; /* pointer to next decision */
    UINT32  jmpRel      : 1;  /* relattive mode for nextDataPtr */
    UINT32  jmpBw       : 1;  /* backward mode for nextDataPtr */
    UINT32  nextDataPtr : 9;  /* pointer to next 2-byte packet data */

    /* used by software */

    UINT32  index;
    UINT32  realIndex;
    } APM_MAC_CLE_PTREE_BRANCH;

/* op field defination */

#define APM_MAC_CLE_PTREE_BRANCH_OP_EQ      0 /* equal */
#define APM_MAC_CLE_PTREE_BRANCH_OP_NEQ     1 /* not equal */
#define APM_MAC_CLE_PTREE_BRANCH_OP_LE      2 /* less than equal */
#define APM_MAC_CLE_PTREE_BRANCH_OP_GE      3 /* greater than equal */
#define APM_MAC_CLE_PTREE_BRANCH_OP_AND     4 /* logical AND */
#define APM_MAC_CLE_PTREE_BRANCH_OP_NAND    5 /* logical NAND */

/* nexDecPtr field defination */

/* Node index (bit[6, 0])to Patricia Tree RAM for all types of node */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_NDID(x)      ((x) & 0x7F)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_NDID_MASK    0x7F

/* key index (bit[11, 7]) for Last Node */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KNID(x)      (((x) & 0x1F) << 7)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KNID_VAL(x)  (((x) >> 7) & 0x1F)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KNID_MASK    0x1F
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KNID_SHIFT   7

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KN(nodeId, knId) \
    (APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_KNID (knId) | \
    APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_NDID (nodeId))

/*
 * Start Branch index (bit[10, 8])when next decision node is single decision
 * node (8-way node)
 */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_EWDN_BID(x)      (((x) & 7) << 8)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_EWDN_BID_MASK     7
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_EWDN_BID_SHIFT    8

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_EWDN(nodeId, bId) \
    (APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_EWDN_BID (bId) | \
    APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_NDID (nodeId))

/*
 * Start Branch index (bit[9, 8])when next decision node is dual decision node
 * (4-way node)
 */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDN_BID(x)      (((x) & 3) << 8)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDN_BID_MASK     3
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDN_BID_SHIFT    8

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_BID_VAL(x)       (((x) >> 8) & 7)

/*
 * 4-way node Node index (bit 7) when next decision node is dual decision node
 * (4-way node) with type 0b111
 */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDNID(x)        (((x) & 1) << 7)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDNID_VAL(x)    (((x) >> 7) & 1)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDNID_MASK      0x80

#define APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDN(nodeId, fwdnId, bId) \
    (APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDN_BID (bId) | \
    APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_FWDNID (fwdnId) | \
    APM_MAC_CLE_PTREE_BRANCH_NXTDECPTR_NDID (nodeId))

/*
 * nextDataPtr field defination
 *
 * The last bit is unused, and only the packet header is supported because of
 * lack of the technical details.
 *
 * 0x000 - 0x003 : SOF Ctrl Word 3
 * 0x004 - 0x07F : packet header
 * 0x080 - 0x0FF : user space alias
 * 0x100 - 0x17F : user space
 * 0x180 - 0x19F : 32-byte search string (0 is for the last 2 byte)
 * 0x1C0 - 0x1C7 : 64-bit Queue Fill Level(0 is for the last RID - RID 3/2/1/0)
 *
 * Note: Now only packet header and 32-byte search string can be used becuse
 * there is no any detail about other fields.
 *
 */

#define APM_MAC_CLE_PTREE_BRANCH_NXTDATAPTR_DATA(x) \
    (((x) + 4) & 0x07E)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDATAPTR_USA(x)  \
    (((x) + 0x80) & 0x0FE)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDATAPTR_US(x)   \
    (((x)+ 0x100) & 0x17E)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDATAPTR_STR(x)  \
    (((0x1F - (x)) + 0x180) & 0x19E)
#define APM_MAC_CLE_PTREE_BRANCH_NXTDATAPTR_RID(x)   \
    ((((x) >> 1) + 0x1C0) & 0x1C6)

/* key definition */

typedef struct
    {
    UINT32  pri     : 3; /* priority */
    UINT32  rstPtr  : 10; /* result pointer */

    /* used by software */

    UINT32  index;
    UINT32  realIndex;
    } APM_MAC_CLE_PTREE_KEY;

/* Node definition */

typedef union
    {

    /* single decision node */

    struct
        {

        /* header */

        UINT32  resv            : 3;
        UINT32  nodeType        : 3;
        UINT32  defRstPtr       : 10;
        UINT32  srchByteSt      : 2;
        UINT32  byteSt          : 2;
        UINT32  hdrLenStore     : 1;
        UINT32  lastNode        : 1;
        UINT32  chainComp       : 1;

        /* branches */

        UINT32  b0valid         : 1;
        UINT32  b0mask          : 16;
        UINT32  b0CompData      : 16;
        UINT32  b0Op            : 3;
        UINT32  b0NextDecPtr    : 12;
        UINT32  b0JmpRel        : 1;
        UINT32  b0JmpBw         : 1;
        UINT32  b0NextDataPtr   : 9;

        UINT32  b1valid         : 1;
        UINT32  b1mask          : 16;
        UINT32  b1CompData      : 16;
        UINT32  b1Op            : 3;
        UINT32  b1NextDecPtr    : 12;
        UINT32  b1JmpRel        : 1;
        UINT32  b1JmpBw         : 1;
        UINT32  b1NextDataPtr   : 9;

        UINT32  b2valid         : 1;
        UINT32  b2mask          : 16;
        UINT32  b2CompData      : 16;
        UINT32  b2Op            : 3;
        UINT32  b2NextDecPtr    : 12;
        UINT32  b2JmpRel        : 1;
        UINT32  b2JmpBw         : 1;
        UINT32  b2NextDataPtr   : 9;

        UINT32  b3valid         : 1;
        UINT32  b3mask          : 16;
        UINT32  b3CompData      : 16;
        UINT32  b3Op            : 3;
        UINT32  b3NextDecPtr    : 12;
        UINT32  b3JmpRel        : 1;
        UINT32  b3JmpBw         : 1;
        UINT32  b3NextDataPtr   : 9;

        UINT32  b4valid         : 1;
        UINT32  b4mask          : 16;
        UINT32  b4CompData      : 16;
        UINT32  b4Op            : 3;
        UINT32  b4NextDecPtr    : 12;
        UINT32  b4JmpRel        : 1;
        UINT32  b4JmpBw         : 1;
        UINT32  b4NextDataPtr   : 9;

        UINT32  b5valid         : 1;
        UINT32  b5mask          : 16;
        UINT32  b5CompData      : 16;
        UINT32  b5Op            : 3;
        UINT32  b5NextDecPtr    : 12;
        UINT32  b5JmpRel        : 1;
        UINT32  b5JmpBw         : 1;
        UINT32  b5NextDataPtr   : 9;

        UINT32  b6valid         : 1;
        UINT32  b6mask          : 16;
        UINT32  b6CompData      : 16;
        UINT32  b6Op            : 3;
        UINT32  b6NextDecPtr    : 12;
        UINT32  b6JmpRel        : 1;
        UINT32  b6JmpBw         : 1;
        UINT32  b6NextDataPtr   : 9;

        UINT32  b7valid         : 1;
        UINT32  b7mask          : 16;
        UINT32  b7CompData      : 16;
        UINT32  b7Op            : 3;
        UINT32  b7NextDecPtr    : 12;
        UINT32  b7JmpRel        : 1;
        UINT32  b7JmpBw         : 1;
        UINT32  b7NextDataPtr   : 9;
        } _WRS_PACK_ALIGN (1) sdn;

    /* dual decision node */

    struct
        {

        /* 1st node header */

        UINT32  resv            : 3;
        UINT32  nodeType        : 3;
        UINT32  defRstPtr1      : 10;
        UINT32  srchByteSt1     : 2;
        UINT32  byteSt1         : 2;
        UINT32  hdrLenStore1    : 1;
        UINT32  lastNode1       : 1;
        UINT32  chainComp1      : 1;

        /* branches */

        UINT32  n1b0valid       : 1;
        UINT32  n1b0mask        : 16;
        UINT32  n1b0CompData    : 16;
        UINT32  n1b0Op          : 3;
        UINT32  n1b0NextDecPtr  : 12;
        UINT32  n1b0JmpRel      : 1;
        UINT32  n1b0JmpBw       : 1;
        UINT32  n1b0NextDataPtr : 9;

        UINT32  n1b1valid       : 1;
        UINT32  n1b1mask        : 16;
        UINT32  n1b1CompData    : 16;
        UINT32  n1b1Op          : 3;
        UINT32  n1b1NextDecPtr  : 12;
        UINT32  n1b1JmpRel      : 1;
        UINT32  n1b1JmpBw       : 1;
        UINT32  n1b1NextDataPtr : 9;

        UINT32  n1b2valid       : 1;
        UINT32  n1b2mask        : 16;
        UINT32  n1b2CompData    : 16;
        UINT32  n1b2Op          : 3;
        UINT32  n1b2NextDecPtr  : 12;
        UINT32  n1b2JmpRel      : 1;
        UINT32  n1b2JmpBw       : 1;
        UINT32  n1b2NextDataPtr : 9;

        UINT32  n1b3valid       : 1;
        UINT32  n1b3mask        : 16;
        UINT32  n1b3CompData    : 16;
        UINT32  n1b3Op          : 3;
        UINT32  n1b3NextDecPtr  : 12;
        UINT32  n1b3JmpRel      : 1;
        UINT32  n1b3JmpBw       : 1;
        UINT32  n1b3NextDataPtr : 9;

        /* 2nd node header */

        UINT32  defRstPtr2      : 10;
        UINT32  srchByteSt2     : 2;
        UINT32  byteSt2         : 2;
        UINT32  hdrLenStore2    : 1;
        UINT32  lastNode2       : 1;
        UINT32  chainComp2      : 1;

        /* branches */

        UINT32  n2b0valid       : 1;
        UINT32  n2b0mask        : 16;
        UINT32  n2b0CompData    : 16;
        UINT32  n2b0Op          : 3;
        UINT32  n2b0NextDecPtr  : 12;
        UINT32  n2b0JmpRel      : 1;
        UINT32  n2b0JmpBw       : 1;
        UINT32  n2b0NextDataPtr : 9;

        UINT32  n2b1valid       : 1;
        UINT32  n2b1mask        : 16;
        UINT32  n2b1CompData    : 16;
        UINT32  n2b1Op          : 3;
        UINT32  n2b1NextDecPtr  : 12;
        UINT32  n2b1JmpRel      : 1;
        UINT32  n2b1JmpBw       : 1;
        UINT32  n2b1NextDataPtr : 9;

        UINT32  n2b2valid       : 1;
        UINT32  n2b2mask        : 16;
        UINT32  n2b2CompData    : 16;
        UINT32  n2b2Op          : 3;
        UINT32  n2b2NextDecPtr  : 12;
        UINT32  n2b2JmpRel      : 1;
        UINT32  n2b2JmpBw       : 1;
        UINT32  n2b2NextDataPtr : 9;

        UINT32  n2b3valid       : 1;
        UINT32  n2b3mask        : 16;
        UINT32  n2b3CompData    : 16;
        UINT32  n2b3Op          : 3;
        UINT32  n2b3NextDecPtr  : 12;
        UINT32  n2b3JmpRel      : 1;
        UINT32  n2b3JmpBw       : 1;
        UINT32  n2b3NextDataPtr : 9;
        } _WRS_PACK_ALIGN (1) ddn;

    struct
        {

        /* header */

        UINT32  resv            : 3;
        UINT32  nodeType        : 3;

        /* key */

        UINT32  k31Pri          : 3;
        UINT32  k31RstPtr       : 10;

        UINT32  k30Pri          : 3;
        UINT32  k30RstPtr       : 10;

        UINT32  k29Pri          : 3;
        UINT32  k29RstPtr       : 10;

        UINT32  k28Pri          : 3;
        UINT32  k28RstPtr       : 10;

        UINT32  k27Pri          : 3;
        UINT32  k27RstPtr       : 10;

        UINT32  k26Pri          : 3;
        UINT32  k26RstPtr       : 10;

        UINT32  k25Pri          : 3;
        UINT32  k25RstPtr       : 10;

        UINT32  k24Pri          : 3;
        UINT32  k24RstPtr       : 10;

        UINT32  k23Pri          : 3;
        UINT32  k23RstPtr       : 10;

        UINT32  k22Pri          : 3;
        UINT32  k22RstPtr       : 10;

        UINT32  k21Pri          : 3;
        UINT32  k21RstPtr       : 10;

        UINT32  k20Pri          : 3;
        UINT32  k20RstPtr       : 10;

        UINT32  k19Pri          : 3;
        UINT32  k19RstPtr       : 10;

        UINT32  k18Pri          : 3;
        UINT32  k18RstPtr       : 10;

        UINT32  k17Pri          : 3;
        UINT32  k17RstPtr       : 10;

        UINT32  k16Pri          : 3;
        UINT32  k16RstPtr       : 10;

        UINT32  k15Pri          : 3;
        UINT32  k15RstPtr       : 10;

        UINT32  k14Pri          : 3;
        UINT32  k14RstPtr       : 10;

        UINT32  k13Pri          : 3;
        UINT32  k13RstPtr       : 10;

        UINT32  k12Pri          : 3;
        UINT32  k12RstPtr       : 10;

        UINT32  k11Pri          : 3;
        UINT32  k11RstPtr       : 10;

        UINT32  k10Pri          : 3;
        UINT32  k10RstPtr       : 10;

        UINT32  k9Pri          : 3;
        UINT32  k9RstPtr       : 10;

        UINT32  k8Pri          : 3;
        UINT32  k8RstPtr       : 10;

        UINT32  k7Pri          : 3;
        UINT32  k7RstPtr       : 10;

        UINT32  k6Pri          : 3;
        UINT32  k6RstPtr       : 10;

        UINT32  k5Pri          : 3;
        UINT32  k5RstPtr       : 10;

        UINT32  k4Pri          : 3;
        UINT32  k4RstPtr       : 10;

        UINT32  k3Pri          : 3;
        UINT32  k3RstPtr       : 10;

        UINT32  k2Pri          : 3;
        UINT32  k2RstPtr       : 10;

        UINT32  k1Pri          : 3;
        UINT32  k1RstPtr       : 10;

        UINT32  k0Pri          : 3;
        UINT32  k0RstPtr       : 10;
        }  _WRS_PACK_ALIGN (1) kn;
    } _WRS_PACK_ALIGN (1) APM_MAC_CLE_PTREE_NODE;

/* nodeType field defination */

#define APM_MAC_CLE_PTREE_NODE_TYPE_INV     0 /* invalid node */
#define APM_MAC_CLE_PTREE_NODE_TYPE_KN      1 /* Key node */
#define APM_MAC_CLE_PTREE_NODE_TYPE_EWDN    2 /* 8-way(single) decision node */
#define APM_MAC_CLE_PTREE_NODE_TYPE_FWDN_1  5 /* 4-way(dual) decision node */
                                              /* with 1st node valid only */
#define APM_MAC_CLE_PTREE_NODE_TYPE_FWDN_2  6 /* 4-way(dual) decision node */
                                              /* with 2nd node valid only */
#define APM_MAC_CLE_PTREE_NODE_TYPE_FWDN    7 /* 4-way(dual) decision node */
                                              /* with 2 nodes valid */

/* number for branch or key in one node */

#define APM_MAC_CLE_PTREE_EWDN_BRANCH_NUM   8
#define APM_MAC_CLE_PTREE_FWDN_BRANCH_NUM   4
#define APM_MAC_CLE_PTREE_KN_KEY_NUM        32

/* Patricia Tree node state */

typedef struct
    {
    UINT32  nodeType;
    UINT32  brachKeyUsed;
    } _WRS_PACK_ALIGN (1) APM_MAC_CLE_PTREE_NODE_STAT;

/* bit mask defination */

#define APM_MAC_CLE_PTREE_NODE_STAT_USED(n)             (1 << (n))

#define APM_MAC_CLE_PTREE_NODE_STAT_USED_FWDN1(n)       (1 << (n))
#define APM_MAC_CLE_PTREE_NODE_STAT_USED_FWDN2(n)       \
    (1 << ((n) + APM_MAC_CLE_PTREE_FWDN_BRANCH_NUM))
#define APM_MAC_CLE_PTREE_NODE_STAT_USED_FWDN1_MASK     0xF
#define APM_MAC_CLE_PTREE_NODE_STAT_USED_FWDN2_MASK     0xF0

/* Parser config */

typedef struct
    {
    UINT32      startNodePtr;
    UINT32      startPktPtr;
    UINT32      defRst;
    UINT32      maxHop;
    } APM_MAC_CLE_PARSER_CFG;

IMPORT void vxbApmPProMacCleInit (VXB_DEVICE_ID pDev);

IMPORT void vxbApmPProMacCleParserInit
    (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_PARSER_CFG *    parserCfg
    );
IMPORT void vxbApmPProMacCleParserStop (VXB_DEVICE_ID pDev);

IMPORT STATUS apmMacCleDbEntryAdd (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_DBRAM_ENTRY *   pDbEntry,
    BOOL                        overWrite
    );
IMPORT STATUS apmMacCleDbEntryDel
    (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_DBRAM_ENTRY *   pDbEntry
    );

IMPORT STATUS apmMacClePTreeBranchKeyAdd
    (
    VXB_DEVICE_ID                   pDev,
    APM_MAC_CLE_PTREE_NODE_HDR *    pPtNodeHdr,
    void *                          pBranchKey,
    BOOL                            overWrite
    );
IMPORT STATUS apmMacClePTreeBranchKeyDel
    (
    VXB_DEVICE_ID                   pDev,
    APM_MAC_CLE_PTREE_NODE_HDR *    pPtNodeHdr,
    UINT32                          branchKeyindex
    );

IMPORT STATUS apmMacClePTreeNodeCreate
    (
    VXB_DEVICE_ID                   pDev,
    APM_MAC_CLE_PTREE_NODE_HDR *    pPtNodeHdr,
    BOOL                            overWrite
    );
IMPORT STATUS apmMacClePTreeNodeDestroy
    (
    VXB_DEVICE_ID                   pDev,
    APM_MAC_CLE_PTREE_NODE_HDR *    pPtNodeHdr
    );

IMPORT STATUS apmMacCleAvlEntryAdd (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_AVLRAM_INFO *   pAvlRamInfo
    );
IMPORT STATUS apmMacCleAvlEntryDel
    (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_AVLRAM_INFO *   pAvlRamInfo
    );
IMPORT STATUS apmMacCleAvlEntrySearch
    (
    VXB_DEVICE_ID               pDev,
    APM_MAC_CLE_AVLRAM_INFO *   pAvlRamInfo
    );
IMPORT STATUS apmMacCleAvlEntryRd
    (
    VXB_DEVICE_ID               pDev,
    UINT32                      index,
    APM_MAC_CLE_AVLRAM_INFO *   pAvlRamInfo
    );
IMPORT STATUS apmMacCleAvlEntryWr
    (
    VXB_DEVICE_ID               pDev,
    UINT32                      index,
    APM_MAC_CLE_AVLRAM_INFO *   pAvlRamInfo
    );

IMPORT void apmMacCleShow
    (
    VXB_DEVICE_ID   pDev,
    int             verbose
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbApmPProMacEndh */
