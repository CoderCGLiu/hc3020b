/* vxbApmPProMacEnd.h - header file for APM PacketPro MAC END driver */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,19dec11,x_z  replaced atomic variables with pointers to proper memory space;
                 fixed documentation issues;
                 removed APM_MAC_VXB_SHOW_ON.
01b,15nov11,x_z  removed unused apmMacRxIdx from private instance context;
                 added drvCtrl into shared instance context.
01a,09jun11,x_z  written based on 01d of templateVxbEnd.h.
*/

#ifndef __INCvxbApmPProMacEndh
#define __INCvxbApmPProMacEndh

#ifdef __cplusplus
extern "C" {
#endif

/* driver generic defines */

#define APM_MAC_NAME                    "apmMac"
#define APM_MAC_MTU                     1500
#define APM_MAC_JUMBO_MTU               9000
#define APM_MAC_TUPLE_CNT               512
#define APM_MAC_TIMEOUT                 1000
#define APM_MAC_MAX_RX                  32
#define APM_MAC_MAX_TX_COMP             16
#define APM_MAC_INC_DESC(x, y)          ((x) = ((x + 1) & (y - 1)))

/* define to reduce the interrupt number */

#undef APM_MAC_TX_MODERATION

/* 2 ethernet ports supported at most */

#define APM_MAC_PORT_NUM                2

/*
 * The buffer length of messages for Rx Free Pool should be larger that
 * cluster size.
 */

#define APM_MAC_CLSIZE                  1536
#define APM_MAC_JUMBO_CLSIZE            9056

/* Should be larger than (APM_QMAN_MSG_NEXT_BUF_MAX + 1) at least */

#define APM_MAC_MAXFRAG                 \
    (APM_QMAN_MSG_NEXT_BUF_MAX + APM_QMAN_MSG_LLBUF_MAX)

/*
 * Rx Queue Configuration
 *
 * For Rx, one packet may be placed in one buffer only because the buffers is
 * enough to saved one packet, so only 32-bytes messages need be used for the Rx
 * ingress queue, and 16-bytes messages can only be used for Rx free pool.
 * Otherwise, some error messages may be received, so the following rule must be
 * followed:
 *
 * (Rx ingress queue size) > (buffer number) * 32
 * (Rx free pool size) >= (buffer number) * 16
 */

#define APM_MAC_RX_BUF_CNT              256
#define APM_MAC_RX_INGQUE_SIZE          APM_QMAN_QSIZE_16KB
#define APM_MAC_RX_FP_SIZE              APM_QMAN_QSIZE_16KB

/*
 * Tx Queue Configuration
 *
 * For Tx, one packet may be placed in one or more buffers, so 32-bytes messages
 * and 64-bytes messages may be used for the Tx egress queue and completion
 * queue. Otherwise, some error messages may be received in the TX completion
 * queue, So the following rule must be followed:
 *
 * (Tx queue size) > (buffer number) * 64
 */

#define APM_MAC_TX_BUF_CNT              64
#define APM_MAC_TX_QUE_SIZE             APM_QMAN_QSIZE_16KB

/* Direct Access Registers */

/* Ethernet Block Registers */

/* Ethernet Block information */

#define APM_MAC_ENET_BID    0x2000 /* Ethernet Block Device ID */
#define APM_MAC_ENET_FPGA   0x2004 /* Ethernet FPGA Information */

/* bit definitions for Ethernet Block Device ID Register */

#define APM_MAC_ENET_BID_REV(x)     (((x) >> 8) & 0x3) /* Revision */
#define APM_MAC_ENET_BID_BUSID(x)   (((x) >> 5) & 0x7) /* Bus ID */
#define APM_MAC_ENET_BID_DEVID(x)   ((x) & 0x1F) /* Device ID */

/* bit definitions for Ethernet FPGA Information Register */

#define APM_MAC_ENET_FPGA_DATE(x)   (((x) >> 24) & 0xFF) /* creation Date */
#define APM_MAC_ENET_FPGA_MONTH(x)  (((x) >> 16) & 0xFF) /* creation Month */
#define APM_MAC_ENET_FPGA_YEAR(x)   (((x) >> 8) & 0xFF)  /* creation Year */
#define APM_MAC_ENET_FPGA_VER(x)    ((x) & 0xFF) /* Version */

/* Ethernet Rx System Interface Sub-Block */

#define APM_MAC_RSIF_CFG            0x2010 /* Config */
#define APM_MAC_RSIF_PRI_PLC0       0x2014 /* Priority Policer 0 */
#define APM_MAC_RSIF_PRI_PLC1       0x2018 /* Priority Policer 1 */
#define APM_MAC_RSIF_PRI_PLC2       0x201C /* Priority Policer 2 */
#define APM_MAC_RSIF_FLWG_PLC0      0x2020 /* Flowgroup Policer 0 */
#define APM_MAC_RSIF_FLWG_PLC1      0x2024 /* Flowgroup Policer 1 */
#define APM_MAC_RSIF_FLWG_PLC2      0x2028 /* Flowgroup Policer 2 */
#define APM_MAC_RSIF_PFLW_PLC0      0x202C /* Perflow Policer 0 */
#define APM_MAC_RSIF_PFLW_PLC1      0x2030 /* Perflow Policer 1 */
#define APM_MAC_RSIF_PFLW_PLC2      0x2034 /* Perflow Policer 2 */
#define APM_MAC_RSIF_PLC_STS        0x2038 /* Number of Packets */
                                           /* Dropped By All Policers */
#define APM_MAC_RSIF_PRI_PLC_STS    0x203C /* Number of Packets Dropped By */
                                           /* Priority Policers */
#define APM_MAC_RSIF_FLWG_PLC_STS   0x2040 /* Number of Packets Dropped By */
                                           /* Flowgroup Policers */
#define APM_MAC_RSIF_PFLW_PLC_STS   0x2044 /* Number of Packets Dropped By */
                                           /* Perflow Policers */
#define APM_MAC_RSIF_RAM_DBG        0x2048 /* RAM Debug */
#define APM_MAC_RSIF_WOL_MODE       0x204C /* WOL Mode */
#define APM_MAC_RSIF_FIFO_EMPTY_STS 0x2050 /* FIFO Empty Status */
#define APM_MAC_RSIF_INT            0x2054 /* Inrerrupt */
#define APM_MAC_RSIF_INT_MASK       0x2058 /* Inrerrupt Mask*/
#define APM_MAC_RSIF_FINT           0x205C /* Inrerrupt */
#define APM_MAC_RSIF_FINT_MASK      0x2060 /* Inrerrupt Mask*/
#define APM_MAC_RSIF_STS            0x2064 /* Status */
#define APM_MAC_RSIF_H0_0           0x2068 /* H0 Info Override 0 */
                                           /* (Lower 32 Bits) */
#define APM_MAC_RSIF_H0_1           0x206C /* H0 Info Override 1 */
#define APM_MAC_RSIF_H1_0           0x2070 /* H1 Info Override 0 */
                                           /* (Lower 32 Bits) */
#define APM_MAC_RSIF_H1_1           0x2074 /* H1 Info Override 1 */
#define APM_MAC_RSIF_FPBTIMEOUT_STS 0x2078 /* Number of FP Buffer Timeout */
#define APM_MAC_RSIF_STS_CNTR_P0    0x207C /* Port 0 Statistics */
#define APM_MAC_RSIF_STS_CNTR_P1    0x2080 /* Port 1 Statistics */

/* bit definitions for Ethernet RSIF Config Register */

#define APM_MAC_RSIF_CFG_FPTO_EN    0x80000000 /* FP Buffer Timeout Enable */
                                               /* for All buffers(Unsupport */
                                               /* by APM86290/APM86190 Rev0 */

#define APM_MAC_RSIF_CFG_FPTO(x)    (((x) & 0x7F) << 24) /* Cycles For FP */
#define APM_MAC_RSIF_CFG_FPTO_MASK  0x7F /* Buffer Timeout */
#define APM_MAC_RSIF_CFG_FPTO_SHIFT 24

#define APM_MAC_RSIF_CFG_FPDP_EN    0x00200000 /* Drop Packet for timeout of */
                                               /* the first FP buffer Enable */
                                               /* Unsupport by Rev0 of */
                                               /* APM86290/APM86190 */

#define APM_MAC_RSIF_CFG_BIG_ENDIAN 0x00100000 /* Big Endian */

#define APM_MAC_RSIF_CFG_BUF_THR(x)     (((x) & 0x1F) << 12) /* Port Data */
#define APM_MAC_RSIF_CFG_BUF_THR_DEF    0x10 /* Threshold */
#define APM_MAC_RSIF_CFG_BUF_THR_MASK   0x1F
#define APM_MAC_RSIF_CFG_BUF_THR_SHIFT  12

#define APM_MAC_RSIF_CFG_CTRLBUF_THR(x)     (((x) & 0x1F) << 4) /* Port Burst */
#define APM_MAC_RSIF_CFG_CTRLBUF_THR_DEF    0x10
#define APM_MAC_RSIF_CFG_CTRLBUF_THR_MASK   0x1F /* Information (Control) */
#define APM_MAC_RSIF_CFG_CTRLBUF_THR_SHIFT  4 /* Buffer Threshold */

#define APM_MAC_RSIF_CFG_CLEBUF_THR(x)      ((x) & 0x7) /* Port packet */
#define APM_MAC_RSIF_CFG_CLEBUF_THR_DEF     0x4
#define APM_MAC_RSIF_CFG_CLEBUF_THR_MASK    0x7 /* classification Information */
                                                /* Buffer Threshold */

/* bit definitions for Ethernet RSIF Priority Policer Register */

#define APM_MAC_RSIF_PRI_PLC0_ADD_CRDT(x)   ((x) &0xFFFFFF) /* Credits To Be */
#define APM_MAC_RSIF_PRI_PLC0_ADD_CRDT_DEF  0xFFFF /* Added To Each Policer */
#define APM_MAC_RSIF_PRI_PLC0_ADD_CRDT_MASK 0xFFFFFF /* In Each Update Cycle */

#define APM_MAC_RSIF_PRI_PLC1_UPD_PROD(x)       (((x) &0x7F) << 24)
#define APM_MAC_RSIF_PRI_PLC1_UPD_PROD_DEF      0x64 /* Credits Update Period */
#define APM_MAC_RSIF_PRI_PLC1_UPD_PROD_MASK     0xFFFFFF /* = ((This Value) * */
#define APM_MAC_RSIF_PRI_PLC1_UPD_PROD_SHIFT    24 /* 40us) */

#define APM_MAC_RSIF_PRI_PLC1_MAX_CRDT(x)   ((x) &0xFFFFFF) /* Maximum Value */
#define APM_MAC_RSIF_PRI_PLC1_MAX_CRDT_DEF  0xFFFFFF /* Of Credits That Can */
#define APM_MAC_RSIF_PRI_PLC1_MAX_CRDT_MASK 0xFFFFFF /* Be Accumulated */

#define APM_MAC_RSIF_PRI_PLC2_DROP_THR(x)   ((x) &0xFFFFFF) /* Credit */
#define APM_MAC_RSIF_PRI_PLC2_DROP_THR_DEF  0x40 /* Threshold Below Which A */
#define APM_MAC_RSIF_PRI_PLC2_DROP_THR_MASK 0xFFFFFF /* Packet Is Dropped */

/* bit definitions for Ethernet RSIF Flowgroup Policer Register */

#define APM_MAC_RSIF_FLWG_PLC0_ADD_CRDT(x)   ((x) &0xFFFFFF) /* Credits To Be */
#define APM_MAC_RSIF_FLWG_PLC0_ADD_CRDT_DEF  0xFFFF /* Added To Each Policer */
#define APM_MAC_RSIF_FLWG_PLC0_ADD_CRDT_MASK 0xFFFFFF /* In Each Update Cycle */

#define APM_MAC_RSIF_FLWG_PLC1_MAX_CRDT(x)   ((x) &0xFFFFFF) /* Maximum Value */
#define APM_MAC_RSIF_FLWG_PLC1_MAX_CRDT_DEF  0xFFFFFF /* Of Credits That Can */
#define APM_MAC_RSIF_FLWG_PLC1_MAX_CRDT_MASK 0xFFFFFF /* Be Accumulated */

#define APM_MAC_RSIF_FLWG_PLC2_DROP_THR(x)   ((x) &0xFFFFFF) /* Credit */
#define APM_MAC_RSIF_FLWG_PLC2_DROP_THR_DEF  0x40 /* Threshold Below Which A */
#define APM_MAC_RSIF_FLWG_PLC2_DROP_THR_MASK 0xFFFFFF /* Packet Is Dropped */

/* bit definitions for Ethernet RSIF Perflow Policer Register */

#define APM_MAC_RSIF_PFLW_PLC0_PRI_EN  0x04000000 /* Priority Policer Enable */
#define APM_MAC_RSIF_PFLW_PLC0_FLWG_EN 0x02000000 /* Flowgroup Policer Enable */
#define APM_MAC_RSIF_PFLW_PLC0_PFLW_EN 0x01000000 /* Perflow Policer Enable */

#define APM_MAC_RSIF_PFLW_PLC0_ADD_CRDT(x)   ((x) &0xFFFFFF) /* Credits To Be */
#define APM_MAC_RSIF_PFLW_PLC0_ADD_CRDT_DEF  0xFFFF /* Added To Each Policer */
#define APM_MAC_RSIF_PFLW_PLC0_ADD_CRDT_MASK 0xFFFFFF /* In Each Update Cycle */

#define APM_MAC_RSIF_PFLW_PLC1_MAX_CRDT(x)   ((x) &0xFFFFFF) /* Maximum Value */
#define APM_MAC_RSIF_PFLW_PLC1_MAX_CRDT_DEF  0xFFFFFF /* Of Credits That Can */
#define APM_MAC_RSIF_PFLW_PLC1_MAX_CRDT_MASK 0xFFFFFF /* Be Accumulated */

#define APM_MAC_RSIF_PFLW_PLC2_DROP_THR(x)   ((x) &0xFFFFFF) /* Credit */
#define APM_MAC_RSIF_PFLW_PLC2_DROP_THR_DEF  0x40 /* Threshold Below Which A */
#define APM_MAC_RSIF_PFLW_PLC2_DROP_THR_MASK 0xFFFFFF /* Packet Is Dropped */

/* bit definitions for Ethernet RSIF RAM Debug Register */

/*
 * If bit 6 is 1, then port 1 enqueue messages will have bit[7, 10] in the RTYPE
 * field; otherwise RType from FP buffers are used in the enqueue message.
 */

#define APM_MAC_RSIF_RAM_DBG_PORT1_RTYPE_EN     0x02000000
#define APM_MAC_RSIF_RAM_DBG_PORT1_RTYPE(x)     (((x) & 0xF) << 21)
#define APM_MAC_RSIF_RAM_DBG_PORT1_RTYPE_DEF    8
#define APM_MAC_RSIF_RAM_DBG_PORT1_RTYPE_MASK   0xF
#define APM_MAC_RSIF_RAM_DBG_PORT1_RTYPE_SHIFT  21

/*
 * If bit 11 is 1, then port 0 enqueue messages will have bit[12, 15] in the
 * RTYPE field; otherwise RType from FP buffers are used in the enqueue message.
 */

#define APM_MAC_RSIF_RAM_DBG_PORT0_RTYPE_EN     0x00100000
#define APM_MAC_RSIF_RAM_DBG_PORT0_RTYPE(x)     (((x) & 0xF) << 16)
#define APM_MAC_RSIF_RAM_DBG_PORT0_RTYPE_DEF    0
#define APM_MAC_RSIF_RAM_DBG_PORT0_RTYPE_MASK   0xF
#define APM_MAC_RSIF_RAM_DBG_PORT0_RTYPE_SHIFT  16

/* bit definitions for Ethernet RSIF WoL Mode Register */

#define APM_MAC_RSIF_WOL_MODE_TESTEN    0x00000004 /* Test Enable */
#define APM_MAC_RSIF_WOL_MODE_WKUP      0x00000002 /* Waked Up */
#define APM_MAC_RSIF_WOL_MODE_HALT      0x00000001 /* Halt */

/* bit definitions for Ethernet RSIF FIFO Empty Status Register */

/* Port 1 FIFO Empty Status */

#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1LCL   0x00400000 /* Pckwd Local/Litte  */
                                                       /* Rxbuff FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1CHK   0x00200000 /* Chksum Buffer FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1TM    0x00100000 /* Timestamp  Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1ERR   0x00080000 /* ERROR Buffer FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1CLE   0x00040000 /* Classifier Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1CTRL  0x00020000 /* Contrrol Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P1BUF   0x00010000 /* Buffer FIFO */

/* Policer FIFO Empty Status for 2 ports */

#define APM_MAC_RSIF_FIFO_EMPTY_STS_PLC    0x00000080

/* Port 0 FIFO Empty Status */

#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0LCL   0x00000040 /* Pckwd Local/Litte  */
                                                       /* Rxbuff FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0CHK   0x00000020 /* Chksum Buffer FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0TM    0x00000010 /* Timestamp  Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0ERR   0x00000008 /* ERROR Buffer FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0CLE   0x00000004 /* Classifier Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0CTRL  0x00000002 /* Contrrol Buffer */
                                                       /* FIFO */
#define APM_MAC_RSIF_FIFO_EMPTY_STS_P0BUF   0x00000001 /* Buffer FIFO */

/* bit definitions for Ethernet RSIF Interrupt and Interrupt Mask Register */

/* Port 1 Interrupt */

#define APM_MAC_RSIF_INT_P1LCL_OF   0x20000000 /* Local Rxbuff FIFO Overflow */
#define APM_MAC_RSIF_INT_P1LCL_UF   0x10000000 /* Local Rxbuff FIFO Underflow */
#define APM_MAC_RSIF_INT_P1CHK_OF   0x08000000 /* Local Chksum Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P1CHK_UF   0x04000000 /* Local Chksum Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P1TM_OF    0x02000000 /* Local Timestamp Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P1TM_UF    0x01000000 /* Local Timestamp Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P1ERR_OF   0x00800000 /* Local Error Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P1ERR_UF   0x00400000 /* Local Error Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P1CLE_OF   0x00200000 /* Local Classifier Buffer */
                                               /* FIFOOverflow */
#define APM_MAC_RSIF_INT_P1CLE_UF   0x00100000 /* Local Classifier Buffer */
                                               /* FIFO Underflow */
#define APM_MAC_RSIF_INT_P1CTRL_OF  0x00080000 /* Local Control Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P1CTRL_UF  0x00040000 /* Local Control Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P1BUF_OF   0x00020000 /* Local Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P1BUF_UF   0x00010000 /* Local Buffer FIFO */
                                               /* Underflow */

/* Policer FIFO Interrupt for 2 ports */

#define APM_MAC_RSIF_INT_PLC_OF     0x00008000 /* Overflow */
#define APM_MAC_RSIF_INT_PLC_UF     0x00004000 /* Underflow */

/* Port 0 Interrupt */

#define APM_MAC_RSIF_INT_P0LCL_OF   0x00002000 /* Local Rxbuff FIFO Overflow */
#define APM_MAC_RSIF_INT_P0LCL_UF   0x00001000 /* Local Rxbuff FIFO Underflow */
#define APM_MAC_RSIF_INT_P0CHK_OF   0x00000800 /* Local Chksum Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P0CHK_UF   0x00000400 /* Local Chksum Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P0TM_OF    0x00000200 /* Local Timestamp Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P0TM_UF    0x00000100 /* Local Timestamp Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P0ERR_OF   0x00000080 /* Local Error Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P0ERR_UF   0x00000040 /* Local Error Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P0CLE_OF   0x00000020 /* Local Classifier Buffer */
                                               /* FIFOOverflow */
#define APM_MAC_RSIF_INT_P0CLE_UF   0x00000010 /* Local Classifier Buffer */
                                               /* FIFO Underflow */
#define APM_MAC_RSIF_INT_P0CTRL_OF  0x00000008 /* Local Control Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P0CTRL_UF  0x00000004 /* Local Control Buffer FIFO */
                                               /* Underflow */
#define APM_MAC_RSIF_INT_P0BUF_OF   0x00000002 /* Local Buffer FIFO */
                                               /* Overflow */
#define APM_MAC_RSIF_INT_P0BUF_UF   0x00000001 /* Local Buffer FIFO */
                                               /* Underflow */

/* bit definitions for Ethernet RSIF FInterrupt and FInterrupt Mask Register */

/* Port 1 Interrupt */

#define APM_MAC_RSIF_FINT_P1MIRERR  0x00080000 /* Mirror Buffer Address */
                                               /* Offset/Length is Unmatched */
                                               /* With Normal XFR Address */
                                               /* Offset/Length */
#define APM_MAC_RSIF_FINT_P1SPLIT   0x00040000 /* Split Boundary Cannot be */
                                               /* Accomodated in the first */
                                               /* buffer */
#define APM_MAC_RSIF_FINT_P1FPTM    0x00020000 /* Packet Dropped By RSIF */
                                               /* because FP Buffer was */
                                               /* Unavailable from QMI */
#define APM_MAC_RSIF_FINT_P1HBFWR   0x00010000 /* HBF Write Error */

/* Port 0 Interrupt */

#define APM_MAC_RSIF_FINT_P0MIRERR  0x00000008 /* Mirror Buffer Address */
                                               /* Offset/Length is Unmatched */
                                               /* With Normal XFR Address */
                                               /* Offset/Length */
#define APM_MAC_RSIF_FINT_P0SPLIT   0x00000004 /* Split Boundary Cannot be */
                                               /* Accomodated in the first */
                                               /* buffer */
#define APM_MAC_RSIF_FINT_P0FPTM    0x00000002 /* Packet Dropped By RSIF */
                                               /* because FP Buffer was */
                                               /* Unavailable from QMI */
#define APM_MAC_RSIF_FINT_P0HBFWR   0x00000001 /* HBF Write Error */

/* bit definitions for Ethernet RSIF Status Register */

/* HBF write response when HBF Write Error is triggered */

#define APM_MAC_RSIF_STS_P1HBF_RESP(x)  (((x) >> 16) & 0x3)
#define APM_MAC_RSIF_STS_P0HBF_RESP(x)  ((x) & 0x3)

/* bit definitions for Ethernet RSIF H0/H1 Information Override 1 Register */

#define APM_MAC_RSIF_H0_1_HI(x) ((x) & 0xFF)
#define APM_MAC_RSIF_H0_1_HR    0x00000200
#define APM_MAC_RSIF_H0_1_HE    0x00000100

#define APM_MAC_RSIF_H1_1_OVEN  0x00010000 /* Enables Override of CLE Output */
                                           /* for H0/H1 info with the values */

#define APM_MAC_RSIF_H1_1_HI(x) ((x) & 0xFF)
#define APM_MAC_RSIF_H1_1_HR    0x00000200
#define APM_MAC_RSIF_H1_1_HE    0x00000100

/* bit definitions for Ethernet RSIF FP Buffer Timeout Statistics Register */

#define APM_MAC_RSIF_FPBTIMEOUT_STS_P1(x)   (((x) >> 16) & 0xFFFF)
#define APM_MAC_RSIF_FPBTIMEOUT_STS_P0(x)   ((x)  & 0xFFFF)

/* bit definitions for Ethernet RSIF Port 0/1 Statistics Register */

/*
 * Total number of pkts processed by port 0/1 mirror DMA Manager, for which an
 * enqueue message was generated
 */

#define APM_MAC_RSIF_STS_CNTR_MIRDMGR(x)   (((x) >> 16) & 0xFFFF)

/*
 * Total number of pkts processed by port 0/1 DMA Manager, for which an
 * enqueue message was generated
 */

#define APM_MAC_RSIF_STS_CNTR_DMGR(x)       ((x)  & 0xFFFF)

/* Ethernet Tx System Interface Sub-Block */

#define APM_MAC_TSIF_CFG0           0x2100 /* Config 0 */
#define APM_MAC_TSIF_CFG1           0x2104 /* Config 1 */
#define APM_MAC_TSIF_P0MSS0         0x2108 /* Maximum Segment Size */
#define APM_MAC_TSIF_P0MSS1         0x210C
#define APM_MAC_TSIF_P1MSS0         0x2110
#define APM_MAC_TSIF_P1MSS1         0x2114
#define APM_MAC_TSIF_FIFO_EPT_STS   0x2118 /* FIFO Empty Status */
#define APM_MAC_TSIF_INT            0x212C /* Interrupt */
#define APM_MAC_TSIF_INT_MASK       0x2130 /* Interrupt Mask */
#define APM_MAC_TSIF_FINT           0x2134 /* Interrupt */
#define APM_MAC_TSIF_FINT_MASK      0x2138 /* Interrupt Mask */
#define APM_MAC_TSIF_STS0           0x213C /* Status */
#define APM_MAC_TSIF_STS1           0x2140
#define APM_MAC_TSIF_STS2           0x2144
#define APM_MAC_TSIF_STS3           0x2148

/* bit definitions for Ethernet TSIF Config 0/1 Register */

#define APM_MAC_TSIF_CFG_ENET          0x00004000 /* ENET Mode (default) */
#define APM_MAC_TSIF_CFG_CLE           0x00002000 /* Classifier Mode */
#define APM_MAC_TSIF_CFG_SOSEOS_PERBUF 0x00001000 /* Gives SOS and EOS flag */
                                                  /* for every Buffer */
#define APM_MAC_TSIF_CFG_DEC_WMSG      0x00000800 /* Dealloc buffers of work */
                                                  /* message */
#define APM_MAC_TSIF_CFG_CMSG_WMSG     0x00000400 /* Send Completion Message */
                                                  /* for work message */
#define APM_MAC_TSIF_CFG_ENDIAN_BIG    0x00000200 /* Big Endian (default) */
#define APM_MAC_TSIF_CFG_WMSG_SWAP     0x00000100 /* Swap work message from  */
                                                  /* QMI */

#define APM_MAC_TSIF_CFG_RRM_THR(x)    (((x) & 7) << 4) /* TSIF Read Request */
#define APM_MAC_TSIF_CFG_RRM_THR_DEF   4 /* Machine FIFO threshold */
#define APM_MAC_TSIF_CFG_RRM_THR_MASK  0x7
#define APM_MAC_TSIF_CFG_RRM_THR_SHIFT 4

#define APM_MAC_TSIF_CFG_RDM_THR(x)    ((x) & 7) /* TSIF Read Data */
#define APM_MAC_TSIF_CFG_RDM_THR_DEF   4 /* Machine FIFO threshold */

/* bit definitions for Ethernet TSIF Maximum segment size 0 Registers */

#define APM_MAC_TSIF_MSS1(x)    (((x) & 0x3FFF) << 16) /* Max segment size */
#define APM_MAC_TSIF_MSS1_DEF   1448
#define APM_MAC_TSIF_MSS1_MASK  0x3FFF
#define APM_MAC_TSIF_MSS1_SHIFT 16

#define APM_MAC_TSIF_MSS0(x)    ((x) & 0x3FFF) /* Max segment size (MSS) 0 */
#define APM_MAC_TSIF_MSS0_DEF   1448
#define APM_MAC_TSIF_MSS0_MASK  0x3FFF

/* bit definitions for Ethernet TSIF Maximum segment size 1 Registers */

#define APM_MAC_TSIF_MSS3(x)    (((x) & 0x3FFF) << 16) /* Max segment size */
#define APM_MAC_TSIF_MSS3_DEF   0x05EE /* (MSS) 3 */
#define APM_MAC_TSIF_MSS3_MASK  0x3FFF
#define APM_MAC_TSIF_MSS3_SHIFT 16

#define APM_MAC_TSIF_MSS2(x)    ((x) & 0x3FFF) /* Max segment size (MSS) 2 */
#define APM_MAC_TSIF_MSS2_DEF   0x0400

/* bit definitions for Ethernet TSIF FIFO Empty Status Register */

#define APM_MAC_TSIF_FIFO_EPT_STS_P1RDM 0x00040000 /* Port 1 RDM Buffer FIFO */
#define APM_MAC_TSIF_FIFO_EPT_STS_P1RRM 0x00020000 /* Port 1 RRM Buffer FIFO */
#define APM_MAC_TSIF_FIFO_EPT_STS_P1AMA 0x00010000 /* Port 1 AMA Buffer FIFO */

#define APM_MAC_TSIF_FIFO_EPT_STS_P0RDM 0x00040000 /* Port 0 RDM Buffer FIFO */
#define APM_MAC_TSIF_FIFO_EPT_STS_P0RRM 0x00020000 /* Port 0 RRM Buffer FIFO */
#define APM_MAC_TSIF_FIFO_EPT_STS_P0AMA 0x00010000 /* Port 0 AMA Buffer FIFO */

/* bit definitions for Ethernet TSIF Interrupt Status and Mask Register */

/* Port 1 */

#define APM_MAC_TSIF_INT_P1RDM_OF   0x00200000 /* RDM Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P1RDM_UF   0x00100000 /* RDM Buffer FIFO Underflow */
#define APM_MAC_TSIF_INT_P1RRM_OF   0x00080000 /* RRM Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P1RRM_UF   0x00040000 /* RRM Buffer FIFO Underflow */
#define APM_MAC_TSIF_INT_P1AMA_OF   0x00020000 /* AMA Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P1AMA_UF   0x00010000 /* AMA Buffer FIFO Underflow */

/* Port 0 */

#define APM_MAC_TSIF_INT_P0RDM_OF   0x00000020 /* RDM Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P0RDM_UF   0x00000010 /* RDM Buffer FIFO Underflow */
#define APM_MAC_TSIF_INT_P0RRM_OF   0x00000008 /* RRM Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P0RRM_UF   0x00000004 /* RRM Buffer FIFO Underflow */
#define APM_MAC_TSIF_INT_P0AMA_OF   0x00000002 /* AMA Buffer FIFO Overflow */
#define APM_MAC_TSIF_INT_P0AMA_UF   0x00000001 /* AMA Buffer FIFO Underflow */

/* bit definitions for Ethernet TSIF FInterrupt Status and Mask Register */

/* Port 1 */

#define APM_MAC_TSIF_INT_P1LLRD_ERR 0x00040000 /* HBF Error when Reading Link */
                                               /* List */
#define APM_MAC_TSIF_INT_P1RD_ERR   0x00020000 /* HBF Error when Reading Data */
#define APM_MAC_TSIF_INT_P1_BADMSG  0x00010000 /* Bad Message Received */

/* Port 0 */

#define APM_MAC_TSIF_INT_P0LLRD_ERR 0x00040000 /* HBF Error when Reading Link */
                                               /* List */
#define APM_MAC_TSIF_INT_P0RD_ERR   0x00020000 /* HBF Error when Reading Data */
#define APM_MAC_TSIF_INT_P0_BADMSG  0x00010000 /* Bad Message Received */

/* bit definitions for Ethernet TSIF Status 0 Register */

/* Port 1*/

#define APM_MAC_TSIF_STS0_P1LLRD_RESP(x)    (((x) >> 18) & 3) /* HBF response */
#define APM_MAC_TSIF_STS0_P1LLRD_RESP_MASK  0x3 /* when Link List Read Error */
#define APM_MAC_TSIF_STS0_P1LLRD_RESP_SHIFT 18 /* is Raised */

#define APM_MAC_TSIF_STS0_P1RD_RESP(x)      (((x) >> 16) & 3) /* HBF response */
#define APM_MAC_TSIF_STS0_P1RD_RESP_MASK    0x3 /* when Read Error is Raised */
#define APM_MAC_TSIF_STS0_P1RD_RESP_SHIFT   16

/* Port 0 */

#define APM_MAC_TSIF_STS0_P0LLRD_RESP(x)    (((x) >> 18) & 3) /* HBF response */
#define APM_MAC_TSIF_STS0_P0LLRD_RESP_MASK  0x3 /* when Link List Read Error */
#define APM_MAC_TSIF_STS0_P0LLRD_RESP_SHIFT 18 /* is Raised */

#define APM_MAC_TSIF_STS0_P0RD_RESP(x)      (((x) >> 16) & 3) /* HBF response */
#define APM_MAC_TSIF_STS0_P0RD_RESP_MASK    0x3 /* when Read Error is Raised */
#define APM_MAC_TSIF_STS0_P0RD_RESP_SHIFT   16

/*
 * bit definitions for Ethernet TSIF Status 1 Register
 *
 * Number of valid work messages processed by TSIF
 */

#define APM_MAC_TSIF_STS1_P1WMSG_CNT(x)     (((x) >> 16) & 0xFFFF)
#define APM_MAC_TSIF_STS1_P1WMSG_CNT_MASK   0xFFFF
#define APM_MAC_TSIF_STS1_P1WMSG_CNT_SHIFT  16

#define APM_MAC_TSIF_STS1_P0WMSG_CNT(x)     ((x) & 0xFFFF)

/*
 * bit definitions for Ethernet TSIF Status 2 Register
 *
 * Number of valid messages processed by TSIF through dealloc FIFO interface
 */

#define APM_MAC_TSIF_STS2_P1DAWMSG_CNT(x)       (((x) >> 16) & 0xFFFF)
#define APM_MAC_TSIF_STS2_P1DAWMSG_CNT_MASK     0xFFFF
#define APM_MAC_TSIF_STS2_P1DAWMSG_CNT_SHIFT    16

#define APM_MAC_TSIF_STS2_P0DAWMSG_CNT(x)     ((x) & 0xFFFF)

/*
 * bit definitions for Ethernet TSIF Status 3 Register
 *
 * Number of bad work messages received by TSIF
 */

#define APM_MAC_TSIF_STS3_P1BADWMSG_CNT(x)      (((x) >> 16) & 0xFFFF)
#define APM_MAC_TSIF_STS3_P1BADWMSG_CNT_MASK    0xFFFF
#define APM_MAC_TSIF_STS3_P1BADWMSG_CNT_SHIFT   16

#define APM_MAC_TSIF_STS3_P0BADWMSG_CNT(x)      ((x) & 0xFFFF)

/* Ethernet TCP Segmentation Offload Sub-Block */

#define APM_MAC_CFG_LINK_AGGR       0x2290 /* Link Aggregation */
#define APM_MAC_MH_FIFO_FULL_THR0   0x2300 /* Message Hold Fifo Full */
                                           /* Threshold 0 */
#define APM_MAC_MH_FIFO_FULL_THR1   0x2304 /* Message Hold Fifo Full */
                                           /* Threshold 1 */
#define APM_MAC_DA_FIFO_FULL_THR0   0x2308 /* Dealloc Hold Fifo Full */
                                           /* Threshold 0 */
#define APM_MAC_DA_FIFO_FULL_THR1   0x230C /* Dealloc Hold Fifo Full */
                                           /* Threshold 1 */
#define APM_MAC_TSO_FIFO_STS        0x2310 /* TSO FIFO Status */
#define APM_MAC_TSO_CFG0            0x2314 /* TSO Config 0 */
#define APM_MAC_TSO_CFG1            0x2318 /* TSO Config 1 */
#define APM_MAC_TSO_CFG_INSVLAN0    0x231C /* TSO VLAN Inserted 0 */
#define APM_MAC_TSO_CFG_INSVLAN1    0x2320 /* TSO VLAN Inserted 1 */
#define APM_MAC_TSO_INT             0x2324 /* TSO Interrupt */
#define APM_MAC_TSO_INT_MASK        0x2328 /* TSO Interrupt Mask */
#define APM_MAC_TTF_FIFO_FULL_THR0  0x232C /* TSO TxBuff FIFO Full Threshold */
#define APM_MAC_TTF_FIFO_FULL_THR1  0x2330

/* bit definitions for Ethernet Link Aggregation Register */

#define APM_MAC_CFG_LINK_AGGR_MPAQ_PSEL(x)   (((x) & 1) << 3) /* Select which */
                                                     /* Port will service MPA */
                                                     /* (SLIMpro) queue */
#define APM_MAC_CFG_LINK_AGGR_MPAQ_PSEL_SHIFT   3
#define APM_MAC_CFG_LINK_AGGR_MPAQ_PSEL_MASK    1

/* Arbitration Mechanism for Link Aggregation of 2 ports */

#define APM_MAC_CFG_LINK_AGGR_ARB_SEL_RR    0x00000004 /* Round Robin */
#define APM_MAC_CFG_LINK_AGGR_ARB_SEL_ST    0x00000000 /* Strict priority */
                                                       /* (Port 0 > port 1)*/

#define APM_MAC_CFG_LINK_AGGR_EN        0x00000002 /* Enable Link Aggregation */
#define APM_MAC_CFG_LINK_AGGR_PSEL(x)   ((x) & 1) /* Select Port on which */
                                                  /* Link Aggregation is */
                                                  /* enabled */

/*
 * bit definitions for Ethernet Message Hold FIFO Full Threshold 0/1 Registers
 *
 * n is the index of queue that the Message Hold FIFO belongs.
 */

#define APM_MAC_MH_FIFO_FULL_THR(x, n)  (((x) & 0xF) << (32 - ((8 - (n)) << 2)))
#define APM_MAC_MH_FIFO_FULL_THR_DEF    0x8
#define APM_MAC_MH_FIFO_FULL_THR_MASK   0xF

/*
 * bit definitions for Ethernet Dealloc Hold FIFO Full Threshold 0/1 Registers
 */

#define APM_MAC_DA_FIFO_FULL_THR(x) ((x) & 0xF)

/* bit definitions for Ethernet TSO FIFO Status Register */

/* Port 1 */

/*  Rlevel Count for Message Hold FIFO */

#define APM_MAC_TSO_FIFO_STS_P1MH_RLVL_CNT(x)     (((x) >> 24) & 0xF)

/*  Rlevel Count for Dealloc FIFO */

#define APM_MAC_TSO_FIFO_STS_P1DA_RLVL_CNT(x)     (((x) >> 20) & 0xF)

/*  Rlevel Count for TSO Tx Buffer FIFO */

#define APM_MAC_TSO_FIFO_STS_P1TSOTXB_RLVL_CNT(x)  (((x) >> 16) & 0xF)

/* Port 0 */

/*  Rlevel Count for Message Hold FIFO */

#define APM_MAC_TSO_FIFO_STS_P0MH_RLVL_CNT(x)     (((x) >> 8) & 0xF)

/*  Rlevel Count for Dealloc FIFO */

#define APM_MAC_TSO_FIFO_STS_P0DA_RLVL_CNT(x)     (((x) >> 4) & 0xF)

/*  Rlevel Count for TSO Tx Buffer FIFO */

#define APM_MAC_TSO_FIFO_STS_P0TSOTXB_RLVL_CNT(x)  ((x) & 0xF)

/* bit definitions for Ethernet TSO Config 0/1 Registers */

#define APM_MAC_TSO_CFG_TCP_FLG_POS(x)  (((x) & 3) << 6) /* TCP Flag Position */
                                                         /* 0 - 15:10 */
                                                         /* 1 - 21:16 */
                                                         /* 2 - 10:15 */
                                                         /* 3 - 16:21 */
#define APM_MAC_TSO_CFG_TCP_FLG_POS_DEF 1

#define APM_MAC_TSO_CFG_MAC_HDR_LEN(x)  ((x) & 0x3F) /* MAC Proprietary */
                                                     /* Header Length */

/* bit definitions for Ethernet TSO Interrupt and Interrupt Mask Registers */

/* Port 1 */

#define APM_MAC_TSO_INT_P1MB_TTF_OF 0x00200000 /* TSO TxBuff FIFO OverFlow */
#define APM_MAC_TSO_INT_P1MB_TTF_UF 0x00100000 /* TSO TxBuff FIFO UnderFlow */
#define APM_MAC_TSO_INT_P1MH_DA_OF  0x00080000 /* MH Dealloc FIFO OverFlow */
#define APM_MAC_TSO_INT_P1MH_DA_UF  0x00040000 /* MH Dealloc FIFO UnderFlow */
#define APM_MAC_TSO_INT_P1MH_OF     0x00020000 /* MH FIFO OverFlow */
#define APM_MAC_TSO_INT_P1MH_UF     0x00010000 /* MH FIFO UnderFlow */

/* Port 0 */

#define APM_MAC_TSO_INT_P0MB_TTF_OF 0x00000020 /* TSO TxBuff FIFO OverFlow */
#define APM_MAC_TSO_INT_P0MB_TTF_UF 0x00000010 /* TSO TxBuff FIFO UnderFlow */
#define APM_MAC_TSO_INT_P0MH_DA_OF  0x00000008 /* MH Dealloc FIFO OverFlow */
#define APM_MAC_TSO_INT_P0MH_DA_UF  0x00000004 /* MH Dealloc FIFO UnderFlow */
#define APM_MAC_TSO_INT_P0MH_OF     0x00000002 /* MH FIFO OverFlow */
#define APM_MAC_TSO_INT_P0MH_UF     0x00000001 /* MH FIFO UnderFlow */

/* bit definitions for Ethernet TSO TxBuff FIFO Full Threshold 0/1 Registers */

#define APM_MAC_TTF_FIFO_FULL_THR(x)    ((x) & 0xF) /* dealloc fifo full */
                                                    /* threshold */

/* Ethernet SPI to SAP and Security Association Database Sub-Block */

#define APM_MAC_SPI2SAP_ADDSPI          0x2400 /* Add SPI */
#define APM_MAC_SPI2SAP_ADDSPIEN        0x2404 /* Add SPI Enable */
#define APM_MAC_SPI2SAP_DELSPI          0x2408 /* Delete SPI */
#define APM_MAC_SPI2SAP_DELSPIEN        0x240C /* Delete SPI Enable */
#define APM_MAC_SPI2SAP_SEARCHSPI       0x2410 /* Search SPI */
#define APM_MAC_SPI2SAP_SEARCHSPIEN     0x2414 /* Search SPI Enable */
#define APM_MAC_SPI2SAP_CFG             0x2418 /* Config */
#define APM_MAC_SPI2SAP_DBG0            0x241C /* Debug */
#define APM_MAC_SPI2SAP_DBG1            0x2420
#define APM_MAC_SPI2SAP_DBG2            0x2424
#define APM_MAC_SPI2SAP_INT             0x2428 /* Interrupt */
#define APM_MAC_SPI2SAP_INT_MASK        0x242C /* Interrupt Mask */
#define APM_MAC_SPI2SAP_STS0            0x2430 /* Status */
#define APM_MAC_SPI2SAP_STS1            0x2434
#define APM_MAC_SPI2SAP_STS2            0x2438
#define APM_MAC_SPI2SAP_STS3            0x2440
#define APM_MAC_SPI2SAP_FIFO_EMPTY_STS  0x2444 /* FIFO Empty Status */
#define APM_MAC_SPI2SAP_INT0            0x2448 /* Interrupt 0 */
#define APM_MAC_SPI2SAP_INT0_MASK       0x244C /* Interrupt 0 Mask */
#define APM_MAC_ENET_SAD_ADRS_CMD       0x2460 /* SAD Address/Command */
#define APM_MAC_ENET_SAD_WDATA0         0x2464 /* SAD Write Data */
#define APM_MAC_ENET_SAD_WDATA1         0x2468
#define APM_MAC_ENET_SAD_WDATA2         0x246C
#define APM_MAC_ENET_SAD_WDATA3         0x2470
#define APM_MAC_ENET_SAD_RDATA0         0x2474 /* SAD Read Data */
#define APM_MAC_ENET_SAD_RDATA1         0x2478
#define APM_MAC_ENET_SAD_RDATA2         0x247C
#define APM_MAC_ENET_SAD_RDATA3         0x2480
#define APM_MAC_ENET_SAD_CMD_STAT       0x2484 /* SAD Command Done */

/* bit definitions for Ethernet Add/Delete/Search SPI Enable Registers */

#define APM_MAC_SPI2SAP_SPIEN_PORT(x)    (((x) & 1) << 2) /* Port */
#define APM_MAC_SPI2SAP_SPIEN_PORT_SHIFT 2
#define APM_MAC_SPI2SAP_SPIEN_PORT_MASK  1

#define APM_MAC_SPI2SAP_SPIEN_DIR(x)    (((x) & 1) << 1) /* Direction */
#define APM_MAC_SPI2SAP_SPIEN_DIR_SHIFT 1
#define APM_MAC_SPI2SAP_SPIEN_DIR_MASK  1
#define APM_MAC_SPI2SAP_SPIEN_DIR_RX    0 /* Rx/Ingress */
#define APM_MAC_SPI2SAP_SPIEN_DIR_TX    1 /* Tx/Egress */

#define APM_MAC_SPI2SAP_SPIEN           1 /* Start Command */

/* bit definitions for Ethernet SPI Config Register */

/*
 * Maximum number of state transitions permitted in add/del/search. after this
 * limit, add/del/search will be terminated with one fatal interrupt
 */

#define APM_MAC_SPI2SAP_CFG_MGR_MAX(x)      (((x) & 0xFFF) << 8)
#define APM_MAC_SPI2SAP_CFG_MGR_MAX_DEF     0xFFF
#define APM_MAC_SPI2SAP_CFG_MGR_MAX_MASK    0xFFF
#define APM_MAC_SPI2SAP_CFG_MGR_MAX_SHIFT   8

/* Depth of AVL search database. This value must be less than or equal to 127 */

#define APM_MAC_SPI2SAP_CFG_MGR_SERACH_DPT(x)   ((x) & 0x7F)
#define APM_MAC_SPI2SAP_CFG_MGR_SERACH_DPT_DEF  0x7F

/* bit definitions for Ethernet SPI Debug 0 Register */

#define APM_MAC_SPI2SAP_DBG0_ALVRAM_WREN    0x00000200 /* Write enable signal */
                                                       /* to AVL RAM */
#define APM_MAC_SPI2SAP_DBG0_DBGEN          0x00000100 /* Enable Debug */
#define APM_MAC_SPI2SAP_DBG0_ALVRAM_ADRS(x) ((x) & 0x7F) /* AVL tree RAM */
                                                         /* address */

/* bit definitions for Ethernet SPI Debug 1/2 Registers */

/* write data LSBs */

#define APM_MAC_SPI2SAP_DBG1_WDATA_LSB(lo) ((lo) & 0x7FFFFF)

/* write data MSBs */

#define APM_MAC_SPI2SAP_DBG2_WDATA_MSB(hi, lo) \
    ((((hi) & 0x7FFFFF) << 13) | (((lo) >> 19) & 0x1FFF))

/* write data Lower word */

#define APM_MAC_SPI2SAP_DBG_WDATA_LO(msb, lsb) \
    (((lsb) & 0x7FFFFF) | (((msb) & 0x1FFF) << 19))

/* write data Higher word */

#define APM_MAC_SPI2SAP_DBG_WDATA_HI(msb)  (((msb) >> 13) & 0x7FFFFF)

/* bit definitions for Ethernet SPI Interrupt and Interrupt Mask Registers */

#define APM_MAC_SPI2SAP_INT_FWR_SPERR   0x00000400 /* AVL tree FATAL write */
                                                   /* node points to itself */
#define APM_MAC_SPI2SAP_INT_FRD_SPERR   0x00000200 /* AVL tree FATAL read */
                                                   /* node points to itself */
#define APM_MAC_SPI2SAP_INT_FWR_BALERR  0x00000100 /* AVL tree FATAL write */
                                                   /* has incorrect balance */
                                                   /* and address */
#define APM_MAC_SPI2SAP_INT_FRD_BALERR  0x00000080 /* AVL tree FATAL read */
                                                   /* has incorrect balance */
                                                   /* and address */
#define APM_MAC_SPI2SAP_INT_FSETP_MAX   0x00000040 /* AVL tree FATAL max */
                                                   /* number of steps  */
                                                   /* exceeeded error */
#define APM_MAC_SPI2SAP_INT_FRD_RAMERR  0x00000020 /* AVL tree FATAL RAM */
                                                   /* read error */
#define APM_MAC_SPI2SAP_INT_RD_RAMERR   0x00000010 /* AVL tree RAM read error */
#define APM_MAC_SPI2SAP_INT_FULL        0x00000008 /* AVL tree full */
#define APM_MAC_SPI2SAP_INT_DUPADD      0x00000004 /* Add Duplicated SPI */
#define APM_MAC_SPI2SAP_INT_EMPTYDEL    0x00000002 /* Delete SPI from Empty */
                                                   /* AVL tree */
#define APM_MAC_SPI2SAP_INT_DEL         0x00000001 /* SPI for deletion is not */
                                                   /* found */

/* bit definitions for Ethernet SPI2SAP Status 0 Register */

#define APM_MAC_SPI2SAP_STS0_ROOTADRS(x) (((x) >> 8) & 0x7F) /* AVL tree Root */
                                                             /*  address*/
#define APM_MAC_SPI2SAP_STS0_SPICNT(x)  ((x) & 0xFF) /* Number of SPIs */
                                                     /* present in AVL tree */

/*
 * bit definitions for Ethernet SPI2SAP Status 1/2 Register
 *
 * Status 1 Register is used to save 19 LSBs, and Status 2 Register is for 32
 * MSBs.
 */

/* Lower word of AVL RAM memory */

#define APM_MAC_SPI2SAP_STS_WDATA_LO(msb, lsb) \
    (((lsb) & 0x7FFFFF) | (((msb) & 0x1FFF) << 19))

/* Higher word of AVL RAM memory */

#define APM_MAC_SPI2SAP_STS_WDATA_HI(msb)  (((msb) >> 13) & 0x7FFFFF)

/*
 * bit definitions for Ethernet SPI2SAP Status 3 Register
 *
 * Address in AVL RAM where RID is deleted/added/found.
 */

#define APM_MAC_SPI2SAP_STS3_DELADRS(x)     (((x) >> 16) & 0x7F)
#define APM_MAC_SPI2SAP_STS3_ADDADRS(x)     (((x) >> 8) & 0x7F)
#define APM_MAC_SPI2SAP_STS3_SEARCHADRS(x)  ((x) & 0x7F)
#define APM_MAC_SPI2SAP_STS3_SEARCH_FAIL    0x7F

/* bit definitions for Ethernet SPI2SAP FIFO Empty Status Register */

#define APM_MAC_SPI2SAP_FIFO_EMPTY_STS_P1   0x2 /* Port 1 */
#define APM_MAC_SPI2SAP_FIFO_EMPTY_STS_P0   0x1 /* Port 0 */

/* bit definitions for Ethernet SPI Interrupt 0 and Mask Registers */

#define APM_MAC_SPI2SAP_INT0_P1SAP_FIFO_OF   0x00020000 /* Rx Port 1 SAP FIFO */
                                                        /* Overflow*/
#define APM_MAC_SPI2SAP_INT0_P1SAP_FIFO_UF   0x00020000 /* Rx Port 1 SAP FIFO */
                                                        /* Underflow*/

#define APM_MAC_SPI2SAP_INT0_P0SAP_FIFO_OF   0x00000002 /* Rx Port 0 SAP FIFO */
                                                        /* Overflow*/
#define APM_MAC_SPI2SAP_INT0_P0SAP_FIFO_UF   0x00000002 /* Rx Port 0 SAP FIFO */
                                                        /* Underflow*/

/* bit definitions for Ethernet SAD Address/Command Register */

#define APM_MAC_ENET_SAD_CMD_REQ    0x00020000 /* Memory access request */
#define APM_MAC_ENET_SAD_CMD_RDWR   0x00010000 /* Memory Read/Write */
#define APM_MAC_ENET_SAD_ADRS(x)    ((x) & 0xFFFF) /* Memory Address */

/* bit definitions for Ethernet SAD Command Done Register */

#define APM_MAC_ENET_SAD_CMD_STAT_DONE  1 /* Memory Access Completed */

/* Ethernet Rx/Tx Buffer Sub-Block */

#define APM_MAC_RXBUF_CFG_THR       0x2500 /* Rx Buffer Threshold Config */
#define APM_MAC_RXBUF_CFG_PDEPTH    0x2504 /* Rx Buffer Port Depth Config */
#define APM_MAC_RXBUF_CFG_SADRS     0x2508 /* Rx Buffer Port Start Address */
#define APM_MAC_RXBUF_CFG_BUF       0x250C /* Rx Buffer Config */
#define APM_MAC_TXBUF_CFG_THR       0x2510 /* Tx Buffer Threshold Config */
#define APM_MAC_TXBUF_CFG_PDEPTH    0x2514 /* Tx Buffer Port Depth Config */
#define APM_MAC_TXBUF_CFG_SADRS     0x2518 /* Tx Buffer Port Start Address */
#define APM_MAC_TXBUF_CFG_BUF       0x251C /* Tx Buffer Config */
#define APM_MAC_TIMER_CFG1          0x2520 /* Timer Config 1 */
#define APM_MAC_TIMER_CFG2          0x2524 /* Timer Config 2 */
#define APM_MAC_RXTXBUF_STS         0x2528 /* Rx/Tx Buffer Status */
#define APM_MAC_RXTXBUF_CHKINT      0x252C /* Interrupt */
#define APM_MAC_RXTXBUF_CHKINT_MASK 0x2530 /* Interrupt Mask */
#define APM_MAC_RXBUF_CFG_PAUSE_THR 0x2534 /* Rx Buffer Pause Threshold */
#define APM_MAC_MEM_CFG1            0x2600 /* Memory Config */
#define APM_MAC_MEM_CFG2            0x2604
#define APM_MAC_DEBUG1              0x26FC /* Debug */
#define APM_MAC_DEBUG               0x2700
#define APM_MAC_ENET_SPARE_CFG      0x2750 /* Spare Config */

/* bit definitions for Ethernet Rx/Tx Buffer Threshold Config Registers */

#define APM_MAC_RXTXBUF_CFG_THR_FULL(x)    (((x) & 0xFFFF) << 16) /* Buffer Full */
#define APM_MAC_RXBUF_CFG_THR_FULL_DEF     0x01F4
#define APM_MAC_TXBUF_CFG_THR_FULL_DEF     0x0C00
#define APM_MAC_RXTXBUF_CFG_THR_FULL_MASK  0xFFFF
#define APM_MAC_RXTXBUF_CFG_THR_FULL_SHIFT 16

#define APM_MAC_RXTXBUF_CFG_THR_EMPTY(x)   (((x) & 0xFFFF) /* Buffer Empty */
#define APM_MAC_RXTXBUF_CFG_THR_FULL_DEF   0

/* bit definitions for Ethernet Rx/Tx Buffer Port Depth Config Registers */

#define APM_MAC_RXTXBUF_CFG_P0DEPTH(x)     (((x) & 0xFFFF) << 16) /* Port 0 */
#define APM_MAC_RXBUF_CFG_P0DEPTH_DEF      0x0200
#define APM_MAC_TXBUF_CFG_P0DEPTH_DEF      0x0C00
#define APM_MAC_RXTXBUF_CFG_P0DEPTH_MASK   0xFFFF
#define APM_MAC_RXTXBUF_CFG_P0DEPTH_SHIFT  16

#define APM_MAC_RXTXBUF_CFG_P1DEPTH(x)     (((x) & 0xFFFF) /* Port 1 */
#define APM_MAC_RXBUF_CFG_P1DEPTH_DEF      0x0200
#define APM_MAC_TXBUF_CFG_P1DEPTH_DEF      0x0C00

/* bit definitions for Ethernet Rx/Tx Buffer Port Start Address Registers */

#define APM_MAC_RXTXBUF_CFG_P0SADRS(x)     (((x) & 0xFFFF) << 16) /* Port 0 */
#define APM_MAC_RXTXBUF_CFG_P0SADRS_DEF    0
#define APM_MAC_RXTXBUF_CFG_P0SADRS_MASK   0xFFFF
#define APM_MAC_RXTXBUF_CFG_P0SADRS_SHIFT  16

#define APM_MAC_RXTXBUF_CFG_P1SADRS(x)     (((x) & 0xFFFF) /* Port 1 */
#define APM_MAC_RXBUF_CFG_P1SADRS_DEF      0
#define APM_MAC_TXBUF_CFG_P1SADRS_DEF      0x0C00

/* bit definitions for Ethernet Rx/Tx Buffer Config Registers */

#define APM_MAC_RXTXBUF_CFG_P0BUF(x)       (((x) & 0xFFFF) << 16) /* Port 0 */
#define APM_MAC_RXTXBUF_CFG_P0BUF_DEF      0
#define APM_MAC_RXTXBUF_CFG_P0BUF_MASK     0xFFFF
#define APM_MAC_RXTXBUF_CFG_P0BUF_SHIFT    16

#define APM_MAC_RXTXBUF_CFG_P1BUF(x)     (((x) & 0xFFFF) /* Port 1 */
#define APM_MAC_RXBUF_CFG_P1BUF_DEF      0
#define APM_MAC_TXBUF_CFG_P1BUF_DEF      0xA

/* bit definitions for Ethernet Timer Config 1 Register */

/*
 * Number of microsecond steps that microsecond counter should be incremented
 * every 1 Microsecondticks for one Millisecond tick
 */

#define APM_MAC_TIMER_CFG1_USINC(x)     (((x) & 0x3FF) << 20)
#define APM_MAC_TIMER_CFG1_USINC_DEF    4
#define APM_MAC_TIMER_CFG1_USINC_MASK   0x3FF
#define APM_MAC_TIMER_CFG1_USINC_SHIFT  20

/* Number of Microsecond ticks for one Millisecond tick */

#define APM_MAC_TIMER_CFG1_USLMT(x)     (((x) & 0x3FF) << 12)
#define APM_MAC_TIMER_CFG1_USLMT_DEF    1000
#define APM_MAC_TIMER_CFG1_USLMT_MASK   0x3FF
#define APM_MAC_TIMER_CFG1_USLMT_SHIFT  12

/* Number of Millisecond ticks for one second tick */

#define APM_MAC_TIMER_CFG1_MSLMT(x)     ((x) & 0x3FF)
#define APM_MAC_TIMER_CFG1_MSLMT_DEF    1000

/*
 * bit definitions for Ethernet Timer Config 2 Register
 *
 * For clock frequencies that do give us integer multipliers to get 100ns, we
 * should be set to identical value CfgMCnt=CfgNCcnt= 1.
 *
 * For clock frequencies that do not give us integer multipliers to get 100ns:
 * Avg = (T_low*CfgMCnt + T_high*CfgNCnt)/(CfgMCnt+CfgNCnt), where T_low and
 * T_high are integral multiples of clock period just less than and just greater
 * than 100ns.
 */

/* 100ns count limitation */

#define APM_MAC_TIMER_CFG2_100NSCNT_LMT(x)      (((x) & 0xFF) << 16)
#define APM_MAC_TIMER_CFG2_100NSCNT_LMT_DEF     15
#define APM_MAC_TIMER_CFG2_100NSCNT_LMT_MASK    0xFF
#define APM_MAC_TIMER_CFG2_100NSCNT_LMT_SHIFT   16

/* CfgMCnt */

#define APM_MAC_TIMER_CFG2_MCNT(x)      (((x) & 0xFF) << 8)
#define APM_MAC_TIMER_CFG2_MCNT_DEF     1
#define APM_MAC_TIMER_CFG2_MCNT_MASK    0xFF
#define APM_MAC_TIMER_CFG2_MCNT_SHIFT   8

/* CfgNCnt */

#define APM_MAC_TIMER_CFG2_NCNT(x)      ((x) & 0xFF)
#define APM_MAC_TIMER_CFG2_NCNT_DEF     1

/* bit definitions for Ethernet Rx/Tx Buffer Status Register */

#define APM_MAC_RXTXBUF_STS_P1TXF_EMPTY 0x20000 /* Port 1 TxBuff FIFO Empty */
#define APM_MAC_RXTXBUF_STS_P1RXF_EMPTY 0x10000 /* Port 1 RxBuff FIFO Empty */
#define APM_MAC_RXTXBUF_STS_P0TXF_EMPTY 0x00002 /* Port 0 TxBuff FIFO Empty */
#define APM_MAC_RXTXBUF_STS_P0RXF_EMPTY 0x00001 /* Port 0 RxBuff FIFO Empty */

/* bit definitions for Ethernet Rx/Tx Buffer Interrupt and Mask Registers */

#define APM_MAC_RXTXBUF_CHKINT_P1(x)    (((x) >> 16) & 0x7F)
#define APM_MAC_RXTXBUF_CHKINT_P0(x)    ((x) & 0x7F)

#define APM_MAC_RXTXBUF_CHKINT_RX_PAUSE 0x40 /* Rx Pause */
#define APM_MAC_RXTXBUF_CHKINT_RX_CHK   0x20 /* Rx Chksum Error */
#define APM_MAC_RXTXBUF_CHKINT_TX_CHK   0x10 /* Tx Chksum Error */
#define APM_MAC_RXTXBUF_CHKINT_RXB_FOF  0x08 /* RxBuff FIFO Overflow */
#define APM_MAC_RXTXBUF_CHKINT_RXB_FUF  0x04 /* RxBuff FIFO Underflow */
#define APM_MAC_RXTXBUF_CHKINT_TXB_FOF  0x02 /* TxBuff FIFO Overflow */
#define APM_MAC_RXTXBUF_CHKINT_TXB_FUF  0x01 /* TxBuff FIFO Underflow */

/* bit definitions for Ethernet Rx Buffer Pause Threshold Register */

#define APM_MAC_RXBUF_CFG_PAUSE_P1THR(x)    (((x) & 0xFFFF) << 16) /* Port 1 */
#define APM_MAC_RXBUF_CFG_PAUSE_P1THR_DEF   0x01F4
#define APM_MAC_RXBUF_CFG_PAUSE_P1THR_MASK  0xFFFF
#define APM_MAC_RXBUF_CFG_PAUSE_P1THR_SHIFT 16

#define APM_MAC_RXBUF_CFG_PAUSE_P0THR(x)    ((x) & 0xFFFF) /* Port 0 */
#define APM_MAC_RXBUF_CFG_PAUSE_P0THR_DEF   0x01F4

/* bit definitions for Ethernet Spare Config Register */

#define APM_MAC_ENET_SPARE_CFG_P1_DISECM(x)     (((x) & 3) << 16) /* Disable */
#define APM_MAC_ENET_SPARE_CFG_P1_DISECM_DEF_1G 0 /* ECM fixes for port 1 */
#define APM_MAC_ENET_SPARE_CFG_P1_DISECM_DEF    (3 << 16) /* for 100M/10M */
#define APM_MAC_ENET_SPARE_CFG_P1_DISECM_MASK   3
#define APM_MAC_ENET_SPARE_CFG_P1_DISECM_SHIFT  16

#define APM_MAC_ENET_SPARE_CFG_P0_DISECM(x)     (((x) & 3) << 14) /* Disable */
#define APM_MAC_ENET_SPARE_CFG_P0_DISECM_DEF_1G 0 /* ECM fixes for port 0 */
#define APM_MAC_ENET_SPARE_CFG_P0_DISECM_DEF    (3 << 14) /* for 100M/10M */
#define APM_MAC_ENET_SPARE_CFG_P0_DISECM_MASK   3
#define APM_MAC_ENET_SPARE_CFG_P0_DISECM_SHIFT  16

#define APM_MAC_ENET_SPARE_CFG_P1_DISRFIX   0x2000 /* Disable Runt Fix for */
                                                   /* Port 1 */
#define APM_MAC_ENET_SPARE_CFG_P0_DISRFIX   0x1000 /* Disable Runt Fix for */
                                                   /* Port 0 */

#define APM_MAC_ENET_SPARE_CFG_P1_HDREN     0x0800 /* Proprietary header */
                                                   /* enable for port 1 */
#define APM_MAC_ENET_SPARE_CFG_P0_HDREN     0x0400 /* Proprietary header */
                                                   /* enable for port 0 */

/*
 * Time to wait (in HBF CLKS) after ENET signals Idle before signalling IDLE to
 * MPA
 */

#define APM_MAC_ENET_SPARE_CFG_IDLE_TIMER(x)    (((x) & 0x3FF)
#define APM_MAC_ENET_SPARE_CFG_IDLE_TIMER_DEF   0x40

/* bit definitions for Debug Registers */

#define APM_MAC_DEBUG_UNISEC_TX_BP  0x4 /* Unisec Bypass Tx mode */
#define APM_MAC_DEBUG_UNISEC_RX_BP  0x2 /* Unisec Bypass Rx mode */

/* MAC Block */

/* RGMII sub-block */

#define APM_MAC_RGMII0_CFG 0x2800   /* RGMII 0 Config */
#define APM_MAC_RGMII1_CFG 0x2804   /* RGMII 1 Config */

/* bit definitions for Ethernet RGMII 0/1 Config Registers */

#define APM_MAC_RGMII_CFG_TCLK_MUXSEL(x)   (((x) & 7) << 29) /* RGMII TxClk */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_1G   4 /* Mux Selection */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL0 0 /* miitx_clk_d3 tap (no delay) */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL1 1 /* miitx_clk tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL2 2 /* miitx_clk_d1 tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL3 3 /* miitx_clk_d2 tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL4 4 /* miitx_clk_d7 tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL5 5 /* miitx_clk_d4 tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL6 6 /* miitx_clk_d5 tap */
#define APM_MAC_RGMII_CFG_TCLK_MUXSEL_VAL7 7 /* miitx_clk_d6 tap */

#define APM_MAC_RGMII_CFG_RCLK_MUXSEL(x)   (((x) & 7) << 26) /* RGMII RxClk */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_DEF  0 /* Mux Selection */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL0 0 /* miirx_clk_d3 tap (no delay) */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL1 1 /* miirx_clk tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL2 2 /* miirx_clk_d1 tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL3 3 /* miirx_clk_d2 tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL4 4 /* miirx_clk_d7 tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL5 5 /* miirx_clk_d4 tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL6 6 /* miirx_clk_d5 tap */
#define APM_MAC_RGMII_CFG_RCLK_MUXSEL_VAL7 7 /* miirx_clk_d6 tap */

#define APM_MAC_RGMII_CFG_LP            0x02000000 /* RGMII Loopback Mode */
#define APM_MAC_RGMII_CFG_SPD_1G        0x01000000 /* Interface Line Speed */
                                                   /* 1 - 1G(default) */
                                                   /* 0 - 10M/100M */
#define APM_MAC_RGMII_CFG_FLIPNIB_TX    0x00800000 /* Flip nibble order to */
                                                   /* send byte */
#define APM_MAC_RGMII_CFG_FLIPNIB_RX    0x00400000 /* Flip nibble order to */
                                                   /* make byte */

/* Header Parser sub-block */

#define APM_MAC_HDRPRS_P0CFG0   0x2A00 /* Port 0 Config */
#define APM_MAC_HDRPRS_P0CFG1   0x2A04
#define APM_MAC_HDRPRS_P0CFG2   0x2A08
#define APM_MAC_HDRPRS_P1CFG0   0x2A0C /* Port 1 Config */
#define APM_MAC_HDRPRS_P1CFG1   0x2A10
#define APM_MAC_HDRPRS_P1CFG2   0x2A14
#define APM_MAC_HDRPRS_P0CFG3   0x2A18 /* Port 0 Config 3 */
#define APM_MAC_HDRPRS_P1CFG3   0x2A1C /* Port 1 Config 3 */

/* bit definitions for Header Parser Port 0/1 Config 0 Registers */

#define APM_MAC_HDRPRS_CFG0_VLAN(x)     (((x) & 0xFF) << 24) /* number of */
#define APM_MAC_HDRPRS_CFG0_VLAN_MASK   0xFF /* VLAN header bytes to skip */
#define APM_MAC_HDRPRS_CFG0_VLAN_SHIFT  24

#define APM_MAC_HDRPRS_CFG0_VLAN_NESTED     0x00000200 /* Nested VLAN Present */

/* Proprietary Header Bytes Skip */

#define APM_MAC_HDRPRS_CFG0_SKIP_PROG_VLD  0x00000100 /* Header present */
#define APM_MAC_HDRPRS_CFG0_SKIP_PROG(x)   ((x) & 0xFF) /* header bytes */

/* bit definitions for Header Parser Port 0/1 Config 1 Registers */

/* Programmable number of bytes to skip to reach next header */

#define APM_MAC_HDRPRS_CFG1_SKIP_PROG(x)    (((x) & 0xFF) << 24)
#define APM_MAC_HDRPRS_CFG1_SKIP_PROG_MASK  0xFF
#define APM_MAC_HDRPRS_CFG1_SKIP_PROG_SHIFT 24

/* number of bytes of programmable Ether type to skip  */

#define APM_MAC_HDRPRS_CFG1_PROG_ETH_TYPE(x)    (((x) & 0xFF) << 16)
#define APM_MAC_HDRPRS_CFG1_PROG_ETH_TYPE_MASK  0xFF
#define APM_MAC_HDRPRS_CFG1_PROG_ETH_TYPE_SHIFT 24

/*  Programmable Ether type to find in ingress packet   */

#define APM_MAC_HDRPRS_CFG1_PROG_ETH_TYPE_VAL(x)    (((x) & 0xFFFF)
#define APM_MAC_HDRPRS_CFG1_PROG_ETH_TYPE_VAL_DEF   0x1111

/* bit definitions for Header Parser Port 0/1 Config 2 Registers */

#define APM_MAC_HDRPRS_CFG2_MPLS(x)     (((x) & 0xFF) << 24) /* number of */
#define APM_MAC_HDRPRS_CFG2_MPLS_MASK   0xFF /* MPLS header bytes to skip */
#define APM_MAC_HDRPRS_CFG2_MPLS_SHIFT  24

#define APM_MAC_HDRPRS_CFG2_SPI(x)      (((x) & 0xFF) << 16) /* number of */
#define APM_MAC_HDRPRS_CFG2_SPI_MASK    0xFF /* header bytes to skip to */
#define APM_MAC_HDRPRS_CFG2_SPI_SHIFT   16 /* reach SPI */

/*
 * Specify IPv4 header length if set, otherwise extract IPv4 header length from
 * the packet
 */

#define APM_MAC_HDRPRS_CFG2_IPV4_LEN_EN     0x8000
#define APM_MAC_HDRPRS_CFG2_IPV4_LEN(x)     (((x) & 0xFF) << 8) /* IPv4 */
#define APM_MAC_HDRPRS_CFG2_IPV4_LEN_DEF    20
#define APM_MAC_HDRPRS_CFG2_IPV4_LEN_MASK   0xFF /* header length */
#define APM_MAC_HDRPRS_CFG2_IPV4_LEN_SHIFT  8

#define APM_MAC_HDRPRS_CFG2_IPV6_LEN(x)     ((x) & 0xFF) /* number of IPv6 header */
#define APM_MAC_HDRPRS_CFG2_IPV6_LEN_DEF    40 /* bytes to skip  */

/* bit definitions for Header Parser Port 0/1 Config 3 Registers */

#define APM_MAC_HDRPRS_CFG3_TIMROUT_EN  0x100 /* Enable timeout feature */
                                              /* MUST be Set */
/*
 * max header size that the Header Parser needs to parse after which it will
 * timeout if cfg_prs_en_timeout is set. It will then indicate
 * UNKNOWN_ETHER_TYPE in its result.
 *
 * This filed MUST be set to 0x80.
 */

#define APM_MAC_HDRPRS_CFG3_MAX_HDR_SIZE(x)  ((x) & 0xFF) /* max header size */
#define APM_MAC_HDRPRS_CFG3_MAX_HDR_SIZE_DEF 0x80

/* Egress Channel Management and sub-block */

#define APM_MAC_CSR_P0ECM_CFG   0x2A20 /* Config */
#define APM_MAC_CSR_P1ECM_CFG   0x2A24
#define APM_MAC_CSR_P0ECM_SWPF  0x2A28 /* Software Pause Frame */
#define APM_MAC_CSR_P1ECM_SWPF  0x2A2C
#define APM_MAC_CSR_P0MULTI_DPF 0x2A30 /* Hardware Pause Frame */
#define APM_MAC_CSR_P1MULTI_DPF 0x2A34
#define APM_MAC_CSR_P0ECM_RSH   0x2A38 /* Rate Shaper */
#define APM_MAC_CSR_P1ECM_RSH   0x2A3C
#define APM_MAC_ECM_P0CFG       0x2D00 /* Config */
#define APM_MAC_ECM_P1CFG       0x2D04

/* bit definitions for ECM Port 0/1 Config Registers */

#define APM_MAC_CSR_ECM_CFG_RSHEN           0x20000000 /* Enable Rate shaper */

/*
 *Enable Automatic New pause generation (Software or Hardware Req) after Pause
 * timer expiration
 */

#define APM_MAC_CSR_ECM_CFG_MULTDPF_AUTO    0x10000000

/* Software request to generate the pause frame */

#define APM_MAC_CSR_ECM_CFG_SWPF_REQ        0x08000000

/* bit definitions for ECM Port 0/1 Software Pause Frame Registers */

#define APM_MAC_CSR_ECM_SWPF_PARAM(x)    (((x) & 0xFFFF) << 16) /* Param */
#define APM_MAC_CSR_ECM_SWPF_PARAM_MASK  0xFFFF
#define APM_MAC_CSR_ECM_SWPF_PARAM_SHIFT 16
#define APM_MAC_CSR_ECM_SWPF_EXTPARAM(x) ((x) & 0xFFFF) /* Extended Param */

/* bit definitions for ECM Port 0/1 Hardware Pause Frame Registers */

#define APM_MAC_CSR_MULTI_DPF_PARAM(x)    (((x) & 0xFFFF) << 16) /* Param */
#define APM_MAC_CSR_MULTI_DPF_PARAM_MASK  0xFFFF
#define APM_MAC_CSR_MULTI_DPF_PARAM_SHIFt 16
#define APM_MAC_CSR_MULTI_DPF_EXTPARAM(x) ((x) & 0xFFFF) /* Extended Param */

/* bit definitions for ECM Port 0/1 Rate shaper Registers */

#define APM_MAC_CSR_ECM_RSH_CRD(x)      (((x) & 0xFFFF) << 16) /* Credit */
#define APM_MAC_CSR_ECM_RSH_CRD_MASK    0xFFFF
#define APM_MAC_CSR_ECM_RSH_CRD_SHIFT   16
#define APM_MAC_CSR_ECM_RSH_PROD(x)     ((x) & 0xFFFF) /* Period */

/* bit definitions for ECM Port 0/1 Config Registers */

/*
 * set to 0 if Enet device clock frequency is > 2x MAC clk and Speed is not 1G,
 * else set to 1
 */

#define APM_MAC_ECM_CFG_ENET_CLK_FREQ_SEL   0x00100000

#define APM_MAC_ECM_CFG_RDFIFO_FULL_THR(x)      (((x) & 0x3FF) << 10) /* ECM */
#define APM_MAC_ECM_CFG_RDFIFO_FULL_THR_DEF     0 /* Read FIFO Full threshold */
#define APM_MAC_ECM_CFG_RDFIFO_FULL_THR_MASK    0x3FF /* for Data FIFO */
#define APM_MAC_ECM_CFG_RDFIFO_FULL_THR_SHIFT   10

#define APM_MAC_ECM_CFG_WRFIFO_FULL_THR(x)      ((x) & 0x3FF) /* ECM Write */
#define APM_MAC_ECM_CFG_WRFIFO_FULL_THR_DEF     0x32 /* FIFO Full threshold */
                                                     /* for Data FIFO */

/* Ingress Channel Management sub-block */

#define APM_MAC_ICM_P0CFG0          0x2C00
#define APM_MAC_ICM_P0CFG1          0x2C04
#define APM_MAC_ICM_P1CFG0          0x2C08
#define APM_MAC_ICM_P1CFG1          0x2C0C
#define APM_MAC_ICM_P0CFG2          0x2C10
#define APM_MAC_ICM_P1CFG2          0x2C14

/* bit definitions for ICM Port 0/1 Config 0 Registers */

#define APM_MAC_ICM_CFG0_MACMODE(x)     (((x) & 3) << 18) /* MAC Mode */
#define APM_MAC_ICM_CFG0_MACMODE_1G     (2 << 18) /* 2 : 1000Mbps */
#define APM_MAC_ICM_CFG0_MACMODE_100M   (1 << 18) /* 1 : 100Mbps */
#define APM_MAC_ICM_CFG0_MACMODE_10M    (0 << 18) /* 0 : 10Mbps */
#define APM_MAC_ICM_CFG0_MACMODE_MASK   3
#define APM_MAC_ICM_CFG0_MACMODE_SHIFT  18

/*
 * ICM FIFO Almost Full threshold value above which the ICM FIFO starts writing
 * the next incoming packet with SOF again.
 */

#define APM_MAC_ICM_CFG0_ALMST_FULL_THR(x)      (((x) & 0x1FF) << 9)
#define APM_MAC_ICM_CFG0_ALMST_FULL_THR_DEF     (0x28  << 9)
#define APM_MAC_ICM_CFG0_ALMST_FULL_THR_MASK    0x1FF
#define APM_MAC_ICM_CFG0_ALMST_FULL_THR_SHIFT   9

/*
 * ICM FIFO Full threshold value above which FIFO Overflows and terminates the
 * current packet with Overflow Drop Indication.
 */

#define APM_MAC_ICM_CFG0_FULL_THR(x)    ((x) & 0x1FF)
#define APM_MAC_ICM_CFG0_FULL_THR_DEF   0x3F

/* bit definitions for ICM Port 0/1 Config 1 Registers */

/* ICM Read FIFO Full threshold for Control FIFO */

#define APM_MAC_ICM_CFG1_RD_CTRLFIFO_FULL_THR(x)    (((x) & 0x3F) << 24)
#define APM_MAC_ICM_CFG1_RD_CTRLFIFO_FULL_THR_DEF   2
#define APM_MAC_ICM_CFG1_RD_CTRLFIFO_FULL_THR_MASK  0x3F
#define APM_MAC_ICM_CFG1_RD_CTRLFIFO_FULL_THR_SHIF  24

/* ICM Write FIFO Full threshold for Control FIFO */

#define APM_MAC_ICM_CFG1_WR_CTRLFIFO_FULL_THR(x)    (((x) & 0x3F) << 18)
#define APM_MAC_ICM_CFG1_WR_CTRLFIFO_FULL_THR_DEF   4
#define APM_MAC_ICM_CFG1_WR_CTRLFIFO_FULL_THR_MASK  0x3F
#define APM_MAC_ICM_CFG1_WR_CTRLFIFO_FULL_THR_SHIF  18

/* ICM Read FIFO Full threshold for Data FIFO */

#define APM_MAC_ICM_CFG1_RDFIFO_FULL_THR(x)     (((x) & 0x1FF) << 9)
#define APM_MAC_ICM_CFG1_RDFIFO_FULL_THR_DEF    0x19
#define APM_MAC_ICM_CFG1_RDFIFO_FULL_THR_MASK   0x1FF
#define APM_MAC_ICM_CFG1_RDFIFO_FULL_THR_SHIFT  9

/* ICM Write FIFO Full threshold for Data FIFO */

#define APM_MAC_ICM_CFG1_WRFIFO_FULL_THR(x)     ((x) & 0x1FF)
#define APM_MAC_ICM_CFG1_WRFIFO_FULL_THR_DEF    0

/* bit definitions for ICM Port 0/1 Config 2 Registers */

#define APM_MAC_ICM_CFG2_WAIT_ASYNCRD_EN    0x00010000 /* Async FIFO Read */
                                                       /* Wait Enable  */
#define APM_MAC_ICM_CFG2_WAIT_ASYNCRD(x)    ((x) & 0xFFFF) /* Async FIFO Read */
#define APM_MAC_ICM_CFG2_WAIT_ASYNCRD_1G    15 /* Wait counter */
#define APM_MAC_ICM_CFG2_WAIT_ASYNCRD_100M  0x50
#define APM_MAC_ICM_CFG2_WAIT_ASYNCRD_10M   0x1F4

/* Common */

#define APM_MAC_ICM_ECM_DROP_P0CNT  0x2D08 /* Port 0 ICM/ECM Drop Counter */
#define APM_MAC_ICM_ECM_DROP_P1CNT  0x2D0C /* Port 1 ICM/ECM Drop Counter */
#define APM_MAC_FIFO_STS            0x2D10 /* FIFO Status */
#define APM_MAC_INT0                0x2D14 /* Interrupt */
#define APM_MAC_INT0_MASK           0x2D18
#define APM_MAC_INT1                0x2D1C
#define APM_MAC_INT1_MASK           0x2D20

/* bit definitions for Port 0/1 ICM/ECM Drop Counter Registers */

/* number of packets dropped due to ICM overrun */

#define APM_MAC_ICM_ECM_DROP_CNT_ICM(x)     (((x) >> 16) & 0xFFFF)

/* number of packets dropped due to ECM underrun */

#define APM_MAC_ICM_ECM_DROP_CNT_ECM(x)     ((x) & 0xFFFF)

/* bit definitions for MAC FIFO Status Register */

/* Port 1 */

#define APM_MAC_FIFO_STS_P1ICM_DFIFO_EMPTY  0x00080000 /* Rx ICM Data Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P1ICM_CFIFO_EMPTY  0x00040000 /* Rx ICM Control Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P1ECM_DFIFO_EMPTY  0x00020000 /* Tx ECM Data Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P1MAC_IFFIFO_EMPTY 0x00010000 /* Rx MAC i/f Fifo */
                                                       /* Empty*/

/* Port 0 */

#define APM_MAC_FIFO_STS_P0ICM_DFIFO_EMPTY  0x00000008 /* Rx ICM Data Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P0ICM_CFIFO_EMPTY  0x00000004 /* Rx ICM Control Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P0ECM_DFIFO_EMPTY  0x00000002 /* Tx ECM Data Fifo */
                                                       /* Empty*/
#define APM_MAC_FIFO_STS_P0MAC_IFFIFO_EMPTY 0x00000001 /* Rx MAC i/f Fifo */
                                                       /* Empty*/

/* bit definitions for MAC Interrupt and Interrupt Mask Register */

/* Port 1 */

#define APM_MAC_INT0_P1ICM_DFIFO_UF  0x01000000 /* Rx ICM Data FIFO Underflow */
#define APM_MAC_INT0_P1ICM_DFIFO_OF  0x00800000 /* Rx ICM Data FIFO Overflow */
#define APM_MAC_INT0_P1ICM_CFIFO_UF  0x00400000 /* Rx ICM Ctrl FIFO Underflow */
#define APM_MAC_INT0_P1ICM_CFIFO_OF  0x00200000 /* Rx ICM Ctrl FIFO Overflow */
#define APM_MAC_INT0_P1ECM_DFIFO_UR  0x00100000 /* Rx ECM Data FIFO Underrun */
#define APM_MAC_INT0_P1ECM_DFIFO_UF  0x00080000 /* Tx ECM Data FIFO Underflow */
#define APM_MAC_INT0_P1ECM_DFIFO_OF  0x00040000 /* Tx ECM Data FIFO Overflow */
#define APM_MAC_INT0_P1MAC_IFFIFO_UF 0x00020000 /* Tx MAC i/f FIFO Underflow */
#define APM_MAC_INT0_P1MAC_IFFIFO_OF 0x00010000 /* Tx MAC i/f FIFO Overflow */

/* Port 0 */

#define APM_MAC_INT0_P0ICM_DFIFO_UF  0x01000000 /* Rx ICM Data FIFO Underflow */
#define APM_MAC_INT0_P0ICM_DFIFO_OF  0x00800000 /* Rx ICM Data FIFO Overflow */
#define APM_MAC_INT0_P0ICM_CFIFO_UF  0x00400000 /* Rx ICM Ctrl FIFO Underflow */
#define APM_MAC_INT0_P0ICM_CFIFO_OF  0x00200000 /* Rx ICM Ctrl FIFO Overflow */
#define APM_MAC_INT0_P0ECM_DFIFO_UR  0x00100000 /* Rx ECM Data FIFO Underrun */
#define APM_MAC_INT0_P0ECM_DFIFO_UF  0x00080000 /* Tx ECM Data FIFO Underflow */
#define APM_MAC_INT0_P0ECM_DFIFO_OF  0x00040000 /* Tx ECM Data FIFO Overflow */
#define APM_MAC_INT0_P0MAC_IFFIFO_UF 0x00020000 /* Tx MAC i/f FIFO Underflow */
#define APM_MAC_INT0_P0MAC_IFFIFO_OF 0x00010000 /* Tx MAC i/f FIFO Overflow */

/* bit definitions for MAC Interrupt 1 and Interrupt 1 Mask Registers */

/* Port 1 */

#define APM_MAC_INT1_P1_LD      0x8 /* Link Down */
#define APM_MAC_INT1_P1_CARRY   0x4 /* Carry for Status Register Overflow */

/* Port 0 */

#define APM_MAC_INT1_P0_LD      0x2 /* Link Down */
#define APM_MAC_INT1_P0_CARRY   0x1 /* Carry for Status Register Overflow */

/* QMI Slave Block */

#define APM_MAC_QMI_CFG0            0x7800 /* Config 0 */
#define APM_MAC_QMI_CFG1            0x7804 /* Config 1 */
#define APM_MAC_QMI_CFG_FPBUF       0x7808 /* FP Buffer Config */
#define APM_MAC_QMI_CFG_WQBUF       0x780C /* WQ Buffer Config */
#define APM_MAC_QMI_STS_FPBUF0      0x7810 /* FP Buffer Status */
#define APM_MAC_QMI_STS_FPBUF1      0x7814
#define APM_MAC_QMI_STS_FPBUF2      0x7818
#define APM_MAC_QMI_STS_WQBUF0      0x781C /* WQ Buffer Status */
#define APM_MAC_QMI_STS_WQBUF1      0x7820
#define APM_MAC_QMI_STS_WQBUF2      0x7824
#define APM_MAC_QMI_STS_WQBUF3      0x7828
#define APM_MAC_QMI_CFG_SAB         0x782C /* SAB Enable */
#define APM_MAC_QMI_CFG_SAB0        0x7830 /* QID monitered for SAB */
#define APM_MAC_QMI_CFG_SAB1        0x7834
#define APM_MAC_QMI_CFG_SAB2        0x7838
#define APM_MAC_QMI_CFG_SAB3        0x783C
#define APM_MAC_QMI_CFG_SAB4        0x7840
#define APM_MAC_QMI_STS_INT0        0x7844 /* FP PB Overflow Interrupt */
#define APM_MAC_QMI_STS_INT0MASK    0x7848
#define APM_MAC_QMI_STS_INT1        0x784C /* WQ PB Overflow Interrupt */
#define APM_MAC_QMI_STS_INT1MASK    0x7850
#define APM_MAC_QMI_STS_INT2        0x7854 /* FP PB Underrun Interrupt */
#define APM_MAC_QMI_STS_INT2MASK    0x7858
#define APM_MAC_QMI_STS_INT3        0x785C /* WQ PB Underrun Interrupt */
#define APM_MAC_QMI_STS_INT3MASK    0x7860
#define APM_MAC_QMI_STS_INT4        0x7864 /* HBF Error Interrupt */
#define APM_MAC_QMI_STS_INT4MASK    0x7868
#define APM_MAC_QMI_CFG_DBGCTRL     0x786C /* Debug Control */
#define APM_MAC_QMI_CFG_DBGWDATA0   0x7870 /* Write/Push Data */
#define APM_MAC_QMI_CFG_DBGWDATA1   0x7874
#define APM_MAC_QMI_CFG_DBGWDATA2   0x7878
#define APM_MAC_QMI_CFG_DBGWDATA3   0x787C
#define APM_MAC_QMI_STS_DBGRDATA    0x7880 /* Read/Pop Data */
#define APM_MAC_QMI_CFG_QML_DEVADRS 0x7884 /* QM Lite Device Address */
#define APM_MAC_QMI_CFG_FPASSOC     0x7888 /* FP PB Association */
#define APM_MAC_QMI_CFG_WQASSOC     0x788C /* WQ PB Association */
#define APM_MAC_QMI_CFG_WQ16MEM     0x7890 /* WQ 16 Memory */
#define APM_MAC_QMI_STS_FIFO        0x7894 /* FIFO Empty Status */

/* bit definitions for QMI Config 0 Register */

/* Normally used by SLIMpro(MPA) */

#define APM_MAC_QMI_CFG0_WQ16_PBDIS     0x20000000 /* Disable PB for WQ 16 */
#define APM_MAC_QMI_CFG0_WQ16_QUERST    0x10000000 /* Reset queue logic for */
                                                   /* PB of WQ 16 */
#define APM_MAC_QMI_CFG0_FP16_PBDIS     0x08000000 /* Disable PB for FP 16 */
#define APM_MAC_QMI_CFG0_FP17_PBDIS     0x04000000 /* Disable PB for FP 16 */
#define APM_MAC_QMI_CFG0_FP16_QUERST    0x02000000 /* Reset queue logic for */
                                                   /* PB of FP 16 */
#define APM_MAC_QMI_CFG0_FP17_QIERST    0x01000000 /* Reset queue logic for */
                                                   /* PB of FP 17 */

/*
 * For FreePool, a decrement message is sent when the difference between sent
 * and accumulated is greater than the threshold.
 */

#define APM_MAC_QMI_CFG0_FP_DECDIFF_THR(x)      (((x) & 7) << 21)
#define APM_MAC_QMI_CFG0_FP_DECDIFF_THR_DEF     0
#define APM_MAC_QMI_CFG0_FP_DECDIFF_THR_MASK    7
#define APM_MAC_QMI_CFG0_FP_DECDIFF_THR_SHIFT   21

/*
 * For Work Queue, a decrement message is sent when the difference between sent
 * and accumulated is greater than the threshold.
 */

#define APM_MAC_QMI_CFG0_WQ_DECDIFF_THR(x)      (((x) & 0xF) << 17)
#define APM_MAC_QMI_CFG0_WQ_DECDIFF_THR_DEF     1
#define APM_MAC_QMI_CFG0_WQ_DECDIFF_THR_MASK    0xF
#define APM_MAC_QMI_CFG0_WQ_DECDIFF_THR_SHIFT   17

/*
 * qmi_ss_dafull is asserted if the buffer contains more than the 128-bit
 * entries of the threshold.
 */

#define APM_MAC_QMI_CFG0_DEALLOC_THR(x)         (((x) & 7) << 14)
#define APM_MAC_QMI_CFG0_DEALLOC_THR_DEF        3
#define APM_MAC_QMI_CFG0_DEALLOC_THR_MASK       7
#define APM_MAC_QMI_CFG0_DEALLOC_THR_SHIFT      14

/*
 * FreePool Queue Decrement Threshold. A decrement message to QM is created when
 * the buffer has the vacant entries (16B) of the threshold.
 */

#define APM_MAC_QMI_CFG0_FPDEC_THR(x)           (((x) & 7) << 11)
#define APM_MAC_QMI_CFG0_FPDEC_THR_DEF          1
#define APM_MAC_QMI_CFG0_FPDEC_THR_MASK         7
#define APM_MAC_QMI_CFG0_FPDEC_THR_SHIFT        11

/*
 * qmi_ss_fpbavl is asserted if the buffer contains more than the 128-bit
 * entries of the threshold.
 */

#define APM_MAC_QMI_CFG0_FPBAVL_THR(x)          (((x) & 7) << 8)
#define APM_MAC_QMI_CFG0_FPBAVL_THR_DEF         0
#define APM_MAC_QMI_CFG0_FPBAVL_THR_MASK        7
#define APM_MAC_QMI_CFG0_FPBAVL_THR_SHIFT       8

/*
 * Work Queue Decrement Threshold. A decrement message to QM is created when the
 * buffer has the vacant entries(32B) of the threshold.
 */

#define APM_MAC_QMI_CFG0_WQDEC_THR(x)           (((x) & 0xF) << 4)
#define APM_MAC_QMI_CFG0_WQDEC_THR_DEF          2
#define APM_MAC_QMI_CFG0_WQDEC_THR_MASK         0xF
#define APM_MAC_QMI_CFG0_WQDEC_THR_SHIFT        4

/*
 * qmi_ss_wqbavl is asserted if the buffer contains more than the 128-bit
 * entries of the threshold.
 */

#define APM_MAC_QMI_CFG0_WQBAVL_THR(x)          ((x) & 0xF)
#define APM_MAC_QMI_CFG0_WEBAVL_THR_DEF         1

/* bit definitions for QMI Config 1 Register */

/*
 * Setting it to 1 will  the TotDataLenthLinkList and NxtLinkListLength fields
 * of Completion Message.
 */

#define APM_MAC_QMI_CFG1_CM_LLOVRD              0x20000000

/* Completion Message Control Buffer Full Threshold */

#define APM_MAC_QMI_CFG1_CM_CTRLBUF_THR(x)      (((x) & 7) << 26)
#define APM_MAC_QMI_CFG1_CM_CTRLBUF_THR_DEF     3
#define APM_MAC_QMI_CFG1_CM_CTRLBUF_THR_MASK    7
#define APM_MAC_QMI_CFG1_CM_CTRLBUF_THR_SHIFT   26

/* Completion Message Data Buffer Full Threshold */

#define APM_MAC_QMI_CFG1_CM_DATABUF_THR(x)      (((x) & 0x1F) << 21)
#define APM_MAC_QMI_CFG1_CM_DATABUF_THR_DEF     0xF
#define APM_MAC_QMI_CFG1_CM_DATABUF_THR_MASK    0x1F
#define APM_MAC_QMI_CFG1_CM_DATABUF_THR_SHIFT   21

/* Completion Message Message FIFO Full Threshold */

#define APM_MAC_QMI_CFG1_CM_MSG_THR(x)          (((x) & 0xF) << 17)
#define APM_MAC_QMI_CFG1_CM_MSG_THR_DEF         4
#define APM_MAC_QMI_CFG1_CM_MSG_THR_MASK        0xF
#define APM_MAC_QMI_CFG1_CM_MSG_THR_SHIFT       17

/* Completion Message Register Full Threshold */

#define APM_MAC_QMI_CFG1_CM_REG_THR(x)          (((x) & 7) << 14)
#define APM_MAC_QMI_CFG1_CM_REG_THR_DEF         2
#define APM_MAC_QMI_CFG1_CM_REG_THR_MASK        7
#define APM_MAC_QMI_CFG1_CM_REG_THR_SHIFT       14

/* QMan Device Address  */

#define APM_MAC_QMI_CFG1_QM_DEV_ADRS(x)         ((x) & 0x3FFF)
#define APM_MAC_QMI_CFG1_QM_DEV_ADRS_DEF        0x377C

/* bit definitions for QMI FP Buffer Config Register */

/*
 * Disable Prefetch Buffer. Makes the corresponding BufferAvail to deassert to
 * not allow subsystem to access the prefetch buffer. Push messages from QM and
 * 32-bit CPU accesses are allowed. 1-bit each for FP PB 0 to 15.
 */

#define APM_MAC_QMI_CFG_FPBUFn_DIS(n)   (1 << (31 - (n)))

/*
 * Resets the queue logic. It is a self clear bit. 1-bit each for FP PB 0 to 15.
 */

#define APM_MAC_QMI_CFG_FPBUFn_QUERST(n) (1 << (15 - (n)))

/* bit definitions for QMI WQ Buffer Config Register */

/*
 * Disable Prefetch Buffer. Makes the corresponding BufferAvail to deassert to
 * not allow subsystem to access the prefetch buffer. Push messages from QM and
 * 32-bit CPU accesses are allowed. 1-bit each for WQ PB 0 to 15.
 */

#define APM_MAC_QMI_CFG_WQBUFn_DIS(n)   (1 << (31 - (n)))

/*
 * Resets the queue logic. It is a self clear bit. 1-bit each for WQ PB 0 to 15.
 */

#define APM_MAC_QMI_CFG_WQBUFn_QUERST(n) (1 << (15 - (n)))

/* bit definitions for QMI FP Buffer Status 0/1/2 Registers */

/* Write(tail) pointer for FP PB 0 to 15 */

#define APM_MAC_QMI_STS_FPBUF0_PBn_WRPTR(x, n)  \
    (((x) >> (32 - (((n) + 1) * 2))) & 3)

/* Write(tail) pointer for FP PB 16 to 17 (Normally used by SLIMpro(MPA)) */

#define APM_MAC_QMI_STS_FPBUF1_PB16_WRPTR(x)    (((x) >> 26) & 3)
#define APM_MAC_QMI_STS_FPBUF1_PB17_WRPTR(x)    (((x) >> 24) & 3)

/* Number of 16B entires in FP PB 0 to 7  */

#define APM_MAC_QMI_STS_FPBUF1_PBn_ENTNUM(x, n)     \
    (((x) >> ((n) * 3)) & 7)
#define APM_MAC_QMI_STS_FPBUF1_PB_CNT               8

/* Number of 16B entires in FP PB 8 to 17  */

#define APM_MAC_QMI_STS_FPBUF2_PBn_ENTNUM(x, n)    \
    (((x) >> (((n) - APM_MAC_QMI_STS_FPBUF1_PB_CNT) * 3)) & 7)

/* bit definitions for QMI WQ Buffer Status 0/1/2/3 Registers */

/* Write(tail) pointer for WQ PB 0 to 7 */

#define APM_MAC_QMI_STS_WQBUF0_PBn_WRPTR(x, n)  \
    (((x) >> (24 - (((n) + 1) * 3))) & 7)
#define APM_MAC_QMI_STS_WQBUF0_PB_PTR_CNT       8

/* Number of 16B entires in WQ PB (Normally used by SLIMpro(MPA)) */

#define APM_MAC_QMI_STS_WQBUF1_PB16_ENTNUM(x)   (((x) >> 27) & 0xF)

/* Write(tail) pointer for WQ PB 8 to 16 */

#define APM_MAC_QMI_STS_WQBUF1_PBn_WRPTR(x, n)  \
    (((x) >> (27 - (((n) - 8 + 1) * 3))) & 7)

/* Number of 16B entires in FP PB 0 to 7  */

#define APM_MAC_QMI_STS_WQBUF2_PBn_ENTNUM(x, n)    \
    (((x) >> (32 - (((n) + 1) * 4))) & 0xF)
#define APM_MAC_QMI_STS_WQBUF2_PB_ENTNUM_CNT       8

/* Number of 16B entires in FP PB 8 to 15  */

#define APM_MAC_QMI_STS_WQBUF3_PBn_ENTNUM(x, n)    \
    (((x) >> (32 - (((n) - 8 + 1) * 4))) & 0xF)

/* bit definitions for QMI SAB Enable Register */

#define APM_MAC_QMI_CFG_SAB_ENn(n)  (1 << (16 - (n))) /* Queue 0 to 16 */

/* bit definitions for QMI QID monitered for SAB 0/1/2/3 Registers */

#define APM_MAC_QMI_CFG_SABn(n)     \
    (APM_MAC_QMI_CFG_SAB0 + (((n) >> 2) & 3)) /* SABn Register Offset */

#define APM_MAC_QMI_CFG_SAB_QIDn(x, n)  \
    (((x) >> (32 - ((4 - (n)) * 8))) & 0xFF) /* QID monitered */

/* bit definitions for QMI QID monitered for SAB 4 Register */

#define APM_MAC_QMI_CFG_SAB_QID16(x)    ((x) & 0xFF)

/* bit definitions for QMI Interrupt and Mask Registers */

/* Per prefetch buffer basis. FP PB overflow Interrupt */

#define APM_MAC_QMI_STS_INT0_FP_PB_OF(n)    (1 << (17 - (n)))

/* Per prefetch buffer basis. WQ PB overflow Interrupt */

#define APM_MAC_QMI_STS_INT1_WQ_PB_OF(n)    (1 << (16 - (n)))

/* Per prefetch buffer basis. FP PB underrun Interrupt */

#define APM_MAC_QMI_STS_INT2_FP_PB_UR(n)    (1 << (17 - (n)))

/* Per prefetch buffer basis. WQ PB underrun Interrupt */

#define APM_MAC_QMI_STS_INT3_FP_PB_UR(n)    (1 << (16 - (n)))

/* HBF slave error on write master channel Interrupt */

#define APM_MAC_QMI_STS_INT4_HBFWR_ERR      0x2

/* HBF decode error on write master channel Interrupt */

#define APM_MAC_QMI_STS_INT4_HBFDEC_ERR     0x1

/* bit definitions for QMI Debug Control Register */

/* ID of prefetch buffer on which the 32-bit operation is to be perfomed */

#define APM_MAC_QMI_CFG_DBGCTRL_PBN(x)      (((x) & 0x3F) << 11)
#define APM_MAC_QMI_CFG_DBGCTRL_PBN_MASK    0x3F
#define APM_MAC_QMI_CFG_DBGCTRL_PBN_SHIFT   11

/*
 * User must program this bit to 1 when performing a 32-bit operation. It gets
 * cleared when the operation is complete.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_NACK    0x00000400

/*
 * A 32-bit operation with last=0 will stall the HBF write slave port. However
 * any outstanding transaction will be completed.
 *
 * A 32-bit operation with last=1 will unstall the HBF write slave port.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_LAST    0x00000200

/*
 * Push Enable to a PB(prefetch buffer). 32-bit data from
 * APM_MAC_QMI_CFG_DBGWDATAn will be written at APM_MAC_QMI_CFG_DBGCTRL_BUFADRS
 * Write Pointer.
 *
 * This operation will result in write pointer to advance by 1 entry(128-bit).
 *
 * This bit gets cleared when request is accepted.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_PUSH    0x00000100

/*
 * Write Enable to a PB(prefetch buffer). 32-bit data from
 * APM_MAC_QMI_CFG_DBGWDATAn will be written at APM_MAC_QMI_CFG_DBGCTRL_BUFADRS.
 *
 * This bit gets cleared when request is accepted.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_WR      0x00000080

/*
 * Pop Enable to a PB(prefetch buffer). APM_MAC_QMI_CFG_DBGRDATA will contain
 * the 32-bit data pointed by APM_MAC_QMI_CFG_DBGCTRL_BUFADRS read Pointer.
 *
 * This operation will result in read pointer to advance by 1 entry(128-bit).
 *
 * This bit gets cleared when request is accepted.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_POP    0x00000040

/*
 * Read Enable to a PB(prefetch buffer). APM_MAC_QMI_CFG_DBGRDATA will contain
 * the 32-bit data pointed by APM_MAC_QMI_CFG_DBGCTRL_BUFADRS read Pointer.
 *
 * This bit gets cleared when request is accepted.
 */

#define APM_MAC_QMI_CFG_DBGCTRL_RD    0x00000020

/* Address of the 32-bit data to access within a prefetch buffer */

#define APM_MAC_QMI_CFG_DBGCTRL_BUFADRS(x) ((x) & 0x1F)

/* bit definitions for QMI QM Lite Device Address Register */

#define APM_MAC_QMI_CFG_QML_DEVADRS_VAL(x)  ((x) & 0x7FFF)
#define APM_MAC_QMI_CFG_QML_DEVADRS_VAL_DEF 0x3400

/*
 * bit definitions for QMI FP PB Association Register
 *
 * Per FP basis:
 * 0 -> FP PB is associated with QM
 * 1 -> FP PB is associated with QM Lite
 */

#define APM_MAC_QMI_CFG_FPASSOCn(n) (1 << (n))
#define APM_MAC_QMI_CFG_FPASSOC_VAL 0x30000 /* FP PB 17 & 16 are for QML */

/*
 * bit definitions for QMI WQ PB Association Register
 *
 * Per WQ basis:
 * 0 -> WQ PB is associated with QM
 * 1 -> WQ PB is associated with QM Lite
 */

#define APM_MAC_QMI_CFG_WQASSOCn(n) (1 << (n))
#define APM_MAC_QMI_CFG_WQASSOC_VAL 0x10000 /* WQ PB 16 is for QML */

/* bit definitions for QMI WQ 16 Memory Register */

#define APM_MAC_QMI_CFG_WQ16MEM_PA(x)      (((x) & 3) << 4) /* Read Write */
#define APM_MAC_QMI_CFG_WQ16MEM_PA_MASK     3 /* Margin control for port A */
#define APM_MAC_QMI_CFG_WQ16MEM_PA_SHIFT    4

#define APM_MAC_QMI_CFG_WQ16MEM_PB(x)      (((x) & 3) << 2) /* Read Write */
#define APM_MAC_QMI_CFG_WQ16MEM_PB_MASK     3 /* Margin control for port B */
#define APM_MAC_QMI_CFG_WQ16MEM_PB_SHIFT    2

#define APM_MAC_QMI_CFG_WQ16MEM_PA_EN       0x2 /* Read Write Margin Control */
                                                /* enable for port A */
#define APM_MAC_QMI_CFG_WQ16MEM_PB_EN       0x1 /* Read Write Margin Control */
                                                /* enable for port B */

/*
 * Indirect Access Control Registers
 *
 * Ethernet block has 2 Port, and each block has one set of MAC contorl and
 * status registers, that can only be accessed by its own indirect access
 * control registers.
 *
 * NOTE: Device Identify register is one gloabl register, and its offset is
 * based the base CSR address of the ethernet block.
 */

#define APM_MAC_DEVID           0x0028  /* Device Identify (gloabl)*/

#define APM_MAC_IND_CTRL_BASE0  0x0000
#define APM_MAC_IND_CTRL_BASE1  0x0030

#define APM_MAC_IND_ADRS        0x00 /* Address to be accessed */
#define APM_MAC_IND_CMD         0x04 /* Access command */
#define APM_MAC_IND_WR          0x08 /* Data to be written */
#define APM_MAC_IND_RD          0x0C /* Data to be read */
#define APM_MAC_IND_DONE        0x10 /* Done status */

#define APM_MAC_STS_IND_ADRS    0x14 /* Status Address to be accessed */
#define APM_MAC_STS_IND_CMD     0x18 /* Status Access command */
#define APM_MAC_STS_IND_WR      0x1c /* Status Data to be written */
#define APM_MAC_STS_IND_RD      0x20 /* Status Data to be read */
#define APM_MAC_STS_IND_DONE    0x24 /* Status Done status */

/* bit definitions for Command Register */

#define APM_MAC_IND_CMD_WR     0x80000000 /* Write command */
#define APM_MAC_IND_CMD_RD     0x40000000 /* Read command */

/* bit definitions for Device Identify Register */

#define APM_MAC_DEVID_ID(x)     (((x) >> 24) & 0xFF) /* Device Id */
#define APM_MAC_DEVID_MAJREV(x) (((x) >> 8) & 0xFF)  /* Major Revision */
#define APM_MAC_DEVID_MINREV(x) ((x) & 0xFF)         /* Minor Revision */

/* MAC Contorl Registers */

#define APM_MAC_MACCFG1         0x00 /* MAC Config 1 */
#define APM_MAC_MACCFG2         0x04 /* MAC Config 2 */
#define APM_MAC_IPG_IFG         0x08 /* IPG/IFG Config */
#define APM_MAC_HD              0x0c /* Half Duplex */
#define APM_MAC_MAX_FRAME_LEN   0x10 /* Maximum Frame Length */
#define APM_MAC_MII_MGMT_CFG    0x20 /* MII Management Config */
#define APM_MAC_MII_MGMT_CMD    0x24 /* MII Management Command */
#define APM_MAC_MII_MGMT_ADRS   0x28 /* MII Management Address */
#define APM_MAC_MII_MGMT_WR     0x2C /* MII Management Data to be written  */
#define APM_MAC_MII_MGMT_RD     0x30 /* MII Management Data to be read  */
#define APM_MAC_MII_MGMT_STS    0x34 /* MII Management Status */
#define APM_MAC_IF_CTRL         0x38 /* Interface Control */
#define APM_MAC_IF_STS          0x3c /* Interface Status */
#define APM_MAC_MACADRS0        0x40 /* Station Address 0 */
#define APM_MAC_MACADRS1        0x44 /* Station Address 1 */

/* bit definitions for MAC Config 1 Register */

#define APM_MAC_MACCFG1_SRST        0x80000000 /* Soft Reset */
#define APM_MAC_MACCFG1_SIM_RST     0x40000000 /* Simulation Reset */
#define APM_MAC_MACCFG1_RXMC_RST    0x00080000 /* Rx MAC Control Block Reset */
#define APM_MAC_MACCFG1_TXMC_RST    0x00040000 /* Tx MAC Control Block Reset */
#define APM_MAC_MACCFG1_RXFUN_RST   0x00020000 /* Rx Function Block Reset */
#define APM_MAC_MACCFG1_TXFUN_RST   0x00010000 /* Tx Function Block Reset */
#define APM_MAC_MACCFG1_LOOPBACK    0x00000100 /* Loopback */
#define APM_MAC_MACCFG1_RXFC_EN     0x00000020 /* Rx Flow Control Enable */
#define APM_MAC_MACCFG1_TXFC_EN     0x00000010 /* Tx Flow Control Enable */
#define APM_MAC_MACCFG1_SYNCRX_EN   0x00000008 /* Synchronized Rx Enable */
#define APM_MAC_MACCFG1_RXEN        0x00000004 /* Rx Enable */
#define APM_MAC_MACCFG1_SYNCTX_EN   0x00000002 /* Synchronized Tx Enable */
#define APM_MAC_MACCFG1_TXEN        0x00000001 /* Tx Enable */

/* bit definitions for MAC Config 2 Register */

#define APM_MAC_MACCFG2_PRELEN(x)   (((x) & 0xF) << 12) /* Preamble Length */
#define APM_MAC_MACCFG2_PRELEN_MASK (0xF << 12)
#define APM_MAC_MACCFG2_PRELEN_DEF  7 /* Default Length */

#define APM_MAC_MACCFG2_IFMODE(x)   (((x) & 0x3) << 8) /* Interface Mode */
#define APM_MAC_MACCFG2_IFMODE_MASK (0x3 << 8)
#define APM_MAC_MACCFG2_IFMODE_NIB  1 /* Nibble mode */
#define APM_MAC_MACCFG2_IFMODE_BYTE 2 /* Byte mode */

#define APM_MAC_MACCFG2_HUGFRAME_EN 0x00000020 /* Huge Frame Enable */
#define APM_MAC_MACCFG2_LEN_CHECK   0x00000010 /* Length Field Checking */
#define APM_MAC_MACCFG2_PAD_CRC     0x00000004 /* PAD / CRC Enable */
#define APM_MAC_MACCFG2_CRC_EN      0x00000002 /* CRC Enable */
#define APM_MAC_MACCFG2_FD          0x00000001 /* Full Duplex (Half Duplex is */
                                               /* not supported)*/

/* bit definitions for IPG/IFG Config Register */

#define APM_MAC_IPG_IFG_IPGR1(x)    (((x) & 0x7F) << 24) /* Non Back-to-Back */
#define APM_MAC_IPG_IFG_IPGR1_DEF   0x40 /* Inter Packet Gap Part 1 */
#define APM_MAC_IPG_IFG_IPGR1_MASK  (0x7F << 24)

#define APM_MAC_IPG_IFG_IPGR2(x)    (((x) & 0x7F) << 16) /* Non Back-to-Back */
#define APM_MAC_IPG_IFG_IPGR2_DEF   0x60 /* Inter Packet Gap Part 2 */
#define APM_MAC_IPG_IFG_IPGR2_MASK  (0x7F << 16)

#define APM_MAC_IPG_IFG_MINIFG(x)   (((x) & 0xFF) << 8) /* Minimum IFG */
#define APM_MAC_IPG_IFG_MINIFG_DEF  0x50 /* Enforcement */
#define APM_MAC_IPG_IFG_MINIFG_MASK (0xFF << 8)

#define APM_MAC_IPG_IFG_B2BIPG(x)   ((x) & 0x7F)  /* Back-to-Back Inter */
#define APM_MAC_IPG_IFG_B2BIPG_DEF  0x50 /* Packet Gap */
#define APM_MAC_IPG_IFG_B2BIPG_MASK 0x7F

/* bit definitions for Half Duplex Register */

#define APM_MAC_HD_ABEBT(x)         (((x) & 0xF) << 20) /* Alternate Binary */
#define APM_MAC_HD_ABEBT_DEF        0xA /* Exponential Backoff Truncation */
#define APM_MAC_HD_ABEBT_MASK       (0xF << 20)

#define APM_MAC_HD_ABEBE            0x00080000 /* Alternate Binary */
                                               /* Exponential Backoff Enable */
#define APM_MAC_HD_BPNB             0x00040000 /* Backpressure No Backoff */
#define APM_MAC_HD_NB               0x00020000 /* No Backoff */
#define APM_MAC_HD_EXDEFER          0x00010000 /* Excessive Defer */

#define APM_MAC_HD_RETRANS_MAX(x)   (((x) & 0xF) << 12) /* Retransmission */
#define APM_MAC_HD_RETRANS_MAX_DEF  0xF /* Maximum */
#define APM_MAC_HD_RETRANS_MAX_MASK (0xF << 12)

#define APM_MAC_HD_COL_WIN(x)       ((x) & 0x3FF) /* Collision Window */
#define APM_MAC_HD_COL_WIN_DEF      0x37
#define APM_MAC_HD_COL_WIN_MASK     0x3FF

/* bit definitions for Maximum Frame Length Register */

#define APM_MAC_MAX_FRAME_LEN_VAL(x)    ((x) & 0xFFFF)
#define APM_MAC_MAX_FRAME_LEN_VAL_DEF   0x600 /* 1536 Bytes */

/* bit definitions for MII Management Config Register */

#define APM_MAC_MII_MGMT_CFG_RST        0x80000000  /* Reset */
#define APM_MAC_MII_MGMT_CFG_SCAN       0x00000020  /* Scan Auto Increment */
#define APM_MAC_MII_MGMT_CFG_PRE        0x00000010  /* Preamble Suppression */
#define APM_MAC_MII_MGMT_CFG_CLK(x)     ((x) & 0x7) /* MDC frequency */
                                                 /* (ratio to source clock) */
                                                 /* 0: divided by 4  */
                                                 /* 1: divided by 4  */
                                                 /* 2: divided by 6  */
                                                 /* 3: divided by 8  */
                                                 /* 4: divided by 10 */
                                                 /* 5: divided by 14 */
                                                 /* 6: divided by 20 */
                                                 /* 7: divided by 28 */
#define APM_MAC_MII_MGMT_CFG_CLK_DEF    7
#define APM_MAC_MII_MGMT_CFG_CLK_MASK   0x7

/* bit definitions for MII Management Command Register */

#define APM_MAC_MII_MGMT_CMD_SCAN       0x00000002 /* Scan Cycle */
#define APM_MAC_MII_MGMT_CMD_READ       0x00000001 /* Read Cycle */

/* bit definitions for MII Management Address Register */

#define APM_MAC_MII_MGMT_ADRS_PHYID(x)  (((x) & 0x1F) << 8) /* PHY Address */
#define APM_MAC_MII_MGMT_ADRS_REGID(x)  ((x) & 0x1F) /* Register Address */

/* bit definitions for MII Management Read/Write Register */

#define APM_MAC_MII_MGMT_PHYREG_VAL(x)  ((x) & 0xFFFF)

/* bit definitions for MII Management Status Register */

#define APM_MAC_MII_MGMT_STS_INV        0x00000004 /* Invalid */
#define APM_MAC_MII_MGMT_STS_SCAN       0x00000002 /* Scanning */
#define APM_MAC_MII_MGMT_STS_BUSY       0x00000001 /* Busy */

/* bit definitions for Interface Control Register */

#define APM_MAC_IF_CTRL_RST             0x80000000 /* Reset Interface Block */
#define APM_MAC_IF_CTRL_TBI_MODE        0x08000000 /* TBI Mode */
#define APM_MAC_IF_CTRL_GHD_MODE        0x04000000 /* GHD Mode */
#define APM_MAC_IF_CTRL_LHD_MODE        0x02000000 /* LHD Mode */
#define APM_MAC_IF_CTRL_PHY_MODE        0x01000000 /* PHY Mode */
#define APM_MAC_IF_CTRL_RST_RMII        0x00800000 /* Reset RMII Module */
#define APM_MAC_IF_CTRL_SPEED_100M      0x00010000 /* 100Mbps RMII Mode */
#define APM_MAC_IF_CTRL_RST_100X        0x00008000 /* Reset 100X Module */
#define APM_MAC_IF_CTRL_QUIET           0x00000400 /* Force Quiet */
#define APM_MAC_IF_CTRL_NOCIPHER        0x00000200 /* No Cipher */
#define APM_MAC_IF_CTRL_DIS_LINK_FAIL   0x00000100 /* Disable Link Fail */
#define APM_MAC_IF_CTRL_RST_GPSI        0x00000080 /* Reset GPSI */
#define APM_MAC_IF_CTRL_EN_JAB          0x00000001 /* Enable Jabber */
                                                   /* Protection */

/* bit definitions for Interface Status Register */

#define APM_MAC_IF_STS_EXDEFER          0x00000200 /* Excess Defer */
#define APM_MAC_IF_STS_CLASH            0x00000100 /* Clash */
#define APM_MAC_IF_STS_MII_JABBER       0x00000080 /* Jabber */
#define APM_MAC_IF_STS_LINK_OK          0x00000040 /* Link OK */
#define APM_MAC_IF_STS_FD               0x00000020 /* Full Duplex */
#define APM_MAC_IF_STS_100M             0x00000010 /* 100Mbps Mode */
#define APM_MAC_IF_STS_LINK_FAIL        0x00000008 /* Link Fail */
#define APM_MAC_IF_STS_LOC              0x00000004 /* Loss of Carrier */
#define APM_MAC_IF_STS_SQE_ERR          0x00000002 /* SQE Error */
#define APM_MAC_IF_STS_JAB              0x00000001 /* Jabber */

/* bit definitions for Station Address Register */

#define APM_MAC_MACADRS0_OCT0(x)        (((x) & 0xFF) << 24)
#define APM_MAC_MACADRS0_OCT1(x)        (((x) & 0xFF) << 16)
#define APM_MAC_MACADRS0_OCT2(x)        (((x) & 0xFF) << 8)
#define APM_MAC_MACADRS0_OCT3(x)        ((x) & 0xFF)

#define APM_MAC_MACADRS1_OCT4(x)        (((x) & 0xFF) << 24)
#define APM_MAC_MACADRS1_OCT5(x)        (((x) & 0xFF) << 16)

/* MAC Status Registers */

/* Counter */

#define APM_MAC_STAT_TR64   0x20 /* Tx Late Collision Packet */
#define APM_MAC_STAT_TR127  0x21 /* Tx & Rx 65 to 127 Byte Frame */
#define APM_MAC_STAT_TR255  0x22 /* Tx & Rx 128 to 255 Byte Frame */
#define APM_MAC_STAT_TR511  0x23 /* Tx & Rx 256 to 511 Byte Frame */
#define APM_MAC_STAT_TR1K   0x24 /* Tx & Rx 512 to 1023 Byte Frame */
#define APM_MAC_STAT_TRMAX  0x25 /* Tx & Rx 1024 to 1518 Byte Frame */
#define APM_MAC_STAT_TRMGV  0x26 /* Tx & Rx 1519 to 1522 Byte VLAN Frame */
#define APM_MAC_STAT_RBYT   0x27 /* Rx Byte */
#define APM_MAC_STAT_RPKT   0x28 /* Rx Packet */
#define APM_MAC_STAT_RFCS   0x29 /* Rx FCS Error */
#define APM_MAC_STAT_RMCA   0x2A /* Rx Multicast Packet */
#define APM_MAC_STAT_RBCA   0x2B /* Rx Broadcast Packet */
#define APM_MAC_STAT_RXCF   0x2C /* Rx Control Frame Packet */
#define APM_MAC_STAT_RXPF   0x2D /* Rx Pause Frame Packet */
#define APM_MAC_STAT_RXUO   0x2E /* Rx Unknown OPCode Packet */
#define APM_MAC_STAT_RALN   0x2F /* Rx Alignment Error */
#define APM_MAC_STAT_RFLR   0x30 /* Rx Frame Length Error */
#define APM_MAC_STAT_RCDE   0x31 /* Rx Code Error */
#define APM_MAC_STAT_RCSE   0x32 /* Rx Carrier Sense Error */
#define APM_MAC_STAT_RUND   0x33 /* Rx Undersize Packet */
#define APM_MAC_STAT_ROVR   0x34 /* Rx Oversize Packet */
#define APM_MAC_STAT_RFRG   0x35 /* Rx Fragments */
#define APM_MAC_STAT_RJBR   0x36 /* Rx Jabber */
#define APM_MAC_STAT_RDRP   0x37 /* Rx Dropped Packet */
#define APM_MAC_STAT_TBYT   0x38 /* Tx Byte */
#define APM_MAC_STAT_TPKT   0x39 /* Tx Packet */
#define APM_MAC_STAT_TMCA   0x3A /* Tx Multicast Packet */
#define APM_MAC_STAT_TBCA   0x3B /* Tx Broadcast Packet */
#define APM_MAC_STAT_TXPF   0x3C /* Tx Pause Frame Packet */
#define APM_MAC_STAT_TDFR   0x3D /* Tx Deferral Packet */
#define APM_MAC_STAT_TEDF   0x3E /* Tx Excessive Deferral Packet */
#define APM_MAC_STAT_TSCL   0x3F /* Tx Single Collision Packet */
#define APM_MAC_STAT_TMCL   0x40 /* Tx Multiple Collision Packet */
#define APM_MAC_STAT_TLCL   0x41 /* Tx Late Collision Packet */
#define APM_MAC_STAT_TXCL   0x42 /* Tx Excessive Collision Packet */
#define APM_MAC_STAT_TNCL   0x43 /* Tx Total Collision */
#define APM_MAC_STAT_TPFH   0x44 /* Tx Pause Frames Honored */
#define APM_MAC_STAT_TDRP   0x45 /* Tx Dropped Packet */
#define APM_MAC_STAT_TJBR   0x46 /* Tx Jabber Frame */
#define APM_MAC_STAT_TFCS   0x47 /* Tx FCS Error */
#define APM_MAC_STAT_TXCF   0x48 /* Tx Control Frame */
#define APM_MAC_STAT_TOVR   0x49 /* Tx Oversize Frame */
#define APM_MAC_STAT_TUND   0x4A /* Tx Undersize Frame */
#define APM_MAC_STAT_TFRG   0x4B /* Tx Fragment */

/* Carry and Carry Mask */

#define APM_MAC_STAT_CAR1   0x0000004C
#define APM_MAC_STAT_CAR2   0x0000004D
#define APM_MAC_STAT_CAM1   0x0000004E
#define APM_MAC_STAT_CAM2   0x0000004F

/* bit definitions for Carry and Carry Mask Register */

#define APM_MAC_STAT_CA1_64     0x80000000
#define APM_MAC_STAT_CA1_127    0x40000000
#define APM_MAC_STAT_CA1_255    0x20000000
#define APM_MAC_STAT_CA1_511    0x10000000
#define APM_MAC_STAT_CA1_1K     0x08000000
#define APM_MAC_STAT_CA1_MAX    0x04000000
#define APM_MAC_STAT_CA1_MGV    0x02000000
#define APM_MAC_STAT_CA1_RBY    0x00010000
#define APM_MAC_STAT_CA1_RPK    0x00008000
#define APM_MAC_STAT_CA1_RFC    0x00004000
#define APM_MAC_STAT_CA1_RMC    0x00002000
#define APM_MAC_STAT_CA1_RBC    0x00001000
#define APM_MAC_STAT_CA1_RXC    0x00000800
#define APM_MAC_STAT_CA1_RXP    0x00000400
#define APM_MAC_STAT_CA1_RXU    0x00000200
#define APM_MAC_STAT_CA1_RAL    0x00000100
#define APM_MAC_STAT_CA1_RFL    0x00000080
#define APM_MAC_STAT_CA1_RCD    0x00000040
#define APM_MAC_STAT_CA1_RCS    0x00000020
#define APM_MAC_STAT_CA1_RUN    0x00000010
#define APM_MAC_STAT_CA1_ROV    0x00000008
#define APM_MAC_STAT_CA1_RFR    0x00000004
#define APM_MAC_STAT_CA1_RJB    0x00000002
#define APM_MAC_STAT_CA1_RDR    0x00000001

#define APM_MAC_STAT_CA2_TJB    0x00080000
#define APM_MAC_STAT_CA2_TFC    0x00040000
#define APM_MAC_STAT_CA2_TCF    0x00020000
#define APM_MAC_STAT_CA2_TOV    0x00010000
#define APM_MAC_STAT_CA2_TUN    0x00008000
#define APM_MAC_STAT_CA2_TFG    0x00004000
#define APM_MAC_STAT_CA2_TBY    0x00002000
#define APM_MAC_STAT_CA2_TPK    0x00001000
#define APM_MAC_STAT_CA2_TMC    0x00000800
#define APM_MAC_STAT_CA2_TBC    0x00000400
#define APM_MAC_STAT_CA2_TPF    0x00000200
#define APM_MAC_STAT_CA2_TDF    0x00000100
#define APM_MAC_STAT_CA2_TED    0x00000080
#define APM_MAC_STAT_CA2_TSC    0x00000040
#define APM_MAC_STAT_CA2_TMA    0x00000020
#define APM_MAC_STAT_CA2_TLC    0x00000010
#define APM_MAC_STAT_CA2_TXC    0x00000008
#define APM_MAC_STAT_CA2_TNC    0x00000004
#define APM_MAC_STAT_CA2_TPH    0x00000002
#define APM_MAC_STAT_CA2_TDP    0x00000001

/* Share instance context */

#define APM_MAC_BAR_ID      0
#define APM_MAC_CLE_BAR_ID  1

typedef struct
    {
    void *                  macHandle;
    void *                  macBar;

    UINT32                  miiPort;
    UINT32                  portUsed[APM_MAC_PORT_NUM];
    void *                  drvCtrl[APM_MAC_PORT_NUM];
    UINT32                  portNum;
    void                    (*clkdivSet)(UINT32, UINT32);
    UINT32                  jumboEnabled;
    JOB_QUEUE_ID            rxJobQueueId;

    SEM_ID                  shareSem;
    SEM_ID                  cleSem;

    /* Ethernet */

    UINT32                  errIntEn;

    /* MAC Error Interrupt */

    UINT32                  errRsifInt;
    UINT32                  errRsifFInt;
    UINT32                  errTsifInt;
    UINT32                  errTsifFInt;
    UINT32                  errTsoInt;
    UINT32                  errSpi2SapInt;
    UINT32                  errRxTxBufChksmInt;
    UINT32                  errP1RxPause;
    UINT32                  errP1RxChksum;
    UINT32                  errP1TxChksum;
    UINT32                  errP1RxBufOf;
    UINT32                  errP1RxBufUf;
    UINT32                  errP1TxBufOf;
    UINT32                  errP1TxBufUf;
    UINT32                  errP0RxPause;
    UINT32                  errP0RxChksum;
    UINT32                  errP0TxChksum;
    UINT32                  errP0RxBufOf;
    UINT32                  errP0RxBufUf;
    UINT32                  errP0TxBufOf;
    UINT32                  errP0TxBufUf;

    UINT32                  errInt0;
    UINT32                  errInt1;

    UINT32                  errQmiInt0;
    UINT32                  errQmiInt1;
    UINT32                  errQmiInt2;
    UINT32                  errQmiInt3;
    UINT32                  errQmiInt4;

    /* QMan Related */

    VXB_DEVICE_ID           qmanDev;
    UINT8                   qmanRType;
    UINT32                  qmanRxFpMsgBufLen;
    APM_QMAN_FUNC *         qmanFunc;

    /* Classifier */

    void *                      cleHandle;
    void *                      cleBar;
    BOOL                        cleStarted[APM_MAC_PORT_NUM];

    UINT32                      dbRamNumPerPort;
    UINT32                      dbRamUsed[APM_MAC_CLE_DBRAM_ENTRY_NUM];
    UINT32                      ptRamNumPerPort;
    APM_MAC_CLE_PTREE_NODE_STAT ptNodeStat[APM_MAC_CLE_PTRAM_ENTRY_NUM];

    UINT32                      avlRamMaxStrLen;
    UINT32                      avlRamMaxDepth;
    UINT32                      avlRamNum;
    } APM_MAC_SHARE_DRV_CTRL;

/* Private instance context */

typedef struct
    {
    END_OBJ             apmMacEndObj;
    VXB_DEVICE_ID       apmMacDev;
    VXB_DEVICE_ID       apmMacShareDev;
    void *              apmMacMuxDevCookie;
    int                 apmMacMaxMtu;

#ifdef APM_MAC_TX_MODERATION
    WDOG_ID             apmMacTxWd;
#endif

    JOB_QUEUE_ID        apmMacJobQueue;
    VXB_DMA_TAG_ID      apmMacMblkTag;

    QJOB                apmMacRxJob;
    atomic_t *          apmMacRxPending;
    M_BLK_ID            apmMacRxMblk[APM_MAC_RX_BUF_CNT];
    VXB_DMA_MAP_ID      apmMacRxMblkMap[APM_MAC_RX_BUF_CNT];

    UINT32              apmMacTxProd;
    QJOB                apmMacTxJob;
    atomic_t *          apmMacTxPending;
    volatile BOOL       apmMacTxStall;
    M_BLK_ID            apmMacTxMblk[APM_MAC_TX_BUF_CNT];
    VXB_DMA_MAP_ID      apmMacTxMblkMap[APM_MAC_TX_BUF_CNT];
    BOOL                apmMacPolling;
    M_BLK_ID            apmMacPollBuf;
    UINT8               apmMacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    apmMacCaps;

    END_IFDRVCONF       apmMacEndStatsConf;
    END_IFCOUNTERS      apmMacEndStatsCounters;

    SEM_ID              apmMacDevSem;

    UINT32              apmMacInErrors;
    UINT32              apmMacInDiscards;
    UINT32              apmMacInUcasts;
    UINT32              apmMacInMcasts;
    UINT32              apmMacInBcasts;
    UINT32              apmMacInOctets;
    UINT32              apmMacOutErrors;
    UINT32              apmMacOutUcasts;
    UINT32              apmMacOutMcasts;
    UINT32              apmMacOutBcasts;
    UINT32              apmMacOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *             apmMacMediaList;
    END_ERR                     apmMacLastError;
    UINT32                      apmMacCurMedia;
    UINT32                      apmMacCurStatus;
    VXB_DEVICE_ID               apmMacMiiBus;
    VXB_DEVICE_ID               apmMacMiiDev;
    FUNCPTR                     apmMacMiiPhyRead;
    FUNCPTR                     apmMacMiiPhyWrite;
    int                         apmMacMiiPhyAddr;

    spinLockNdTimed_t *         apmMacMdioLock;
    spinLockNdTimedKey_t        apmMacMdioKey;

    /* End MII/ifmedia required fields */

    /* Ethernet */

    UINT32                          apmMacErrIntEn;
    UINT32                          apmMacEthPort;
    APM_MAC_SHARE_DRV_CTRL *        apmMacShareCtrl;

    /* Patricia Tree config */

    APM_MAC_CLE_PARSER_CFG *        parserCfg;

    APM_MAC_CLE_DBRAM_ENTRY *       dbEntryArray;
    UINT32                          dbEntryNum;

    APM_MAC_CLE_PTREE_NODE_HDR *    ptNodeHdrArray;
    APM_MAC_CLE_PTREE_NODE_HDR *    ptNodeHdrArrayClone;
    UINT32                          ptNodeHdrNum;

    /* Queue Info */

    VXB_DMA_TAG_ID                  apmMacParentTag;

    VXB_DMA_TAG_ID                  apmMacRxIngQueDmaTag;
    VXB_DMA_MAP_ID                  apmMacRxIngQueDmaMap;;
    void *                          apmMacRxIngQueMem;
    UINT64                          apmMacRxIngQueMemPhy;
    APM_QMAN_QUE_INFO               apmMacRxIngQueInfo;

    VXB_DMA_TAG_ID                  apmMacRxFpDmaTag;
    VXB_DMA_MAP_ID                  apmMacRxFpDmaMap;;
    void *                          apmMacRxFpMem;
    UINT64                          apmMacRxFpMemPhy;
    APM_QMAN_QUE_INFO               apmMacRxFpInfo;

    VXB_DMA_TAG_ID                  apmMacTxQueDmaTag;
    VXB_DMA_MAP_ID                  apmMacTxQueDmaMap;;

    void *                          apmMacTxEgQueMem;
    UINT64                          apmMacTxEgQueMemPhy;
    APM_QMAN_QUE_INFO               apmMacTxEgQueInfo;

    void *                          apmMacTxCompQueMem;
    UINT64                          apmMacTxCompQueMemPhy;
    APM_QMAN_QUE_INFO               apmMacTxCompQueInfo;

    VXB_DMA_TAG_ID                  apmMacTxLLBufDmaTag;
    VXB_DMA_MAP_ID                  apmMacTxLLBufDmaMap;
    void *                          apmMacTxLLBufMem;
    UINT64                          apmMacTxLLBufMemPhy;
    }APM_MAC_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbApmPProMacEndh */
