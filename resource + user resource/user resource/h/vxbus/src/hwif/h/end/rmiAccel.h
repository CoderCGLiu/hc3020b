/* rmiAccel.h - header file for RMI network accelerator */

/*
 * Copyright (c) 2007-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,15sep10,x_f  add more definitions for SPI4
01e,15mar10,h_k  fixed a compiler warning in GNU env.
01d,11jan10,h_k  LP64 adaptation.
01c,21may09,wap  Add more definitions for the parser
01b,02may08,wap  Add 10GbE XGMAC support
01a,10oct07,wap  written
*/

#ifndef __INCrmiAccelh
#define __INCrmiAccelh

#ifdef __cplusplus
extern "C" {
#endif

#define XLRA_TXCTRL		0x0280 /* Transmit Control The TxnHalt field (bits [31:16]) N/A for gmac */
#define XLRA_RXCTRL		0x0284 /* Receive Control SinglePort (bit 6) N/A for gmac */
#define XLRA_DPKTCTRL		0x0288 /* Packet Descriptor Control ByteOffset (bits [19:17]) is N/A forXGMII/SPI4 */
#define XLRA_STATCTRL		0x028C /* Statistics Control */
#define XLRA_L2CTRL		0x0290 /* L2 Cache Allocation */
#define XLRA_IMR		0x0294 /* Interrupt Mask */
#define XLRA_ISR		0x0298 /* Interrupt */
#define XLRA_CORECTRL		0x02A0 /* Core Control Register Speed (bits 1:0) N/A for XGMII and SPI4*/
#define XLRA_BYTEOFFSETS0	0x02A4 /* Byte Offsets for Channels */
#define XLRA_BYTEOFFSETS1	0x02A8 /* Byte Offsets for Channels */
#define XLRA_PAUSEFRAME		0x02AC /* Pause Frame */
#define XLRA_FIFOMODECTRL	0x02BC /* FIFO Mode Control Applies only to RGMII */

/* Parser registers */

#define XLRA_L2TYPE_0		0x03C0
#define XLRA_PARSERCFG		0x0400
#define XLRA_PARSEDEPTH		0x0404
#define XLRA_EXTRACTSTRHASH0	0x0408
#define XLRA_EXTRACTSTRHASH1	0x040C
#define XLRA_EXTRACTSTRHASH2	0x0410
#define XLRA_EXTRACTSTRHASH3	0x0414
#define XLRA_L3_L4_PROTO	0x0418
#define XLRA_L3CTABLE		0x0500
#define XLRA_L4CTABLE		0x0580
#define XLRA_CAM4X128TABLE	0x058C
#define XLRA_CAM4X128KEY	0x0600
#define XLRA_TRANSTABLE		0x0680
#define XLRA_L3CTABLEMASKS	0x0780
#define XLRA_L4CTABLEMASKS	0x0790
#define XLRA_HASH_BASE		0x07A0
#define XLRA_HASH_BASE_KEYS	0x07B0

#define XLRA_DMACR0		0x0800 /* DMA Credit Register */  
#define XLRA_DMACR1		0x0804 /* DMA Credit Register Does not apply to XLRA.   */
#define XLRA_DMACR2		0x0808 /* DMA Credit Register Does not apply to XLRA. */
#define XLRA_DMACR3		0x080C /* DMA Credit Register */
                  
#define XLRA_FRINSPMEMSTART0	0x0810 /* Start address of RegularFree Descriptor In Spill-over */  
#define XLRA_FRINSPMEMSTART1	0x0814 /* Extended start address of Regular Free Descriptor InSpill-over */
#define XLRA_FRINSPMEMSIZE	0x0818 /* Size of Regular FreeDescriptor Spill-over Memory */
                               
#define XLRA_FROUTSPMEMSTART0	0x081C /* Start address of Free Descriptor Out Spill-over */
#define XLRA_FROUTSPMEMSTART1	0x0820 /* Extended Start address of Free Descriptor Out Spill-over */
#define XLRA_FROUTSPMEMSIZE	0x0824 /* Free Out Descriptor Spill-over Memory Size */
        
#define XLRA_CLASS0SPMEMSTART0	0x0828 /* Start address of Class0 Descriptor Spill-over */
#define XLRA_CLASS0SPMEMSTART1	0x082C /* Extended start address of Class0 Descriptor Spill-over */
#define XLRA_CLASS0SPMEMSIZE	0x0830 /* Size of Class0 Descriptor */
#define XLRA_CLASS1SPMEMSTART0	0x0840 /* Start address of Class1 Descriptor Spill-over */
#define XLRA_CLASS1SPMEMSTART1	0x0844 /* Extended start address of Class1 Descriptor Spill-over */
#define XLRA_CLASS1SPMEMSIZE	0x0848 /* Size of Class1 descriptor Spill-over Memory */
#define XLRA_CLASS2SPMEMSTART0	0x084C /* Start address of Class2 Descriptor Spill-over */
#define XLRA_CLASS2SPMEMSTART1	0x0850 /* Extended start address of Class2 Descriptor Spill-over */
#define XLRA_CLASS2SPMEMSIZE	0x0854 /* Size of Class2 Descriptor Spill-over Memory */
#define XLRA_CLASS3SPMEMSTART0	0x0858 /* Start address of Class3 Descriptor Spill-over */
#define XLRA_CLASS3SPMEMSTART1	0x085C /* Extended start address of Class3 Descriptor Spill-over */
#define XLRA_CLASS3SPMEMSIZE	0x0860 /* Size of Class3 Descriptor Spill-over Memory */
#define XLRA_FRIN1SPMEMSTART0	0x0864 /* Start address of Regular Free Descriptor In Spill Over Only for Gmac */
#define XLRA_FRIN1SPMEMSTART1	0x0868 /* Extended start address of Regular Free Descriptor In Spill Over. Only for Gmac */
#define XLRA_FRIN1SPMEMSIZE	0x086C /* Regular Free Descriptor Spill Over Memory Only for Gmac */

#define XLRA_SPI_HUNGRY0        0x0864 /* SPI4 Tx FIFO Hungry Thresholds */
#define XLRA_SPI_HUNGRY1        0x0868 /* SPI4 Tx FIFO Hungry Thresholds */
#define XLRA_SPI_HUNGRY2        0x086c /* SPI4 Tx FIFO Hungry Thresholds */
#define XLRA_SPI_HUNGRY3        0x0870 /* SPI4 Tx FIFO Hungry Thresholds */

#define XLRA_SPI_STARVE0        0x0874 /* SPI4 Tx FIFO Starve Thresholds */
#define XLRA_SPI_STARVE1        0x0878 /* SPI4 Tx FIFO Starve Thresholds */
#define XLRA_SPI_STARVE2        0x087c /* SPI4 Tx FIFO Starve Thresholds */
#define XLRA_SPI_STARVE3        0x0880 /* SPI4 Tx FIFO Starve Thresholds */

#define XLRA_PADCAL0		0x08C4 /* Termination and drive strength control */
#define XLRA_PADCAL1		0x08C8 /* Termination and drive strength status */

#define XLRA_PADCAL0_RXP_PRESET	0x00000001
#define XLRA_PADCAL0_RXN_PRESET	0x00000002
#define XLRA_PADCAL0_TXP_PRESET	0x00000004
#define XLRA_PADCAL0_TXN_PRESET	0x00000008
#define XLRA_PADCAL0_RXAUTO	0x00000010
#define XLRA_PADCAL0_TXAUTO	0x00000020
#define XLRA_PADCAL0_LVDS   0x00002000

/* Descriptor start address settings in 32-bit access mode */

#define XLRA_SPMEMSTART0_SHIFT	5
#define XLRA_SPMEMSTART1_SHIFT	37
#define XLRA_SPMEMSTART1_MASK	0x000000FF00000000ULL
#define XLRA_SPMEMSTART0(x)	(UINT32)((x) >> XLRA_SPMEMSTART0_SHIFT)
#define XLRA_SPMEMSTART1(x)	(UINT32)(((x) >> XLRA_SPMEMSTART1_SHIFT) & \
                                         (XLRA_SPMEMSTART1_MASK >> \
                                          XLRA_SPMEMSTART1_SHIFT))

/* Virtual MIPS mode support */

#define XLRA_FRQCARVE		0x08CC

#define XLRA_FRQCARVE_MAXMTU	0x0007FFE0
#define XLRA_FRQCARVE_FOMODE	0x00000040	/* Free out carve mode */
#define XLRA_FRQCARVE_FIMODE	0x00000020	/* Free in carve mode */
#define XLRA_FRQCARVE_MAXURTR	0x0000001F	/* max underrun retries */

/* FIFO watermark registers */

#define XLRA_CLASSWM		0x0910
#define XLRA_RX0WM		0x0914
#define XLRA_RX1WM		0x0918
#define XLRA_RX2WM		0x091C
#define XLRA_RX3WM		0x0920
#define XLRA_FREEWM		0x0924

#define XLRA_CLASSWM_0		0x1F000000
#define XLRA_CLASSWM_1		0x001F0000
#define XLRA_CLASSWM_2		0x00001F00
#define XLRA_CLASSWM_3		0x0000001F

#define XLRA_FREEWM_FREEOUT	0xFFFF0000
#define XLRA_FREEWM_FREEIN	0x0000007F

#define XLRA_FREEOUTWM(x)	(((x) << 16) & XLRA_FREEWM_FREEOUT)
#define XLRA_FREEINWM(x)	((x) & XLRA_FREEWM_FREEIN)

/* SPI4.2 slot carving */

#define XLRA_FIFOCARVE0		0x0928
#define XLRA_FIFOCARVE1		0x092C

#define XLRA_FIFOCARVE0_C0	0x0000000F
#define XLRA_FIFOCARVE0_C1	0x000000F0
#define XLRA_FIFOCARVE0_C2	0x00000F00
#define XLRA_FIFOCARVE0_C3	0x0000F000
#define XLRA_FIFOCARVE0_C4	0x000F0000
#define XLRA_FIFOCARVE0_C5	0x00F00000
#define XLRA_FIFOCARVE0_C6	0x0F000000
#define XLRA_FIFOCARVE0_C7	0xF0000000

#define XLRA_FIFOCARVE1_C8	0x0000000F
#define XLRA_FIFOCARVE1_C9	0x000000F0
#define XLRA_FIFOCARVE1_C10	0x00000F00
#define XLRA_FIFOCARVE1_C11	0x0000F000
#define XLRA_FIFOCARVE1_C12	0x000F0000
#define XLRA_FIFOCARVE1_C13	0x00F00000
#define XLRA_FIFOCARVE1_C14	0x0F000000
#define XLRA_FIFOCARVE1_C15	0xF0000000


/* PDE class registers */

#define XLRA_PDECLASS0_LO	0x0C00
#define XLRA_PDECLASS0_HI	0x0C04
#define XLRA_PDECLASS1_LO	0x0C08
#define XLRA_PDECLASS1_HI	0x0C0C
#define XLRA_PDECLASS2_LO	0x0C10
#define XLRA_PDECLASS2_HI	0x0C14
#define XLRA_PDECLASS3_LO	0x0C18
#define XLRA_PDECLASS3_HI	0x0C1C

#define XLRA_MSG_TX_THRESH	0x0C20

/*
 * Bucket size registers.
 * These are for RGMII specifically.
 */

#define XLRA_BKTSIZE_RXJFREE	0x0C80	/* jumbo buffers, not used */
#define XLRA_BKTSIZE_RXFREE	0x0C84	/* RX free buffers */
#define XLRA_BKTSIZE_TX0	0x0C88
#define XLRA_BKTSIZE_TX1	0x0C8C
#define XLRA_BKTSIZE_TX2	0x0C90
#define XLRA_BKTSIZE_TX3	0x0C94
#define XLRA_BKTSIZE_RXJFREE1	0x0C98	/* jumbo buffers, not used */
#define XLRA_BKTSIZE_RXFREE1	0x0C9C	/* free free, context 1 - split mode */

/*
 * Bucket size registers.
 * These are for XGMII specifically.
 */

#define XLRA_BKTSIZE_XSAJFREE	0x0CC0
#define XLRA_BKTSIZE_XSAFREE	0x0CC4
#define XLRA_BKTSIZE_XGMIITX	0x0C88 /* Same as RGMII */

/*
 * Bucket size registers.
 * These are for SPI4 specifically.
 */

#define XLRA_BKTSIZE_SPI4FR     0x0CC4
#define XLRA_BKTSIZE_SPI4TX     0x0C80

/*
 * Credit counter registers. There's one of these for every possible
 * message destination, though we're really only interested in the
 * ones for the CPUs, since we want the network accelerator to be
 * able to send messages to the CPU to notify it of packet completion
 * events.
 */

/* CPU0, buckets 0 through 7 */

#define XLRA_CRDCNT_CPU0B0	0x0E00
#define XLRA_CRDCNT_CPU0B1	0x0E04
#define XLRA_CRDCNT_CPU0B2	0x0E08
#define XLRA_CRDCNT_CPU0B3	0x0E0C
#define XLRA_CRDCNT_CPU0B4	0x0E10
#define XLRA_CRDCNT_CPU0B5	0x0E14
#define XLRA_CRDCNT_CPU0B6	0x0E18
#define XLRA_CRDCNT_CPU0B7	0x0E1C

/* CPU1, buckets 0 through 7 */

#define XLRA_CRDCNT_CPU1B0	0x0E20
#define XLRA_CRDCNT_CPU1B1	0x0E24
#define XLRA_CRDCNT_CPU1B2	0x0E28
#define XLRA_CRDCNT_CPU1B3	0x0E2C
#define XLRA_CRDCNT_CPU1B4	0x0E30
#define XLRA_CRDCNT_CPU1B5	0x0E34
#define XLRA_CRDCNT_CPU1B6	0x0E38
#define XLRA_CRDCNT_CPU1B7	0x0E3C

/* CPU2 buckets 0 through 7 */

#define XLRA_CRDCNT_CPU2B0	0x0E40
#define XLRA_CRDCNT_CPU2B1	0x0E44
#define XLRA_CRDCNT_CPU2B2	0x0E48
#define XLRA_CRDCNT_CPU2B3	0x0E4C
#define XLRA_CRDCNT_CPU2B4	0x0E50
#define XLRA_CRDCNT_CPU2B5	0x0E54
#define XLRA_CRDCNT_CPU2B6	0x0E58
#define XLRA_CRDCNT_CPU2B7	0x0E5C

/* CPU3, buckets 0 through 7 */

#define XLRA_CRDCNT_CPU3B0	0x0E60
#define XLRA_CRDCNT_CPU3B1	0x0E64
#define XLRA_CRDCNT_CPU3B2	0x0E68
#define XLRA_CRDCNT_CPU3B3	0x0E6C
#define XLRA_CRDCNT_CPU3B4	0x0E70
#define XLRA_CRDCNT_CPU3B5	0x0E74
#define XLRA_CRDCNT_CPU3B6	0x0E78
#define XLRA_CRDCNT_CPU3B7	0x0E7C


/* TX control register */

#define XLRA_TXCTRL_TXHLT15	0x80000000	/* TX port 15 halted */
#define XLRA_TXCTRL_TXHLT14	0x40000000	/* TX port 14 halted */
#define XLRA_TXCTRL_TXHLT13	0x20000000	/* TX port 13 halted */
#define XLRA_TXCTRL_TXHLT12	0x10000000	/* TX port 12 halted */
#define XLRA_TXCTRL_TXHLT11	0x08000000	/* TX port 11 halted */
#define XLRA_TXCTRL_TXHLT10	0x04000000	/* TX port 10 halted */
#define XLRA_TXCTRL_TXHLT9	0x02000000	/* TX port 9 halted */
#define XLRA_TXCTRL_TXHLT8	0x01000000	/* TX port 8 halted */
#define XLRA_TXCTRL_TXHLT7	0x00800000	/* TX port 7 halted */
#define XLRA_TXCTRL_TXHLT6	0x00400000	/* TX port 6 halted */
#define XLRA_TXCTRL_TXHLT5	0x00200000	/* TX port 5 halted */
#define XLRA_TXCTRL_TXHLT4	0x00100000	/* TX port 4 halted */
#define XLRA_TXCTRL_TXHLT3	0x00080000	/* TX port 3 halted */
#define XLRA_TXCTRL_TXHLT2	0x00040000	/* TX port 2 halted */
#define XLRA_TXCTRL_TXHLT1	0x00020000	/* TX port 1 halted */
#define XLRA_TXCTRL_TXHLT0	0x00010000	/* TX port 0 halted */
#define XLRA_TXCTRL_ENABLE	0x00004000	/* TX enable */
#define XLRA_TXCTRL_THRESH	0x00003FFF	/* TX start thresh (in bytes) */

/* RX control register */

#define XLRA_RXCTRL_SINGLE	0x00000400	/* single port mode */
#define XLRA_RXCTRL_ERRSTS	0x000003C0	/* error status mask */
#define XLRA_RXCTRL_IPRST	0x00000020	/* hard reset the MAC */
#define XLRA_RXCTRL_SRSTDONE	0x00000008	/* soft reset done */
#define XLRA_RXCTRL_SRST	0x00000004	/* force soft reset */
#define XLRA_RXCTRL_HALT	0x00000002	/* RX halted */
#define XLRA_RXCTRL_ENABLE	0x00000001	/* RX enable */

#define XLRA_RXCTL_CODEERR	0x00000040
#define XLRA_RXCTL_CRCERR	0x00000080
#define XLRA_RXCTL_LENCHKERR	0x00000100
#define XLRA_RXCTL_RSVDERR	0x00000200

/* Packet descriptor control register */

#define XLRA_DPKTCTRL_MAXTXDP	0xFC000000	/* Max tx desc hold */
#define XLRA_DPKTCTRL_MAXDHOLD	0x03F00000	/* Max tx tmpfifo hold */
#define XLRA_DPKTCTRL_BYTEOFF	0x000E0000	/* GMAC byte offset */
#define XLRA_DPKTCTRL_PREPAD	0x00010000	/* enable packet prepad */
#define XLRA_DPKTCTRL_REGSIZE	0x00003FFF	/* size of packet buffer */

#define XLRA_MAXTXDP(x)		(((x) << 26) & XLRA_DPKTCTRL_MAXTXDP)
#define XLRA_MAXDHOLD(x)	(((x) << 20) & XLRA_DPKTCTRL_MAXDHOLD)
#define XLRA_BYTEOFF(x)		(((x) << 17) & XLRA_DPKTCTRL_BYTEOFF)

/* Statistics control */

#define XLRA_STATCTRL_OFLOW_EN	0x00000010	/* carry overflow int enable */
#define XLRA_STATCTRL_GIG	0x00000008	/* driver by GMAC core */
#define XLRA_STATCTRL_STEN	0x00000004	/* Enable stats counting */
#define XLRA_STATCTRL_CLRCNT	0x00000002	/* Clear all counters */
#define XLRA_STATCTRL_AUTOZ	0x00000001	/* Auto clear on read */

/* L2 Cache allocation control */

#define XLRA_L2CTRL_TXL2ALLOC	0x0003FE00	/* number of TX cache lines */
#define XLRA_L2CTRL_RXL2ALLOC	0x000001FF	/* number of RX cache lines */

/* Interrupt mask and status registers */

#define XLRA_ISR_SPI4_TX	0x10000000	/* SPI4 TX interrupt */
#define XLRA_ISR_SPI4_RX	0x08000000	/* SPI4 RX interrupt */
#define XLRA_ISR_RGMII_COLL	0x08000000	/* collision in RGMII mode */
#define XLRA_ISR_TX_ABORT	0x04000000	/* TX abort */
#define XLRA_ISR_TX_UFLOW	0x02000000	/* TX underrun */
#define XLRA_ISR_DISCARD	0x01000000	/* data discarded */
#define XLRA_ISR_AFOFLOW	0x00800000	/* async FIFO overflow */
#define XLRA_ISR_TAGFULL	0x00400000	/* all RX tags in use */
#define XLRA_ISR_C3FULL		0x00200000	/* class3 FIFO full */
#define XLRA_ISR_C3EARLYFULL	0x00100000	/* class3 early full */
#define XLRA_ISR_C2FULL		0x00080000	/* class2 FIFO full */
#define XLRA_ISR_C2EARLYFULL	0x00040000	/* class2 early full */
#define XLRA_ISR_C1FULL		0x00020000	/* class1 FIFO full */
#define XLRA_ISR_C1EARLYFULL	0x00010000	/* class1 early full */
#define XLRA_ISR_C0FULL		0x00008000	/* class0 FIFO full */
#define XLRA_ISR_C0EARLYFULL	0x00004000	/* class0 early full */
#define XLRA_ISR_RXFULL		0x00002000	/* RX data FIFO full */
#define XLRA_ISR_RXEARLYFULL	0x00001000	/* RX data FIFO early full */
#define XLRA_ISR_RXEMPTY	0x00000200	/* RX desc FIFO empty */
#define XLRA_ISR_RXEARLYEMPTY	0x00000100	/* RX desc FIFO early empty */
#define XLRA_ISR_SPILLORP2P_ERR	0x00000080	/* ECC err on spill or p2p chan */
#define XLRA_ISR_DEBUG		0x00000040	/* debug event interrupt */
#define XLRA_ISR_FREEOFULL	0x00000020	/* Free out desc FIFO full */
#define XLRA_ISR_FREEOEARLYFULL	0x00000010	/* Free out desc FIFO early full*/
#define XLRA_ISR_TXFETCHERR	0x00000008	/* TX fetch error */
#define XLRA_ISR_CARRY		0x00000004	/* stats counter overflow */
#define XLRA_ISR_MDINT		0x00000002	/* MII management port event */
#define XLRA_ISR_TXILLEGAL	0x00000001	/* bad TX descriptor */

/* Core control register */

#define XLRA_CORECTRL_ERRID	0x000003F8
#define XLRA_CORECTRL_USEERRID	0x00000004
#define XLRA_CORECTRL_SPEED	0x00000003

#define XLRA_SPEED_125MHZ	0x00000000
#define XLRA_SPEED_25MHZ	0x00000001
#define XLRA_SPEED_2_5MHZ	0x00000002

#define XLRA_ERRID(x)		(((x) << 3) & XLRA_CORECTRL_ERRID)

/*
 * Parser register definitions
 */

/* L2Type register */

#define XLRA_L2TYPE_XHDRPROTSIZ	0x7C000000	/* Extra header protocol size */
#define XLRA_L2TYPE_XHDRPROTOFF	0x03F00000	/* Extra header protocol offset */
#define XLRA_L2TYPE_XHDRSIZE	0x000FC000	/* Extra header offset */
#define XLRA_L2TYPE_PROTOFF	0x00003F00	/* L3 protocol field offset */
#define XLRA_L2TYPE_FIXHDROFF	0x000000FC	/* Fixed header offset */
#define XLRA_L2TYPE_L2PROTO	0x00000003	/* Protocol */

/* ParserConfig register */

#define XLRA_PARSERCFG_UCMASK	0x01FF0000	/* Unclassified mask */
#define XLRA_PARSERCFG_CRCPOLY	0x00007F00	/* 7 bit CRC hash polynomial */
#define XLRA_PARSERCFG_USEPORT	0x00000008	/* Use port bind mode */
#define XLRA_PARSERCFG_USECAM	0x00000004	/* Use 128-bit TCAM */
#define XLRA_PARSERCFG_USEHASH	0x00000002	/* Use 7 bit */
#define XLRA_PARSERCFG_USEPROTO	0x00000001	/* L3_CAM, L4_CAM lookup */

#define XLRA_UCMASK_L3_CAM_HIT	0x01000000
#define XLRA_UCMASK_L4_CAM_HIT	0x00800000
#define XLRA_UCMASK_LC3TAB_EX0	0x00400000
#define XLRA_UCMASK_LC3TAB_EX1	0x00200000
#define XLRA_UCMASK_LC3TAB_EX2	0x00100000
#define XLRA_UCMASK_LC4TAB_EX0	0x00080000
#define XLRA_UCMASK_LC4TAB_EX1	0x00040000
#define XLRA_UCMASK_L2HDRDONE	0x00020000
#define XLRA_UCMASK_L3HDRDONE	0x00010000

/* Translate table register */

#define XLRA_TRANSTABLE_CLSID1	0x00C00000	/* Class ID */
#define XLRA_TRANSTABLE_BKTID1	0x007E0000	/* Bucket ID */
#define XLRA_TRANSTABLE_USEBKT1	0x01800000	/* Use bucket ID */
#define XLRA_TRANSTABLE_CLSID0	0x00000180	/* Class ID */
#define XLRA_TRANSTABLE_BKTID0	0x0000007E	/* Bucket ID */
#define XLRA_TRANSTABLE_USEBKT0	0x00000001	/* Use bucket ID */

#define XLRA_CLSID(x)		(((x) << 7) & XLRA_TRANSTABLE_CLSID0)
#define XLRA_BKTID(x)		(((x) << 1) & XLRA_TRANSTABLE_BKTID0)

/*
 * The following are the descriptor structures used by the network
 * accelerator module. These are passed between the cores and
 * accelerator via the fast messaging network (FMN).
 */

/* RX descriptor */

typedef struct xlra_rx_desc
    {
    volatile UINT32	xlra_sts;
    volatile UINT32	xlra_bufaddr_lo;
    } XLRA_RX_DESC;

#define XLRA_RXD_EOP		0x80000000
#define XLRA_RXD_STS		0x7F000000
#define XLRA_RXD_CLASSID	0x00C00000
#define XLRA_RXD_BUFLEN		0x003FFF00
#define XLRA_RXD_BUFADDR_HI	0x000000FF	/* 32 byte aligned */
#define XLRA_RXD_BUFADDR_LO	0xFFFFFFE0	/* 32 byte aligned */
#define XLRA_RXD_UNCLASSIFIED	0x00000010
#define XLRA_RXD_PORTID		0x0000000F

#define XLRA_RXCLASS(x)		(((x) & XLRA_RXD_CLASSID) >> 22)
#define XLRA_RXPORT(x)		((x) & 0xF)
#define XLRA_RXLEN(x)		(((x) & XLRA_RXD_BUFLEN) >> 8)
#define XLRA_RXADDR(x)		((x) & XLRA_RXD_BUFADDR_LO)

typedef UINT64	XLRA_RX_DESC_64;

#define XLRA_RXD_BUFADDR_64	0x000000FFFFFFFFE0ULL

#define XLRA_RXADDR_64(x)	((x) & XLRA_RXD_BUFADDR_64)

/* RX free descriptor */

typedef UINT64 XLRA_RXF_DESC;

#define XLRA_RXF_BUFADDR	0x000000FFFFFFFFE0 /* 32 byte aligned */

/* Status bits for GMAC and XGMAC */

#define XLRA_RXSTS_ERROR	0x40000000
#define XLRA_RXSTS_BCAST	0x20000000
#define XLRA_RXSTS_MCAST	0x10000000
#define XLRA_RXSTS_UCAST	0x08000000
#define XLRA_RXSTS_UMATCH	0x06000000
#define XLRA_RXSTS_CODEERR	0x04000000
#define XLRA_RXSTS_CRCERR	0x02000000
#define XLRA_RXSTS_VLAN		0x01000000
#define XLRA_RXSTS_BADLEN	0x01000000

/* TX descriptor */

typedef struct xlra_tx_desc
    {
    volatile UINT32	xlra_cmd;
    volatile UINT32	xlra_bufaddr_lo;
    } XLRA_TX_DESC;

#define XLRA_TXD_EOP		0x80000000
#define XLRA_TXD_P2P		0x40000000
#define XLRA_TXD_RDEX		0x20000000
#define XLRA_TXD_FBID		0x1FC00000
#define XLRA_TXD_BUFLEN		0x003FFF00
#define XLRA_TXD_BUFADDRHI	0x000000FF
#define XLRA_TXD_BUFADDRLO	0xFFFFFFFF /* any alignment */

#define XLRA_TXLEN(x)		(((x) << 8) & XLRA_TXD_BUFLEN)
#define XLRA_TXFBID(x)		(((x) << 22) & XLRA_TXD_FBID)
#define XLRA_TXADDRLO(x)	((x) & XLRA_TXD_BUFADDRLO)

typedef UINT64	XLRA_TX_DESC_64;

#define XLRA_TXD_EOP_64		0x8000000000000000ULL
#define XLRA_TXD_P2P_64		0x4000000000000000ULL
#define XLRA_TXD_RDEX_64	0x2000000000000000ULL
#define XLRA_TXD_FBID_64	0x1FC0000000000000ULL
#define XLRA_TXD_BUFLEN_64	0x003FFF0000000000ULL
#define XLRA_TXD_BUFADDR_64	0x000000FFFFFFFFFFULL

#define XLRA_TXLEN_64(x)	((UINT64)((x) & (XLRA_TXD_BUFLEN >> 8)) << 40)
#define XLRA_TXFBID_64(x)	((UINT64)((x) & (XLRA_TXD_FBID >> 22)) << 54)
#define XLRA_TXADDR_64(x)	((x) & XLRA_TXD_BUFADDR_64)

/* TX free back descriptor */

typedef struct xlra_txfb_desc
    {
    volatile UINT32	xlra_sts;
    volatile UINT32	xlra_bufaddr_lo;
    } XLRA_TXFB_DESC;

#define XLRA_TXFB_EOP		0x80000000
#define XLRA_TXFB_P2P		0x40000000
#define XLRA_TXFB_COLL		0x20000000
#define XLRA_TXFB_BUSERR	0x10000000
#define XLRA_TXFB_UFLOW		0x08000000
#define XLRA_TXFB_ABORT		0x04000000
#define XLRA_TXFB_PORTID	0x03C00000
#define XLRA_TXFB_BUFLEN	0x003FFF00
#define XLRA_TXFB_BUFADDR_HI	0x000000FF
#define XLRA_TXFB_BUFADDR_LO	0xFFFFFFFF

#define XLRA_TXPORT(x)		(((x) & 0x03C00000) >> 22)

#ifdef __cplusplus
}
#endif

#endif /* __INCrmiAccelh */
