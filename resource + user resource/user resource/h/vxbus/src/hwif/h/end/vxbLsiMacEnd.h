/* vxbLsiMacEnd.h - header file for LSI MAC VxBus END driver */

/*
 * Copyright (c) 2010-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,18dec12,clx  enlarge the DMA buffer size to 1536 bytes. (WIND00394781)
01d,15mar11,x_z  added LSI_MAC_BUF_ADRS and LSI_MAC_INC_DESC;
                 removed rxQueTailCopy/txQueTailCopy and added lsiMacRxIdx/
                 lsiMacTxXXX to LSI_MAC_DRV_CTRL for new RX/TX scheme.
01c,10mar11,x_z  removed unused LSI_MAC_MAXFRAG and added lsiMacTxMblk array.
01b,18nov10,x_z  adjusted budffer configuration for better performance.
01a,22mar10,x_z  written based on 01d of templateVxbEnd.h.
*/

#ifndef __INCvxbLsiMacEndh
#define __INCvxbLsiMacEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void lsiMacVxbEndRegister (void);

#ifndef BSP_VERSION

/* driver generic defines */

#define LSI_MAC_NAME                    "lsiMac"
#define LSI_MAC_MTU                     1500
#define LSI_MAC_CLSIZE                  1536
#define LSI_MAC_TIMEOUT                 10000
#define LSI_MAC_TUPLE_CNT               384
#define LSI_MAC_MAX_RX                  32

/* RX/TX/DMA register zone base address(offset to MAC base address) */

#define LSI_MAC_RX_BASE                 0
#define LSI_MAC_TX_BASE                 0x1000
#define LSI_MAC_DMA_BASE                0x2000

/* Fast Ethernet Receive MAC registers */

#define LSI_MAC_FE_RMII_STATUS   (LSI_MAC_RX_BASE + 0x010) /* RMII Status */
#define LSI_MAC_FE_RX_CFG        (LSI_MAC_RX_BASE + 0x04C) /* Config */
#define LSI_MAC_FE_RX_VLAN_CFG   (LSI_MAC_RX_BASE + 0x1D0) /* VLAN Config */

/* Statistics counters */

#define LSI_MAC_FE_RXVLAN_STAT   (LSI_MAC_RX_BASE + 0x270) /* VLAN */
#define LSI_MAC_FE_RXOV_STAT     (LSI_MAC_RX_BASE + 0x278) /* Overflow */
#define LSI_MAC_FE_RXUS_STAT     (LSI_MAC_RX_BASE + 0x280) /* Undersize */
#define LSI_MAC_FE_RX64_STAT     (LSI_MAC_RX_BASE + 0x288) /* 64 */
#define LSI_MAC_FE_RX127_STAT    (LSI_MAC_RX_BASE + 0x290) /* 65 to 127 */
#define LSI_MAC_FE_RX255_STAT    (LSI_MAC_RX_BASE + 0x298) /* 128 to 255 */
#define LSI_MAC_FE_RX511_STAT    (LSI_MAC_RX_BASE + 0x2A0) /* 256 to 511 */
#define LSI_MAC_FE_RX1023_STAT   (LSI_MAC_RX_BASE + 0x2A8) /* 512 to 1023 */
#define LSI_MAC_FE_RXMAX_STAT    (LSI_MAC_RX_BASE + 0x2B0) /* 1024 to Max */
#define LSI_MAC_FE_RXOS_STAT     (LSI_MAC_RX_BASE + 0x2B8) /* Oversize */
#define LSI_MAC_FE_RXOK_STAT     (LSI_MAC_RX_BASE + 0x2C0) /* Packet OK */
#define LSI_MAC_FE_RXCRC_STAT    (LSI_MAC_RX_BASE + 0x2C8) /* CRC Error */
#define LSI_MAC_FE_RXMC_STAT     (LSI_MAC_RX_BASE + 0x2D0) /* Multicast */
#define LSI_MAC_FE_RXBC_STAT     (LSI_MAC_RX_BASE + 0x2D8) /* Broadcast */
#define LSI_MAC_FE_RXMAC_STAT    (LSI_MAC_RX_BASE + 0x2E0) /* MAC Type */
#define LSI_MAC_FE_RXALIGN_STAT  (LSI_MAC_RX_BASE + 0x2E8) /* Align Error */
#define LSI_MAC_FE_RXLEN_LO_STAT (LSI_MAC_RX_BASE + 0x2F0) /* Packet Length */
                                                           /* L [31:0] */
#define LSI_MAC_FE_RXLEN_HI_STAT (LSI_MAC_RX_BASE + 0x2F8) /* Packet Length */
                                                           /* H [63:32] */

/* Global Receive MAC registers */

#define LSI_MAC_GLB_RX_ETH_MODE  (LSI_MAC_RX_BASE + 0x800) /* Ethernet Mode */
#define LSI_MAC_GLB_RX_MAC_RST   (LSI_MAC_RX_BASE + 0x808) /* MAC Soft Reset */
#define LSI_MAC_GLB_RX_MAC_MBIST (LSI_MAC_RX_BASE + 0x810) /* MAC MBIST */

/* Interrupt Receive MAC registers */

#define LSI_MAC_RX_INT_CTRL   (LSI_MAC_RX_BASE + 0xC00) /* MAC Internal */
                                                        /* Interrupt Control */
#define LSI_MAC_RX_EXINT_CTRL (LSI_MAC_RX_BASE + 0xC04) /* MAC External */
                                                        /* Interrupt Control */
#define LSI_MAC_RX_INT_STATUS (LSI_MAC_RX_BASE + 0xC20) /* MAC Interrupt */
                                                        /* Status */

/* Fast Ethernet Transmit MAC registers */

#define LSI_MAC_FE_TX_WM         (LSI_MAC_TX_BASE + 0x018) /* Watermark */
#define LSI_MAC_FE_SWAP_SA2      (LSI_MAC_TX_BASE + 0x020) /* Swap SA 2 */
#define LSI_MAC_FE_SWAP_SA1      (LSI_MAC_TX_BASE + 0x024) /* Swap SA 1 */
#define LSI_MAC_FE_SWAP_SA0      (LSI_MAC_TX_BASE + 0x028) /* Swap SA 0 */
#define LSI_MAC_FE_TX_EXT_CFG    (LSI_MAC_TX_BASE + 0x030) /* Extended Config */
#define LSI_MAC_FE_TX_HD_CFG     (LSI_MAC_TX_BASE + 0x034) /* Half Duplex */
                                                           /* Config */
#define LSI_MAC_FE_TX_CFG        (LSI_MAC_TX_BASE + 0x050) /* Config */
#define LSI_MAC_FE_TM_VAL_CFG    (LSI_MAC_TX_BASE + 0x05C) /* TimeValue */
                                                           /* Config */
#define LSI_MAC_FE_TX_VER        (LSI_MAC_TX_BASE + 0x1C8) /* Version */

/* Statistics counters */

#define LSI_MAC_FE_TXUR_STAT     (LSI_MAC_TX_BASE + 0x300) /* Underrun */
#define LSI_MAC_FE_TXDEF_STAT    (LSI_MAC_TX_BASE + 0x308) /* Deferred */
#define LSI_MAC_FE_TXPAUSE_STAT  (LSI_MAC_TX_BASE + 0x310) /* Pause */
#define LSI_MAC_FE_TXOK_STAT     (LSI_MAC_TX_BASE + 0x318) /* Packet OK */
#define LSI_MAC_FE_TX64_STAT     (LSI_MAC_TX_BASE + 0x320) /* 64 */
#define LSI_MAC_FE_TX127_STAT    (LSI_MAC_TX_BASE + 0x328) /* 65 to 127 */
#define LSI_MAC_FE_TX255_STAT    (LSI_MAC_TX_BASE + 0x330) /* 128 to 255 */
#define LSI_MAC_FE_TX511_STAT    (LSI_MAC_TX_BASE + 0x338) /* 256 to 511 */
#define LSI_MAC_FE_TX1023_STAT   (LSI_MAC_TX_BASE + 0x340) /* 512 to 1023 */
#define LSI_MAC_FE_TXMAX_STAT    (LSI_MAC_TX_BASE + 0x348) /* 1024 to Max */
#define LSI_MAC_FE_TXUS_STAT     (LSI_MAC_TX_BASE + 0x350) /* UnderSize */
#define LSI_MAC_FE_TXLEN_LO_STAT (LSI_MAC_TX_BASE + 0x358) /* Packet Length*/
                                                           /* L [31:0] */
#define LSI_MAC_FE_TXLEN_HI_STAT (LSI_MAC_TX_BASE + 0x360) /* Packet Length*/
                                                           /* H [63:32] */
#define LSI_MAC_FE_TXLC_STAT     (LSI_MAC_TX_BASE + 0x368) /* Late Collision */
#define LSI_MAC_FE_TXEC_STAT     (LSI_MAC_TX_BASE + 0x370) /* Excessive */
                                                           /* Collision */
#define LSI_MAC_FE_TXED_STAT     (LSI_MAC_TX_BASE + 0x378) /* Excessive */
                                                           /* Deferred */
#define LSI_MAC_FE_TXCAW_STAT    (LSI_MAC_TX_BASE + 0x380) /* Collision */
                                                           /* Above Watermark */

/* Global Transmit MAC registers */

#define LSI_MAC_GLB_TX_ETH_MODE  (LSI_MAC_TX_BASE + 0x800) /* Ethernet Mode */
#define LSI_MAC_GLB_TX_MAC_RST   (LSI_MAC_TX_BASE + 0x808) /* MAC Soft Reset */
#define LSI_MAC_GLB_TX_MAC_MBIST (LSI_MAC_TX_BASE + 0x810) /* MAC MBIST */

/* Interrupt Transmit MAC registers */

#define LSI_MAC_TX_INT_CTRL   (LSI_MAC_TX_BASE + 0xC00) /* MAC Internal */
                                                        /* Interrupt Control */
#define LSI_MAC_TX_INT_STATUS (LSI_MAC_TX_BASE + 0xC20) /* MAC Interrupt */
                                                        /* Status */

/* MAC DMA registers */

#define LSI_MAC_DMA_CTRL        (LSI_MAC_DMA_BASE + 0x00) /* Control */
#define LSI_MAC_DMA_ENABLE      (LSI_MAC_DMA_BASE + 0x08) /* Enable */
#define LSI_MAC_DMA_INT_STATUS  (LSI_MAC_DMA_BASE + 0x18) /* Interrupt Status */
#define LSI_MAC_DMA_INT_EN      (LSI_MAC_DMA_BASE + 0x1C) /* Interrupt Enable */
#define LSI_MAC_DMA_RXQ_BA      (LSI_MAC_DMA_BASE + 0x30) /* Rx Queue Base */
#define LSI_MAC_DMA_RXQ_SIZE    (LSI_MAC_DMA_BASE + 0x34) /* Rx Queue Size */
#define LSI_MAC_DMA_TXQ_BA      (LSI_MAC_DMA_BASE + 0x38) /* Tx Queue Base */
#define LSI_MAC_DMA_TXQ_SIZE    (LSI_MAC_DMA_BASE + 0x3C) /* Tx Queue Size */
#define LSI_MAC_DMA_RXQ_TAIL    (LSI_MAC_DMA_BASE + 0x48) /* Rx Tail Pointer */
#define LSI_MAC_DMA_TXQ_TAIL    (LSI_MAC_DMA_BASE + 0x4C) /* Tx Tail Pointer */
#define LSI_MAC_DMA_RXQ_HEAD    (LSI_MAC_DMA_BASE + 0x50) /* Rx Head Pointer */
#define LSI_MAC_DMA_RXQ_TAIL_LC (LSI_MAC_DMA_BASE + 0x54) /* Rx Tail Pointer */
                                                         /* (Local Copy) */
#define LSI_MAC_DMA_TXQ_HEAD    (LSI_MAC_DMA_BASE + 0x58) /* Tx Head Pointer */
#define LSI_MAC_DMA_TXQ_TAIL_LC (LSI_MAC_DMA_BASE + 0x5C) /* Tx Tail Pointer */
                                                         /* (Local Copy) */
#define LSI_MAC_DMA_WM_LO       (LSI_MAC_DMA_BASE + 0x60) /* Low Watermark */
#define LSI_MAC_DMA_WM_HI       (LSI_MAC_DMA_BASE + 0x64) /* High Watermark */

/* bit definitions for Receive RMII status Register */

#define LSI_MAC_RX_FCD                  0x10 /* False Carrier Detect */
#define LSI_MAC_RX_RMII_JABBER          0x08 /* Jabber Error */
#define LSI_MAC_RX_RMII_LINK_UP         0x04 /* Link Up */
#define LSI_MAC_RX_RMII_DUPLEX          0x02 /* Full Duplex */
#define LSI_MAC_RX_RMII_SPEED_100M      0x01 /* 100 MBits */

/* bit definitions for Receive Configuration Register */

#define LSI_MAC_FC_TXEN                 0x8000 /* FlowControl Transmit Enable */
#define LSI_MAC_FC_RXEN                 0x4000 /* FlowControl Receive Enable */
#define LSI_MAC_RX_LINK_UP              0x2000 /* Link up */
#define LSI_MAC_RX_LINK_DOWN            0x0000 /* Link down */
#define LSI_MAC_RX_FD                   0x1000 /* Full Duplex */
#define LSI_MAC_RX_HD                   0x0000 /* Half Duplex */
#define LSI_MAC_RX_SPPED_100M           0x0800 /* 100Mbps Speed for Rx MAC */
#define LSI_MAC_RX_SPPED_10M            0x0000 /* 10Mbps Speed for Rx MAC */
#define LSI_MAC_VLAN_EN                 0x0200 /* VLAN Enable */
#define LSI_MAC_FC_ACP                  0x0040 /* Accept FlowControl */
#define LSI_MAC_RX_ALL                  0x0020 /* Pass All MAC Types */
#define LSI_MAC_RX_STRIPCRC             0x0010 /* Strip CRC */
#define LSI_MAC_RX_JUMBO                0x0008 /* Jumbo(9216B) */
#define LSI_MAC_RX_FILTER_PREAMBLE      0x0004 /* Filter_Pre-enable */
#define LSI_MAC_RX_ANY                  0x0002 /* Pass Any Packet */
                                               /* (valid and invalid)*/
#define LSI_MAC_RX_EN                   0x0001 /* Receive Port Enable */
                                               /* must be set with link up */

/* bit definitions for VLAN Configuration Register */

#define LSI_MAC_VLAN_PRI_MASK           0x7 /* VLAN priority writing */
#define LSI_MAC_VLAN_PRI_SHIFT          12  /* from the CPU */

/* bit definitions for Receive/Transmit Ethernet Mode Register */

#define LSI_MAC_PORT0_GE_MODE           0x0010 /* GE Mode Enable for Port 0 */
#define LSI_MAC_PORT0_FE_MODE           0x0000 /* FE Mode Enable for Port 0 */
#define LSI_MAC_PORT0_ETH_MODE          0x0001 /* Ethernet Mode Enable Port 0 */

/* bit definitions for Receive/Transmit MAC Soft Reset Register */

#define LSI_MAC_SRST_FEMAC0             0x1 /* soft RESET for FEMAC 0 */

/* bit definitions for Receive/Transmit MAC MBIST Register */

#define LSI_MAC_MBIST_EN                0x80000000 /* MBIST Enable */
#define LSI_MAC_MBIST_GO                0x40000000 /* success or failure */
#define LSI_MAC_MBIST_DONE              0x20000000 /* MBIST done flag */
#define LSI_MAC_MBIST_GO_ID_MASK        0x00ffffff /* one-bit flag string for */
                                                   /* module passed/failed */

/* bit definitions for Receive MAC External Interrupt Control Register */

#define LSI_MAC_RX_EXTINT_POL           0x10 /* external interrupt active */
                                             /* high/low for Port 0 */
#define LSI_MAC_RX_EXTINT_EN            0x01 /* external interrupt enable */
                                             /* Valid for GMII/SMII and */
                                             /* controlled by external PHY */

/* bit definitions for Receive MAC Interrupt Status Register */

#define LSI_MAC_RX_EXTINT_STATUS        0x10 /* external interrupt status */
                                             /* for Port 0 */

/* bit definitions for Transmit Watermark Register */

#define LSI_MAC_FE_TX_WM_DTPA_ASSERT    0x8000 /* assert dtpa/backpressure */
#define LSI_MAC_FE_TX_WM_DTPA_DIS       0x4000 /* Disable dtpa/backpressure */
#define LSI_MAC_FE_TX_WM_DTPA_HI_MASK   0x3F00 /* high watermark level */
#define LSI_MAC_FE_TX_WM_DTPA_HI_SHIFT  8
#define LSI_MAC_FE_TX_WM_DTPA_LO_MASK   0x003F /* low watermark level */

#define LSI_MAC_FE_TX_WM_DTPA_DEF_VAL   0x300A

/* bit definitions for Swap SA n Register */

#define LSI_MAC_SWAP_SA_HI_SHIFT        8 /* Swap Source Address High */

/* bit definitions for Transmit Extended Configuration Register (Half-duplex) */

#define LSI_MAC_TXCL_WM_HI_MASK         0xf    /* Transmit Collision */
#define LSI_MAC_TXCL_WM_HI_SHIFT        12     /* Watermark Level */
#define LSI_MAC_EXC_DEF_DROP            0x0200 /* Excessive Deferred Packet */
                                               /* Drop */
#define LSI_MAC_EXC_DEF_JUMBO           0x0100 /* calculate excessive */
                                               /* deference limit*/
#define LSI_MAC_LC_WIN_CNT_MASK         0x00ff /* Late Collision Window Count */
#define LSI_MAC_FE_TX_EXT_CFG_DEF       0x5275 /* reset value */

/* bit definitions for Transmit Half Duplex Configuration Register */

#define LSI_MAC_RANDOM_SEED_MASK        0xf /* Value for BkOffLFSR algorithm */
                                            /* Used only in Half-duplex mode*/
#define LSI_MAC_RANDOM_SEED_DEF         1   /* reset value */

/* bit definitions for Transmit Configuration Register */

#define LSI_MAC_TX_SWAP_SA              0x8000 /* Swap source MAC address */
#define LSI_MAC_TX_LINK_UP              0x2000 /* Link up */
#define LSI_MAC_TX_LINK_DOWN            0x0000 /* Link down */
#define LSI_MAC_TX_FD                   0x1000 /* Full Duplex */
#define LSI_MAC_TX_HD                   0x0000 /* Half Duplex */
#define LSI_MAC_TX_SPPED_100M           0x0800 /* 100Mbps Speed for Tx MAC */
#define LSI_MAC_TX_SPPED_10M            0x0000 /* 10Mbps Speed for Tx MAC */
#define LSI_MAC_IFG_LEN_MASK            0x1f   /* InterFrame Gap Length */
#define LSI_MAC_IFG_LEN_SHIFT           4      /* IFG on Eth = ((n*4)+12) bit */
#define LSI_MAC_APP_CRC_EN              0x0004 /* appending the CRC enable */
#define LSI_MAC_TX_PAD_EN               0x0002 /* padding for Tx Port enable */
#define LSI_MAC_TX_EN                   0x0001 /* Transmit Port Enable */
                                               /* must be set with link up */

#define LSI_MAC_IFG_LEN_DEF             0xF

/* bit definitions for Time Value Configuration Register */

#define LSI_MAC_TM_VAL_MASK             0xffff /* time value of pause packet */
#define LSI_MAC_TM_VAL_DEF              0xffff /* reset value */

/* bit definitions for Transmit Version Register */

#define LSI_MAC_VER_MASK                0xffff /* Version number of the MAC */

/* bit definitions for Transmit MAC Internal Interrupt Control Register */

#define LSI_MAC_TX_INT_POL              0x10 /* Internal interrupt active */
                                             /* high/low for Port 0 */
#define LSI_MAC_TX_INT_EN               0x01 /* Internal interrupt enable */
                                             /* Valid for a packet that */
                                             /* overflows the internal FIFO */

/* bit definitions for Transmit MAC Interrupt Status Register */

#define LSI_MAC_TX_INT                  0x01 /* Internal interrupt status */
                                             /* for Port 0 */

/* bit definitions for MAC DMA Control Register */

#define LSI_MAC_DMA_MASTER_RST          0x80000000 /* MAC DMA Master Reset */
#define LSI_MAC_DMA_ERR_RST             0x40000000 /* MAC DMA Error Reset */
#define LSI_MAC_DMA_EN                  0x00010000 /* MAC DMA Enable */

#define LSI_MAC_ARB_RR                  0x00000000 /* MAC Arbitration Control */
#define LSI_MAC_ARB_RX                  0x00000001 /* 00 or 11= Round Robin */
#define LSI_MAC_ARB_TX                  0x00000002 /* 01 = RX priority */
                                                   /* 10 = TX priority */

/* bit definitions for MAC DMA Enable Register */

#define LSI_MAC_DMA_RX_EN               0x00020000 /* MAC DMA Receive Enable */
#define LSI_MAC_DMA_TX_EN               0x00010000 /* MAC DMA Transmit Enable */

/* bit definitions for MAC DMA Interrupt Status/Enable Register */

#define LSI_MAC_DMA_RX_INT              0x00000002 /* MAC DMA Rx Interrupt */
#define LSI_MAC_DMA_TX_INT              0x00000001 /* MAC DMA Tx Interrupt */

/* bit definitions for MAC DMA Rx/Tx Queue Size Register */

#define LSI_MAC_DMA_RX_PERFORMANCE      0x80000000 /* Disable performance bug */
                                                   /* fix for Rx */
#define LSI_MAC_DMA_QUE_SIZE_MASK       0x3f /* Disable performance bug */

/* bit definitions for MAC DMA Rx/Tx Head/Tail(Local Copy) Pointer Register */

#define LSI_MAC_DMA_QUEUE_TOGGLE        0x00100000 /* Toggle when Pointer */
                                                   /* rolls over from 0xFFFFF */
                                                   /* to 0. */
#define LSI_MAC_DMA_QUEUE_POINTER_MASK  0x000fffff /* Tx/Rx Head/Tail Pointer */

/* bit definitions for MAC DMA Low Watermark Register */

#define LSI_MAC_BP_CNT_VAL_MASK             0xff /* MAC backpressure Counter */
#define LSI_MAC_BP_CNT_VAL_SHIFT            16   /* Value */

#define LSI_MAC_DMA_WM_LO_DEF               0x280044 /* reset value */

/* bit definitions for MAC DMA High Watermark Register */

#define LSI_MAC_DMA_WM_HI_DEF               0xC0       /* reset value */

/* DMA descriptors definitions */

typedef struct lsiMacDmaDesc
    {
    volatile UINT32 flag;
    volatile UINT32 len;
    volatile UINT32 targetAdrs; /* used for CBI ring DMA */
    volatile UINT32 hostAdrs;
    } LSI_MAC_DMA_DESC;

typedef union {
    UINT32 val;

    struct
        {
        volatile UINT32 res     : 11;
        volatile UINT32 toggle  : 1;
        volatile UINT32 pointer : 20;
        } field;
}LSI_MAC_DMA_QUE;

/*
 * The granularity of DMA Queue Size is 1K Byte, and the size of DMA descriptor
 * is 16, so the descriptor count must be a multiple of 64, and it also must not
 * be greater than 65536..
 */

#define LSI_MAC_DMA_DESC_ALIGN          64
#define LSI_MAC_DMA_RX_DESC_CNT         512
#define LSI_MAC_DMA_TX_DESC_CNT         256

/*
 * The DMA buffer length must be aligned with 64 bytes,and must be less than
 * 0xFFFF, so the range is 0x40(64) - 0xFFC0(65472).
 *
 * NOTE: The start address of the DMA buffer must be aligned with 64 bytes.
 */

#define LSI_MAC_DMA_BUF_ALIGN           64
#define LSI_MAC_DMA_BUF_LEN             LSI_MAC_CLSIZE

#define LSI_MAC_DMA_RX_QUE_SIZE         \
    (LSI_MAC_DMA_RX_DESC_CNT * (sizeof (LSI_MAC_DMA_DESC)))
#define LSI_MAC_DMA_RX_QUE_SIZE_KB      \
    (LSI_MAC_DMA_RX_QUE_SIZE / 1024)

#define LSI_MAC_DMA_TX_QUE_SIZE         \
    (LSI_MAC_DMA_TX_DESC_CNT * (sizeof (LSI_MAC_DMA_DESC)))
#define LSI_MAC_DMA_TX_QUE_SIZE_KB      \
    (LSI_MAC_DMA_TX_QUE_SIZE / 1024)

#define LSI_MAC_BUF_ADRS(x, y)          ((x) + (y) * LSI_MAC_DMA_BUF_LEN)
#define LSI_MAC_INC_DESC(x, y)          (x) = ((x + 1) & (y - 1))
#define LSI_MAC_ADJ(m)                  (m)->m_data += 2

/* bit definitions for flag field */

#define LSI_MAC_DMA_DESC_TYPE_FILL          0x00000000 /* Fill DMA */
#define LSI_MAC_DMA_DESC_TYPE_BLK           0x00000001 /* Block DMA */
#define LSI_MAC_DMA_DESC_TYPE_SCATTER       0x00000002 /* Scatter DMA */
#define LSI_MAC_DMA_DESC_WR                 0x00000004 /* Write */
#define LSI_MAC_DMA_DESC_RD                 0x00000000 /* Read */
#define LSI_MAC_DMA_DESC_SOP                0x00000008 /* Start of Packet */
#define LSI_MAC_DMA_DESC_EOP                0x00000010 /* End of Packet */
#define LSI_MAC_DMA_DESC_INT                0x00000020 /* Interrupt Enable */
#define LSI_MAC_DMA_DESC_RX_ERR             0x00000040 /* Receive PDU Error */
#define LSI_MAC_DMA_DESC_SWAP               0x00000080 /* Byte swap enable */

/* bit definitions for len field */

#define LSI_MAC_DMA_DESC_DATA_LEN_MASK      0xffff /* Data Transfer  */
                                                   /* Length */
#define LSI_MAC_DMA_DESC_PDU_LEN_MASK       0xffff /* PDU Length  */
#define LSI_MAC_DMA_DESC_PDU_LEN_SHIFT      16

/* Private adapter context structure */

typedef struct lsiMacDrvCtrl
    {
    END_OBJ                     lsiMacEndObj;
    VXB_DEVICE_ID               lsiMacDev;
    void *                      lsiMacHandle;
    void *                      lsiMacBar;
    void *                      lsiMacMuxDevCookie;

    JOB_QUEUE_ID                lsiMacJobQueue;

    QJOB                        lsiMacRxJob;
    atomic_t                    lsiMacRxPending;

    QJOB                        lsiMacTxJob;
    atomic_t                    lsiMacTxPending;
    atomic_t                    lsiMacTxStall;

    BOOL                        lsiMacPolling;
    UINT16                      lsiMacIntMask;

    UINT8                       lsiMacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES            lsiMacCaps;

    END_IFDRVCONF               lsiMacEndStatsConf;
    END_IFCOUNTERS              lsiMacEndStatsCounters;

    SEM_ID                      lsiMacDevSem;

    UINT32                      lsiMacInErrors;
    UINT32                      lsiMacInDiscards;
    UINT32                      lsiMacInUcasts;
    UINT32                      lsiMacInMcasts;
    UINT32                      lsiMacInBcasts;
    UINT32                      lsiMacInOctets;
    UINT32                      lsiMacOutErrors;
    UINT32                      lsiMacOutUcasts;
    UINT32                      lsiMacOutMcasts;
    UINT32                      lsiMacOutBcasts;
    UINT32                      lsiMacOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *             lsiMacMediaList;
    END_ERR                     lsiMacLastError;
    UINT32                      lsiMacCurMedia;
    UINT32                      lsiMacCurStatus;
    VXB_DEVICE_ID               lsiMacMiiBus;
    VXB_DEVICE_ID               lsiMacMiiDev;
    FUNCPTR                     lsiMacMiiPhyRead;
    FUNCPTR                     lsiMacMiiPhyWrite;
    int                         lsiMacMiiPhyAddr;

    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */

    LSI_MAC_DMA_QUE             rxQueHead;
    LSI_MAC_DMA_QUE             txQueHead;
    volatile LSI_MAC_DMA_QUE *  pRxQueTail;
    volatile LSI_MAC_DMA_QUE *  pTxQueTail;

    VXB_DMA_TAG_ID              lsiMacParentTag;

    VXB_DMA_TAG_ID              lsiMacDmaTailTag;
    VXB_DMA_MAP_ID              lsiMacDmaTailMap;
    LSI_MAC_DMA_QUE *           lsiMacDmaTailMem;

    VXB_DMA_TAG_ID              lsiMacDmaRxDescTag;
    VXB_DMA_MAP_ID              lsiMacDmaRxDescMap;
    LSI_MAC_DMA_DESC *          lsiMacDmaRxDescMem;

    VXB_DMA_TAG_ID              lsiMacDmaRxBufTag;
    VXB_DMA_MAP_ID              lsiMacDmaRxBufMap;
    char *                      lsiMacDmaRxBufMem;
    char *                      lsiMacDmaRxBufMemPhy;
    M_BLK_ID                    lsiMacTxMblk[LSI_MAC_DMA_TX_DESC_CNT];

    VXB_DMA_TAG_ID              lsiMacDmaTxDescTag;
    VXB_DMA_MAP_ID              lsiMacDmaTxDescMap;
    LSI_MAC_DMA_DESC *          lsiMacDmaTxDescMem;

    VXB_DMA_TAG_ID              lsiMacDmaTxBufTag;
    VXB_DMA_MAP_ID              lsiMacDmaTxBufMap;
    char *                      lsiMacDmaTxBufMem;
    char *                      lsiMacDmaTxBufMemPhy;

    UINT32                      lsiMacRxIdx;
    UINT32                      lsiMacTxProd;
    UINT32                      lsiMacTxCons;
    UINT32                      lsiMacTxFree;

    int                         lsiMacMaxMtu;
    } LSI_MAC_DRV_CTRL;

#define LSI_MAC_BAR(p)     ((LSI_MAC_DRV_CTRL *)(p)->pDrvCtrl)->lsiMacBar
#define LSI_MAC_HANDLE(p)  ((LSI_MAC_DRV_CTRL *)(p)->pDrvCtrl)->lsiMacHandle

#define CSR_READ_4(pDev, addr)              \
    vxbRead32(LSI_MAC_HANDLE (pDev),        \
        (UINT32 *) ((char *) LSI_MAC_BAR (pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)       \
    vxbWrite32(LSI_MAC_HANDLE (pDev),       \
        (UINT32 *) ((char *) LSI_MAC_BAR (pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)     \
    CSR_WRITE_4(pDev, offset, CSR_READ_4 (pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)     \
    CSR_WRITE_4(pDev, offset, CSR_READ_4 (pDev, offset) & (~(val)))

#define DESC_READ_4(pDev, addr)             \
    vxbRead32(LSI_MAC_HANDLE (pDev), (UINT32 *) (addr))

#define DESC_WRITE_4(pDev, addr, data)      \
    vxbWrite32(LSI_MAC_HANDLE (pDev), (UINT32 *) (addr), data)

#define DESC_SETBIT_4(pDev, offset, val)    \
    DESC_WRITE_4(pDev, offset, DESC_READ_4 (pDev, offset) | (val))
#define DESC_CLRBIT_4(pDev, offset, val)    \
    DESC_WRITE_4(pDev, offset, DESC_READ_4 (pDev, offset) & (~(val)))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbLsiMacEndh */
