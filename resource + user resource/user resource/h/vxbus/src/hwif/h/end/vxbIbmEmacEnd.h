/* vxbIbmEmacEnd.h - header file for IBM/AMCC EMAC VxBus END driver */

/*
 * Copyright (c) 2008-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,26may09,x_z  add support for PPC440EPx EMAC.
01b,03jul08,b_m  add support for PPC405EX EMAC.
01a,15jan08,wap  written
*/

#ifndef __INCvxbIbmEmacEndh
#define __INCvxbIbmEmacEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbEmacRegister (void);

#define EMAC_TYPE_DUAL		1
#define EMAC_TYPE_QUAD		2
#define EMAC_TYPE_405EX		3
#define EMAC_TYPE_440EPX	4

#define EMAC_TYPE_405GP		EMAC_TYPE_DUAL
#define EMAC_TYPE_440GP		EMAC_TYPE_DUAL
#define EMAC_TYPE_440EP		EMAC_TYPE_DUAL
#define EMAC_TYPE_440GX		EMAC_TYPE_QUAD

#ifndef BSP_VERSION

#define EMAC_MR0		0x0000	/* Mode register 0 */
#define EMAC_MR1		0x0004	/* Mode register 1 */
#define EMAC_TMR0		0x0008	/* TX mode register 0 */
#define EMAC_TMR1		0x000C	/* TX mode register 1 */
#define EMAC_RMR		0x0010	/* RX mode register */
#define EMAC_ISR		0x0014	/* Interrupt status register */
#define EMAC_ISER		0x0018	/* Interrupt enable register */
#define EMAC_IAHR		0x001C	/* Individual address high */
#define EMAC_IALR		0x0020	/* Individual address low */
#define EMAC_VTPID		0x0024	/* VLAN TPID */
#define EMAC_VTCI		0x0028	/* VLAN TCP */
#define EMAC_PTR		0x002C	/* Pause timer */
#define EMAC_IAHT0		0x0030	/* Individual address hash 0 */
#define EMAC_IAHT1		0x0034	/* Individual address hash 1 */
#define EMAC_IAHT2		0x0038	/* Individual address hash 2 */
#define EMAC_IAHT3		0x003C	/* Individual address hash 3 */
#define EMAC_GAHT0		0x0040	/* Multicast address hash 0 */
#define EMAC_GAHT1		0x0044	/* Multicast address hash 1 */
#define EMAC_GAHT2		0x0048	/* Multicast address hash 2 */
#define EMAC_GAHT3		0x004C	/* Multicast address hash 3 */
#define EMAC_LSAH		0x0050	/* Last source address high */
#define EMAC_LSAL		0x0054	/* Last source address low */
#define EMAC_IPGVR		0x0058	/* Inter-packet gap value */
#define EMAC_STACR		0x005C	/* STA control register */
#define EMAC_TRTR		0x0060	/* Transmit request threshold */
#define EMAC_RWMR		0x0064	/* RX low/high watermark */
#define EMAC_OCTX		0x0068	/* Octets transmitted counter */
#define EMAC_OCRX		0x006C	/* Octets received counter */

/* PPC405EX EMAC has new IAH/GAH register offset from 0x80 */

#define EMAC_405EX_IAHT0	0x0080	/* Individual address hash 0 */
#define EMAC_405EX_IAHT1	0x0084	/* Individual address hash 1 */
#define EMAC_405EX_IAHT2	0x0088	/* Individual address hash 2 */
#define EMAC_405EX_IAHT3	0x008C	/* Individual address hash 3 */
#define EMAC_405EX_IAHT4	0x0090	/* Individual address hash 4 */
#define EMAC_405EX_IAHT5	0x0094	/* Individual address hash 5 */
#define EMAC_405EX_IAHT6	0x0098	/* Individual address hash 6 */
#define EMAC_405EX_IAHT7	0x009C	/* Individual address hash 7 */
#define EMAC_405EX_GAHT0	0x00A0	/* Multicast address hash 0 */
#define EMAC_405EX_GAHT1	0x00A4	/* Multicast address hash 1 */
#define EMAC_405EX_GAHT2	0x00A8	/* Multicast address hash 2 */
#define EMAC_405EX_GAHT3	0x00AC	/* Multicast address hash 3 */
#define EMAC_405EX_GAHT4	0x00B0	/* Multicast address hash 4 */
#define EMAC_405EX_GAHT5	0x00B4	/* Multicast address hash 5 */
#define EMAC_405EX_GAHT6	0x00B8	/* Multicast address hash 6 */
#define EMAC_405EX_GAHT7	0x00BC	/* Multicast address hash 7 */

/* Mode register 0 */

#define EMAC_MR0_RXI		0x80000000	/* RX idle */
#define EMAC_MR0_TXI		0x40000000	/* TX idle */
#define EMAC_MR0_SRST		0x20000000	/* soft reset */
#define EMAC_MR0_TXE		0x10000000	/* TX enable */
#define EMAC_MR0_RXE		0x08000000	/* RX enable */
#define EMAC_MR0_WKE		0x04000000	/* wakeup enable */

/* Mode register 1 */

#define EMAC_MR1_FDE		0x80000000	/* full duplex enable */
#define EMAC_MR1_ILE		0x40000000	/* internal loopback enable */
#define EMAC_MR1_VLE		0x20000000	/* VLAN enable */
#define EMAC_MR1_EIFC		0x10000000	/* integrated flow control */
#define EMAC_MR1_APP		0x08000000	/* allow pause packet */
#define EMAC_MR1_IST		0x01000000	/* Ignore SQE test */
#define EMAC_MR1_MF		0x00C00000	/* medium freq. (10/100) */
#define EMAC_MR1_RFS		0x00300000	/* RX fifo size */
#define EMAC_MR1_TFS		0x000C0000	/* TX fifo size */
#define EMAC_MR1_TR0		0x00018000	/* TX request on queue 0 */
#define EMAC_MR1_TR1		0x00006000	/* TX request on queue 1 */
#define EMAC_MR1_JE		0x00001000	/* Jumbo enable */

#define EMAC_MF_10MBPS		0x00000000
#define EMAC_MF_100MBPS		0x00400000
#define EMAC_MF_1000MBPS	0x00800000
#define EMAC_MF_1000GPCS	0x00C00000

#define EMAC_RFS_512B		0x00000000
#define EMAC_RFS_1KB		0x00100000
#define EMAC_RFS_2KB		0x00200000
#define EMAC_RFS_4KB		0x00300000

#define EMAC_TFS_1KB		0x00040000
#define EMAC_TFS_2KB		0x00080000

#define EMAC_TR0_SINGLE		0x00000000
#define EMAC_TR0_MULTI		0x00008000
#define EMAC_TR0_DEPENDENT	0x00010000

#define EMAC_TR1_SINGLE		0x00000000
#define EMAC_TR1_MULTI		0x00002000
#define EMAC_TR1_DEPENDENT	0x00004000

/* Mode register 1 for gigE parts */

#define EMAC_GXMR1_FDE		0x80000000	/* full duplex enable */
#define EMAC_GXMR1_ILE		0x40000000	/* internal loopback enable */
#define EMAC_GXMR1_VLE		0x20000000	/* VLAN enable */
#define EMAC_GXMR1_EIFC		0x10000000	/* integrated flow control */
#define EMAC_GXMR1_APP		0x08000000	/* allow pause packet */
#define EMAC_GXMR1_IST		0x01000000	/* Ignore SQE test */
#define EMAC_GXMR1_MF		0x00C00000	/* medium freq. (10/100) */
#define EMAC_GXMR1_RFS		0x00380000	/* RX fifo size */
#define EMAC_GXMR1_TFS		0x00070000	/* TX fifo size */
#define EMAC_GXMR1_TR		0x00008000	/* TX request on queue 0 */
#define EMAC_GXMR1_MWSW		0x00007000	/* TX request on queue 1 */
#define EMAC_GXMR1_JE		0x00000800	/* Jumbo enable */
#define EMAC_GXMR1_IPPA		0x000007C0	/* Internal PCS PHY addr */
#define EMAC_GXMR1_OBCI		0x00000038	/* OPB bus clock indication */

#define EMAC_GXMF_10MBPS	0x00000000
#define EMAC_GXMF_100MBPS	0x00400000
#define EMAC_GXMF_1000MBPS	0x00800000
#define EMAC_GXMF_1000GPCS	0x00C00000

#define EMAC_GXRFS_512B		0x00000000
#define EMAC_GXRFS_1KB		0x00080000
#define EMAC_GXRFS_2KB		0x00100000
#define EMAC_GXRFS_4KB		0x00180000
#define EMAC_GXRFS_8KB		0x00200000
#define EMAC_GXRFS_16KB		0x00280000

#define EMAC_GXTFS_512B		0x00000000
#define EMAC_GXTFS_1KB		0x00010000
#define EMAC_GXTFS_2KB		0x00020000
#define EMAC_GXTFS_4KB		0x00030000
#define EMAC_GXTFS_8KB		0x00040000
#define EMAC_GXTFS_16KB		0x00050000

#define EMAC_GXTR_SINGLE	0x00000000
#define EMAC_GXTR_MULTI		0x00008000

#define EMAC_GXMWSW_NONE	0x00000000
#define EMAC_GXMWSW_ONE		0x00001000

/* TX Mode control register 0 */

#define EMAC_TMR0_GNP0		0x80000000	/* Get new packet chan 0 */
#define EMAC_TMR0_GNP1		0x40000000	/* Get new packet chan 1 */
#define EMAC_TMR0_GNPD		0x20000000	/* Get new packet dep mode */
#define EMAC_TMR0_FC		0x10000000	/* First channel (dep mode) */

/* TX Mode control register 1 */

#define EMAC_TMR1_TLR		0xF8000000	/* TX low request */
#define EMAC_TMR1_TUR		0x00FF0000	/* TX urgent request */

#define EMAC_TLR(x)		((x) << 27)
#define EMAC_TUR(x)		((x) << 16)
#define EMAC_TURGX(x)		((x) << 14)

/* RX Mode control register */

#define EMAC_RMR_SP		0x80000000	/* Strip padding + FCS */
#define EMAC_RMR_SFCS		0x40000000	/* Strip FCS */
#define EMAC_RMR_RRP		0x20000000	/* Allow runt frames */
#define EMAC_RMR_RFP		0x10000000	/* Allow bad FCS frames */
#define EMAC_RMR_ROP		0x08000000	/* Allow giant frames */
#define EMAC_RMR_RPIR		0x04000000	/* Allow in range err frames */
#define EMAC_RMR_PPP		0x02000000	/* Propagate pause packet */
#define EMAC_RMR_PME		0x01000000	/* Promisc mode enable */
#define EMAC_RMR_PMME		0x00800000	/* Allmulticast enable */
#define EMAC_RMR_IAE		0x00400000	/* Unicast enable */
#define EMAC_RMR_MIAE		0x00200000	/* Mulitiple unicast enable */
#define EMAC_RMR_BAE		0x00100000	/* Broadcast enable */
#define EMAC_RMR_MAE		0x00080000	/* Multicast enable */

/* Interrupt status register */

#define EMAC_ISR_TXPE		0x20000000	/* TX parity error */
#define EMAC_ISR_RXPE		0x10000000	/* RX parity error */
#define EMAC_ISR_TXUE		0x08000000	/* TX underrun event */
#define EMAC_ISR_RXOE		0x04000000	/* RX overrun event */
#define EMAC_ISR_OVR		0x02000000	/* RX overrun */
#define EMAC_ISR_PP		0x01000000	/* Pause packet */
#define EMAC_ISR_BP		0x00800000	/* Bad packet */
#define EMAC_ISR_RP		0x00400000	/* Runt packet */
#define EMAC_ISR_SE		0x00200000	/* Short event */
#define EMAC_ISR_ALE		0x00100000	/* Alignment error */
#define EMAC_ISR_BFCS		0x00080000	/* Bad FCS */
#define EMAC_ISR_PTLE		0x00040000	/* Packet too long (giant) */
#define EMAC_ISR_ORE		0x00020000	/* Out of range error */
#define EMAC_ISR_IRE		0x00010000	/* In range error */
#define EMAC_ISR_DBDM		0x00000200	/* Dead bit, dependent mode */
#define EMAC_ISR_DB0		0x00000100	/* Dead bit (SQE error) q0 */
#define EMAC_ISR_SE0		0x00000080	/* Signal quality error q0 */
#define EMAC_ISR_TE0		0x00000040	/* TX error (aborted) q0 */
#define EMAC_ISR_DB1		0x00000020	/* Dead bit (SQE error) q0 */
#define EMAC_ISR_SE1		0x00000010	/* Signal quality error q1 */
#define EMAC_ISR_TE1		0x00000008	/* TX error (aborted) q1 */
#define EMAC_ISR_MOS		0x00000002	/* MDIO transfer succeeded */
#define EMAC_ISR_MOF		0x00000001	/* MDIO transfer failed */

/* Interrupt status enable register */

#define EMAC_ISER_TXPE		0x20000000	/* TX parity error */
#define EMAC_ISER_RXPE		0x10000000	/* RX parity error */
#define EMAC_ISER_TXUE		0x08000000	/* TX underrun event */
#define EMAC_ISER_RXOE		0x04000000	/* RX overrun event */
#define EMAC_ISER_OVR		0x02000000	/* RX overrun */
#define EMAC_ISER_PP		0x01000000	/* Pause packet */
#define EMAC_ISER_BP		0x00800000	/* Bad packet */
#define EMAC_ISER_RP		0x00400000	/* Runt packet */
#define EMAC_ISER_SE		0x00200000	/* Short event */
#define EMAC_ISER_ALE		0x00100000	/* Alignment error */
#define EMAC_ISER_BFCS		0x00080000	/* Bad FCS */
#define EMAC_ISER_PTLE		0x00040000	/* Packet too long (giant) */
#define EMAC_ISER_ORE		0x00020000	/* Out of range error */
#define EMAC_ISER_ISR		0x00040000	/* In range error */
#define EMAC_ISER_DBDM		0x00000200	/* Dead bit, dependent mode */
#define EMAC_ISER_DB0		0x00000100	/* Dead bit (SQE error) q0 */
#define EMAC_ISER_SE0		0x00000080	/* Signal quality error q0 */
#define EMAC_ISER_TE0		0x00000040	/* TX error (aborted) q0 */
#define EMAC_ISER_DB1		0x00000020	/* Dead bit (SQE error) q0 */
#define EMAC_ISER_SE1		0x00000010	/* Signal quality error q1 */
#define EMAC_ISER_TE1		0x00000008	/* TX error (aborted) q1 */
#define EMAC_ISER_MOS		0x00000002	/* MDIO transfer succeeded */
#define EMAC_ISER_MOF		0x00000001	/* MDIO transfer failed */

/* STA control register */

#define EMAC_STACR_DATA		0xFFFF0000
#define EMAC_STACR_DONE		0x00008000
#define EMAC_STACR_ERROR	0x00004000
#define EMAC_STACR_CMD		0x00003000
#define EMAC_STACR_FREQ		0x00000C00
#define EMAC_STACR_PHYADDR	0x000003E0
#define EMAC_STACR_PHYREG	0x0000001F

#define EMAC_STACR_CMD_READ	0x00001000
#define EMAC_STACR_CMD_WRITE	0x00002000
#define EMAC_405EX_STACR_CMD_WRITE	0x00000800

#define EMAC_STACR_FREQ_50MHZ	0x00000000
#define EMAC_STACR_FREQ_66MHZ	0x00000400
#define EMAC_STACR_FREQ_83MHZ	0x00000800
#define EMAC_STACR_FREQ_100MHZ	0x00000C00

#define EMAC_PHYREG(x)		((x) & EMAC_STACR_PHYREG)
#define EMAC_PHYADDR(x)		(((x) << 5) & EMAC_STACR_PHYADDR)

/* TX request threshold */

#define EMAC_TRTR_TRT		0xF8000000

#define EMAC_TXTHRESH(x)	(((x) >> 6) << 27)

/* TX request threshold for GX parts */

#define EMAC_GXTRTR_TRT		0xFF000000

#define EMAC_GXTXTHRESH(x)	(((x) >> 6) << 24)


/* RX Low/High watermark register */

#define EMAC_RWMR_RLWM		0xFF000000	/* TX low request */
#define EMAC_RWMR_RHWM		0x0000FF00	/* TX urgent request */

#define EMAC_RLWM(x)		((x) << 23)
#define EMAC_RHWM(x)		((x) << 7)

#define EMAC_RLWMGX(x)		((x) << 22)
#define EMAC_RHWMGX(x)		((x) << 6)

/* EMAC specific ctrl/sts bits in MAL descriptors */

/* Note: TCP acceleration features (TAH) available in 10/100/1000 EMACs only */

#define EMAC_RXSTS_OVR		0x0200	/* RX overrun error */
#define EMAC_RXSTS_PP		0x0100	/* Pause packet */
#define EMAC_RXSTS_BP		0x0080	/* Bad packet */
#define EMAC_RXSTS_RP		0x0040	/* Runt packet */
#define EMAC_RXSTS_SE		0x0020	/* Short event */
#define EMAC_RXSTS_ALE		0x0010	/* Alignment error */
#define EMAC_RXSTS_BFCS		0x0008	/* Bad FCS */
#define EMAC_RXSTS_PTLE		0x0004	/* Packet too long error */
#define EMAC_RXSTS_ORE		0x0002	/* Out of range error */
#define EMAC_RXSTS_IRE		0x0001	/* In range error */
#define EMAC_RXSTS_CSUM		0x0003	/* TCP/IP csum verification failed */

#define EMAC_TXCTL_FCS		0x0200	/* Generate FCS */
#define EMAC_TXCTL_PAD		0x0100	/* Pad short frames */
#define EMAC_TXCTL_ISA		0x0080	/* Insert source address */
#define EMAC_TXCTL_RSA		0x0040	/* Replace source address */
#define EMAC_TXCTL_IVL		0x0020	/* Insert VLAN tag */
#define EMAC_TXCTL_RVL		0x0010	/* Insert VLAN tag */
#define EMAC_TXCTL_TAH		0x000E	/* TCP hardware acceleration */

#define EMAC_TXCTL_TAH_NONE	0x0000	/* TAH disabled */
#define EMAC_TXCTL_TAH_SSR0	0x0002	/* TCP/IP csum+TSO using SRR0 */
#define EMAC_TXCTL_TAH_SSR1	0x0004	/* TCP/IP csum+TSO using SRR1 */
#define EMAC_TXCTL_TAH_SSR2	0x0006	/* TCP/IP csum+TSO using SRR2 */
#define EMAC_TXCTL_TAH_SSR3	0x0008	/* TCP/IP csum+TSO using SRR3 */
#define EMAC_TXCTL_TAH_SSR4	0x000A	/* TCP/IP csum+TSO using SRR4 */
#define EMAC_TXCTL_TAH_SSR5	0x000C	/* TCP/IP csum+TSO using SRR5 */
#define EMAC_TXCTL_TAH_CSUM	0x000E	/* TCP/IP csum generation only */

#define EMAC_TXSTS_BFCS		0x0200	/* Bad FCS */
#define EMAC_TXSTS_BPD		0x0100	/* Bad previous frame in dep mode */
#define EMAC_TXSTS_LCS		0x0080	/* Lost carrier sense */
#define EMAC_TXSTS_EXD		0x0040	/* Excessive deferral */
#define EMAC_TXSTS_EXC		0x0020	/* Exessive collisions */
#define EMAC_TXSTS_LCL		0x0010	/* Late collision */
#define EMAC_TXSTS_MCL		0x0008	/* Multiple collision */
#define EMAC_TXSTS_SCL		0x0004	/* Single collision */
#define EMAC_TXSTS_UR		0x0002	/* Underrun */
#define EMAC_TXSTS_SQE		0x0001	/* Signal quality error */


#define EMAC_MTU	1500
#define EMAC_CLSIZE	1536

#define EMAC_NAME	"emac"
#define EMAC_TIMEOUT 10000

#define EMAC_INTRS (EMAC_RXINTRS|EMAC_TXINTRS)

#define EMAC_RXINTRS	\
    (EMAC_ISR_OVR|EMAC_ISR_RXOE|EMAC_ISR_RXPE|EMAC_ISR_BP|EMAC_ISR_RP|	\
     EMAC_ISR_SE|EMAC_ISR_ALE|EMAC_ISR_BFCS|EMAC_ISR_PTLE|EMAC_ISR_ORE|	\
     EMAC_ISR_IRE)

#define EMAC_TXINTRS	\
    (EMAC_ISR_DBDM|EMAC_ISR_DB0|EMAC_ISR_SE0|EMAC_ISR_TE0|EMAC_ISR_DB1|	\
     EMAC_ISR_SE1|EMAC_ISR_TE1)

#define EMAC_RX_DESC_CNT	MAL_RX_DESC_CNT
#define EMAC_TX_DESC_CNT	MAL_TX_DESC_CNT
#define EMAC_MAXFRAG		16
#define EMAC_MAX_RX		32
#define EMAC_INTR_SKIP		0xF

#define EMAC_INC_DESC(x, y)      (x) = ((x + 1) & (y - 1))

/*
 * Private adapter context structure.
 */

typedef struct emac_drv_ctrl
    {
    END_OBJ		emacEndObj;
    VXB_DEVICE_ID	emacDev;
    void *		emacMuxDevCookie;
    void *		emacBar;
    void *		emacHandle;

    JOB_QUEUE_ID	emacJobQueue;
    QJOB		emacIntJob;
    atomic_t		emacIntPending;

    QJOB		emacRxJob;
    atomic_t		emacRxPending;

    QJOB		emacTxJob;
    atomic_t		emacTxPending;
    volatile BOOL	emacTxStall;

    UINT32              emacTxProd;
    UINT32              emacTxCons;
    UINT32              emacTxFree;
    UINT32              emacRxIdx;

    BOOL		emacPolling;
    M_BLK_ID		emacPollBuf;
    UINT32		emacIntMask;
    UINT32		emacIntrs;

    UINT8		emacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	emacCaps;

    END_IFDRVCONF	emacEndStatsConf;
    END_IFCOUNTERS	emacEndStatsCounters;
    UINT32              emacInErrors;
    UINT32              emacInDiscards;
    UINT32              emacInUcasts;
    UINT32              emacInMcasts;
    UINT32              emacInBcasts;
    UINT32              emacInOctets;
    UINT32              emacOutErrors;
    UINT32              emacOutUcasts;
    UINT32              emacOutMcasts;
    UINT32              emacOutBcasts;
    UINT32              emacOutOctets;

    SEM_ID		emacDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*emacMediaList;
    END_ERR		emacLastError;
    UINT32		emacCurMedia;
    UINT32		emacCurStatus;
    VXB_DEVICE_ID	emacMiiBus;
    VXB_DEVICE_ID       emacMiiDev;
    FUNCPTR             emacMiiPhyRead;
    FUNCPTR             emacMiiPhyWrite;
    int                 emacMiiPhyAddr;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */

    VXB_DMA_TAG_ID	emacParentTag;

    MAL_DESC *		emacRxDescMem;
    MAL_DESC *		emacTxDescMem;

    VXB_DMA_TAG_ID      emacMblkTag;

    VXB_DMA_MAP_ID      emacRxMblkMap[EMAC_RX_DESC_CNT];
    VXB_DMA_MAP_ID      emacTxMblkMap[EMAC_TX_DESC_CNT];

    M_BLK_ID            emacRxMblk[EMAC_RX_DESC_CNT];
    M_BLK_ID            emacTxMblk[EMAC_TX_DESC_CNT];

    VXB_DMA_RESOURCE_ID	emacRxChan;
    VXB_DMA_RESOURCE_ID	emacTxChan;

    int			emacMaxMtu;

    int			emacType;
#ifdef EMAC_TX_MODERATION
    WDOG_ID		emacTxWd;
#endif

    int			emacNum;
    int			emacRgmiiPort;
    } EMAC_DRV_CTRL;


#define EMAC_BAR(p)   ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->emacBar
#define EMAC_HANDLE(p)   ((EMAC_DRV_CTRL *)(p)->pDrvCtrl)->emacHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (EMAC_HANDLE(pDev), (UINT32 *)((char *)EMAC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (EMAC_HANDLE(pDev),                             \
        (UINT32 *)((char *)EMAC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIbmEmacEndh */
