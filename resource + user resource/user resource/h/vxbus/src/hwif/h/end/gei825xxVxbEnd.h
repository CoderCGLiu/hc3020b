/* gei825xxVxbEnd.h - header file for gei VxBus END driver */

/*
 * Copyright (c) 2006-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
13oct15,wyt  Add Receive Filter Control Register. (VXW6-84799)
25jun14,p_x  Support parameter tupleCntFactor. (VXW6-7598)
05may14,p_x  Add support for I218 devices. (VXW6-81834)
10apr14,xms  Fix couldn't detect correct MAC address for I210. (VXW6-7439)
             support I210 with device ID 0x157B. (VXW6-9251)
16jan14,xms  Add support Energy Efficient Ethernet for I350. (WIND00440439)
23dec13,xms  Fix I350 receive issue function. (WIND00448378)
02i,14feb13,scm  WIND00387999 support for Haswell micro-arch...
02h,03dec12,fao  Fix geiTxMblk/geiRxMblk definition error. (WIND00380916)
02g,18oct12,y_c  Add the define of annother GEI OUI value. (WIND00381657)
02f,13jul12,y_c  Add the define of GEI_PARH1. (WIND00367378)
02e,07mar12,wap  Add support for I210/I211 adapters, add code to select
                 valid flash bank for ICH/PCH devices (WIND00341911).
02d,10aprl2,y_c  fix a compiling warning in struct gei_drv_ctrl.(WIND00344099)
02c,22mar12,c_t  add BER optimize setting for Nano 8044(82574L).(WIND00315506)
02b,23sep11,wap  Correctly detect 82577/8/9 PHYs on boards booted with cable
                 disconnected (WIND00295339)
02a,20sep11,wap  Make sure gigE speed is enabled on 82579 (WIND00297475)
01z,23aug11,wap  Add support for I350 devices
01z,08aug11,wap  Sync with latest
01x,25apr11,j_z  Add EXTCNF register GATE_PHY_CFG bit.
01w,20oct10,j_z  Add support for 82579LM device on Cougar Point PCH. 
02a,11aug11,rbc  WIND00238999 - Fix build errors
01z,28jul11,rbc  WIND00238999 - Add VSB _WRS_CONFIG_PWR_MGMT
01y,06jul11,jjk  Support for Wake-On-Lan and device power management.
01x,26may11,jjk  Added support for One Acre-Quad Copper type cards. Also bring
                 in support for 82579LM device on Cougar Point PCH
01w,29mar11,dlk  Add PCI device ID for E1G44ET2.
01v,03jun10,dlk  Corrections to a couple of unused items.
01x,16jun10,wap  Add PCI IDs for more devices
01w,03jun10,wap  Correct typos
01v,14apr10,wap  Add support for more 82576 devices, 82580 dual and quad port
                 MACs, and PCH integrated MACs. Also make sure to clear TBI
                 link reset bit when coming out of reset (WIND00187458), add
                 support for 82567V3 device, add support for 82547
01v,09apr10,jpb  Added logLib.h for _func_logMsg.
01u,23feb10,h_k  added m_len adjust to GEI_ADJ().
01t,02sep09,wap  Work around interrupt handling issues on simulated PRO/1000
                 devices in VMware and similar systems (WIND00182689)
01y,14sep09,h_k  enabled GEI_64 for LP64.
01x,27aug09,h_k  replaced cacheUserFuncs with cacheDmaFuncs.
01w,26may09,h_k  replaced atomic_t with atomic32_t.
01v,21apr09,h_k  cleaned up compiler warnings in LP64.
01u,14jul09,mdk  merge from networking branch
01t,03jun09,wap  Make sure RX DMA ring is totally drained
01s,26apr09,wap  Add PCI ID for 82574 on Apple Mac Pro and 82583,
                 simplify interrupt handling
01r,10mar09,wap  Correct GEI_FL_GFP_BASE and GEI_FL_GFP_SIZE
01q,04dec08,dlk  Back out ISR/task synchronization changes for WIND00125287.
01p,27feb09,wap  Add support for 82576
01o,19feb09,wap  Add support for ICH8, ICH9 and ICH10 devices.
01n,16jul08,wap  Add support for Tolapai and ES2LAN devices
01m,03jul08,wap  Fix previous fix
01l,02jul08,dtr  Fix merge issue.
01k,20jun08,wap  Add PCI IDs for 82574/82575 adapters
01j,22mar08,dlk  Use CSUM_IPHDR_OFFSET() to find offset to IP header in
                 TX checksum offload code.
01i,21mar08,wap  Turn off GEI_READ_TDH to work around occasional transmit
                 stalls with some hardware (WIND00116611)
01h,09jan08,dlk  Manually round up descriptor memory allocated using
                 cacheDmaMalloc() (WIND00114029)
01g,12nov07,dlk  Increase GEI_MAX_RX from 16 to 32. Other performance
                 optimizations for WIND00104927.
01f,30jul07,wap  Fix WIND00099929 (workarounds for 82544 errata)
01e,05jul07,wap  Convert to new register access API
01d,27apr07,dlk  SMP synchronization improvements.
01c,08mar07,wap  Remove 82573_KCS PCI ID (not a network adapter)
01b,07dec06,wap  Add jumbo frame support
01a,14apr06,wap  written
*/

#ifndef __INCgei825xxVxbEndh
#define __INCgei825xxVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

#include <logLib.h>		/* _func_logMsg */

/*
 * When GEI_READ_TDH is defined, the driver determines whether
 * transmit descriptors are done by reading the TDH register.
 * Intel documentation warns that this may have a potential
 * race conditons on some systems that perform I/O write
 * buffering.
 * When GEI_READ_TDH is not defined, the driver determines
 * whether tranmsit descriptors are done by looking at the
 * DD bit in written-back descriptors.  There are some
 * reports of problems with this method on some AMD
 * processors (reference unknown).
 */

#undef GEI_READ_TDH

#ifdef	_WRS_CONFIG_LP64
#define GEI_64			/* 64-bit physical address support */
#endif	/* _WRS_CONFIG_LP64 */

/*
 * When GEI_VXB_DMA_BUF is defined, the driver makes use of
 * vxbDmaBufLib().  When GEI_VXB_DMA_BUF is not defined,
 * it relies only on CACHE_USER_FUNCS, and allocates
 * descriptor memory using cacheDmaMalloc().
 * Use of vxbDmaBufLib() is required on architectures
 * for which the PCI device may not be able to access
 * all memory (that might be used for either descriptors
 * or data clusters) on the target.
 *
 * Presently we disable GEI_VXB_DMA_BUF only for the I80X86
 * CPU family.
 */

#if (_VX_CPU_FAMILY == _VX_I80X86) && \
    (defined(_WRS_CONFIG_LP64) || !defined(GEI_64))
#undef GEI_VXB_DMA_BUF
#else
#define GEI_VXB_DMA_BUF
#endif


IMPORT void geiRegister (void);

#define GEI_LOGMSG(x,a,b,c,d,e,f)		\
    do {					\
	if (_func_logMsg != NULL)		\
	    _func_logMsg (x,			\
			  (_Vx_usr_arg_t)a,	\
			  (_Vx_usr_arg_t)b,	\
			  (_Vx_usr_arg_t)c,	\
			  (_Vx_usr_arg_t)d,	\
			  (_Vx_usr_arg_t)e,	\
			  (_Vx_usr_arg_t)f);	\
	} while (FALSE)

#define GEI_CACHE_DRV_VIRT_TO_PHYS(adrs)	\
    CACHE_DRV_VIRT_TO_PHYS(&cacheDmaFuncs, (void *) (adrs))

#ifndef BSP_VERSION

#define INTEL_VENDORID	0x8086

#define INTEL_DEVICEID_82542               0x1000

#define INTEL_DEVICEID_82543GC_FIBER       0x1001
#define INTEL_DEVICEID_82543GC_COPPER      0x1004

#define INTEL_DEVICEID_82544EI_COPPER      0x1008
#define INTEL_DEVICEID_82544EI_FIBER       0x1009
#define INTEL_DEVICEID_82544GC_COPPER      0x100C
#define INTEL_DEVICEID_82544GC_LOM         0x100D

#define INTEL_DEVICEID_82540EM             0x100E
#define INTEL_DEVICEID_82540EM2            0x100A
#define INTEL_DEVICEID_82540EM_LOM         0x1015
#define INTEL_DEVICEID_82540EP_LOM         0x1016
#define INTEL_DEVICEID_82540EP             0x1017
#define INTEL_DEVICEID_82540EP_LP          0x101E

#define INTEL_DEVICEID_82541EI             0x1013
#define INTEL_DEVICEID_82541ER_LOM         0x1014
#define INTEL_DEVICEID_82541EI_MOBILE      0x1018
#define INTEL_DEVICEID_82541GI             0x1076
#define INTEL_DEVICEID_82541GI_MOBILE      0x1077
#define INTEL_DEVICEID_82541ER             0x1078
#define INTEL_DEVICEID_82541GI_LF          0x107C

#define INTEL_DEVICEID_82545EM_COPPER      0x100F
#define INTEL_DEVICEID_82545EM_FIBER       0x1011
#define INTEL_DEVICEID_82545GM_COPPER      0x1026
#define INTEL_DEVICEID_82545GM_FIBER       0x1027
#define INTEL_DEVICEID_82545GM_SERDES      0x1028

#define INTEL_DEVICEID_82546EB_COPPER      0x1010
#define INTEL_DEVICEID_82546EB_FIBER       0x1012
#define INTEL_DEVICEID_82546EB_QUAD_COPPER 0x101D
#define INTEL_DEVICEID_82546GB_COPPER2     0x105B
#define INTEL_DEVICEID_82546GB_COPPER      0x1079
#define INTEL_DEVICEID_82546GB_FIBER       0x107A
#define INTEL_DEVICEID_82546GB_SERDES      0x107B
#define INTEL_DEVICEID_82546GB_PCIE        0x108A
#define INTEL_DEVICEID_82546GB_QUAD_COPPER        0x1099
#define INTEL_DEVICEID_82546GB_QUAD_COPPER_SRV    0x109B
#define INTEL_DEVICEID_82546GB_QUAD_COPPER_KSP3	  0x10B5

#define INTEL_DEVICEID_82547EI             0x1019
#define INTEL_DEVICEID_82547EI_MOBILE      0x101A
#define INTEL_DEVICEID_82547GI             0x1075

#define INTEL_DEVICEID_82571EB_QUAD_COPPER 0x10A4
#define INTEL_DEVICEID_82571EB_QUAD_FIBER  0x10A5
#define INTEL_DEVICEID_82571EB_DUAL_COPPER 0x105E
#define INTEL_DEVICEID_82571EB_FIBER       0x105F
#define INTEL_DEVICEID_82571EB_SERDES      0x1060
#define INTEL_DEVICEID_82571EB_QUAD_COPPER_LP     0x10BC
#define INTEL_DEVICEID_82571PT_QUAD        0x10D5
#define INTEL_DEVICEID_82571EB_MEZZ_DUAL   0x10D9
#define INTEL_DEVICEID_82571EB_MEZZ_QUAD   0x10DA

#define INTEL_DEVICEID_82572EI_COPPER      0x107D
#define INTEL_DEVICEID_82572EI_FIBER       0x107E
#define INTEL_DEVICEID_82572EI_SERDES      0x107F
#define INTEL_DEVICEID_82572EI             0x10B9

#define INTEL_DEVICEID_82573V              0x108B
#define INTEL_DEVICEID_82573E_IAMT         0x108C
#define INTEL_DEVICEID_82573L              0x109A
#define INTEL_DEVICEID_82573L_PL           0x10B0
#define INTEL_DEVICEID_82573V_PM           0x10B2
#define INTEL_DEVICEID_82573E_PM           0x10B3
#define INTEL_DEVICEID_82573L_PL2          0x10B4

#define INTEL_DEVICEID_82574L              0x10D3
#define INTEL_DEVICEID_82574L_MACPRO       0x10F6

#define INTEL_DEVICEID_80003ES2LAN_COPPER_DPT     0x1096
#define INTEL_DEVICEID_80003ES2LAN_SERDES_DPT     0x1098
#define INTEL_DEVICEID_80003ES2LAN_COPPER_DPT2    0x10BA
#define INTEL_DEVICEID_80003ES2LAN_SERDES_DPT2    0x10BB

/* 82566 devices */

#define INTEL_DEVICEID_ICH8_IGP_M_AMT       0x1049
#define INTEL_DEVICEID_ICH8_IGP_AMT         0x104A
#define INTEL_DEVICEID_ICH8_IGP_C           0x104B
#define INTEL_DEVICEID_ICH8_IGP_M           0x104D
#define INTEL_DEVICEID_ICH9_IGP_AMT         0x10BD
#define INTEL_DEVICEID_ICH9_IGP_M           0x10BF
#define INTEL_DEVICEID_ICH9_IGP_C           0x294C
#define INTEL_DEVICEID_ICH9_BM              0x10E5

/* 82562 10/100 devices */

#define INTEL_DEVICEID_ICH9_IFE             0x10C0
#define INTEL_DEVICEID_ICH9_IFE_G           0x10C2
#define INTEL_DEVICEID_ICH9_IFE_GT          0x10C3
#define INTEL_DEVICEID_ICH8_IFE             0x104C
#define INTEL_DEVICEID_ICH8_IFE_GT          0x10C4
#define INTEL_DEVICEID_ICH8_IFE_G           0x10C5

/* 82567 devices */

#define INTEL_DEVICEID_82567LF              0x10BF
#define INTEL_DEVICEID_82567V               0x10CB
#define INTEL_DEVICEID_82567LM              0x10F5
#define INTEL_DEVICEID_ICH8_82567V3         0x1501
#define INTEL_DEVICEID_82567V3				0x1501
#define INTEL_DEVICEID_82567V4              0x1525

/* 82567LM-3 ICH10 */

#define INTEL_DEVICEID_ICH10_R_BM_LM        0x10CC
#define INTEL_DEVICEID_ICH10_R_BM_LF        0x10CD
#define INTEL_DEVICEID_ICH10_R_BM_V         0x10CE
#define INTEL_DEVICEID_ICH10_D_BM_LM        0x10DE
#define INTEL_DEVICEID_ICH10_D_BM_LF        0x10DF

/* 82577/82578 PCH */

#define INTEL_DEVICEID_PCH_M_HV_LM          0x10EA
#define INTEL_DEVICEID_PCH_M_HV_LC          0x10EB
#define INTEL_DEVICEID_PCH_D_HV_DM          0x10EF
#define INTEL_DEVICEID_PCH_D_HV_DC          0x10F0

/* 82583 */

#define INTEL_DEVICEID_82583                0x150C

/* 82575 and later are advanced devices */

#define INTEL_DEVICEID_82575EB_COPPER       0x10A7
#define INTEL_DEVICEID_82575EB_FIBER_SERDES 0x10A9
#define INTEL_DEVICEID_82575GB_QUAD_COPPER  0x10D6

#define INTEL_DEVICEID_82576_COPPER         0x10C9
#define INTEL_DEVICEID_82576_FIBER          0x10E6
#define INTEL_DEVICEID_82576_SERDES         0x10E7
#define INTEL_DEVICEID_82576_QUAD_COPPER    0x10E8
#define INTEL_DEVICEID_82576_NS             0x150A
#define INTEL_DEVICEID_82576_NS_SERDES      0x1518
#define INTEL_DEVICEID_82576_QUAD_SERDES    0x150D
#define INTEL_DEVICEID_82576_QUAD_ET2       0x1526  /* E1G44ET2 */

/* Tolapai integrated gigE MACs */

#define INTEL_DEVICEID_TOLAPAI_REVB_ID1     0x5040
#define INTEL_DEVICEID_TOLAPAI_REVB_ID2     0x5044
#define INTEL_DEVICEID_TOLAPAI_REVB_ID3     0x5048
#define INTEL_DEVICEID_TOLAPAI_REVC_ID1     0x5041
#define INTEL_DEVICEID_TOLAPAI_REVC_ID2     0x5045
#define INTEL_DEVICEID_TOLAPAI_REVC_ID3     0x5049

/* 82579LM devices */

#define INTEL_DEVICEID_PCH2_82579LM         0x1502
#define INTEL_DEVICEID_PCH2_82579V          0x1503

/* 82580 devices */

#define INTEL_DEVICEID_82580_QUAD_COPPER    0x150E
#define INTEL_DEVICEID_82580_FIBER          0x150F
#define INTEL_DEVICEID_82580_BACKPLANE      0x1510
#define INTEL_DEVICEID_82580_SFP            0x1511
#define INTEL_DEVICEID_82580_DUAL_COPPER    0x1516
#define INTEL_DEVICEID_82580_QUAD_FIBER     0x1527
#define INTEL_DEVICEID_DH89XXCC_SGMII       0x0438
#define INTEL_DEVICEID_DH89XXCC_SERDES      0x043A
#define INTEL_DEVICEID_DH89XXCC_BACKPLANE   0x043C
#define INTEL_DEVICEID_DH89XXCC_SFP         0x0440  

/* I350 devices */

#define INTEL_DEVICEID_I350_COPPER          0x1521 /* dual and quad */
#define INTEL_DEVICEID_I350_FIBER           0x1522
#define INTEL_DEVICEID_I350_SERDES          0x1523
#define INTEL_DEVICEID_I350_SGMII           0x1524
#define INTEL_DEVICEID_I350_DA4             0x1546

/* I210/I211 devices */

#define INTEL_DEVICEID_I210AT_COPPER        0x1533
#define INTEL_DEVICEID_I210IT_COPPER        0x1535
#define INTEL_DEVICEID_I210IS_SERDES        0x1537
#define INTEL_DEVICEID_I210IS_SGMII         0x1538
#define INTEL_DEVICEID_I211AT_COPPER        0x1539
#define INTEL_DEVICEID_I210AT_FLASHLESS     0X157B /* copper only without flash attached */

/* Clarkville LAN on HASWELL Boards */

#define INTEL_DEVICEID_HASWELL_REVA_ID1     0x153A

/* I218 devices */

#define INTEL_DEVICEID_PCH_I218LM           0x155A
#define INTEL_DEVICEID_PCH_I218V            0x1559

#define GEI_DEVTYPE_PCIX	1	/* 8254x PCI-X devices */
#define GEI_DEVTYPE_PCIE	2	/* 8257x PCIe devices */
#define GEI_DEVTYPE_ADVANCED	3	/* 82575/82576/82580/I350 advanced devices */
#define GEI_DEVTYPE_ES2LAN	4	/* ES2LAN devices */
#define GEI_DEVTYPE_TOLAPAI	5	/* Tolapai integrated devices */
#define GEI_DEVTYPE_ICH		6	/* ICH8/ICH9 devices */

/* for tolapai integrated MAC */
#define PCI_SMIA_REG			0xE8

#define PCI_SMIA_ENABLE_INT0		0x04
#define PCI_SMIA_ENABLE_INT1		0x02
#define PCI_SMIA_ENABLE_INT_ERR		0x00

/* MAC offset in eeprom */

#define TOLAPAI_MACADD_EEPROM_OFFSET_ID1	0x12
#define TOLAPAI_MACADD_EEPROM_OFFSET_ID2	0x22
#define TOLAPAI_MACADD_EEPROM_OFFSET_ID3	0x32

/* EEPROM Initialization Control Registers */

#define GEI_INIT_CONTROL3_PORT_A    0x24
#define GEI_INIT_CONTROL3_PORT_B    0x14
#define GEI_EEPROM_APME             0x400

/* 16 bit Power Management Control and Status register bit settings */

#define GEI_PMCSR_PME_ENABLE    0x0100
#define GEI_PMCSR_PME_STATUS    0x8000
#define GEI_PMCSR_PME_D3        0x0003

/* Management Control */

#define GEI_MANC_SMBUS_EN       0x00000001 /* SMBus Enabled - RO */
#define GEI_MANC_ASF_EN         0x00000002 /* ASF Enabled - RO */
#define GEI_MANC_R_ON_FORCE     0x00000004 /* Reset on Force TCO - RO */
#define GEI_MANC_RMCP_EN        0x00000100 /* Enable RCMP 026Fh Filtering */
#define GEI_MANC_0298_EN        0x00000200 /* Enable RCMP 0298h Filtering */
#define GEI_MANC_IPV4_EN        0x00000400 /* Enable IPv4 */
#define GEI_MANC_IPV6_EN        0x00000800 /* Enable IPv6 */
#define GEI_MANC_EN_MNG2HOST    0x00200000 /* MNG packets to host memory */
#define GEI_MANC_RCV_TCO_EN     0x00020000 /* Receive TCO Packets Enabled */
#define GEI_MANC_ARP_EN         0x00002000 /* Enable ARP Request Filtering */

/* Wake up control register bit settings */

#define GEI_WUC_APME            0x00000001 /* APM Enable */
#define GEI_WUC_PME_EN          0x00000002 /* PME Enable */
#define GEI_WUC_PME_STATUS      0x00000004 /* PME Status */
#define GEI_WUC_APMPME          0x00000008 /* Assert PME on APM Wakeup */
#define GEI_WUC_LSCWE           0x00000010 /* Link Status wake up enable */
#define GEI_WUC_LSCWO           0x00000020 /* Link Status wake up override */
#define GEI_WUC_SPM             0x80000000 /* Enable SPM */
#define GEI_WUC_PHY_WAKE        0x00000100 /* if PHY supports wakeup */

/* Wake Up Filter Control register bit settings */

#define GEI_WUFC_LNKC           0x00000001 /* Link Status Change Wakeup Enable */
#define GEI_WUFC_MAG            0x00000002 /* Magic Packet Wakeup Enable */
#define GEI_WUFC_EX             0x00000004 /* Directed Exact Wakeup Enable */
#define GEI_WUFC_MC             0x00000008 /* Directed Multicast Wakeup Enable */
#define GEI_WUFC_BC             0x00000010 /* Broadcast Wakeup Enable */
#define GEI_WUFC_ARP            0x00000020 /* ARP Request Packet Wakeup Enable */
#define GEI_WUFC_IPV4           0x00000040 /* Directed IPv4 Packet Wakeup Enable */
#define GEI_WUFC_IPV6           0x00000080 /* Directed IPv6 Packet Wakeup Enable */
#define GEI_WUFC_IGNORE_TCO_PHY 0x00000800 /* Ignore WakeOn TCO packets */
#define GEI_WUFC_FLX0_PHY       0x00001000 /* Flexible Filter 0 Enable */
#define GEI_WUFC_FLX1_PHY       0x00002000 /* Flexible Filter 1 Enable */
#define GEI_WUFC_FLX2_PHY       0x00004000 /* Flexible Filter 2 Enable */
#define GEI_WUFC_FLX3_PHY       0x00008000 /* Flexible Filter 3 Enable */
#define GEI_WUFC_FLX4_PHY       0x00000200 /* Flexible Filter 4 Enable */
#define GEI_WUFC_FLX5_PHY       0x00000400 /* Flexible Filter 5 Enable */
#define GEI_WUFC_IGNORE_TCO     0x00008000 /* Ignore WakeOn TCO packets */
#define GEI_WUFC_FLX0           0x00010000 /* Flexible Filter 0 Enable */
#define GEI_WUFC_FLX1           0x00020000 /* Flexible Filter 1 Enable */
#define GEI_WUFC_FLX2           0x00040000 /* Flexible Filter 2 Enable */
#define GEI_WUFC_FLX3           0x00080000 /* Flexible Filter 3 Enable */
#define GEI_WUFC_FLX4           0x00100000 /* Flexible Filter 4 Enable */
#define GEI_WUFC_FLX5           0x00200000 /* Flexible Filter 5 Enable */
#define GEI_WUFC_ALL_FILTERS_PHY_4 0x0000F0FF /* Mask for all wakeup filters */
#define GEI_WUFC_FLX_OFFSET_PHY    12         /* Offset to the Flexible Filters bits */
#define GEI_WUFC_FLX_FILTERS_PHY_4 0x0000F000 /* Mask for 4 flexible filters */
#define GEI_WUFC_ALL_FILTERS_PHY_6 0x0000F6FF /* Mask for 6 wakeup filters */
#define GEI_WUFC_FLX_FILTERS_PHY_6 0x0000F600 /* Mask for 6 flexible filters*/
#define GEI_WUFC_ALL_FILTERS       0x000F00FF /* Mask for all wakeup filters */
#define GEI_WUFC_ALL_FILTERS_6     0x003F00FF /* Mask for all 6 wakeup filters*/
#define GEI_WUFC_FLX_OFFSET        16         /* Offset to the Flexible Filters bits */
#define GEI_WUFC_FLX_FILTERS       0x000F0000 /* Mask for the 4 flexible filters */
#define GEI_WUFC_FLX_FILTERS_6     0x003F0000 /* Mask for 6 flexible filters */


/* Intel PRO/1000 I/O registers */

#define GEI_CTRL	0x0000	/* Device control */
#define GEI_CTRL_DUP	0x0004	/* Device control duplicate (shadow) */
#define GEI_STS		0x0008	/* Device status */
#define GEI_EECD	0x0010	/* EEPROM/flash control */
#define GEI_EERD	0x0014	/* EEPROM/flash data (!82544) */
#define GEI_FLA		0x001C	/* Flash access (82541,82547 only) */
#define GEI_CTRLEXT	0x0018	/* Extended device control */
#define GEI_MDIC	0x0020	/* MDI control */
#define GEI_FCAL	0x0028	/* Flow control address, low */
#define GEI_FCAH	0x002C	/* Flow control address, high */
#define GEI_FCT		0x0030	/* Flow control type */
#define GEI_KUMCTLSTS	0x0034	/* GLCI (Kumeran) ctrl/sts */
#define GEI_CONNSW	0x0034	/* Copper/fiber switch ctl (575/576/580) */
#define GEI_VET		0x0038	/* VLAN ethertype */
#define GEI_ICR		0x00C0	/* Interrupt cause read */
#define GEI_ITR		0x00C4	/* Interrupt throttling (!82544) */
#define GEI_ICS		0x00C8	/* Interrupt cause set */
#define GEI_IMS		0x00D0	/* Interrupt mask set/read */
#define GEI_IMC		0x00D8	/* Interrupt mask clear */
#define GEI_RCTL	0x0100	/* Receive control */
#define GEI_FCTTV	0x0170	/* Flow control TX timer value */
#define GEI_TXCW	0x0178	/* TX config (!82540,!82541,!82547) */
#define GEI_RXCW	0x0180	/* RX config (!82540,!82541,!82547) */
#define GEI_TCTL	0x0400	/* Transmit control */
#define GEI_TCTL_EXT	0x0404	/* Transmit control extended */
#define GEI_TIPG	0x0410	/* Transmit inter-packet gap */
#define GEI_AIFS	0x0458	/* Adaptive IFS throttle */
#define GEI_LEDCTL	0x0E00	/* LED control (!82544) */
#define GEI_EXTCNF_CTRL	0x0F00  /* Extended config control */
#define GEI_EXTCNF_SIZE	0x0F08  /* Extended config size */
#define GEI_PHY_CTRL	0x0F10	/* PHY control */
#define GEI_PBA		0x1000	/* packet buffer allocation */
#define GEI_GPIE	0x1514	/* General Purpose Interrupt Enable */
#define GEI_EICS	0x1520	/* Extended interrupt cause set */
#define GEI_EIMS	0x1524	/* Extended interrupt mask set/read */
#define GEI_EIMC	0x1528	/* Extended interrupt mask clear */
#define GEI_EIAC	0x152C	/* Extended interrupt auto clear */
#define GEI_EIAM	0x1530	/* Extended interrupt auto clear */
#define GEI_EICR	0x1530	/* Extended interrupt cause read */
#define GEI_EITR0	0x1680  /* Interrupt throttling rate 0 */
#define GEI_EITR24	0x16F0  /* Interrupt throttling rate 23 */
#define GEI_IVAR0	0x1700	/* Interrupt vector alloc 0 */
#define GEI_IVAR1	0x1704	/* Interrupt vector alloc 1 */
#define GEI_IVAR2	0x1708	/* Interrupt vector alloc 2 */
#define GEI_IVAR3	0x170C	/* Interrupt vector alloc 3 */
#define GEI_IVAR4	0x1710	/* Interrupt vector alloc 4 */
#define GEI_IVAR5	0x1714	/* Interrupt vector alloc 5 */
#define GEI_IVAR6	0x1718	/* Interrupt vector alloc 6 */
#define GEI_IVAR7	0x171C	/* Interrupt vector alloc 7 */
#define GEI_IVARMISC	0x1740  /* Intr. alloc. registers misc */
#define GEI_FCRTL	0x2160	/* RX flow control threshold, low */
#define GEI_FCRTH	0x2168	/* RX flow control threshold, high */
#define GEI_RDBAL0	0x2800	/* RX descriptor base, low */
#define GEI_RDBAH0	0x2804	/* RX descriptor base, high */
#define GEI_RDLEN0	0x2808	/* RX descriptor table length */
#define GEI_SRRCTL0	0x280C	/* RX Split/replication control (82575) */
#define GEI_RDH0	0x2810	/* RX descriptor head (consumer idx) */
#define GEI_RDT0	0x2818	/* RX descriptor tail (producer idx) */
#define GEI_RDTR	0x2820	/* RX delay timer */
#define GEI_RXDCTL0	0x2828	/* RX descriptor control */
#define GEI_RADV	0x282C	/* RX absolute intr delay timer (!82544) */
#define GEI_RDBAL1	0x2900	/* RX descriptor base, low */
#define GEI_RDBAH1	0x2904	/* RX descriptor base, high */
#define GEI_RDLEN1	0x2908	/* RX descriptor table length */
#define GEI_RDH1	0x2910	/* RX descriptor head (consumer idx) */
#define GEI_RDT1	0x2918	/* RX descriptor tail (producer idx) */
#define GEI_RDTR1	0x2920	/* RX delay timer */
#define GEI_RXDCTL1	0x2928	/* RX descriptor control */
#define GEI_RSRPD	0x2C00	/* RX small pkt detect intr (!82544) */
#define GEI_TXDMAC	0x3000	/* TX DMA control (82544 only) */
#define GEI_TDBAL0	0x3800	/* TX descriptor base, low */
#define GEI_TDBAH0	0x3804	/* TX descriptor base, high */
#define GEI_TDLEN0	0x3808	/* TX descriptor table length */
#define GEI_TDH0	0x3810	/* TX descriptor head (consumer idx) */
#define GEI_TDT0	0x3818	/* TX descriptor tail (producer idx) */
#define GEI_TIDV	0x3820	/* TX interrupt delay value */
#define GEI_TXDCTL0	0x3828	/* TX descriptor control */
#define GEI_TADV	0x382C	/* TX absolute intr delay timer (!82544) */
#define GEI_TSPMT	0x3830	/* TCP segmentation pad and threshold */
#define GEI_TDBAL1	0x3900	/* TX descriptor base, low */
#define GEI_TDBAH1	0x3904	/* TX descriptor base, high */
#define GEI_TDLEN1	0x3908	/* TX descriptor table length */
#define GEI_TDH1	0x3910	/* TX descriptor head (consumer idx) */
#define GEI_TDT1	0x3918	/* TX descriptor tail (producer idx) */
#define GEI_TXDCTL1	0x3928	/* TX descriptor control */
#define GEI_RXCSUM	0x5000	/* RX checksum control */
#define GEI_RFCTL	0x5008  /* Receive Filter Control */
#define GEI_MAR0	0x5200	/* Multicast hash table start */
#define GEI_MAR127	0x53FC	/* Multicast hash table end */
#define GEI_PARL0	0x5400	/* 1nd half of Perfect match table, low */
#define GEI_PARH0	0x5404	/* 1nd half of Perfect match table, high */
#define GEI_PARL1	0x54E0	/* 2nd half of Perfect match table, low */
#define GEI_PARH1	0x54E4	/* 2nd half of Perfect match table, high */
#define GEI_VFTA0	0x5600	/* VLAN filter table, start (!82541)*/
#define GEI_VFTA127	0x57FC	/* VLAN filter table, end (!82541) */
#define GEI_WUC		0x5800	/* Wakeup control */
#define GEI_WUFC	0x5808	/* Wakeup filter control */
#define GEI_WUS		0x5810	/* Wakeup status */
#define GEI_MANC	0x5820  /* Management Control - RW */
#define GEI_IP4AV	0x5838	/* IP address valid */
#define GEI_IP4AT_START	0x5840	/* IPv4 address table */
#define GEI_IP4AT_END	0x5848	/* IPv4 address table */
#define GEI_IP6AT_START	0x5880	/* IPv6 address table (!82544) */
#define GEI_IP6AT_END	0x588C	/* IPv6 address table (!82544) */
#define GEI_WUPL	0x5900	/* Wakeup packet length */
#define GEI_WUPM_START	0x5A00	/* Wakeup packet memory */
#define GEI_WUPM_END	0x5A7C	/* Wakeup packet memory */
#define GEI_FFLT_START	0x5F00	/* Flexible filter length table */
#define GEI_FFLT_END	0x5F18	/* Flexible filter length table */
#define GEI_FFMT_START	0x9000	/* Flexible filter mask table */
#define GEI_FFMT_END	0x93F8	/* Flexible filter mask table */
#define GEI_FFVT_START	0x9800	/* Flexible filter value table */
#define GEI_FFVT_END	0x9BF8	/* Flexible filter value table */

/* Add definitions for I350 NIC */

#define GEI_IPCNFG      0x0E38  /* Internal PHY Configuration */
#define GEI_EEER        0x0E30  /* Energy Efficient Ethernet */
#define GEI_EEE_SU      0x0E34  /* EEE Setup */

#define GEI_IPCNFG_MDI_FLIP        0x00000001  /* MDI Flip */
#define GEI_IPCNFG_10BASE_TE       0x00000002  /* Enable low amplitude 10BASE-T operation */
#define GEI_IPCNFG_EEE_100M_AN     0x00000004  /* EEE Enable 100M AN */
#define GEI_IPCNFG_EEE_1G_AN       0x00000008  /* EEE Enable 1G AN */
#define GEI_EEER_TX_LPI_EN         0x00010000  /* EEE Tx LPI Enable */
#define GEI_EEER_RX_LPI_EN         0x00020000  /* EEE Rx LPI Enable */
#define GEI_EEER_LPI_FC            0x00040000  /* EEE Enable on FC */
#define GEI_EEE_SU_LPI_CLK_STP     0x00800000  /* EEE LPI Clock Stop */
#define GEI_EEER_FRC_AN            0x10000000  /* Enable EEE in loopback */
#define GEI_EEER_EEE_NEG           0x20000000  /* EEE capability nego */
#define GEI_EEER_RX_LPI            0x40000000  /* RX IN LPI STATE */
#define GEI_EEER_TX_LPI            0x80000000  /* TX IN LPI STATE */

#define GEI_RXCTL(n)	  ((n) < 4 ? (0x02814 + ((n) * 0x100)) : \
				      (0x0C014 + ((n) * 0x40)))
#define GEI_DCA_RXCTRL(n)	GEI_RXCTL(n)

#define GEI_TXCTL(n)   ((n) < 4 ? (0x03814 + ((n) * 0x100)) : \
				      (0x0E014 + ((n) * 0x40)))
#define GEI_DCA_TXCTRL(n) GEI_TXCTL(n)

#define GEI_DCA_RXCTRL_DESC_DCA_EN (1 << 5)  /* DCA Rx Desc enable */
#define GEI_DCA_RXCTRL_DATA_DCA_EN (1 << 7)  /* DCA Rx Desc payload enable */
#define GEI_DCA_RXCTRL_DESC_RRO_EN (1 << 9)  /* DCA Rx rd Desc Relax Order enable */
#define GEI_DCA_RXCTRL_DESC_WRO_EN (1 << 13)  /* DCA Rx wr Desc Relax Order enable */

#define GEI_DCA_TXCTRL_DESC_DCA_EN (1 << 5)  /* DCA Tx Desc enable */
#define GEI_DCA_TXCTRL_DESC_RRO_EN (1 << 9)  /* Tx rd Desc Relax Order */
#define GEI_DCA_TXCTRL_TX_WB_RO_EN (1 << 11) /* Tx Desc writeback RO bit */
#define GEI_DCA_TXCTRL_DATA_RRO_EN (1 << 13) /* Tx rd data Relax Order */

#define GEI_MAX_QUEUES_PER_PORT    (8)  /* max queues for RX or TX */

/* Add definitions for I210*/

#define GEI_EEC                 0x12010
#define FLASH_IN_USE_MASK       0x40

#define GEI_INVM_DATA_REG(n)    (0x12120 + 4*(n))
#define GEI_INVM_SIZE           64 /* Number of INVM Data Registers */

#define INVM_DWORD_TO_RECORD_TYPE(word) \
    (UINT8)((word) & 0x7)
#define INVM_DWORD_TO_WORD_ADDRESS(word) \
    (UINT8)(((word) & 0x0000FE00) >> 9)
#define INVM_DWORD_TO_WORD_DATA(word) \
    (UINT16)(((word) & 0xFFFF0000) >> 16)

enum GEI_INVM_STRUCTURE_TYPE {
    GEI_INVM_UNINITIALIZED_STRUCTURE            = 0x00,
    GEI_INVM_WORD_AUTOLOAD_STRUCTURE            = 0x01,
    GEI_INVM_CSR_AUTOLOAD_STRUCTURE             = 0x02,
    GEI_INVM_PHY_REGISTER_AUTOLOAD_STRUCTURE    = 0x03,
    GEI_INVM_RSA_KEY_SHA256_STRUCTURE           = 0x04,
    GEI_INVM_INVALIDATED_STRUCTURE              = 0x0F,
};

#define GEI_INVM_RSA_KEY_SHA256_DATA_SIZE_IN_DWORDS 8
#define GEI_INVM_CSR_AUTOLOAD_DATA_SIZE_IN_DWORDS   1
#define GEI_INVM_ULT_BYTES_SIZE                     8
#define GEI_INVM_RECORD_SIZE_IN_BYTES               4
#define GEI_INVM_VER_FIELD_ONE          0x1FF8
#define GEI_INVM_VER_FIELD_TWO          0x7FE000
#define GEI_INVM_IMGTYPE_FIELD          0x1F800000

#define GEI_INVM_MAJOR_MASK             0x3F0
#define GEI_INVM_MINOR_MASK             0xF
#define GEI_INVM_MAJOR_SHIFT            4

/* The 82576 has some registers in different locations. */

#define GEI_RBAL_76	0xC000	/* RBAL for 82576 */
#define GEI_RBAH_76	0xC004	/* RBAH for 82576 */
#define GEI_RDLEN_76	0xC008	/* RDLEN for 82576 */
#define GEI_SRRCTL_76	0xC00C	/* SRRCTL for 82576 */
#define GEI_RDH_76	0xC010	/* RDH for 82576 */
#define GEI_RDT_76	0xC018	/* RDT for 82576 */
#define GEI_RXDCTL_76	0xC028	/* RXDCTL for 82576 */

#define GEI_TBAL_76	0xE000	/* TBAL for 82576 */
#define GEI_TBAH_76	0xE004	/* TBAH for 82576 */
#define GEI_TDLEN_76	0xE008	/* TDLEN for 82576 */
#define GEI_TDH_76	0xE010	/* TDH for 82576 */
#define GEI_TDT_76	0xE018	/* TDT for 82576 */
#define GEI_TXDCTL_76	0xE028	/* TXDCTL for 82576 */

#define GEI_RDBAL	GEI_RDBAL0
#define GEI_RDBAL	GEI_RDBAL0
#define GEI_RDBAH	GEI_RDBAH0
#define GEI_RDLEN	GEI_RDLEN0
#define GEI_SRRCTL	GEI_SRRCTL0
#define GEI_RDH		GEI_RDH0
#define GEI_RDT		GEI_RDT0
#define GEI_RXDCTL	GEI_RXDCTL0

#define GEI_TDBAL	GEI_TDBAL0
#define GEI_TDBAH	GEI_TDBAH0
#define GEI_TDLEN	GEI_TDLEN0
#define GEI_TDH		GEI_TDH0
#define GEI_TDT		GEI_TDT0
#define GEI_TXDCTL	GEI_TXDCTL0

/*
 * Note: there are actually 16 addresses, but on the 82571, 82572
 * and ES2LAN devices, the last entry is reserved for use by the
 * management firmware running in the NIC. We have to be careful
 * not to overwrite this entry.
 */

#define GEI_PAR_CNT	15
#define GEI_MAR_CNT	128

/*
 * For ICH8 devices, only 6 address slots are available, and for
 * ICH9 and ICH10 devices, only 7 are available. For PCH2 devices,
 * only 5 address slots are available.
 * For ICH8, ICH9 and ICH19 devices, only 32 multicast entries
 * are available.
 */

#define GEI_PCH2_PAR_CNT	5
#define GEI_ICH8_PAR_CNT	6
#define GEI_ICH_PAR_CNT		7
#define GEI_ICH_MAR_CNT		32

/* For some advanced devices, there are more entries available */

#define GEI_82575_PAR_CNT	16
#define GEI_82576_PAR_CNT	24
#define GEI_82580_PAR_CNT	24
#define GEI_I350_PAR_CNT	32
#define GEI_I210_PAR_CNT	16

/* Stats registers */

#define GEI_CRCERRC	0x4000	/* RX CRC error count */
#define GEI_ALIGNERRC	0x4004	/* RX alignment error count */
#define GEI_SYMERRC	0x4008	/* RX symbol error count */
#define GEI_RXERRC	0x400C	/* RX error count */
#define GEI_MPC		0x4010	/* RX missed packet count */
#define GEI_SCC		0x4014	/* TX single collision count */
#define GEI_ECOL	0x4018	/* TX excessive collisions count */
#define GEI_MCC		0x401C	/* TX multiple collisions count */
#define GEI_LATECOL	0x4020	/* TX late collision count */
#define GEI_COLC	0x4028	/* TX collision count */
#define GEI_DC		0x4030	/* TX defer count */
#define GEI_TNCRS	0x4034	/* TX carrier sense lost */
#define GEI_SEC		0x4038	/* TX sequence error count */
#define GEI_CEXTERR	0x403C	/* TX carrier extension error count */
#define GEI_RLEC	0x4040	/* RX length error count */
#define GEI_XONRXC	0x4048	/* RX XON received count */
#define GEI_XONTXC	0x404C	/* TX XON transmitted count */
#define GEI_XOFFRXC	0x4050	/* RX XOFF received count */
#define GEI_XOFFTXC	0x4054	/* TX XOFF transmitted count */
#define GEI_FCRUC	0x4058	/* RX FC received unsupported count */
#define GEI_PRC64	0x405C	/* RX 64 byte frames received */
#define GEI_PRC127	0x4060	/* RX 65-127 byte frames received */
#define GEI_PRC255	0x4064	/* RX 128-255 byte frames received */
#define GEI_PRC511	0x4068	/* RX 256-511 byte frames received */
#define GEI_PRC1023	0x406C	/* RX 512-1023 byte frames received */
#define GEI_PRC1522	0x4070	/* RX 1024 to max byte frames received */
#define GEI_GPRC	0x4074	/* RX good frames received */
#define GEI_BPRC	0x4078	/* RX bcast frames received */
#define GEI_MPRC	0x407C	/* RX mcast frames received */
#define GEI_GPTC	0x4080	/* TX good frames transmitted */
#define GEI_GORCL	0x4088	/* RX good octets received, low */
#define GEI_GORCH	0x408C	/* RX good octets received, high */
#define GEI_GOTCL	0x4090	/* TX good octets transmitted, low */
#define GEI_GOTCH	0x4094	/* TX good octets transmitted, high */
#define GEI_RNBC	0x40A0	/* RX frames drop due to no buffers */
#define GEI_RUC		0x40A4	/* RX runts */
#define GEI_RFC		0x40A8	/* RX fragments */
#define GEI_ROC		0x40AC	/* RX giants */
#define GEI_RJC		0x40B0	/* RX jabber errors */
#define GEI_MGTPRC	0x40B4	/* RX mgmt frame count (!82544, !82541) */
#define GEI_MGTPDC	0x40B8	/* RX mgmt frames dropped (!82544, !82541) */
#define GEI_MGTPTC	0x40BC	/* TX mgmt frames sent (!82544, !82541) */
#define GEI_TORL	0x40C0	/* RX total octets received, low */
#define GEI_TORH	0x40C4	/* RX total octets received, high */
#define GEI_TOTL	0x40C8	/* TX total octets transmitted, low */
#define GEI_TOTH	0x40CC	/* TX total octets transmitted, high */
#define GEI_TPR		0x40D0	/* RX total packets received */
#define GEI_TPT		0x40D4	/* RX total packets transmitted */
#define GEI_PTC64	0x40D8	/* TX total 64 byte packets sent */
#define GEI_PTC127	0x40DC	/* TX total 65-127 byte packets sent */
#define GEI_PTC255	0x40E0	/* TX total 128-255 byte packets sent */
#define GEI_PTC511	0x40E4	/* TX total 256-511 byte packets sent */
#define GEI_PTC1023	0x40E8	/* TX total 512-1023 byte packets sent */
#define GEI_PTC1522	0x40EC	/* TX total 1024 to max byte packets sent */
#define GEI_MPTC	0x40F0	/* TX total mcast frames sent */
#define GEI_BPTC	0x40F4	/* TX total bcast frames sent */
#define GEI_TSCTC	0x40F8	/* TX TCP segmentation context sent count */
#define GEI_TSCTFC	0x40F8	/* TX TCP segmentation context failed count */

/* PCS (SGMII/SerDes 82575/82576/82580) */

#define GEI_PCSCFG	0x4200	/* PCS configuration */
#define GEI_PCSLCTL	0x4208	/* PCS link control */
#define GEI_PCSLSTS	0x420C	/* PCS link status */
#define GEI_PCSANAR	0x4218	/* PCS autoneg advertisement */
#define GEI_PCSLPAR	0x421C	/* PCS link partner ability */
#define GEI_PCSNP	0x4220	/* PCS next page transmit */
#define GEI_PCSLPNP	0x4224	/* PCS link partner next page */

/* Diagnostics */

#define GEI_RDFH	0x2410	/* RX data FIFO head */
#define GEI_RDFT	0x2418	/* RX data FIFO tail */
#define GEI_RDFHS	0x2420	/* RX data FIFO head saved */
#define GEI_RDFTS	0x2428	/* RX data FIFO tail saved */
#define GEI_RDFPC	0x2430	/* RX data FIFO packet count */
#define GEI_TDFH	0x3410	/* TX data FIFO head */
#define GEI_TDFT	0x3418	/* TX data FIFO tail */
#define GEI_TDFHS	0x3420	/* TX data FIFO head saved */
#define GEI_TDFTS	0x3428	/* TX data FIFO tail saved */
#define GEI_TDFPC	0x3430	/* TX data FIFO packet count */

/* Packet buffer memory */

#define GEI_PBM_START	0x10000	
#define GEI_PBM_END	0x1FFFC


/* Device control register */

#define GEI_CTRL_FD		0x00000001 /* Full duplex */
#define GEI_CTRL_BEM		0x00000002 /* Big-endian mode */
#define GEI_CTRL_PRIOR		0x00000004 /* DMA priority (82543) */
#define GEI_CTRL_GIO_MSTR_DIS	0x00000004 /* GIO master disable */
#define GEI_CTRL_LRST		0x00000008 /* TBI Link reset (!82540/1/7) */
#define GEI_CTRL_TME		0x00000010 /* Test mode enable */
#define GEI_CTRL_ASDE		0x00000020 /* Autospeed sense enable */
#define GEI_CTRL_SLU		0x00000040 /* Set link up */
#define GEI_CTRL_ILOS		0x00000080 /* Invert loss of signal */
#define GEI_CTRL_SPEED		0x00000300 /* Link speed */
#define GEI_CTRL_FRCSPD		0x00000800 /* Force speed */
#define GEI_CTRL_FRCDPX		0x00001000 /* Force duplex */
#define GEI_CTRL_LANPHYPC_OVR	0x00010000 /* LAN/PHY Power Ctrl pin override */
#define GEI_CTRL_LANPHYPC_VAL	0x00020000 /* forced LANPHYPC value */
#define GEI_CTRL_SDP0_DATA	0x00040000 /* SDP0 pin control */
#define GEI_CTRL_SDP1_DATA	0x00080000 /* SDP1 pin control */
#define GEI_CTRL_SDP2_DATA	0x00100000 /* SDP2 pin control */
#define GEI_CTRL_SDP3_DATA	0x00200000 /* SDP3 pin control */
#define GEI_CTRL_ADVD3WUC	0x00100000 /* Advertise D3 wakeup capability */
#define GEI_CTRL_PHY_PWR_MGMT	0x00200000 /* Enable PHY power management */
#define GEI_CTRL_SDP0_DIR	0x00400000 /* SDP0 pin I/O direction */
#define GEI_CTRL_SDP1_DIR	0x00800000 /* SDP1 pin I/O direction */
#define GEI_CTRL_SDP2_DIR	0x01000000 /* SDP2 pin I/O direction */
#define GEI_CTRL_SDP3_DIR	0x02000000 /* SDP3 pin I/O direction */
#define GEI_CTRL_RST		0x04000000 /* Reset */
#define GEI_CTRL_RFCE		0x08000000 /* RX flow control enable */
#define GEI_CTRL_TFCE		0x10000000 /* TX flow control enable */
#define GEI_CTRL_VME		0x40000000 /* VLAN mode enable (!82541) */
#define GEI_CTRL_PHY_RST	0x80000000 /* PHY reset */

/* Legacy bitbang MDIO support for 82543 devices. */

#define GEI_CTRL_MDIO		GEI_CTRL_SDP2_DATA
#define GEI_CTRL_MDC		GEI_CTRL_SDP3_DATA
#define GEI_CTRL_MDIO_DIR	GEI_CTRL_SDP2_DIR
#define GEI_CTRL_MDC_DIR	GEI_CTRL_SDP3_DIR

#define GEI_CSPEED_10		0x00000000
#define GEI_CSPEED_100		0x00000100
#define GEI_CSPEED_1000		0x00000200

/* Device status register */

#define GEI_STS_FD		0x00000001 /* Full duplex */
#define GEI_STS_LU		0x00000002 /* Link up */
#define GEI_STS_FID		0x0000000C /* Function ID */
#define GEI_STS_TXOFF		0x00000010 /* TX paused */
#define GEI_STS_TBIMODE		0x00000020 /* TBI/SerDes mode */
#define GEI_STS_SPEED		0x000000C0 /* Link speed */
#define GEI_STS_ASDV		0x00000300 /* Auto detected speed value */
#define GEI_STS_PCI66		0x00000800 /* 1 == 66Mhz bus, !82547 */
#define GEI_STS_BUS64		0x00001000 /* 64 bit bus */
#define GEI_STS_PCIX_MODE	0x00002000 /* PCI-X mode */
#define GEI_STS_PCIX_SPD	0x0000C000 /* PCIX bus speed */
#define GEI_STS_GIO_MSTR_STS	0x00080000 /* Status of master requests */
#define GEI_SSPEED_10		0x00000000
#define GEI_SSPEED_100		0x00000040
#define GEI_SSPEED_1000		0x00000080

#define GEI_FID(x)		(((x) & GEI_STS_FID) >> 2)

#define GEI_PCIXSPD_50_66MHZ	0x00000000
#define GEI_PCIXSPD_66_100MHZ	0x00004000
#define GEI_PCIXSPD_100_133MHZ	0x00008000

/* EEPROM/flash control register (must be used on 82544 or earlier) */

#define GEI_EECD_SK		0x00000001 /* EEPROM clock */
#define GEI_EECD_CS		0x00000002 /* EEPROM chip select */
#define GEI_EECD_DI		0x00000004 /* EEPROM data in */
#define GEI_EECD_DO		0x00000008 /* EEPROM data out */
#define GEI_EECD_FWE		0x00000030 /* Flash write enable */
#define GEI_EECD_EE_REQ		0x00000040 /* Request access (!82544) */
#define GEI_EECD_EE_GNT		0x00000080 /* Grant access (!82544) */
#define GEI_EECD_EE_PRES	0x00000100 /* EEPROM present (!82544) */
#define GEI_EECD_EE_SIZE	0x00000200 /* EEPROM size (!82544) */
#define GEI_EECD_EE_RELOAD_DONE	0x00000200 /* reload done (82573) */
#define GEI_EECD_EE_TYPE	0x00001000 /* EEPROM type (!82544) */
#define GEI_EECD_SEC1_VALID	0x00400000 /* NVRAM sector 1 valid */

#define GEI_EECD_SECVALID	(GEI_EECD_EE_PRES|GEI_EECD_EE_SIZE)

#define GEI_FWE_WRITE_DISABLE	0x00000010
#define GEI_FWE_WRITE_ENABLE	0x00000020

/* 9346 EEPROM commands */

#define GEI_9346_WRITE		0x5
#define GEI_9346_READ		0x6
#define GEI_9346_ERASE		0x7

/* EEPROM read register (not valid on 82544 or earlier) */

#define GEI_EERD_START		0x00000001 /* Initiate read */
#define GEI_EERD_DONE		0x00000010 /* Read completed */
#define GEI_EERD_ADDR		0x0000FF00 /* Read address */
#define GEI_EERD_DATA		0xFFFF0000 /* Returned data */

#define GEI_EEADDR(x)		(((x) << 8) & GEI_EERD_ADDR)
#define GEI_EEDATA(x)		(((x) & GEI_EERD_DATA) >> 16)

/* EEPROM read register layout for PCIe devices, which have large EEPROMs */

#define GEI_EERDPCIE_START	GEI_EERD_START
#define GEI_EERDPCIE_DONE	0x00000002
#define GEI_EERDPCIE_ADDR	0x0000FFFC
#define GEI_EERDPCIE_DATA	0xFFFF0000

#define GEI_EEPCIEADDR(x)	(((x) << 2) & GEI_EERDPCIE_ADDR)
#define GEI_EEPCIEDATA(x)	GEI_EEDATA(x)

/* Extended control register */

#define GEI_CTRLX_GPI0_EN	0x00000001 /* General purpose intr enables */
#define GEI_CTRLX_GPI1_EN	0x00000002 /* General purpose intr enables */
#define GEI_CTRLX_PHYINT_EN	GEI_CTRLX_GPI1_EN
#define GEI_CTRLX_GPI2_EN	0x00000004 /* General purpose intr enables */
#define GEI_CTRLX_GPI3_EN	0x00000008 /* General purpose intr enables */
#define GEI_CTRLX_SDP4_DATA	0x00000010
#define GEI_CTRLX_PHYINT	GEI_CTRLX_SDP4_DATA
#define GEI_CTRLX_SDP5_DATA	0x00000020
#define GEI_CTRLX_SDP6_DATA	0x00000040
#define GEI_CTRLX_SDP7_DATA	0x00000080
#define GEI_CTRLX_SDP4_DIR	0x00000100
#define GEI_CTRLX_SDP5_DIR	0x00000200
#define GEI_CTRLX_SDP6_DIR	0x00000400
#define GEI_CTRLX_SDP7_DIR	0x00000800
#define GEI_CTRLX_ASDCHK	0x00001000 /* Initiate ASD sequence */
#define GEI_CTRLX_EE_RST	0x00002000 /* EEPROM reset */
#define GEI_CTRLX_IPS		0x00004000 /* Invert power state */
#define GEI_CTRLX_SPD_BYPS	0x00008000 /* Speed select bypass */
#define GEI_CTRLX_RO_DIS	0x00020000 /* Relaxed ordering disable */
#define GEI_CTRLX_SERDES_LOPRW	0x00040000 /* SERDES low power */
#define GEI_CTRLX_DMA_DYNGATE	0x00080000 /* DMA dynamic clock gating */
#define GEI_CTRLX_PHY_PWRDN	0x00100000 /* PHY power down */
#define GEI_CTRLX_VREG_PWRDN	0x00200000 /* Voltage regulator power down */
#define GEI_CTRLX_LINKMODE	0x00C00000 /* Link mode */
#define GEI_CTRLX_PB_PAREN	0x01000000 /* Pktbuf parity error detect */
#define GEI_CTRLX_DF_PAREN	0x02000000 /* Desc FIFO parity error detect */
#define GEI_CTRLX_IAME		0x08000000 /* int ack automask enable */
#define GEI_CTRLX_DRV_LOAD	0x10000000 /* driver loaded */
#define GEI_CTRLX_INTTMRCLR_EN	0x20000000 /* interrupt timers clear */
#define GEI_CTRLX_HOST_PAREN	0x40000000 /* host data FIFO parity error */

#define GEI_LINKMODE_GMII	0x00000000
#define GEI_LINKMODE_KMRN	0x00000000
#define GEI_LINKMODE_1000BASEKX	0x00400000 /* 82580 */
#define GEI_LINKMODE_SGMII	0x00800000 /* 82580 */
#define GEI_LINKMODE_TBI	0x00C00000
#define GEI_LINKMODE_SERDES	0x00C00000 /* also 1000BaseBX for 82580 */

/* MDIC register */

#define GEI_MDIC_DATA		0x0000FFFF
#define GEI_MDIC_REGADD		0x001F0000
#define GEI_MDIC_PHYADD		0x03E00000
#define GEI_MDIC_OPCODE		0x0C000000
#define GEI_MDIC_READY		0x10000000
#define GEI_MDIC_INTEN		0x20000000
#define GEI_MDIC_ERROR		0x40000000

#define GEI_MDIO_WRITE		0x04000000
#define GEI_MDIO_READ		0x08000000

#define GEI_MDIO_REGADDR(x)	(((x) << 16) & GEI_MDIC_REGADD)
#define GEI_MDIO_PHYADDR(x)	(((x) << 21) & GEI_MDIC_PHYADD)

#define GEI_OUI_INTEL0		0x00AA00
#define GEI_OUI_INTEL1		0x005500
#define GEI_MODEL_IGP		0x0038

#define GEI_OUI_INTEL2		0x005500
#define GEI_MODEL_82577		0x0005

/* GLCI (Kumeran) control/status register */

#define GEI_KUMCTLSTS_VAL	0x0000FFFF	/* Read/write value */
#define GEI_KUMCTLSTS_OFF	0x001F0000	/* Register offset */
#define GEI_KUMCTLSTS_REN	0x00200000	/* Read enable */

#define GEI_KMRN_VAL(x)		((x) & GEI_KUMCTLSTS_VAL)
#define GEI_KMRN_OFFSET(x)	(((x) << 16) & GEI_KUMCTLSTS_OFF)

#define GEI_KMRN_EXTREG(page, offset)	(((page) << 5) | (offset))

/* Copper/Fiber switch control (82575/576/580) */

#define GEI_CONNSW_ASENSE_EN	0x00000001	/* autosense enable */
#define GEI_CONNSW_ASENSE_CF	0x00000002	/* autosense config mode */
#define GEI_CONNSW_ENRGSRC	0x00000004	/* SerDes energy detect src */
#define GEI_CONNSW_SERDESD	0x00000200	/* SerDes sig detect ind */
#define GEI_CONNSW_PHYSD	0x00000400	/* PHY sig detect ind */

/* Kumeran FIFO control register */

#define GEI_KMRN_FIFO_CTRL	0x00

#define GEI_KMRN_FIFO_CTRL_RX_BYPASS	0x0008
#define GEI_KMRN_FIFO_CTRL_TX_BYPASS	0x0800

/* Kumeran in-band control register */

#define GEI_KMRN_INB_CTRL	0x02

#define GEI_KMRN_INB_CTRL_DIS_PADDING	0x0010
#define GEI_KMRN_INB_CTRL_LINK_STS_TX_TIMEOUT	0x0500

/* Kumeran half duplex control register */

#define GEI_KMRN_HD_CTRL	0x10

#define GEI_KMRN_HD_CTRL_10_100		0x0004
#define GEI_KMRN_HD_CTRL_1000		0x0000

/*
 * The following register/page offsets are not documented
 * in any of the Intel manuals, but the Intel reference drivers
 * use them to work around a bug in ES2LAN devices where
 * timeouts can occur in 10Mbps mode.
 */

/* Kumeran PHY poll timer register */

#define GEI_KMRN_PHYPOLL_TIMER	GEI_KMRN_EXTREG(0x34, 4)

/* Kumeran PHY poll interation/count register */

#define GEI_KMRN_PHYPOLL_COUNT	GEI_KMRN_EXTREG(0x34, 9)

/* Extended config control (8257x only) */

#define GEI_EXTCNF_PHY_WRITE       0x00000002 /* allow access to extended PHY conf */
#define GEI_EXTCNF_DUD_EN          0x00000004 /* allow access to dock/undoc conf */
#define GEI_EXTCNF_DOCK_OWNER      0x00000010 /* determine who loads dock config */
#define GEI_EXTCNF_MDIO_SW         0x00000020 /* software owns MDIO */
#define GEI_EXTCNF_MDIO_HW         0x00000040 /* hardware owns MDIO */
#define GEI_EXTCNF_GATE_PHY_CFG    0x00000080 /* Gate/ungate PHY auto configuration by hardware */

/* PHY control register */

#define GEI_PHY_CTRL_SPD	0x00000001 /* Smart Powerdown enable */
#define GEI_PHY_CTRL_LPLUD0A	0x00000002 /* LPLU in D0a */
#define GEI_PHY_CTRL_LPLUND0A	0x00000004 /* LPLU in non-D0a */
#define GEI_PHY_CTRL_1GDISND0A	0x00000008 /* 1Gbps disable in non-D0A */
#define GEI_PHY_CTRL_1GDIS	0x00000400 /* 1Gbps disable (all states) */

/* 82574L BER improvement register */

#define GEI_BIAS_REG_1          29
#define GEI_BIAS_REG_2          30

/* LED control register */

#define GEI_LEDCTL_LED0MODE	0x0000000F
#define GEI_LEDCTL_LED0IVRT	0x00000040
#define GEI_LEDCTL_LED0BLNK	0x00000080
#define GEI_LEDCTL_LED1MODE	0x00000F00
#define GEI_LEDCTL_LED1IVRT	0x00004000
#define GEI_LEDCTL_LED1BLNK	0x00008000
#define GEI_LEDCTL_LED2MODE	0x000F0000
#define GEI_LEDCTL_LED2IVRT	0x00400000
#define GEI_LEDCTL_LED2BLNK	0x00800000
#define GEI_LEDCTL_LED3MODE	0x0F000000
#define GEI_LEDCTL_LED3IVRT	0x40000000
#define GEI_LEDCTL_LED3BLNK	0x80000000

#define GEI_LED0MODE(x)		((x) & GEI_LEDCTL_LED0MODE)
#define GEI_LED1MODE(x)		(((x) << 8) & GEI_LEDCTL_LED1MODE)
#define GEI_LED2MODE(x)		(((x) << 16) & GEI_LEDCTL_LED2MODE)
#define GEI_LED3MODE(x)		(((x) << 24) & GEI_LEDCTL_LED3MODE)

#define GEI_LEDMODE_LINK_10_1000	0
#define GEI_LEDMODE_LINK_100_1000	1
#define GEI_LEDMODE_LINK_UP		2	
#define GEI_LEDMODE_ACTIVITY		3
#define GEI_LEDMODE_LINK_ACTIVITY	4
#define GEI_LEDMODE_LINK_10		5
#define GEI_LEDMODE_LINK_100		6
#define GEI_LEDMODE_LINK_1000		7
#define GEI_LEDMODE_PCIX_MODE		8
#define GEI_LEDMODE_FULL_DUPLEX		9
#define GEI_LEDMODE_COLLISION		10
#define GEI_LEDMODE_BUS_SPEED		11
#define GEI_LEDMODE_BUS_SIZE		12
#define GEI_LEDMODE_PAUSED		14
#define GEI_LEDMODE_VCC_LED_ON		14
#define GEI_LEDMODE_GND_LED_OFF		15

/* Interrupt cause read register (also cause set, mask set/read, mask clear) */

#define GEI_ICR_TXDW		0x00000001 /* TX descriptor writeback */
#define GEI_ICR_TXQE		0x00000002 /* TX queue empty */
#define GEI_ICR_LSC		0x00000004 /* Link status change */
#define GEI_ICR_RXSEQ		0x00000008 /* RX sequence error (82543/4) */
#define GEI_ICR_RXDMT0		0x00000010 /* RX desc minimum threshold */
#define GEI_ICR_RXO		0x00000040 /* RX overrun */
#define GEI_ICR_RXT0		0x00000080 /* RX timer expired */
#define GEI_ICR_MDAC		0x00000200 /* MDI access complete */
#define GEI_ICR_RXCFG		0x00000400 /* Received /C/ ordered sets */
#define GEI_ICR_GPI_SDP1	0x00000800 /* general purpose int, 82544 */
#define GEI_ICR_PHYINT		0x00001000 /* Phy interrupt pin asserted */
#define GEI_ICR_SPI_SDP6	0x00002000 /* general purpose int, pin 6[2] */
#define GEI_ICR_SPI_SDP7	0x00004000 /* general purpose int, pin 7[3] */
#define GEI_ICR_TXD_LOW		0x00008000 /* TX desc low threshold (!82544) */
#define GEI_ICR_SRPD		0x00010000 /* RX small packet (!82544) */
#define GEI_ICR_OMED		0x00100000 /* Other link detect (82575/576/580) */

/*
 * The GEI_INT_PENDING bit is a software flag that must not conflict
 * with any of the ICR interrupt bits in GEI_INTRS (see above & below).
 */
#define GEI_INT_PENDING		0x80000000

/* RX control register */

#define GEI_RCTL_EN		0x00000002 /* Receiver enable */
#define GEI_RCTL_SBP		0x00000004 /* Store bad packets */
#define GEI_RCTL_UPE		0x00000008 /* Unicast promisc mode */
#define GEI_RCTL_MPE		0x00000010 /* Multicast promisc mode */
#define GEI_RCTL_LPE		0x00000020 /* Large (jumbo) packet receive */
#define GEI_RCTL_LBM		0x000000C0 /* Loopback mode */
#define GEI_RCTL_RDMTS		0x00000300 /* RX desc minimum threshold */
#define GEI_RCTL_MO		0x00003000 /* Multicast offset */
#define GEI_RCTL_BAM		0x00008000 /* Broadcast accept mode */
#define GEI_RCTL_BSIZE		0x00030000 /* RX buffer size */
#define GEI_RCTL_VFE		0x00040000 /* VLAN filter enable */
#define GEI_RCTL_CFIEN		0x00080000 /* Canonical Form Indicator Enable */
#define GEI_RCTL_CFI		0x00100000 /* Canonical Form Indicator val */
#define GEI_RCTL_DPF		0x00400000 /* Discard pause frames */
#define GEI_RCTL_PMCF		0x00800000 /* Pass MAC control frames */
#define GEI_RCTL_BSEX		0x02000000 /* BSIZE extention */
#define GEI_RCTL_SECRC		0x04000000 /* Strip ethernet CRC */

#define GEI_BSIZE_256		0x00030000
#define GEI_BSIZE_512		0x00020000
#define GEI_BSIZE_1024		0x00010000
#define GEI_BSIZE_2048		0x00000000
#define GEI_BSIZE_4096		(GEI_BSIZE_256 | GEI_RCTL_BSEX)
#define GEI_BSIZE_8192		(GEI_BSIZE_512 | GEI_RCTL_BSEX)
#define GEI_BSIZE_16384		(GEI_BSIZE_1024 | GEI_RCTL_BSEX)

#define GEI_MO_47_36		0x00000000
#define GEI_MO_46_35		0x00001000
#define GEI_MO_45_34		0x00002000
#define GEI_MO_43_32		0x00003000

/* General Purpose Interrupt Enable */

#define GEI_GPIE_NSICR		0x00000001
#define GEI_GPIE_MSIX_MODE	0x00000010
#define GEI_GPIE_EIAME		0x40000000
#define GEI_GPIE_PBA		0x80000000

/* Interrupt vector allocation register */

#define GEI_IVAR_ALLOC0		0x0000001F
#define GEI_IVAR_VALID0		0x00000080
#define GEI_IVAR_ALLOC1		0x00001F00
#define GEI_IVAR_VALID1		0x00008000
#define GEI_IVAR_ALLOC2		0x001F0000
#define GEI_IVAR_VALID2		0x00800000
#define GEI_IVAR_ALLOC3		0x1F000000
#define GEI_IVAR_VALID3		0x80000000

#define GEI_ALLOC0(x)		((x) & GEI_IVAR_ALLOC0)
#define GEI_ALLOC1(x)		(((x) << 8) & GEI_IVAR_ALLOC1)
#define GEI_ALLOC2(x)		(((x) << 16) & GEI_IVAR_ALLOC2)
#define GEI_ALLOC3(x)		(((x) << 24) & GEI_IVAR_ALLOC3)

#define GEI_RXQ 0
#define GEI_TXQ 1

/* Misc. interrupt vector allocation register */

#define GEI_IVARMISC_TCP	0x0000001F /* TCP timer interrupt */
#define GEI_IVARMIST_TCP_VALID	0x00000080
#define GEI_IVARMIST_OTH	0x00001F00 /* Other cause interrupts */
#define GEI_IVARMIST_OTH_VALID	0x00008000

/* Split and replication control register */

#define GEI_SRRCTL_BSIZEPKT	0x0000007F /* pkt size in 1K chunks */
#define GEI_SRRCTL_BSIZEHDR	0x00000F00 /* hdr size in 64 byte chunks */
#define GEI_SRRCTL_DESCTYPE	0x0E000000 /* RX descriptor type */
#define GEI_SRRCTL_DROP_ENB	0x80000000 /* drop when out of descs */

#define GEI_DESCTYPE_LEGACY	0x00000000 /* Legacy */
#define GEI_DESCTYPE_ADV_ONE	0x02000000 /* Advanced, one buffer */
#define GEI_DESCTYPE_ADV_SP	0x04000000 /* Advanced, header splitting */
#define GEI_DESCTYPE_ADV_REP	0x06000000 /* Advanced, header replication */
#define GEI_DESCTYPE_ADV_LRREP	0x08000000 /* Adv, hdr repl, large pkt only */
#define GEI_DESCTYPE_ADV_SPALW	0x0A000000 /* Adv, hdr split always */

/* Transmit control register */

#define GEI_TCTL_RST		0x00000001 /* Software reset */
#define GEI_TCTL_EN		0x00000002 /* TX enable */
#define GEI_TCTL_BCE		0x00000004 /* Busy check enable */
#define GEI_TCTL_PSP		0x00000008 /* Pad short packets */
#define GEI_TCTL_CT		0x00000FF0 /* Collision threshold */
#define GEI_TCTL_COLD		0x003FF000 /* Collision distance */
#define GEI_TCTL_SWXOFF		0x00400000 /* Software XOFF transmission */
#define GEI_TCTL_PBE		0x00800000 /* Packet burst enable */
#define GEI_TCTL_RTLC		0x01000000 /* Retransmit on late collision */
#define GEI_TCTL_NRTU		0x02000000 /* No rexmit on underrun (82544) */
#define GEI_TCTL_MULR		0x10000000 /* Multiple request support */

#define GEI_CT(x)	(((x) << 4) & GEI_TCTL_CT)
#define GEI_COLD(x)	(((x) << 12) & GEI_TCTL_COLD)

#define GEI_COLLTHRESH		15
#define GEI_COLLDIST_HDK_1000	0x200
#define GEI_COLLDIST_HDX	0x40

/* Transmit control register extended */

#define GEI_TCTLEXT_COLD	0x000FFC00
#define GEI_COLD_EXT(x)		(((x) << 10) & GEI_TCTLEXT_COLD)

/* Transmit IPG register */

#define GEI_TIPG_IPGT		0x000003FF /* IPG transmit time */
#define GEI_TIPG_IPGR1		0x000FFC00 /* IPG receive time 1 */
#define GEI_TIPG_IPGR2		0x3FF00000 /* IPG receive time 2 */

#define GEI_IPGT(x)		((x) & 0x000003FF)
#define GEI_IPGR1(x)		(((x) << 10) & 0x000FFC00)
#define GEI_IPGR2(x)		(((x) << 20) & 0x3FF00000)

/*
 * Default IPG values. Note that these values were slightly different
 * for the 82542, but we don't support that device with this driver.
 */

#define GEI_IPGT_DFLT_COPPER	8
#define GEI_IPGT_DFLT_FIBER	9
#define GEI_IPGR1_DFLT		8
#define GEI_IPGR1_ES2LAN_DFLT	9
#define GEI_IPGR2_DFLT		6
#define GEI_IPGR2_ES2LAN_DFLT	7

/* TX descriptor control */

#define GEI_TXDCTL_PTHRESH	0x0000003F /* Prefetch threshold  */
#define GEI_TXDCTL_HTHRESH	0x00003F00 /* Host threshold */
#define GEI_TXDCTL_WTHRESH	0x003F0000 /* Writeback threshold */
#define GEI_TXDCTL_COUNT_DESC	0x00400000 /* Count outstanding descs */
#define GEI_TXDCTL_GRAN		0x01000000 /* Granularity */
#define GEI_TXDCTL_ENABLE	0x02000000 /* Enable queue */
#define GEI_TXDCTL_SWFLUSH	0x04000000 /* TX software flush */
#define GEI_TXDCTL_LWTHRESH	0xFE000000 /* Low descriptor thresh (!82544) */

#define GEI_PTHRESH(x)		((x) & GEI_TXDCTL_PTHRESH)
#define GEI_HTHRESH(x)		(((x) << 8) & GEI_TXDCTL_HTHRESH)
#define GEI_WTHRESH(x)		(((x) << 16) & GEI_TXDCTL_WTHRESH)
#define GEI_LWTHRESH(x)		(((x) << 25) & GEI_TXDCTL_LWTHRESH)

/* RX descriptor control */

#define GEI_RXDCTL_PTHRESH	0x0000003F /* Prefetch threshold  */
#define GEI_RXDCTL_HTHRESH	0x00003F00 /* Host threshold */
#define GEI_RXDCTL_WTHRESH	0x003F0000 /* Writeback threshold */
#define GEI_RXDCTL_GRAN		0x01000000 /* Granularity */
#define GEI_RXDCTL_ENABLE	0x02000000 /* Enable queue */
#define GEI_RXDCTL_SWFLUSH	0x04000000 /* RX software flush */

/* RX checksum control */

#define GEI_RXCSUM_PCSS		0x000000FF /* Packet checksum start */
#define GEI_RXCSUM_IPOFLD	0x00000100 /* IP csum offload enable */
#define GEI_RXCSUM_TUOFLD	0x00000200 /* TCP/UDP csum offload enable */
#define GEI_RXCSUM_IPV6OFL	0x00000400 /* IPv6 enable (!82544) */
#define GEI_RXCSUM_CRCOFL	0x00000800 /* FCS offload enable */

/* Receive Filter Control Register */

#define GEI_RFCTL_NFSW_DIS      0x00000040 /* NFS Write Disable */
#define GEI_RFCTL_NFSR_DIS      0x00000080 /* NFS Read Disable */
#define GEI_RFCTL_EXSTEN        0x00008000 /* Extended Status Enable */

/* RX address excact match filter registers */

#define GEI_PARH_AS		0x00030000 /* Address select */
#define GEI_PARH_AV		0x80000000 /* Address valid */

#define GEI_AS_DST		0x00000000
#define GEI_AS_SRC		0x00010000

/* PCS configuration register (82575/576/580) */

#define GEI_PCSCFG_ENB		0x00000008 /* PCS enable */
#define GEI_PCSCFG_ISO		0x40000000 /* PCS isolate */
#define GEI_PCSCFG_SRESET	0x80000000 /* PCS soft reset */

/* PCS link control register (82575/576/580) */

#define GEI_PCSLCTL_FLV		0x00000001 /* Forced link value */
#define GEI_PCSLCTL_FSV		0x00000006 /* Forced speed value */
#define GEI_PCSLCTL_FDV		0x00000008 /* Forced duplex value */
#define GEI_PCSLCTL_FSD		0x00000010 /* Force speed + duplex */
#define GEI_PCSLCTL_FLNK	0x00000020 /* Force link up */
#define GEI_PCSLCTL_LNKLOW	0x00000040 /* Link latch low */
#define GEI_PCSLCTL_AN_ENB	0x00010000 /* Autoneg enable */
#define GEI_PCSLCTL_AN_RST	0x00020000 /* Autoneg restart */
#define GEI_PCSLCTL_AN_TMO	0x00040000 /* Autoneg timeout enable */
#define GEI_PCSLCTL_AN_SGMIIBYP	0x00080000 /* Autoneg SGMII bypass */
#define GEI_PCSLCTL_AN_SGMIITRG	0x00100000 /* Autoneg SGMII trigger */
#define GEI_PCSLCTL_FLT		0x01000000 /* Fast link timer */
#define GEI_PCSLCTL_LNKOK_FIX	0x02000000 /* Link OK fix enable */

/* PCS link status register (82575/576/580) */

#define GEI_PCSLSTS_LNKOK	0x00000001 /* Link OK */
#define GEI_PCSLSTS_SPEED	0x00000006 /* Speed */
#define GEI_PCSLSTS_DUPLEX	0x00000008 /* Duplex */
#define GEI_PCSLSTS_SYNCOK	0x00000010 /* Sync ok */
#define GEI_PCSLSTS_AN_DONE	0x00010000 /* Autoneg complete */
#define GEI_PCSLSTS_AN_TMO	0x00040000 /* Autoneg timed out */
#define GEI_PCSLSTS_AN_REMFLT	0x00080000 /* Autoneg remote failt */
#define GEI_PCSLSTS_AN_ERR	0x00100000 /* Autoneg error */

/* PCS autoneg advertisement (82575/576/580) */

#define GEI_PCSANAR_FDX		0x00000020 /* Full duplex */
#define GEI_PCSANAR_HDX		0x00000040 /* Half duplex */
#define GEI_PCSANAR_ASM		0x00000180 /* Pause capabilities */
#define GEI_PCSANAR_RFLT	0x00003000 /* Remote fault */
#define GEI_PCSANAR_NEXTP	0x00008000 /* Next page */


/* PCS link partner ability (82575/576/580) */

#define GEI_PCSLPAR_FDX		0x00000020 /* Full duplex */
#define GEI_PCSLPAR_HDX		0x00000040 /* Half duplex */
#define GEI_PCSLPAR_ASM		0x00000180 /* Pause capabilities */
#define GEI_PCSLPAR_SGMIISPD	0x00000C00 /* SGMII speed */
#define GEI_PCSLPAR_RFLT	0x00003000 /* Remote fault */
#define GEI_PCSLPAR_ACK		0x00004000 /* Ack next page */
#define GEI_PCSLPAR_NEXTP	0x00008000 /* Next page */

/*
 * EEPROM offsets
 */

#define GEI_EE_IA	0x00
#define GEI_EE_VID	0x0E

/*
 * ICH flash register information.
 * These registers are accessible via a separate BAR
 */

#define GEI_FL_GFP	0x0000
#define GEI_FL_HFSSTS	0x0004
#define GEI_FL_HFSCTL	0x0006
#define GEI_FL_FADDR	0x0008
#define GEI_FL_FDATA0	0x0010

/* Flash parameters, base page number and sector count */

#define GEI_FL_GFP_BASE		0x00001FFF
#define GEI_FL_GFP_SIZE		0x1FFF0000

#define GEI_FLASH_BASE(x)	((x) & GEI_FL_GFP_BASE)
#define GEI_FLASH_SIZE(x)	(((x) & GEI_FL_GFP_SIZE) >> 16)

/* Hardware sequencing flash status */

#define GEI_FL_HFSSTS_FLCDONE	0x0001 /* cycle done */
#define GEI_FL_HFSSTS_FLCERR	0x0002 /* cycle error */
#define GEI_FL_HFSSTS_DAEL	0x0004 /* Direct access error log */
#define GEI_FL_HFSSTS_ERSIZE	0x0018 /* Sector erase size */
#define GEI_FL_HFSSTS_FCLBUSY	0x0020 /* Cycle in progress */
#define GEI_FL_HFSSTS_DVALID	0x4000 /* Flash descriptor valid */
#define GEI_FL_HFSSTS_LOCK	0x8000 /* Config lockdown */

/* Hardware sequencing flash control */

#define GEI_FL_HFSCTL_FLCGO	0x0001 /* Initiate cycle */
#define GEI_FL_HFSCTL_FLCYCLE	0x0006 /* Flash cycle */
#define GEI_FL_HFSCTL_FLDCOUNT	0x0300 /* Flash data byte count */

#define GEI_FL_READ		0x0000
#define GEI_FL_WRITE		0x0004
#define GEI_FL_ERASE		0x0006

#define GEI_FL_SIZE_BYTE	0x0000
#define GEI_FL_SIZE_WORD	0x0100
#define GEI_FL_SIZE_LWORD	0x0200

#define GEI_FL_SECTOR_SIZE	4096

#define GEI_FLASH_VALID_WORD	0x13
#define GEI_FLASH_VALID_MASK	0xC0
#define GEI_FLASH_VALID_VALUE	0x80

/* 82579 PHY registers */

#define GEI_PHYPAGE(x)		((x) << 5)
#define GEI_PHYPAGE_REG		31

/* OEM bits register, PHY addr 1, page 0, offset 25 */

#define GEI_82579_OEMBITS	25
#define GEI_82579_OEMBITS_PAGE	0

#define GEI_82579_OEMBITS_ANEG	0x0400	/* Restart autoneg */
#define GEI_82579_OEMBITS_1GDIS	0x0040	/* 1GBps autoneg disabled */
#define GEI_82579_OEMBITS_LPLU	0x0004	/* Low Power Link Up */

/* Port control register, PHY addr 1, page 769, offset 16 */

#define GEI_82579_PCTRL		16
#define GEI_82579_PCTRL_PAGE	769

#define GEI_82579_PCTRL_MDIOF	0x0400

#define GEI_MDIOF_NORMAL	0x0000
#define GEI_MDIOF_REDUCED	0x0400

/*
 * Descriptor definitions
 */

typedef struct gei_rdesc
    {
    volatile UINT32	gei_addrlo;
    volatile UINT32	gei_addrhi;
    volatile UINT16	gei_len;
    volatile UINT16	gei_csum;
    volatile UINT8	gei_sts;
    volatile UINT8	gei_err;
    volatile UINT16	gei_special;
    } GEI_RDESC;


#define GEI_RDESC_STS_PIF	0x80 /* passed exact filter */
#define GEI_RDESC_STS_IPCS	0x40 /* IP checksum calculated */
#define GEI_RDESC_STS_TCPCS	0x20 /* TCP/UDP checksum calculated */
#define GEI_RDESC_STS_VP	0x08 /* Frame is VLAN tagged */
#define GEI_RDESC_STS_IXSM	0x04 /* Ignore checksum results */
#define GEI_RDESC_STS_EOP	0x02 /* End of packet */
#define GEI_RDESC_STS_DD	0x01 /* Descriptor done */

#define GEI_RDESC_ERR_RXE	0x80 /* RX data error */
#define GEI_RDESC_ERR_IPE	0x40 /* IP checksum error */
#define GEI_RDESC_ERR_TCPE	0x20 /* TCP/UDP checksum error */
#define GEI_RDESC_ERR_CXE	0x10 /* Carrier extension error */
#define GEI_RDESC_ERR_SEQ	0x04 /* Sequence error */
#define GEI_RDESC_ERR_SE	0x02 /* Symbol error */
#define GEI_RDESC_ERR_CE	0x01 /* CRC or alignment error */

#define GEI_RDESC_ERRSUM	\
    (GEI_RDESC_ERR_RXE|GEI_RDESC_ERR_CXE|GEI_RDESC_ERR_SEQ|	\
    GEI_RDESC_ERR_SE|GEI_RDESC_ERR_CE)

#define GEI_RDESC_SPC_PRI	0xE000 /* VLAN user priority field */
#define GEI_RDESC_SPC_CFI	0x1000 /* VLAN canonical form indicator */
#define GEI_RDESC_SPC_VLAN	0x0FFF /* VLAN identifier */

/* TCP/IP data descriptor format (DEXT = 1, DTYP = 1) */

typedef struct gei_tdesc
    {
    volatile UINT32	gei_addrlo;
    volatile UINT32	gei_addrhi;
    volatile UINT32	gei_cmd;
    volatile UINT8	gei_sts;
    volatile UINT8	gei_popts;
    volatile UINT16	gei_special;
    } GEI_TDESC;

/* Original legacy descriptor format (DEXT = 0, DTYP = 0) */

typedef struct gei_todesc
    {
    volatile UINT32	gei_addrlo;
    volatile UINT32	gei_addrhi;
    volatile UINT16	gei_len;
    volatile UINT8	gei_cso;
    volatile UINT8	gei_cmd;
    volatile UINT8	gei_sts;
    volatile UINT8	gei_css;
    volatile UINT16	gei_special;
    } GEI_TODESC;

/* TCP/IP context descriptor format (DEXT = 1, DTYP = 0) */

typedef struct gei_cdesc
    {
    volatile UINT8	gei_ipcss;
    volatile UINT8	gei_ipcso;
    volatile UINT16	gei_ipcse;
    volatile UINT8	gei_tucss;
    volatile UINT8	gei_tucso;
    volatile UINT16	gei_tucse;
    volatile UINT32	gei_cmd;
    volatile UINT8	gei_sts;
    volatile UINT8	gei_hdrlen;
    volatile UINT16	gei_mss;
    } GEI_CDESC;

#define GEI_OTDESC_CMD_IDE	0x80 /* Interrupt delay enable */
#define GEI_OTDESC_CMD_VLE	0x40 /* VLAN enable */
#define GEI_OTDESC_CMD_DEXT	0x20 /* Extension */
#define GEI_OTDESC_CMD_RPS	0x10 /* Report packet sent (82544 only) */
#define GEI_OTDESC_CMD_RS	0x08 /* Report status */
#define GEI_OTDESC_CMD_IC	0x04 /* Insert checksum */
#define GEI_OTDESC_CMD_IFCS	0x02 /* Insert FCS */
#define GEI_OTDESC_CMD_EOP	0x01 /* End of packet */

#define GEI_TDESC_STS_TU	0x08 /* Transmit underrun */
#define GEI_TDESC_STS_LC	0x04 /* Late collision */
#define GEI_TDESC_STS_EC	0x02 /* Excess collisions */
#define GEI_TDESC_STS_DD	0x01 /* Descriptor done */

#define GEI_TDESC_SPC_PRI	0xE000 /* VLAN user priority field */
#define GEI_TDESC_SPC_CFI	0x1000 /* VLAN canonical form indicator */
#define GEI_TDESC_SPC_VLAN	0x0FFF /* VLAN identifier */

#define GEI_TDESC_DTYP_CTX	0x00000000 /* TCP/IP context descriptor */
#define GEI_TDESC_DTYP_DSC	0x00100000 /* TCP/IP data descriptor */

#define GEI_CDESC_CMD_IDE	0x80000000 /* Interrupt delay enable */
#define GEI_CDESC_CMD_DEXT	0x20000000 /* Extension */
#define GEI_CDESC_CMD_RS	0x08000000 /* Report status */
#define GEI_CDESC_CMD_TSE	0x04000000 /* TCP segmentation enable */
#define GEI_CDESC_CMD_IP	0x02000000 /* IP packet */
#define GEI_CDESC_CMD_TCP	0x01000000 /* TCP packet */
#define GEI_CDESC_CMD_DTYP	0x00F00000 /* Descriptor type */
#define GEI_CDESC_CMD_LEN	0x000FFFFF /* data length */

#define GEI_TDESC_CMD_IDE	0x80000000 /* Interrupt delay enable */
#define GEI_TDESC_CMD_VLE	0x40000000 /* VLAN enable */
#define GEI_TDESC_CMD_DEXT	0x20000000 /* Extension */
#define GEI_TDESC_CMD_RPS	0x10000000 /* Report packet sent (82544 only) */
#define GEI_TDESC_CMD_RSV	0x10000000 /* TCP segment sent (82544 only) */
#define GEI_TDESC_CMD_RS	0x08000000 /* Report status */
#define GEI_TDESC_CMD_TSE	0x04000000 /* TCP segmentation enable */
#define GEI_TDESC_CMD_IFCS	0x02000000 /* Insert FCS */
#define GEI_TDESC_CMD_EOP	0x01000000 /* End of packet */
#define GEI_TDESC_CMD_DTYP	0x00F00000 /* Descriptor type */
#define GEI_TDESC_CMD_LEN	0x000FFFFF /* data length */

#define GEI_TDESC_OPT_IXSM	0x01 /* Insert IP header checksum */
#define GEI_TDESC_OPT_TXSM	0x02 /* Insert TCP/UDP checksum */

#define GEI_IP_CSUM_OFFSET	10

/*
 * The 82575 (and 82576) use an advanced descriptor format for RX
 * and TX descriptors.
 */

/* Advanced RX descriptor format */

typedef union gei_adv_rdesc
    {
    struct
        {
        volatile UINT32	gei_addrlo;
        volatile UINT32	gei_addrhi;
        volatile UINT8  gei_sts;
        volatile UINT8	gei_hdrlo0;
        volatile UINT16	gei_hdrlo1;
        volatile UINT32 gei_hdrhi;
        } read;
    struct
        {
        volatile UINT32	gei_pktinfo;
        volatile UINT16	gei_ipid;
        volatile UINT16	gei_pktcsum;
        volatile UINT32	gei_errsts;
        volatile UINT16	gei_len;
        volatile UINT16 gei_vlan;
        } write;
    } GEI_ADV_RDESC;

#define GEI_ADV_RINFO_RSSTYPE		0x0000000F
#define GEI_ADV_RINFO_PKTTYPE		0x0000FFF0
#define GEI_ADV_RINFO_RSVD		0x001F0000
#define GEI_ADV_RINFO_HDRLEN		0x7FE00000
#define GEI_ADV_RINFO_SPH		0x80000000	/* Split header */

#define GEI_ADV_RTYPE_NONE		0x00000000	/* No hash */
#define GEI_ADV_RTYPE_TCPIPV4		0x00000001
#define GEI_ADV_RTYPE_IPV4		0x00000002
#define GEI_ADV_RTYPE_TCPIPV6		0x00000003
#define GEI_ADV_RTYPE_IPV6_EX		0x00000004
#define GEI_ADV_RTYPE_IPV6		0x00000005
#define GEI_ADV_RTYPE_TCPIPV6_EX	0x00000006
#define GEI_ADV_RTYPE_UDPIPV4		0x00000007
#define GEI_ADV_RTYPE_UDPIPV6		0x00000008
#define GEI_ADV_RTYPE_UDPIPV6_EX	0x00000009

#define GEI_ADV_PTYPE_IPV4		0x00000010
#define GEI_ADV_PTYPE_IPV4E		0x00000020
#define GEI_ADV_PTYPE_IPV6		0x00000040
#define GEI_ADV_PTYPE_IPV6E		0x00000080
#define GEI_ADV_PTYPE_TCP		0x00000100
#define GEI_ADV_PTYPE_UDP		0x00000200
#define GEI_ADV_PTYPE_SCTP		0x00000400
#define GEI_ADV_PTYPE_NFS		0x00000800

#define GEI_ADV_RDESC_STS_DD		0x00000001
#define GEI_ADV_RDESC_STS_EOP		0x00000002
#define GEI_ADV_RDESC_STS_IXSM		0x00000004
#define GEI_ADV_RDESC_STS_VLAN		0x00000008
#define GEI_ADV_RDESC_STS_UDPCS		0x00000010
#define GEI_ADV_RDESC_STS_TCPCS		0x00000020
#define GEI_ADV_RDESC_STS_IPCS		0x00000040
#define GEI_ADV_RDESC_STS_PIF		0x00000080
#define GEI_ADV_RDESC_STS_CRCV		0x00000100
#define GEI_ADV_RDESC_STS_VEXT		0x00000200
#define GEI_ADV_RDESC_STS_UDPV		0x00000400	/* UDP CSUM valid */
#define GEI_ADV_RDESC_STS_DYNINT	0x00000800
#define GEI_ADV_RDESC_ERR_HBO		0x00800000
#define GEI_ADV_RDESC_ERR_CE		0x01000000
#define GEI_ADV_RDESC_ERR_SE		0x02000000
#define GEI_ADV_RDESC_ERR_LE		0x10000000
#define GEI_ADV_RDESC_ERR_TCPE		0x20000000
#define GEI_ADV_RDESC_ERR_IPE		0x40000000
#define GEI_ADV_RDESC_ERR_RXE		0x80000000

#define GEI_ADV_RDESC_ERRSUM	\
    (GEI_ADV_RDESC_ERR_HBO|GEI_ADV_RDESC_ERR_CE|	\
     GEI_ADV_RDESC_ERR_LE|GEI_ADV_RDESC_ERR_RXE|GEI_ADV_RDESC_ERR_SE)


/* Advanced TCP/IP context descriptor format (DEXT = 1, DTYP = 2) */

typedef struct gei_adv_cdesc
    {
    volatile UINT16	gei_macip;
    volatile UINT16	gei_vlan;
    volatile UINT32	gei_seqnum;
    volatile UINT32	gei_cmd;
    volatile UINT8	gei_idx;
    volatile UINT8	gei_l4len;
    volatile UINT16	gei_mss;
    } GEI_ADV_CDESC;

#define GEI_ADV_CDESC_MACIP_IP	0x01FF		/* IP header length */
#define GEI_ADV_CDESC_MACIP_MAC	0xFE00		/* Frame header length */

#define GEI_ADV_IPLEN(x)	((x) & GEI_ADV_CDESC_MACIP_IP)
#define GEI_ADV_MACLEN(x)	(((x) << 9) & GEI_ADV_CDESC_MACIP_MAC)

#define GEI_ADV_CDESC_CMD_DEXT	0x20000000	/* 1 = advanced */
#define GEI_ADV_CDESC_CMD_DTYP	0x00F00000	/* Descriptor type */
#define GEI_ADV_CDESC_CMD_MRKRQ	0x00002000	/* Markers required */
#define GEI_ADV_CDESC_CMD_L4T	0x00001800	/* L4 packet type */
#define GEI_ADV_CDESC_CMD_IPV4	0x00000400	/* 1 = IPV4, 0 = IPV6 */
#define GEI_ADV_CDESC_CMD_SNAP	0x00000200	/* SNAP frame */
#define GEI_ADV_CDESC_CMD_MKRL	0x000000FF	/* Marker offset */

#define GEI_ADV_CDESC_L4T_UDP	0x00000000
#define GEI_ADV_CDESC_L4T_TCP	0x00000800

/* Advanced TCP/IP data descriptor format (DEXT = 1, DTYP = 3) */

typedef struct gei_adv_tdesc
    {
    volatile UINT32	gei_addrlo;
    volatile UINT32	gei_addrhi;
    volatile UINT32	gei_cmd;
    volatile UINT32	gei_sts;
    } GEI_ADV_TDESC;

#define GEI_ADV_TDESC_CMD_TSE	0x80000000	/* TSO enable */
#define GEI_ADV_TDESC_CMD_VLE	0x40000000	/* VLAN insertion enable */
#define GEI_ADV_TDESC_CMD_DEXT	0x20000000	/* 1 = advanced */
#define GEI_ADV_TDESC_CMD_RS	0x08000000	/* report status */
#define GEI_ADV_TDESC_CMD_IFCS	0x02000000	/* Insert FCS */
#define GEI_ADV_TDESC_CMD_EOP	0x01000000	/* End of packet */
#define GEI_ADV_TDESC_CMD_DTYP	0x00F00000	/* Descriptor type */
#define GEI_ADV_TDESC_CMD_LEN	0x0000FFFF	/* Data length */

#define GEI_AVD_TDESC_STS_STA	0x0000000F	/* Status */
#define GEI_ADV_TDESC_STS_IDX	0x000000F0	/* Context index */
#define GEI_ADV_TDESC_STS_POPTS	0x00003F00	/* Packet options */
#define GEI_ADV_TDESC_STS_PAYLN	0xFFFFC000	/* Payload length */

#define GEI_ADV_STS_DD		0x00000001
#define GEI_ADV_IDX(x)		(((x) << 4) & GEI_ADV_TDESC_STS_IDX)
#define GEI_ADV_PAYLEN(x)	(((x) << 14) & GEI_ADV_TDESC_STS_PAYLN)

#define GEI_ADV_POPT_IXSM	0x00000100	/* Insert IP header checksum */
#define GEI_ADV_POPT_TXSM	0x00000200	/* Insert TCP/UDP checksum */

#define GEI_ADV_TDESC_DTYP_CTX	0x00200000	/* TCP/IP context descriptor */
#define GEI_ADV_TDESC_DTYP_DSC	0x00300000	/* TCP/IP data descriptor */

#define GEI_MTU		1500
#define GEI_JUMBO_MTU	9000
#define GEI_CLSIZE	1536
#define GEI_NAME	"gei"
#define GEI_TIMEOUT	10000
#define GEI_INTRS	(GEI_RXINTRS|GEI_TXINTRS|GEI_LINKINTRS)
#define GEI_RXINTRS	(GEI_ICR_RXO|GEI_ICR_RXT0)
#define GEI_TXINTRS	GEI_ICR_TXDW
#define GEI_LINKINTRS	GEI_ICR_LSC

/* The PRO/1000 hardware requires at least 128 descriptors per DMA ring */

#define GEI_RX_DESC_CNT 128
#define GEI_TX_DESC_CNT 128

#define GEI_DEFAULT_TUPLE_CNT_FACTOR 3

#define GEI_TX_MARK_THRESH 8
#define GEI_TX_CLEAN_THRESH 32

#define GEI_MAXFRAG 16
#define GEI_MAX_RX 32

/*#define GEI_INC_DESC(x, y)	(x) = (((x) + 1) % y)*/
#define GEI_INC_DESC(x, y)     (x) = ((x + 1) & (y - 1))

#define GEI_ADDR_LO(y)	((UINT32)((UINT64)(y) & 0xFFFFFFFF))
#define GEI_ADDR_HI(y)	((UINT32)(((UINT64)(y) >> 32) & 0xFFFFFFFF))

#define GEI_ADJ(m)		\
    do	{			\
	(m)->m_data += 2;	\
	(m)->m_len  -= 2;	\
	} while (FALSE)

/* Carrier extension byte. */

#define GEI_CARREXT	0x0F

struct gei_drv_ctrl;
typedef void (*geiIntFunc)(struct gei_drv_ctrl *);

/*
 * Private adapter context structure.
 */

typedef struct gei_drv_ctrl
    {
    END_OBJ		geiEndObj;
    VXB_DEVICE_ID	geiDev;
    void *		geiBar;
    void *		geiFlashBar;
    void *		geiIoBar;
    void *		geiHandle;

    void *		geiFlashHandle;
    void *		geiIoHandle;

    JOB_QUEUE_ID	geiJobQueue;
    QJOB		geiIntJob;
    UINT32		geiIntrs;
    UINT32		geiMoreRx;
    atomic32_t		geiIntPending;

    BOOL		geiPolling;
    M_BLK_ID		geiPollBuf;
    UINT32		geiIntMask;

    UINT8		geiAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	geiCaps;

    END_IFDRVCONF	geiEndStatsConf;
    END_IFCOUNTERS	geiEndStatsCounters;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*geiMediaList;
    END_ERR		geiLastError;
    UINT32		geiCurMedia;
    UINT32		geiCurStatus;
    VXB_DEVICE_ID	geiMiiBus;
    /* End MII/ifmedia required fields */

#ifdef GEI_VXB_DMA_BUF
    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	geiParentTag;

    VXB_DMA_TAG_ID	geiRxDescTag;
    VXB_DMA_MAP_ID	geiRxDescMap;
#endif
    GEI_RDESC *		geiRxDescMem;

#ifdef GEI_VXB_DMA_BUF
    VXB_DMA_TAG_ID	geiTxDescTag;
    VXB_DMA_MAP_ID	geiTxDescMap;
#endif
    GEI_TDESC *		geiTxDescMem;

#ifdef GEI_VXB_DMA_BUF
    VXB_DMA_TAG_ID	geiMblkTag;

    VXB_DMA_MAP_ID	geiRxMblkMap[GEI_RX_DESC_CNT];
    VXB_DMA_MAP_ID	geiTxMblkMap[GEI_TX_DESC_CNT];
#endif

    M_BLK_ID		geiTxMblk[GEI_TX_DESC_CNT];
    M_BLK_ID		geiRxMblk[GEI_RX_DESC_CNT];

    UINT32              geiTxProd;
    UINT32              geiTxCons;
    UINT32              geiTxFree;
    UINT32		geiTxSinceMark;
    BOOL                geiTxStall;

    UINT32              geiRxIdx;

    int			geiMaxMtu;
    UINT16		geiDevId;
    UINT8		geiRevId;
    int			geiDevType;
    VXB_DEVICE_ID	geiMiiDev;
    FUNCPTR		geiMiiPhyRead;
    FUNCPTR		geiMiiPhyWrite;
    int			geiMiiPhyAddr;
    int			geiEeWidth;
#define	geiFlashBankSize	geiEeWidth

    SEM_ID		geiDevSem;

    BOOL		geiTbiCompat;
    BOOL		geiTbi;
    BOOL		geiSerDes;
    BOOL		gei82544PcixWar;
    UINT32		geiLastCtx;
#ifdef CSUM_IPHDR_OFFSET
    int			geiLastOffsets;
#else
    int			geiLastIpLen;
#endif
    UINT16		geiLastVlan;
#ifndef GEI_VXB_DMA_BUF
    char *              geiDescBuf;  /* possibly unaligned desc buffer to free */
#endif

    void *		geiMuxDevCookie;

    geiIntFunc		geiIntFunc;

    int			geiParCnt;
    int			geiMarCnt;

#if (defined _WRS_CONFIG_PWR_MGMT) && (defined _WRS_ARCH_HAS_DEV_PWR_MGMT)
    UINT8		geiPmcsr;
    UINT32		geiWol;
    UINT32		geiPciConfig[16];  /* This storage space may fall away as generic 
			                      Power Management implementation progress */
    UINT32		geiPhyPwrDown;
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */

    UINT32		tupleCntFactor;
    } GEI_DRV_CTRL;

#if (CPU_FAMILY == I80X86)

#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((ULONG)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((ULONG)pDev->pRegBase[0] + addr);    \
            *(pReg) = (UINT32)(data);                           \
        } while (FALSE)

#else /* CPU_FAMILY == I80X86 */

#define GEI_BAR(p)   ((GEI_DRV_CTRL *)(p)->pDrvCtrl)->geiBar
#define GEI_HANDLE(p)   ((GEI_DRV_CTRL *)(p)->pDrvCtrl)->geiHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (GEI_HANDLE(pDev), (UINT32 *)((char *)GEI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (GEI_HANDLE(pDev),                             \
        (UINT32 *)((char *)GEI_BAR(pDev) + addr), data)

#endif /* CPU_FAMILY == I80X86 */

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))

/*
 * Macros for accessing the flash on ICH8/9 devices only.
 */

#define FL_BAR(p)   ((GEI_DRV_CTRL *)(p)->pDrvCtrl)->geiFlashBar
#define FL_HANDLE(p)   ((GEI_DRV_CTRL *)(p)->pDrvCtrl)->geiFlashHandle

#define FL_READ_4(pDev, addr)                                  \
    vxbRead32 (FL_HANDLE(pDev), (UINT32 *)((char *)FL_BAR(pDev) + addr))

#define FL_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FL_HANDLE(pDev),                             \
        (UINT32 *)((char *)FL_BAR(pDev) + addr), data)

#define FL_READ_2(pDev, addr)                                  \
    vxbRead16 (FL_HANDLE(pDev), (UINT16 *)((char *)FL_BAR(pDev) + addr))

#define FL_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (FL_HANDLE(pDev),                             \
        (UINT16 *)((char *)FL_BAR(pDev) + addr), data)

#define FL_SETBIT_4(pDev, offset, val)          \
        FL_WRITE_4(pDev, offset, FL_READ_4(pDev, offset) | (val))

#define FL_CLRBIT_4(pDev, offset, val)          \
        FL_WRITE_4(pDev, offset, FL_READ_4(pDev, offset) & ~(val))


#define FL_SETBIT_2(pDev, offset, val)          \
        FL_WRITE_2(pDev, offset, FL_READ_2(pDev, offset) | (val))

#define FL_CLRBIT_2(pDev, offset, val)          \
        FL_WRITE_2(pDev, offset, FL_READ_2(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCgei825xxVxbEndh */
