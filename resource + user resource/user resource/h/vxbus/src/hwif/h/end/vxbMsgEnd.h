/* vxbMsgEnd.h - message virtual END driver */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,20dec12,j_z  add statistical variable.
01a,19sep12,j_z  written
*/

/*
DESCRIPTION
This module implements a virtual message net driver.

SEE ALSO: vxBus, ifLib, miiBus
*/

#ifndef __INC_vxbMsgEndH
#define __INC_vxbMsgEndH

/* includes */

#include <hwif/util/vxbMsgSupport.h>

/* defines */

#define MSGNET_NAME             "msgEnd"
#define MSGNET_TIMEOUT          10000

#define MSGNET_INC_DESC(x, y)   (x) = ((x + 1) & (y - 1))

#define MSGNET_RX_DESC_CNT      256
#define MSGNET_TX_DESC_CNT      256

#define MSGNET_ENET_1           0x0

typedef struct msgnet_drv_ctrl
    {
    END_OBJ             msgEndObj;
    VXB_DEVICE_ID       msgNetDev;
    VXB_DEVICE_ID       msgNetServDev;
    char *              msgNetServName;
    UINT32              msgNetServUnit;
    ULONG               msgNetRxArg;
    ULONG               msgNetTxArg;
    UINT32              msgNetHwAdrs;
    BOOL                msgNetJumboEnable;
    UINT32              msgNetTxDescCnt;
    UINT32              msgNetPktMtu;
    END_ERR             msgNetLastError;
    void *              msgNetMuxDevCookie;
    SEM_ID              msgNetDevSem;
    UINT32              msgNetTxProd;
    UINT32              msgNetTxCons;
    UINT32              msgNetMaxMtu;
    UINT32              msgNetTxFree;
    volatile BOOL       msgNetTxStall;
    int                 msgNetIntPending;
    M_BLK_ID            msgNetTxMblk [MSGNET_TX_DESC_CNT];
    UINT8               msgNetAddr [ETHER_ADDR_LEN];
    M_BLK_ID            msgNetPollBuf;
    MSG_TRANS_API    *  msgNetApi;
    END_IFCOUNTERS      msgNetStatsCounters;
    } MSGNET_DRV_CTRL;

#endif /* __INC_vxbMsgEndH */

