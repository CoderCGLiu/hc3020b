/* an983VxbEnd.h - header file for an VxBus ENd driver */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,06jul07,wap  Make this driver SMP safe, convert to new register
                 access API
01b,29jan07,wap  Correct RX fixup issue on MIPS32 arch
01a,11aug06,wap  written
*/

#ifndef __INCan983VxbEndh
#define __INCan983VxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void anRegister (void);

#ifndef BSP_VERSION

#define AN_VENDORID_ADMTEK	0x1317

#define AN_DEVICEID_AN983	0x0985
#define AN_DEVICEID_FA511	0x1985
#define AN_DEVICEID_ADM9511	0x9511
#define AN_DEVICEID_ADM9513	0x9511

#define AN_VENDORID_3COM	0x10b7

#define AN_DEVICEID_3CSOHOB	0x9300

#define AN_VENDORID_MICROSOFT	0x1414

#define AN_DEVICEID_MSMN120	0x0001
#define AN_DEVICEID_MSMN130	0x0002

#define AN_BUSCTL		0x00    /* bus control */
#define AN_TXSTART		0x08    /* tx start demand */
#define AN_RXSTART		0x10    /* rx start demand */
#define AN_RXADDR		0x18    /* rx descriptor list start addr */
#define AN_TXADDR		0x20    /* tx descriptor list start addr */
#define AN_ISR			0x28    /* interrupt status register */
#define AN_NETCFG		0x30    /* network config register */
#define AN_IMR			0x38    /* interrupt mask */
#define AN_FRAMESDISCARDED	0x40    /* # of discarded frames */
#define AN_SIO			0x48    /* MII and ROM/EEPROM access */
#define AN_TIMER		0x58    /* general purpose timer */
#define AN_WKUPCTL		0x60	/* wakeup control/status register */
#define AN_WDOG			0x78	/* watchdog timer */
#define AN_AUXISR		0x80	/* auxilliary interrupt status */
#define AN_AUXIMR		0x84	/* auxilliary interrupt mask */
#define AN_CR			0x88    /* command register */
#define AN_PAR0			0xA4    /* station address */
#define AN_PAR1			0xA8    /* station address */
#define AN_MAR0			0xAC    /* multicast hash filter */
#define AN_MAR1			0xB0    /* multicast hash filter */
#define AN_UAR0			0xB4    /* unicast hash filter */
#define AN_UAR1			0xB8    /* unicast hash filter */
#define AN_OMR			0xFC	/* operating mode register */

/*
 * Bus control bits.
 */
#define AN_BUSCTL_RESET         0x00000001
#define AN_BUSCTL_ARBITRATION   0x00000002
#define AN_BUSCTL_SKIPLEN       0x0000007C
#define AN_BUSCTL_BUF_BIGENDIAN 0x00000080
#define AN_BUSCTL_BURSTLEN      0x00003F00
#define AN_BUSCTL_CACHEALIGN    0x0000C000
#define AN_BUSCTL_TXPOLL        0x000E0000
#define AN_BUSCTL_DBO           0x00100000
#define AN_BUSCTL_MRME          0x00200000
#define AN_BUSCTL_MRLE          0x00800000
#define AN_BUSCTL_MWIE          0x01000000
#define AN_BUSCTL_ONNOW_ENB     0x04000000

#define AN_SKIPLEN_1LONG        0x00000004
#define AN_SKIPLEN_2LONG        0x00000008
#define AN_SKIPLEN_3LONG        0x00000010
#define AN_SKIPLEN_4LONG        0x00000020
#define AN_SKIPLEN_5LONG        0x00000040

#define AN_CACHEALIGN_NONE      0x00000000
#define AN_CACHEALIGN_8LONG     0x00004000
#define AN_CACHEALIGN_16LONG    0x00008000
#define AN_CACHEALIGN_32LONG    0x0000C000

#define AN_BURSTLEN_USECA       0x00000000
#define AN_BURSTLEN_1LONG       0x00000100
#define AN_BURSTLEN_2LONG       0x00000200
#define AN_BURSTLEN_4LONG       0x00000400
#define AN_BURSTLEN_8LONG       0x00000800
#define AN_BURSTLEN_16LONG      0x00001000
#define AN_BURSTLEN_32LONG      0x00002000

#define AN_TXPOLL_OFF           0x00000000
#define AN_TXPOLL_1             0x00020000
#define AN_TXPOLL_2             0x00040000
#define AN_TXPOLL_3             0x00060000
#define AN_TXPOLL_4             0x00080000
#define AN_TXPOLL_5             0x000A0000
#define AN_TXPOLL_6             0x000C0000
#define AN_TXPOLL_7             0x000E0000

/*
 * Interrupt status bits.
 */
#define AN_ISR_TX_OK            0x00000001
#define AN_ISR_TX_IDLE          0x00000002
#define AN_ISR_TX_NOBUF         0x00000004
#define AN_ISR_TX_JABBERTIMEO   0x00000008
#define AN_ISR_TX_UNDERRUN      0x00000020
#define AN_ISR_RX_OK            0x00000040
#define AN_ISR_RX_NOBUF         0x00000080
#define AN_ISR_RX_READ          0x00000100
#define AN_ISR_RX_WATDOGTIMEO   0x00000200
#define AN_ISR_TIMER_EXPIRED    0x00000800
#define AN_ISR_BUS_ERR          0x00002000
#define AN_ISR_ABNORMAL         0x00008000
#define AN_ISR_NORMAL           0x00010000
#define AN_ISR_RX_STATE         0x000E0000
#define AN_ISR_TX_STATE         0x00700000
#define AN_ISR_BUSERRTYPE       0x03800000

#define AN_RXSTATE_STOPPED      0x00000000      /* 000 - Stopped */
#define AN_RXSTATE_FETCH        0x00020000      /* 001 - Fetching descriptor */
#define AN_RXSTATE_ENDCHECK     0x00040000      /* 010 - check for rx end */
#define AN_RXSTATE_WAIT         0x00060000      /* 011 - waiting for packet */
#define AN_RXSTATE_SUSPEND      0x00080000      /* 100 - suspend rx */
#define AN_RXSTATE_CLOSE        0x000A0000      /* 101 - close tx desc */
#define AN_RXSTATE_FLUSH        0x000C0000      /* 110 - flush from FIFO */
#define AN_RXSTATE_DEQUEUE      0x000E0000      /* 111 - dequeue from FIFO */

#define AN_TXSTATE_RESET        0x00000000      /* 000 - reset */
#define AN_TXSTATE_FETCH        0x00100000      /* 001 - fetching descriptor */
#define AN_TXSTATE_WAITEND      0x00200000      /* 010 - wait for tx end */
#define AN_TXSTATE_READING      0x00300000      /* 011 - read and enqueue */
#define AN_TXSTATE_RSVD         0x00400000      /* 100 - reserved */
#define AN_TXSTATE_SETUP        0x00500000      /* 101 - setup packet */
#define AN_TXSTATE_SUSPEND      0x00600000      /* 110 - suspend tx */
#define AN_TXSTATE_CLOSE        0x00700000      /* 111 - close tx desc */

/*
 * Network config bits.
 */

#define AN_NETCFG_RX_ON         0x00000002
#define AN_NETCFG_RX_BADFRAMES  0x00000008
#define AN_NETCFG_BACKOFFCNT    0x00000020
#define AN_NETCFG_RX_PROMISC    0x00000040
#define AN_NETCFG_RX_ALLMULTI   0x00000080
#define AN_NETCFG_LOOPBACK      0x00000C00
#define AN_NETCFG_FORCECOLL     0x00001000
#define AN_NETCFG_TX_ON         0x00002000
#define AN_NETCFG_TX_THRESH     0x0000C000
#define AN_NETCFG_HEARTBEAT     0x00080000
#define AN_NETCFG_STORENFWD     0x00200000

#define AN_OPMODE_NORM          0x00000000
#define AN_OPMODE_INTLOOP       0x00000400
#define AN_OPMODE_EXTLOOP       0x00000800

#define AN_TXTHRESH_72BYTES     0x00000000
#define AN_TXTHRESH_96BYTES     0x00004000
#define AN_TXTHRESH_128BYTES    0x00008000
#define AN_TXTHRESH_160BYTES    0x0000C000

#define AN_TXTHRESH_MIN         0x00000000
#define AN_TXTHRESH_INC         0x00004000
#define AN_TXTHRESH_MAX         0x0000C000

/*
 * Serial I/O (EEPROM/ROM) bits.
 */
#define AN_SIO_EE_CS            0x00000001      /* EEPROM chip select */
#define AN_SIO_EE_CLK           0x00000002      /* EEPROM clock */
#define AN_SIO_EE_DATAIN        0x00000004      /* EEPROM data output */  
#define AN_SIO_EE_DATAOUT       0x00000008      /* EEPROM data input */   
#define AN_SIO_ROMDATA4         0x00000010
#define AN_SIO_ROMDATA5         0x00000020
#define AN_SIO_ROMDATA6         0x00000040
#define AN_SIO_ROMDATA7         0x00000080
#define AN_SIO_EESEL            0x00000800
#define AN_SIO_ROMSEL           0x00001000
#define AN_SIO_ROMCTL_WRITE     0x00002000
#define AN_SIO_ROMCTL_READ      0x00004000
#define AN_SIO_MII_CLK          0x00010000      /* MDIO clock */
#define AN_SIO_MII_DATAOUT      0x00020000      /* MDIO data out */
#define AN_SIO_MII_DIR          0x00040000      /* MDIO dir */
#define AN_SIO_MII_DATAIN       0x00080000      /* MDIO data in */

#define AN_EECMD_WRITE          0x140
#define AN_EECMD_READ           0x180
#define AN_EECMD_ERASE          0x1c0

/* 9346 EEPROM commands */
#define AN_9346_WRITE		0x5
#define AN_9346_READ		0x6
#define AN_9346_ERASE		0x7
#define AN_9346_EWEN		0x4
#define AN_9346_EWEN_ADDR	0x30
#define AN_9456_EWDS		0x4
#define AN_9346_EWDS_ADDR	0x00

#define AN_EE_SIGNATURE1	0x0985
#define AN_EE_SIGNATURE2	0x1985
#define AN_EE_EADDR		4

/*
 * General purpose timer register
 */
#define AN_TIMER_VALUE          0x0000FFFF
#define AN_TIMER_CONTINUOUS     0x00010000

/*
 * Wakeup control/status bits
*/

#define AN_WKUPCTL_LINKSTS	0x00000001	/* link change detected */
#define AN_WKUPCTL_MAGICRX	0x00000002	/* magic pkt received */
#define AN_WKUPCTL_WAKEUPRX	0x00000004	/* wakeup pkt received */
#define AN_WKUPCTL_LINKSTS_ENB	0x00000100
#define AN_WKUPCTL_MACIGRX_ENB	0x00000200
#define AN_WKUPCTL_WAKEUPRX_ENB	0x00000400
#define AN_WKUPCTL_LINKON_ENB	0x00001000
#define AN_WKUPCTL_LINKOFF_ENB	0x00002000
#define AN_WKUPCTL_WKUP_PAT	0x3E000000
#define AN_WKUPCTL_CRC16_TYPE	0x40000000

/*
 * Watchdog timer register bits
 */

#define AN_WDOG_JABBER_DISABLE	0x00000001
#define AN_WDOG_NON_JABBER	0x00000002
#define AN_WDOG_JABBER_CLK	0x00000004
#define AN_WDOG_RX_ENABLE	0x00000010
#define AN_WDOG_RX_RELEASE	0x00000020
#define AN_WDOG_MII_CLKREV	0x10000000

/*
 * Auxilliary ISR bits
 */

#define AN_AUXISR_TX_OK			0x00000001
#define AN_AUXISR_TX_IDLE		0x00000002
#define AN_AUXISR_TX_NOBUF		0x00000004
#define AN_AUXISR_TX_JABBERTIMEO	0x00000008
#define AN_AUXISR_TX_UNDERRUN		0x00000020
#define AN_AUXISR_RX_OK			0x00000040
#define AN_AUXISR_RX_NOBUF		0x00000080
#define AN_AUXISR_RX_READ		0x00000100
#define AN_AUXISR_RX_WATDOGTIMEO	0x00000200
#define AN_AUXISR_TIMER_EXPIRED		0x00000800
#define AN_AUXISR_BUS_ERR		0x00002000
#define AN_AUXISR_ABNORMAL		0x00008000
#define AN_AUXISR_NORMAL		0x00010000
#define AN_AUXISR_PAUSERX		0x04000000
#define AN_AUXISR_TX_DEFER		0x10000000
#define AN_AUXISR_LINKCHANGE		0x20000000
#define AN_AUXISR_RX_EARLY		0x40000000
#define AN_AUXISR_TX_EARLY		0x80000000

/*
 * Operating mode register bits.
 */

#define AN_OMR_OPMODE		0x00000007
#define AN_OMR_EERELOAD		0x04000000
#define AN_OMR_LINK		0x20000000
#define AN_OMR_FD		0x40000000
#define AN_OMR_SPEED		0x80000000

#define AN_OPMODE_MACONLY	0x00000004
#define AN_OPMODE_MACPLUSPHY	0x00000007


/* DMA descriptor definition */

typedef struct an_desc
    {
    volatile UINT32	anSts;
    volatile UINT32	anCtl;
    volatile UINT32	anPtr1;
    volatile UINT32	anPtr2;
    } AN_DESC;


#define anData anPtr1
#define anNext anPtr2

#define AN_RXSTAT_FIFOOFLOW     0x00000001
#define AN_RXSTAT_CRCERR        0x00000002
#define AN_RXSTAT_DRIBBLE       0x00000004
#define AN_RXSTAT_MIIERR        0x00000008
#define AN_RXSTAT_WATCHDOG      0x00000010
#define AN_RXSTAT_FRAMETYPE     0x00000020      /* 0 == IEEE 802.3 */
#define AN_RXSTAT_COLLSEEN      0x00000040
#define AN_RXSTAT_GIANT         0x00000080
#define AN_RXSTAT_LASTFRAG      0x00000100
#define AN_RXSTAT_FIRSTFRAG     0x00000200
#define AN_RXSTAT_MULTICAST     0x00000400
#define AN_RXSTAT_RUNT          0x00000800
#define AN_RXSTAT_RXTYPE        0x00003000
#define AN_RXSTAT_DE            0x00004000
#define AN_RXSTAT_RXERR         0x00008000
#define AN_RXSTAT_RXLEN         0x3FFF0000
#define AN_RXSTAT_OWN           0x80000000

#define AN_RXBYTES(x)           ((x & AN_RXSTAT_RXLEN) >> 16)
#define AN_RXSTAT (AN_RXSTAT_FIRSTFRAG|AN_RXSTAT_LASTFRAG|AN_RXSTAT_OWN)

#define AN_RXCTL_BUFLEN1        0x00000FFF
#define AN_RXCTL_BUFLEN2        0x00FFF000
#define AN_RXCTL_RLINK          0x01000000
#define AN_RXCTL_RLAST          0x02000000

#define AN_TXSTAT_DEFER         0x00000001
#define AN_TXSTAT_UNDERRUN      0x00000002
#define AN_TXSTAT_LINKFAIL      0x00000003
#define AN_TXSTAT_COLLCNT       0x00000078
#define AN_TXSTAT_SQE           0x00000080
#define AN_TXSTAT_EXCESSCOLL    0x00000100
#define AN_TXSTAT_LATECOLL      0x00000200
#define AN_TXSTAT_NOCARRIER     0x00000400
#define AN_TXSTAT_CARRLOST      0x00000800
#define AN_TXSTAT_JABTIMEO      0x00004000
#define AN_TXSTAT_ERRSUM        0x00008000
#define AN_TXSTAT_OWN           0x80000000

#define AN_TXCTL_BUFLEN1        0x000007FF
#define AN_TXCTL_BUFLEN2        0x003FF800
#define AN_TXCTL_PAD            0x00800000
#define AN_TXCTL_TLINK          0x01000000
#define AN_TXCTL_TLAST          0x02000000
#define AN_TXCTL_NOCRC          0x04000000
#define AN_TXCTL_FIRSTFRAG      0x20000000
#define AN_TXCTL_LASTFRAG       0x40000000
#define AN_TXCTL_FINT           0x80000000



#define AN_MAXFRAG	16
#define AN_MAX_RX	16
#define AN_RX_DESC_CNT	32
#define AN_TX_DESC_CNT	32

#if (CPU_FAMILY == I80X86) || (CPU_FAMILY == PPC) || (CPU_FAMILY == COLDFIRE)
#define AN_ADJ(x)
#else
#define AN_RX_FIXUP
#define AN_ADJ(x)      m_adj((x), 8)
#endif

#define AN_INC_DESC(x, y)	(x) = ((x) + 1) % (y)

#define AN_CLSIZE	1536
#define AN_NAME		"an"
#define AN_TIMEOUT	10000

#define AN_INTRS							\
    (AN_AUXISR_TX_OK|AN_AUXISR_RX_OK|AN_AUXISR_TX_IDLE|			\
     AN_AUXISR_TX_NOBUF|AN_AUXISR_TX_UNDERRUN|AN_AUXISR_RX_NOBUF|	\
     AN_AUXISR_NORMAL|AN_AUXISR_ABNORMAL|AN_AUXISR_LINKCHANGE)

#define AN_RXINTRS	(AN_AUXISR_RX_OK|AN_AUXISR_RX_NOBUF)

#define AN_TXINTRS							\
    (AN_AUXISR_TX_OK|AN_AUXISR_TX_IDLE|					\
     AN_AUXISR_TX_NOBUF|AN_AUXISR_TX_UNDERRUN)

#define AN_LINKINTRS	(AN_AUXISR_LINKCHANGE)

/*
 * Private adapter context structure.
 */

typedef struct an_drv_ctrl
    {
    END_OBJ		anEndObj;
    VXB_DEVICE_ID	anDev;
    void *		anBar;
    void *		anHandle;
    void *		anMuxDevCookie;

    JOB_QUEUE_ID	anJobQueue;
    QJOB		anIntJob;
    atomicVal_t		anIntPending;

    QJOB		anRxJob;
    atomicVal_t		anRxPending;

    QJOB		anTxJob;
    atomicVal_t		anTxPending;
    UINT8		anTxCur;
    UINT8		anTxLast;
    volatile BOOL	anTxStall;
    UINT16		anTxThresh;

    BOOL		anPolling;
    M_BLK_ID		anPollBuf;
    UINT32		anIntMask;
    UINT32		anIntrs;

    UINT8		anAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	anCaps;

    END_IFDRVCONF	anEndStatsConf;
    END_IFCOUNTERS	anEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*anMediaList;
    END_ERR		anLastError;
    UINT32		anCurMedia;
    UINT32		anCurStatus;
    VXB_DEVICE_ID	anMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	anParentTag;

    VXB_DMA_TAG_ID	anRxDescTag;
    VXB_DMA_MAP_ID	anRxDescMap;
    AN_DESC		*anRxDescMem;

    VXB_DMA_TAG_ID	anTxDescTag;
    VXB_DMA_MAP_ID	anTxDescMap;
    AN_DESC		*anTxDescMem;

    VXB_DMA_TAG_ID	anMblkTag;

    VXB_DMA_MAP_ID	anRxMblkMap[AN_RX_DESC_CNT];
    VXB_DMA_MAP_ID	anTxMblkMap[AN_TX_DESC_CNT];

    M_BLK_ID		anRxMblk[AN_RX_DESC_CNT];
    M_BLK_ID		anTxMblk[AN_TX_DESC_CNT];

    UINT32		anTxProd;
    UINT32		anTxCons;
    UINT32		anTxFree;
    UINT32		anRxIdx;

    UINT8		anEeWidth;

    SEM_ID		anDevSem;
    } AN_DRV_CTRL;


#define AN_BAR(p)   ((AN_DRV_CTRL *)(p)->pDrvCtrl)->anBar
#define AN_HANDLE(p)   ((AN_DRV_CTRL *)(p)->pDrvCtrl)->anHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (AN_HANDLE(pDev), (UINT32 *)((char *)AN_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (AN_HANDLE(pDev),                             \
        (UINT32 *)((char *)AN_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (AN_HANDLE(pDev), (UINT16 *)((char *)AN_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (AN_HANDLE(pDev),                             \
        (UINT16 *)((char *)AN_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (AN_HANDLE(pDev), (UINT8 *)((char *)AN_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (AN_HANDLE(pDev),                              \
        (UINT8 *)((char *)AN_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCan983VxbEndh */
