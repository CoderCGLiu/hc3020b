/* vxbXlnxTemacEnd.h - header file for Xlnx temac VxBus ENd driver */

/*
 * Copyright (c) 2009-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*modification history
--------------------
01b,15dec10,e_d  remove DMA header file define
01a,19jun09,e_d  create
*/

/*
DESCRIPTION
This file contains the xilinx temac register definitions.
*/

#ifndef __INCvxbXlnxTemacEndh
#define __INCvxbXlnxTemacEndh

#ifdef __cplusplus
extern "C"
{
#endif
IMPORT void temacVxbEndRegister(void);

#ifndef BSP_VERSION

    /* PLB Directly Addressable Memory and Soft Register Offset */

#define TEMAC_RAFR     0x0     /* Reset and Address Filter Register */
#define TEMAC_TPFR     0x4     /* Transmit Pause Frame Register */
#define TEMAC_IFGPR    0x8     /* Transmit Inter Frame Gap Adjustment Register */
#define TEMAC_ISR      0xc     /* Interrupt Status Register */
#define TEMAC_IPR      0x10    /* Interrupt Pending Register */
#define TEMAC_IER      0x14    /* Interrupt Enable Register */
#define TEMAC_TTAGR    0x18    /* Transmit VLAN Tag Register */
#define TEMAC_RTAGR    0x1c    /* Receive VLAN Tag Register */
#define TEMAC_MSWR     0x20    /* TEMAC Most Significant Word Data Register */
#define TEMAC_LSWR     0x24    /* TEMAC LEAST Significant Word Data Register */
#define TEMAC_CTLR     0x28    /* TEMAC Control Register */
#define TEMAC_RDYR     0x2c    /* TEMAC Ready Status Register */
#define TEMAC_UAWLR    0x30    /* Unicast Address Word Low Register */
#define TEMAC_UAWUR    0x34    /* Unicast Address Word Upper Register */
#define TEMAC_TPIDR0   0x38    /* VLAN TPID Word 0 Register */
#define TEMAC_TPIDR1   0x3c    /* VLAN TPID Word 1 Register */

    /* TEMAC_RAFR */

#define TEMAC_RAFR_HTRST    (1<<0) /* Hard TEMAC Reset 1:reset */
#define TEMAC_RAFR_MCSTREJ  (1<<1) /* Reject Receive Multicast Address */
#define TEMAC_RAFR_BCSTREJ  (1<<2) /* Reject Receive Boardcast Address */
#define TEMAC_RAFR_NEWFUNEN (1<<10) /* New Functions Enable 1:enable */
#define TEMAC_RAFR_EMFLTREN (1<<11) /* Enhanced Multicast Filter Enable */
#define TEMAC_RAFR_STSRST   (1<<12) /* Statistics Counters Reset 1:reset */

    /* TEMAC_TPFR */

#define TEMAC_TPFR_TPFV     0      /* Transmit Pause Frame Value */

    /* TEMAC_IFGPR */

#define TEMAC_IFGPR_IFGP    0      /* Transmit Inter Frame Gap Adjustment Value */

    /* TEMAC_ISR */

#define TEMAC_ISR_HACSCMP   (1<<0) /* Hard register Access Complete */
#define TEMAC_ISR_AUTONEG   (1<<1) /* Auto Negotiation Complete 1: complete */
#define TEMAC_ISR_RXCMP     (1<<2) /* Receive Complete 1: frame receive complete */
#define TEMAC_ISR_RXJECT    (1<<3) /* Receive Frame Rejected */
#define TEMAC_ISR_RFIFOOVR  (1<<4) /* Receive FIFO Overrun */
#define TEMAC_ISR_TXCMP     (1<<5) /* Transmit Complete */
#define TEMAC_ISR_RXDCMLOCK (1<<6) /* Receive DCM Lock */
#define TEMAC_ISR_MGTRDY    (1<<7) /* MGT Ready */

    /* TEMAC_IPR */

#define TEMAC_IPR_HACSCMP   (1<<0) /* Hard register Access Complete */
#define TEMAC_IPR_AUTONEG   (1<<1) /* Auto Negotiation */
#define TEMAC_IPR_RXCMP     (1<<2) /* Receive Complete */
#define TEMAC_IPR_RXJECT    (1<<3) /* Receive Frame Rejected */
#define TEMAC_IPR_RFIFOOVR  (1<<4) /* Receive FIFO Overrun */
#define TEMAC_IPR_TXCMP     (1<<5) /* Transmit Complete */
#define TEMAC_IPR_RXDCMLOCK (1<<6) /* Receive DCM Lock */
#define TEMAC_IPR_MGTRDY    (1<<7) /* MGT Ready */

    /* TEMAC_IER */

#define TEMAC_IER_HACSCMP   (1<<0) /* Hard register Access Complete */
#define TEMAC_IER_AUTONEG   (1<<1) /* Auto Negotiation Complete 1: complete */
#define TEMAC_IER_RXCMP     (1<<2) /* Receive Complete 1: frame receive complete */
#define TEMAC_IER_RXJECT    (1<<3) /* Receive Frame Rejected */
#define TEMAC_IER_RFIFOOVR  (1<<4) /* Receive FIFO Overrun */
#define TEMAC_IER_TXCMP     (1<<5) /* Transmit Complete */
#define TEMAC_IER_RXDCMLOCK (1<<6) /* Receive DCM Lock */
#define TEMAC_IER_MGTRDY    (1<<7) /* MGT Ready */

    /* TEMAC_TTAGR */

#define TEMAC_TTAGR_CFI     (1<<12) /* Canonical Format Indicator */

    /* TEMAC_RTAGR */

#define TEMAC_RTAGR_CFI     (1<<12) /* Canonical Format Indicator */

    /* TEMAC_CTLR */

#define TEMAC_CTLR_WEN      (1<<15) /* Write Enable 0:read 1:read */
#define TEMAC_CTLR_ADDCODE(x) ((x) & 0x03ff)   /* Address Code */

    /* TEMAC_RDYR */

#define TEMAC_RDYR_FABRR    (1<<0) /* Fabric Read Ready Interface */
#define TEMAC_RDYR_MIIRR    (1<<1) /* MII Management Read Ready Interface */
#define TEMAC_RDYR_MIIWR    (1<<2) /* MII Management Write Ready Interface */
#define TEMAC_RDYR_AFRR     (1<<3) /* Address Filter Read Ready Interface */
#define TEMAC_RDYR_AFWR     (1<<4) /* Address Filter Write Ready Interface */
#define TEMAC_RDYR_CFGRR    (1<<5) /* Configuration Register Read Ready */
#define TEMAC_RDYR_CFGWR    (1<<6) /* Configuration Register Write Ready */
#define TEMAC_RDYR_HARDRDY  (1<<16) /* Hard register access ready */

    /* Ethernet Mac Register Offset */

#define TEMAC_RCR0          0x200   /* Receiver Configuration Register 0 */
#define TEMAC_RCR1          0x240   /* Receiver Configuration Register 1 */
#define TEMAC_TCR           0x280   /* Transmitter Configuration Register */
#define TEMAC_FCCR          0x2c0   /* Flow Control Configuration Register */
#define TEMAC_EMMCR         0x300   /* Etherner MAC Mode Configuration Register */
#define TEMAC_RSCR          0x320   /* RGMII/SGMII Configuration Register */
#define TEMAC_MCR           0x340   /* Management Configuration Register */
#define TEMAC_UAR0          0x380   /* Unicast Address Register 0 */
#define TEMAC_UAR1          0x384   /* Unicast Address Register 1 */
#define TEMAC_AATAR0        0x388   /* Additional Address Table Access Register 0 */
#define TEMAC_AATAR1        0x38c   /* Additional Address Table Access Register 1 */
#define TEMAC_AFMR          0x390   /* Address Filter Mode Register */
#define TEMAC_TIS           0x3A0   /* TEMAC Interrupt Status Register */
#define TEMAC_TIE           0x3A4   /* TEMAC Interrupt Enable Register */
#define TEMAC_MIIMWD        0x3B0   /* TEMAC MII Management Write Data Register */
#define TEMAC_MIIMAI        0x3B4   /* TEMAC MII Management Interface Register */

    /* TEMAC_RCR0 */

    /* TEMAC_RCR1 */

#define TEMAC_RCR1_PAUSEADDR  (1<<0)  /* Pause frame Ethernet MAC Address[47:32] */
#define TEMAC_RCR1_LTDIS      (1<<25) /* Length/Type Check 1:Disable 0:Enable */
#define TEMAC_RCR1_HD         (1<<26) /* Half-duplex mode 1:half_duplex 0:full_duplex */
#define TEMAC_RCR1_VLAN       (1<<27) /* VLAN Enable 1:Enable 0:Disable */
#define TEMAC_RCR1_RX         (1<<28) /* Receive enable 1:Enable 0:Disable */
#define TEMAC_RCR1_FCS        (1<<29) /* In-band FCS enable 1:Enable 0:Disable */
#define TEMAC_RCR1_JUM        (1<<30) /* Jumbo frame enable 1:Enable 0:Disable */
#define TEMAC_RCR1_RESET      (1<<31) /* Reset 1:Reset */

    /* TEMAC_TCR */

#define TEMAC_TCR_LTDIS      (1<<25) /* Length/Type Check 1:Disable 0:Enable */
#define TEMAC_TCR_HD         (1<<26) /* Half-duplex mode 1:half_duplex 0:full_duplex */
#define TEMAC_TCR_VLAN       (1<<27) /* VLAN Enable 1:Enable 0:Disable */
#define TEMAC_TCR_TX         (1<<28) /* Transmitter enable 1:Enable 0:Disable */
#define TEMAC_TCR_FCS        (1<<29) /* In-band FCS enable 1:Enable 0:Disable */
#define TEMAC_TCR_JUM        (1<<30) /* Jumbo frame enable 1:Enable 0:Disable */
#define TEMAC_TCR_RESET      (1<<31) /* Reset 1:Reset */

    /* TEMAC_FCCR */

#define TEMAC_FCCR_FCRX      (1<<29) /* Rx flow control enable 1:Enable 0:Disable */
#define TEMAC_FCCR_FCTX      (1<<30) /* Tx flow control enable 1:Enable 0:Disable */

    /* TEMAC_EMMCR */

#define TEMAC_EMMCR_RX16     (1<<24) /* Reveive 16bit Client Interface enable 1:16bit 0:8bit */
#define TEMAC_EMMCR_TX16     (1<<25) /* Transmit 16bit Client Interface enable 1:16bit 0:8bit */
#define TEMAC_EMMCR_HOST     (1<<26) /* Host Interface enable 1:Enable 0:Disable */
#define TEMAC_EMMCR_GPCS     (1<<27) /* 1000BASE-X mode enable 1:Enable 0:Disable */
#define TEMAC_EMMCR_SGMII    (1<<28) /* SGMII mode enable 1:Enable 0:Disable */
#define TEMAC_EMMCR_RGMII    (1<<29) /* RGMII mode enable 1:Enable 0:Disable */
#define TEMAC_EMMCR_SPEED10M (0<<30) /* Speed selection 10M */
#define TEMAC_EMMCR_SPEED100M  (1<<30) /* Speed selection 100M */
#define TEMAC_EMMCR_SPEED1000M (2<<30) /* Speed selection 1000M */
#define TEMAC_EMMCR_SPEEDCLR   (3<<30) /* Clear speed bit */

    /* TEMAC_RSCR */

#define TEMAC_RSCR_RLINK       (1<<0)  /* RGMII link status 1:link 0:down */
#define TEMAC_RSCR_RHD         (1<<1)  /* RGMII duplex status 1:half-duplex 0:full-duplex */
#define TEMAC_RSCR_RSPEED10M   (0<<2)  /* RGMII speed status 10b:1000M 01b:100M 00b:10M */
#define TEMAC_RSCR_RSPEED100M  (1<<2)  /* RGMII speed status 10b:1000M 01b:100M 00b:10M */
#define TEMAC_RSCR_RSPEED1000M (2<<2)  /* RGMII speed status 10b:1000M 01b:100M 00b:10M */
#define TEMAC_RSCR_SSPEED10M   (0<<30) /* SGMII speed status 10b:1000M 01b:100M 00b:10M */
#define TEMAC_RSCR_SSPEED100M  (1<<30) /* SGMII speed status 10b:1000M 01b:100M 00b:10M */
#define TEMAC_RSCR_SSPEED1000M (2<<30) /* SGMII speed status 10b:1000M 01b:100M 00b:10M */

    /* TEMAC_MCR */

#define TEMAC_MCR_DIVIDE       (0<<0)  /* Clock divide */
#define TEMAC_MCR_MDIOEN       (1<<6)  /* Mdio enable 1:Enable 0:Disable */

    /* TEMAC_UAR0 */

    /* TEMAC_UAR1 */

    /* TEMAC_AATAR0 */

    /* TEMAC_AATAR1 */

#define TEMAC_AATAR1_ADDR(x) (x<<16) /* General Address Register */
#define TEMAC_AATAR1_RNW     (1<<23) /* Address Table read enable 1:read 0:write */

#define TEMAC_MAX_MULTABLE   4       /* Max num of multicast Table */

    /* TEMAC_AFMR */

#define TEMAC_AFMR_PM        (1<<31) /* Promiscuous mode enable 1:Disable 0:Enable */

#define TEMAC_MTU       1500
#define TEMAC_CLSIZE    1536

#define TEMAC_NAME      "temac"
#define TEMAC_TIMEOUT   10000

#define TEMAC_STACR_PHYREG   0x1f
#define TEMAC_STACR_PHYADDR  0x3e0

#define TEMAC_PHYREG(x)     ((x) & TEMAC_STACR_PHYREG)
#define TEMAC_PHYADDR(x)    (((x) << 5) & TEMAC_STACR_PHYADDR)

#define TEMAC_RX_DESC_CNT   XLNXDMA_RX_DESC_CNT
#define TEMAC_TX_DESC_CNT   XLNXDMA_TX_DESC_CNT
#define TEMAC_MAXFRAG       16
#define TEMAC_MAX_RX        64
#define TEMAC_INTR_SKIP     0xF

#define TEMAC_INC_DESC(x, y)      (x) = ((x + 1) & (y - 1))

#define TEMAC_CSNCTRLEN     0x00000001 /* Enable Tran Checksum */
#define TEMAC_TCP_INSOFFSET 0x10
#define TEMAC_UDP_INSOFFSET 0x06

#define TEMAC_REV_NUMMASK   0x00003FFF;

#define TEMAC_DMA_FLAG      0x80000000

    /*
     * Private adapter context structure.
     */

    typedef struct temac_drv_ctrl
        {
        END_OBJ temacEndObj;
        VXB_DEVICE_ID temacDev;
        void * temacMuxDevCookie;
        void * temacBar;
        void * temacHandle;

        JOB_QUEUE_ID temacJobQueue;
        QJOB temacIntJob;
        atomic_t temacIntPending;

        QJOB temacRxJob;
        atomic_t temacRxPending;

        QJOB temacTxJob;
        atomic_t temacTxPending;
        volatile BOOL temacTxStall;

        UINT32 temacTxProd;
        UINT32 temacTxCons;
        UINT32 temacTxFree;
        UINT32 temacRxIdx;

        BOOL temacPolling;
        M_BLK_ID temacPollBuf;
        UINT32 temacIntMask;
        UINT32 temacIntrs;

        UINT8 temacAddr[ETHER_ADDR_LEN];

        END_CAPABILITIES temacCaps;

        END_IFDRVCONF temacEndStatsConf;
        END_IFCOUNTERS temacEndStatsCounters;
        UINT32 temacInErrors;
        UINT32 temacInDiscards;
        UINT32 temacInUcasts;
        UINT32 temacInMcasts;
        UINT32 temacInBcasts;
        UINT32 temacInOctets;
        UINT32 temacOutErrors;
        UINT32 temacOutUcasts;
        UINT32 temacOutMcasts;
        UINT32 temacOutBcasts;
        UINT32 temacOutOctets;

        SEM_ID temacDevSem;

        /* Begin MII/ifmedia required fields. */

        END_MEDIALIST * temacMediaList;
        END_ERR temacLastError;
        UINT32 temacCurMedia;
        UINT32 temacCurStatus;
        VXB_DEVICE_ID temacMiiBus;
        VXB_DEVICE_ID temacMiiDev;
        FUNCPTR temacMiiPhyRead;
        FUNCPTR temacMiiPhyWrite;
        int temacMiiPhyAddr;

        /* DMA tags and maps. */

        VXB_DMA_TAG_ID temacParentTag;

        XLNXDMA_DESC * temacRxDescMem;
        XLNXDMA_DESC * temacTxDescMem;

        VXB_DMA_TAG_ID temacMblkTag;

        VXB_DMA_MAP_ID temacRxMblkMap[TEMAC_RX_DESC_CNT];
        VXB_DMA_MAP_ID temacTxMblkMap[TEMAC_TX_DESC_CNT];

        M_BLK_ID temacRxMblk[TEMAC_RX_DESC_CNT];
        M_BLK_ID temacTxMblk[TEMAC_TX_DESC_CNT];

        VXB_DMA_RESOURCE_ID temacRxChan;
        VXB_DMA_RESOURCE_ID temacTxChan;

        int temacMaxMtu;

        int temacType;
#ifdef EMAC_TX_MODERATION
        WDOG_ID temacTxWd;
#endif

        int temacNum;
        int temacRgmiiPort;
        } TEMAC_DRV_CTRL;

#define DELTA(a,b)      (abs((int)a - (int)b))

#define TEMAC_BAR(p)      ((TEMAC_DRV_CTRL *)(p)->pDrvCtrl)->temacBar
#define TEMAC_HANDLE(p)   ((TEMAC_DRV_CTRL *)(p)->pDrvCtrl)->temacHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (TEMAC_HANDLE(pDev), (UINT32 *)((char *)TEMAC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (TEMAC_HANDLE(pDev),                             \
        (UINT32 *)((char *)TEMAC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define CSR_READ_INDEXREG(pDev, addr, val)       \
        do                                       \
            {                                     \
            CSR_WRITE_4(pDev, TEMAC_CTLR, addr);  \
            val = CSR_READ_4(pDev, TEMAC_LSWR);   \
            }while(0)

#define CSR_WRITE_INDEXREG(pDev, addr, val)      \
        do                                       \
            {                                    \
            CSR_WRITE_4(pDev, TEMAC_LSWR, val);     \
            CSR_WRITE_4(pDev, TEMAC_CTLR, addr|TEMAC_CTLR_WEN);  \
            }while(0)
#endif /* BSP_VERSION */


#ifdef __cplusplus
}
#endif

#endif /* __INCvxbXlnxTemacEndh */







