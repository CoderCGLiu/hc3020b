/* vxbStmSpear13xxGmacEnd.h - STM SPEAr 13xx GMAC VxBus END driver */

/*
 * Copyright (c) 2010, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,18oct12,clx add spear1310 Rev C support
01a,27dec10,clx created
*/

#ifndef __INCvxbStmGmacEndh
#define __INCvxbStmGmacEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void stm13xxGmacRegister (void);

#define GMAC_MEDIA_MII         2
#define GMAC_MEDIA_GMII        3
#define GMAC_MEDIA_RMII        1
#define GMAC_MEDIA_RGMII       0

#ifndef BSP_VERSION

/* GMAC driver generic defines */

#define GMAC_NAME              "stmGmac"
#define GMAC_MTU               1500
#define GMAC_JUMBO_MTU         2048
#define GMAC_CLSIZE            1536
#define GMAC_TIMEOUT           1000000
#define GMAC_TUPLE_CNT         768
#define GMAC_ADJ(x)            ((x)->m_data += 2)
#define GMAC_DESC_ALIGN         16

/* packet buffer descriptors definitions */

#define GMAC_TX_DESC_CNT           256
#define GMAC_RX_DESC_CNT           256
#define GMAC_INC_DESC(x, y)        (x) = (((x) + 1) % y)
#define GMAC_MAXFRAG               16
#define GMAC_MAX_RX                16

/* GMAC number of modules */

#define GMAC_MOD_NUM          1

/* GMAC registers offsets */

#define GMAC_CONTROL        0x0000  /* MAC Configuration */
#define GMAC_FRAME_FILTER   0x0004  /* MAC Frame Filter */
#define GMAC_HASH_H         0x0008  /* MAC Hash Table[63:32] */
#define GMAC_HASH_L         0x000C  /* MAC Hash Table[31:0] */
#define GMAC_GMII_ADDR      0x0010  /* GMII Address */
#define GMAC_GMII_DATA      0x0014  /* GMII Data */
#define GMAC_FLOW_CTRL      0x0018  /* Flow Control */
#define GMAC_VLAN_TAG       0x001C  /* VLAN Tag */
#define GMAC_VERSION        0x0020  /* GMAC CORE Version */
#define GMAC_RE_WKUP_FILTER 0x0028  /* Remote Wake-up Frame Filter */
#define GMAC_PMT_CSR        0x002C  /* PMT Control and Status */
#define GMAC_PHY_DATA_MASK  0xffff  /* data mask */

#define GMAC_INT_STATUS     0x0038  /* Interrupt status */
#define GMAC_INT_MASK       0x003C  /* Interrupt Mask */
#define MAC_ADDRL_HI(x)     (0x0040 + (x * 8))  /* MAC Address(0-15) High */
#define MAC_ADDRL_LO(x)     (0x0044 + (x * 8))  /* MAC Address(0-15) Low */

#define MAC_ADDRH_HI(x)     (0x0800 + (x * 8))  /* MAC Address(16-31) High */
#define MAC_ADDRH_LO(x)     (0x0804 + (x * 8))  /* MAC Address(16-31) Low */

/* statistics registers */

#define GMAC_TX_OCTETS_GB        0x0114  /* TX bytes Good/Bad Frame */
#define GMAC_TX_FRAMES_GB        0x0118  /* TX Frame Good/Bad Frame */
#define GMAC_TX_BCAST_FRAMES_G   0x011C  /* TX broadcast Good frames */
#define GMAC_TX_MCAST_FRAMES_G   0x0120  /* TX multicast Good frames */
#define GMAC_TX_FRAME64_GB       0x0124  /* TX 64 bytes Good/Bad frames */
#define GMAC_TX_FRAME65T127_GB   0x0128  /* TX 65-127 bytes Good/Bad frames */
#define GMAC_TX_FRAME128T255_GB  0x012C  /* TX 128-255 bytes Good/Bad frames */
#define GMAC_TX_FRAME256T511_GB  0x0130  /* TX 256-511 bytes Good/Bad frames */
#define GMAC_TX_FRAME512T1023_GB 0x0134  /* TX 512-1023 bytes Good/Bad frames */
#define GMAC_TX_FRAME1024TUP_GB  0x0138  /* TX 1024+ bytes Good/Bad frames */
#define GMAC_TX_UCAST_FRAMES_GB  0x0140  /* TX Good/Bad unicaset frames */
#define GMAC_TX_MCAST_FRAMES_GB  0x0140  /* TX Good/Bad multicast frames */
#define GMAC_TX_BCAST_FRAMES_GB  0x0144  /* TX Good/Bad broadcast frames */
#define GMAC_TX_UNDERFLOW_ERROR  0x0148  /* TX underflow Error */
#define GMAC_TX_SINGLE_COLL_G    0x014C  /* TX single collisions frames */
#define GMAC_TX_MULTI_COLL_G     0x0150  /* TX multiple collisions frames */
#define GMAC_TX_DEFERRED         0x0154  /* TX deferred frames */
#define GMAC_TX_LATE_COLL        0x0158  /* TX late collisions frames */
#define GMAC_TX_EXCESSIVE_COLL   0x015C  /* TX excessive collisions frames */
#define GMAC_TX_CARRIER_SENSE    0x0160  /* TX carrier sense error */
#define GMAC_TX_OCTETS_G         0x0164  /* TX bytes Good Frame */
#define GMAC_TX_FRAMES_G         0x0168  /* TX Frame Good Frame*/
#define GMAC_TX_EXCESS_DEF       0x016C  /* TX Excess deferred frames */
#define GMAC_TX_PAUSE_FRAMES     0x0170  /* TX pause frames */
#define GMAC_TX_VLAN_FRAMES_G    0x0174  /* TX VLAN Good Frames */

#define GMAC_RX_FRAMES_GB        0x0180  /* RX Frame Good/Bad Frame */
#define GMAC_RX_OCTETS_GB        0x0184  /* RX bytes Good/Bad Frame */
#define GMAC_RX_OCTETS_G         0x0188  /* RX bytes Good Frame */
#define GMAC_RX_BCAST_FRAMES_G   0x018C  /* RX broadcast Good frames */
#define GMAC_RX_MCAST_FRAMES_G   0x0190  /* RX multicast Good frames */
#define GMAC_RX_CRC_ERRORS       0x0194  /* RX CRC error frames */
#define GMAC_RX_ALIGN_ERRORS     0x0198  /* RX alignment error frames */
#define GMAC_RX_OVERRUN          0x019C  /* RX overrun */
#define GMAC_RX_JABBER           0x01A0  /* RX jabber frames */
#define GMAC_RX_UNDERSIZED       0x01A4  /* RX Good undersized frames */
#define GMAC_RX_OVERSIZED        0x01A8  /* RX Good oversized frames */
#define GMAC_RX_FRAME64_GB       0x01AC  /* RX Good/Bad 64 bytes frames */
#define GMAC_RX_FRAME65T127_GB   0x01B0  /* RX Good/Bad 65-127 bytes frames */
#define GMAC_RX_FRAME128T255_GB  0x01B4  /* RX Good/Bad 128-255 bytes frames */
#define GMAC_RX_FRAME256T511_GB  0x01B8  /* RX Good/Bad 256-511 bytes frames */
#define GMAC_RX_FRAME512T1023_GB 0x01BC  /* RX Good/Bad 512-1023 bytes frames */
#define GMAC_RX_FRAME1024TUP_GB  0x01C0  /* RX Good/Bad 1024+ bytes frames */
#define GMAC_RX_UCAST_FRAMES_G   0x01C4  /* RX Good Unicast frames */
#define GMAC_RX_LENGTH_ERRORS    0x01C8  /* RX length error frames */
#define GMAC_RX_OUT_RANGE_TYPE   0x01CC  /* RX out of range type */
#define GMAC_RX_PAUSE_FRAMES     0x01D0  /* RX pause frames */
#define GMAC_RX_FIFO_OVERFLOW    0x01D4  /* RX FIFO overflow */
#define GMAC_RX_VLAN_FRAMES_GB   0x01D8  /* RX VLAN Good/Bad Frames */
#define GMAC_RX_WD_ERR           0x01DC  /* RX Watchdog Error */

/* GMAC Control register bit define */

#define CFG_SMII_FTERR   (1 << 26)  /* SMII Force Tx Error */
#define CFG_CST          (1 << 25)  /* CRC stripping for type frames */
#define CFG_TX_CONFIG    (1 << 24)  /* Tx Configuration in RGMII/SGMII/SMII */
#define CFG_WD_DIS       (1 << 23)  /* Watchdog Disable */
#define CFG_JABBER_DIS   (1 << 22)  /* Jabber Disable */
#define CFG_FRAME_BST_EN (1 << 21)  /* Frame Burst Enable */
#define CFG_JUMBO_EN     (1 << 20)  /* Jumbo Frame Enable */
#define CFG_IFG(x)       (((x) & 7) << 17) /* Inter-Frame Gap */
#define CFG_DIS_CRS      (1 << 16)  /* Disable carrier sense during Tx */
#define CFG_MII_PORT_SEL (1 << 15)  /* Port Select 0:GMII, 1:MII */
#define CFG_FES_100      (1 << 14)  /* Seep 0:10,1:100 */
#define CFG_DIS_RX_OWN   (1 << 13)  /* Disable Rx Own */
#define CFG_LOOPBACK     (1 << 12)  /* Loop-back Mode */
#define CFG_DPLX_MODE    (1 << 11)  /* Duplex Mode */
#define CFG_IPC          (1 << 10)  /* Checksum offload */
#define CFG_DIS_RETRY    (1 << 9)   /* Disable Retry */
#define CFG_LINK_UD      (1 << 8)   /* LINK Up/Down */
#define CFG_AUTO_PCS     (1 << 7)   /* Automatic Pad/CRC Stripping */
#define CFG_DEF_CHECK    (1 << 4)   /* Deferral Check */
#define CFG_TX_EN        (1 << 3)   /* Tx Enable */
#define CFG_RX_EN        (1 << 2)   /* Rx Enable */

#define GMAC_INIT (CFG_DIS_RX_OWN | CFG_JABBER_DIS | CFG_AUTO_PCS | CFG_IPC | \
                       CFG_FRAME_BST_EN | CFG_DPLX_MODE | CFG_WD_DIS | CFG_CST)

/* MAC Frame Filter register bit define */

#define FFILTER_PROMIS_MODE   (1 << 0) /* Promiscuous Mode */
#define FFILTER_HASH_UCAST    (1 << 1)
#define FFILTER_HASH_MCAST    (1 << 2)
#define FFILTER_DA_INVERSE    (1 << 3)
#define FFILTER_PASSALL_MCAST (1 << 4)
#define FFILTER_DIS_BCAST     (1 << 5)
#define FFILTER_PASS_CFRAME   (1 << 6) /* Pass Control Frame */
#define FFILTER_SA_INVERSE    (1 << 8)
#define FFILTER_SA_FILTER_EN  (1 << 9)
#define FFILTER_RX_ALL        (1 << 31)

/* GMII Address register bit define */

#define GMII_WRITE            (1 << 1)
#define GMII_BUSY             (1 << 0)
#define GMII_PHY_ADDR(x)      (x << 11)
#define GMII_REG_OFFSET(x)    (x << 6)
#define GMII_REGMSK           (0x1F << 6)
#define GMII_ADDRMSK          (0x1F << 11)

/* MMC registers offset */

#define GMAC_MMC_CNTRL        0x0100  /* MMC Control */
#define GMAC_MMC_RX_INT       0x0104  /* MMC Rx Int */
#define GMAC_MMC_TX_INT       0x0108  /* MMC Tx Int */
#define GMAC_MMC_RXINT_MASK   0x010C  /* MMC Rx Int Mask */
#define GMAC_MMC_TXINT_MASK   0x0110  /* MMC Tx Int Mask */

/* MMC Control register bit define */

#define MMC_FH_PRESET        (1 << 5) /* Full-Half preset */
#define MMC_COUNTER_PRESET   (1 << 4) /* Counters preset */
#define MMC_COUNTER_FREEZE   (1 << 3) /* MMC counter freeze */
#define MMC_RESET_ON_READ    (1 << 2) /* Counters reset on read */
#define MMC_COUNTER_STOP     (1 << 1) /* Counters stop rollover */
#define MMC_COUNTER_RESET    (1 << 0) /* Counters reset */

/* DMA registers offsets */

#define DMA_BUS_MODE          0x1000  /* Bus Mode */
#define DMA_TX_POLL_DEMAND    0x1004  /* Tx Poll Demand */
#define DMA_RX_POLL_DEMAND    0x1008  /* Rx Poll Demand */
#define DMA_RX_DESC_LIST_ADDR 0x100C  /* Rx Desc List Base Address */
#define DMA_TX_DESC_LIST_ADDR 0x1010  /* Tx Desc List Base Address */
#define DMA_STATUS            0x1014  /* Status Register */
#define DMA_OP_MODE           0x1018  /* Operational Mode */
#define DMA_INT_EN            0x101C  /* Int Enable */
#define DMA_MFBO_CNTR         0x1020  /* Missed Frame/Buffer Overflow Counter */
#define DMA_RX_INT_WDOG_TIMER 0x1024  /* Rx Int Watchdog Timer */
#define DMA_CH_TX_DESC        0x1048  /* Current Host Tx Desc */
#define DMA_CH_RX_DESC        0x104C  /* Current Host Rx Desc */
#define DMA_CUR_TX_BUF_ADDR   0x1050  /* Current Host Tx Buffer Address */
#define DMA_CUR_RX_BUF_ADDR   0x1054  /* Current Host Rx Buffer Address */
#define DMA_HW_FEATURE        0x1058  /* HW Feature */

#define DMA_BUS_MB      (1 << 26)     /* Mixed Burst */
#define DMA_BUS_AAL     (1 << 25)     /* Address Aligned Beats */
#define DMA_BUS_8PBL    (1 << 24)     /* 8*PBL Mode */
#define DMA_BUS_USP     (1 << 23)     /* Use Separte PBL */

/* Rx Programmable Burst Len: 1/2/4/8/16/32 */

#define DMA_BUS_RPBL(x) ((x & 0x3f) << 17)

#define DMA_BUS_FB      (1 << 16)     /* Fixed Burst */

/* Rx,Tx Priority Ratio, 0/1/2/3 */

#define DMA_BUS_PR(x)   ((x & 0x3) << 14)

/* Programmable Burst Length: 1/2/4/8/16/32 */

#define DMA_BUS_PBL(x)  ((x & 0x3f) << 8)

#define DMA_BUS_DSL(x)  ((x & 0x1F) << 2)
                                      /* Descriptor Skip Length */
#define DMA_BUS_DA      (1 << 1)      /* DMA Arbitration Scheme,Rx High Pro */
#define DMA_BUS_SWR     (1 << 0)      /* Software Reset */

#define DMA_BUS_INIT  (DMA_BUS_FB | DMA_BUS_PBL(16) | DMA_BUS_RPBL(16))

/* Operational Mode register bit define */

#define DMA_OP_DT     (1 << 26)       /* No Dropping of TCP/IP csum Err Frame */
#define DMA_OP_RSF    (1 << 25)       /* Rx Store and Forward */
#define DMA_OP_TSF    (1 << 21)       /* Tx Store and Forward */
#define DMA_OP_FTF    (1 << 20)       /* Flush Tx FIFO */
#define DMA_OP_TTC(x) ((x & 7) << 14) /* Tx Threshold Control */
#define DMA_OP_ST     (1 << 13)       /* Start/Stop Tx */
#define DMA_OP_RFD(x) ((x & 3) << 11) /* Threshold for DeActive Flow Control */
#define DMA_OP_RFA(x) ((x & 3) << 9)  /* Threshold for Active Flow Control */
#define DMA_OP_EFC    (1 << 8)        /* Enable HW Flow control */
#define DMA_OP_FEF    (1 << 7)        /* Forward Error Frame */
#define DMA_OP_FUF    (1 << 6)        /* Forward Undersize Good Frame */
#define DMA_OP_RTC(x) ((x & 3) << 3)  /* Rx Threshold Control */
#define DMA_OP_OSF    (1 << 2)        /* Operate On Second Mode */
#define DMA_OP_SR     (1 << 1)        /* Start/Stop Rx */

#define DMA_OP_INIT  (DMA_OP_TSF | DMA_OP_OSF | DMA_OP_RSF)

/* DMA Status register bit define */

#define DMA_STATUS_TTI   (1 << 29)    /* Time-Stamp Trigger interrupt */
#define DMA_STATUS_GPI   (1 << 28)    /* GMAC PMT interface */
#define DMA_STATUS_GMI   (1 << 27)    /* GMAC MMC interrupt */
#define DMA_STATUS_GLI   (1 << 26)    /* GMAC Line interface interrupt */
#define DMA_STATUS_EB_MASK      0x00380000 /* Error Bits Mask */
#define DMA_STATUS_EB_TX_ABORT  0x00080000 /* Error Bits - TX Abort */
#define DMA_STATUS_EB_RX_ABORT  0x00100000 /* Error Bits - RX Abort */
#define DMA_STATUS_TS_MASK      0x00700000 /* Tx Process State */
#define DMA_STATUS_TS_SHIFT     20
#define DMA_STATUS_RS_MASK      0x000e0000 /* Rx Process State */
#define DMA_STATUS_RS_SHIFT     17
#define DMA_STATUS_NIS   (1 << 16)    /* Normal Interrupt Summary */
#define DMA_STATUS_AIS   (1 << 15)    /* Abnormal Interrupt Summary */
#define DMA_STATUS_ERI   (1 << 14)    /* Early Rx Interrupt */
#define DMA_STATUS_FBI   (1 << 13)    /* Fatal Bus Error Interrupt */
#define DMA_STATUS_ETI   (1 << 10)    /* Early Tx Interrupt */
#define DMA_STATUS_RWT   (1 << 9)     /* Rx Watchdog Timeout */
#define DMA_STATUS_RPS   (1 << 8)     /* Rx Process Stopped */
#define DMA_STATUS_RU    (1 << 7)     /* Rx Buffer Unavailable */
#define DMA_STATUS_RI    (1 << 6)     /* Rx Interrupt */
#define DMA_STATUS_UNF   (1 << 5)     /* Tx Underflow */
#define DMA_STATUS_OVF   (1 << 4)     /* Rx Overflow */
#define DMA_STATUS_TJT   (1 << 3)     /* Tx Jabber Timeout */
#define DMA_STATUS_TU    (1 << 2)     /* Tx Buffer unavailable */
#define DMA_STATUS_TPS   (1 << 1)     /* Tx Process stopped */
#define DMA_STATUS_TI    (1 << 0)     /* Tx Interrupt */

/* DMA Int Enable register bit define */

/* Normal Int */

#define DMA_INT_EN_NIS   (1 << 16)    /* Normal Interrupt Summary */
#define DMA_INT_EN_AIS   (1 << 15)    /* Abnormal Interrupt Summary */
#define DMA_INT_EN_ERI   (1 << 14)    /* Early Rx Interrupt */
#define DMA_INT_EN_FBI   (1 << 13)    /* Fatal Bus Error Interrupt */
#define DMA_INT_EN_ETI   (1 << 10)    /* Early Tx Interrupt */
#define DMA_INT_EN_RWT   (1 << 9)     /* Rx Watchdog Timeout */
#define DMA_INT_EN_RPS   (1 << 8)     /* Rx Process Stopped */
#define DMA_INT_EN_RU    (1 << 7)     /* Rx Buffer Unavailable */
#define DMA_INT_EN_RI    (1 << 6)     /* Rx Interrupt */
#define DMA_INT_EN_UNF   (1 << 5)     /* Tx Underflow */
#define DMA_INT_EN_OVF   (1 << 4)     /* Rx Overflow */
#define DMA_INT_EN_TJT   (1 << 3)     /* Tx Jabber Timeout */
#define DMA_INT_EN_TU    (1 << 2)     /* Tx Buffer unavailable */
#define DMA_INT_EN_TPS   (1 << 1)     /* Tx Process stopped */
#define DMA_INT_EN_TI    (1 << 0)     /* Tx Interrupt */

#define DMA_INT_NORMAL  (DMA_INT_EN_NIS | DMA_INT_EN_RI | \
                         DMA_INT_EN_TI)

#define DMA_INT_ABNORMAL (DMA_INT_EN_AIS | DMA_INT_EN_FBI | \
                         DMA_INT_EN_UNF)

/* DMA default interrupt mask */

#define DMA_INT_DEFAULT_MASK  (DMA_INT_NORMAL)

/* DMA Tx Desc status descriptor */

#define TX_DESC_DEFERRED       (1 << 0)     /* Status : Tx deferred */
#define TX_DESC_UNDERFLOW_ERR  (1 << 1)     /* Underflow Err */
#define TX_DESC_EXCE_DEF_ERR   (1 << 2)     /* Excessive Tx deferral Err */
#define TX_DESC_COL_COUNT(x)   (((x) >> 3) & 0xf) /* Collision count */
#define TX_DESC_VLAN_FRAME     (1 << 7)     /* Status : VLAN frame */
#define TX_DESC_EXCE_COL       (1 << 8)     /* Excessive collisions Err */
#define TX_DESC_LATE_COL       (1 << 9)     /* Late collision Err */
#define TX_DESC_NO_CARRIER     (1 << 10)    /* No carrier Err */
#define TX_DESC_LOSS_CARRIER   (1 << 11)    /* Loss carrier Err */
#define TX_DESC_PAYLOAD_ERR    (1 << 12)    /* Addr/Payload csum Err */
#define TX_DESC_FRAME_FLUSH    (1 << 13)    /* Frame flushed Err */
#define TX_DESC_JAB_TIMEOUT    (1 << 14)    /* Jabber timeout Err */
#define TX_DESC_ERR_SUMMARY    (1 << 15)    /* Tx Error summary */
#define TX_DESC_HEADER_ERR     (1 << 16)    /* IP header csum Err */
#define TX_DESC_TIMESTAMP_ST   (1 << 17)
#define TX_DESC_CHAINED        (1 << 20)
#define TX_DESC_RING_END       (1 << 21)
#define TX_DESC_CSUM_INSERT(x) ((x) << 22)
#define TX_DESC_TIMESTAMP_EN   (1 << 25)
#define TX_DESC_PADDING_DIS    (1 << 26)
#define TX_DESC_CRC_DIS        (1 << 27)
#define TX_DESC_FIRST          (1 << 28)
#define TX_DESC_LAST           (1 << 29)
#define TX_DESC_INT            (1 << 30)
#define TX_DESC_OWNBYDMA       (1 << 31)

/* DMA Tx Desc control descriptor */

#define TX_BUFFER1_SIZE(x)       (((x) & 0x1FFF) << 0)
#define TX_BUFFER2_SIZE(x)       (((x) & 0x1FFF) << 16)

/* DMA Rx Desc status descriptor */

#define RX_DESC_PAYLOAD_CSUM_ERR (1 << 0)
#define RX_DESC_CRC_ERR          (1 << 1)     /* CRC error */
#define RX_DESC_DRIBBLING        (1 << 2)     /* Dribbling error */
#define RX_DESC_GMII_ERR         (1 << 3)     /* GMII Rx Error */
#define RX_DESC_RX_WATCHDOG      (1 << 4)     /* Rx watchdog error */
#define RX_DESC_FRAMEETHER       (1 << 5)
#define RX_DESC_LATE_COL         (1 << 6)     /* late collision error */
#define RX_DESC_IPC_CSUM_ERR     (1 << 7)     /* IPC csum Err/Giant frame */
#define RX_DESC_LAST             (1 << 8)
#define RX_DESC_FIRST            (1 << 9)
#define RX_DESC_VLAN_TAG         (1 << 10)    /* Status : VLAN frame tagged */
#define RX_DESC_OVERFLOW_ERR     (1 << 11)    /* overflow error */
#define RX_DESC_LENGTH_ERR       (1 << 12)    /* length_error */
#define RX_DESC_SAFILTER_FAIL    (1 << 13)    /* Source Address filter fail */
#define RX_DESC_DESC_ERR         (1 << 14)    /* Descriptor error */
#define RX_DESC_ERROR_SUMMARY    (1 << 15)    /* Rx Error summary */
#define RX_DESC_FRMLEN_MASK     (((x) & 0x3FFF) << 16)
#define RX_DESC_FRMLEN_LEN_MASK  (0x3FFF)
#define RX_DESC_FRMLEN_LEN_SHIFT (16)
#define RX_DESC_DAFILTER_FAIL    (1 << 30)    /* Dest Address filter fail */
#define RX_DESC_OWNBYDMA         (1 << 31)

#define RX_DESC_ERR_FRAME  (RX_DESC_ERROR_SUMMARY | RX_DESC_PAYLOAD_CSUM_ERR | \
                             RX_DESC_CRC_ERR)

/* DMA Rx Desc control descriptor */

#define RX_DESC_CTL_CHAINED      (1 << 14)
#define RX_DESC_CTL_RINGEND      (1 << 15)
#define RX_BUFFER1_SIZE(x)       (((x) & 0x1FFF) << 0)
#define RX_BUFFER2_SIZE(x)       (((x) & 0x1FFF) << 16)
#define RX_DESC_CTL_INT_DIS      (1 << 31)

/* private adapter context structure */

typedef struct gmacTxDesc
    {
    volatile UINT32 txStatus;
    volatile UINT32 txControl;
    volatile UINT32 pointer;
    UINT32 nextDesc;
    } GMAC_TX_DESC;

typedef struct gmacRxDesc
    {
    volatile UINT32 rxStatus;
    volatile UINT32 rxControl;
    volatile UINT32 pointer;
    UINT32 nextDesc;
    } GMAC_RX_DESC;

typedef struct gmacDrvCtrl
    {
    END_OBJ             gmacEndObj;
    VXB_DEVICE_ID       gmacDev;
    void *              gmacMuxDevCookie;

    JOB_QUEUE_ID        gmacJobQueue;
    QJOB                gmacIntJob;
    volatile BOOL       gmacIntPending;
    QJOB                gmacTxJob;
    volatile BOOL       gmacTxPending;
    QJOB                gmacRxJob;
    volatile BOOL       gmacRxPending;

    volatile BOOL       gmacTxStall;

    BOOL                gmacPolling;
    M_BLK_ID            gmacPollBuf;
    UINT32              gmacIntrs;

    UINT8               gmacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    gmacCaps;

    END_IFDRVCONF       gmacEndStatsConf;
    END_IFCOUNTERS      gmacEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST *     gmacMediaList;
    END_ERR             gmacLastError;
    UINT32              gmacCurMedia;
    UINT32              gmacCurStatus;
    VXB_DEVICE_ID       gmacMiiBus;
    VXB_DEVICE_ID       gmacMiiDev;
    FUNCPTR             gmacMiiPhyRead;
    FUNCPTR             gmacMiiPhyWrite;
    int                 gmacMiiPhyAddr;

    /* end MII/ifmedia required fields */

    GMAC_TX_DESC *     gmacTxDescMem;
    GMAC_RX_DESC *     gmacRxDescMem;

    /* possibly unaligned desc buffer to free */

    char*               descBuf;

    M_BLK_ID            gmacRxMblk[GMAC_RX_DESC_CNT];
    M_BLK_ID            gmacTxMblk[GMAC_TX_DESC_CNT];

    UINT32              gmacTxProd;
    UINT32              gmacTxCons;
    UINT32              gmacTxFree;
    UINT32              gmacRxIdx;

    SEM_ID              gmacMiiSem;

    int                 gmacMaxMtu;

    void *              regBase[GMAC_MOD_NUM];
    void *              handle[GMAC_MOD_NUM];

    UINT32              gmacDescMem;
    UINT32              gmacMdcDiv;
    UINT32              gmacMedia;

    /* prevent the access race condition */

    SEM_ID              gmacDevSem;
    } GMAC_DRV_CTRL;

/* GMAC module register low level access routines */

#define GMAC_BAR0(p)       ((GMAC_DRV_CTRL *)(p)->pDrvCtrl)->regBase[0]
#define GMAC_HANDLE0(p)    ((GMAC_DRV_CTRL *)(p)->pDrvCtrl)->handle[0]

#define CSR0_READ_4(pDev, addr)             \
        vxbRead32(GMAC_HANDLE0(pDev),      \
                  (UINT32 *)((char *)GMAC_BAR0(pDev) + addr))

#define CSR0_WRITE_4(pDev, addr, data)      \
        vxbWrite32(GMAC_HANDLE0(pDev),     \
                   (UINT32 *)((char *)GMAC_BAR0(pDev) + addr), data)

#define CSR0_SETBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) | (val))

#define CSR0_CLRBIT_4(pDev, offset, val)    \
        CSR0_WRITE_4(pDev, offset, CSR0_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbStmGmacEndh */
