/* vxbSmscLan9118End.h - header file for SMSC LAN9118 VxBus END driver */

/*
 * Copyright (c) 2007, 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,11aug11,rbc  WIND00238999 - Fix build errors
01d,28jul11,rbc  WIND00238999 - Add VSB _WRS_CONFIG_PWR_MGMT
01c,03jun11,rec  WIND00255358 - device power management
01b,20jul10,fao  add be8 support.
01a,23may07,wap  written
*/

#ifndef __INCvxbSmscLan9118Endh
#define __INCvxbSmscLan9118Endh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void smeRegister (void);

#ifndef BSP_VERSION

/*
 * SMSC 91C118 device registers
 */

#define SME_RXDATA_FIFO		0x00 /* RX data access port */
#define SME_TXDATA_FIFO		0x20 /* TX data access port */
#define SME_RXSTS_FIFO		0x40 /* RX status access port */
#define SME_RXSTS_FIFO_PEEK	0x44 /* RX status peek port */
#define SME_TXSTS_FIFO		0x48 /* TX status access port */
#define SME_TXSTS_FIFO_PEEK	0x4C /* TX status peek port */
#define SME_ID_REV		0x50 /* Chip ID and revision */
#define SME_IRQ_CFG		0x54 /* Main interrupt configuration */
#define SME_ISR			0x58 /* Interrupt status */
#define SME_IMR			0x5C /* Interrupt mask */
#define SME_RSVD0		0x60
#define SME_BYTE_TEST		0x64 /* Byte order testing register */
#define SME_FIFO_INT		0x68 /* FIFO level interrupts */
#define SME_RX_CFG		0x6C /* Receive configuration */
#define SME_TX_CFG		0x70 /* Transmit configuration */
#define SME_HW_CFG		0x74 /* Hardware configuration */
#define SME_RX_DP_CTL		0x78 /* RX datapath control */
#define SME_RX_FIFO_INF		0x7C /* RX FIFO information */
#define SME_TX_FIFO_INF		0x80 /* TX FIFO information */
#define SME_PMT_CTRL		0x84 /* Power management control */
#define SME_GPIO_CFG		0x88 /* General purpose I/O configuration */
#define SME_GPT_CFG		0x8C /* General purpose timer configuration */
#define SME_GPT_CNT		0x90 /* General purpose timer count */
#define SME_RSVD1		0x94
#define SME_ENDIAN		0x98 /* Endianness */
#define SME_FREE_RUN		0x9C /* Free running counter */
#define SME_RX_DROP		0xA0 /* Dropped frame count */
#define SME_MAC_CMD		0xA4 /* MAC register index */
#define SME_MAC_DATA		0xA8 /* MAC register data */
#define SME_AFG_CFG		0xAC /* Automatic flow control config */
#define SME_EE_CMD		0xB0 /* EEPROM index */
#define SME_EE_DATA		0xB4 /* EEPROM data */

/*
 * MAC registers, accessed via MAC_IDX and MAC_DATA registers
 */

#define SME_MAC_CR		0x1 /* MAC control register */
#define SME_MAC_ADDRH		0x2 /* MAC address high */
#define SME_MAC_ADDRL		0x3 /* MAC address low */
#define SME_MAC_HASHH		0x4 /* multicast hash filter high */
#define SME_MAC_HASHL		0x5 /* multicast hash filter low */
#define SME_MAC_MIICMD		0x6 /* MII control register */
#define SME_MAC_MIIDATA		0x7 /* MII data register */
#define SME_MAC_FLOW		0x8 /* flow control */
#define SME_MAC_VLAN1		0x9 /* VLAN tag 1 */
#define SME_MAC_VLAN2		0xA /* VLAN tag 2 */
#define SME_MAC_WUFF		0xB /* Wakeup frame filter */
#define SME_MAC_WUCSR		0xC /* Wakeup command and status */

/* Interrupt config register */

#define SME_IRQCFG_INT_DEAS	0xFF000000 /* int deassert interval */
#define SME_IRQCFG_INT_DEAS_CLR	0x00004000 /* int deassert interval clear */
#define SME_IRQCFG_INT_DEAS_STS 0x00002000 /* int deassert interval status */
#define SME_IRQCFG_IRQ_INT	0x00001000 /* master interrupt */
#define SME_IRQCFG_IRQ_ENABLE	0x00000100 /* IRQ pin enable */
#define SME_IRQCFG_IRQ_POLARITY 0x00000010 /* IRQ pin polarity */
#define SME_IRQCFG_IRQ_TYPE	0x00000001 /* IRQ buffer type */

#define SME_DEASINT(x)		(((x) << 24) & SME_IRQCFG_INT_DEAS)

/* Interrupt status and mask registers */

#define SME_ISR_SW_INT		0x80000000 /* software interrupt */
#define SME_ISR_TXSTOP		0x02000000 /* STOP_TX in TX_CFG is set */
#define SME_ISR_RXSTOP		0x01000000 /* receiver is halted */
#define SME_ISR_RXDFH		0x00800000 /* RX dropped frame counter oflow */
#define SME_ISR_TXIOC		0x00200000 /* TX interrupt on completion */
#define SME_ISR_RXDMA		0x00100000 /* RX DMA complete */
#define SME_ISR_GPT		0x00080000 /* General purpose timer wrap */
#define SME_ISR_PHY		0x00040000 /* PHY interrupt */
#define SME_ISR_PME		0x00020000 /* Power management event */
#define SME_ISR_TXSO		0x00010000 /* TX status FIFO overflow */
#define SME_ISR_RWT		0x00008000 /* RX watchdog (giant frame) */
#define SME_ISR_RXE		0x00004000 /* RX error */
#define SME_ISR_TXE		0x00002000 /* TX error */
#define SME_ISR_TDFU		0x00000800 /* TX data FIFO underrun */
#define SME_ISR_TDFO		0x00000400 /* TX data FIFO overrun */
#define SME_ISR_TDFA		0x00000200 /* TX data FIFO available */
#define SME_ISR_TSFF		0x00000100 /* TX status FIFO full */
#define SME_ISR_TSFL		0x00000080 /* TX status FIFO level */
#define SME_ISR_RXDF		0x00000040 /* RX frame dropped */
#define SME_ISR_RDFL		0x00000020 /* RX data FIFO level */
#define SME_ISR_RSFF		0x00000010 /* RX status FIFO full */
#define SME_ISR_RSFL		0x00000008 /* RX status FIFO level */
#define SME_ISR_GPIO2		0x00000004 /* GPIO interrupts */
#define SME_ISR_GPIO1		0x00000002 /* GPIO interrupts */
#define SME_ISR_GPIO0		0x00000001 /* GPIO interrupts */

/* FIFO interrupt register */

#define SME_FIFOINT_TXDATA_LVL	0xFF000000 /* TX data level, 64 byte blocks */
#define SME_FIFOINT_TXSTS_LVL	0x00FF0000 /* TX status level, dwords */
#define SME_FIFOINT_RXDATA_LVL	0x0000FF00 /* RX free space, 64 byte blocks */
#define SME_FIFOINT_RXSTS_LVL	0x000000FF /* RX status level, dwords */

/* Receive control register */

#define SME_RXCFG_ENDALIGN	0xC0000000 /* termination alignment */
#define SME_RXCFG_DMACNT	0x0FFF0000 /* RX DMA count (dwords) */
#define SME_RXCFG_DUMP		0x00008000 /* Empty status and data FIFOs */
#define SME_RXCFG_DATAOFF	0x00001F00 /* Data offset (bytes) */

#define SME_RXENDALIGN_4	0x00000000
#define SME_RXENDALIGN_16	0x40000000
#define SME_RXENDALIGN_32	0x80000000

#define SME_RXDATAOFF(x)	(((x) << 8) & SME_RXCFG_DATAOFF)

/* Transmit control register */

#define SME_TXCFG_DUMP_STS	0x00008000 /* Empty status FIFO */
#define SME_TXCFG_DUMP_DATA	0x00004000 /* Empty data FIFO */
#define SME_TXCFG_STS_OFLOW_OK	0x00000004 /* Continue TX on status overrun */
#define SME_TXCFG_TX_ON		0x00000002 /* Transmit enable */
#define SME_TXCFG_TX_OFF	0x00000001 /* Transmit disable */

/* Hardware configuration register */

#define SME_HWCFG_TXTHRMODE	0x00200000 /* 1 == 10Mbps, 0 == 100Mbps */
#define SME_HWCFG_STORENFWD	0x00100000 /* buffer full frame on TX */
#define SME_HWCFG_TXFIFOLEN	0x000F0000 /* TX fifo size */
#define SME_HWCFG_TXTHRCTL	0x00003000 /* TX threshold control */
#define SME_HWCFG_32_16		0x00000004 /* 1 == 32 bit mode, 0 == 16 */
#define SME_HWCFG_RST_TIMEOUT	0x00000002 /* Reset timed out */
#define SME_HWCFG_RST		0x00000001 /* Issue soft reset */

#define SME_TXFIFOLEN_1536	0x00020000 /* RX = 13440/896 */
#define SME_TXFIFOLEN_2560	0x00030000 /* RX = 12480/832 */
#define SME_TXFIFOLEN_3584	0x00040000 /* RX = 11520/768 */
#define SME_TXFIFOLEN_4608	0x00050000 /* RX = 10560/704 */
#define SME_TXFIFOLEN_5632	0x00060000 /* RX = 9600/640 */
#define SME_TXFIFOLEN_6656	0x00070000 /* RX = 8640/576 */
#define SME_TXFIFOLEN_7680	0x00080000 /* RX = 7680/512 */
#define SME_TXFIFOLEN_8704	0x00090000 /* RX = 6720/448 */
#define SME_TXFIFOLEN_9728	0x000A0000 /* RX = 5760/384 */
#define SME_TXFIFOLEN_10752	0x000B0000 /* RX = 4800/320 */
#define SME_TXFIFOLEN_11776	0x000C0000 /* RX = 3840/256 */
#define SME_TXFIFOLEN_12800	0x000D0000 /* RX = 2880/192 */
#define SME_TXFIFOLEN_13824	0x000E0000 /* RX = 1920/128 */

#define SME_TXTHRCTL_72_128	0x00000000
#define SME_TXTHRCTL_96_256	0x00001000
#define SME_TXTHRCTL_128_512	0x00002000
#define SME_TXTHRCTL_160_1024	0x00003000

/* RX datapath control register */

#define SME_RXDPCTL_FF		0x80000000 /* Fast forward to next frame */

/* RX FIFO information register */

#define SME_RXFIFOINF_STS_USED	0x00FF0000 /* RX status dwords used */
#define SME_RXFIFOINF_DATA_USED	0x0000FFFF /* RX data bytes used */

/* TX FIFO information register */

#define SME_TXFIFOINF_STS_USED	0x00FF0000 /* TX status dwords used */
#define SME_TXFIFOINF_DATA_FREE	0x0000FFFF /* TX free bytes available */

/* Power management control register */

#define SME_PMTCTRL_MODE	0x00003000 /* Power management mode */
#define SME_PMTCTRL_PHYRST	0x00000400 /* Reset internal PHY */
#define SME_PMTCTRL_WOL_ON	0x00000200 /* Wake on LAN enable */
#define SME_PMTCTRL_ENDET_ON	0x00000100 /* Energy detect enable */
#define SME_PMTCTRL_PME_BUFTYPE	0x00000040 /* PME buffer type */
#define SME_PMTCTRL_WUPS	0x00000030 /* Wakeup status */
#define SME_PMTCTRL_PME_IND	0x00000008 /* PME indication */
#define SME_PMTCTRL_PME_POL	0x00000004 /* PME polarity */
#define SME_PMTCTRL_PNE_ON	0x00000002 /* PME enable */
#define SME_PMTCTRL_DEV_RDY 	0x00000001 /* device is ready */

#define SME_PMODE_D0		0x00000000
#define SME_PMODE_D1		0x00001000
#define SME_PMODE_D2		0x00002000
#define SME_PMODE_MSK		0x00003000

#define SME_WUPS_NOEVENT	0x00000000 /* No wake up event detected */
#define SME_WUPS_ENERGYDET	0x00000010 /* Energy detected */
#define SME_WUPS_WOL		0x00000020 /* Wake up frame/magic pkt detected*/
#define SME_WUPS_MULTI		0x00000030 /* multiple events */

/* GPIO configuration register */

#define SME_GPIOCFG_LED2	0x40000000 /* 1 = LED output, 0 = GPIO signal*/
#define SME_GPIOCFG_LED1	0x20000000 /* 1 = LED output, 0 = GPIO signal*/
#define SME_GPIOCFG_LED0	0x10000000 /* 1 = LED output, 0 = GPIO signal*/
#define SME_GPIOCFG_INTPOL2	0x04000000 /* 1 = int on high, 0 = int on lo */
#define SME_GPIOCFG_INTPOL1	0x02000000 /* 1 = int on high, 0 = int on lo */
#define SME_GPIOCFG_INTPOL0	0x01000000 /* 1 = int on high, 0 = int on lo */
#define SME_GPIOCFG_EECTL	0x00700000
#define SME_GPIOCFG_DIR2	0x00000400 /* 1 = output, 0 = input */
#define SME_GPIOCFG_DIR1	0x00000200 /* 1 = output, 0 = input */
#define SME_GPIOCFG_DIR0	0x00000100 /* 1 = output, 0 = input */
#define SME_GPIOCFG_DATA4	0x00000010 /* always output */
#define SME_GPIOCFG_DATA3	0x00000008 /* always output */
#define SME_GPIOCFG_DATA2	0x00000004 /* in if DIR2 = 0, out if DIR2 = 1 */
#define SME_GPIOCFG_DATA1	0x00000002 /* in if DIR1 = 0, out if DIR1 = 1 */
#define SME_GPIOCFG_DATA0	0x00000001 /* in if DIR0 = 0, out if DIR0 = 1 */

#define SME_EECMD_EEDIO_EECLK	0x00000000
#define SME_EECMD_GPO3_GPO4	0x00100000
#define SME_EECMD_RSVD0		0x00200000
#define SME_EECMD_GPO3_RXDV	0x00300000
#define SME_EECMD_RSVD1		0x00400000
#define SME_EECMD_TXEN_GPO4	0x00500000
#define SME_EECMD_TXEN_RXDV	0x00600000
#define SME_EECMD_TXCLK_RXCLK	0x00700000

/* General purpose timer configuration register */

#define SME_GPTCFG_TIMER_ON	0x20000000 /* Timer enabled */
#define SME_GPTCFG_PRELOAD	0x0000FFFF /* Value preloaded into timer */

/* MAC command register */

#define SME_MACCMD_BUSY		0x80000000 /* MAC access in progress */
#define SME_MACCMD_RW		0x40000000 /* 1 = read access, 0 = write */
#define SME_MACCMD_ADDR		0x000000FF /* MAC register index */

/* Automatic flow control register */

#define SME_AFCCFG_HI		0x00FF0000 /* AFC high level */
#define SME_AFCCFG_LO		0x0000FF00 /* AFC low level */
#define SME_AFCCFG_BACKDIR	0x000000F0 /* Backpressure duration */
#define SME_AFCCFG_FCMCAST	0x00000008 /* Flow control on multicast */
#define SME_AFCCFG_FCBCAST	0x00000004 /* Flow control on broadcast */
#define SME_AFCCFG_FCUCAST	0x00000002 /* Flow control on unicast */
#define SME_AFCCFG_FCANY	0x00000001 /* Flow control on any frame */

/* EEPROM command register */

#define SME_EECMD_BUSY		0x80000000 /* EEPROM access in progress */
#define SME_EECMD_EEACT		0x70000000 /* EEPROM access command */
#define SME_EECMD_TIMEOUT	0x00000200 /* EEPROM access timed out */
#define SME_EECMD_MAC_LOADED	0x00000100 /* MAC address loaded */
#define SME_EECMD_EEADDR	0x000000FF /* EEPROM address to access */

#define SME_EEACT_READ		0x00000000 /* Read location */
#define SME_EEACT_EWDS		0x10000000 /* Erase/write disable */
#define SME_EEACT_EWEN		0x20000000 /* Erase/write enable */
#define SME_EEACT_WRITE		0x30000000 /* Write location */
#define SME_EEACT_WRAL		0x40000000 /* Write all */
#define SME_EEACT_ERASE		0x50000000 /* Erase location */
#define SME_EEACT_ERAL		0x60000000 /* Erase all */
#define SME_EEACT_RELOAD	0x70000000 /* MAC address reload */

/* MAC command register */

#define SME_MACCR_RXALL		0x80000000 /* 1 = no destaddr filtering */
#define SME_MACCR_NORCVOWN	0x00800000 /* 0 = FDX, 1 = HDX */
#define SME_MACCR_LOOPBACK	0x00200000 /* 0 = no loop, 1 = MII loopback */
#define SME_MACCR_FDX		0x00100000 /* 1 = FDX, 0 = HDX */
#define SME_MACCR_ALLMULTI	0x00080000 /* 1 = receive all multicasts */
#define SME_MACCR_PROMISC	0x00040000 /* 1 = promiscuous mode */
#define SME_MACCR_INVERSE	0x00020000 /* 1 = inverse filtering mode */
#define SME_MACCR_RXBAD		0x00010000 /* 1 = receive error frames */
#define SME_MACCR_HASHONLY	0x00008000 /* 1 = unicast hash filtering */
#define SME_MACCR_HASHPERFECT	0x00002000 /* 1 = mcast hash + dstaddr */
#define SME_MACCR_LATECOLL	0x00001000 /* 1 = retransmit late colls */
#define SME_MACCR_NOBCAST	0x00000800 /* 1 = don't receive bcasts */
#define SME_MACCR_NORETRY	0x00000400 /* 1 = don't retry collisions */
#define SME_MACCR_RXSTRIP	0x00000200 /* 1 = strip pad and FCS on RX */
#define SME_MACCR_BACKOFFLIM	0x000000C0 /* backoff limit */
#define SME_MACCR_DEFERCHK	0x00000020 /* 1 = enable TX deferal check */
#define SME_MACCR_TX_ON		0x00000008 /* 1 = enable TX */
#define SME_MACCR_RX_ON		0x00000004 /* 1 = enable RX */

/* MII access register */

#define SME_MACMIICMD_PHYADDR	0x0000F800 /* PHY address to access */
#define SME_MACMIICMD_REGADDR	0x000007C0 /* register to access */
#define SME_MACMIICMD_WRITE	0x00000002 /* 0 = read, 1 = write */
#define SME_MACMIICMD_BUSY	0x00000001 /* access in progress */

#define SME_PHYADDR(x)		(((x) << 11) & SME_MACMIICMD_PHYADDR)
#define SME_REGADDR(x)		(((x) << 6) & SME_MACMIICMD_REGADDR)

/* MII data register */

#define SME_MACMIIDATA_VAL	0x0000FFFF /* PHY register value */

/* PHY registers */

#define SME_PHY_BCR		0      /* Phy control register */
#define SME_PHY_MODE_STATUS	17     /* Phy Mode control/status */
#define SME_PHY_SPECTLSTAT_IND	27     /* phy special control status indicator*/
#define SME_PHY_ISR		29
#define SME_PHY_IMR		30
#define SME_PHY_SPECTLSTAT      31     /* phy special control status  */

/* BCR register bits */

#define SME_PHY_BCR_SOFTRESET   0x0001 /* Software Reset */
#define SME_PHY_BCR_PWR_DWN	0x0800 /* power down */	
#define SME_PHY_BCR_AUTONEG_EN  0x1000 /* auto-negotiation enable */

/* SME_PHY_MODE_STATUS register bits */

#define SME_PHY_ENERGYON        0x0002 /* SME_PHY_MODE_STATUS Energy present */
#define SME_PHY_EDPWRDOWN       0x2000 /* Energy Detect Power Down Enable */

#define SME_PHYISR_ENERGYON	0x0080 /* ENERGYON generated */
#define SME_PHYISR_ANEGDONE	0x0040 /* autonegotiation complete */
#define SME_PHYISR_REMFAULT	0x0020 /* remove fault detected */
#define SME_PHYISR_LINKDOWN	0x0010 /* link down */
#define SME_PHYISR_ANEGLPACK	0x0008 /* autoneg link partner ack */
#define SME_PHYISR_PARDETFAULT	0x0004 /* parallel detection fault */
#define SME_PHYISR_ANEGPGRX	0x0002 /* autoneg page received */


/* RX and TX buffer header and status formats */

typedef struct sme_tx_desc
    {
    UINT32	sme_txa;
    UINT32	sme_txb;
    } SME_TX_DESC;

#define SME_TXA_INT		0x80000000 /* interrupt on completion */
#define SME_TXA_ENDALIGN	0x03000000 /* buffer termination alignment */
#define SME_TXA_DATASTART	0x001F0000 /* data start offset */
#define SME_TXA_SOF		0x00002000 /* Start of frame */
#define SME_TXA_EOF		0x00001000 /* End of frame */
#define SME_TXA_BUFLEN		0x000007FF /* Size of buffer */

#define SME_TXENDALIGN_4	0x00000000
#define SME_TXENDALIGN_16	0x01000000
#define SME_TXENDALIGN_32	0x02000000

#define SME_TXOFF(x)		(((x) << 16) & SME_TXA_DATASTART)

#define SME_TXB_TAG		0xFFFF0000 /* User defined packet tag */
#define SME_TXB_NOCRC		0x00002000 /* Don't compute CRC */
#define SME_TXB_NOPAD		0x00001000 /* Don't autopad short frames */
#define SME_TXB_PKTLEN		0x000007FF /* Total frame length */

/* TX status word */

#define SME_TXSTS_TAG		0xFFFF0000 /* User defined packet tag */
#define SME_TXSTS_ERR		0x00008000 /* TX error */
#define SME_TXSTS_CARRLOSS	0x00000800 /* Carrier loss */
#define SME_TXSTS_NOCARR	0x00000400 /* No carrier */
#define SME_TXSTS_LATECOLL	0x00000200 /* Late collision */
#define SME_TXSTS_EXCESSCOLL	0x00000100 /* Excessive collisions */
#define SME_TXSTS_COLLCNT	0x00000078 /* Collision count */
#define SME_TXSTS_EXCESSDEFER	0x00000004 /* Excessive deferals */
#define SME_TXSTS_UFLOW		0x00000002 /* TX Underrun */
#define SME_TXSTS_DEFERRED	0x00000001 /* Transmission deferred */

/* RX status word */

#define SME_RXSTS_NOMATCH	0x40000000 /* Packet didn't match any filter */
#define SME_RXSTS_PKTLEN	0x3FFF0000 /* Packet length */
#define SME_RXSTS_ERR		0x00008000 /* RX error */
#define SME_RXSTS_BCAST		0x00002000 /* Broadcast frame */
#define SME_RXSTS_LENERR	0x00001000 /* Length error */
#define SME_RXSTS_RUNT		0x00000800 /* Runt frame */
#define SME_RXSTS_MCAST		0x00000400 /* Multicast frame */
#define SME_RXSTS_GIANT		0x00000080 /* Giant frame */
#define SME_RXSTS_COLLSEEN	0x00000040 /* Collision seen */
#define SME_RXSTS_FRAMETYPE	0x00000020 /* 1 = ethernet2, 0 = 802.3 */
#define SME_RXSTS_WDOG_TIMEOUT	0x00000010 /* RX watchdog timeout */
#define SME_RXSTS_MIIERR	0x00000008 /* MII error */
#define SME_RXSTS_DRIBBLE	0x00000004 /* Dribbling bit set */
#define SME_RXSTS_CRCERR	0x00000002 /* CRC error */

#define SME_RXLEN(x)		(((x) & SME_RXSTS_PKTLEN) >> 16)

#define SME_MAX_RX 16
#define SME_MAXFRAG 16
#define SME_TX_INC(x, y)      (x) = (((x) + 1) % y)
#define SME_TX_CNT 16
#define SME_ETHER_ALIGN 2

#define SME_MTU 1500
#define SME_NAME	"sme"
#define SME_TIMEOUT 10000
#define SME_READY_TIMEOUT 1000000
#define SME_INTRS  (SME_RXINTRS|SME_TXINTRS|SME_LINKINTRS)
#define SME_RXINTRS (SME_ISR_RSFL|SME_ISR_RXE)
#define SME_TXINTRS (SME_ISR_TSFL|SME_ISR_TXE|SME_ISR_TXIOC)
#define SME_LINKINTRS SME_ISR_PHY

/*
 * Private adapter context structure.
 */

typedef struct sme_drv_ctrl
    {
    END_OBJ		smeEndObj;
    VXB_DEVICE_ID	smeDev;
    void *		smeMuxDevCookie;
    void *		smeBar;
    void *		smeHandle;
    JOB_QUEUE_ID	smeJobQueue;
    QJOB		smeIntJob;
    atomicVal_t		smeIntPending;

    QJOB		smeRxJob;
    atomicVal_t		smeRxPending;

    QJOB		smeTxJob;
    atomicVal_t		smeTxPending;
    UINT8		smeTxCur;
    UINT8		smeTxLast;
    volatile BOOL	smeTxStall;
    UINT16		smeTxThresh;

    BOOL		smePolling;
    M_BLK_ID		smePollBuf;
    UINT32		smeIntMask;
    UINT32		smeIntrs;

    UINT8		smeAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	smeCaps;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	smeMediaList;
    END_ERR		smeLastError;
    UINT32		smeCurMedia;
    UINT32		smeCurStatus;
    VXB_DEVICE_ID	smeMiiBus;
    /* End MII/ifmedia required fields */

    M_BLK_ID		smeTxMblk[SME_TX_CNT];

    UINT32              smeTxProd;
    UINT32              smeTxCons;
    UINT32              smeTxFree;

    int			smeMaxMtu;

    SEM_ID		smeDevSem;
#if (defined _WRS_CONFIG_PWR_MGMT) && (defined _WRS_ARCH_HAS_DEV_PWR_MGMT)
    TASK_ID             tSmeWakeup;
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */
    } SME_DRV_CTRL;

#define SME_BAR(p)   ((SME_DRV_CTRL *)(p)->pDrvCtrl)->smeBar
#define SME_HANDLE(p)   ((SME_DRV_CTRL *)(p)->pDrvCtrl)->smeHandle

#ifdef ARMBE8
#    define SWAP32 vxbSwap32
#else
#    define SWAP32 
#endif /* ARMBE8 */

#define CSR_READ_4(pDev, addr)				\
    SWAP32(vxbRead32 (SME_HANDLE(pDev), (UINT32 *)((char *)SME_BAR(pDev) + addr)))

#define CSR_WRITE_4(pDev, addr, data)			\
    vxbWrite32 (SME_HANDLE(pDev),			\
        (UINT32 *)((char *)SME_BAR(pDev) + addr), SWAP32(data))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbSmscLan9118Endh */
