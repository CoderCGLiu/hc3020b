/* templateVxbEnd.h - header file for template VxBus ENd driver */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,23aug08,wap  Update to use vxbReadXX()/vxbWriteXX() register access
                 routines
01c,30oct07,pmr  SMP awareness
01b,07dec06,wap  Add jumbo frame support
01a,14apr06,wap  written
*/

#ifndef __INCtemplateVxbEndh
#define __INCtemplateVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void templateVxbEndRegister (void);

#ifndef BSP_VERSION

#define TEMPLATE_MTU 1500
#define TEMPLATE_JUMBO_MTU 9000
#define TEMPLATE_CLSIZE	1536
#define TEMPLATE_NAME	"template"
#define TEMPLATE_TIMEOUT 10000
#define TEMPLATE_INTRS 0
#define TEMPLATE_RXINTRS 0
#define TEMPLATE_TXINTRS 0
#define TEMPLATE_LINKINTRS 0

/*
 * Private adapter context structure.
 */

typedef struct template_drv_ctrl
    {
    END_OBJ		templateEndObj;
    VXB_DEVICE_ID	templateDev;
    void *		templateHandle;
    void *		templateBar;
    void *		templateMuxDevCookie;

    JOB_QUEUE_ID	templateJobQueue;
    QJOB		templateIntJob;
    volatile BOOL	templateIntPending;

    QJOB		templateRxJob;
    volatile BOOL	templateRxPending;

    QJOB		templateTxJob;
    volatile BOOL	templateTxPending;
    UINT8		templateTxCur;
    UINT8		templateTxLast;
    volatile BOOL	templateTxStall;
    UINT16		templateTxThresh;

    QJOB		templateLinkJob;
    volatile BOOL	templateLinkPending;

    BOOL		templatePolling;
    M_BLK_ID		templatePollBuf;
    UINT16		templateIntMask;

    UINT8		templateAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	templateCaps;

    END_IFDRVCONF	templateEndStatsConf;
    END_IFCOUNTERS	templateEndStatsCounters;

    SEM_ID		templateDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	templateMediaList;
    END_ERR		templateLastError;
    UINT32		templateCurMedia;
    UINT32		templateCurStatus;
    VXB_DEVICE_ID	templateMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	templateParentTag;

    int			templateMaxMtu;
    } TEMPLATE_DRV_CTRL;

#define TEMPLATE_BAR(p)		((TEMPLATE_DRV_CTRL *)(p)->pDrvCtrl)->templateBar
#define TEMPLATE_HANDLE(p)	((TEMPLATE_DRV_CTRL *)(p)->pDrvCtrl)->templateHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (TEMPLATE_HANDLE(pDev), (UINT32 *)((char *)TEMPLATE_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (TEMPLATE_HANDLE(pDev),                             \
        (UINT32 *)((char *)TEMPLATE_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (TEMPLATE_HANDLE(pDev), (UINT16 *)((char *)TEMPLATE_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (TEMPLATE_HANDLE(pDev),                             \
        (UINT16 *)((char *)TEMPLATE_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (TEMPLATE_HANDLE(pDev), (UINT8 *)((char *)TEMPLATE_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (TEMPLATE_HANDLE(pDev),                              \
        (UINT8 *)((char *)TEMPLATE_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCtemplateVxbEndh */
