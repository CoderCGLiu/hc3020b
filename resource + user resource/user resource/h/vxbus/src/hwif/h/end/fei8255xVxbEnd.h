/* fei8255xVxbEnd.h - header file for fei VxBus ENd driver */

/*
 * Copyright (c) 2006-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01n,20dec11,jjk  WIND00255693 - Copyright date correction
01m,15nov11,jjk  WIND00255693 - Support CedarRock NM10 Lan controller and
                 82552V PHY
01l,09apr10,jpb  Added logLib.h for _func_logMsg.
01k,27may09,h_k  replaced atomic_t with atomic32_t.
01j,31jan08,l_z  Increase FEI_TIMEOUT.
01i,12nov07,dlk  Increase FEI_CBCNT and FEI_RXCNT to 64.
01h,10aug07,wap  Remove _WRS_VX_SMP macros (WIND00101788)
01g,29jun07,wap  Convert to new vxbAccess mechanism
01f,04apr07,wap  Add PCI ID for 82562GX part
01e,03mar07,wap  Make this driver SMP safe
01d,18feb07,wap  Add a few more PCI IDs
01c,16jan07,wap  Update descriptor handling to reduce hwMem usage
01b,17jun06,wap  Add MII semaphore
01a,14apr06,wap  written
*/

#ifndef __INCfei8255xVxbEndh
#define __INCfei8255xVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

#include <logLib.h>		/* _func_logMsg */

#define FEI_LOGMSG(x,a,b,c,d,e,f)		\
    do {					\
	if (_func_logMsg != NULL)		\
	    _func_logMsg (x,			\
			  (_Vx_usr_arg_t)a,	\
			  (_Vx_usr_arg_t)b,	\
			  (_Vx_usr_arg_t)c,	\
			  (_Vx_usr_arg_t)d,	\
			  (_Vx_usr_arg_t)e,	\
			  (_Vx_usr_arg_t)f);	\
	} while (FALSE)

IMPORT void feiRegister (void);

#ifndef BSP_VERSION

#define INTEL_VENDORID	0x8086

#define INTEL_DEVICEID_PRO100CARDBUS	0x1029
#define INTEL_DEVICEID_PRO100_559	0x1030
#define INTEL_DEVICEID_PRO100ICH3VE	0x1031
#define INTEL_DEVICEID_PRO100ICH3VE2	0x1032
#define INTEL_DEVICEID_PRO100ICH3VM	0x1033
#define INTEL_DEVICEID_PRO100ICH3VM2	0x1034
#define INTEL_DEVICEID_PRO100ICH3	0x1035
#define INTEL_DEVICEID_PRO100ICH3_2	0x1036
#define INTEL_DEVICEID_PRO100ICH3_3	0x1037
#define INTEL_DEVICEID_PRO100ICH3VM3	0x1038
#define INTEL_DEVICEID_PRO100ICH4VE	0x1039
#define INTEL_DEVICEID_PRO100ICH4_1	0x103A
#define INTEL_DEVICEID_PRO100ICH4VM	0x103B
#define INTEL_DEVICEID_PRO100ICH4_2	0x103C
#define INTEL_DEVICEID_PRO100ICH4VM2	0x103D
#define INTEL_DEVICEID_PRO100ICH4VE2	0x103E
#define INTEL_DEVICEID_PRO100D865VE	0x1050
#define INTEL_DEVICEID_PRO100ICH5	0x1051
#define INTEL_DEVICEID_PRO100M		0x1059
#define INTEL_DEVICEID_PRO100ICH6	0x1064
#define INTEL_DEVICEID_PRO100ICH6T	0x1065
#define INTEL_DEVICEID_PRO100ICH6M	0x1068
#define INTEL_DEVICEID_PRO100ICH6EM	0x1069
#define INTEL_DEVICEID_82562GX		0x1091
#define INTEL_DEVICEID_PRO100VE		0x1092
#define INTEL_DEVICEID_PRO100VM		0x1093
#define INTEL_DEVICEID_PRO100ICH7	0x1094
#define INTEL_DEVICEID_PRO100ER		0x1209
#define INTEL_DEVICEID_PRO100		0x1229
#define INTEL_DEVICEID_ICH2		0x2449
#define INTEL_DEVICEID_ICH2_2		0x2459
#define INTEL_DEVICEID_ICH2_3		0x245D
#define INTEL_DEVICEID_ICH7		0x27DC
#define INTEL_DEVICEID_SERVER		0x5200
#define INTEL_DEVICEID_SERVER_2		0x5201

/*
 * The generic PRO/100 ID (0x1229) has many variants identified by
 * the PCI revision code.
 */

#define INTEL_REVID_PRO100_82557_1	0x01
#define INTEL_REVID_PRO100_82557_2	0x02
#define INTEL_REVID_PRO100_82557_3	0x03
#define INTEL_REVID_PRO100_82558_1	0x04
#define INTEL_REVID_PRO100_82558_2	0x05
#define INTEL_REVID_PRO100_82559_1	0x06
#define INTEL_REVID_PRO100_82559_2	0x07
#define INTEL_REVID_PRO100_82559_3	0x08
#define INTEL_REVID_PRO100_82559_4	0x09
#define INTEL_REVID_PRO100_82550_1	0x0C
#define INTEL_REVID_PRO100_82550_2	0x0D
#define INTEL_REVID_PRO100_82551_1	0x0E
#define INTEL_REVID_PRO100_82551_2	0x0F
#define INTEL_REVID_PRO100_82551_3	0x10

/* Device registers */

#define FEI_SCB_STS		0x0000	/* 8 bits */
#define FEI_SCB_ISR		0x0001	/* 8 bits */
#define FEI_SCB_CMD		0x0002	/* 8 bits */
#define FEI_SCB_IMR		0x0003	/* 8 bits */
#define FEI_SCB_GP		0x0004	/* 32 bits */
#define FEI_PORT		0x0008	/* 32 bits */
#define FEI_EECTL		0x000E	/* 16 bits */
#define FEI_MDIO		0x0010	/* 32 bits */
#define FEI_RXDMA_BYTES		0x0014	/* 32 bits */
#define FEI_FLOWCTL		0x0019	/* 16 bits */
#define FEI_PWRMGMT		0x001B	/* 8 bits */
#define FEI_GENCTL		0x001C	/* 8 bits */
#define FEI_CENCTL		0x001D	/* 8 bits */

/*
 * Status register, provides status of the command unit
 * and the receive unit.
 */

#define FEI_SCBSTS_CUSTS	0xC0
#define FEI_SCBSTS_RUSTS	0x3C
#define FEI_SCBSTS_RSVD		0x03

#define FEI_CUSTS_IDLE		0x00
#define FEI_CUSTS_SUSPENDED	0x40
#define FEI_CUSTS_LPQ_ACTIVE	0x80
#define FEI_CUSTS_HQP_ACTIVE	0xC0

#define FEI_RUSTS_IDLE		0x00
#define FEI_RUSTS_SUSPENDED	0x04
#define FEI_RUSTS_NORESOURCE	0x08	/* out of RFDs */
#define FEI_RUSTS_READY		0x10
#define FEI_RUSTS_SUSPNORBD	0x24	/* suspended with no RBDs */
#define FEI_RUSTS_RNRNORBD	0x28	/* out of resources due to no RBDs */
#define FEI_RUSTS_RDYNORBD	0x30	/* ready with no RBDs */

/*
 * Interrupt status register
 */

#define FEI_SCBISR_CXTNO	0x80	/* command completed */
#define FEI_SCBISR_FR		0x40	/* frame received */
#define FEI_SCBISR_CNA		0x20	/* CU left active state */
#define FEI_SCBISR_RNR		0x10	/* RU left ready state */
#define FEI_SCBISR_MDI		0x08	/* MDIO transaction done */
#define FEI_SCBISR_SWI		0x04	/* software interrupt */
#define FEI_SCBISR_RSVD		0x02
#define FEI_SCBISR_FCP		0x01	/* flow ctl packet received */

/*
 * SCB command register
 */

#define FEI_SCBCMD_CUCMD	0xF0	/* command unit command */
#define FEI_SCBCMD_RSVD		0x08
#define FEI_SCBCMD_RUCMD	0x07	/* receive unit command */

#define FEI_CUCMD_NOP		0x00
#define FEI_CUCMD_START		0x10
#define FEI_CUCMD_RESUME	0x20
#define FEI_CUCMD_LOADSTATSADDR	0x40
#define FEI_CUCMD_DUMPSTATS	0x50
#define FEI_CUCMD_LOADCUBASE	0x60
#define FEI_CUCMD_DUMPRSTSTATS	0x70
#define FEI_CUCMD_STATICRESUME	0xA0	/* 82558 and later only */

#define FEI_RUCMD_NOP		0x00
#define FEI_RUCMD_START		0x01	/* load offset from RU base */
#define FEI_RUCMD_RESUME	0x02
#define FEI_RUCMD_DMAREDIRECT	0x03	/* reload RBD pointer for GP */
#define FEI_RUCMD_ABORT		0x04
#define FEI_RUCMD_LOADHDRSZ	0x05	/* load header data size */
#define FEI_RUCMD_LOADRUBASE	0x06	/* load RX area base addr from GP */
#define FEI_RUCMD_RBDRESUME	0x07

/*
 * Interrupt mask register
 */

#define FEI_SCBIMR_CXTNO	0x80
#define FEI_SCBIMR_FR		0x40
#define FEI_SCBIMR_CNA		0x20
#define FEI_SCBIMR_RNR		0x10
#define FEI_SCBIMR_ER		0x08
#define FEI_SCBIMR_FCP		0x04
#define FEI_SCBIMR_SWI		0x02	/* trigger software interrupt */
#define FEI_SCBIMR_MASKALL	0x01	/* mask all interrupts */

/*
 * PORT command register
 */

#define FEI_PORT_SOFTRESET	0x00000000
#define FEI_PORT_SELFTEST	0x00000001
#define FEI_PORT_SELECTIVE_RST	0x00000002
#define FEI_PORT_DUMP		0x00000003

/*
 * EEPROM control register
 */

#define FEI_EECTL_CLK		0x0001	/* clock pin control */
#define FEI_EECTL_CS		0x0002	/* chip select */
#define FEI_EECTL_DIN		0x0004	/* data in */
#define FEI_EECTL_DOUT		0x0008	/* data out */

/* 9346 EEPROM commands */

#define FEI_9346_WRITE		0x5
#define FEI_9346_READ		0x6
#define FEI_9346_ERASE		0x7

/*
 * MDIO register
 */

#define FEI_MDIO_DATA		0x0000FFFF /* read/write data */
#define FEI_MDIO_PHYREG		0x001F0000 /* PHY bus address */
#define FEI_MDIO_PHYADDR	0x03E00000 /* PHY register */
#define FEI_MDIO_OPCODE		0x0C000000 /* opcode, read or write */
#define FEI_MDIO_RDY		0x10000000 /* transaction completed */
#define FEI_MDIO_INTREN		0x20000000 /* MDI interrupt on completion */

#define FEI_MDIO_WRITE		0x04000000
#define FEI_MDIO_READ		0x08000000

#define FEI_PHYREG(x)		(((x) << 16) & FEI_MDIO_PHYREG)
#define FEI_PHYADDR(x)		(((x) << 21) & FEI_MDIO_PHYADDR)


/*
 * Command structures.
 * Each command has a header portion which is the same for all
 * commands, followed by additional command-specific fields.
 */

typedef struct fei_cbhdr
    {
    volatile UINT16	fei_sts;
    volatile UINT16	fei_cmd;
    volatile UINT32	fei_linkaddr;
    } FEI_CBHDR;

#define FEI_CBSTS_OK		0x2000
#define FEI_CBSTS_COMPLETE	0x8000

#define FEI_CBCMD_OPCODE	0x0009
#define FEI_CBCMD_SF		0x0008	/* simple or flexible mode */
#define FEI_CBCMD_I		0x2000	/* generate interrupt */
#define FEI_CBCMD_S		0x4000	/* suspend when complete */
#define FEI_CBCMD_EL		0x8000	/* end of list */

#define FEI_CBCMD_NOP		0x0000
#define FEI_CBCMD_IAS		0x0001	/* set individual/station address */
#define FEI_CBCMD_CONF		0x0002	/* load config block */
#define FEI_CBCMD_MCAST		0x0003	/* load multicast filter list */
#define FEI_CBCMD_XMIT		0x0004	/* normal transmit */
#define FEI_CBCMD_UCODE		0x0005	/* upload new microcode */
#define FEI_CBCMD_DUMP		0x0006	/* dump internal registers */
#define FEI_CBCMD_DIAG		0x0007	/* run diagnostics */
#define FEI_CBCMD_LOADFILT	0x0008	/* load WOL filter */
#define FEI_CBCMD_IPCBXMIT	0x0009	/* IPCB transmit (checksum offload) */

/* NOP command has no special fields */

typedef volatile FEI_CBHDR FEI_CB_NOP;

/*
 * Configuration command. This structure contains a series of bytes
 * containing configuration bits that are used to control the operation
 * of the controller. Unfortunately, the 82557, 82558, 82559 and
 * 82550/82551 all interpret the configuration block a little
 * differently.
 * For the 82557/8/9, the config block is 22 bytes in size. For the
 * 82550/82551, it's 32 bytes. Some of the additional bytes are needed
 * to enable 'gamla' receive mode, which allows us to use RX TCP/IP
 * checksum offload.
 */

#define FEI_CFLEN	22
#define FEI_CFLEN_EXT	32

typedef struct fei_cb_cf
    {
    volatile FEI_CBHDR	fei_hdr;
    volatile UINT8	fei_cfbyte[FEI_CFLEN_EXT];
    } FEI_CB_CF;


#define FEI_CFBYTE1_RXFIFO	0x0F
#define FEI_CFBYTE1_TXFIFO	0x70

#define FEI_CFBYTE2_AIFS	0xFF	/* adaptive interframe spacing */

#define FEI_CFBYTE3_MWI_ENB	0x01	/* MWI enable */
#define FEI_CFBYTE3_TYPE_ENB	0x02
#define FEI_CFBYTE3_RALIGN_ENB	0x04	/* read alignment enable */
#define FEI_CFBYTE3_TWRCL_ENB	0x08	/* terminate write on cache line */

#define FEI_CFBYTE4_MAXRXDMA	0x7F

#define FEI_CFBYTE5_MAXTXDMA	0x7F
#define FEI_CFBYTE5_DMBC_ENB	0x80	/* enable DMA max byte counters */

#define FEI_CFBYTE6_LATESCB	0x01	/* 82557 only */
#define FEI_CFBYTE6_MBO_1	0x02	/* must be one on 82557 */
#define FEI_CFBYTE6_TNO_INT	0x04	/* enable TX no good interrupt, 82557 */
#define FEI_CFBYTE6_TCO_STATS	0x04	/* enable TCO stats, 82559 only */
#define FEI_CFBYTE6_CI_INT	0x08
#define FEI_CFBYTE6_EXT_TXCB	0x10	/* extented TXCB, 82558 and later */
#define FEI_CFBYTE6_MBO_2	0x10	/* must be one on 82557 */
#define FEI_CFBYTE6_EXT_STATS	0x20	/* extented stats, 82558 and later */
#define FEI_CFBYTE6_MBO_3	0x20	/* must be one on 82557 */
#define FEI_CFBYTE6_DROP_ORUNS	0x40	/* discard RX overrun frames */
#define FEI_CFBYTE6_RX_BADPKTS	0x80	/* save bad frames */

#define FEI_CFBYTE7_DROP_RUNTS	0x01	/* discard runts */
#define FEI_CFBYTE7_UFLOW_RETRY	0x06	/* TX underrun retries */
#define FEI_CFBYTE7_EXT_RFD	0x20	/* extended RFD, 82550 only */
#define FEI_CFBYTE7_2PKTINFIFO	0x40	/* 2 frames in FIFO, 82558 and later */
#define FEI_CFBYTE7_DYN_TBD	0x80	/* dynamic TBD, 82558 and later */

#define FEI_CFBYTE8_503_MII	0x01	/* serial v. MII, 82557 only */
#define FEI_CFBYTE8_CSMA_DIS	0x80	/* CSMA disable, 82558 and later */

#define FEI_CFBYTE9_CSUM_ENB	0x01	/* TCP/UDP csum, 82559 only */
#define FEI_CFBYTE9_VLANTCO	0x10	/* 82559 only */
#define FEI_CFBYTE9_VLANARP	0x10	/* 82558 only */
#define FEI_CFBYTE9_LNK_WKUP	0x20	/* link wakeup, 82558 and later */
#define FEI_CFBYTE9_ARP_WKUP	0x40	/* arp wakeup, 82558 only */
#define FEI_CFBYTE9_MC_WKUP	0x80	/* mcast match wakeup, 82558 only */

#define FEI_CFBYTE10_MBO_1	0x02
#define FEI_CFBYTE10_MBO_2	0x04
#define FEI_CFBYTE10_NSAI	0x08
#define FEI_CFBYTE10_PREAMLEN	0x30
#define FEI_CFBYTE10_LOOPBK	0xC0

#define FEI_CFBYTE11_LINEARPRI	0x07	/* 82557 only */

#define FEI_CFBYTE12_LPRIMODE	0x01	/* 82557 only */
#define FEI_CFBYTE12_IFS	0xF0	/* interframe spacing */

#define FEI_CFBYTE13_IPADDRHI	0xFF	/* IP addr high, 82558B only */
#define FEI_CFBYTE14_IPADDRLO	0xFF	/* IP addr low, 82558B only */

#define FEI_CFBYTE15_PROMISC	0x01	/* promisc mode */
#define FEI_CFBYTE15_NOBROAD	0x02	/* broadcast RX disable */
#define FEI_CFBYTE15_WAITWIN	0x04	/* wait after win, 558/559 only */
#define FEI_CFBYTE15_MBO_1	0x08	/* must be one */
#define FEI_CFBYTE15_IGN_UL	0x10	/* ignore U/L bit on addr match */
#define FEI_CFBYTE15_CRC16	0x20	/* use CRC16 instead of CRC32 */
#define FEI_CFBYTE15_MBO_2	0x40	/* must be one */
#define FEI_CFBYTE15_CRS_CDT	0x80	/* 1 for serial, 0 for MII */

#define FEI_CFBYTE16_FC_LOW	0xFF	/* flow control delay, LSB */

#define FEI_CFBYTE17_FC_HIGH	0xFF	/* flow control delay, MSB */

#define FEI_CFBYTE18_STRIP	0x01	/* RX stripping */
#define FEI_CFBYTE18_PADDING	0x02	/* pad short TX frames */
#define FEI_CFBYTE18_RXCRC	0x04	/* include CRC with RX frames */
#define FEI_CFBYTE18_GIANTOK	0x08	/* upload giant frames, 82558/9 only */
#define FEI_CFBYTE18_FCTHRESH	0x70	/* flow control threshold, 82558/9 */
#define FEI_CFBYTE18_MBO_557	0xF0	/* must be one on 82557 */

#define FEI_CFBYTE19_IAWAKE	0x01	/* addr match wakeup, 82558 only */
#define FEI_CFBTYE19_MAGICWAKE	0x02	/* magic packet wakeup, 82558/9 only */
#define FEI_CFBYTE19_TXFC	0x04	/* transmit flow control, 82558/9 */
#define FEI_CFBYTE19_RXFC_RSTP	0x08	/* RX flow control restop, 82558/9 */
#define FEI_CFBYTE19_RXFC_RSTR	0x10	/* RX flow control restart, 82558/9 */
#define FEI_CFBYTE19_FC_REJECT	0x20	/* reject flow control, 82558/9 only */
#define FEI_CFBYTE19_FORCE_FDX	0x40	/* force full duplex */
#define FEI_CFBYTE19_AUTO_FDX	0x80	/* auto fdx */

#define FEI_CFBYTE20_MBO	0x1F	/* must be one */
#define FEI_CFBYTE20_PRIFCLOC	0x20	/* priority FC location, 82558/9 only */
#define FEI_CFBYTE20_MULTI_IA	0x40	/* multi IA instead of MC */

#define FEI_CFBYTE21_MBO	0x05	/* must be one */
#define FEI_CFBYTE21_ALLMULTI	0x08	/* allmulticast mode */

#define FEI_CFBYTE22_TCPIP_CSUM	0x01	/* TCP/IP csum offload, 82550 only */
#define FEI_CFBYTE22_VLAN_STRIP	0x02	/* RX VLAN stripping, 82550 only */

/* Default values for 82557 */

#define FEI_CFBYTE1_82557	0x08
#define FEI_CFBYTE2_82557	0x00
#define FEI_CFBYTE3_82557	0x00
#define FEI_CFBYTE4_82557	0x00
#define FEI_CFBYTE5_82557	0x00
#define FEI_CFBYTE6_82557	(FEI_CFBYTE6_MBO_1|FEI_CFBYTE6_MBO_2| \
				FEI_CFBYTE6_MBO_3|FEI_CFBYTE6_RX_BADPKTS)
#define FEI_CFBYTE7_82557	FEI_CFBYTE7_DROP_RUNTS|0x2
#define FEI_CFBYTE8_82557	FEI_CFBYTE8_503_MII
#define FEI_CFBYTE9_82557	0x00
#define FEI_CFBYTE10_82557	(FEI_CFBYTE10_MBO_1|FEI_CFBYTE10_MBO_2|	\
				FEI_CFBYTE10_NSAI|0x20)
#define FEI_CFBYTE11_82557	0x00
#define FEI_CFBYTE12_82557	0x60
#define FEI_CFBYTE13_82557	0x00
#define FEI_CFBYTE14_82557	0xF2
#define FEI_CFBYTE15_82557	FEI_CFBYTE15_MBO_1|FEI_CFBYTE15_MBO_2
#define FEI_CFBYTE16_82557	0x00
#define FEI_CFBYTE17_82557	0x40
#define FEI_CFBYTE18_82557	FEI_CFBYTE18_PADDING|FEI_CFBYTE18_MBO_557
#define FEI_CFBYTE19_82557	FEI_CFBYTE19_AUTO_FDX
#define FEI_CFBYTE20_82557	FEI_CFBYTE20_MBO|FEI_CFBYTE20_PRIFCLOC
#define FEI_CFBYTE21_82557	FEI_CFBYTE21_MBO

/* Default values for 82558 */

#define FEI_CFBYTE1_82558	0x08
#define FEI_CFBYTE2_82558	0x00
#define FEI_CFBYTE3_82558	(FEI_CFBYTE3_MWI_ENB|FEI_CFBYTE3_RALIGN_ENB| \
				FEI_CFBYTE3_TWRCL_ENB)
#define FEI_CFBYTE4_82558	0x00
#define FEI_CFBYTE5_82558	0x00
#define FEI_CFBYTE6_82558	FEI_CFBYTE6_EXT_STATS|FEI_CFBYTE6_EXT_TXCB
#define FEI_CFBYTE7_82558	FEI_CFBYTE7_DROP_RUNTS|0x2
#define FEI_CFBYTE8_82558	FEI_CFBYTE8_503_MII
#define FEI_CFBYTE9_82558	0x00
#define FEI_CFBYTE10_82558	(FEI_CFBYTE10_MBO_1|FEI_CFBYTE10_MBO_2|	\
				FEI_CFBYTE10_NSAI|0x20)
#define FEI_CFBYTE11_82558	0x00
#define FEI_CFBYTE12_82558	0x60
#define FEI_CFBYTE13_82558	0x00
#define FEI_CFBYTE14_82558	0xF2
#define FEI_CFBYTE15_82558	FEI_CFBYTE15_MBO_1|FEI_CFBYTE15_MBO_2
#define FEI_CFBYTE16_82558	0x00
#define FEI_CFBYTE17_82558	0x40
#define FEI_CFBYTE18_82558	(FEI_CFBYTE18_PADDING|FEI_CFBYTE18_GIANTOK| \
                                FEI_CFBYTE18_MBO_557)
#define FEI_CFBYTE19_82558	FEI_CFBYTE19_AUTO_FDX
#define FEI_CFBYTE20_82558	FEI_CFBYTE20_MBO|FEI_CFBYTE20_PRIFCLOC
#define FEI_CFBYTE21_82558	FEI_CFBYTE21_MBO

/* Default values for 82559 */

#define FEI_CFBYTE1_82559	0x08
#define FEI_CFBYTE2_82559	0x00
#define FEI_CFBYTE3_82559	(FEI_CFBYTE3_MWI_ENB|FEI_CFBYTE3_RALIGN_ENB| \
				FEI_CFBYTE3_TWRCL_ENB)
#define FEI_CFBYTE4_82559	0x00
#define FEI_CFBYTE5_82559	0x00
#define FEI_CFBYTE6_82559	FEI_CFBYTE6_EXT_STATS|FEI_CFBYTE6_EXT_TXCB
#define FEI_CFBYTE7_82559	FEI_CFBYTE7_DROP_RUNTS|0x2
#define FEI_CFBYTE8_82559	FEI_CFBYTE8_503_MII
#define FEI_CFBYTE9_82559	0x00
#define FEI_CFBYTE10_82559	(FEI_CFBYTE10_MBO_1|FEI_CFBYTE10_MBO_2|	\
				FEI_CFBYTE10_NSAI|0x20)
#define FEI_CFBYTE11_82559	0x00
#define FEI_CFBYTE12_82559	0x60
#define FEI_CFBYTE13_82559	0x00
#define FEI_CFBYTE14_82559	0xF2
#define FEI_CFBYTE15_82559	FEI_CFBYTE15_MBO_1|FEI_CFBYTE15_MBO_2
#define FEI_CFBYTE16_82559	0x00
#define FEI_CFBYTE17_82559	0x00
#define FEI_CFBYTE18_82559	(FEI_CFBYTE18_PADDING|FEI_CFBYTE18_GIANTOK| \
                                FEI_CFBYTE18_MBO_557)
#define FEI_CFBYTE19_82559	FEI_CFBYTE19_AUTO_FDX
#define FEI_CFBYTE20_82559	FEI_CFBYTE20_MBO|FEI_CFBYTE20_PRIFCLOC
#define FEI_CFBYTE21_82559	FEI_CFBYTE21_MBO


typedef struct fei_macaddr
    {
    volatile UINT8	fei_octet[ETHER_ADDR_LEN];
    } FEI_ADDR;

/* MAC address set command */

typedef struct fei_cb_ia
    {
    volatile FEI_CBHDR	fei_hdr;
    volatile FEI_ADDR	fei_macaddr;
    } FEI_CB_IA;

/*
 * Multicast filter set command.
 * We allow a maximum of 41 addresses per setup command.
 * At 6 bytes per address, that requires 248 bytes of space.
 * Add in 2 bytes for the count field, and 8 bytes for the
 * CB header, and the total is 256 bytes.
 */

#define FEI_MAXMCADDR	41
    
typedef struct fei_cb_mc
    {
    volatile FEI_CBHDR	fei_hdr;
    volatile UINT16	fei_mccnt;
    volatile FEI_ADDR	fei_macaddr[FEI_MAXMCADDR];
    } FEI_CB_MC;


typedef struct fei_tbd
    {
    volatile UINT32 fei_tbaddr;
    volatile UINT16 fei_tbsize;
    volatile UINT16 fei_rsvd;
    } FEI_TBD;

typedef struct fei_tbd2
    {
    UINT32 fei_tbaddr;
    UINT32 fei_tbsize;
    } FEI_TBD2;

#define FEI_TBD_EL	0x8000
#define FEI_MAXFRAG	28

/*
 * IPCB structure. This is used only on 82550 and 82551 controllers
 * with TCP/IP checksum offload enabled. To use this structure, you
 * must configure the adapter for extended TxCBs, and use the
 * IPCBXMIT command instead of the regular XMIT command.
 */

typedef struct fei_ipcb
    {
    volatile UINT16	fei_ipcb_sched_low;
    volatile UINT8	fei_ipcb_sched;
    volatile UINT8	fei_ipcb_act_high;
    volatile UINT16	fei_ipcb_vlan_id;
    volatile UINT8	fei_ipcb_ip_hdr_offset;
    volatile UINT8	fei_ipcb_tcp_hdr_offset;
    } FEI_IPCB;

/*
 * IPCB field definitions
 */

#define FEI_IPCB_HARDWAREPARSING_ENABLE	0x01
#define FEI_IPCB_INSERTVLAN_ENABLE	0x02
#define FEI_IPCB_IP_CHECKSUM_ENABLE	0x10
#define FEI_IPCB_TCPUDP_CHECKSUM_ENABLE	0x20
#define FEI_IPCB_TCP_PACKET		0x40
#define FEI_IPCB_LARGESEND_ENABLE	0x80

typedef struct fei_cb_tx
    {
    volatile FEI_CBHDR	fei_hdr;
    volatile UINT32	fei_tbdaddr;
    volatile UINT16	fei_count;
    volatile UINT8	fei_txthresh;
    volatile UINT8	fei_tbdcnt;
    union
        {
        volatile FEI_IPCB	fei_ipcb;
	volatile FEI_TBD	fei_tbd[FEI_MAXFRAG];
        } tx_cb_u;
    } FEI_CB_TX;

/*
 * This is the same as the cb_tx structure above, except with
 * structure members expressed as 32 bit fields. This allows the
 * packet transmission code to update several fields at once with
 * a single 32 bit store operation, which provides a small speed
 * optimization.
 */

typedef struct fei_cb_tx2
    {
    UINT32		fei_txcmd;
    UINT32		fei_linkaddr;
    UINT32		fei_tbdaddr;
    UINT32		fei_txparm;
    union
        {
	FEI_TBD2	fei_tbd[FEI_MAXFRAG];
        FEI_IPCB	fei_ipcb;
        } tx_cb_u;
    } FEI_CB_TX2;

#define fei_tbd			tx_cb_u.fei_tbd
#define fei_ipcb_sched_low	tx_cb_u.fei_ipcb.fei_ipcb_sched_low
#define fei_ipcb_sched		tx_cb_u.fei_ipcb.fei_ipcb_sched
#define fei_ipcb_act_high	tx_cb_u.fei_ipcb.fei_ipcb_act_high
#define fei_ipcb_vlan_id	tx_cb_u.fei_ipcb.fei_ipcb_vlan_id
#define fei_ipcb_ip_hdr_offset	tx_cb_u.fei_ipcb.fei_ipcb_ip_hdr_offset
#define fei_ipcb_tcp_hdr_offset	tx_cb_u.fei_ipcb.fei_ipcb_tcp_hdr_offset


#define FEI_TXCB_STS_U		0x1000	/* underrun */
#define FEI_TXCB_STS_OK		0x2000	/* packet transmitted OK */
#define FEI_TXCB_STS_COMPLETE	0x8000	/* DMA complete */

#define FEI_TXCB_CMD_OPCODE	0x0009
#define FEI_TXCB_CMD_SF		0x0008	/* simple or flexible mode */
#define FEI_TXCB_CMD_NC		0x0010	/* no CRC */
#define FEI_TXCB_CMD_CID	0x1F00	/* CNA interrupt delay field valid */
#define FEI_TXCB_CMD_I		0x2000	/* generate interrupt */
#define FEI_TXCB_CMD_S		0x4000	/* suspend when complete */
#define FEI_TXCB_CMD_EL		0x8000	/* end of list */

#define FEI_TBDCNT_EOF		0x8000

/* RX data structures */

typedef struct fei_rbd
    {
    volatile UINT16	fei_framelen;	/* set by chip */
    volatile UINT16	fei_rsvd0;
    volatile UINT32	fei_nextrbd_addr;
    volatile UINT32	fei_rxbuf_addr;
    volatile UINT16	fei_rxbuf_len;	/* set by host */
    volatile UINT16	fei_rsvd1;
    } FEI_RBD;

/* Bits stashed in fei_framelen field */
#define FEI_RBD_EOF	0x8000		/* last RBD in frame */
#define FEI_RBD_F	0x4000		/* RBD is full */

/* Bits stashed in fei_rxbuflen field */
#define FEI_RBD_EL	0x8000		/* end of RBD list */

typedef struct fei_rfd
    {
    volatile FEI_CBHDR	fei_hdr;
    volatile UINT32	fei_rbdaddr;
    volatile UINT16	fei_framelen;
    volatile UINT16	fei_buflen;

    /*
     * The following additonal fields are only available
     * when extended receive mode is used on the
     * 82550 and 82551 devices.
     */

    volatile UINT16	fei_vlan_id;
    volatile UINT8	fei_rx_parser_sts;
    volatile UINT8	fei_rsvd;
    volatile UINT16	fei_security_sts;
    volatile UINT8	fei_csum_sts;
    volatile UINT8	fei_zerocopy_sts;
    volatile UINT8	fei_pad[8];

    volatile FEI_RBD	fei_rbd;
    } FEI_RFD;

#define FEI_RFD_STS_RCOL	0x0001  /* receive collision */
#define FEI_RFD_STS_IAMATCH	0x0002  /* 0 = matches station address */
#define FEI_RFD_STS_NOAMATCH	0x0004  /* 1 = doesn't match anything */
#define FEI_RFD_STS_PARSE	0x0008  /* pkt parse ok (82550/1 only) */
#define FEI_RFD_STS_RXERR	0x0010  /* receive error from PHY */
#define FEI_RFD_STS_TL		0x0020  /* type/length */
#define FEI_RFD_STS_FTS		0x0080  /* frame too short */
#define FEI_RFD_STS_OVERRUN	0x0100  /* DMA overrun */
#define FEI_RFD_STS_RNR		0x0200  /* no resources */
#define FEI_RFD_STS_ALIGN	0x0400  /* alignment error */
#define FEI_RFD_STS_CRC		0x0800  /* CRC error */
#define FEI_RFD_STS_VLANID	0x1000	/* vlan_id field is valid */
#define FEI_RFD_STS_OK		0x2000  /* packet received okay */
#define FEI_RFD_STS_COMPLETE	0x8000  /* packet reception complete */

#define FEI_RFD_STS_ERROR (FEI_RFD_STS_CRC|FEI_RFD_STS_ALIGN| \
    FEI_RFD_STS_RNR|FEI_RFD_STS_OVERRUN|FEI_RFD_STS_RXERR)

#define FEI_RFD_CMD_SF		0x0008  /* simple/flexible memory mode */
#define FEI_RFD_CMD_H		0x0010  /* header RFD */
#define FEI_RFD_CMD_S		0x4000  /* suspend after reception */
#define FEI_RFD_CMD_EL		0x8000  /* end of list */

/* Bits in the 'csum_sts' byte */
#define FEI_RFD_CS_TCPUDP_CSUM_BIT_VALID	0x10
#define FEI_RFD_CS_TCPUDP_CSUM_VALID		0x20
#define FEI_RFD_CS_IP_CSUM_BIT_VALID		0x01
#define FEI_RFD_CS_IP_CSUM_VALID		0x02

/* Bits in the 'packet parser' byte */
#define FEI_RFD_P_PARSE_BIT			0x08
#define FEI_RFD_P_CSUM_PROTOCOL_MASK		0x03
#define FEI_RFD_P_TCP_PACKET			0x00
#define FEI_RFD_P_UDP_PACKET			0x01
#define FEI_RFD_P_IP_PACKET			0x03


/*
 * Private adapter context structure.
 */

#define FEI_ADJ(x)		m_adj((x), 2)
#define FEI_INC(x, y)		(x) = ((x) + 1) % (y)

#define FEI_CURRXMAP(x)		pDrvCtrl->feiRxMblkMap[(x)]
#define FEI_CURTXMAP(x)		pDrvCtrl->feiTxMblkMap[(x)]

#define FEI_MAX_RX	16
#define FEI_CLSIZE	1536
#define FEI_NAME	"fei"
#define FEI_TIMEOUT	100000
#define FEI_INTRS	(FEI_SCBISR_CXTNO|FEI_SCBISR_FR|FEI_SCBISR_CNA|	\
			 FEI_SCBISR_RNR|FEI_SCBISR_MDI|FEI_SCBISR_SWI|	\
                         FEI_SCBISR_FCP)
#define FEI_RXINTRS 	(FEI_SCBISR_FR|FEI_SCBISR_RNR)
#define FEI_TXINTRS 	(FEI_SCBISR_CXTNO|FEI_SCBISR_CNA)

#define FEI_CBCNT	64
#define FEI_RXCNT	64

#define FEI_DEVTYPE_82557	0	/* original 82557 */
#define FEI_DEVTYPE_82558	1	/* i82558 */
#define FEI_DEVTYPE_82559	2	/* i82559 */
#define FEI_DEVTYPE_82550	3	/* i82550 */
#define FEI_DEVTYPE_82551	4	/* i82551 */

#define FEI_PHYADDR_NOTSET          0xFF           /* 0xFF not in use */
#define FEI_OUI_INTEL_82552V        0x001374

typedef struct fei_drv_ctrl
    {
    END_OBJ		feiEndObj;
    VXB_DEVICE_ID	feiDev;
    void *		feiBar;
    void *		feiHandle;
    void		*feiMuxDevCookie;

    JOB_QUEUE_ID	feiJobQueue;
    QJOB		feiRxJob;
    atomic32_t		feiRxPending;
    volatile BOOL	feiRxStall;

    QJOB		feiTxJob;
    atomic32_t		feiTxPending;
    volatile BOOL	feiTxStall;

    QJOB		feiIntJob;
    atomic32_t		feiIntPending;

    BOOL		feiPolling;
    M_BLK_ID		feiPollBuf;
    UINT8		feiIntMask;
    UINT8		feiIntrs;

    UINT8		feiAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	feiCaps;

    END_IFDRVCONF	feiEndStatsConf;
    END_IFCOUNTERS	feiEndStatsCounters;
    UINT32              feiInErrors;
    UINT32              feiInDiscards;
    UINT32              feiInUcasts;
    UINT32              feiInMcasts;
    UINT32              feiInBcasts;
    UINT32              feiInOctets;
    UINT32              feiOutErrors;
    UINT32              feiOutUcasts;
    UINT32              feiOutMcasts;
    UINT32              feiOutBcasts;
    UINT32              feiOutOctets;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST       *feiMediaList;
    END_ERR             feiLastError;
    UINT32              feiCurMedia;
    UINT32              feiCurStatus;
    UINT8               feiMiiPhyAddr;
    
    VXB_DEVICE_ID       feiMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	feiParentTag;

    VXB_DMA_MAP_ID	feiCbMap;
    VXB_DMA_TAG_ID	feiCbTag;
    FEI_CB_MC		*feiCb;
    FEI_CB_MC		*feiPrevCb[FEI_CBCNT];

    VXB_DMA_TAG_ID	feiCmdCbTag;
    FEI_CBHDR		*feiCmdCb;
    VXB_DMA_MAP_ID	feiCmdCbMap;

    VXB_DMA_TAG_ID	feiRfdTag;
    VXB_DMA_MAP_ID	feiRfdMap;
    FEI_RFD		*feiRfd;

    VXB_DMA_TAG_ID	feiMblkTag;
    VXB_DMA_MAP_ID	feiTxMblkMap[FEI_CBCNT];
    VXB_DMA_MAP_ID	feiRxMblkMap[FEI_RXCNT];

    M_BLK_ID		feiTxMblk[FEI_CBCNT];
    M_BLK_ID		feiRxMblk[FEI_RXCNT];

    UINT32		feiTxProd;
    UINT32		feiTxCons;
    UINT32		feiTxFree;
    UINT8		feiTxThresh;

    UINT32		feiRxIdx;

    int			feiEeWidth;

    UINT8		feiDevType;

    BOOL		feiCuResumeWar;

    SEM_ID		feiDevSem;
    } FEI_DRV_CTRL;

#define FEI_BAR(p)   ((FEI_DRV_CTRL *)(p)->pDrvCtrl)->feiBar
#define FEI_HANDLE(p)   ((FEI_DRV_CTRL *)(p)->pDrvCtrl)->feiHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FEI_HANDLE(pDev), (UINT32 *)((char *)FEI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FEI_HANDLE(pDev),                             \
        (UINT32 *)((char *)FEI_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (FEI_HANDLE(pDev), (UINT16 *)((char *)FEI_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (FEI_HANDLE(pDev),                             \
        (UINT16 *)((char *)FEI_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (FEI_HANDLE(pDev), (UINT8 *)((char *)FEI_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (FEI_HANDLE(pDev),                              \
        (UINT8 *)((char *)FEI_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCfei8255xVxbEndh */
