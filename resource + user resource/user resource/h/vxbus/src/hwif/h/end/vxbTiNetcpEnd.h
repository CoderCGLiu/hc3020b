/* vxbTiNetcpEnd.h - header file for TI NETCP VxBus END driver */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,24sep13,y_c  written.
*/

#ifndef __INCvxbTiNetcpEndh
#define __INCvxbTiNetcpEndh

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

IMPORT void vxbTiNetcpRegister(void);

#define SGMII_OFFSET(n)                     ((n <= 2)? ((n - 1) * 0x100): (((n - 1) * 0x100) + 0x100))

#ifndef BIT
#   define BIT(n)                           (1 << (n))
#endif

/* NETCP SGMII offset and bit definitions */

#define NETCP_SGMII_IDVER_REG(n)            (SGMII_OFFSET(n) + 0x000)
#define NETCP_SGMII_SRESET_REG(n)           (SGMII_OFFSET(n) + 0x004)
#define NETCP_SGMII_CTL_REG(n)              (SGMII_OFFSET(n) + 0x010)
#define NETCP_SGMII_STATUS_REG(n)           (SGMII_OFFSET(n) + 0x014)
#define NETCP_SGMII_MRADV_REG(n)            (SGMII_OFFSET(n) + 0x018)
#define NETCP_SGMII_LPADV_REG(n)            (SGMII_OFFSET(n) + 0x020)
#define NETCP_SGMII_TXCFG_REG(n)            (SGMII_OFFSET(n) + 0x030)
#define NETCP_SGMII_RXCFG_REG(n)            (SGMII_OFFSET(n) + 0x034)
#define NETCP_SGMII_AUXCFG_REG(n)           (SGMII_OFFSET(n) + 0x038)

#define SGMII_REG_STATUS_LOCK               BIT(4)
#define SGMII_REG_STATUS_LINK               BIT(0)
#define SGMII_REG_STATUS_AUTONEG            BIT(2)
#define SGMII_REG_CONTROL_AUTONEG           BIT(0)
#define SGMII_REG_CONTROL_MASTER            BIT(5)
#define SGMII_REG_MR_ADV_ENABLE             BIT(0)
#define SGMII_REG_MR_ADV_LINK               BIT(15)
#define SGMII_REG_MR_ADV_FULL_DUPLEX        BIT(12)
#define SGMII_REG_MR_ADV_GIG_MODE           BIT(11)
#define SGMII_REG_SRESET_RT_SOFT_RESET      BIT(0)
#define SGMII_REG_SRESET_SOFT_RESET         BIT(1)

#define SGMII_CONF_TIMEOUT                  1000

/* NETCP MDIO register offset and bit definitions */

#define NETCP_MDIO_CONTROL                  (0x4)
#define NETCP_USERACCESSn(n)                (0x80 + (n) * 0x8)
#define NETCP_MDIO_CLK_DIV                  (0xff)
#define NETCP_MDIO_EN                       (0x40000000)
#define NETCP_MDIO_GO                       (0x80000000)
#define NETCP_MDIO_WRITE                    (0x40000000)
#define NETCP_MDIO_ACK                      (0x20000000)
#define NETCP_PHY_ADDR_SHIFT                (16)
#define NETCP_REG_ADDR_SHIFT                (21)

/* NETCP SWITCH register offset and bit definitions */

#define NETCP_SWITCH_REG_CTL                0x004
#define NETCP_SWITCH_REG_STAT_PORT_EN       0x00c
#define NETCP_SWITCH_REG_MAXLEN             0x040

#define NETCP_SWITCH_REG_VAL_STAT_ENABLE    0xf
#define NETCP_SWITCH_CTL_P0_ENABLE          BIT(2)

/* NETCP ALE register offsets and bit definitions */

#define NETCP_ALE_TBLCTL                    (0x20)
#define NETCP_ALE_WORD0                     (0x3C)
#define NETCP_ALE_WORD1                     (0x38)
#define NETCP_ALE_WORD2                     (0x34)
#define NETCP_ALE_CONTROL                   (0x08)
#define NETCP_ALE_UNKNOWN_VLAN              (0x18)
#define NETCP_ALE_PORTCTL(n)                (0x40 + (n) * 0x4)
#define NETCP_ALE_ENTRY_IDX_MASK            (0x3ff)
#define NETCP_ALE_ENTRY_NR                  (1024)
#define NETCP_ALE_PORT_FW                   (0x3)
#define NETCP_ALE_MULTICAST                 (0x10000000)
#define NETCP_ALE_MULTICAST_FW              (0x40000000)
#define NETCP_ALE_SUPER                     (0x2)
#define NETCP_ALE_ENTRY_MASK                (0x30000000)
#define NETCP_ALE_BLOCK                     (0x2)
#define NETCP_ALE_SECURE                    (0x1)
#define NETCP_ALE_UNICAST                   (0x10000000)
#define NETCP_ALE_VLAN                      (0x20000000)
#define NETCP_ALE_UNICAST_AGEABLE_NOT       (0x0)
#define NETCP_ALE_UNICAST_AGEABLE           (0x4)
#define NETCP_ALE_BYPASS                    (0x10)
#define NETCP_ALE_CTL_NO_LEARN              (0x10)
#define NETCP_ALE_CLR_TABLE                 (0x40000000)
#define NETCP_ALE_EN_TABLE                  (0x80000000)
#define NETCP_ALE_VLAN_AWARE                (0x4)
#define NETCP_ALE_WRITE                     (0x80000000)
#define NETCP_ALE_ENRY_MASK                 (0x3ff)

#define NETCP_ALE_REG_VAL_CTL_RESET_ENABLE  (0xc0000000)
#define NETCP_ALE_REG_VAL_CTL_BYPASS        (0x00000010)
#define NETCP_ALE_VAL_PORTCTL_FORWARD       0x3
#define NETCP_ALE_REG_VAL_PORTCTL_LEARN     0x2

/* NETCP MAC register offsets and bit definitions */

#define MAC_OFFSET(n)                       ((n - 1) * 0x40)

#define NETCP_MAC_ID(n)                     (MAC_OFFSET(n) + 0x000)
#define NETCP_MAC_CONTROL(n)                (MAC_OFFSET(n) + 0x004)
#define NETCP_MAC_STATUS(n)                 (MAC_OFFSET(n) + 0x008)
#define NETCP_MAC_RESET(n)                  (MAC_OFFSET(n) + 0x00c)
#define NETCP_MAC_MAXLEN(n)                 (MAC_OFFSET(n) + 0x010)
#define NETCP_MAC_BOFF(n)                   (MAC_OFFSET(n) + 0x014)
#define NETCP_MAC_RX_PAUSE(n)               (MAC_OFFSET(n) + 0x018)
#define NETCP_MAC_TX_PAUSE(n)               (MAC_OFFSET(n) + 0x01c)
#define NETCP_MAC_EM_CTL(n)                 (MAC_OFFSET(n) + 0x020)
#define NETCP_MAC_PRI(n)                    (MAC_OFFSET(n) + 0x024)

#define NETCP_MAC_RESET_VAL_RESET_MASK      BIT(0)
#define NETCP_MAC_RESET_VAL_RESET           BIT(0)

#define NETCP_MAC_RESET_TIMEOUT             100

#define NETCP_MAC_RX_ENABLE_CONTROL_FRAMES  BIT(24)
#define NETCP_MAC_RX_ENABLE_SHORT_FRAMES    BIT(23)
#define NETCP_MAC_RX_ENABLE_ERROR_FRAMES    BIT(22)
#define NETCP_MAC_RX_ENABLE_EXT_CTL         BIT(18)
#define NETCP_MAC_RX_ENABLE_GIG_FORCE       BIT(17)
#define NETCP_MAC_RX_ENABLE_IFCTL_B         BIT(16)
#define NETCP_MAC_RX_ENABLE_IFCTL_A         BIT(15)
#define NETCP_MAC_RX_ENABLE_CMD_IDLE        BIT(11)
#define NETCP_MAC_TX_ENABLE_SHORT_GAP       BIT(10)
#define NETCP_MAC_ENABLE_GIG_MODE           BIT(7)
#define NETCP_MAC_TX_ENABLE_PACE            BIT(6)
#define NETCP_MAC_ENABLE                    BIT(5)
#define NETCP_MAC_TX_ENABLE_FLOW_CTL        BIT(4)
#define NETCP_MAC_RX_ENABLE_FLOW_CTL        BIT(3)
#define NETCP_MAC_ENABLE_LOOPBACK           BIT(1)
#define NETCP_MAC_ENABLE_FULL_DUPLEX        BIT(0)

/* NETCP software defines */

#define NETCP_MTU                           1500
#define NETCP_JUMBO_MTU                     9000
#define NETCP_CLSIZE                        1536
#define NETCP_TUPLE_CNT                     768
#define NETCP_MAXFRAG                       16
#define NETCP_MAX_RX                        16
#define NETCP_NAME                          "netcp"
#define NETCP_TIMEOUT                       10000

/* NETCP descriptor defines */

#define NETCP_RBD_ERR                       0xF00000
#define NETCP_BD_RETURN_POLICY_ALL_RUTURN   BIT(15)
#define NETCP_BD_RETURN_PUSH_POLICY_HEAD    BIT(14)

/* note: the two macros must equal to that in bsp */

#define NETCP_RX_DESC_CNT                   128
#define NETCP_TX_DESC_CNT                   128

typedef struct netcpAleTbl
    {
    UINT32 word0;
    UINT32 word1;
    UINT8  word2;
    }NETCP_ALE_TBL;

typedef struct netcp_hw_statistics
    {
    /* rx hw statitics */

    UINT32 rxgood;
    UINT32 rxbroadcast;
    UINT32 rxmulticast;
    UINT32 rxpause;
    UINT32 rxcrcerros;
    UINT32 rxalignmenterrors;
    UINT32 rxoversized;
    UINT32 rxjabber;
    UINT32 rxundersized;
    UINT32 rxfrags;
    UINT32 reserve[2];
    UINT32 rxoctets;

    /* tx hw statitics */

    UINT32 txgood;
    UINT32 txbroadcast;
    UINT32 txmulticast;
    UINT32 txpause;
    UINT32 txdefered;
    UINT32 txcollision;
    UINT32 txsinglecol;
    UINT32 txmulticol;
    UINT32 txexceesive;
    UINT32 txlatecol;
    UINT32 txunderrun;
    UINT32 txcariersense;
    UINT32 txoctets;

    UINT32 sz64octets;
    UINT32 sz65_127octets;
    UINT32 sz128_255octets;
    UINT32 sz256_511octets;
    UINT32 sz512_1023octets;
    UINT32 sz1024octets;

    UINT32 netoctets;
    UINT32 rxfifooverrun[3];
    }NETCP_STAT;

typedef struct netcp_desc
    {
    UINT32 descInfo;
    UINT32 tagInfo;
    UINT32 packetInfo;
    UINT32 buffLen;
    UINT32 buffPtr;
    UINT32 nextPtr;
    UINT32 origBuffLen;
    UINT32 origBuffPtr;
    UINT32 timeStamp;
    UINT32 swInfo[3];
    UINT32 psData[19];
    UINT32 index;
    }NETCP_DESC;

/* private adapter context structure */

typedef struct netcp_drv_ctrl
    {
    END_OBJ         netcpEndObj;
    VXB_DEVICE_ID   netcpDev;
    void *          netcpMuxDevCookie;

    JOB_QUEUE_ID    netcpJobQueue;
    QJOB            netcpRxJob;
    atomicVal_t     netcpRxPending;
    QJOB            netcpTxJob;
    atomicVal_t     netcpTxPending;

    UINT8           netcpTxCur;
    UINT8           netcpTxLast;
    volatile BOOL   netcpTxStall;
    UINT16          netcpTxThresh;

    BOOL            netcpPolling;
    M_BLK_ID        netcpPollBuf;
    UINT32          netcpIntMask;
    UINT32          netcpIntrs;

    UINT32          txCmplQueue;
    UINT32          rxRcvQueue;
    UINT32          txSndQueue;
    UINT32          rxFreeQueue;
    UINT32          qpoolQueue;

    UINT8           netcpAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES netcpCaps;

    END_IFDRVCONF   netcpEndStatsConf;
    END_IFCOUNTERS  netcpEndStatsCounters;

    /* begin MII/ifmedia required fields */

    END_MEDIALIST * netcpMediaList;
    END_ERR         netcpLastError;
    UINT32          netcpCurMedia;
    UINT32          netcpCurStatus;
    VXB_DEVICE_ID   netcpMiiBus;
    VXB_DEVICE_ID   netcpMiiDev;
    FUNCPTR         netcpMiiPhyRead;
    FUNCPTR         netcpMiiPhyWrite;
    int             netcpMiiPhyAddr;

    /* functions provided by QMAN driver */

    FUNCPTR         netcpQmPop;
    FUNCPTR         netcpQmPush;
    FUNCPTR         netcpQmssInit;
    FUNCPTR         netcpQmssReset;

    /* end MII/ifmedia required fields */

    VXB_DMA_TAG_ID  netcpParentTag;

    VXB_DMA_TAG_ID  netcpRxDescTag;
    VXB_DMA_TAG_ID  netcpTxDescTag;
    VXB_DMA_MAP_ID  netcpRxDescMap;
    VXB_DMA_MAP_ID  netcpTxDescMap;
    NETCP_DESC *    netcpRxDescMem;
    NETCP_DESC *    netcpTxDescMem;

    VXB_DMA_TAG_ID  netcpMblkTag;
    VXB_DMA_MAP_ID  netcpRxMblkMap[NETCP_RX_DESC_CNT];
    VXB_DMA_MAP_ID  netcpTxMblkMap[NETCP_TX_DESC_CNT];
    M_BLK_ID        netcpRxMblk[NETCP_RX_DESC_CNT];
    M_BLK_ID        netcpTxMblk[NETCP_TX_DESC_CNT];

    UINT32          netcpTxFree;

    SEM_ID          netcpMiiSem;
    spinlockIsr_t   aleLock;

    int             netcpMaxMtu;

    void *          regBase;
    UINT32          sgmiiOffset;
    UINT32          mdioOffset;
    UINT32          switchOffset;
    UINT32          macOffset;
    UINT32          aleOffset;
    UINT32          statsOffset;

    void *          handle;
    UINT32          netcpFreq;
    int             mediaIfMode;
    NETCP_STAT      netcpStat;
    }NETCP_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbTiNetcpEndh */
