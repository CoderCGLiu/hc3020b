/* vxbSmscLan91c111End.h - SMSC 91C111 END header */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,24sep09,wap  stability fixes: clean up PHY access handling, streamline
                 interrupt handling (WIND00168723)
01a,02feb08,l_z  ported from devcSmsc91c111End.h 
*/

/* MII and PHY Notes */

/*
MDIO: Management Data input/output. Bidirectional signal between MAC and PHY that
      carries management data. All control and status information sent over this
      pin is driven and sampled synchronously to the rising edge of the MDC signal.

MDC: Management Data clock. Sourced by the MAC as a timing reference for transfer
     of information on the MDIO signal. MDC is a periodic signal with no maximum
     high or low times. The minimum high and low times should be 160 ns each and
     the minimum period of the signal should be 400 ns. These values are regardless
     of the nominal period of the TX and RX clocks (Ethernet data transfer clocks)
     Management Frame Structure


Management Frames   PRE   ST OP PHYAD REGAD TA DATA             IDLE

READ                1...1 01 10 AAAAA RRRRR Z0 DDDDDDDDDDDDDDDD Z
WRITE               1...1 01 01 AAAAA RRRRR 10 DDDDDDDDDDDDDDDD Z

The table above shows the format for the read and write management frames. Each
character represents one 'bit' on the MDIO line.

The fields are:


PRE:   Preamble. 32 contiguous logic '1's sent by the MAC on MDIO along with 32
       corresponding cycles on MDC. This provides synchronization for the PHY.

ST:    Start of Frame. Indicated by a 01 pattern.

OP:    Operation code. Read = 10. Write = 01.

PHYAD: PHY address. Up to 32 PHYs can be connected to one MAC. This 5 bit field
       selects which PHY the frame is directed to.

REGAD: Register Address. This is a 5 bit field that selects which one of the 32
       registers of the PHY this operation refers to.

TA:    Turnaround. This is a two bit time spacing between the register address and
       the data field of a frame to avoid contention during a read transaction.

DATA:  Data. These are the 16 bits of Data.

IDLE: Idle Condition, not actually part of the management frame. This is a high
      impedance state. Electrically, the PHY's pull-up resistor will pull the
      MDIO line to a logic one. The PHY Address is usually hardwired through
      hardware pins of the PHY device. Check your hardware schematics for the
      PHY Address you are using.


MII Registers in the PHY:

PHY  Register Map  Register Address Register Name       Basic/Extended

Reg  0             Control                              (Basic)
Reg  1             Status                               (Basic)
Regs 2,3           PHY ID                               (Extended)
Reg  4             Auto-Negotiation Advertisement       (Extended)
Reg  5             Auto-Negotiation Link Partner        (Extended)
Reg  6             Auto-Negotiation Expansion           (Extended)
Reg  7             Auto-Negotiation Next Page Transmit  (Extended)
Regs 8..15         Reserved                             (Extended)
Regs 16..31        Vendor specific                      (Extended)

The basic register set consists of two registers: Control and Status. All PHYs
that provide an MII should incorporate these two registers. The other registers
in the table are optional MII defined (2 through 7) or proprietary (16 through 32)
extensions.


Bit 15 Bit 14 Bit 13 Bit 12 Bit 11 Bit 10 Bit 9 Bit 8


Bit 7  Bit 6  Bit 5  Bit 4  Bit 3  Bit 2  Bit 1 Bit 0



The values below each bit denotes Type and Power Up/Reset default value.

Types are:

R:  Read Only

W:  Write Only

RW: Read/Write

SC: Self clearing

LH: Latch high

LL: Latch low


Reg 0: Control Register

Reset     Loopback Speed Nway Enab. PwrDwn Isolate NwayRest. Duplex
RW,SC,0   RW,0     RW,1  RW,1       RW,0   RW,1    RW,SC,0   RW,0

Coll. Test. Reserved Reserved Reserved Reserved Reserved Reserved Reserved
RW,0        RW       RW       RW       RW       RW       RW       RW



Reset: A '1' written to this bit will initiate a reset of the PHY. The bit is
       self-clearing, and the PHY will return a '1' on reads to this bit until
       the reset is completed. Write transactions to this register may be ignored
       while the PHY is processing the reset. All PHY registers will be driven to
       their default states after a reset.

Loopback: Writing a '1' will put the PHY into loopback mode.

Speed: When Auto Negotiation is disabled this bit can be used to manually select
       the link speed. Writing a '1' to this bit selects 100 Mbps, a '0' selects
       10 Mbps. When Auto-Negotiation is enabled reading or writing this bit has
       no meaning/effect, unless he PHY only supports one speed in which case the
       bit will always reflect that value and will not changed upon a write
       operation. Note that the default of this bit is '1' unless the PHY only
       supports 10Mbps (reported in status register) in which case the default
       is '0'.


Nway Enab: Auto-negotiation (NWAY) is on when this bit is '1'. In that case the
           contents of bits Speed and Duplex are ignored and the NWAY process
           determines the link configuration. If the PHY does not support NWAY
           this bit should be read as zero. Note that the default of this bit is
           '1' unless the PHY does not support NWAY (reported in status register)
           in which case the default is '0'.


PwrDwn: Setting this bit to '1' will put the PHY in Power Down mode. In this state
        the PHY will respond to management transactions.

Isolate: Setting this bit will set the PHY to an isolated mode in which it will
         respond to MII management frames over the MII management interface but
         will ignore data on the MII data interface.

Nway Rest: This bit will return 0 if the PHY does not support NWAY or if NWAY is
           disabled through the Nway Enab bit. If neither of the previous is true,
           setting this bit to '1' restarts the NWAY process. This bit is self
           clearing and the PHY will return a '1' until NWAY is initiated, writing
           a '0' does not affect the NWAY process.

Duplex: When Auto Negotiation is disabled this bit can be used to manually select
        the link duplex state. Writing a '1' to this bit selects full duplex while
        a '0' selects half duplex. When Auto-Negotiation is enabled reading or
        writing this bit has no effect, unless the PHY only supports one duplex mode
        in which case the bit will always reflect that value and will not change
        upon a write operation. Note that the default of this bit is '0' unless
        the PHY only supports full duplex (reported in status register) in which
        case the default is '1'.


Coll. Test: Setting a '1' allows for testing of the MII COL signal. '0' allows
            normal operation.

Reserved: Reserved for future standardization. Should be written as 0 and ignored
          when read.


Reg 1: Status Register

100BaseT4 100BaseXD 100BaseXH 10Mbps-D 10Mbps-H Reserved Reserved Reserved
RO        RO        RO        RO       RO       RO       RO       RO

Res. MFPre Sup Nway Comp Rem.Fault Nway Ability Link Status Jabber Det. Extend Cap
RO   RO        RO        RO,LH     RO           RO,LL       RO,LH       RO


100BaseT4: '1' Indicates 100Base-T4 capable PHY, '0' not capable.

100BaseXD: '1' Indicates 100Base-X full duplex capable PHY, '0' not capable.

100BaseXH: '1' Indicates 100Base-X half duplex capable PHY, '0' not capable.

10Mbps-D:  '1' Indicates 10Mbps full duplex capable PHY, '0' not capable.

10Mbps-H:  '1' Indicates 10Mbps half duplex capable PHY, '0' not capable.

Reserved:  Reserved for future standardization. Should be ignored when read.

MFPre Sup: MF Preamble suppression ability. '1' indicates the PHY is able to
           receive management frames even if not preceded by a preamble. '0' when
           it is not able.

Nway Comp: Auto-Negotiation Complete. When read as '1' indicates Nway has been
           completed and that contents in registers 4,5,6 and 7 are valid. '0' means
           Nway has not completed and contents in registers 4,5,6 and 7 are
           meaningless. The PHY returns zero if it lacks the ability to perform
           Nway or Nway is disabled.

Rem. fault: '1' Indicates a Remote Fault. Latches the '1' condition and is cleared
            by reading this register or resetting the PHY.

Nway Ability: Indicates the ability ('1') to perform NWAY or not ('0').

Link Status: A '1' indicates a valid Link and a '0' and invalid Link. The '0'
             condition is latched until this register is read.

Jabber Det: Jabber condition detected when '1' for 10Mbps. '1' latched until this
            register is read or the PHY is reset. Always '0' for 100Mbps.

Extend. Cap: Extended Capability register. '1' Indicates extended registers are
             implemented.


Regs 2,3: PHY Identifier Registers

These two registers (offsets 2 and 3) provide a 32 bit value unique to a PHY. The
information includes manufacturer identifier, model within the manufacturer and
revision number.


Reg 4: Auto-Negotiation Advertisement Register

Next Page  Reserved  Rem.Fault TA7 TA6 TA5 TA4 TA3
RW         R0        RW        RW  RW  RW  RW  RW

TA2 TA1 TA0 S4 S3 S2 S1 S0
RW  RW  RW  RW RW RW RW RW


This register controls the values transmitted by the PHY to the remote partner
when advertising its abilities.


Next Page: A '1' Indicates the PHY wishes to exchange Next Page information. Beyond
           the scope of these comments. See additional professional references for
           more information

Reserved: Write as '0'. Ignored when Read.

Rem. Fault: Remote Fault. When set, an advertisement frame will be sent with the
            corresponding bit set. This in turn will cause the PHY receiving it to
            set the Remote Fault bit in its Status register.

TA 0..7: Technology Ability Field. This byte holds the current technologies
         supported by the auto-negotiation standard. Currently the bits assigned
         are:
             TA0 10Base-T
             TA1 10Base-T Full Duplex
             TA2 100Base-TX
             TA3 100Base-TX Full Duplex
             TA4 100Base-T4
             TA5 Reserved for future technology
             TA6 Reserved for future technology
             TA7 Reserved for future technology

The management entity sets the value of this field prior to autonegotiation. '1' in
TAn bit indicates that the mode of operation that corresponds to TAn will be
acceptable to be auto-negotiated to. Only modes supported by the PHY can be set.


S 0..4: Selector Field Value. For SMSC current devices (802.3) S[4..0] = 00001.


Reg 5: Auto-Negotiation Link Partner Ability Register

Next Page Ack Rem.Fault TA7 TA6 TA5 TA4 TA3
RW        R0  RW        RW  RW  RW  RW  RW

TA2 TA1 TA0 S4 S3 S2 S1 S0
RW  RW  RW  RW RW RW RW RW

The bit definitions are analogous to the Auto Negotiation Advertisement Register
except for the following:

Ack: Acknowledge. It is used by the Auto-negotiation function to indicate that a
device has successfully received its Link Partner's Link code Word. The PHY may
support additional pages of information and wish to exchange them with its link
partner. This is beyond the scope of these notes. See references 1 and 3
for more information.

Regs 6-7: Auto-Negotiation Expansion Register and Auto-Negotiation Next Page
          Transmit Register

These two registers are used for the Next Page Exchange which is beyond the scope
of these notes.

Regs 8-15: Reserved Registers

These Registers are reserved for future standardization by 802.3.

Regs 16-31: Vendor Specific Registers

These registers are vendor specific. If implemented, refer to the spec of the PHY
you are using for details. For example, some PHYs implement the capability of
generating an interrupt upon Auto-Negotiation completion.
*/

#ifndef vxbSmscLan91c111Endh
#define vxbSmscLan91c111Endh

#ifdef __cplusplus
extern "C" {
#endif

#include <end.h>			/* Common END structures. */
#ifndef DOC             /* don't include when building documentation */
#include <net/mbuf.h>
#endif  /* DOC */
#include <netBufLib.h>
#include <cacheLib.h>
#include <netinet/if_ether.h>

/* Defines */

#define SMSC_NAME "smsc"
#define SMSC_NAME_LEN 7
#define SMSC_CLSIZE 1536
#define SMSC_TIMEOUT 10000

/** #define MTU_CLUSTER_SIZE   (ETHERMTU + ENET_HDR_REAL_SIZ + 6) **/
#define MTU_CLUSTER_SIZE   (ETHERMTU + SIZEOF_ETHERHEADER + 6)

/* BANK 0-3 I/O Space mapping */

#define SMSC_BANK_SELECT       0xE     /* see chip manual for details */

/* BANK 0 I/O Space mapping */

#define SMSC_TCR               0x0     /* transmit control register */
#define SMSC_EPH               0x2     /* EPH status register */
#define SMSC_RCR               0x4     /* receive control register */
#define SMSC_COUNTER           0x6     /* counter register */
#define SMSC_MIR               0x8     /* memory information register */
#define SMSC_RPCR              0xA     /* receive phy control register */

/* Transmit Control Register */

#define SMSC_TCR_SWFDUPLX      0x8000  /* enable switched full duplex (overrides SMSC_TCR_FDUPLX and SMSC_TCR_MON_CNS) */
#define SMSC_TCR_EPH_LOOP      0x2000  /* internal loopback at EPH block */
#define SMSC_TCR_STP_SQET      0x1000  /* stop trans on signal quality error */
#define SMSC_TCR_FDUPLX        0x0800  /* enable full duplex */
#define SMSC_TCR_MON_CNS       0x0400  /* monitors the carrier while trans */
#define SMSC_TCR_NOCRC         0x0100  /* NOT append CRC to frame if set */
#define SMSC_TCR_PAD           0x0080  /* pad short frames to 64 bytes */
#define SMSC_TCR_FORCOL        0x0040  /* force collision, then auto resets */
#define SMSC_TCR_LOOP          0x0020  /* internal loopback */
#define SMSC_TCR_TXEN          0x0001  /* transmit enabled when set */
#define SMSC_TCR_RESET         0x0000  /* Disable the transmitter */

/* EPH Status Register - stores status of last transmitted frame */

#define SMSC_TS_TXUNRN         0x8000  /* tramsmit under run */
#define SMSC_TS_LINK_OK        0x4000  /* twisted pair link cond */
#define SMSC_TS_RX_OVRN        0x2000  /* on FIFO overrun... */
#define SMSC_TS_CTR_ROL        0x1000  /* counter rollover */
#define SMSC_TS_EXC_DEF        0x0800  /* excessive deferal */
#define SMSC_TS_LOST_CARR      0x0400  /* ## lost carrier sense */
#define SMSC_TS_LATCOL         0x0200  /* ## late collision */
#define SMSC_TS_TX_DEFR        0x0080  /* transmit deferred - auot cleared */
#define SMSC_TS_LTX_BRD        0x0040  /* last frame was broascast packet */
#define SMSC_TS_SQET           0x0020  /* ## signal quality error test */
#define SMSC_TS_16COL          0x0010  /* ## 16 collisions reached */
#define SMSC_TS_LXT_MULT       0x0008  /* last frame was multicast */
#define SMSC_TS_MULCOL         0x0004  /* multiple collision detected */
#define SMSC_TS_SNGLCOL        0x0002  /* single collision detected */
#define SMSC_TS_TX_SUC         0x0001  /* last Transmit was successful */

    /* ## indicates fatal error */
/* Receive control register */

#define SMSC_RCR_EPH_RST       0x8000  /* software activated reset */
#define SMSC_RCR_FILT_CAR      0x4000  /* filter carrier sense 12 bits */
#define SMSC_RCR_STRIP_CRC     0x0200  /* when set, strips CRC */
#define SMSC_RCR_RXEN          0x0100  /* ENABLE receiver */
#define SMSC_RCR_ALMUL         0x0004  /* receive all multicast packets */
#define SMSC_RCR_PRMS          0x0002  /* enable promiscuous mode */
#define SMSC_RCR_RX_ABORT      0x0001  /* set if reciever overrun */
#define SMSC_RCR_RESET         0x0     /* set it to a base state */

/* Memory Information Register */

#define SMSC_MIR_FREE          MIR     /* read at any time for free mem */
#define SMSC_MIR_SIZE          MIR+1   /* determine amount of onchip mem */


#define SMSC_RPCR_SPEED		0x2000
#define SMSC_RPCR_DPLX		0x1000
#define SMSC_RPCR_ANEG		0x0800
#define SMSC_RPCR_LEDA2		0x0080
#define SMSC_RPCR_LEDA1		0x0040
#define SMSC_RPCR_LEDA0		0x0020
#define SMSC_RPCR_LEDB2		0x0010
#define SMSC_RPCR_LEDB1		0x0008
#define SMSC_RPCR_LEDB0		0x0004

/* BANK 1 I/O Space mapping */

#define SMSC_CONFIG            0x0     /* configuration register */
#define SMSC_BASE              0x2     /* base address register */
#define SMSC_ADDR_0            0x4     /* individual address register 0 */
#define SMSC_ADDR_1            0x6     /* individual address register 1 */
#define SMSC_ADDR_2            0x8     /* individual address register 2 */
#define SMSC_GENERAL           0xA     /* general purpose register */
#define SMSC_CONTROL           0xC     /* control register */

/* Configuration register */

#define SMSC_CFG_NO_WAIT_ST     0x1000  /* when set */
#define SMSC_CFG_EXT_PHY	0x0200	/* use external phy */
#define SMSC_CFG_FULL_STEP      0x0400  /* AUI, use full step signaling */
#define SMSC_CFG_SET_SQLCH      0x0200  /* when set squelch level is 240mV */
#define SMSC_CFG_AUI_SELECT     0x0100  /* when set use AUI */
#define SMSC_CFG_MII_SELECT     0x8000  /* when set use MII */
#define SMSC_INT_INTR_ZERO      0xf9    /* mask to clear interrrupt setting */
#define SMSC_INT_SEL_INTR0      0x0
#define SMSC_INT_SEL_INTR1      0x2
#define SMSC_INT_SEL_INTR2      0x4
#define SMSC_INT_SEL_INTR3      0x6

/* Control Register */

#define SMSC_CTL_RCV_BAD       0x4000  /* when set bad CRC packets received */
#define SMSC_CTL_PWEDN         0x2000  /* high puts chip in powerdown mode */
#define SMSC_CTL_AUTO_RELEASE  0x0800  /* transmit memory auto released */
#define SMSC_CTL_LE_ENABLE     0x0080  /* Link error enable */
#define SMSC_CTL_CR_ENABLE     0x0040  /* counter rollover enable */
#define SMSC_CTL_TE_ENABLE     0x0020  /* transmit error enable */
#define SMSC_CTL_EPROM_SELECT  0x0004  /* EEprom access bit */


/* BANK 2 I/O Space mapping */

#define SMSC_MMU               0x0     /* MMU command register */
#define SMSC_PNR               0x2     /* packet number register */
#define SMSC_ARR               0x2     /* allocation result register (WORD ADDRESS!) */
#define SMSC_FIFO              0x4     /* FIFO ports register */
#define SMSC_FIFO_TX           0x4     /* FIFO ports register */
#define SMSC_FIFO_RX           0x5     /* FIFO ports register */
#define SMSC_PTR               0x6     /* pointer register */
#define SMSC_DATA_1            0x8     /* data register 1 */
#define SMSC_DATA_2            0xA     /* data register 2 */
#define SMSC_INT_STAT          0xC     /* int status & acknowledge register */
#define SMSC_INT_ACK           0xC     /* int status & acknowledge register */
#define SMSC_INT_MASK          0xC     /* int mask register (WORD ADDRESS!) */

/* MMU Command Register */

#define SMSC_MMU_NOP           0x0000
#define SMSC_MMU_BUSY          0x0001  /* set if MMU busy */
#define SMSC_MMU_ALLOC         0x0020  /* get memory from chip memory/256 */
#define SMSC_MMU_RESET         0x0040
#define SMSC_MMU_RX_REMOVE     0x0060  /* remove current RX frame from FIFO */
#define SMSC_MMU_RX_RELEASE    0x0080  /* also release memory associated */
#define SMSC_MMU_TX_RELEASE    0x00A0  /* free packet memory in PNR register */
#define SMSC_MMU_ENQUEUE       0x00C0  /* Enqueue the packet for transmit */
#define SMSC_MMU_TX_RESET      0x00E0  /* reset TX FIFO's */


/* Allocation Result Register */

#define SMSC_ARR_FAILED         0x80

/* FIFO Ports Register */

#define SMSC_FP_RXEMPTY        0x8000  /* no rx packets queued in RX FIFO */
#define SMSC_FP_RXPNR          0x3F00  /* top of RX FIFO packet number */
#define SMSC_FP_TXEMPTY        0x0080  /* no tx packets queued in TX FIFO  */
#define SMSC_FP_TXPNR          0x003F  /* top of TX FIFO packet number */

/* Pointer Register - address to be accessed in chip memory*/

#define SMSC_PTR_RCV           0x8000  /* when set, refers to receive area */
#define SMSC_PTR_AUTOINC       0x4000  /* auto incs. PTR correct amount */
#define SMSC_PTR_READ          0x2000  /* type of access to follow */

/* Interrupt Detail */

#define SMSC_INT_MDINT         0x80    /* PHY interrupt */
#define SMSC_INT_ERCV          0x40    /* for other chip sets */
#define SMSC_INT_EPH           0x20    /* Set when EPH handler fires */
#define SMSC_INT_RX_OVRN       0x10    /* set when receiver overruns */
#define SMSC_INT_ALLOC         0x08    /* set when MMU allocation is done */
#define SMSC_INT_TX_EMPTY      0x04    /* set id TX_FIFO empty */
#define SMSC_INT_TX            0x02    /* set when at least one packet sent */
#define SMSC_INT_RCV           0x01    /* RX Done interrupt */

/* Interrupts for chip to generate */

#define SMSC_INTERRUPT_MASK    (SMSC_INT_EPH | SMSC_INT_RX_OVRN | \
				SMSC_INT_RCV | SMSC_INT_TX | SMSC_INT_MDINT)


/* BANK 3 I/O Space mapping */


#define SMSC_MULTICAST0        0x0     /* multicast table - WORD 0 */
#define SMSC_MULTICAST1        0x2     /* multicast table - WORD 1 */
#define SMSC_MULTICAST2        0x4     /* multicast table - WORD 2 */
#define SMSC_MULTICAST3        0x6     /* multicast table - WORD 3 */
#define SMSC_MGMT              0x8
#define SMSC_REVISION          0xA     /* chip set and revision encoded here */
#define SMSC_ERCV              0xC     /* early receive register */

/* Receive frame status word - located at beginning of each received frame*/

#define SMSC_RS_ALGNERR        0x8000  /* frame had alignment error */
#define SMSC_RS_BRODCAST       0x4000  /* receive frame was broadcast */
#define SMSC_RS_BADCRC         0x2000  /* CRC error */
#define SMSC_RS_ODDFRM         0x1000  /* receive frame had odd byte */
#define SMSC_RS_TOOLONG        0x0800  /* longer then 1518 bytes */
#define SMSC_RS_TOOSHORT       0x0400  /* shorter then 64 bytes */
#define SMSC_RS_MULTICAST      0x0001  /* receive frame was multicast */
#define SMSC_RS_ERROR_MASK   (SMSC_RS_ALGNERR | SMSC_RS_BADCRC | SMSC_RS_TOOLONG | SMSC_RS_TOOSHORT)

/* MII Register Definition */

#define SMSC_MII_MDO           0x0001  /* MII Management output */
#define SMSC_MII_MDI           0x0002  /* MII Management input */
#define SMSC_MII_MCLK          0x0004  /* MII Management clock */
#define SMSC_MII_MDOE          0x0008  /* MII Management output enable */
#define SMSC_MII_MDALL         0x000F  /* MII All */
#define SMSC_MII_MSKCRS        0x4000  /* Diasbles CRS100 {SWFDUP=0} */
#define SMSC_MII_MFLTST        0x8000  /* Facilitates Packet forwarding */

/* Read-Write PHY */

#define SMSC_PHY_OPWrite       0x01    /* Write PHY */
#define SMSC_PHY_OPRead        0x02    /* Read PHY */

/* PHY Register Numbers 
 *
 * PHY registers 0 and 1 are mandatory, 2-7 are optional, 8-15 are reserved and
 * 16-31 are vendor proprietary.
 */

#define PHY_CONTROL            0x0     /* Control Register */
#define PHY_STATUS             0x1     /* Status Register */
#define PHY_ID1                0X2     /* ID1 Register */
#define PHY_ID2                0X3     /* ID2 Register */
#define PHY_AUTO_AD            0x4     /* Auto-Negotiation Advertisement Register */
#define PHY_AUTO_LINK          0x5     /* Auto-Negotiation Link Partner Register */
#define PHY_AUTO_EXP           0x6     /* Auto-Negotiation Expansion */
#define PHY_AUTO_NXT_PAGE_XMT  0x7     /* Auto-Neg. Next Page Xmit Register */
#define PHY_RES8               0x8     /* Reserved Register */
#define PHY_RES9               0x9     /* Reserved Register */
#define PHY_RES10              0xA     /* Reserved Register */
#define PHY_RES11              0xB     /* Reserved Register */
#define PHY_RES12              0xC     /* Reserved Register */
#define PHY_RES13              0xD     /* Reserved Register */
#define PHY_RES14              0xE     /* Reserved Register */
#define PHY_RES15              0xF     /* Reserved Register */
#define PHY_RES16              0x10    /* Reserved Register */
#define PHY_RES17              0x11    /* Reserved Register */
#define PHY_DCR                0x12    /* Disconnect Counter Register */
#define PHY_FCSCR              0x13    /* False Carrier Sense Counter Reg */
#define PHY_RES20              0x14    /* Reserved Register (DO NOT R/W) */
#define PHY_RECR               0x15    /* Receive Error Counter Reg */
#define PHY_SRR                0x16    /* Silicon Revision Register */
#define PHY_PCR                0x17    /* PCS Sub-Layer Configuration Reg */
#define PHY_LBREMR             0x18    /* Loopback, Bypass & Receiver Error Mask Reg */
#define PHY_PAR                0x19    /* PHY Address Register */
#define PHY_VENDOR26           0x1A    /* Vendor Proprietary Register */
#define PHY_10BTSR             0x1B    /* 10BaseT Status Register */
#define PHY_10BTCR             0x1C    /* 10BaseT Configuration Register */
#define PHY_VENDOR29           0x1D    /* Vendor Proprietary Register  */
#define PHY_VENDOR30           0x1E    /* Vendor Proprietary Register  */
#define PHY_VENDOR31           0x1F    /* Vendor Proprietary Register  */

#define PHY_83C180_SPEC        24
#define PHY_83C180_SPEC_FDPLX  0x0020


#define PHY_ANEG_STATUS        25
#define PHY_ANEG_STATUS_SPEED     0x0020
#define PHY_ANEG_STATUS_DUPLEX    0x0040
#define PHY_ANEG_STATUS_COMPLETE  0x0080

/* PHY Control Register Definition */

#define PHY_CTRL_COLTST        0x0080  /* Col Test Bit */
#define PHY_CTRL_DUPLEX        0x0100  /* Duplex Mode Bit */
#define PHY_CTRL_NWAYREST      0x0200  /* NWAY REST Bit */
#define PHY_CTRL_ISOLATE       0x0400  /* Isolation Bit */
#define PHY_CTRL_PWRDOWN       0x0800  /* PHY Power Down Bit */
#define PHY_CTRL_NWAYEN        0x1000  /* Auto-Negotiation Enable Bit */
#define PHY_CTRL_SPEED         0x2000  /* 10 or 100 Mbps Speed Bit (manual prog.) */
#define PHY_CTRL_LOOPBACK      0x4000  /* Loopback Bit */
#define PHY_CTRL_RESET         0x8000  /* Reset Bit */

/* PHY Status Register Definition */

#define PHY_STAT_EXTEND_CAP    0x0001  /* Extended Register Capability Bit */
#define PHY_STAT_JABBER_DET    0x0002  /* Jabber Condition Detected Bit */
#define PHY_STAT_LNK_STAT      0x0004  /* Valid Link Bit */
#define PHY_STAT_NWAY_AVAIL    0x0008  /* Auto-Negotiation Capability Bit */
#define PHY_STAT_REM_FAULT     0x0010  /* Remote Fault Bit */
#define PHY_STAT_NWAY_COMP     0x0020  /* Auto-Negotiation Complete Bit */
#define PHY_STAT_MPRE_SUP      0x0040  /* MF Preample Suppression Bit */
#define PHY_STAT_10BPSH        0x0800  /* 10 Mbps Half Duplex Capability Bit */
#define PHY_STAT_10BPSD        0x1000  /* 10 Mbps Full Duplex Capability Bit */
#define PHY_STAT_100BASEXH     0x2000  /* 100 BaseXH Half Duplex Capability Bit */
#define PHY_STAT_100BASEXD     0x4000  /* 100 BaseXD Full Duplex Capability Bit */
#define PHY_STAT_100BASET4     0x5000  /* 100 BaseT4 Capability Bit */

/* PHY Auto-Negotiation Advertisement Register Definition */

#define PHY_AUTO_AD_S0          0x0001  /* Selector Field 0 Bit */
#define PHY_AUTO_AD_S1          0x0002  /* Selector Field 1 Bit */
#define PHY_AUTO_AD_S2          0x0004  /* Selector Field 2 Bit */
#define PHY_AUTO_AD_S3          0x0008  /* Selector Field 3 Bit */
#define PHY_AUTO_AD_S4          0x0010  /* Selector Field 4 Bit */
#define PHY_AUTO_AD_TA0         0x0020  /* 10 BaseT Capability Bit */
#define PHY_AUTO_AD_TA1         0x0040  /* 10 BaseT Full Duplex Capability Bit */
#define PHY_AUTO_AD_TA2         0x0080  /* 100 BaseTX Capability Bit */
#define PHY_AUTO_AD_TA3         0x0100  /* 100 BaseTX Full Duplex Capability Bit */
#define PHY_AUTO_AD_TA4         0x0200  /* 100 BaseT4 Capability Bit */
#define PHY_AUTO_AD_TA5         0x0400  /* Technology Ability 5 Bit */
#define PHY_AUTO_AD_TA6         0x0800  /* Technology Ability 6 Bit */
#define PHY_AUTO_AD_TA7         0x1000  /* Technology Ability 7 Bit */
#define PHY_AUTO_AD_REMFAULT    0x2000  /* Remote Fault Bit */
#define PHY_AUTO_AD_ACK         0x4000  /* Acknowledge Bit */
#define PHY_AUTO_AD_NXTPAGE     0x8000  /* Exchange Next Page Information Bit */

/* PHY Auto-Negotiation Link Partner Ability Register Definition */

#define PHY_AUTO_LINK_S0        0x0001  /* Selector Field 0 Bit */
#define PHY_AUTO_LINK_S1        0x0002  /* Selector Field 1 Bit */
#define PHY_AUTO_LINK_S2        0x0004  /* Selector Field 2 Bit */
#define PHY_AUTO_LINK_S3        0x0008  /* Selector Field 3 Bit */
#define PHY_AUTO_LINK_S4        0x0010  /* Selector Field 4 Bit */
#define PHY_AUTO_LINK_10BT      0x0020  /* 10 BaseT Capability Bit */
#define PHY_AUTO_LINK_10BTF     0x0040  /* 10 BaseT Full Duplex Capability Bit */
#define PHY_AUTO_LINK_100BTX    0x0080  /* 100 BaseTX Capability Bit */
#define PHY_AUTO_LINK_100BTXF   0x0100  /* 100 BaseTX Full Duplex Capability Bit */
#define PHY_AUTO_LINK_100BT4    0x0200  /* 100 BaseT4 Capability Bit */
#define PHY_AUTO_LINK_RES10     0x0400  /* Reserved Bit */
#define PHY_AUTO_LINK_RES11     0x0800  /* Reserved Bit */
#define PHY_AUTO_LINK_RES12     0x1000  /* Reserved Bit */
#define PHY_AUTO_LINK_REMFAULT  0x2000  /* Remote Fault Bit */
#define PHY_AUTO_LINK_ACK       0x4000  /* Received Partner Link Code Word Bit */
#define PHY_AUTO_LINK_NXTPAGE   0x8000  /* Exchange Next Page Information Bit */

/* PHY Auto-Negotiation Expansion Register Definition */

#define PHY_AUTO_EXP_RES1       0x0001  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES2       0x0002  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES3       0x0004  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES4       0x0008  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES5       0x0010  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES6       0x0020  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES7       0x0040  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES8       0x0080  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES9       0x0100  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES10      0x0200  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_RES11      0x0400  /* ANER Reserved Bit */
#define PHY_AUTO_EXP_MLF        0x0800  /* Mulitple Link Fault Bit */
#define PHY_AUTO_EXP_LP_NP_ABLE 0x1000  /* Link Partner Next Page Able Bit */
#define PHY_AUTO_EXP_NP_ABLE    0x2000  /* Next Page Able Bit */
#define PHY_AUTO_EXP_PAGE_RX    0x4000  /* Link Code Word Page Received Bit */
#define PHY_AUTO_EXP_LP_AN_ABLE 0x8000  /* Link Partner Auto-Negotiation Able Bit */

/* PCS Configuration Register(PCR) Definition */

#define PHY_PCR_NRZI_EN         0x0001  /* NRZI Enable */
#define PHY_PCR_DESCR_TO_SEL    0x0002  /* Descramble Timeout Select */
#define PHY_PCR_DESCR_TO_DIS    0x0004  /* Descramble Timeout Disable */
#define PHY_PCR_REPEATER        0x0008  /* Repeater/Node Mode */
#define PHY_PCR_ENCSEL          0x0010  /* Encoder Mode Select */
#define PHY_PCR_RES6            0x0020  /* PCR Reserved Bit */
#define PHY_PCR_RES7            0x0040  /* PCR Reserved Bit */
#define PHY_PCR_RES8            0x0080  /* PCR Reserved Bit */
#define PHY_PCR_CLK25MDIS       0x0100  /* CLK25M Disable */
#define PHY_PCR_F_LINK_100      0x0200  /* Force Good Link in 100Mbps */
#define PHY_PCR_CIM_DIS         0x0400  /* Carrier Integrity Monitor Disable */
#define PHY_PCR_TX_OFF          0x0800  /* Force Transmit Off */
#define PHY_PCR_RES13           0x1000  /* PCR Reserved Bit */
#define PHY_PCR_LED1_MODE       0x2000  /* LED1_Mode */
#define PHY_PCR_LED4_MODE       0x4000  /* LED4_Mode */
#define PHY_PCR_RES16           0x8000  /* PCR reserved Bit */

/* Loopback, BYPASS and Receiver Error Mask Register(LBREMR) Definition */

#define PHY_LBREMR_RES0         0x0001  /* LBREMR Reserved Bit */
#define PHY_LBREMR_PKT_ERR      0x0002  /* Packet Errors */
#define PHY_LBREMR_LINK_ERR     0x0004  /* Link Errors */
#define PHY_LBREMR_PE_ERR       0x0008  /* Premature End Errors */
#define PHY_LBREMR_CODE_ERR     0x0010  /* Code Errors */
#define PHY_LBREMR_LBK_XMT_DS   0x0020  /* 100Mb/s Xmit Disable in Loopback */
#define PHY_LBREMR_ALT_CRS      0x0040  /* Alternate CRS Operation */
#define PHY_LBREMR_RES7         0x0080  /* LBREMR Reserved Bit */
#define PHY_LBREMR_LB0          0x0100  /* Loopback Control Bit 0 */
#define PHY_LBREMR_LB1          0x0200  /* Loopback Control Bit 1 */
#define PHY_LBREMR_RES10        0x0400  /* LBREMR Reserved Bit */
#define PHY_LBREMR_10BT_LPBK    0x0800  /* 10BaseT Encoder/Decoder Loopback*/
#define PHY_LBREMR_BP_ALIGN     0x1000  /* BYPASS Symbol Alignment Function */
#define PHY_LBREMR_BP_SCR       0x2000  /* BYPASS Scrambler/Descrambler Function */
#define PHY_LBREMR_BP_4B5B      0x4000  /* BYPASS 4B5B Encoding/Decoding */
#define PHY_LBREMR_BAD_SSD_EN   0x8000  /* Bad SSD Enable */

/* PHY Address Register(PAR) Definition */

#define PHY_PAR_PHYADDR0        0x0001  /**/
#define PHY_PAR_PHYADDR1        0x0002  /*  */
#define PHY_PAR_PHYADDR2        0x0004  /* */
#define PHY_PAR_PHYADDR3        0x0008  /*  */
#define PHY_PAR_PHYADDR4        0x0010  /*  */
#define PHY_PAR_CIM_STATUS      0x0020  /*  */
#define PHY_PAR_SPEED_10        0x0040  /* */
#define PHY_PAR_DUPLEX_STAT     0x0080  /*  */
#define PHY_PAR_FEFI_EN         0x0100  /* */
#define PHY_PAR_RES9            0x0200  /* */
#define PHY_PAR_AN_EN_STAT      0x0400  /* */
#define PHY_PAR_DIS_CRS_JAB     0x0800  /*  */
#define PHY_PAR_RES12           0x1000  /* */
#define PHY_PAR_RES13           0x2000  /* */
#define PHY_PAR_RES14           0x4000  /*  */
#define PHY_PAR_RES15           0x8000  /*  */

#define PHY_SMSC83C180_ID1      0x0282
#define PHY_SMSC83C180_ID2      0x1C53

/* EEprom operation */

#define SMSC_CTL_EPROM_STORE	0x0001	/* EEprom storage operation */
#define SMSC_CTL_EPROM_RELOAD	0x0002	/* EEprom reload operation */

/* SMSC 91C111 internal PHY registers */

#define SMSC_PHY_ISR			18
#define SMSC_PHY_IMR			19

#define SMSC_PHYISR_INT			0x8000	/* interrupt asserted */
#define SMSC_PHYISR_LINKFAIL		0x4000	/* link failure */
#define SMSC_PHYISR_LOSSSYNC		0x2000	/* loss of sync */
#define SMSC_PHYISR_CRWD		0x1000	/* codeword error */
#define SMSC_PHYISR_SSD			0x0800	/* start of stream error */
#define SMSC_PHYISR_ESD			0x0400	/* end of stream error */
#define SMSC_PHYISR_RPOL		0x0200	/* polarity reversal detected */
#define SMSC_PHYISR_JABBER		0x0100	/* jabber detexted */
#define SMSC_PHYISR_SPEED		0x0080	/* 1 == 100Mbps, 0 == 10Mbps */
#define SMSC_PHYISR_DPX			0x0040	/* 1 == full, 0 == half */
#define SMSC_PHYISR_RSVD		0x003F

/* typedefs */

/* The definition of the driver control structure */

typedef struct smsc_drv_ctrl
    {
    END_OBJ		endObject;              /* The class we inherit from. */
    VXB_DEVICE_ID	smscDev;
    void *		smscMuxDevCookie;
    void *		smscBar;
    void *		smscHandle;

    JOB_QUEUE_ID	smscJobQueue;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	smscMediaList;
    END_ERR		smscLastError;
    UINT32		smscCurMedia;
    UINT32		smscCurStatus;
    VXB_DEVICE_ID	smscMiiBus;
    /* End MII/ifmedia required fields */

    BOOL		smscPhySkip;
    UINT16		smscStsReg;

    int			polling;                /* Polling flag */
    UCHAR		enetAddr[6];            /* ethernet address */

    QJOB		smscIntJob;
    atomic_t		smscIntPending;

    QJOB		smscRxJob;
    BOOL		smscRxPending;

    QJOB		smscTxJob;
    BOOL		smscTxPending;

    BOOL		smscTxStall;
    BOOL		smscTxActive;
    int			smscTxPktNo;
    M_BLK_ID		smscTxBuf;
    SEM_ID		smscDevSem;
    UINT8		smscMoreRx;
    BOOL		smscLinkIntr;
    spinlockIsr_t	smscLock;
    } SMSC_DRV_CTRL;

#define bswap16(x)      ((LSB(x) << 8) | MSB(x))

#if _BYTE_ORDER == _LITTLE_ENDIAN
#define htobe16(x)      bswap16((x))
#define htole16(x)      ((UINT16)(x))

#define be16toh(x)      bswap16((x))
#define le16toh(x)      ((UINT16)(x))
#else /* _BYTE_ORDER != _LITTLE_ENDIAN */
#define htobe16(x)      ((UINT16)(x))
#define htole16(x)      bswap16((x))

#define be16toh(x)      ((UINT16)(x))
#define le16toh(x)      bswap16((x))
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

IMPORT void	sysDelay(void);

#define SMSC_BAR(p)   ((SMSC_DRV_CTRL *)(p)->pDrvCtrl)->smscBar
#define SMSC_HANDLE(p)   ((SMSC_DRV_CTRL *)(p)->pDrvCtrl)->smscHandle

#define SMSC_ADJ(x)		(x)->m_data += 2


#define CSR_READ_2(pDev, y)	            \
	vxbRead16 (SMSC_HANDLE(pDev), (UINT16 *)((char *)SMSC_BAR(pDev) + y))
#define CSR_WRITE_2(pDev, y, z)	        \
	vxbWrite16 (SMSC_HANDLE(pDev),(UINT16 *)((char *)SMSC_BAR(pDev) + y), z)
	
#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_BANKSEL(x, y)	\
	    CSR_WRITE_2(x, SMSC_BANK_SELECT, y);

#define SMSC_SPEED_10      10000000   /* 10 Mbit/sec interface */
#define SMSC_SPEED_100     100000000  /* 100 Mbit/sec interface */


#ifdef __cplusplus
}
#endif

#endif  /* vxbSmscLan91c111Endh */ 
