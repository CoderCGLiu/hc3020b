/* vxbMyriLanaiEnd.h - header file for Myricom Lanai 10Gbe VxBus END driver */

/*
 * Copyright (c) 2009, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,26feb13,wyt  fix coverity error (WIND00401412)
01a,17mar09,wap  written
*/

#ifndef __INCvxbMyriLanaiEndh
#define __INCvxbMyriLanaiEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void myeRegister (void);

#ifndef BSP_VERSION

#define MYRICOM_VENDORID	0x14C1

#define MYRICOM_DEVICEID_LANAI_Z8E	0x0008
#define MYRICOM_DEVICEID_LANAI_Z8E_9	0x0009

#define MYE_SRAM		0x000000 /* On-board 2MB SRAM */
#define MYE_GEN_HDR_PTR		0x00003C /* Location of gen_hdr structure */
#define MYE_FW_ADDR		0x100000 /* New firmware base address */
#define MYE_STRINGS		0x1DFE00 /* EEPROM strings */
#define MYE_ETH_SEND4		0x200000 /* 4 descriptor write location */
#define MYE_ETH_SEND1		0x240000 /* 1 descriptor write location */
#define MYE_ETH_SEND2		0x280000 /* 2 descriptor write location */
#define MYE_ETH_SEND3		0x2C0000 /* 3 descriptor write location */
#define MYE_ETH_RX_SMALL	0x300000 /* Small RX write location */
#define MYE_ETH_RX_BIG		0x340000 /* Big RX write location */
#define MYE_ETH_IRQ_DEASSERT	0xF40000 /* Deassert interrupt */
#define MYE_ETH_CMD		0xF80000 /* Command write location */
#define MYE_BOOT_RQST		0xFC0000 /* Boot request write location */
#define MYE_BOOT_DUMP		0xFC0040 /* Dump internal RAM to host */
#define MYE_BOOT_END		0xFC0080 /* Get addr of end of firmware */
#define MYE_BOOT_JUMP		0xFC00C0 /* Call firmware function */
#define MYE_BOOT_EEPROM_CTL	0xFC0100 /* NIC Accesses JTAG */
#define MYE_BOOT_EEPROM_DATA	0xFC0140 /* NIC Accesses JTAG */
#define MYE_BOOT_JTAG_RELEASE	0xFC0180 /* No action */
#define MYE_BOOT_DUMMY_RDMA	0xFC01C0 /* Perform DMA read */

/* Command codes */

#define MYE_CMD_NONE		0	/* No action */
#define MYE_CMD_RESET		1	/* Reset NIC */
#define MYE_CMD_GET_MCPVER	2	/* Get version number */
#define MYE_CMD_SET_INTRQ_ADDR	3	/* Set address of interrupt queue */
#define MYE_CMD_SET_BIGRX_LEN	4	/* Set big RX buffer size */
#define MYE_CMD_SET_SMLRX_LEN	5	/* Set small RX buffer size */
#define MYE_CMD_GET_TX_ADDR	6	/* Get TX send ring address */
#define MYE_CMD_GET_SMLRX_ADDR	7	/* Get small RX buffer ring address */
#define MYE_CMD_GET_BIGRX_ADDR	8	/* Get small RX buffer ring address */
#define MYE_CMD_GET_INTACK_ADDR	9	/* Get address of interrupt ack */
#define MYE_CMD_GET_INTDS_ADDR	10	/* Get address of interrupt deassert */
#define MYE_CMD_GET_TX_RING_LEN	11	/* Get TX ring size */
#define MYE_CMD_GET_RX_RING_LEN	12	/* Get RX ring sizes */
#define MYE_CMD_SET_INTRQ_LEN	13	/* Set interrupt queue size */
#define MYE_CMD_ETH_UP		14	/* Bring up ethernet link */
#define MYE_CMD_ETH_DOWN	15	/* Bring down ethernet link */
#define MYE_CMD_SET_MTU		16	/* Set NIC MTU */
#define MYE_CMD_GET_ICOAL_ADDR	17	/* Get intr coalesce delay */
#define MYE_CMD_SET_STATS_IVAL	18	/* Set stats update interval */
#define MYE_CMD_PROMISC_ON	20	/* Enable promisc mode */
#define MYE_CMD_PROMISC_OFF	21	/* Disable promisc mode */
#define MYE_CMD_SET_MAC_ADDR	22	/* Set station address */
#define MYE_CMD_FC_ENABLE	23	/* Enable flow control */
#define MYE_CMD_FC_DISABLE	24	/* Disable flow control */
#define MYE_CMD_DMA_TEST	25	/* Perform DMA test */
#define MYE_CMD_ALLMULTI_ON	26	/* Set all-multicast mode */
#define MYE_CMD_ALLMULTI_OFF	27	/* Disable all-multicast mode */
#define MYE_CMD_MCAST_JOIN	28	/* Join a multicast group */
#define MYE_CMD_MCAST_LEAVE	29	/* Leave a multicast group */
#define MYE_CMD_MCAST_LEAVEALL	30	/* Leave all mcast groups */
#define MYE_CMD_SET_STATS_ADDR2	31	/* Set address of intr data */
#define MYE_CMD_UNALIGNED_TEST	32	/* Perform unaligned DMA test */
#define MYE_CMD_UNALIGNED_STS	33	/* NIC reports unaligned PCIe pkts */
#define MYE_CMD_USE_N_BIGBUFS	34	/* Always use N big buffers */

/* Error codes */

#define MYE_ERR_OK		0	/* No error */
#define MYE_ERR_BADCMD		1	/* Unknown cmd */
#define MYE_ERR_BADRANGE	2	/* Out of range */
#define MYE_ERR_BUSY		3	/* NIC is busy */
#define MYE_ERR_EMPTY		4	/* unused */
#define MYE_ERR_CLOSED		5	/* unused */
#define MYE_ERR_HASH_ERR	6	/* Mcast hash table erro */
#define MYE_ERR_BAD_PORT	7	/* unused */
#define MYE_ERR_RESOURCES	8	/* unused */
#define MYE_ERR_MULTICAST	9	/* unused */
#define MYE_ERR_UNALIGNED	10	/* Unaligned PCIe packets detected */

/* DMA test arguments */

#define MYE_DMA_LEN 4096
#define MYE_DMA_READLEN(x)	(((x) & 0xFFFF) << 16)
#define MYE_DMA_WRITELEN(x)	((x) & 0xFFFF)  

/*
 * DMA test results.
 * The upper 16 bits of the command result is the number of transfers
 * completed during the test. The lower 16 bits is the number of .5
 * microsecond ticks that elapsed during the test.
 */

#define MYE_DMA_NUMXFRS(x)	(((x) >> 16) & 0xFFFF)
#define MYE_DMA_NUMTICKS(x)	((x) & 0xFFFF)
#define MYE_DMA_READBW(x, y)	\
    ((MYE_DMA_NUMXFRS(x) * y * 2) / MYE_DMA_NUMTICKS(x))
#define MYE_DMA_WRITEBW(x, y)	\
    ((MYE_DMA_NUMXFRS(x) * y * 2) / MYE_DMA_NUMTICKS(x))
#define MYE_DMA_READWRITEBW(x, y)	\
    ((MYE_DMA_NUMXFRS(x) * y * 2 * 2) / MYE_DMA_NUMTICKS(x))

/* ETH_CMD structure format */

typedef struct mye_cmd
    {
    volatile UINT32	mye_cmd;
    volatile UINT32	mye_data0;
    volatile UINT32	mye_data1;
    volatile UINT32	mye_data2;
    volatile UINT32	mye_addrhi;
    volatile UINT32	mye_addrlo;
    volatile UINT32	mye_rsvd[10];
    } MYE_CMD;

/* ETH_CMD response structure format */

typedef struct mye_resp
    {
    volatile UINT32	mye_data;
    volatile UINT32	mye_err;
    } MYE_RESP;

typedef struct mye_irq_data
    {
    volatile UINT32	mye_rsvd0[1];
    volatile UINT32	mye_dropped_pause;
    volatile UINT32	mye_dropped_ucast_filtered;
    volatile UINT32	mye_dropped_bad_crc32;
    volatile UINT32	mye_dropped_bad_phy;
    volatile UINT32	mye_dropped_mcast_filtered;
    volatile UINT32	mye_tx_done;
    volatile UINT32	mye_link_up;
    volatile UINT32	mye_dropped_link_oflow;
    volatile UINT32	mye_dropped_linkerr_or_filtered;
    volatile UINT32	mye_dropped_runt;
    volatile UINT32	mye_dropped_overrun;
    volatile UINT32	mye_dropped_no_small_bufs;
    volatile UINT32	mye_dropped_no_big_bufs;
    volatile UINT32	mye_rdma_avail_tags;
    volatile UINT8	mye_tx_stopped;
    volatile UINT8	mye_link_down;
    volatile UINT8	mye_stats_updated;
    volatile UINT8	mye_valid;
    } MYE_IRQ_DATA;

#define MYE_LINK_DOWN		0x0
#define MYE_LINK_UP		0x1
#define MYE_LINK_MYRINET	0x2
#define MYE_LINK_UNKNOWN	0x3

typedef struct mye_gen_header
    {
    volatile UINT32	mye_headerlen;
    volatile UINT32	mye_type;
    volatile char	mye_version[128];
    volatile UINT32	mye_globals;
    volatile UINT32	mye_sram_size;
    volatile UINT32	mye_string_specs;
    volatile UINT32	mye_string_len;
    } MYE_GEN_HEADER;

/* RX descriptor format */

typedef struct mye_bdesc
    {
    volatile UINT32	mye_addrhi;
    volatile UINT32	mye_addrlo;
    } MYE_BDESC;

typedef struct mye_rdesc
    {
    volatile UINT16	mye_csum;
    volatile UINT16	mye_len;
    } MYE_RDESC;

/* TX descriptor format */

typedef struct mye_tflags
    {
    volatile UINT8	mye_rsvd;
    volatile UINT8	mye_rdmacnt;
    volatile UINT8	mye_csum_offset;
    volatile UINT8	mye_flags;
    } MYE_TFLAGS;

typedef union mye_tfu
    {
    MYE_TFLAGS		mye_tflags;
    volatile UINT32	mye_all;
    } MYE_TFU;

typedef struct mye_tdesc
    {
    volatile UINT32	mye_addrhi;
    volatile UINT32	mye_addrlo;
    volatile UINT16	mye_phdr_offset;
    volatile UINT16	mye_buflen;
    MYE_TFU		mye_tfu;
    } MYE_TDESC;

/* TX descriptor flags */

#define MYE_TDESC_FLAG_SMALL		0x01 /* Frame less than 1520 */
#define MYE_TDESC_FLAG_TSO_HDR		0x01 /* TSO frame header */
#define MYE_TDESC_FLAG_FIRST		0x02 /* First buffer in frame */
#define MYE_TDESC_FLAG_ODDALIGN		0x04 /* Length is odd */
#define MYE_TDESC_FLAG_CSUM		0x08 /* Calculate TCP/UDP csum */
#define MYE_TDESC_FLAG_TSO_LAST		0x08 /* Last buffer in TSO send */
#define MYE_TDESC_FLAG_NO_TSO		0x10 /* Not a TSO packet */
#define MYE_TDESC_FLAG_TSO_CHOP		0x10 /* Multifragment TSO */
#define MYE_TDESC_FLAG_TSO_PLD		0x20 /* TSO payload */


#define MYE_CMD_SETUP(x, y)			\
    do						\
        {					\
	(x)->myeCmd->mye_cmd = htobe32(y);	\
	(x)->myeCmd->mye_data0 =		\
	(x)->myeCmd->mye_data1 =		\
	(x)->myeCmd->mye_data2 = 0;		\
        }					\
    while ((0))

#define MYE_MTU		1500
#define MYE_JUMBO_MTU	9000
#define MYE_CLSIZE	1536
#define MYE_NAME	"mye"
#define MYE_TIMEOUT	10000

#define MYE_SMALL_CUTOFF	1520

/* Maximum values. */

#define MYE_RX_DESC_CNT		512
#define MYE_TX_DESC_CNT		1024

#define MYE_TX_CLEAN_THRESH	32
#define MYE_MAXFRAG		16
#define MYE_MAX_RX		64

#define MYE_COAL_USECS		30

#define MYE_INC_DESC(x, y)     (x) = ((x + 1) & (y))

#define MYE_ADDR_LO(y)  ((UINT32)((UINT64)(y) & 0xFFFFFFFF))
#define MYE_ADDR_HI(y)  ((UINT32)(((UINT64)(y) >> 32) & 0xFFFFFFFF))

#define MYE_ADJ(m)      (m)->m_data += 2

#define MYE_ALIGN(x)    (char *)(((size_t)(x) + 4095UL) & ~4095UL)

#define MYE_FORCE(x)      \
        (x)->m_data = MYE_ALIGN((x)->m_data)

typedef struct mye_rx
    {
    MYE_BDESC *		myeRxDescMem;
    UINT32		myeRxCnt;
    UINT32		myeRxMask;
    UINT32		myeRxIdx;
    NET_POOL_ID		myeNetPool;
    M_BLK_ID		myeRxMblk[MYE_RX_DESC_CNT];
    VXB_DMA_TAG_ID	myeMblkTag;
    VXB_DMA_MAP_ID	myeRxMblkMap[MYE_RX_DESC_CNT];
    } MYE_RX;

#define MYE_RX_SMALL	0
#define MYE_RX_BIG	1

/*
 * Private adapter context structure.
 */

typedef struct mye_drv_ctrl
    {
    END_OBJ		myeEndObj;
    VXB_DEVICE_ID	myeDev;
    void *		myeBar;
    void *		myeHandle;
    void *		myeMuxDevCookie;

    JOB_QUEUE_ID	myeJobQueue;
    QJOB		myeIntJob;
    atomic_t		myeIntPending;
    UINT32		myeIntrs;

    BOOL		myePolling;
    M_BLK_ID		myePollBuf;

    UINT8		myeAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	myeCaps;

    END_IFDRVCONF	myeEndStatsConf;
    END_IFCOUNTERS	myeEndStatsCounters;
    UINT32		myeInErrors;
    UINT32		myeInDiscards;
    UINT32		myeInUcasts;
    UINT32		myeInMcasts;
    UINT32		myeInBcasts;
    UINT32		myeInOctets;
    UINT32		myeOutErrors;
    UINT32		myeOutUcasts;
    UINT32		myeOutMcasts;
    UINT32		myeOutBcasts;
    UINT32		myeOutOctets;

    SEM_ID		myeDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	myeMediaList;
    END_ERR		myeLastError;
    UINT32		myeCurMedia;
    UINT32		myeCurStatus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	myeParentTag;

    VXB_DMA_TAG_ID	myeRxDescTag;
    VXB_DMA_TAG_ID	myeTxDescTag;
    VXB_DMA_TAG_ID	myeCmdRespTag;
    VXB_DMA_TAG_ID	myeDmaTestTag;
    VXB_DMA_TAG_ID	myeIntrTag;
    VXB_DMA_TAG_ID	myePadTag;

    VXB_DMA_MAP_ID	myeRxDescMap;
    VXB_DMA_MAP_ID	myeTxDescMap;
    VXB_DMA_MAP_ID	myeCmdRespMap;
    VXB_DMA_MAP_ID	myeDmaTestMap;
    VXB_DMA_MAP_ID	myeIntrMap;
    VXB_DMA_MAP_ID	myePadMap;

    VXB_DMA_TAG_ID	myeMblkTag;

    VXB_DMA_MAP_ID	myeTxMblkMap[MYE_TX_DESC_CNT];
    M_BLK_ID		myeTxMblk[MYE_TX_DESC_CNT];

    M_BLK_ID		myeBadMblks;

    UINT32		myeLink;
    void *		myeEthCmd;
    void *		myeBootRqst;
    UINT32 *		myeIntrAck;
    UINT32 *		myeIntrDeas;
    UINT32 *		myeIntrCoal;

    MYE_CMD *		myeCmd;
    MYE_RESP *		myeResp;
    void *		myeDma;

    MYE_RDESC *		myeRxDescMem;
    MYE_TDESC *		myeTxDescMem;
    MYE_IRQ_DATA *	myeIntrMem;
    char *		myePadMem;

    UINT32		myeTxProd;
    UINT32		myeTxCons;
    int			myeTxFree;
    UINT32		myeTxDone;
    BOOL		myeTxStall;

    MYE_TDESC *		myeTxReq;
    UINT32		myeRxCnt;
    UINT32		myeTxMask;
    UINT32		myeTxCnt;
    UINT32		myeTxMax;

    int			myeTxBound;

    /*
     * Two RX contexts: one for the small buffer ring,
     * one for the large buffer ring.
     */

    MYE_RX		myeRx[2];
    UINT32		myeRxIdx;
    UINT32		myeRxMask;

    int			myeMaxMtu;
    } MYE_DRV_CTRL;


#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
            *(pReg) = (UINT32)(data);                           \
        } while ((0))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbMyriLanaiEndh */
