/* vxbUccEnd.h - header file for UCC ethernet VxBus END driver */

/*
 * Copyright (c) 2008-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,02sep09,x_z  rewrite MURAM pool routines and update for MPC8569
01c,05aug09,x_z  add support for SGMII mode
01b,13may09,wap  Correct RX and TX thread counts, correct TX thread structure
                 size
01a,07feb08,wap  written
*/

#ifndef __INCvxbUccEndh
#define __INCvxbUccEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void uccRegister (void);

#define UCC_MEDIA_MII_GMII		0	/* normal MII/GMII mode */
#define UCC_MEDIA_TBI			1	/* TBI mode */
#define UCC_MEDIA_RTBI			2	/* reduced TBI mode */
#define UCC_MEDIA_RMII			3	/* reduced pin 10/100 */
#define UCC_MEDIA_RGMII			4	/* reduced pin 10/100/1000 */
#define UCC_MEDIA_SGMII			5	/* SGMII 10/100/1000 */

#ifndef BSP_VERSION

/* Parameter RAM */

#define UCC_GUMR	0x0000		/* General UCC mode register */
#define UCC_UPSMR	0x0004		/* UCC protocol specific mode reg. */
#define UCC_UTODR	0x0008		/* TX on demand register */
#define UCC_UDSR	0x000C		/* Data synchronization register */
#define UCC_UCCE	0x0010		/* UCC event register */
#define UCC_UCCM	0x0014		/* UCC event mask register */
#define UCC_UCCS	0x0018		/* UCC (TX) status register */

#define UCC_URFB	0x0020		/* UCC RX virtual FIFO base */
#define UCC_URFS	0x0024		/* UCC RX virtual FIFO size */
#define UCC_URFET	0x0028		/* UCC RX virtual FIFO emerg. thresh*/
#define UCC_URFSET	0x002A		/* UCC RX vFIFO spec. emerg. thresh*/
#define UCC_UTFB	0x002C		/* UCC TX virtual FIFO base */
#define UCC_UTFS	0x0030		/* UCC TX virtual FIFO size */
#define UCC_UTFET	0x0034		/* UCC TX virtual FIFO emerg. thresh*/
#define UCC_UTFTT	0x0038		/* UCC TX vFIFO transmit thresh.*/
#define UCC_UTPT	0x003C		/* UCC TX polling timer */
#define UCC_URTRY	0x0040		/* UCC retry counter timer */

#define UCC_GUEMR	0x0090		/* UCC extended mode register */

#define UCC_MACCFG1	0x0100		/* MAC config register 1 */
#define UCC_MACCFG2	0x0104		/* MAC config register 2 */
#define UCC_IPGIFG	0x0108		/* interpacket/intergrame gap */
#define UCC_HAFDUP	0x010C		/* half duplex reigster */
#define UCC_MIIMCFG	0x0120		/* MII management config register */
#define UCC_MIIMCOM	0x0124		/* MII management command register */
#define UCC_MIIMADD	0x0128		/* MII management address register */
#define UCC_MIIMCON	0x012C		/* MII management control register */
#define UCC_MIIMSTAT	0x0130		/* MII management status register */
#define UCC_MIIMIND	0x0134		/* MII management indicator register */
#define UCC_IFSTAT	0x013C		/* interface status register */
#define UCC_PAR0	0x0140		/* unicast address register 0 */
#define UCC_PAR1	0x0144		/* unicast address register 1 */
#define UCC_UEMPR	0x0150		/* ethernet MAC parameter */
#define UCC_UTBIPAR	0x0154		/* ten bit interface address */
#define UCC_UESCR	0x0158		/* ethernet statistics control */

/* Hardware statistics counters */

#define UCC_TX64	0x0180		/* 4-64 byte frames sent */
#define UCC_TX127	0x0184		/* 65-127 byte frames sent */
#define UCC_TX255	0x0188		/* 128-255 byte frames sent */
#define UCC_RX64	0x018C		/* 4-64 bytes frames received */
#define UCC_RX127	0x0190		/* 65-127 byte frames received */
#define UCC_RX255	0x0194		/* 127-255 byte frames received */
#define UCC_TXOK	0x0198		/* total good TX octets */
#define UCC_TXCF	0x019C		/* TX pause frames */
#define UCC_TMCA	0x01A0		/* TX multicasts */
#define UCC_TBCA	0x01A4		/* TX broadcasts */
#define UCC_RXFOK	0x01A8		/* total RX frames */
#define UCC_RBYT	0x01B0		/* total good RX octets */
#define UCC_RXBOK	0x01AC		/* total good+bad RX octets */
#define UCC_RMCA	0x01B4		/* RX multicasts */
#define UCC_RBCA	0x01B8		/* RX broadcasts */
#define UCC_SCAR	0x01BC		/* carry bits */
#define UCC_SCAM	0x01C0		/* mask bits */
#define UCC_RXOV	0x01C4		/* RX discards due to overruns */

/* Mode specific register */

#define UCC_UPSMR_FTFE	0x80000000	/* Flush TX FIFO on flow ctl */
#define UCC_UPSMR_PTPE	0x40000000	/* IEEE 1588 enable */
#define UCC_UPSMR_ECM	0x04000000	/* Discard ext. parser miss */
#define UCC_UPSMR_HSE	0x02000000	/* Hardware stats enable */
#define UCC_UPSMR_ATRD	0x01000000	/* Auto TX recovery disable */
#define UCC_UPSMR_PRO	0x00400000	/* Promiscuous mode */
#define UCC_UPSMR_RSH	0x00100000	/* Receive short frames */
#define UCC_UPSMR_RPM	0x00080000	/* Reduced GMII/TBI pin mode */
#define UCC_UPSMR_R10M	0x00040000	/* Reduced pin 10Mbps mode */
#define UCC_UPSMR_RLPB	0x00020000	/* RMII loopback mode */
#define UCC_UPSMR_TBIM	0x00010000	/* TBI mode */
#define UCC_UPSMR_AUFC	0x0000C000	/* auto flow control enable */
#define UCC_UPSMR_MBO	0x00002000	/* reserved, must be one */
#define UCC_UPSMR_RMM	0x00001000	/* RMII 10/100 mode */
#define UCC_UPSMR_MDCP	0x00000800	/* MDIO clock prescale */
#define UCC_UPSMR_CAM	0x00000800	/* CAM filter matching */
#define UCC_UPSMR_BRO	0x00000200	/* Broad cast frame enable */
#define UCC_UPSMR_SGMM	0x00000020	/* SGMII mode */

/* TX on demand register */

#define UCC_UTODR_TOD	0x0001		/* send frame now */

/* Event register */

#define UCC_ISR_MPD	0x80000000	/* Magic packet detected */
#define UCC_ISR_SCAR	0x40000000	/* stats overflow */
#define UCC_ISR_GRA	0x20000000	/* Graceful stop complete */
#define UCC_ISR_CBPR	0x10000000	/* Change in backpressure state */
#define UCC_ISR_BSY	0x08000000	/* RX overrun (busy) */
#define UCC_ISR_RXC	0x04000000	/* RX flow control */
#define UCC_ISR_TXC	0x02000000	/* TX flow control */
#define UCC_ISR_TXE	0x01000000	/* TX error */
#define UCC_ISR_TXB7	0x00800000	/* TX buffer complete */
#define UCC_ISR_TXB6	0x00400000	/* TX buffer complete */
#define UCC_ISR_TXB5	0x00200000	/* TX buffer complete */
#define UCC_ISR_TXB4	0x00100000	/* TX buffer complete */
#define UCC_ISR_TXB3	0x00080000	/* TX buffer complete */
#define UCC_ISR_TXB2	0x00040000	/* TX buffer complete */
#define UCC_ISR_TXB1	0x00020000	/* TX buffer complete */
#define UCC_ISR_TXB0	0x00010000	/* TX buffer complete */
#define UCC_ISR_RXB7	0x00008000	/* RX buffer complete */
#define UCC_ISR_RXB6	0x00004000	/* RX buffer complete */
#define UCC_ISR_RXB5	0x00002000	/* RX buffer complete */
#define UCC_ISR_RXB4	0x00001000	/* RX buffer complete */
#define UCC_ISR_RXB3	0x00000800	/* RX buffer complete */
#define UCC_ISR_RXB2	0x00000400	/* RX buffer complete */
#define UCC_ISR_RXB1	0x00000200	/* RX buffer complete */
#define UCC_ISR_RXB0	0x00000100	/* RX buffer complete */
#define UCC_ISR_RXF7	0x00000080	/* RX frame complete */
#define UCC_ISR_RXF6	0x00000040	/* RX frame complete */
#define UCC_ISR_RXF5	0x00000020	/* RX frame complete */
#define UCC_ISR_RXF4	0x00000010	/* RX frame complete */
#define UCC_ISR_RXF3	0x00000008	/* RX frame complete */
#define UCC_ISR_RXF2	0x00000004	/* RX frame complete */
#define UCC_ISR_RXF1	0x00000002	/* RX frame complete */
#define UCC_ISR_RXF0	0x00000001	/* RX frame complete */

/* UCC extended mode register (8 bits) */

#define UCC_UGEMR_MBO		0x10	/* must be one */
#define UCC_UGEMR_URMODE	0x02	/* RX mode: 0 = slow, 1 = fast */
#define UCC_UGEMR_UTMODE	0x01	/* TX mode: 0 = slow, 1 = fast */

/* MAC configuration 1 */

#define UCC_MACCFG1_RXF	0x00000020	/* RX flow control enable */
#define UCC_MACCFG1_TXF	0x00000010	/* TX flow control enable */
#define UCC_MACCFG1_RXS	0x00000008	/* RX synced and enabled */
#define UCC_MACCFG1_RXE	0x00000004	/* RX enable */
#define UCC_MACCFG1_TXS	0x00000002	/* TX synced and enabled */
#define UCC_MACCFG1_TXE	0x00000001	/* TX enable */

/* MAC configuration 2 */

#define UCC_MACCFG2_PRL	0x0000F000	/* Preamble length */
#define UCC_MACCFG2_IFM	0x00000300	/* Interface mode */
#define UCC_MACCFG2_SRP	0x00000080	/* Soft RX preamble enable */
#define UCC_MACCFG2_STP	0x00000040	/* Soft TX preamble enable */
#define UCC_MACCFG2_MBO	0x00000020	/* Reserved, must be one */
#define UCC_MACCFG2_LC	0x00000010	/* Perform RX length check */
#define UCC_MACCFG2_MPE	0x00000008	/* Magic packet enable */
#define UCC_MACCFG2_PCE	0x00000004	/* Pad/CRC outbound frames enable */
#define UCC_MACCFG2_CRE	0x00000002	/* CRC enable */
#define UCC_MACCFG2_FDX	0x00000001	/* Full duplex mode enable */

#define UCC_PREAMLEN(x)			(((x) << 12) & UCC_MACCFG2_PRL)

#define UCC_IFMODE_RSVD0		0x00000000
#define UCC_IFMODE_MII			0x00000100 /* nibble mode */
#define UCC_IFMODE_GMII_TBI		0x00000200 /* byte mode */
#define UCC_IFMODE_RSVD1		0x00000300

/* Inter-frame gap register */

#define UCC_IPGIFG_BACKTOBACK          0x0000007F
#define UCC_IPGIFG_MINIMUM             0x00007F00
#define UCC_IPGIFG_NONBACKTOBACK1      0x007F0000
#define UCC_IPGIFG_NONBACKTOBACK2      0x7F000000

/* Half duplex register */

#define UCC_HAFDUP_COLLWIN		0x0000003F
#define UCC_HAFDUP_RETRYCNT		0x0000F000
#define UCC_HAFDUP_EXCESSDEFER		0x00010000
#define UCC_HAFDUP_NOBACKOFF		0x00020000
#define UCC_HAFDUP_BP_NOBACKOFF		0x00040000
#define UCC_HAFDUP_ALTBEB		0x00080000
#define UCC_HAFDUP_ALTBDB_TRUNC		0x00F00000

/* MII management configuration register */

#define UCC_MIIMCFG_RESET              0x80000000
#define UCC_MIIMCFG_NO_PRE             0x00000010
#define UCC_MIIMCFG_MGMT_CLK           0x0000000F

#define UCC_MIIMCFG_MCS_2              0x00000000
#define UCC_MIIMCFG_MCS_4              0x00000001
#define UCC_MIIMCFG_MCS_6              0x00000002
#define UCC_MIIMCFG_MCS_8              0x00000003
#define UCC_MIIMCFG_MCS_10             0x00000004
#define UCC_MIIMCFG_MCS_14             0x00000005
#define UCC_MIIMCFG_MCS_20             0x00000006
#define UCC_MIIMCFG_MCS_28             0x00000007

/* MII management command register */

#define UCC_MIIMCOM_SCAN               0x00000002
#define UCC_MIIMCOM_READ               0x00000001

/* MII management address register */

#define UCC_MIIMADD_PHYADDR            0x0000001F
#define UCC_MIIMADD_REGADDR            0x00001F00

/* MII management control register */

#define UCC_MIIMCON_WRDATA             0x0000FFFF

/* MII management status register */

#define UCC_MIIMSTAT_RDDATA            0x0000FFFF

/* MII management indicator register */

#define UCC_MIIMIND_NOT_VALID          0x00000004
#define UCC_MIIMIND_SCAN               0x00000002
#define UCC_MIIMIND_BUSY               0x00000001

/* Ethernet Statistics Control Register */

#define UCC_UESCR_AUTOZ			0x8000	/* Zero counters on read */
#define UCC_UESCR_CLRCNT		0x4000	/* clear all counters */
#define UCC_UESCR_MAXCOV		0x3F00	/* max coalescing value */
#define UCC_UESCR_SCOV			0x00FF	/* Status coalescing value */

typedef struct ucc_desc
    {
    volatile UINT16		bdSts;
    volatile UINT16		bdLen;
    volatile UINT32		bdPtr;
    } UCC_DESC;

/* TX Parameter RAM */

#define UCC_TEMODER	0x0000		/* TX eth mode register (16bits)*/
#define UCC_SQPTR	0x0038		/* TX send queue base addr (32bits)*/
#define UCC_SCPTR	0x003C		/* scheduler mem base addr (32bits)*/
#define UCC_TXRMONPTR	0x0040		/* TX counter base addr (32bits)*/
#define UCC_TSTATE	0x0044		/* transmit state (32bits) */
#define UCC_IPOFF0	0x0048		/* offset to IP header (8bits) */
#define UCC_IPOFF1	0x0049		/* offset to IP header (8bits) */
#define UCC_IPOFF2	0x004A		/* offset to IP header (8bits) */
#define UCC_IPOFF3	0x004B		/* offset to IP header (8bits) */
#define UCC_IPOFF4	0x004C		/* offset to IP header (8bits) */
#define UCC_IPOFF5	0x004D		/* offset to IP header (8bits) */
#define UCC_IPOFF6	0x004E		/* offset to IP header (8bits) */
#define UCC_IPOFF7	0x004F		/* offset to IP header (8bits) */
#define UCC_VTAGTBL0	0x0050		/* VLAN table (32bits) */
#define UCC_VTAGTBL1	0x0054		/* VLAN table (32bits) */
#define UCC_VTAGTBL2	0x0058		/* VLAN table (32bits) */
#define UCC_VTAGTBL3	0x005C		/* VLAN table (32bits) */
#define UCC_VTAGTBL4	0x0060		/* VLAN table (32bits) */
#define UCC_VTAGTBL5	0x0064		/* VLAN table (32bits) */
#define UCC_VTAGTBL6	0x0068		/* VLAN table (32bits) */
#define UCC_VTAGTBL7	0x006C		/* VLAN table (32bits) */
#define UCC_TQPTR	0x0070		/* thread base addr (32bits) */

/* Transmit ethermet mode register */

#define UCC_TEMODER_SOF		0x8000	/* start of frame */
#define UCC_TEMODER_SOB		0x4000	/* start of bd */
#define UCC_TEMODER_SCENB	0x2000	/* sched enable */
#define UCC_TEMODER_PACF	0x1000	/* pref and conf flag */
#define UCC_TEMODER_IPCHK	0x0400	/* enable IP checksum generation */
#define UCC_TEMODER_OPTIM	0x0200	/*  */
#define UCC_TEMODER_RMONEN	0x0100	/* RMON stats enable */
#define UCC_TEMODER_QNUM	0x0007	/* Number of queues */

#define UCC_TXQNUM(x)		(((x) - 1) & UCC_TEMODER_QNUM)

typedef struct ucc_sqqd
    {
    volatile UCC_DESC *	ucc_bdbase;
    volatile UINT8	ucc_rsvd0[8];
    volatile UCC_DESC *	ucc_bdlast;
    volatile UINT8	ucc_rsvd1[48];
    } UCC_SQQD;

#define UCC_SQQD_ALIGN		32
#define UCC_SQQD_SIZE		(sizeof(UCC_SQQD) * UCC_TX_QUEUE_CNT)

#define UCC_TX_QUEUES		8


/* RX parameter RAM */

#define UCC_REMODER	0x0000		/* RX ethernet mode register */
#define UCC_RQPTR	0x0004		/* RX thread data pointer */
#define UCC_TOL		0x0020		/* type or length */
#define UCC_RXGSTPACK	0x0022		/* RX graceful stop ack (16bits)*/
#define UCC_RXRMONPTR	0x0024		/* RX RMON base addr */
#define UCC_INTCOALPTR	0x0030		/* int coal table ptr */
#define UCC_BUSYVEC	0x0034		/* busy vector bits (8 bits)*/
#define UCC_RSTATE	0x0036		/* RX state (8 bits) */
#define UCC_VEDPM	0x0038		/* VPri based Early Drop Priority Mask */
#define UCC_OVSKIPFRM	0x0044		/* frames to skip on oflow (8bits)*/
#define UCC_OVSKIPFRMCT	0x0045		/* internal variable */
#define UCC_MRBLR	0x0046		/* max RX buflen (16 bits)*/
#define UCC_RBDQPTR	0x0048		/* RX BD table ptr (32 bits)*/
#define UCC_MFLR	0x004C		/* MAX frame len (16 bits) */
#define UCC_MINFLR	0x004E		/* MIN frame length (16bits) */
#define UCC_MAXD1	0x0050		/* Max DMA1 length (16bits) */
#define UCC_MAXD2	0x0052		/* Max DMA2 length (16bits) */
#define UCC_ECAMPTR	0x0054		/* CAM pointer (reserved?) */
#define UCC_L2QT	0x0058		/* VLAN prio mapping table (32bits)*/
#define UCC_L3QT	0x005C		/* IP prio mapping table (32bits)*/
#define UCC_VLANTYPE	0x007C		/* VLAN type (0x8100) (16bits)*/
#define UCC_TCI		0x007E		/* default TCI (16bits) */
#define UCC_AF0		0x0080		/* address filter table (32bits) */
#define UCC_AF1		0x0084		/* address filter table (32bits) */
#define UCC_AF2		0x0088		/* address filter table (32bits) */
#define UCC_AF3		0x008C		/* address filter table (32bits) */
#define UCC_AF4		0x0090		/* address filter table (32bits) */
#define UCC_AF5		0x0094		/* address filter table (32bits) */
#define UCC_AF6		0x0098		/* address filter table (32bits) */
#define UCC_AF7		0x009C		/* address filter table (32bits) */
#define UCC_AF8		0x00A0		/* address filter table (32bits) */
#define UCC_AF9		0x00A4		/* address filter table (32bits) */
#define UCC_AFA		0x00A8		/* address filter table (32bits) */
#define UCC_AFB		0x00AC		/* address filter table (32bits) */
#define UCC_AFC		0x00B0		/* address filter table (32bits) */
#define UCC_AFD		0x00B4		/* address filter table (32bits) */
#define UCC_AFE		0x00B8		/* address filter table (32bits) */
#define UCC_AFF		0x00BC		/* address filter table (32bits) */

#define UCC_IADDR_H	0x0080		/* Individual address filter high */
#define UCC_IADDR_L	0x0084		/* Individual address filter low */
#define UCC_GADDR_H	0x0088		/* Group address filter high */
#define UCC_GADDR_L	0x008C		/* Group address filter low */

#define UCC_TADDR_H	0x0092		/* Hash table address high */
#define UCC_TADDR_M	0x0094		/* Hash table address middle */
#define UCC_TADDR_L	0x0096		/* Hash table address low */

#define UCC_PADDR1_H	0x009A		/* Extra ucast address high */
#define UCC_PADDR1_M	0x009C		/* Extra ucast address med */
#define UCC_PADDR1_L	0x009E		/* Extra ucast address low */

#define UCC_PADDR2_H	0x00A2		/* Extra ucast address high */
#define UCC_PADDR2_M	0x00A4		/* Extra ucast address med */
#define UCC_PADDR2_L	0x00A6		/* Extra ucast address low */

#define UCC_PADDR3_H	0x00AA		/* Extra ucast address high */
#define UCC_PADDR3_M	0x00AC		/* Extra ucast address med */
#define UCC_PADDR3_L	0x00AE		/* Extra ucast address low */

#define UCC_PADDR4_H	0x00B2		/* Extra ucast address high */
#define UCC_PADDR4_M	0x00B4		/* Extra ucast address med */
#define UCC_PADDR4_L	0x00B6		/* Extra ucast address low */

#define UCC_TCI2TYPE	0x00BA		/* Value of type field in 2nq qtag. */

#define UCC_EXPGPPTR	0x00C0		/* extended gbl parm table (32bits)*/
#define UCC_LLFCPTR	0x00C4		/* lossless flowctl table (32bits)*/

#define UCC_REMODER_IPAE	0x00000001	/* IP address align enable */
#define UCC_REMODER_IPCHK	0x00000002	/* IP checksum check */
#define UCC_REMODER_DNE		0x00000004	/* dynamic min framelen enb */
#define UCC_REMODER_DXE		0x00000008	/* dynamic max framelen enb */
#define UCC_REMODER_VFSEN	0x00000020	/* virtual FIFO smoother enb*/
#define UCC_REMODER_QNUM	0x00000700	/* number of queues */
#define UCC_REMODER_EXP		0x00000800	/* extended address parsing */
#define UCC_REMODER_RFSE	0x00001000	/* RX firmware stats cntrs */
#define UCC_REMODER_LLFCEN	0x00004000	/* lossless flow control */
#define UCC_REMODER_RQOS	0x00030000	/* RX QoS mode */
#define UCC_REMODER_VNTOP	0x00200000	/* VLAN non-tag op */
#define UCC_REMODER_VTOP	0x03C00000	/* VLAN tag op */
#define UCC_REMODER_EXF		0x80000000	/* Extended feature mode */

#define UCC_RXQNUM(x)		((((x) - 1) << 8) & UCC_REMODER_QNUM)

/* Bus mode register (RSTATE and TSTATE) */

#define UCC_BMR_BDB		0x01		/* BD bus
						   0 == coherent system bus
						   1 == QE secondary bus
						 */
#define UCC_BMR_DTB		0x02		/* data buffer bus */
#define UCC_BMR_CETM		0x04		/* QE transaction mark */
#define UCC_BMR_BO		0x18		/* Byte ordering (0x10 == BE) */
#define UCC_BMR_GBL		0x20		/* CSB snoop enable */

#define UCC_BO_BIGENDIAN	0x10

typedef struct ucc_rqqd
    {
    volatile UINT32		ucc_bdbase;
    volatile UINT32		ucc_bdcons;
    volatile UCC_DESC *		ucc_extbdbase;
    volatile UCC_DESC *		ucc_extbdcons;
    volatile UCC_DESC		ucc_prefetch[4];
    } UCC_RQQD;

#define UCC_RQQD_ALIGN		8
#define UCC_RQQD_SIZE		(sizeof(UCC_RQQD) * UCC_RX_QUEUE_CNT)

typedef struct ucc_rxic
    {
    volatile UINT32		ucc_maxval;
    volatile UINT32		ucc_coalcnt;
    } UCC_RXIC;

#define UCC_RXIC_ALIGN		64
#define UCC_RXIC_SIZE		((sizeof(UCC_RXIC) * UCC_RX_QUEUE_CNT) + 4)

#define UCC_RX_QUEUES		8

#define UCC_TBD_CTL_RDY		0x8000		/* Ready */
#define UCC_TBD_CTL_PADCRC	0x4000		/* Autopad/auto CRC */
#define UCC_TBD_CTL_WRAP	0x2000		/* End of ring */
#define UCC_TBD_CTL_INTR	0x1000		/* Interrupt on complete */
#define UCC_TBD_CTL_LAST	0x0800		/* Last buffer in frame */
#define UCC_TBD_CTL_TCRC	0x0400		/* Generate CRC */
#define UCC_TBD_CTL_PP		0x0100		/* Send programmable preamble*/
#define UCC_TBD_CTL_IPCH0	0x0040		/* IP checksum offset */
#define UCC_TBD_CTL_VID		0x003C		/* VLAN ID */
#define UCC_TBD_CTL_IPCH1	0x0002		/* IP checksum offset */
#define UCC_TBD_CTL_IPCH2	0x0001		/* IP checksum offset */

#define UCC_TBD_STS_DEF		0x0200		/* Frame defered */
#define UCC_TBD_STS_EXTDEF	0x0100		/* Excessive deferal */
#define UCC_TBD_STS_LC		0x0080		/* Late collision */
#define UCC_TBD_STS_RL		0x0040		/* Retry limit exceeded */
#define UCC_TBD_STS_RC		0x003C		/* Retry count */
#define UCC_TBD_STS_UN		0x0002		/* Underrun */
#define UCC_TBD_STS_CSL		0x0001		/* Carrier sense lost */

#define UCC_RBD_CTL_EMPTY	0x8000		/* Empty */
#define UCC_RBD_CTL_WRAP	0x2000		/* End of ring */
#define UCC_RBD_CTL_INTR	0x1000		/* Interrupt on completion */

#define UCC_RBD_STS_LAST	0x0800		/* Last buffer in frame */
#define UCC_RBD_STS_FIRST	0x0400		/* First buffer in frame */
#define UCC_RBD_STS_PTP		0x0200		/* PTP frame */
#define UCC_RBD_STS_MISS	0x0100		/* filter miss (promisc mode)*/
#define UCC_RBD_STS_BCAST	0x0080		/* Broadcast frame */
#define UCC_RBD_STS_MCAST	0x0040		/* Multicast frame */
#define UCC_RBD_STS_GIANT	0x0020		/* Frame too largr */
#define UCC_RBD_STS_NO		0x0010		/* Non-octet aligned frame */
#define UCC_RBD_STS_RUNT	0x0008		/* Frame too small */
#define UCC_RBD_STS_CRC		0x0004		/* CRC error */
#define UCC_RBD_STS_OV		0x0002		/* Overrun */
#define UCC_RBD_STS_IPCH	0x0001		/* IP header checksum good */

typedef struct ucc_rx_mib
    {
    volatile UINT32		ucc_crcerrs;	/* CRC errors */
    volatile UINT32		ucc_alignerrs;	/* alignment errors */
    volatile UINT32		ucc_irangeerrs; /* in range errors */
    volatile UINT32		ucc_orangeerrs;	/* out of range errors */
    volatile UINT32		ucc_giants;	/* giant frames */
    volatile UINT32		ucc_runts;	/* runt frames */
    volatile UINT32		ucc_verylong;	/* very long frames */
    volatile UINT32		ucc_symbolerrs;	/* symbol errors */
    volatile UINT32		ucc_norxbd;	/* drops due to lack of BDs */
    volatile UINT32		ucc_rsvd0;
    volatile UINT32		ucc_rsvd1;
    volatile UINT32		ucc_mismatch;	/* filter drops */
    volatile UINT32		ucc_rx64;	/* 0-63 byte RX frames */
    volatile UINT32		ucc_rx256;	/* 0-63 byte RX frames */
    volatile UINT32		ucc_rx512;	/* 0-63 byte RX frames */
    volatile UINT32		ucc_rx1024;	/* 0-63 byte RX frames */
    volatile UINT32		ucc_rxjumbo;	/* 0-63 byte RX frames */
    volatile UINT32		ucc_macerr;	/* MAC internal error */
    volatile UINT32		ucc_rxpause;	/* pause frames */
    volatile UINT32		ucc_vlandrop;	/* vlan drop count */
    volatile UINT32		ucc_vlanremv;	/* vlan remove count */
    volatile UINT32		ucc_vlanrepl;	/* vlan replace count */
    volatile UINT32		ucc_vlanins;	/* vlan insert count */
    volatile UINT32		ucc_ipcsumerr;	/* IP checksum errors */
    } UCC_RX_MIB;

#define UCC_RX_MIB_ALIGN	4

typedef struct ucc_tx_mib
    {
    volatile UINT32		ucc_singlecols;	/* single collisions */
    volatile UINT32		ucc_multicols;	/* multiple collisions */
    volatile UINT32		ucc_latecolls;	/* late collisions */
    volatile UINT32		ucc_aborts;	/* aborts due to collisions */
    volatile UINT32		ucc_macerr;	/* MAC internal error */
    volatile UINT32		ucc_carrloss;	/* carrier sense lost */
    volatile UINT32		ucc_txok;	/* total good transmits */
    volatile UINT32		ucc_excessdef;	/* excessive deferrals */
    volatile UINT32		ucc_tx256;	/* 256-511 byte transmits */
    volatile UINT32		ucc_tx512;	/* 512-1024 byte transmits */
    volatile UINT32		ucc_tx1024;	/* 1024-1517 byte transmits */
    volatile UINT32		ucc_txjumbo;	/* jumbo transmits */
    } UCC_TX_MIB;

#define UCC_TX_MIB_ALIGN	4

#define UCC_MTU			1500
#define UCC_JUMBO_MTU		9000
#define UCC_CLSIZE		1536
#define UCC_NAME		"qefcc"
#define UCC_TIMEOUT		10000

#define UCC_INTRS	(UCC_RXINTRS|UCC_TXINTRS)

#define UCC_RXINTRS (UCC_ISR_BSY|UCC_ISR_RXF0|UCC_ISR_RXC)
#define UCC_TXINTRS (UCC_ISR_TXB0|UCC_ISR_TXC|UCC_ISR_TXE)

#define UCC_TBI_ADDR		0x1E

#define UCC_TX_DESC_CNT		128
#define UCC_RX_DESC_CNT		128

/* Number of RX and TX descriptor rings in use. */

#define UCC_RX_QUEUE_CNT	1
#define UCC_TX_QUEUE_CNT	1

/*
 * Extended global parsing paremeters
 */

#define UCC_EXP_ALIGN		8

/* RX/TX thread num configuration(rgf/tgf) */

#define UCC_NUM_OF_THREAD_1 0x1 /* 1 thread */
#define UCC_NUM_OF_THREAD_2 0x2 /* 2 thread */
#define UCC_NUM_OF_THREAD_4 0x0 /* 4 thread (unsupported on 8323) */
#define UCC_NUM_OF_THREAD_6 0x3 /* 6 thread (unsupported on 8323) */
#define UCC_NUM_OF_THREAD_8 0x4 /* 8 thread (unsupported on 8323) */

/*
 * 4 threads for 1000/100/10 Mbps and 1 thread for 100/10Mbps suggested by the
 * manual. The global RX thread are not included.
 *
 * The default value is 4 for MPC8569 and 1 for other because only MPC8569 has
 * enough threads.
 */

#define UCC_RGF_DEFAULT         UCC_NUM_OF_THREAD_1
#define UCC_TGF_DEFAULT         UCC_NUM_OF_THREAD_1
#define UCC_RX_THR_CNT          1
#define UCC_TX_THR_CNT          1

#define UCC_RGF_DEFAULT_8569    UCC_NUM_OF_THREAD_4
#define UCC_TGF_DEFAULT_8569    UCC_NUM_OF_THREAD_4
#define UCC_RX_THR_CNT_8569     4
#define UCC_TX_THR_CNT_8569     4

/*
 * Thread parameter RAM size.
 */

#define UCC_RX_THR_PRAM_SIZE	128
#define UCC_RX_THR_PRAM_ALIGN	128

#define UCC_TX_THR_PRAM_SIZE	64
#define UCC_TX_THR_PRAM_ALIGN	64


/*
 * RX thread data structure size. The manual recommends 160 bytes
 * for 1000Mbps and 40 bytes for 10/100.
 */

#define UCC_RX_THR_SIZE		40
#define UCC_RX_THR_ALIGN	128

/*
 * TX thread data structure size. The manual recommends 416 bytes
 * for 4 threads (1000Mbps) and 136 bytes for 1 thread (10/100).
 * Note that except for the 1 thread case, the size is a multiple
 * of 104 bytes (208, 416, 624, 832).
 *
 * Note: for later parts, documentation indicates that driver
 * code must allocate 136 bytes per thread.
 */

#define UCC_TX_THR_SIZE		136
#define UCC_TX_THR_ALIGN	128

/*
 * RX global parameter RAM size
 */

#define UCC_RX_GBL_PRAM_SIZE	256
#define UCC_RX_GBL_PRAM_ALIGN	64

/*
 * TX global parameter RAM size
 */

#define UCC_TX_GBL_PRAM_SIZE	128
#define UCC_TX_GBL_PRAM_ALIGN	64

/*
 * Virtual FIFO sizes. The manual recommends the following values for MPC8323:
 *
 * Rx - 512 for 1 thread, 816 for 2 threads
 * Tx - 512 for 1 thread, 512 for 2 threads
 *
 * and the following values for others:
 *
 * Rx - 3072 for gigabit ethernet, 272 for Fast Ethernet
 * Tx - 2048 for gigabit ethernet, 272 for Fast Ethernet
 *
 * Sufficient FIFO size is important for increasing the QUICC Engine block's
 * performance by eliminating overrun and underrun conditions. But the MURAM may
 * be not enough for the recommended value except for MPC8569.
 *
 * Note: The gigabit ethernet is unsupported on MPC8323.
 */

#define UCC_RX_FIFO_SIZE        1024
#define UCC_TX_FIFO_SIZE        1024
#define UCC_RX_FIFO_SIZE_8569   3072
#define UCC_TX_FIFO_SIZE_8569   2048
#define UCC_RX_FIFO_ALIGN       8
#define UCC_TX_FIFO_ALIGN       8

/*
 * Rx VFIFO Block Size. The Rx VFIFO has to be at least 4 times the size of Rx
 * VFIFO Block. The default value is 0 becuase the value must be 0 for the
 * MPC8323.
 */

#define UCC_RX_VFIFO_BLK_SIZE_128       0x0
#define UCC_RX_VFIFO_BLK_SIZE_192       0x00bf0000
#define UCC_RX_VFIFO_BLK_SIZE_248       0x00f70000

#define UCC_ETHER_ALIGN		2
#define UCC_ADJ(x)		(x)->m_data += UCC_ETHER_ALIGN

#define UCC_INC_DESC(x, y)      (x) = ((x + 1) & (y - 1))
#define UCC_MAXFRAG             16
#define UCC_MAX_RX              32

/* UCC TBI control register */

#define UCC_TBI_TBICON          17

#define UCC_TBI_TBICON_CLK_SEL  0x0020  /* clock select  */

/*
 * Private adapter context structure.
 */

typedef struct ucc_drv_ctrl
    {
    END_OBJ		uccEndObj;
    VXB_DEVICE_ID	uccDev;
    void *		uccBar;
    void *		uccHandle;
    void *		uccMuxDevCookie;

    int			uccNum;
    VXB_DEVICE_ID	qeDev;
    FUNCPTR		qeCmdFunc;
    FUNCPTR		qeMuAllocFunc;
    FUNCPTR		qeMuFreeFunc;
    FUNCPTR		qeMuOffsetGetFunc;
    FUNCPTR		qeThreadAllocFunc;
    FUNCPTR		qeThreadFreeFunc;
    CECDR_RXTX_INIT *	uccCecdr;

    JOB_QUEUE_ID	uccJobQueue;
    QJOB		uccIntJob;
    atomic_t		uccIntPending;

    QJOB		uccRxJob;
    atomic_t		uccRxPending;

    QJOB		uccTxJob;
    atomic_t		uccTxPending;
    UINT8		uccTxCur;
    UINT8		uccTxLast;
    volatile BOOL	uccTxStall;
    UINT16		uccTxThresh;

    BOOL		uccPolling;
    M_BLK_ID		uccPollBuf;
    UINT32		uccIntMask;
    UINT32		uccIntrs;

    UINT8		uccAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	uccCaps;

    END_IFDRVCONF	uccEndStatsConf;
    END_IFCOUNTERS	uccEndStatsCounters;

    SEM_ID		uccDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *	uccMediaList;
    END_ERR		uccLastError;
    UINT32		uccCurMedia;
    UINT32		uccCurStatus;
    VXB_DEVICE_ID	uccMiiBus;
    VXB_DEVICE_ID       uccMiiDev;
    FUNCPTR             uccMiiPhyRead;
    FUNCPTR             uccMiiPhyWrite;
    int                 uccMiiPhyAddr;
    int                 uccTbiAddr;

    /* End MII/ifmedia required fields */

    BOOL		uccMedia;

    /* DMA tags and maps. */

    char *		uccRxGPRam;
    char *		uccRxThrRam[9];
    UCC_RQQD *		uccRxQDesc;
    char *		uccRxQRam;
    char *		uccRxFifo;
    UCC_RXIC *		uccRxCoalDesc;
    UCC_RX_MIB *	uccRxMib;

    char *		uccTxGPRam;
    char *		uccTxThrRam[9];
    UCC_SQQD *		uccTxQDesc;
    char *		uccTxQRam;
    char *		uccTxFifo;
    UCC_TX_MIB *	uccTxMib;

    UCC_DESC *		uccRxDescMem;
    UCC_DESC *		uccTxDescMem;

    M_BLK_ID		uccRxMblk[UCC_RX_DESC_CNT];
    M_BLK_ID		uccTxMblk[UCC_TX_DESC_CNT];

    UINT32		uccTxProd;
    UINT32		uccTxCons;
    UINT32		uccTxFree;
    UINT32		uccRxIdx;

    UINT32		uccMaxMtu;

    UINT32		uccRxThNum;
    UINT32		uccTxThNum;
    UINT32		uccRxFifoSize;
    UINT32		uccTxFifoSize;
    UINT32		uccVersion;
    } UCC_DRV_CTRL;

#define UCC_BAR(p)   ((UCC_DRV_CTRL *)(p)->pDrvCtrl)->uccBar
#define UCC_HANDLE(p)   ((UCC_DRV_CTRL *)(p)->pDrvCtrl)->uccHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (UCC_HANDLE(pDev), (UINT32 *)((char *)UCC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (UCC_HANDLE(pDev),                             \
        (UINT32 *)((char *)UCC_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (UCC_HANDLE(pDev), (UINT16 *)((char *)UCC_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (UCC_HANDLE(pDev),                             \
        (UINT16 *)((char *)UCC_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (UCC_HANDLE(pDev), (UINT8 *)((char *)UCC_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (UCC_HANDLE(pDev),                              \
        (UINT8 *)((char *)UCC_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define PRAM_READ_4(base, addr)			\
        *(volatile UINT32 *)((UINT32)base + addr)

#define PRAM_WRITE_4(base, addr, data)				\
        do {							\
            volatile UINT32 *pReg =				\
                (UINT32 *)((UINT32)base + addr);		\
            *(pReg) = (UINT32)(data);				\
            WRS_ASM("eieio");					\
        } while ((0))

#define PRAM_SETBIT_4(base, offset, val)          \
        PRAM_WRITE_4(base, offset, PRAM_READ_4(base, offset) | (val))

#define PRAM_CLRBIT_4(base, offset, val)          \
        PRAM_WRITE_4(base, offset, PRAM_READ_4(base, offset) & ~(val))

#define PRAM_READ_2(base, addr)			\
        *(volatile UINT16 *)((UINT32)base + addr)

#define PRAM_WRITE_2(base, addr, data)				\
        do {							\
            volatile UINT16 *pReg =				\
                (UINT16 *)((UINT32)base + addr);		\
            *(pReg) = (UINT16)(data);				\
            WRS_ASM("eieio");					\
        } while ((0))

#define PRAM_SETBIT_2(base, offset, val)          \
        PRAM_WRITE_2(base, offset, PRAM_READ_2(base, offset) | (val))

#define PRAM_CLRBIT_2(base, offset, val)          \
        PRAM_WRITE_2(base, offset, PRAM_READ_2(base, offset) & ~(val))

#define PRAM_READ_1(base, addr)			\
        *(volatile UINT8 *)((UINT32)base + addr)

#define PRAM_WRITE_1(base, addr, data)				\
        do {							\
            volatile UINT8 *pReg =				\
                (UINT8 *)((UINT32)base + addr);			\
            *(pReg) = (UINT8)(data);				\
            WRS_ASM("eieio");					\
        } while ((0))

#define PRAM_SETBIT_1(base, offset, val)          \
        PRAM_WRITE_1(base, offset, PRAM_READ_2(base, offset) | (val))

#define PRAM_CLRBIT_1(base, offset, val)          \
        PRAM_WRITE_1(base, offset, PRAM_READ_2(base, offset) & ~(val))
#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbUccEndh */
