/* vxbViaRhineEnd.h - header file for Via Networking Rhine VxBus END driver */

/*
 * Copyright (c) 2007-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,19jul08,wap  Correct definition for VR_RXSTAT_RX_OK
01a,18aug07,wap  written
*/

#ifndef __INCvxbViaRhineEndh
#define __INCvxbViaRhineEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vrRegister (void);

#ifndef BSP_VERSION

#define VR_VENDORID_VIA		0x1106

/* Rhine PCI IDs */

#define VR_DEVICEID_RHINE	0x3043
#define VR_DEVICEID_RHINE_II	0x6100
#define VR_DEVICEID_RHINE_II_2	0x3065
#define VR_DEVICEID_RHINE_III	0x3106
#define VR_DEVICEID_RHINE_III_M	0x3053

/* Rhine revision IDs. */

#define VR_REV_VT3043_E		0x04
#define VR_REV_VT3071_A		0x20
#define VR_REV_VT3071_B		0x21
#define VR_REV_VT3065_A		0x40
#define VR_REV_VT3065_B		0x41
#define VR_REV_VT3065_C		0x42
#define VR_REV_VT6102_APOLLO	0x74
#define VR_REV_VT3106		0x80
#define VR_REV_VT3106_J		0x80    /* 0x80-0x8F */
#define VR_REV_VT3106_S		0x90    /* 0x90-0xA0 */


/*
 * Rhine register definitions.
 */

#define VR_PAR0			0x00    /* node address 0 to 4 */
#define VR_PAR1			0x04    /* node address 2 to 6 */
#define VR_RXCFG		0x06    /* receiver config register */
#define VR_TXCFG		0x07    /* transmit config register */
#define VR_COMMAND		0x08    /* command register */
#define VR_ISR			0x0C    /* interrupt/status register */
#define VR_IMR			0x0E    /* interrupt mask register */
#define VR_MAR0			0x10    /* multicast hash 0 */
#define VR_MAR1			0x14    /* multicast hash 1 */
#define VR_RXBASE		0x18    /* rx descriptor list start addr */
#define VR_TXBASE		0x1C    /* tx descriptor list start addr */
#define VR_CURRXDESC0		0x20
#define VR_CURRXDESC1		0x24
#define VR_CURRXDESC2		0x28
#define VR_CURRXDESC3		0x2C
#define VR_NEXTRXDESC0		0x30
#define VR_NEXTRXDESC1		0x34
#define VR_NEXTRXDESC2		0x38
#define VR_NEXTRXDESC3		0x3C
#define VR_CURTXDESC0		0x40
#define VR_CURTXDESC1		0x44
#define VR_CURTXDESC2		0x48
#define VR_CURTXDESC3		0x4C
#define VR_NEXTTXDESC0		0x50
#define VR_NEXTTXDESC1		0x54
#define VR_NEXTTXDESC2		0x58
#define VR_NEXTTXDESC3		0x5C
#define VR_CURRXDMA		0x60    /* current RX DMA address */
#define VR_CURTXDMA		0x64    /* current TX DMA address */
#define VR_TALLYCNT		0x68    /* tally counter test register */
#define VR_MIIPHYADDR		0x6C
#define VR_MIISTAT		0x6D
#define VR_BCR0			0x6E
#define VR_BCR1			0x6F
#define VR_MIICMD		0x70
#define VR_MIIREGADDR		0x71
#define VR_MIIDATA		0x72
#define VR_EECSR		0x74
#define VR_TEST			0x75
#define VR_GPIO			0x76
#define VR_CONFIG		0x78
#define VR_MPA_CNT		0x7C
#define VR_CRC_CNT		0x7E
#define VR_MISC_CR1		0x81
#define VR_STICKHW		0x83

/*
 * RX configuration register
 */

#define VR_RXCFG_RX_ERRPKTS     0x01
#define VR_RXCFG_RX_RUNT        0x02
#define VR_RXCFG_RX_MULTI       0x04
#define VR_RXCFG_RX_BROAD       0x08
#define VR_RXCFG_RX_PROMISC     0x10
#define VR_RXCFG_RX_THRESH      0xE0

#define VR_RXTHRESH_32BYTES     0x00
#define VR_RXTHRESH_64BYTES     0x20
#define VR_RXTHRESH_128BYTES    0x40
#define VR_RXTHRESH_256BYTES    0x60
#define VR_RXTHRESH_512BYTES    0x80
#define VR_RXTHRESH_768BYTES    0xA0
#define VR_RXTHRESH_1024BYTES   0xC0
#define VR_RXTHRESH_STORENFWD   0xE0

/*
 * TX configuration register 
 */

#define VR_TXCFG_RSVD0          0x01
#define VR_TXCFG_LOOPBKMODE     0x06
#define VR_TXCFG_BACKOFF        0x08
#define VR_TXCFG_RSVD1          0x10
#define VR_TXCFG_TX_THRESH      0xE0

#define VR_TXTHRESH_32BYTES     0x00
#define VR_TXTHRESH_64BYTES     0x20
#define VR_TXTHRESH_128BYTES    0x40
#define VR_TXTHRESH_256BYTES    0x60
#define VR_TXTHRESH_512BYTES    0x80
#define VR_TXTHRESH_768BYTES    0xA0
#define VR_TXTHRESH_1024BYTES   0xC0
#define VR_TXTHRESH_STORENFWD   0xE0

/*
 * Command register
 */

#define VR_CMD_INIT             0x0001
#define VR_CMD_START            0x0002
#define VR_CMD_STOP             0x0004
#define VR_CMD_RX_ON            0x0008
#define VR_CMD_TX_ON            0x0010
#define VR_CMD_TX_GO            0x0020
#define VR_CMD_RX_GO            0x0040
#define VR_CMD_RSVD             0x0080
#define VR_CMD_RX_EARLY         0x0100
#define VR_CMD_TX_EARLY         0x0200
#define VR_CMD_FULLDUPLEX       0x0400
#define VR_CMD_TX_NOPOLL        0x0800
#define VR_CMD_RESET            0x8000

/*
 * Interrupt status register
 */

#define VR_ISR_RX_OK            0x0001  /* packet rx ok */
#define VR_ISR_TX_OK            0x0002  /* packet tx ok */
#define VR_ISR_RX_ERR           0x0004  /* packet rx with err */
#define VR_ISR_TX_ABRT          0x0008  /* tx aborted due to excess colls */
#define VR_ISR_TX_UNDERRUN      0x0010  /* tx buffer underflow */
#define VR_ISR_RX_NOBUF         0x0020  /* no rx buffer available */
#define VR_ISR_BUSERR           0x0040  /* PCI bus error */
#define VR_ISR_STATSOFLOW       0x0080  /* stats counter oflow */
#define VR_ISR_RX_EARLY         0x0100  /* rx early */
#define VR_ISR_LINKSTAT         0x0200  /* MII status change */
#define VR_ISR_ETI              0x0200  /* Tx early (3043/3071) */
#define VR_ISR_UDFI             0x0200  /* Tx FIFO underflow (3065) */
#define VR_ISR_RX_OFLOW         0x0400  /* rx FIFO overflow */
#define VR_ISR_RX_DROPPED       0x0800
#define VR_ISR_RX_NOBUF2        0x1000
#define VR_ISR_TX_ABRT2         0x2000
#define VR_ISR_LINKSTAT2        0x4000
#define VR_ISR_MAGICPACKET      0x8000

/*
 * Interrupt mask register.
 */

#define VR_IMR_RX_OK            0x0001  /* packet rx ok */
#define VR_IMR_TX_OK            0x0002  /* packet tx ok */
#define VR_IMR_RX_ERR           0x0004  /* packet rx with err */
#define VR_IMR_TX_ABRT          0x0008  /* tx aborted due to excess colls */
#define VR_IMR_TX_UNDERRUN      0x0010  /* tx buffer underflow */
#define VR_IMR_RX_NOBUF         0x0020  /* no rx buffer available */
#define VR_IMR_BUSERR           0x0040  /* PCI bus error */
#define VR_IMR_STATSOFLOW       0x0080  /* stats counter oflow */
#define VR_IMR_RX_EARLY         0x0100  /* rx early */
#define VR_IMR_LINKSTAT         0x0200  /* MII status change */
#define VR_IMR_RX_OFLOW         0x0400  /* rx FIFO overflow */
#define VR_IMR_RX_DROPPED       0x0800
#define VR_IMR_RX_NOBUF2        0x1000
#define VR_IMR_TX_ABRT2         0x2000
#define VR_IMR_LINKSTAT2        0x4000
#define VR_IMR_MAGICPACKET      0x8000

/*
 * MII status register.
 */

#define VR_MIISTAT_SPEED        0x01
#define VR_MIISTAT_LINKFAULT    0x02
#define VR_MIISTAT_MGTREADERR   0x04
#define VR_MIISTAT_MIIERR       0x08
#define VR_MIISTAT_PHYOPT       0x10
#define VR_MIISTAT_MDC_SPEED    0x20
#define VR_MIISTAT_RSVD         0x40
#define VR_MIISTAT_GPIO1POLL    0x80

/*
 * MII command register.
 */

#define VR_MIICMD_CLK           0x01
#define VR_MIICMD_DATAOUT       0x02
#define VR_MIICMD_DATAIN        0x04
#define VR_MIICMD_DIR           0x08
#define VR_MIICMD_DIRECTPGM     0x10
#define VR_MIICMD_WRITE_ENB     0x20
#define VR_MIICMD_READ_ENB      0x40
#define VR_MIICMD_AUTOPOLL      0x80

/*
 * EEPROM control register.
 */

#define VR_EECSR_DATAOUT        0x01    /* data out */
#define VR_EECSR_DATAIN         0x02    /* data in */
#define VR_EECSR_CLK            0x04    /* clock */
#define VR_EECSR_CS             0x08    /* chip select */
#define VR_EECSR_DPM            0x10
#define VR_EECSR_LOAD           0x20
#define VR_EECSR_EMBP           0x40
#define VR_EECSR_EEPR           0x80

/* 9346 EEPROM commands */

#define VR_9346_WRITE		0x5
#define VR_9346_READ		0x6
#define VR_9346_ERASE		0x7

#define VR_EE_ADDR		0x0

/*
 * Test register.
 */

#define VR_TEST_TEST0           0x01
#define VR_TEST_TEST1           0x02
#define VR_TEST_TEST2           0x04
#define VR_TEST_TSTUD           0x08
#define VR_TEST_TSTOV           0x10
#define VR_TEST_BKOFF           0x20
#define VR_TEST_FCOL            0x40
#define VR_TEST_HBDES           0x80

/*
 * Config register.
 */

#define VR_CFG_GPIO2OUTENB      0x00000001
#define VR_CFG_GPIO2OUT         0x00000002      /* gen. purp. pin */
#define VR_CFG_GPIO2IN          0x00000004      /* gen. purp. pin */
#define VR_CFG_AUTOOPT          0x00000008      /* enable rx/tx autopoll */
#define VR_CFG_MIIOPT           0x00000010
#define VR_CFG_MMIENB           0x00000020      /* memory mapped mode enb */
#define VR_CFG_JUMPER           0x00000040      /* PHY and oper. mode select */
#define VR_CFG_EELOAD           0x00000080      /* enable EEPROM programming */
#define VR_CFG_LATMENB          0x00000100      /* larency timer effect enb. */
#define VR_CFG_MRREADWAIT       0x00000200
#define VR_CFG_MRWRITEWAIT      0x00000400
#define VR_CFG_RX_ARB           0x00000800
#define VR_CFG_TX_ARB           0x00001000
#define VR_CFG_READMULTI        0x00002000
#define VR_CFG_TX_PACE          0x00004000
#define VR_CFG_TX_QDIS          0x00008000
#define VR_CFG_ROMSEL0          0x00010000
#define VR_CFG_ROMSEL1          0x00020000
#define VR_CFG_ROMSEL2          0x00040000
#define VR_CFG_ROMTIMESEL       0x00080000
#define VR_CFG_RSVD0            0x00100000
#define VR_CFG_ROMDLY           0x00200000
#define VR_CFG_ROMOPT           0x00400000
#define VR_CFG_RSVD1            0x00800000
#define VR_CFG_BACKOFFOPT       0x01000000
#define VR_CFG_BACKOFFMOD       0x02000000
#define VR_CFG_CAPEFFECT        0x04000000
#define VR_CFG_BACKOFFRAND      0x08000000
#define VR_CFG_MAGICKPACKET     0x10000000
#define VR_CFG_PCIREADLINE      0x20000000
#define VR_CFG_DIAG             0x40000000
#define VR_CFG_GPIOEN           0x80000000

/*
 * BCR0 register bits. (At least for the VT6102 chip.)
 */

#define VR_BCR0_DMA_LENGTH      0x07
#define VR_BCR0_RX_THRESH       0x38
#define VR_BCR0_EXTLED          0x40
#define VR_BCR0_MED2            0x80


#define VR_BCR0_DMA_32BYTES     0x00
#define VR_BCR0_DMA_64BYTES     0x01
#define VR_BCR0_DMA_128BYTES    0x02
#define VR_BCR0_DMA_256BYTES    0x03
#define VR_BCR0_DMA_512BYTES    0x04
#define VR_BCR0_DMA_1024BYTES   0x05
#define VR_BCR0_DMA_STORENFWD   0x07

#define VR_BCR0_RXTHRCFG	0x00
#define VR_BCR0_RXTHR64BYTES	0x08
#define VR_BCR0_RXTHR128BYTES	0x10
#define VR_BCR0_RXTHR256BYTES	0x18
#define VR_BCR0_RXTHR512BYTES	0x20
#define VR_BCR0_RXTHR1024BYTES	0x28
#define VR_BCR0_RXTHRSTORENFWD	0x38

/*
 * BCR1 register bits. (At least for the VT6102 chip.)
 */
#define VR_BCR1_POT0            0x01
#define VR_BCR1_POT1            0x02
#define VR_BCR1_POT2            0x04
#define VR_BCR1_TX_THRESH       0x38

#define VR_BCR1_TXTHRCFG	0x00
#define VR_BCR1_TXTHR64BYTES	0x08
#define VR_BCR1_TXTHR128BYTES	0x10
#define VR_BCR1_TXTHR256BYTES	0x18
#define VR_BCR1_TXTHR512BYTES	0x20
#define VR_BCR1_TXTHR1024BYTES	0x28
#define VR_BCR1_TXTHRSTORENFWD	0x38

/* Misc control register 1 */

#define VR_MISCCR1_FORCERST      0x40

/*
 * Sticky HW register
 */

#define VR_STICKHW_DS0          0x01
#define VR_STICKHW_DS1          0x02
#define VR_STICKHW_WOL_ENB      0x04
#define VR_STICKHW_WOL_STS      0x08
#define VR_STICKHW_LEGWOL_ENB   0x80


/*
 * Rhine DMA descriptor structures. Each descriptor is 16 bytes in
 * size, and similar in layout to the DEC 21140/21143 layout.
 */

typedef struct vr_desc
    {
    volatile UINT32	vrSts;
    volatile UINT32	vrCtl;
    volatile UINT32	vrPtr;
    volatile UINT32	vrNext;
    } VR_DESC;

/* RX descriptor status field */

#define VR_RXSTAT_RXERR         0x00000001
#define VR_RXSTAT_CRCERR        0x00000002
#define VR_RXSTAT_FRAMEALIGNERR 0x00000004
#define VR_RXSTAT_FIFOOFLOW     0x00000008
#define VR_RXSTAT_GIANT         0x00000010
#define VR_RXSTAT_RUNT          0x00000020
#define VR_RXSTAT_BUSERR        0x00000040
#define VR_RXSTAT_BUFFERR       0x00000080
#define VR_RXSTAT_LASTFRAG      0x00000100
#define VR_RXSTAT_FIRSTFRAG     0x00000200
#define VR_RXSTAT_RLINK         0x00000400
#define VR_RXSTAT_RX_PHYS       0x00000800
#define VR_RXSTAT_RX_BROAD      0x00001000
#define VR_RXSTAT_RX_MULTI      0x00002000
#define VR_RXSTAT_RX_VIDMATCH   0x00004000
#define VR_RXSTAT_RX_OK         0x00008000
#define VR_RXSTAT_RXLEN         0x07FF0000
#define VR_RXSTAT_RXLEN_EXT     0x78000000
#define VR_RXSTAT_OWN           0x80000000

#define VR_RXBYTES(x)           ((x & VR_RXSTAT_RXLEN) >> 16)

/* Rx descriptor control field. */

#define VR_RXCTL_BUFLEN         0x000007FF
#define VR_RXCTL_BUFLEN_EXT     0x00007800
#define VR_RXCTL_CHAIN          0x00008000
#define VR_RXCTL_RX_INTR        0x00800000
#define VR_RXCTL_GOODIP         0x00280000
#define VR_RXCTL_GOODTCPUDP     0x00100000

/* TX descriptor status field */

#define VR_TXSTAT_DEFER         0x00000001
#define VR_TXSTAT_UNDERRUN      0x00000002
#define VR_TXSTAT_COLLCNT       0x00000078
#define VR_TXSTAT_SQE           0x00000080
#define VR_TXSTAT_ABRT          0x00000100
#define VR_TXSTAT_LATECOLL      0x00000200
#define VR_TXSTAT_CARRLOST      0x00000400
#define VR_TXSTAT_UDF           0x00000800
#define VR_TXSTAT_BUSERR        0x00002000
#define VR_TXSTAT_JABTIMEO      0x00004000
#define VR_TXSTAT_ERRSUM        0x00008000
#define VR_TXSTAT_OWN           0x80000000

/* TX descriptor control field */

#define VR_TXCTL_BUFLEN         0x000007FF
#define VR_TXCTL_BUFLEN_EXT     0x00007800
#define VR_TXCTL_TLINK          0x00008000
#define VR_TXCTL_NOCRC          0x00010000
#define VR_TXCTL_INSERTTAG      0x00020000
#define VR_TXCTL_IPCSUM         0x00040000
#define VR_TXCTL_UDPCSUM        0x00080000
#define VR_TXCTL_TCPCSUM        0x00100000
#define VR_TXCTL_FIRSTFRAG      0x00200000
#define VR_TXCTL_LASTFRAG       0x00400000
#define VR_TXCTL_FINT           0x00800000

#define VR_RX_DESC_CNT	32
#define VR_TX_DESC_CNT	32

#define VR_MAXFRAG	16
#define VR_MAX_RX	16
#define VR_MTU		1500
#define VR_JUMBO_MTU	9000
#define VR_CLSIZE	1536
#define VR_NAME		"vr"
#define VR_TIMEOUT	10000
#define VR_INTRS (VR_RXINTRS|VR_TXINTRS|VR_LINKINTRS)
#define VR_RXINTRS (VR_ISR_RX_OK|VR_ISR_RX_NOBUF|VR_ISR_RX_ERR|VR_ISR_RX_DROPPED)
#define VR_TXINTRS (VR_ISR_TX_OK|VR_ISR_TX_UNDERRUN|VR_ISR_TX_ABRT)
#define VR_LINKINTRS (VR_ISR_LINKSTAT2)

#if (CPU_FAMILY == I80X86) || (CPU_FAMILY == PPC) || (CPU_FAMILY == COLDFIRE)
#define VR_ADJ(x)
#else
#define VR_RX_FIXUP
#define VR_ADJ(x)      m_adj((x), 8)
#endif

#define VR_INC_DESC(x, y)       (x) = ((x) + 1) % (y)

/*
 * Private adapter context structure.
 */

typedef struct vr_drv_ctrl
    {
    END_OBJ		vrEndObj;
    VXB_DEVICE_ID	vrDev;
    void *		vrBar;
    void *		vrHandle;
    void *		vrMuxDevCookie;

    JOB_QUEUE_ID	vrJobQueue;
    QJOB		vrIntJob;
    atomic_t		vrIntPending;

    QJOB		vrRxJob;
    atomic_t		vrRxPending;

    QJOB		vrTxJob;
    atomic_t		vrTxPending;
    volatile BOOL	vrTxStall;

    BOOL		vrPolling;
    M_BLK_ID		vrPollBuf;
    UINT16		vrIntMask;
    UINT16		vrIntrs;

    UINT8		vrAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	vrCaps;

    BOOL		vrTxAlign;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*vrMediaList;
    END_ERR		vrLastError;
    UINT32		vrCurMedia;
    UINT32		vrCurStatus;
    VXB_DEVICE_ID	vrMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	vrParentTag;

    VXB_DMA_TAG_ID	vrRxDescTag;
    VXB_DMA_MAP_ID	vrRxDescMap;
    VR_DESC *		vrRxDescMem;

    VXB_DMA_TAG_ID      vrTxDescTag;
    VXB_DMA_MAP_ID      vrTxDescMap;
    VR_DESC *		vrTxDescMem;

    VXB_DMA_TAG_ID	vrMblkTag;

    VXB_DMA_MAP_ID	vrRxMblkMap[VR_RX_DESC_CNT];
    VXB_DMA_MAP_ID	vrTxMblkMap[VR_TX_DESC_CNT];

    M_BLK_ID		vrRxMblk[VR_RX_DESC_CNT];
    M_BLK_ID		vrTxMblk[VR_TX_DESC_CNT];

    UINT32		vrTxProd;
    UINT32		vrTxCons;
    UINT32		vrTxFree;
    UINT32		vrRxIdx;

    int			vrEeWidth;

    SEM_ID		vrDevSem;
    } VR_DRV_CTRL;


#define VR_BAR(p)   ((VR_DRV_CTRL *)(p)->pDrvCtrl)->vrBar
#define VR_HANDLE(p)   ((VR_DRV_CTRL *)(p)->pDrvCtrl)->vrHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (VR_HANDLE(pDev), (UINT32 *)((char *)VR_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (VR_HANDLE(pDev),                             \
        (UINT32 *)((char *)VR_BAR(pDev) + addr), data)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (VR_HANDLE(pDev), (UINT16 *)((char *)VR_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                           \
    vxbWrite16 (VR_HANDLE(pDev),                             \
        (UINT16 *)((char *)VR_BAR(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (VR_HANDLE(pDev), (UINT8 *)((char *)VR_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (VR_HANDLE(pDev),                              \
        (UINT8 *)((char *)VR_BAR(pDev) + addr), data)

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbViaRhineEndh */
