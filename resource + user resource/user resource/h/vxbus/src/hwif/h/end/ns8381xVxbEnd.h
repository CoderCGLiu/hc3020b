/* ns8381xVxbEnd.h - header file for NatSemi DP83815/6 VxBus END driver */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,11jul07,wap  Make this driver SMP safe, convert to new register access API
01c,29jan07,wap  Correct RX fixup issue on MIPS32 arch
01b,01sep06,wap  Switch to using the I/O space BAR
01a,14apr06,wap  written
*/

#ifndef __INCns8381xVxbEndh
#define __INCns8381xVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void nseRegister (void);

#ifndef BSP_VERSION

#define NSE_VENDORID_NATSEMI	0x100B
#define NSE_DEVICEID_8381X	0x0020

/*
 * Registers for the MAC
 */

#define NSE_CSR			0x00
#define NSE_CFG			0x04
#define NSE_EECTL		0x08
#define NSE_PCICTL		0x0C
#define NSE_ISR			0x10
#define NSE_IMR			0x14
#define NSE_IER			0x18
#define NSE_IHR			0x1C
#define NSE_TX_LISTPTR		0x20
#define NSE_TXCFG		0x24
#define NSE_RX_LISTPTR		0x30
#define NSE_RXCFG		0x34
#define NSE_CLKRUN		0x3C
#define NSE_WOL			0x40
#define NSE_PAUSE		0x44
#define NSE_RXFILT_CTL		0x48
#define NSE_RXFILT_DATA		0x4C
#define NSE_BOOTROM_ADDR	0x50
#define NSE_BOOTROM_DATA	0x54
#define NSE_SRR			0x58
#define NSE_MIBCTL		0x5C
#define NSE_MIBDAT0		0x60
#define NSE_MIBDAT1		0x64
#define NSE_MIBDAT2		0x68
#define NSE_MIBDAT3		0x6C
#define NSE_MIBDAT4		0x70
#define NSE_MIBDAT5		0x74
#define NSE_MIBDAT6		0x78


/* control/status register */

#define NSE_CSR_TX_ENABLE       0x00000001
#define NSE_CSR_TX_DISABLE      0x00000002
#define NSE_CSR_RX_ENABLE       0x00000004
#define NSE_CSR_RX_DISABLE      0x00000008
#define NSE_CSR_TX_RESET        0x00000010
#define NSE_CSR_RX_RESET        0x00000020
#define NSE_CSR_SOFTINTR        0x00000080
#define NSE_CSR_RESET           0x00000100

/* configuration and media status register */

#define NSE_CFG_BIGENDIAN	0x00000001
#define NSE_CFG_BROM_DISABLE	0x00000004
#define NSE_CFG_PERR_DETECT	0x00000008
#define NSE_CFG_DEFER_DISABLE	0x00000010
#define NSE_CFG_OUTOFWIN_TIMER	0x00000020
#define NSE_CFG_SINGLE_BACKOFF	0x00000040
#define NSE_CFG_PCIREQ_ALG	0x00000080
#define NSE_CFG_83810_COMPAT	0x00000100
#define NSE_CFG_INTPHY_DISABLE	0x00000200
#define NSE_CFG_INTPHY_RESET	0x00000400
#define NSE_CFG_EXTPHY_ENABLE	0x00001000
#define NSE_CFG_ANEG_SEL	0x0000E000
#define NSE_CFG_PAUSE_ADV	0x00010000
#define NSE_CFG_PHYINT_AUTOCLR	0x00020000
#define NSE_CFG_PHYCFG		0x00FC0000
#define NSE_CFG_ANEG_DONE	0x08000000
#define NSE_CFG_POLARITY	0x10000000
#define NSE_CFG_FULL_DUPLEX	0x20000000
#define NSE_CFG_SPEED100	0x40000000
#define NSE_CFG_LINK		0x80000000

/* EEPROM access and MDIO register */

#define NSE_EECTL_DOUT		0x00000001
#define NSE_EECTL_DIN		0x00000002
#define NSE_EECTL_CLK		0x00000004
#define NSE_EECTL_CSEL		0x00000008
#define NSE_EECTL_MDIO_DATA	0x00000010
#define NSE_EECTL_MDIO_DIR	0x00000020
#define NSE_EECTL_MDIO_CLK	0x00000040

#define NSE_9346_WRITE		0x5
#define NSE_9346_READ		0x6
#define NSE_9346_ERASE		0x7

#define NSE_EE_NODEADDR		0x6

/* PCI test control register */

#define NSE_PCICTL_EEBIST_FAIL	0x00000001
#define NSE_PCICTL_EEBIST_EN	0x00000002
#define NSE_PCICTL_EELOAD_EN	0x00000004
#define NSE_PCICTL_RBIST_RXFFAIL	0x00000008
#define NSE_PCICTL_RBIST_TXFAIL	0x00000010
#define NSE_PCICTL_RBIST_RXFAIL	0x00000020
#define NSE_PCICTL_RBIST_DONE	0x00000040
#define NSE_PCICTL_RBIST_ENABLE	0x00000080
#define NSE_PCICTL_RBIST_RESET	0x00000400

/* Interrupt status (and mask) register */

#define NSE_ISR_RX_OK           0x00000001
#define NSE_ISR_RX_DESC_OK      0x00000002
#define NSE_ISR_RX_ERR          0x00000004
#define NSE_ISR_RX_EARLY        0x00000008
#define NSE_ISR_RX_IDLE         0x00000010
#define NSE_ISR_RX_OFLOW        0x00000020
#define NSE_ISR_TX_OK           0x00000040
#define NSE_ISR_TX_DESC_OK      0x00000080
#define NSE_ISR_TX_ERR          0x00000100
#define NSE_ISR_TX_IDLE         0x00000200
#define NSE_ISR_TX_UFLOW        0x00000400
#define NSE_ISR_MIB_OFLOW	0x00000800
#define NSE_ISR_SOFTINTR        0x00001000
#define NSE_ISR_PME		0x00002000
#define NSE_ISR_PHY		0x00004000
#define NSE_ISR_HIBITS          0x00008000
#define NSE_ISR_RX_FIFO_OFLOW   0x00010000
#define NSE_ISR_TGT_ABRT        0x00100000
#define NSE_ISR_BM_ABRT         0x00200000
#define NSE_ISR_SYSERR          0x00400000
#define NSE_ISR_PARITY_ERR      0x00800000
#define NSE_ISR_RX_RESET_DONE   0x01000000
#define NSE_ISR_TX_RESET_DONE   0x02000000

/* Interrupt enable register */

#define NSE_IER_ENABLE		0x00000001

/* Interrupt holdoff register */

#define NSE_IHR_HOLDCNT		0x000000FF
#define NSE_IHR_CTL		0x00000100

/* Transmit configuration register */

#define NSE_TXCFG_DRAIN_THRESH  0x0000003F /* 32-byte units */
#define NSE_TXCFG_FILL_THRESH   0x00003F00 /* 32-byte units */
#define NSE_TXCFG_MPII03D       0x00040000 /* "Must be 1" */ 
#define NSE_TXCFG_DMABURST      0x00700000
#define NSE_TXCFG_EC_RETRY	0x00800000
#define NSE_TXCFG_IFG		0x0C000000
#define NSE_TXCFG_AUTOPAD       0x10000000
#define NSE_TXCFG_LOOPBK        0x20000000
#define NSE_TXCFG_IGN_HBEAT     0x40000000
#define NSE_TXCFG_IGN_CARR      0x80000000

#define NSE_TXCFG_DRAIN(x)      (((x) >> 5) & NSE_TXCFG_DRAIN_THRESH)
#define NSE_TXCFG_FILL(x)       ((((x) >> 5) << 8) & NSE_TXCFG_FILL_THRESH)

#define NSE_TXDMA_512BYTES      0x00000000
#define NSE_TXDMA_4BYTES        0x00100000
#define NSE_TXDMA_8BYTES        0x00200000
#define NSE_TXDMA_16BYTES       0x00300000
#define NSE_TXDMA_32BYTES       0x00400000
#define NSE_TXDMA_64BYTES       0x00500000
#define NSE_TXDMA_128BYTES      0x00600000
#define NSE_TXDMA_256BYTES      0x00700000

#define NSE_TXCFG_INIT   \
        (NSE_TXDMA_256BYTES|NSE_TXCFG_AUTOPAD|NSE_TXCFG_EC_RETRY|\
         NSE_TXCFG_FILL(512)|NSE_TXCFG_DRAIN(1472))

/* Receive configuration registers */

#define NSE_RXCFG_DRAIN_THRESH	0x0000003E /* 8-byte units */
#define NSE_RXCFG_DMABURST	0x00700000
#define NSE_RXCFG_RX_GIANT	0x08000000
#define NSE_RXCFG_RX_TXPKTS	0x10000000
#define NSE_RXCFG_RX_RUNTS	0x40000000
#define NSE_RXCFG_RX_BAD	0x80000000

#define NSE_RXCFG_DRAIN(x)      ((((x) >> 3) << 1) & NSE_RXCFG_DRAIN_THRESH)

#define NSE_RXDMA_512BYTES      0x00000000
#define NSE_RXDMA_4BYTES        0x00100000
#define NSE_RXDMA_8BYTES        0x00200000
#define NSE_RXDMA_16BYTES       0x00300000
#define NSE_RXDMA_32BYTES       0x00400000
#define NSE_RXDMA_64BYTES       0x00500000
#define NSE_RXDMA_128BYTES      0x00600000
#define NSE_RXDMA_256BYTES      0x00700000

#define NSE_RXCFG256 \
        (NSE_RXCFG_DRAIN(64)|NSE_RXDMA_256BYTES)
#define NSE_RXCFG64 \
        (NSE_RXCFG_DRAIN(64)|NSE_RXDMA_64BYTES)

/* CLKRUN control/status register */

#define NSE_CLKRUN_ENABLE	0x000000001
#define NSE_CLKRUN_PME_ENABLE	0x000000100
#define NSE_CLKRUN_PMESTS	0x000008000

/* Pause control/status register */

#define NSE_PAUSE_PAUSECNT	0x0000FFFF
#define NSE_PAUSE_MANUAL_LOAD	0x00010000
#define NSE_PAUSE_NEGOTIATED	0x00200000
#define NSE_PAUSE_FRAME_RXED	0x00400000
#define NSE_PAUSE_ACTIVE	0x00800000
#define NSE_PAUSE_UCAST		0x20000000
#define NSE_PAUSE_MCAST		0x40000000
#define NSE_PAUSE_RX_ENABLE	0x80000000

/* RX filter control register */

#define NSE_RXFILTCTL_ADDR	0x000003FF
#define NSE_RXFILTCTL_UCHASH	0x00100000
#define NSE_RXFILTCTL_MCHASH	0x00200000
#define NSE_RXFILTCTL_ARP	0x00400000
#define NSE_RXFILTCTL_UCAST	0x08000000
#define NSE_RXFILTCTL_ALLPHYS	0x10000000
#define NSE_RXFILTCTL_ALLMULTI	0x20000000
#define NSE_RXFILTCTL_BROAD	0x40000000
#define NSE_RXFILTCTL_ENABLE	0x80000000

#define NSE_FILTADDR_PAR0	0x00000000
#define NSE_FILTADDR_PAR1	0x00000002
#define NSE_FILTADDR_PAR2	0x00000004

#define NSE_FILTADDR_FMEM_LO	0x00000200
#define NSE_FILTADDR_FMEM_HI	0x000003FE

/* NS silicon revisions */

#define NSE_SRR_15C		0x302
#define NSE_SRR_15D		0x403
#define NSE_SRR_16A4		0x504
#define NSE_SRR_16A5		0x505

/* DMA descriptor formats */

typedef struct ns_desc
    {
    volatile UINT32	nse_next;
    volatile UINT32	nse_cmdsts;
    volatile UINT32	nse_data;
    } NSE_DESC;

#define nse_rxstat	nse_cmdsts
#define nse_txstat	nse_cmdsts
#define nse_ctl		nse_cmdsts

#define NSE_CMDSTS_BUFLEN       0x00000FFF
#define NSE_CMDSTS_PKT_OK       0x08000000
#define NSE_CMDSTS_CRC          0x10000000
#define NSE_CMDSTS_INTR         0x20000000
#define NSE_CMDSTS_MORE         0x40000000
#define NSE_CMDSTS_OWN          0x80000000

#define NSE_RXSTAT_COLL         0x00010000
#define NSE_RXSTAT_LOOPBK       0x00020000
#define NSE_RXSTAT_ALIGNERR     0x00040000
#define NSE_RXSTAT_CRCERR       0x00080000
#define NSE_RXSTAT_SYMBOLERR    0x00100000
#define NSE_RXSTAT_RUNT         0x00200000
#define NSE_RXSTAT_GIANT        0x00400000
#define NSE_RXSTAT_DSTCLASS     0x01800000
#define NSE_RXSTAT_OVERRUN      0x02000000
#define NSE_RXSTAT_RX_ABORT     0x04000000

#define NSE_RXSTAT_ERR		\
    (NSE_RXSTAT_ALIGNERR|NSE_RXSTAT_CRCERR|NSE_RXSTAT_SYMBOLERR|	\
     NSE_RXSTAT_RUNT|NSE_RXSTAT_GIANT|NSE_RXSTAT_OVERRUN|NSE_RXSTAT_RX_ABORT)

#define NSE_DSTCLASS_REJECT     0x00000000
#define NSE_DSTCLASS_UNICAST    0x00800000
#define NSE_DSTCLASS_MULTICAST  0x01000000
#define NSE_DSTCLASS_BROADCAST  0x02000000

#define NSE_TXSTAT_COLLCNT      0x000F0000
#define NSE_TXSTAT_EXCESSCOLLS  0x00100000
#define NSE_TXSTAT_OUTOFWINCOLL 0x00200000
#define NSE_TXSTAT_EXCESS_DEFER 0x00400000
#define NSE_TXSTAT_DEFERED      0x00800000
#define NSE_TXSTAT_CARR_LOST    0x01000000
#define NSE_TXSTAT_UNDERRUN     0x02000000
#define NSE_TXSTAT_TX_ABORT     0x04000000

#define NSE_TXSTAT_ERR		\
    (NSE_TXSTAT_EXCESSCOLLS|NSE_TXSTAT_OUTOFWINCOLL|			\
     NSE_TXSTAT_EXCESS_DEFER|NSE_TXSTAT_DEFERED|NSE_TXSTAT_CARR_LOST|	\
     NSE_TXSTAT_UNDERRUN|NSE_TXSTAT_TX_ABORT)

#define NSE_INC_DESC(x, y)	(x) = ((x) + 1) % y

/*
 * Registers for the internal 10/100 PHY
 * The BMCR, BMSR, ANAR, ANLPAR, ANER and ANNP registers are
 * defined by the MII standard, which in VxWorks is described
 * in miiLib.h. We just use the miiLib definitions instead of
 * duplicating them here.
 *
 * The other registers are specific to the NatSemi PHYTER PHY
 * and are described below.
 */

#define NSE_BMCR		0x80	/* control */
#define NSE_BMSR		0x84	/* status */
#define NSE_PHYIDR1		0x88	/* PHY ID1 */
#define NSE_PHYIDR2		0x8C	/* PHY ID2 */
#define NSE_ANAR		0x90	/* autoneg advertisement */
#define NSE_ANLPAR		0x94	/* link partner ability */
#define NSE_ANER		0x98	/* autoneg expansion */
#define NSE_ANNP		0x9C	/* next page */
#define NSE_PHYSTS		0xC0
#define NSE_MICR		0xC4
#define NSE_MISR		0xC8
#define NSE_PHY_PAGE		0xCC
#define NSE_FCSCR		0xD0
#define NSE_RECR		0xD4
#define NSE_PCSR		0xD8
#define NSE_PHYCR		0xE4
#define NSE_TBTSCR		0xE8
#define NSE_PHY_EXTCFG		0xF0
#define NSE_PHY_DSPCFG		0xF4
#define NSE_PHY_SDCFG		0xF8
#define NSE_PHY_TDATA		0xFC

/* PHY status register */

#define NSE_PHYSTS_LINK		0x0001
#define NSE_PHYSTS_SPEED10	0x0002
#define NSE_PHYSTS_DUPLEX	0x0004
#define NSE_PHYSTS_LOOPBACK	0x0008
#define NSE_PHYSTS_ANEG_DONE	0x0010
#define NSE_PHYSTS_JABBER	0x0020
#define NSE_PHYSTS_REMFAULT	0x0040
#define NSE_PHYSTS_INTR		0x0080
#define NSE_PHYSTS_PAGE_RXED	0x0100
#define NSE_PHYSTS_DESCRAM_LCK	0x0200
#define NSE_PHYSTS_SIG_DETECT	0x0400
#define NSE_PHYSTS_POLARITY	0x0800
#define NSE_PHYSTS_RXERR	0x1000

/* Interrupt control register */

#define NSE_MICR_TESTINT	0x0001
#define NSE_MICR_INTEN		0x0002

/* Interrupt status & misc control register */

#define NSE_MISR_RXCNT_OFLOW	0x0200
#define NSE_MISR_FCAR_OFLOW	0x0400
#define NSE_MISR_ANEG_DONE	0x0800
#define NSE_MISR_REMFAULT	0x1000
#define NSE_MISR_JABBER		0x2000
#define NSE_MISR_LINK		0x4000
#define NSE_MISR_INTR		0x8000

/* 100Mbps PCS config and status register */

#define NSE_PCSR_NRZI_BYPASS	0x0004
#define NSE_PCSR_FORCE_LINK	0x0020
#define NSE_PCSR_SD_OPTION	0x0100
#define NSE_PCSR_FORCE_B	0x0200
#define NSE_PCSR_TRUE_QUIET	0x0400
#define NSE_PCSR_FREE_RXCLK	0x0800
#define NSE_PCSR_4B5B_BYPASS	0x1000

/* PHY control register */

#define NSE_PHYCR_PHYADDR	0x001F
#define NSE_PHYCR_PAUSE_STS	0x0080
#define NSE_PHYCR_STRETCH_BYP	0x0100
#define NSE_PHYCR_BIST_START	0x0200
#define NSE_PHYCR_BIST_STS	0x0400
#define NSE_PHYCR_PSR_15	0x0800

/* 10BaseT control/status register */

#define NSE_TBTSCR_JABBER_DIS	0x0001
#define NSE_TBTSCR_HBEAD_DIS	0x0002
#define NSE_TBTSCR_AUTOPOL_DIS	0x0008
#define NSE_TBTSCR_POLARITY	0x0010
#define NSE_TBTSCR_FORCE_POL	0x0020
#define NSE_TBTSCR_FORCE_LINK	0x0040
#define NSE_TBTSCR_LP_DIS	0x0080
#define NSE_TBTSCR_LOOPBK_DIS	0x0100



#define NSE_CLSIZE	1536
#define NSE_NAME	"nse"
#define NSE_TIMEOUT 10000
#define NSE_INTRS 	\
    (NSE_ISR_RX_OFLOW|NSE_ISR_TX_UFLOW|NSE_ISR_TX_OK|	\
     NSE_ISR_TX_IDLE|NSE_ISR_RX_OK|NSE_ISR_RX_ERR|	\
     NSE_ISR_RX_IDLE|NSE_ISR_PHY)

#define NSE_RXINTRS					\
    (NSE_ISR_RX_OFLOW|NSE_ISR_RX_OK|NSE_ISR_RX_ERR|NSE_ISR_RX_IDLE)

#define NSE_TXINTRS					\
    (NSE_ISR_TX_UFLOW|NSE_ISR_TX_OK|NSE_ISR_TX_ERR|NSE_ISR_TX_IDLE)

#define NSE_LINKINTRS 	NSE_ISR_PHY

#define NSE_RX_DESC_CNT	32
#define NSE_TX_DESC_CNT	32
#define NSE_MAX_RX	16
#define NSE_MAXFRAG	16
#if (CPU_FAMILY == I80X86) || (CPU_FAMILY == PPC) || (CPU_FAMILY == COLDFIRE)
#define NSE_ADJ(x)
#else
#define NSE_RX_FIXUP
#define NSE_ADJ(x)      m_adj((x), 8)
#endif

/*
 * Private adapter context structure.
 */

typedef struct nse_drv_ctrl
    {
    END_OBJ		nseEndObj;
    VXB_DEVICE_ID	nseDev;
    void *		nseBar;
    void *		nseHandle;
    void *		nseMuxDevCookie;

    JOB_QUEUE_ID	nseJobQueue;
    QJOB		nseIntJob;

    QJOB		nseRxJob;
    atomicVal_t		nseRxPending;

    QJOB		nseTxJob;
    atomicVal_t		nseTxPending;
    UINT8		nseTxCur;
    UINT8		nseTxLast;
    volatile BOOL	nseTxStall;
    UINT16		nseTxThresh;

    BOOL		nsePolling;
    M_BLK_ID		nsePollBuf;
    UINT32		nseIntMask;
    UINT32		nseIntrs;
    UINT32		nseIntStatus;

    UINT8		nseAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	nseCaps;

    END_IFDRVCONF	nseEndStatsConf;
    END_IFCOUNTERS	nseEndStatsCounters;


    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*nseMediaList;
    END_ERR		nseLastError;
    UINT32		nseCurMedia;
    UINT32		nseCurStatus;
    VXB_DEVICE_ID	nseMiiBus;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */
    VXB_DMA_TAG_ID	nseParentTag;

    VXB_DMA_TAG_ID	nseRxDescTag;
    VXB_DMA_MAP_ID	nseRxDescMap;
    NSE_DESC		*nseRxDescMem;

    VXB_DMA_TAG_ID      nseTxDescTag;
    VXB_DMA_MAP_ID      nseTxDescMap;
    NSE_DESC		*nseTxDescMem;

    VXB_DMA_TAG_ID	nseMblkTag;

    VXB_DMA_MAP_ID      nseRxMblkMap[NSE_RX_DESC_CNT];
    VXB_DMA_MAP_ID      nseTxMblkMap[NSE_TX_DESC_CNT];

    M_BLK_ID		nseRxMblk[NSE_RX_DESC_CNT];
    M_BLK_ID		nseTxMblk[NSE_TX_DESC_CNT];

    UINT32		nseTxProd;
    UINT32		nseTxCons;
    UINT32		nseTxFree;
    UINT32		nseRxIdx;

    UINT8		nseEeWidth;
    SEM_ID		nseDevSem;
    spinlockIsr_t	nseLock;
    } NSE_DRV_CTRL;

#define NSE_BAR(p)   ((NSE_DRV_CTRL *)(p)->pDrvCtrl)->nseBar
#define NSE_HANDLE(p)   ((NSE_DRV_CTRL *)(p)->pDrvCtrl)->nseHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (NSE_HANDLE(pDev), (UINT32 *)((char *)NSE_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (NSE_HANDLE(pDev),                             \
        (UINT32 *)((char *)NSE_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCns8381xVxbEndh */
