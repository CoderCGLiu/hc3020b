/* fecVxbEnd.h - header file for Motorola/Freescale FEC VxBus END driver */

/*
 * Copyright (c) 2007-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,30jul09,pad  Now use CPU instead of CPU_FAMILY for PPC32.
01b,07jan08,wap  Update to support MCF52xx/MCF53xx FEC
01a,25jan07,wap  written
*/

#ifndef __INCfecVxbEndh
#define __INCfecVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void fecRegister (void);

METHOD_DECL(fecEnetEnable);
METHOD_DECL(fecEnetDisable);

#ifndef BSP_VERSION

#if (CPU == MCF5400)
#define FEC_STYLE_SLAVEDMA
#define FEC_STYLE_MCF54XXREG
#endif

#if (CPU == MCF5200) || (CPU == MCF5300)
#define FEC_STYLE_BUSMASTER
#define FEC_STYLE_MULTIINTR
#define FEC_STYLE_MCF52XXREG
#endif

#if (CPU == PPC32)
#define FEC_STYLE_BUSMASTER
#define FEC_STYLE_PPC60XREG
#endif

/* revision D.3 and greater processors require special FEC initialization */

#define REV_D_4 0x0502
#define REV_D_3 0x0501

/*
 * There are two sets of these, one for PPC and another for Coldfire.
 * Names are kept consistent whereever possible.
 */

#ifdef FEC_STYLE_MCF54XXREG

#define FEC_EVENT       0x0004  /* interrupt event register */
#define FEC_MASK        0x0008  /* interrupt mask register */
#define FEC_CTRL        0x0024  /* FEC control register */
#define FEC_MII_DATA    0x0040  /* MII data register */
#define FEC_MII_SPEED   0x0044  /* MII speed register */
#define FEC_MIB_CSR     0x0064  /* MII speed register */
#define FEC_RX_CTRL     0x0084  /* rx control register */
#define FEC_RX_HASH     0x0088  /* rx hash register */
#define FEC_TX_CTRL     0x00c4  /* tx control register */
#define FEC_ADDR_L      0x00e4  /* lower 32-bits of MAC address */
#define FEC_ADDR_H      0x00e8  /* upper 16-bits of MAC address */
#define FEC_OPC         0x00eC  /* Opcode pause duration reg address */
#define FEC_IAHASH_H    0x0118  /* upper 32-bits of IA hash table */
#define FEC_IAHASH_L    0x011c  /* lower 32-bits of IA hash table */
#define FEC_HASH_H      0x0120  /* upper 32-bits of group address */
#define FEC_HASH_L      0x0124  /* lower 32-bits of group address */
#define FEC_TFWR        0x0144  /* FEC Transmit FIFO Watermark */
#define FEC_RFDR        0x0184  /* FEC RX FIFO Data Register*/
#define FEC_RFSR        0x0188  /* FEC RX FIFO Status Register*/
#define FEC_RFCR        0x018C  /* FEC RX FIFO Control Register*/
#define FEC_RLRFP       0x0190  /* FEC RX FIFO Last Read Frame Ptr */
#define FEC_RLWFP       0x0194  /* FEC RX FIFO Last Write Frame Ptr*/
#define FEC_RFAR        0x0198  /* FEC RX FIFO Alarm Register*/
#define FEC_RFRP        0x019C  /* FEC RX FIFO Read Pointer Register*/
#define FEC_RFWP        0x01A0  /* FEC RX FIFO Write Pointer Register*/
#define FEC_TFDR        0x01A4  /* FEC TX FIFO Data Register*/
#define FEC_TFSR        0x01A8  /* FEC TX FIFO Status Register*/
#define FEC_TFCR        0x01AC  /* FEC TX FIFO Control Register*/
#define FEC_TLRFP       0x01B0  /* FEC TX FIFO Last Read Frame Ptr */
#define FEC_TLWFP       0x01B4  /* FEC TX FIFO Last Write Frame Ptr */
#define FEC_TFAR        0x01B8  /* FEC TX FIFO Alarm Register*/
#define FEC_TFRP        0x01BC  /* FEC TX FIFO Read Pointer Register*/
#define FEC_TFWP        0x01C0  /* FEC TX FIFO Write Pointer Register*/
#define FEC_FRST        0x01C4  /* FEC FIFO Reset Register*/
#define FEC_CTCWR       0x01C8  /* FEC CRC and Transmit Frame Control Register */
#define FEC_RX_ACT      0x018c  /* rx FIFO control register*/
#define FEC_TX_ACT      0x01ac  /* tx FIFO control register*/

/*
 * The Coldfire has RMON/IEEE stats registers. Each
 * register is 32 bits wide.
 */

#define FEC_RMON_TX_DROPS           0x0200
#define FEC_RMON_TX_UCASTS          0x0204
#define FEC_RMON_TX_BCASTS          0x0208
#define FEC_RMON_TX_MCASTS          0x020C
#define FEC_RMON_TX_CRCALGN         0x0210  /* CRC/alignment errors */
#define FEC_RMON_TX_RUNTS           0x0214
#define FEC_RMON_TX_GIANTS          0x0218
#define FEC_RMON_TX_FRAG            0x021C
#define FEC_RMON_TX_JABBER          0x0220
#define FEC_RMON_TX_COLLS           0x0224
#define FEC_RMON_TX_P64             0x0228
#define FEC_RMON_TX_P65TO128        0x022C
#define FEC_RMON_TX_P128TO255       0x0230
#define FEC_RMON_TX_P256TO511       0x0234
#define FEC_RMON_TX_P512TO1023      0x0238
#define FEC_RMON_TX_P1024TO2047     0x023C
#define FEC_RMON_TX_PGTE2048        0x0240
#define FEC_RMON_TX_OCTETS          0x0244
#define FEC_IEEE_TX_DROPS           0x0248
#define FEC_IEEE_TX_OK              0x024C
#define FEC_IEEE_TX_1COL            0x0250
#define FEC_IEEE_TX_MCOL            0x0254
#define FEC_IEEE_TX_DEFERED         0x0258
#define FEC_IEEE_TX_LATECOL         0x025C
#define FEC_IEEE_TX_EXCOL           0x0260
#define FEC_IEEE_TX_UFLOW           0x0264
#define FEC_IEEE_TX_CARRLOSS        0x0268
#define FEC_IEEE_TX_SQE             0x026C
#define FEC_IEEE_TX_FDXFC           0x0270
#define FEC_IEEE_TX_OCTETS_OK       0x0274

#define FEC_RMON_RX_DROPS           0x0280
#define FEC_RMON_RX_UCASTS          0x0284
#define FEC_RMON_RX_BCASTS          0x0288
#define FEC_RMON_RX_MCASTS          0x028C
#define FEC_RMON_RX_CRCALGN         0x0290  /* CRC/alignment errors */
#define FEC_RMON_RX_RUNTS           0x0294
#define FEC_RMON_RX_GIANTS          0x0298
#define FEC_RMON_RX_FRAG            0x029C
#define FEC_RMON_RX_JABBER          0x02A0
#define FEC_RMON_RX_RSVD            0x02A4
#define FEC_RMON_RX_P64             0x02A8
#define FEC_RMON_RX_P65TO128        0x02AC
#define FEC_RMON_RX_P128TO255       0x02B0
#define FEC_RMON_RX_P256TO511       0x02B4
#define FEC_RMON_RX_P512TO1023      0x02B8
#define FEC_RMON_RX_P1024TO2047     0x02BC
#define FEC_RMON_RX_PGTE2048        0x02C0
#define FEC_RMON_RX_OCTETS          0x02C4
#define FEC_IEEE_RX_DROPS           0x02C8
#define FEC_IEEE_RX_OK              0x02CC
#define FEC_IEEE_RX_CRC             0x02D0
#define FEC_IEEE_RX_ALIGN           0x02D4
#define FEC_IEEE_RX_OFLOW           0x02D8
#define FEC_IEEE_RX_FDXFC           0x02DC
#define FEC_IEEE_RX_OCTETS_OK       0x02E0

#endif

#ifdef FEC_STYLE_PPC60XREG

#define FEC_ADDR_L      0x0000  /* lower 32-bits of MAC address */
#define FEC_ADDR_H      0x0004  /* upper 16-bits of MAC address */
#define FEC_HASH_H      0x0008  /* upper 32-bits of hash table */
#define FEC_HASH_L      0x000C  /* lower 32-bits of hash table */
#define FEC_RX_START    0x0010  /* rx ring start address */
#define FEC_TX_START    0x0014  /* tx ring start address */
#define FEC_RX_BUF      0x0018  /* max rx buf length */
#define FEC_CTRL        0x0040  /* FEC control register */
#define FEC_EVENT       0x0044  /* interrupt event register */
#define FEC_MASK        0x0048  /* interrupt mask register */
#define FEC_VEC         0x004C  /* interrupt level/vector register */
#define FEC_RX_ACT      0x0050  /* rx ring has been updated */
#define FEC_TX_ACT      0x0054  /* tx ring has been updated */
#define FEC_MII_DATA    0x0080  /* MII data register */
#define FEC_MII_SPEED   0x0084  /* MII speed register */
#define FEC_RX_BOUND    0x00CC  /* rx fifo limit in the 860T ram */
#define FEC_RX_FIFO     0x00D0  /* rx fifo base in the 860T ram */
#define FEC_TFWR        0x00E4  /* tx fifo watermark */
#define FEC_TX_FIFO     0x00EC  /* tx fifo base in the 860T ram */
#define FEC_SDMA        0x0134  /* function code to SDMA */
#define FEC_RX_CTRL     0x0144  /* rx control register */
#define FEC_RX_FR       0x0148  /* max rx frame length */
#define FEC_TX_CTRL     0x0184  /* tx control register */

#endif

#ifdef FEC_STYLE_MCF52XXREG

#define FEC_EVENT       0x0004  /* interrupt event register */
#define FEC_MASK        0x0008  /* interrupt mask register */
#define FEC_RX_ACT      0x0010  /* rx ring has been updated */
#define FEC_TX_ACT      0x0014  /* tx ring has been updated */
#define FEC_CTRL        0x0024  /* FEC control register */
#define FEC_MII_DATA    0x0040  /* MII data register */
#define FEC_MII_SPEED   0x0044  /* MII speed register */
#define FEC_MIBCTL      0x0064  /* MIB control/status register */
#define FEC_RX_CTRL     0x0084  /* rx control register */
#define FEC_TX_CTRL     0x00C4  /* tx control register */
#define FEC_ADDR_L      0x00E4  /* lower 32-bits of MAC address */
#define FEC_ADDR_H      0x00E8  /* upper 16-bits of MAC address */
#define FEC_OPD         0x00EC  /* opcode/pause duration */
#define FEC_IAHASH_H    0x0118  /* upper 32-bits of IA hash table */
#define FEC_IAHASH_L    0x011C  /* lower 32-bits of IA hash table */
#define FEC_HASH_H      0x0120  /* upper 32-bits of group hash table */
#define FEC_HASH_L      0x0124  /* lower 32-bits of group hash table */
#define FEC_TFWR        0x0144  /* FEC Transmit FIFO Watermark */
#define FEC_RXF_BOUND   0x014C  /* FIFO Receive Bound */
#define FEC_RXF_START   0x0150  /* FIFO Receife FIFO Start */
#define FEC_RX_START    0x0180  /* rx ring start address */
#define FEC_TX_START    0x0184  /* tx ring start address */
#define FEC_RX_BUF      0x0188  /* max rx buf length */

#endif

/* Control/Status Registers (CSR) bit definitions */

#define FEC_RX_START_MSK    0xfffffffc      /* quad-word alignment */
                                                /* required for rx BDs */

#define FEC_TX_START_MSK    0xfffffffc      /* quad-word alignment */
                                                /* required for tx BDs */
/* Ethernet CSR bit definitions */

#define FEC_ETH_PINMUX      0x00000004      /* select FEC for port D pins */
#define FEC_ETH_EN          0x00000002      /* enable Ethernet operation */
#define FEC_ETH_DIS         0x00000000      /* disable Ethernet operation */
#define FEC_ETH_RES         0x00000001      /* reset the FEC */
#define FEC_CTRL_MASK       0x00000007      /* FEC control register mask */

/*
 * interrupt bits definitions: these are common to both the
 * mask and the event register.
 */

#define FEC_EVENT_HB        0x80000000      /* heartbeat error */
#define FEC_EVENT_BABR      0x40000000      /* babbling rx error */
#define FEC_EVENT_BABT      0x20000000      /* babbling tx error */
#define FEC_EVENT_GRA       0x10000000      /* graceful stop complete */
#define FEC_EVENT_TXF       0x08000000      /* tx frame */
#define FEC_EVENT_TXB       0x04000000      /* tx buffer */
#define FEC_EVENT_RXF       0x02000000      /* rx frame */
#define FEC_EVENT_RXB       0x01000000      /* rx buffer */
#define FEC_EVENT_MII       0x00800000      /* MII transfer */
#define FEC_EVENT_BERR      0x00400000      /* U-bus access error */
#define FEC_EVENT_LC        0x00200000      /* Late collision */
#define FEC_EVENT_RL        0x00100000      /* Collision retry limit */
#define FEC_EVENT_UN        0x00080000      /* TX underrun */
#define FEC_EVENT_MSK       0xffc00000      /* clear all interrupts */
#define FEC_MASK_ALL        FEC_EVENT_MSK    /* mask all interrupts */

/*
 * On the Coldfire 52xx/53xx, each FEC interrupt source gets its own
 * interrupt vector. The vector offsets are specified below.
 */

#define FEC_IVEC_TXF    0x0 /* frame sent */
#define FEC_IVEC_TXB    0x1 /* TX buffer processed */
#define FEC_IVEC_UN     0x2 /* TX underrun */
#define FEC_IVEC_RL     0x3 /* collision retry limit */
#define FEC_IVEC_RXF    0x4 /* frame received */
#define FEC_IVEC_RXB    0x5 /* receive buffer processed */
#define FEC_IVEC_MII    0x6 /* MDIO interrupt */
#define FEC_IVEC_LC     0x7 /* late collision */
#define FEC_IVEC_HB     0x8 /* heartbeat error */
#define FEC_IVEC_GRA    0x9 /* graceful stop complete */
#define FEC_IVEC_BERR   0xA /* bus error */
#define FEC_IVEC_BABT   0xB /* babbling transmit */
#define FEC_IVEC_BABR   0xC /* babbling receive */

/* bit masks for the interrupt level/vector CSR */

#define FEC_LVL_MSK         0xe0000000      /* intr level */
#define FEC_TYPE_MSK        0x0000000c      /* highest pending intr */
#define FEC_VEC_MSK         0xe000000c      /* this register mask */
#define FEC_RES_MSK         0x1ffffff3      /* reserved bits */
#define FEC_LVL_SHIFT       0x1d            /* intr level bits location */

/* transmit and receive active registers definitions */

#define FEC_TX_ACTIVE       0x01000000      /* tx active bit */
#define FEC_RX_ACTIVE       0x01000000      /* rx active bit */


/* bit settings for the transmit watermark register */
#define FEC_TFWR_X_WMRK_64  0x00000000      /* watermark = 64 bytes */
#define FEC_TFWR_X_WMRK_128 0x00000001      /* watermark = 128 bytes */
#define FEC_TFWR_X_WMRK_192 0x00000002      /* watermark = 192 bytes */
#define FEC_TFWR_X_WMRK_256 0x00000003      /* watermark = 256 bytes */
#define FEC_TFWR_X_WMRK_320 0x00000004      /* watermark = 320 bytes */
#define FEC_TFWR_X_WMRK_384 0x00000005      /* watermark = 384 bytes */
#define FEC_TFWR_X_WMRK_448 0x00000006      /* watermark = 448 bytes */
#define FEC_TFWR_X_WMRK_512 0x00000007      /* watermark = 512 bytes */

#define FEC_TFWR_X_WMRK_576 0x00000008      /* watermark = 576 bytes */
#define FEC_TFWR_X_WMRK_640 0x00000009      /* watermark = 640 bytes */
#define FEC_TFWR_X_WMRK_704 0x0000000a      /* watermark = 704 bytes */
#define FEC_TFWR_X_WMRK_768 0x0000000b      /* watermark = 768 bytes */
#define FEC_TFWR_X_WMRK_832 0x0000000c      /* watermark = 832 bytes */
#define FEC_TFWR_X_WMRK_896 0x0000000d      /* watermark = 896 bytes */
#define FEC_TFWR_X_WMRK_960 0x0000000e      /* watermark = 960 bytes */
#define FEC_TFWR_X_WMRK_1024 0x0000000f      /* watermark = 1024 bytes */

#if defined(FEC_STYLE_MCF54XXREG) || defined(FEC_STYLE_MCF52XXREG)
/* bit settings for the transmit FIFO alarm register */
#define FEC_TX_ALARM(x)       (((x)&0x000003FF)<<0)

/* bit settings for the transmit FIFO control register */
#define FEC_TFCR_TIMER      0x10000000
#define FEC_TFCR_WFR        0x20000000
#define FEC_TFCR_FRMEN      0x08000000
#define FEC_TFCR_IPMSK      0x00800000
#define FEC_TFCR_FAEMSK     0x00400000
#define FEC_TFCR_UFMSK      0x00100000
#define FEC_TFCR_OFMSK      0x00080000
#define FEC_TFCR_TXWMSK     0x00040000
#define FEC_TFCR_GR(x)      (((x)&0x00000007)<<24|0x00200000)

/* bit settings for the receive FIFO control register */
#define FEC_RFCR_TIMER      0x10000000
#define FEC_RFCR_FRMEN      0x08000000
#define FEC_RFCR_GR(x)      (((x)&0x00000007)<<24)
#define FEC_RFCR_IPWMSK     0x00800000
#define FEC_RFCR_FAEWMSK    0x00400000
#define FEC_RFCR_RXWMSK     0x00200000
#define FEC_RFCR_UFMSK      0x00100000
#define FEC_RFCR_OFMSK      0x00080000

/* bit settings for the receive FIFO alarm register */
#define FEC_RX_ALARM(x)       (((x)&0x000003FF)<<0)


/* bit settings for the CTCWR register */
#define FEC_CTCWR_TFCW      0x01000000
#define FEC_CTCWR_CRC       0x02000000

#define FEC_TYPE_MSK        0x0000000c      /* highest pending intr */
#define FEC_VEC_MSK         0xe000000c      /* this register mask */
#define FEC_RES_MSK         0x1ffffff3      /* reserved bits */
#define FEC_LVL_SHIFT       0x1d            /* intr level bits location */

#endif /* defined(FEC_STYLE_MCF54XXREG) || defined(FEC_STYLE_MCF52XXREG) */

/* MII management frame CSRs */

#define FEC_MII_ST          0x40000000      /* start of frame delimiter */
#define FEC_MII_OP_RD       0x20000000      /* perform a read operation */
#define FEC_MII_OP_WR       0x10000000      /* perform a write operation */
#define FEC_MII_ADDR_MSK    0x0f800000      /* PHY address field mask */
#define FEC_MII_REG_MSK     0x007c0000      /* PHY register field mask */
#define FEC_MII_TA          0x00020000      /* turnaround */
#define FEC_MII_DATA_MSK    0x0000ffff      /* PHY data field */
#define FEC_MII_RA_SHIFT    0x12            /* mii reg address bits */
#define FEC_MII_PA_SHIFT    0x17            /* mii PHY address bits */

#define FEC_MII_PRE_DIS     0x00000080      /* desable preamble */

#ifdef FEC_STYLE_MCF54XXREG

/*
 * For the Coldfire: MDCfreq = sys-freq/(4*MII_SPEED)
 * So MII_SPEED = sys-freq/(4*MDCfreq)
 * The max MDCfreq (compliant with IEEE MII spec) is 2.5 MHz
 * These are the recommended values of MII_SPEED for various
 * CPU frequencies. The calculation we use is
 * ((sysCpuSpeed()+9999999)/10000000)<<1
 */

#define FEC_MII_SPEED_25    (3 << 1)        /* recommended for 25Mhz CPU */
#define FEC_MII_SPEED_33    (4 << 1)        /* recommended for 33Mhz CPU */
#define FEC_MII_SPEED_50    (5 << 1)        /* recommended for 50Mhz CPU */
#define FEC_MII_SPEED_66    (7 << 1)        /* recommended for 66Mhz CPU */
#define FEC_MII_CPUSPEED   (((sysCpuSpeed()+9999999)/10000000)<<1)

IMPORT unsigned long sysCpuSpeed(void);

#else /* FEC_STYLE_MCF54XXREG */

#define FEC_MII_SPEED_25    0x00000005      /* recommended for 25Mhz CPU */
#define FEC_MII_SPEED_33    0x00000007      /* recommended for 33Mhz CPU */
#define FEC_MII_SPEED_40    0x00000008      /* recommended for 40Mhz CPU */
#define FEC_MII_SPEED_50    0x0000000a      /* recommended for 50Mhz CPU */
#define FEC_MII_SPEED_SHIFT 1               /* MII_SPEED bits location */
#define FEC_MII_CLOCK_MAX   2500000         /* max freq of MII clock (Hz) */

#endif /* FEC_STYLE_MCF54XXREG */

#define FEC_MII_MAN_DIS     0x00000000      /* disable the MII management */
                                                /* interface */
#define FEC_MII_SPEED_MSK   0xffffff81      /* speed field mask */

/* FIFO transmit and receive CSRs definitions */

#define FEC_FIFO_MSK        0x000003ff      /* FIFO rx/tx/bound mask */

/* SDMA function code CSR */

#define FEC_SDMA_DATA_BE    0x60000000      /* big-endian byte-ordering */
                                                /* for SDMA data transfer */

#define FEC_SDMA_DATA_PPC   0x20000000      /* PPC byte-ordering */
                                                /* for SDMA data transfer */

#define FEC_SDMA_DATA_RES   0x00000000      /* reserved value */

#define FEC_SDMA_BD_BE      0x18000000      /* big-endian byte-ordering */
                                                /* for SDMA BDs transfer */

#define FEC_SDMA_BD_PPC     0x08000000      /* PPC byte-ordering */
                                                /* for SDMA BDs transfer */
                                                /* for SDMA BDs transfer */


#define FEC_SDMA_BD_RES     0x00000000      /* reserved value */
#define FEC_SDMA_FUNC_0     0x00000000      /* no function code */

/* receive control/hash registers bit definitions */

#define FEC_RX_CTRL_RXLEN   0x07FF0000
#define FEC_RX_CTRL_FCE     0x00000020      /* flow control enable */
#define FEC_RX_CTRL_NOBROAD 0x00000010      /* broadcast reject */
#define FEC_RX_CTRL_PROM    0x00000008      /* promiscous mode */
#define FEC_RX_CTRL_MII     0x00000004      /* select MII interface */
#define FEC_RX_CTRL_DRT     0x00000002      /* disable rx on transmit */
#define FEC_RX_CTRL_LOOP    0x00000001      /* loopback mode */

#define FEC_RXLEN(x)    (((x) << 16) & FEC_RX_CTRL_RXLEN)

#ifdef FEC_STYLE_MCF54XXREG

#define FEC_RX_FR_MSK       0x07ff0000      /* rx frame length mask */
#define FEC_RX_FCE      0x00000020      /* rx flow control enable */

#else /* FEC_STYLE_MCF54XXREG */

#define FEC_RX_FR_MSK       0x000007ff      /* rx frame length mask */

#endif /* FEC_STYLE_MCF54XXREG */

/* transmit control register bit definitions */

#define FEC_TX_CTRL_FD      0x00000004      /* enable full duplex mode */
#define FEC_TX_CTRL_HBC     0x00000002      /* HB check is performed */
#define FEC_TX_CTRL_GRA     0x00000001      /* issue a graceful tx stop */

/* rx/tx buffer descriptors definitions */

typedef struct fec_desc
    {
    volatile UINT16	bdSts;
    volatile UINT16	bdLen;
    volatile UINT32	bdPtr;
    } FEC_DESC;


/* TBD bits definitions */

#define FEC_TBD_RDY         0x8000          /* ready for transmission */
#define FEC_TBD_TO1         0x4000          /* transmit ownership bit 1 */
#define FEC_TBD_WRAP        0x2000          /* look at CSR5 for next bd */
#define FEC_TBD_TO2         0x1000          /* transmit ownership bit 2 */
#define FEC_TBD_INTERRUPT   0x1000          /* interrupt on transmit MCF547x/8x only */
#define FEC_TBD_LAST        0x0800          /* last bd in this frame */
#define FEC_TBD_CRC         0x0400          /* transmit the CRC sequence */
#define FEC_TBD_DEF         0x0200          /* deferred transmission */
#define FEC_TBD_HB          0x0100          /* heartbeat error */
#define FEC_TBD_LC          0x0080          /* late collision */
#define FEC_TBD_RL          0x0040          /* retransmission limit */
#define FEC_TBD_UN          0x0002          /* underrun error */
#define FEC_TBD_CSL         0x0001          /* carrier sense lost */
#define FEC_TBD_RC_MASK     0x003c          /* retransmission count mask */

/* RBD bits definitions */

#define FEC_RBD_EMPTY       0x8000          /* ready for reception */
#define FEC_RBD_RO1         0x4000          /* receive ownership bit 1 */
#define FEC_RBD_WRAP        0x2000          /* look at CSR4 for next bd */
#define FEC_RBD_RO2         0x1000          /* receive ownership bit 2 */
#define FEC_RBD_INTERRUPT   0x1000          /* interrupt on receive MCF547x/8x only */
#define FEC_RBD_LAST        0x0800          /* last bd in this frame */
#define FEC_RBD_RES1        0x0400          /* reserved bit 1 */
#define FEC_RBD_RES2        0x0200          /* reserved bit 2 */
#define FEC_RBD_MISS        0x0100          /* address recognition miss */
#define FEC_RBD_BC          0x0080          /* broadcast frame */
#define FEC_RBD_MC          0x0040          /* multicast frame */
#define FEC_RBD_LG          0x0020          /* frame length violation */
#define FEC_RBD_NO          0x0010          /* nonoctet aligned frame */
#define FEC_RBD_SH          0x0008          /* short frame error */
                                                /* not supported by the 860T */
#define FEC_RBD_CRC         0x0004          /* CRC error */
#define FEC_RBD_OV          0x0002          /* overrun error */
#define FEC_RBD_TR          0x0001          /* truncated frame (>2KB) */

#define FEC_RBD_ERR					\
    (FEC_RBD_LG|FEC_RBD_NO|FEC_RBD_CRC|	\
     FEC_RBD_OV|FEC_RBD_TR)


#define FEC_HASH_MASK       0x7c000000      /* bits 27-31 */
#define FEC_HASH_SHIFT      0x1a            /* to get the index */


#ifdef FEC_STYLE_SLAVEDMA

/* FIFO reset register bit definitions */

#define FEC_FIFO_SW_RST     0x02000000
#define FEC_FIFO_RST_CTL    0x01000000

/*
 * DMA Task Priorities
 */
#define FEC0RX_DMA_PRI      5
#define FEC1RX_DMA_PRI      5
#define FEC0TX_DMA_PRI      6
#define FEC1TX_DMA_PRI      6

#endif /* FEC_STYLE_SLAVEDMA */

#define FEC_MTU 1500
#define FEC_JUMBO_MTU 9000
#define FEC_CLSIZE	1536
#define FEC_NAME	"motfec"
#define FEC_TIMEOUT 10000
#define FEC_INTRS (FEC_RXINTRS|FEC_TXINTRS)
#ifdef FEC_STYLE_BUSMASTER
#define FEC_RXINTRS (FEC_EVENT_RXF|FEC_EVENT_RXB|FEC_EVENT_BABR)
#define FEC_TXINTRS (FEC_EVENT_TXF|FEC_EVENT_TXB|FEC_EVENT_BABT)
#else
#define FEC_RXINTRS (FEC_EVENT_BABR)
#define FEC_TXINTRS (FEC_EVENT_BABT)
#endif

#define FEC_RX_DESC_CNT 64
#define FEC_TX_DESC_CNT 64

#define FEC_INC_DESC(x, y)      (x) = ((x + 1) & (y - 1))
#define FEC_MAXFRAG 16
#define FEC_MAX_RX 16

#ifdef FEC_STYLE_MCF52XXREG
#define FEC_ALIGN(x)    (char *)(((UINT32)(x) + 15) & ~15)

#define FEC_ADJ(x)      \
        (x)->m_data = FEC_ALIGN((x)->m_data)
#else
#define FEC_ADJ(x)
#endif

#ifdef FEC_STYLE_SLAVEDMA
#define FEC_RX_DMA_SIZE 2048
/*
 * DMA Task Priorities
 */
#define FEC0RX_DMA_PRI      5
#define FEC1RX_DMA_PRI      5
#define FECRX_DMA_PRI(x)    ((x == 0) ? FEC0RX_DMA_PRI : FEC1RX_DMA_PRI)
#define FEC0TX_DMA_PRI      6
#define FEC1TX_DMA_PRI      6
#define FECTX_DMA_PRI(x)    ((x == 0) ? FEC0TX_DMA_PRI : FEC1TX_DMA_PRI)
#endif

/*
 * Private adapter context structure.
 */

typedef struct fec_drv_ctrl
    {
    END_OBJ		fecEndObj;
    VXB_DEVICE_ID	fecDev;
    void *		fecMuxDevCookie;
    void *		fecBar;
    void *		fecHandle;

    JOB_QUEUE_ID	fecJobQueue;
    QJOB		fecIntJob;
    atomic_t		fecIntPending;

    QJOB		fecRxJob;
    atomic_t		fecRxPending;

    QJOB		fecTxJob;
    atomic_t		fecTxPending;
    UINT8		fecTxCur;
    UINT8		fecTxLast;
    volatile BOOL	fecTxStall;
    UINT16		fecTxThresh;

    BOOL		fecPolling;
    M_BLK_ID		fecPollBuf;
    UINT32		fecIntMask;
    UINT32		fecIntrs;

    UINT8		fecAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	fecCaps;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*fecMediaList;
    END_ERR		fecLastError;
    UINT32		fecCurMedia;
    UINT32		fecCurStatus;
    VXB_DEVICE_ID	fecMiiBus;
    VXB_DEVICE_ID	fecMiiDev;
    FUNCPTR		fecMiiPhyRead;
    FUNCPTR		fecMiiPhyWrite;
    int			fecMiiPhyAddr;
    /* End MII/ifmedia required fields */

    /* DMA tags and maps. */

    VXB_DMA_TAG_ID	fecParentTag;

    VXB_DMA_TAG_ID	fecRxDescTag;
    VXB_DMA_MAP_ID	fecRxDescMap;
    FEC_DESC *		fecRxDescMem;

    VXB_DMA_TAG_ID	fecTxDescTag;
    VXB_DMA_MAP_ID	fecTxDescMap;
    FEC_DESC *		fecTxDescMem;

#ifdef FEC_STYLE_SLAVEDMA
    VXB_DMA_RESOURCE_ID	fecRxChan;
    VXB_DMA_RESOURCE_ID	fecTxChan;
#endif

    VXB_DMA_TAG_ID	fecMblkTag;

    VXB_DMA_MAP_ID	fecRxMblkMap[FEC_RX_DESC_CNT];
    VXB_DMA_MAP_ID	fecTxMblkMap[FEC_TX_DESC_CNT];

    M_BLK_ID		fecRxMblk[FEC_RX_DESC_CNT];
    M_BLK_ID		fecTxMblk[FEC_TX_DESC_CNT];

    UINT32		fecTxProd;
    UINT32		fecTxCons;
    UINT32		fecTxFree;
    UINT32		fecRxIdx;

    SEM_ID		fecDevSem;

    int			fecMaxMtu;
#ifdef FEC_STYLE_PPC60XREG
    int			fecPinMux;
#endif

    } FEC_DRV_CTRL;

#define FEC_BAR(p)   ((FEC_DRV_CTRL *)(p)->pDrvCtrl)->fecBar
#define FEC_HANDLE(p)   ((FEC_DRV_CTRL *)(p)->pDrvCtrl)->fecHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FEC_HANDLE(pDev), (UINT32 *)((char *)FEC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FEC_HANDLE(pDev),                             \
        (UINT32 *)((char *)FEC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCfecVxbEndh */
