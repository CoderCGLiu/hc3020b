/* fccVxbEnd.h - header file for fcc VxBus END driver */

/*
 * Copyright (c) 2006-2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,14nov07,wap  Increase number of RX and TX descriptors, increase
                 RX processing limit (WIND00104927)
01e,23aug07,wap  Remove use of vxbDmaBufLib (WIND00101790)
01d,16jul07,wap  Convert to new register access API.
01c,05dec06,wap  Allocate private DPRAM for TX pad buffer
01b,29aug06,wap  Update PHY handling scheme
01a,14apr06,wap  written
*/

#ifndef __INCfccVxbEndh
#define __INCfccVxbEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void fccRegister (void);

METHOD_DECL(fccEnetEnable);
METHOD_DECL(fccEnetDisable);

#ifndef BSP_VERSION

#define FCC_CLSIZE	1536
#define FCC_NAME	"motfcc"
#define FCC_TIMEOUT	10000
#define FCC_INTRS	\
	(M8260_FEM_ETH_BSY|M8260_FEM_ETH_RXF|M8260_FEM_ETH_RXC|	\
	M8260_FEM_ETH_TXB|M8260_FEM_ETH_TXC|M8260_FEM_ETH_TXE)

#define FCC_RXINTRS (M8260_FEM_ETH_BSY|M8260_FEM_ETH_RXF|M8260_FEM_ETH_RXC)
#define FCC_TXINTRS (M8260_FEM_ETH_TXB|M8260_FEM_ETH_TXC|M8260_FEM_ETH_TXE)

/*#define FCC_ADJ(x)	m_adj(x, 2)*/
#define FCC_ADJ(x)	(x)->m_data += 2

#define FCC_INC_DESC(x, y)	(x) = ((x + 1) & (y - 1))
/*#define FCC_INC_DESC(x, y)	(x) = (((x) + 1) % y)*/
#define FCC_MAXFRAG		16
#define FCC_MAX_RX		32

#define FCC_RX_DESC_CNT		64
#define FCC_TX_DESC_CNT		64

typedef struct fcc_desc
    {
    volatile UINT16	bdSts;
    volatile UINT16	bdLen;
    volatile UINT32	bdAddr;
    } FCC_DESC;

/*
 * Private adapter context structure.
 */

typedef struct fcc_drv_ctrl
    {
    END_OBJ		fccEndObj;
    VXB_DEVICE_ID	fccDev;
    void *		fccHandle;
    void		*fccMuxDevCookie;

    JOB_QUEUE_ID	fccJobQueue;
    QJOB		fccIntJob;
    volatile BOOL	fccIntPending;

    QJOB		fccRxJob;
    volatile BOOL	fccRxPending;

    QJOB		fccTxJob;
    volatile BOOL	fccTxPending;
    UINT8		fccTxCur;
    UINT8		fccTxLast;
    volatile BOOL	fccTxStall;
    UINT16		fccTxThresh;

    QJOB		fccLinkJob;
    volatile BOOL	fccLinkPending;

    BOOL		fccPolling;
    M_BLK_ID		fccPollBuf;
    UINT16		fccIntMask;

    UINT8		fccAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES	fccCaps;

    END_IFDRVCONF	fccEndStatsConf;
    END_IFCOUNTERS	fccEndStatsCounters;
    UINT32		fccInErrors;
    UINT32		fccInDiscards;
    UINT32		fccInUcasts;
    UINT32		fccInMcasts;
    UINT32		fccInBcasts;
    UINT32		fccInOctets;
    UINT32		fccOutErrors;
    UINT32		fccOutUcasts;
    UINT32		fccOutMcasts;
    UINT32		fccOutBcasts;
    UINT32		fccOutOctets;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST	*fccMediaList;
    END_ERR		fccLastError;
    UINT32		fccCurMedia;
    UINT32		fccCurStatus;
    VXB_DEVICE_ID	fccMiiBus;
    VXB_DEVICE_ID	fccMiiDev;
    FUNCPTR		fccMiiPhyRead;
    FUNCPTR		fccMiiPhyWrite;
    int			fccMiiPhyAddr;
    /* End MII/ifmedia required fields */

    FCC_DESC		*fccRxDescMem;

    FCC_DESC		*fccTxDescMem;

    M_BLK_ID		fccRxMblk[FCC_RX_DESC_CNT];
    M_BLK_ID		fccTxMblk[FCC_TX_DESC_CNT];

    UINT32		fccTxProd;
    UINT32		fccTxCons;
    UINT32		fccTxFree;
    UINT32		fccRxIdx;

    SEM_ID		fccMiiSem;
    int			fccNum;

    void *		fccRxDpram;
    void *		fccTxDpram;
    void *		fccPadDpram;

    } FCC_DRV_CTRL;

#define FCC_HANDLE(p)   ((FCC_DRV_CTRL *)(p)->pDrvCtrl)->fccHandle
#define FCC_BAR(p, ix)   (p)->pRegBase[ix]

#define CSR_READ_4(pDev, addr, ix)			\
    vxbRead32 (FCC_HANDLE(pDev), (UINT32 *)((char *)FCC_BAR(pDev, ix) + addr))

#define CSR_WRITE_4(pDev, addr, ix, data)		\
    vxbWrite32 (FCC_HANDLE(pDev),			\
        (UINT32 *)((char *)FCC_BAR(pDev, ix) + addr), data)

#define CSR_READ_2(pDev, addr, ix)			\
    vxbRead16 (FCC_HANDLE(pDev), (UINT16 *)((char *)FCC_BAR(pDev, ix) + addr))

#define CSR_WRITE_2(pDev, addr, ix, data)		\
    vxbWrite16 (FCC_HANDLE(pDev),			\
        (UINT16 *)((char *)FCC_BAR(pDev, ix) + addr), data)

#define CSR_READ_1(pDev, addr, ix)			\
    vxbRead8 (FCC_HANDLE(pDev), (UINT8 *)((char *)FCC_BAR(pDev, ix) + addr))

#define CSR_WRITE_1(pDev, addr, ix, data)		\
    vxbWrite8 (FCC_HANDLE(pDev),			\
        (UINT8 *)((char *)FCC_BAR(pDev, ix) + addr), data)

#define FCC_PRAM 0
#define FCC_IRAM 1

#define CSR_SETBIT_1(pDev, offset, ix, val)          \
        CSR_WRITE_1(pDev, offset, ix, CSR_READ_1(pDev, offset, ix) | (val))

#define CSR_CLRBIT_1(pDev, offset, ix, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset, ix) & ~(val))

#define CSR_SETBIT_2(pDev, offset, ix, val)          \
        CSR_WRITE_2(pDev, offset, ix, CSR_READ_2(pDev, offset, ix) | (val))

#define CSR_CLRBIT_2(pDev, offset, ix, val)          \
        CSR_WRITE_2(pDev, offset, ix, CSR_READ_2(pDev, offset, ix) & ~(val))

#define CSR_SETBIT_4(pDev, offset, ix, val)          \
        CSR_WRITE_4(pDev, offset, ix, CSR_READ_4(pDev, offset, ix) | (val))

#define CSR_CLRBIT_4(pDev, offset, ix, val)          \
        CSR_WRITE_4(pDev, offset, ix, CSR_READ_4(pDev, offset, ix) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCfccVxbEndh */
