/* vxbOcteonMgmtEnd.h - header file for mgmt VxBus ENd driver */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,11oct10,d_c  Move from cav_cn56xx_mipsi64r2sf BSP.
                 NOT YET BUILT - still requires removal of
                 direct SDK references.
01a,18jan10,l_z  written
*/

#ifndef __INCvxbOcteonMgmtEndh
#define __INCvxbOcteonMgmtEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbOcteonMgmtEndRegister (void);

#define MGMT_MTU       1522
#define MGMT_TUPLE_CNT 1024
#define MGMT_NAME      "mgmt"
#define MGMT_TIMEOUT   10000

#define MGMT_RING_ENTRY_TX_CNT        128
#define MGMT_RING_ENTRY_RX_CNT        128
#define MGMT_MAX_RX                   32
#define MGMT_INC_DESC(x, y)           (x) = (((x) + 1) % y)
#define MGMT_INPUT_RING_ENTRY_DONE    0xF

#define MGMT_ADJ(x)            (x)->m_data += 2

/*  Format of the TX/RX ring buffer entries */

/* Copied from SDK 3.1.1-p1-547 : cvmx-mgmt-port.c */

typedef union
{
    uint64_t u64;
    struct
    {
        volatile uint64_t reserved_62_63     :2;
        volatile uint64_t len                :14;    /* Length of the buffer/packet in bytes */
        volatile uint64_t tstamp             :1;     /* For TX, signals that the packet should be timestamped */
        volatile uint64_t code               :7;     /* The RX error code */
        volatile uint64_t addr               :40;    /* Physical address of the buffer */
    } s;
    struct cn78xx_tx_ring_entry
    {
        volatile uint64_t len                :14;    /* Length of the buffer/packet in bytes */
        volatile uint64_t tstamp             :1;     /* For TX, signals that the packet should be timestamped */
        volatile uint64_t reserved_48_42     :7;
        volatile uint64_t addr               :42;    /* Physical address of the buffer */
    } cn78xx_tx;
    struct cn78xx_rx_ring_entry
    {
        volatile uint64_t len                :14;    /* Length of the buffer/packet in bytes */
        volatile uint64_t code               :6;     /* The RX error code */
        volatile uint64_t reserved_43_42     :2;
        volatile uint64_t addr               :42;    /* Physical address of the buffer */
    } cn78xx_rx;
} MGMT_RING_ENTRY;

/*
 * Private adapter context structure.
 */

typedef struct mgmt_drv_ctrl
    {
    END_OBJ             mgmtEndObj;
    VXB_DEVICE_ID       mgmtDev;
    void *              mgmtMuxDevCookie;
    int                 mgmtUnit;

    MGMT_RING_ENTRY*    mgmtRingTxBase;
    MGMT_RING_ENTRY*    mgmtRingRxBase;

    UINT32              mgmtTxProd;
    UINT32              mgmtTxCons;
    UINT32              mgmtTxFree;
    UINT32              mgmtRxIdx;

    M_BLK_ID            mgmtRxMblk[MGMT_RING_ENTRY_TX_CNT];
    M_BLK_ID            mgmtTxMblk[MGMT_RING_ENTRY_RX_CNT];

    JOB_QUEUE_ID        mgmtJobQueue;

    QJOB                mgmtIntJob;
    atomicVal_t         mgmtIntPending;
    
    QJOB                mgmtRxJob;
    atomicVal_t         mgmtRxPending;

    QJOB                mgmtTxJob;
    atomicVal_t         mgmtTxPending;
    volatile BOOL       mgmtTxStall;

    BOOL                mgmtPolling;
    M_BLK_ID            mgmtPollBuf;

    UINT8               mgmtAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    mgmtCaps;
    END_IFDRVCONF       mgmtEndStatsConf;
    END_IFCOUNTERS      mgmtEndStats;

    UINT32              mgmtInErrors;
    UINT32              mgmtInDiscards;
    UINT32              mgmtInUcasts;
    UINT32              mgmtInMcasts;
    UINT32              mgmtInBcasts;
    UINT32              mgmtInOctets;
    UINT32              mgmtOutErrors;
    UINT32              mgmtOutUcasts;
    UINT32              mgmtOutMcasts;
    UINT32              mgmtOutBcasts;
    UINT32              mgmtOutOctets;

    SEM_ID              mgmtDevSem;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *     mgmtMediaList;
    END_ERR             mgmtLastError;
    UINT32              mgmtCurMedia;
    UINT32              mgmtCurStatus;
    VXB_DEVICE_ID       mgmtMiiBus;
    VXB_DEVICE_ID       mgmtMiiDev;
    FUNCPTR             mgmtMiiPhyRead;
    FUNCPTR             mgmtMiiPhyWrite;
    int                 mgmtMiiPhyAddr;
    /* End MII/ifmedia required fields */

    int                 mgmtMaxMtu;
    int                 mgmtIpdPortNum;
    int                 mgmtInterface;
    } MGMT_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbOcteonMgmtEndh */
