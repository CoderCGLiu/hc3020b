/* vxbFslEspi.h - Freescale ESPI hardware defintions */

/*
 * Copyright (c) 2012, 2014-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
20may15,p_x add SPI_RCV_WAIT.
04nov14,wyt  add SPI_DONE_WAIT. (VXW6-83608)
01a,22oct12,y_y  created
*/

#ifndef __INCvxbFslEspih
#define __INCvxbFslEspih

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef BSP_VERSION

/* defines */

#define FSL_ESPI_NAME          "fslEspi"

/* Freescale ESPI registers defines */

/* Register Addresses */

#define SPMODE                  0x00   /* eSPI mode register */
#define SPIE                    0x04   /* eSPI event register */
#define SPIM                    0x08   /* eSPI mask register */
#define SPCOM                   0x0C   /* eSPI command register */
#define SPITF                   0x10   /* eSPI transmit FIFO access register */
#define SPIRF                   0x14   /* eSPI receive FIFO access register */
#define SPMODE0                 0x20   /* eSPI CS0 mode register */
#define SPMODE1                 0x24   /* eSPI CS1 mode register */
#define SPMODE2                 0x28   /* eSPI CS2 mode register */
#define SPMODE3                 0x2C   /* eSPI CS3 mode register */

/* SPI command register description */

#define SPI_EN                  ((UINT32)0x01 << (31))
#define SPCOM_TO                0x08000000  /* Transmit only mode */
#define SPCOM_CS_SHIFT          30
#define SPCOM_RX_SKIP_SHIFT     16
#define SPMODE_TXTHR_SHIFT      8
#define SPMODE_RXTHR_SHIFT      0

/* SPI event register description */

#define SPI_EVENT_CLR           0xFFFFFFFF
#define SPIE_RXCNT_MASK         0x3F000000  /* Number of full Rx FIFO bytes */
#define SPIE_TXCNT_MASK         0x003F0000  /* Number of free Tx FIFO bytes */
#define SPIE_TXE                0x00008000  /* Tx FIFO is empty */
#define SPIE_DON                0x00004000  /* Last character was transmitted */
#define SPIE_RXT                0x00002000  /* Rx FIFO has more than RXTHR bytes */
#define SPIE_RXF                0x00001000  /* Rx FIFO is full */
#define SPIE_TXT                0x00000800  /* Tx FIFO has less than TXTHR bytes */
#define SPIE_RNE                0x00000200  /* Rx FIFO not empty */
#define SPIE_TNF                0x00000100  /* Tx FIFO not full */
#define SPIE_RXCNT_SHIFT        24
#define SPIE_TXCNT_SHIFT        16
#define SPIE_RXCNT(x)          ((x >> SPIE_RXCNT_SHIFT) & 0x3F)
#define SPIE_TXCNT(x)          ((x >> SPIE_TXCNT_SHIFT) & 0x3F)

/* SPI mask register description */

#define SPIM_TXE                0x00008000  /* Tx FIFO is empty */
#define SPIM_DON                0x00004000  /* Last character was transmitted */
#define SPIM_RXT                0x00002000  /* Rx FIFO has more than RXTHR bytes */
#define SPIM_RXF                0x00001000  /* Rx FIFO is full */
#define SPIM_TXT                0x00000800  /* Tx FIFO has less than TXTHR bytes */
#define SPIM_RNE                0x00000200  /* Rx FIFO not empty */
#define SPIM_TNF                0x00000100  /* Tx FIFO not full */

/* SPI chip-select mode register description */

#define SPI_CSMODE_CSCG(x)     ((x) << 3)   /* Clock gap */
#define SPI_CSMODE_CSAFT(x)    ((x) << 8)   /* after clock finishes toggling */
#define SPI_CSMODE_CSBEF(x)    ((x) << 12)  /* before clock toggles */
#define SPI_CSMODE_LEN(x)      ((x - 1) << 16)  /* Character length in bits per character */
#define SPI_CSMODE_POL(x)      ((x) << 20)  /* CS0 Polarity */
#define SPI_CSMODE_REV(x)      ((x) << 29)  /* Reverse data mode */
#define SPI_CSMODE_CP(x)       ((x) << 30)  /* Clock phase */
#define SPI_CSMODE_PM(x)       ((x) << 24)
#define SPI_CSMODE_CI(x)       ((UINT32)(x) << 31)

/* Mask */

#define SPIMODE_PM_MASK          0xe07fffff
#define SPIMODE_CS_MASK          0xc0000000
#define SPIMODE_CICP_MASK        0xc0000000
#define SPIMODE_LEN_MASK         0x000f0000
#define SPIMODE_REV_MASK         0x20000000
#define SPIMODE_CSPOL_MASK       0x00100000
#define SPICSMODE_DIV16          0x10000000
#define SPICSMODE_ODD            0x00800000

/* Number of chip selects supported */

#define ESPI_MAX_CS_NUM          4

/* Device default freq */

#define DEFAULT_BUS_FREQ         20000000

/* The max translen */

#define MAX_TRANLEN              0xffff

/* Rx FIFO threshold */

#define ESPI_RX_FIFO_THRESHOLD   16

/* Tx FIFO threshold */

#define ESPI_TX_FIFO_THRESHOLD   16

/* microseconds to wait for SPI transfer done */

#define SPI_TX_WAIT              1

/* seconds to wait for SD/MMC command or data done */

#define SPI_CMD_WAIT_IN_SECS      10

#define SPI_DONE_WAIT             10000

#define SPI_RCV_WAIT             10000

/* ESPI controllor read and write interface */

#undef CSR_READ_1
#undef CSR_WRITE_1

#define FSL_ESPI_BAR(p)       ((char *)((p)->pRegBase[0]))
#define FSL_ESPI_HANDLE(p)    (((FSL_ESPI_CTRL *)(p)->pDrvCtrl)->fslEspiHandle)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (FSL_ESPI_HANDLE(pDev), (UINT8 *)((char *)FSL_ESPI_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)               \
    vxbWrite8 (FSL_ESPI_HANDLE(pDev),                \
        (UINT8 *)((char *)FSL_ESPI_BAR(pDev) + addr), data)

#define CSR_CLRBIT_1(pDev, offset, val)             \
    CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & (val))

#define CSR_SETBIT_1(pDev, offset, val)             \
    CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_SETCLRBIT_1(pDev, offset, val1, val2)          \
        CSR_WRITE_1(pDev, offset, (CSR_READ_1(pDev, offset) & val1) | val2)

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (FSL_ESPI_HANDLE(pDev),               \
        (UINT16 *)((char *)FSL_ESPI_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)               \
    vxbWrite16 (FSL_ESPI_HANDLE(pDev),              \
        (UINT16 *)((char *)FSL_ESPI_BAR(pDev) + addr), data)

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FSL_ESPI_HANDLE(pDev),               \
        (UINT32 *)((char *)FSL_ESPI_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)               \
    vxbWrite32 (FSL_ESPI_HANDLE(pDev),              \
        (UINT32 *)((char *)FSL_ESPI_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)             \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)             \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (val))

#define CSR_SETCLRBIT_4(pDev, offset, val1, val2)          \
        CSR_WRITE_4(pDev, offset, (CSR_READ_4(pDev, offset) & val1) | val2)

/* typedefs */

/* structure holding the instance specific details */

typedef struct fsl_espi_drv_ctrl
    {
    VXB_DEVICE_ID       pDev;
    void *              fslEspiHandle;
    UINT32              clkFrequency;
    UINT32              devFreq;
    UINT32              actualSpeed;
    UINT8               chipSelect;
    UINT8 *             txBuf;
    UINT8 *             rxBuf;
    UINT16              rxLeft;
    UINT16              txLeft;
    UINT8               initPhase;
    UINT8               numOfcs;
    BOOL                polling;
    SEM_ID              syncSem;
    SPI_DEV_INFO        devInfo;
    VXB_SPI_MAST_SPEC   specialInfo;
    VXB_SPI_BUS_CTRL    vxbSpiCtrl;
    } FSL_ESPI_CTRL;

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFslEspih */

