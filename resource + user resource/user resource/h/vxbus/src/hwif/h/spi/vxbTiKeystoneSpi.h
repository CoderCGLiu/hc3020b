/* vxbTiKeystoneSpi.h - TI KeyStone SPI hardware defintions */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,18aug13,sye  Written.
*/

#ifndef __INCvxbTiKeystoneSpih
#define __INCvxbTiKeystoneSpih

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/* defines */

#define TI_KEYSTONE_SPI_NAME            "tiKeystoneSpi"

/* register addresses */

#define KEYSTONE_SPIGCR0                (0x0)
#define KEYSTONE_SPIGCR1                (0x4)
#define KEYSTONE_SPIINT0                (0x8)
#define KEYSTONE_SPILVL                 (0xc)
#define KEYSTONE_SPIFLG                 (0x10)
#define KEYSTONE_SPIPC0                 (0x14)
#define KEYSTONE_SPIDAT0                (0x38)
#define KEYSTONE_SPIDAT1                (0x3c)
#define KEYSTONE_SPIBUF                 (0x40)
#define KEYSTONE_SPIEMU                 (0x44)
#define KEYSTONE_SPIDELAY               (0x48)
#define KEYSTONE_SPIDEF                 (0x4c)
#define KEYSTONE_SPIFMT0                (0x50)
#define KEYSTONE_SPIFMT1                (0x54)
#define KEYSTONE_SPIFMT2                (0x58)
#define KEYSTONE_SPIFMT3                (0x5c)
#define KEYSTONE_INTVEC0                (0x60)
#define KEYSTONE_INTVEC1                (0x64)

#define KEYSTONE_SPIFMT(chan)           (KEYSTONE_SPIFMT0 + 4 * (chan))
#define KEYSTONE_DELAY_CFG(delay)       ((delay) & 0xff)

#undef BIT
#define BIT(x)                          ((UINT32)0x1 << (x))

/* SPIGCR0 */

#define SPIGCR0_SPIRST_DIS              (0x1)
#define SPIGCR0_SPIRST_ENA              (0x0)

/* SPIGCR1 */

#define SPIGCR1_SPIENA                  BIT(24)
#define SPIGCR1_LOOPBACK                BIT(16)
#define SPIGCR1_POWERDOWN               BIT(8)
#define SPIGCR1_CLKMOD                  BIT(1)
#define SPIGCR1_MASTER                  BIT(0)

/* SPIINT0 */

#define SPIINT0_DMAREQEN                BIT(16)
#define SPIINT0_TXINTENA                BIT(9)
#define SPIINT0_RXINTENA                BIT(8)
#define SPIINT0_OVRNINTENA              BIT(6)
#define SPIINT0_BITERRENA               BIT(4)
#define SPIINT0_DEFAULTS                (SPIINT0_RXINTENA       | \
                                         SPIINT0_OVRNINTENA     | \
                                         SPIINT0_BITERRENA)

/* SPILVL */

#define SPILVL_TXINTLVL_INT1            BIT(9)
#define SPILVL_RXINTLVL_INT1            BIT(8)
#define SPILVL_OVRNINTLVL_INT1          BIT(6)
#define SPILVL_BITERRLVL_INT1           BIT(4)

#define SPILVL_INT1_DEFAULTS            (SPILVL_TXINTLVL_INT1   | \
                                         SPILVL_RXINTLVL_INT1   | \
                                         SPILVL_OVRNINTLVL_INT1 | \
                                         SPILVL_BITERRLVL_INT1)

#define SPILVL_INT0_DEFAULTS            (0)

/* SPIFLG */

#define SPIFLG_TXINTFLG                 BIT(9)
#define SPIFLG_RXINTFLG                 BIT(8)
#define SPIFLG_OVRNINTFLG               BIT(6)
#define SPIFLG_BITERRFLG                BIT(4)
#define SPIFLG_ERRORS                   (SPIFLG_OVRNINTFLG | SPIFLG_BITERRFLG)
#define SPIFLG_EVENTS                   (SPIFLG_TXINTFLG | \
                                         SPIFLG_RXINTFLG | \
                                         SPIFLG_ERRORS)

/* SPIPC0 */

#define SPIPC0_SOMIFUN                  BIT(11)
#define SPIPC0_SIMOFUN                  BIT(10)
#define SPIPC0_CLKFUN                   BIT(9)
#define SPIPC0_SCSFUN(chan)             BIT(chan)

/* SPIDAT0 */

#define SPIDAT0_TXDATA(data)            ((data) & 0xffff)

/* SPIDAT1 */

#define SPIDAT1_CSHOLD                  BIT(28)
#define SPIDAT1_WDEL                    BIT(26)
#define SPIDAT1_DFSEL_OFFSET            (24)
#define SPIDAT1_CSNR_OFFSET             (16)
#define SPIDAT1_TXDATA_MASK             (0xffff0000)

/* SPIBUF */

#define SPIBUF_RXEMPTY                  BIT(31)
#define SPIBUF_RXOVR                    BIT(30)
#define SPIBUF_TXFULL                   BIT(29)
#define SPIBUF_BITERR                   BIT(28)
#define SPIBUF_RXDATA(data)             (UINT16)((data) & 0xffff)

/* SPIDELAY */

#define SPI_C2TDELAY_OFFSET             (24)
#define SPI_T2CDELAY_OFFSET             (16)

/* SPIDEF */

#define SPIDEF_CSDEF(chan)              BIT(chan)

/* SPIFMT0 */

#define SPIFMT_WDELAY_OFFSET            (24)
#define SPIFMT_WDELAY_MASK              (0x3f)
#define SPIFMT_SHIFTDIR                 BIT(20)
#define SPIFMT_DISCSTIMERS              BIT(18)
#define SPIFMT_POLARITY                 BIT(17)
#define SPIFMT_PHASE                    BIT(16)
#define SPIFMT_PRESCALE_OFFSET          (8)
#define SPIFMT_PRESCALE_MASK            (0xff)
#define SPIFMT_CHARLEN_MIN              (2)
#define SPIFMT_CHARLEN_MAX              (0x10)
#define SPIFMT_CHARLEN_OFFSET           (0)

/* INTVEC */

#define INTVEC_ERRINT	                (0x11)
#define INTVEC_RXFULL                   (0x12)
#define INTVEC_RXOVRN	                (0x13)
#define INTVEC_TXEMPTY                  (0x14)
#define INTVEC_VAL(val)                 ((val >> 1) & 0x1f)

/* chip selects supported */

#define KEYSTONE_SPI_MAX_CS_NUM         (4)

/* device default value */

#define KEYSTONE_SPI_CLK_DEFAULT        (48000000)
#define KEYSTONE_SPI_TIMEOUT_US         (3000000)
#define SPIFMT_WDELAY_DEFAULT           (0x10)
#define SPIFMT_C2TDELAY_DEFAULT         (0x30)
#define SPIFMT_T2CDELAY_DEFAULT         (0x30)

/* all interrupts will use INT1, default will use INT0 */

#define KEYSTONE_SPI_INTLVL_INT1        (0x10000)

/* time out for interrupt mode transfer */

#define KEYSTONE_SPI_TIMEOUT_COUNT      (5)   
#define KEYSTONE_SPI_TIMEOUT_EXTRA      (20)

/* SPI rw modes */

#define SPI_RW_MODE_INT                 (0)
#define SPI_RW_MODE_POLL                (1)

/* typedefs */

/* structure holding the instance specific details */

typedef struct ti_keystone_spi_drv_ctrl
    {
    VXB_DEVICE_ID    pDev;
    void *           regBase;
    void *           regHandle;
    UINT32           rwMode;
    UINT32           clkFrequency;
    UINT32           initPhase;
    UINT32           csNum;
    UINT8 *          txBuf;
    UINT32           txLeft;
    UINT8 *          rxBuf;
    UINT32           rxLeft;
    UINT32           transLen;
    UINT32           rxSkipLen;
    UINT32           alignSize;
    UINT32           curWorkingFrq;
    UINT32           wdelay;
    UINT32           c2tdelay;
    UINT32           t2cdelay;
    BOOL             useInt1;
    BOOL             initDone;
    SEM_ID           semSync;
    SEM_ID           muxSem;
    VXB_SPI_BUS_CTRL vxbSpiCtrl;
    } TI_KEYSTONE_SPI_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbTiKeystoneSpih */
