/* spinLockLib.h - spin lock library definitions header */

/*
 * Copyright (c) 2006-2007, 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River License agreement.
 */
                                                                             
/*
modification history
--------------------
01o,30oct09,gls  added build warning when ND spinlocks used when DETERMINISTIC
                 VSB used (WIND00188965)
01n,01sep09,gls  aligned nextTicket to the architecture defined value
                 for atomic operations (WIND00136154)
01m,11feb09,gls  added non-deterministic, timed spinlock support
01l,29nov07,h_k  added SPIN_LOCK_ACKNOWLEDGED. (CQ:111571)
01l,30nov07,bha  merge for Q3 release
01k,26oct07,dlk  Address WIND00109150.
01j,14aug07,dlk  In SPIN_LOCK_TRACE case, pass file/line arguments after
                 lock to lessen problems with modules that don't know about
		 these arguments. Also, add one layer of braces in
		 SPIN_LOCK_ISR_DECL initializer to avoid compiler warning.
		 Also, add pTcb member to struct rtSpinlock for optimization.
01i,13aug07,mmi  update spinLockTask structure for ticket-lock implementation
01h,27jun07,gls  added S_spinLockLib_NOT_SPIN_LOCK_CALLABLE
                 added SPIN_LOCK_RESTRICT()
01g,14jun07,rfr  Optimize size of ISR spinlocks
01f,04apr07,rfr  Added Realtime Spinlocks
01e,29mar07,h_k  removed spinLockIsrSingleTake and Give.
01d,08mar07,mmi  add SPIN_LOCK_TRACE
01c,10jan07,h_k  added spinLockIsrSingleTake and spinLockIsrSingleGive.
01b,15dec06,mmi	 fixed SPIN_LOCK_ISR_DECL 
01b,11dec06,mmi	 add trace utility
01a,01nov06,mmi  written
*/

#ifndef __USR_LOCK__H__
#define __USR_LOCK__H__

//#include <vsbConfig.h>
#include <vwModNum.h>
#include <time.h>

#include <spinlock.h>

#ifdef __cplusplus
extern "C" {
#endif


#define SPIN_LOCK_ISR_INIT(x, y)    (spin_lock_init(x))
# define SPIN_LOCK_ISR_TAKE(x)	    (spin_lock_irq (x) )
#define SPIN_LOCK_ISR_GIVE(x)	    (spin_unlock_irq (x) )


#define SPIN_LOCK_TASK_INIT(x, y)   ( spin_lock_init(x))
#define SPIN_LOCK_TASK_TAKE(x)	    (spin_lock (x) )
#define SPIN_LOCK_TASK_GIVE(x)	    (spin_unlock (x) )

#  define SPIN_LOCK_TASK_DECL(x, flag)			    \
		spinlockTask_t x = {0, 0, NULL, 0}

# define SPIN_LOCK_ISR_DECL(x, flag)			    \
		spinlockIsr_t x = {-1, 0, 0, NULL, {{0}}}

#define SPIN_LOCK_EMPTY		0
#define SPIN_LOCK_BUSY		1
#define SPIN_LOCK_INTERESTED	2
#define SPIN_LOCK_ACKNOWLEDGED	3
#define SPIN_LOCK_NOBODY	-1


typedef spinlock_t			spinlockTask_t;
typedef spinlock_rt_dtm_t	spinlockIsr_t;


typedef void (*SPIN_LOCK_ISR_TAKE_FUNC) (spinlockIsr_t * lock);
typedef void (*SPIN_LOCK_TASK_TAKE_FUNC) (spinlockTask_t * lock);

extern void spin_lock(spinlock_t *lock);
extern void spin_unlock(spinlock_t *lock);
extern void spin_lock_init(spinlock_t *lock);
extern void spin_lock_irq(spinlock_t *lock);
extern void spin_unlock_irq(spinlock_t *lock);

#ifdef __cplusplus
}
#endif
#endif /* __INCspinLockLibh */
