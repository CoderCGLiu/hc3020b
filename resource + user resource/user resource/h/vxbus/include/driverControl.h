/* driverControl.h -- VxBus Driver Control for 3rd-party driver classes */ 

/*********************************************************************
*
* Copyright (C) 2007  Wind River Systems, Inc. All rights are reserved.
* 
* The right to copy, distribute, modify, or otherwise make use
* of this software may be licensed only pursuant to the terms
* of an applicable Wind River license agreement.
*/

/* 
modification history 
--------------------
01a,21aug07,tor  created 
*/

#ifndef __INCdriverControlh
#define __INCdriverControlh

/* includes */

#include <vxWorks.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>

/* typedefs */

struct vxbDriverControl
    {
    char *	driverName;
    int		drvCtrlCmd;
    void *	drvCtrlInfo;
    };

#endif /* __INCdriverControlh */
