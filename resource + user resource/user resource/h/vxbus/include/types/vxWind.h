#ifndef __INCvxWindh
#define __INCvxWindh

//typedef struct msg_q *		MSG_Q_ID;
#ifdef _WRS_CONFIG_LP64
//typedef thread_t	TASK_ID;
typedef struct wind_rtp *	RTP_ID;
#define VX_SEMAPHORE_SIZE	192	/* 0xc0 */
#define VX_MSG_Q_SIZE		320	/* 0x140 */
#define VX_MSG_NODE_SIZE	24	/* 0x18 */
#define VX_WDOG_SIZE		184	/* 0xb8 */

/* reader/writer semaphore extension sizes */

#define SEM_RW_LIST_ENTRY_SIZE	16	/* 0x10 */
#define SEM_RW_EXT_SIZE       	40	/* 0x28 */
#else
typedef int	                TASK_ID;
typedef int	                RTP_ID;
#define VX_SEMAPHORE_SIZE	112	/* 0x70 */
#define VX_MSG_Q_SIZE		176	/* 0xb0 */
#define VX_MSG_NODE_SIZE	12	/* 0x0c */
#define VX_WDOG_SIZE		96	/* 0x60 */

/* reader/writer semaphore extension sizes */

#define SEM_RW_LIST_ENTRY_SIZE	12	/* 0x0c */
#define SEM_RW_EXT_SIZE		24	/* 0x18 */

#endif
typedef struct wind_isr *       ISR_ID;
#endif
typedef struct wind_sd *	SD_ID;
