/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义SHELL命令结构、函数
 * 修改：
 *       2014-11-07，宋伟，建立.
 *       SHELL命令管理；
 * 		        
 */

#ifndef __SHELL_COMMAND_H__
#define __SHELL_COMMAND_H__

#ifdef __cplusplus
extern "C" {
#endif

/*  命令解析时状态定义 */
enum ParseState {
	PS_WHITESPACE,
	PS_TOKEN,
	PS_STRING,
	PS_ESCAPE
};

/*  用户命令结构 */
typedef struct command{
	const char *name;
	void (*cmdfunc)(int argc, const char **);
	struct command *next_cmd;
	const char *helpstr;	
}COMMAND;

/*  用户子命令结构 */
typedef struct subcommand {
	const char *name;
	void (*cmdfunc)(int argc, const char **);
	const char *helpstr;
} SUBCOMMAND;

/* 通用接口声明 */
void add_command(COMMAND *cmd);
int  init_builtin_cmds(void);

#ifdef __cplusplus
}
#endif

#endif
