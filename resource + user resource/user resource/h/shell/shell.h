/*! @file shell.h
    @brief ReWorks shell模块头文件
    
 * 本文件定义了ReWorks系统中的符号类型信息。
 *
 * @version 4.7
 * 
 * @see 无
 */
 
 
#ifndef _REWORKS_SHELL_H_
#define _REWORKS_SHELL_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#include <reworks/types.h>

/**
 * @defgroup group_os_io_symbol_shell shell模块
 * @ingroup group_os_develop
 * 
 * @{
 * 
 */	
 

/*!
 * \fn int lkup(const char *name)
 * 
 * \brief 显示符号表中包含name的所有符号的信息
 * 
 * \param name 符号名称
 * 
 * \return 成功返回0；失败返回-1
 */
int lkup(const char *name);
	

/*!
 * \fn int shell_module_init(int prio) 
 * 
 * \brief shell模块初始化接口
 * 
 * \param prio shell任务优先级
 * 
 * \return 成功返回0；失败返回-1
 */
int shell_module_init(int prio);

/*!
 * \fn char *shell_getline(char *shell)
 * 
 * \brief 从标准输入获取一行字符串数据
 * 
 * \return 成功返回0；失败返回-1
 */
char *shell_getline(char *shell);

/*!
 * \fn char *custom_console_start(char *input_devname, char *output_devname)
 * 
 * \brief 启动用户定制的控制台shell
 * 
 * \return 成功返回0；失败返回-1
 */
int custom_console_start(char *input_devname, char *output_devname);


void shell_init_tecla();
void shell_init_no_tecla();

void shell_add_shell_plus();
int reworks_shell_init(void);

OS_STATUS shellPlus_init();

OS_STATUS shellPlus_parseline(char *cmdline);

/* @} */

#ifdef __cplusplus
}
#endif

#endif /*SHELL_H_*/
