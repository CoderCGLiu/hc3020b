#ifndef _FORCED_LINKAGE_LIBC_H_
#define _FORCED_LINKAGE_LIBC_H_

#ifdef __cplusplus
extern "C"
{
#endif

void lib_c_module_init();

#ifdef __cplusplus
}
#endif

#endif
