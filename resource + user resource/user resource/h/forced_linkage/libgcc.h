#ifndef _FORCED_LINKAGE_LIBGCC_H_
#define _FORCED_LINKAGE_LIBGCC_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
void lib_gcc_module_init();

#ifdef __cplusplus
}
#endif

#endif
