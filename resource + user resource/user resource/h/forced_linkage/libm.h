#ifndef _FORCED_LINKAGE_LIBM_H_
#define _FORCED_LINKAGE_LIBM_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
void lib_math_module_init();

#ifdef __cplusplus
}
#endif

#endif
