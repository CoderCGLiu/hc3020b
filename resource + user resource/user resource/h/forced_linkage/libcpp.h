#ifndef _FORCED_LINKAGE_LIBCPP_H_
#define _FORCED_LINKAGE_LIBCPP_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
void lib_cpp_module_init();

#ifdef __cplusplus
}
#endif

#endif
