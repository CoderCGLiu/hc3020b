/*! @file tty_driver.h
 *  @brief ReWorks TTY模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 
 /******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：串口设备驱动程序头文件
 * 修改：
 *      2013-09-04，姚秋果，与库头文件同步更新
 * 		2012-05-05，唐立三，tty_driver增加open和close接口
 * 		2011-08-24，唐立三，建立 
 */
 
#ifndef _REWORKS_IOS_TTY_DRIVER_H_
#define _REWORKS_IOS_TTY_DRIVER_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
/**
 * @defgroup group_os_io_ttydriver TTY驱动模块
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	
/**
 * TTY设备的次设备号划分
 */	
/*!
 * \def TTY_SERIAL_START
 *  
 * \brief 串口设备起始次设备号
 */
#define TTY_SERIAL_START			0
/*!
 * \def TTY_SERIAL_NUM
 *  
 * \brief 串口设备设备数
 */
#define TTY_SERIAL_NUM				64

/*!
 * \def TTY_TERM_START
 *  
 * \brief 终端设备起始次设备号
 */
#define TTY_TERM_START				(TTY_SERIAL_START + TTY_SERIAL_NUM)
/*!
 * \def TTY_TERM_NUM
 *  
 * \brief 终端设备设备数
 */
#define TTY_TERM_NUM				64
	
/*!
 * \def TTY_PTY_START
 *  
 * \brief 伪终端设备起始次设备号
 */
#define TTY_PTY_START				(TTY_TERM_START + TTY_TERM_NUM)
/*!
 * \def TTY_PTY_NUM
 *  
 * \brief 伪终端设备设备数
 */
#define TTY_PTY_NUM					64
	

/*!
 * \def MAX_LDISC_NAME
 *  
 * \brief 规则库名称最大长度 
 */
#define MAX_LDISC_NAME					64
	
/*!
 * \def DATA_LDISC_NAME
 * 
 * \brief 数据规则库
 */
#define DATA_LDISC_NAME				"data_ldisc"
	
/*!
 * \def LINE_LDISC_NAME
 * 
 * \brief 行规则库名称
 */
#define LINE_LDISC_NAME				"line_ldisc"
	
/*!
 * \def PTY_LDISC_NAME
 * 
 * \brief 伪终端规则库
 */
#define PTY_LDISC_NAME				"pty_ldisc"

/**
 * 前向声明
 */
struct tty_driver;

/**
 * 串口驱动操作类型定义
 */
/*!
 * \typedef TTY_OPEN
 * 
 * \brief TTY设备打开操作
 */
typedef int (*TTY_OPEN)(struct tty_driver *driver);

/*!
 * \typedef TTY_CLOSE
 * 
 * \brief TTY设备关闭操作
 */
typedef int (*TTY_CLOSE)(struct tty_driver *driver);

/*!
 * \typedef TTY_START_TX
 * 
 * \brief TTY设备开始传输操作
 */
typedef int (*TTY_START_TX)(struct tty_driver *driver);

/*!
 * \typedef TTY_STOP_TX
 * 
 * \brief TTY设备停止传输操作
 */
typedef int (*TTY_STOP_TX)(struct tty_driver *driver);

/*!
 * \typedef TTY_START_RX
 * 
 * \brief TTY设备开始接收操作
 */
typedef int (*TTY_START_RX)(struct tty_driver *driver);

/*!
 * \typedef TTY_STOP_RX
 * 
 * \brief TTY设备停止接收操作
 */
typedef int (*TTY_STOP_RX)(struct tty_driver *driver);

/*!
 * \typedef TTY_RX_CHAR
 * 
 * \brief TTY设备接收字符操作
 */
typedef int (*TTY_RX_CHAR)(struct tty_driver *driver, char *c);

/*!
 * \typedef TTY_TX_CHAR
 * 
 * \brief TTY设备传输字符操作
 */
typedef int (*TTY_TX_CHAR)(struct tty_driver *driver, char c);

/*!
 * \typedef TTY_IOCTL
 * 
 * \brief TTY设备控制操作
 */
typedef int (*TTY_IOCTL)(struct tty_driver *driver, int cmd, void *arg);

/*!
 * \struct tty_driver
 * 
 * \brief TTY设备基本结构体定义
 */
struct tty_driver
{
	dev_t 					dev;			//!< 设备号 
	ptrdiff_t					ldisc_num;		//!< 规则库标识 
	TTY_OPEN				open;			//!< 打开接口 
	TTY_CLOSE				close;			//!< 关闭接口 
	TTY_START_TX 			start_tx;		//!< 启动发送 
	TTY_STOP_TX 			stop_tx;		//!< 停止发送 
	TTY_START_RX			start_rx;		//!< 启动接收 
	TTY_STOP_RX				stop_rx;		//!< 启动发送 
	TTY_RX_CHAR 			rx_char;		//!< 接收字符 
	TTY_TX_CHAR 			tx_char;		//!< 发送字符 
	TTY_IOCTL 				ioctl;			//!< 控制 
};


/*!
 * \fn int register_tty_device(const char *ldisc_name,	const char *pathname, struct tty_driver *driver);	
 * 
 * \brief TTY设备注册接口
 * 
 * \param ldisc_name 规则库名称
 * \param pathname 设备路径
 * \param driver 设备结构体
 *   
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int register_tty_device(const char *ldisc_name, 	
						const char *pathname, 		
						struct tty_driver *driver);	


/*!
 * \fn int read_buf_from_tty(struct tty_driver *driver, char *buf, u32 size);	
 * 
 * \brief 从TTY读缓冲
 * 
 * \param driver 串口设备驱动
 * \param buf 数据缓冲
 * \param size 数据大小
 *   
 * \return 成功返回读到的字符数
 * \return 失败返回-1
 * 
 */
int read_buf_from_tty(struct tty_driver *driver, 	
					  char *buf, 				
					  u32 size);					

/*!
 * \fn int read_char_from_tty(struct tty_driver *driver, char *c);	
 * 
 * \brief 从TTY读字符
 * 
 * \param driver 串口设备驱动
 * \param c 读取字符的指针
 *   
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int read_char_from_tty(struct tty_driver *driver, 		/* 串口设备驱动 */
					   char *c);						/* 读取字符的指针 */

/*!
 * \fn int get_tty_wbuf_cnt(struct tty_driver *driver);	
 * 
 * \brief 获取TTY写缓冲可读字符个数
 * 
 * \param driver 串口设备驱动
 *   
 * \return 成功返回字符个数
 * \return 失败返回-1
 * 
 */
int get_tty_wbuf_cnt(struct tty_driver *driver);

/*!
 * \fn int write_buf_to_tty(struct tty_driver *driver, const char *buf, u32 size);	
 * 
 * \brief 向TTY写缓冲
 * 
 * \param driver 串口设备驱动
 * \param buf 数据缓冲
 * \param size 数据大小
 *   
 * \return 成功返回写入的字符数
 * \return 失败返回-1
 * 
 */
int write_buf_to_tty(struct tty_driver *driver, 		/* 串口设备驱动 */
					 const char *buf, 					/* 数据缓冲 */
					 u32 size);							/* 数据大小 */

/*!
 * \fn int write_char_to_tty(struct tty_driver *driver, char c);		
 * 
 * \brief 向TTY写字符
 * 
 * \param driver 串口设备驱动
 * \param c 待写入的字符
 *   
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int write_char_to_tty(struct tty_driver *driver, 		/* 串口设备驱动 */
					  char c);							/* 待写入的字符 */

/*!
 * \fn int tx_in_poll(int serial_num, const char *buffer, int count);	
 * 
 * \brief 以轮询方式向串口发送字符
 * 
 * \param serial_num 串口编号（从0开始编号）
 * \param buffer 数据缓冲
 * \param count 数据大小
 *   
 * \return 成功返回发送的字符数
 * \return 失败返回-1
 * 
 */
int tx_in_poll(int serial_num, const char *buffer, int count);


/*!
 * \fn int rx_in_poll(int serial_num, char *buffer, int count);
 * 
 * \brief 以轮询方式从串口接收字符
 * 
 * \param serial_num 串口编号（从0开始编号）
 * \param buffer 数据缓冲
 * \param count 数据大小
 *   
 * \return 成功返回接收的字符数
 * \return 失败返回-1
 * 
 */
int rx_in_poll(int serial_num, char *buffer, int count);


/*!
 * \fn int data_ldisc_init(void);
 * 
 * \brief 数据规则库初始化接口
 * 
 * \param 无
 *   
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int data_ldisc_init(void);


/*!
 * \fn int line_ldisc_init(void);
 * 
 * \brief 行规则库初始化接口
 * 
 * \param 无
 *   
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int line_ldisc_init();

/* @} */

#ifdef __cplusplus
}
#endif

	
#endif
