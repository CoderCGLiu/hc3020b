/*! @file lx_ioctl.h
 *  @brief linux提供的ioctl命令编码头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了linux提供的ioctl命令编码格式
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2012-06-27，唐立三，建立
 * 
 */
 
#ifndef _REWORKS_IOS_LINUX_IOCTL_H_
#define _REWORKS_IOS_LINUX_IOCTL_H_


#ifdef __cplusplus
extern "C" 
{
#endif
	
/**
 * @defgroup group_os_io_lxioctl Linux控制命令
 * @ingroup group_os_io_cmd
 * 
 * @{
 * 
 */	

#define IOCPARM_MASK					0x1fff		//!< 参数长度掩码 
#define IOCPARM_LEN(x)					(((x) >> 16) & IOCPARM_MASK)  //!< 参数长度，至多13bit
#define IOCBASECMD(x)					((x) & ~(IOCPARM_MASK << 16)) //!< BASECMD值
#define IOCGROUP(x)						(((x) >> 8) & 0xff)   //!< GROUP值
#define IOCPARM_MAX						PAGE_SIZE		//!< ioctl参数最大值 
#define IOC_VOID						0x20000000		//!< 无参
#define IOC_OUT							0x40000000		//!< 输出
#define IOC_IN							0x80000000		//!< 输入
#define IOC_INOUT						(IOC_IN | IOC_OUT) //!< 输入/输出
#define IOC_DIRMASK						0xe0000000		//!< 输入输出方向掩码 

/**
 * 命令编码
 */
#define _IOC(inout,group,num,len) \
	((u32)(inout) | \
	 (u32)((u32)(((u32)len) & IOCPARM_MASK) << 16) | \
	 (u32)((group) << 8) | \
	 (u32)(num))   //!< 命令编码 

#define _IO(g,n)		_IOC(IOC_VOID,(g), (n), 0)			//!< 无参数 		
#define _IOR(g,n,t)		_IOC(IOC_OUT, (g), (n), sizeof(t))	//!< 输出式 
#define _IOW(g,n,t)		_IOC(IOC_IN, (g), (n), sizeof(t))	//!< 输入式 
#define _IOWR(g,n,t)	_IOC(IOC_INOUT, (g), (n), sizeof(t))//!< 输出输入式 
#ifdef USE_REWORKS_IPNET_69
#define IOC_USER        0x8000 /* tunnellable from RTP space */
#define	_IOU(g,n)	(IOC_USER|_IO(g,n))
#define	_IORU(g,n,t)	(IOC_USER|_IOR(g,n,t))
#define	_IOWU(g,n,t)	(IOC_USER|_IOW(g,n,t))
#define	_IOWRU(g,n,t)	(IOC_USER|_IOWR(g,n,t))
#endif
		
/* @} */	
#ifdef __cplusplus
}
#endif	
	
#endif
