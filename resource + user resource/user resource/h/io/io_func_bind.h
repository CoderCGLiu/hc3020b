#ifndef _REWORKS_IO_FUNC_BIND_H_
#define _REWORKS_IO_FUNC_BIND_H_
#ifdef __cplusplus
extern "C"
{
#endif 

#include <sys/types.h>
#include <driver.h>

typedef int (*xbd_partdev_func)(const char *devname);
typedef uint32_t (*xbd_ramdev_create_func)
    (
    struct block_device *     bd,       /* pointer to block device */
    boolean          flag,              /* should the disk support partitions? */
    const char *  name,               /* name of ram disk */
    char * ramAddr
    );
typedef uint32_t (*xbd_blkdev_create_func)
	    (
	    struct block_device *     bd,       /* pointer to block device */
	    const char *  name      /* pointer to device name  */
	    );

extern xbd_partdev_func xbd_partdev_create_func_p;
extern xbd_partdev_func xbd_partdev_delete_func_p;
extern xbd_ramdev_create_func xbd_ramdev_create_func_p;
extern xbd_blkdev_create_func xbd_blkdev_create_func_p;

#ifdef __cplusplus
}
#endif 
#endif
