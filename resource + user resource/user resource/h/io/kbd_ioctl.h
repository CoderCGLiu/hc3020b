/*! @file kbd_ioctl.h
 *  @brief 键盘控制命令
 *
 * @version 4.7
 * 
 * @see 《ReWorks应用开发编程指南附录C》
 */
 
 /******************************************************************************
 * 
 * 版权：
 * 		中国电子科技集团公司第三十二研究所
 * 描述：
 * 		TTY设备头文件
 * 修改：
 * 		2012-05-07，唐立三，建立 
 *      2013-09-04，姚秋果，与库头文件同步更新
 */
 
#ifndef _REWORKS_KEYBOARD_IOCTL_H_
#define _REWORKS_KEYBOARD_IOCTL_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <io_ctl.h>
	
/**
 * @defgroup group_os_io_kbd_ioctl 键盘控制命令
 * @ingroup group_os_io_cmd
 * 
 * 键盘相关的控制命令《ReWorks应用开发编程指南附录C》
 * @{
 * 
 */	
	
/*!
 * \def KBD_SETMODE
 * 
 * \brief 设置键盘模式 
 */
#define KBD_SETMODE				_IOW(PS2_GROUP, 1, char)
	
/*!
 * \def KBD_GETMODE
 * 
 * \brief 获取键盘模式
 */
#define KBD_GETMODE				_IOR(PS2_GROUP, 2, char)
	
/*!
 * \def KBD_GETENTRY
 * 
 * \brief 获取转换表中的一项
 */
#define KBD_GETENTRY			_IOR(PS2_GROUP, 3, char)


/*==============================================================================
 * 
 * TTY_KBD_SETMODE选项
 */	
/*!
 * \def KBD_XLATE
 * 
 * \brief 转换后的键码
 */
#define KBD_XLATE					0x0	
	
/*!
 * \def KBD_MEDIUMRAW
 * 
 * \brief 键码（中间原始模式）
 */
#define KBD_MEDIUMRAW				0x1	
	
/*!
 * \def KBD_RAW
 * 
 * \brief 扫描码（原始模式）
 */
#define KBD_RAW						0x2	

/*!
 * \def KBD_UNICODE
 * 
 * \brief unicode模式
 */
#define KBD_UNICODE					0x3	
	
/* @} */	
	
#ifdef __cplusplus
}
#endif

#endif
