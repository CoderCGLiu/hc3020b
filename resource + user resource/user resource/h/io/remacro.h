/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks中常用的宏定义
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2012-06-27，唐立三，建立
 * 
 */
#ifndef _REWORKS_IOS_REMACRO_H_
#define _REWORKS_IOS_REMACRO_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \def offsetof
 * 
 * \brief 获取结构体成员的偏移
 */
#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

/*!
 * \def container_of
 * 
 * \brief 根据结构体成员地址获取结构体地址
 */
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})	
	
#ifdef __cplusplus
}
#endif	

#endif
