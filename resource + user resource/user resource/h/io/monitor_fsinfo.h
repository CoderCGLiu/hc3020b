/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件为监测文件系统信息fsinfo定义头文件
 *
 */

#ifndef REWORKS_MONITOR_FSINFO_H_INCLUDED__
#define REWORKS_MONITOR_FSINFO_H_INCLUDED__
#ifdef __cplusplus
extern "C" 
{
#endif

#include <stdio.h>
#include <string.h>
#include <reworksio.h>
#include <fs.h>


//文件系统监控数据结构体
struct fsinfo
{
	char 	fs_name[FS_NAME_MAX];	/* name of filesystem */
	char	mount_point[PATH_MAX];
	u64		fs_size;			/* fs size */
	u64		fs_free;			/* fs free size */
};

/*******************************************************************************
 * 
 * 获取已挂载的文件系统信息
 * 
 * 输入：
 * 		fsinfo_table  文件系统信息结构体数组指针，
 * 		size	fsinfo_table的大小，建议为FS_MP_MAX，即最大挂载点数
 * 输出：
 * 		fsinfo_table  文件系统信息结构体数组，
 * 返回：
 * 		挂载文件系统个数，成功返回个数；失败返回-1
 * 		(注：若返回个数大于size，则说明输入的fsinfo_table预先分配的size不够)
 */
extern int fs_monitor_getinfo(struct fsinfo *fsinfo_table, int size);

/** 
 * @brief 显示监测文件系统信息fsinfo
 * 
 * 该接口用于显示监测文件系统信息fsinfo
 * 
 * 输入：
 * 		fsinfo_table  文件系统信息结构体数组指针，
 * 		size	fsinfo_table的大小
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void show_monitor_fsinfo(struct fsinfo *fsinfo_table, int size);
#ifdef __cplusplus
}
#endif
#endif


