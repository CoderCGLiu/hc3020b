/*! @file device.h
 *  @brief 设备操作头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_IOS_DEVICE_H_
#define _REWORKS_IOS_DEVICE_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
	
/**
 * @defgroup group_os_io_device 设备接口
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	

/*!
 * \fn int ramdev_create(const char *devname, int size)
 * 
 * \brief 本接口由I/O模块提供，用于创建大小为size（MB）的内存虚拟块设备。
 * 
 * 内存虚拟块设备使用内存单元模式普通块设备，可用于格式化和挂载文件系统。
 * 
 * \param devname 内存文件系统对应的内存设备路径
 * \param size 内存文件系统的（存储空间）大小，以MB为单位
 * 
 * \exception ENOMEM 系统内存不足，无法完成分配指定的内存
 * 
 * \return 成功返回0
 * \return 失败返回-1
 
 * \note 设备空间从用户堆中分配，因此不允许超过剩余用户堆大小。建议在创建前调用相应接口查看剩余用户堆的大小。
 */
int ramdev_create(const char *devname, int size);

/*!
 * \fn block_t bread(const char *pathname, block_t offset, void *buf, block_t blocks)
 * 
 * \brief 块设备直接读取接口
 * 
 * bread接口用于从参数pathname指向的块设备中直接读取blocks块数据存放到缓冲buf中。
 * 
 * \param pathname 要读取的块设备名称
 * \param offset 要读取的块偏移
 * \param buf 存放读取数据的缓冲区
 * \param blocks 要读取的数据块数 
 * 
 * \exception 无
 * 
 * \return 成功返回读取的块数
 * \return 失败返回-1
 */
block_t bread(const char *pathname, block_t offset, void *buf, block_t blocks);


/*!
 * \fn block_t bwrite(const char *pathname, block_t offset, const void *buf, block_t blocks)
 * 
 * \brief 块设备直接写入接口
 * 
 * bwrite接口用于将参数buf中的数量为blocks块的数据写到参数pathname指向的块设备中。
 * 
 * \param pathname 要写入数据的块设备（路径）名称
 * \param offset 要写入的（扇区）块偏移
 * \param buf 存放写入数据的缓冲区
 * \param blocks 要写入的数据块数 
 * 
 * \exception 无
 * 
 * \return 成功返回写入的块数
 * \return 失败返回-1
 */
block_t bwrite(const char *pathname, block_t offset, const void *buffer, block_t blocks);


/*!
 * \fn int bdump(const char *name, block_t offset)
 * 
 * \brief 块设备数据显示接口
 * 
 * 直接显示name执行的块设备的偏移offset处的一块数据；
 * 如果name指向的是整个块设备，那么offset为该块设备的偏移；
 * 如果name指向的是块设备的某个分区，那么offset为该分区内的偏移。
 * 
 * \param name 要写入数据的块设备名称
 * \param offset 要显示数据所在的偏移
 * 
 * \exception 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int bdump(const char *name, block_t offset);

/*!
 * \fn int bshow(const char *devname);
 * 
 * \brief 获显示指定的块设备（分区）信息
 * 
 * \param devname 块设备（分区）名称
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int bshow(const char *devname);

/*!
 * \fn int fdisk(const char *devname, block_t size1, block_t size2, block_t size3, block_t size4)
 * 
 * \brief 块设备分区接口
 * 
 * 当前ReWorks仅支持MBR分区体系，将块设备最多分为四个主分区，暂不支持扩展分区的分区方式。参数size1、
 * size2、size3和size4表示四个主分区的分区大小占整个块设备的百分比。如果分区数小于四个分区，则
 * 仅需设置前几个分区大小即可，即设置size1，根据情况设置size2、size3和size4。本分区接口允许四个分区
 * 大小之和不为100的情况，即允许在块设备尾部预留一段存储空间用于自定义存储。
 * 
 * \param devname 存储设备路径
 * \param   size1 第一个分区大小占整个设备的百分比
 * \param   size2 第二个分区大小占整个设备的百分比
 * \param   size3 第三个分区大小占整个设备的百分比
 * \param   size4 第四个分区大小占整个设备的百分比
 * 
 * \exception 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int fdisk(const char *devname, block_t size1, block_t size2, block_t size3, block_t size4);

/** @example blkdev_bops.c
 * 下面的例子演示了如何使用块设备裸读写接口操作块设备
 */ 

/** @example blkdev_create.c
 * 下面的例子演示了如何创建块设备
 */ 

/** @example blkdev_format.c
 * 下面的例子演示了如何格式化块设备
 */ 


/* @} */

#ifdef __cplusplus
}
#endif 
	
#endif
