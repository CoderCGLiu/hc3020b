/*! @file keycode.h
 *  @brief 标准PC键盘转码模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了PS/2键盘驱动模块
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2012-05-02，唐立三，建立键盘驱动模板，将键码与目标码转换部分分离
 * 		 2011-10-12，唐立三，建立
 * 		 2006-  -  ，侯勇、吴家华
 */
 
#ifndef _REWORKS_IOS_KBD_KEYCODE_H_
#define _REWORKS_IOS_KBD_KEYCODE_H_

#ifdef __cplusplus
extern "C"
{
#endif 

/**
 * @defgroup group_os_io_kbd_code 键盘转码模块
 * @ingroup group_os_io_drv
 * 
 * @{
 * 
 */	
	
/*******************************************************************************
 * 键盘LED灯标识
 */
#define KBD_LED_SCROLLLOCK			1 //!< 键盘滚动锁定LED灯标识
#define KBD_LED_NUMLOCK				2 //!< 键盘数码锁定LED灯标识
#define KBD_LED_CAPLOCK				4 //!< 键盘大写锁定LED灯标识

#define KBD_LED_SCROLLSHIFT			0 //!< 滚动锁定LED灯标识移位数
#define KBD_LED_NUMSHIFT			1 //!< 数码锁定LED灯标识移位数
#define KBD_LED_CAPSHIFT			2 //!< 大写锁定LED灯标识移位数

#define KBD_LED_MASK				0x7 //!< 键盘LED灯标识掩码

/*******************************************************************************
 * 
 * 扩充键码值
 * 即0xe0；0xe1、0x1d为扩充码的键值定义
 */
#define E0_KPENTER 					96   //!< 扩充键码值
#define E0_RCTRL   					97   //!< 扩充键码值
#define E0_KPSLASH 					98   //!< 扩充键码值
#define E0_PRSCR   					99   //!< 扩充键码值
#define E0_RALT    					100  //!< 扩充键码值
#define E0_BREAK   					101  //!< 扩充键码值	(control-pause)
#define E0_HOME    					102  //!< 扩充键码值
#define E0_UP      					103  //!< 扩充键码值
#define E0_PGUP    					104  //!< 扩充键码值
#define E0_LEFT    					105  //!< 扩充键码值
#define E0_RIGHT   					106  //!< 扩充键码值
#define E0_END     					107  //!< 扩充键码值
#define E0_DOWN    					108  //!< 扩充键码值
#define E0_PGDN    					109  //!< 扩充键码值
#define E0_INS     					110  //!< 扩充键码值
#define E0_DEL     					111  //!< 扩充键码值
#define E1_PAUSE   					119	 //!< 扩充键码值	（ 0xe1、0x1d、0x45 ）


#define SC_LIM 						89	//!< 高值扫描码转换界限
#define FOCUS_PF1 					85  //!< 高值扫描码   
#define FOCUS_PF2 					89  //!< 高值扫描码
#define FOCUS_PF3 					90  //!< 高值扫描码
#define FOCUS_PF4 					91  //!< 高值扫描码
#define FOCUS_PF5 					92  //!< 高值扫描码
#define FOCUS_PF6 					93  //!< 高值扫描码
#define FOCUS_PF7 					94  //!< 高值扫描码
#define FOCUS_PF8 					95  //!< 高值扫描码
#define FOCUS_PF9 					120 //!< 高值扫描码
#define FOCUS_PF10 					121 //!< 高值扫描码
#define FOCUS_PF11 					122 //!< 高值扫描码
#define FOCUS_PF12 					123 //!< 高值扫描码
#define JAP_86     					124 //!< 高值扫描码
#define RGN1 						124 //!< 高值扫描码
#define RGN2 						125 //!< 高值扫描码
#define RGN3 						126 //!< 高值扫描码
#define RGN4 						127 //!< 高值扫描码

#define KG_SHIFT					0  //!< KG_SHIFT
#define KG_CTRL						2  //!< KG_CTRL
#define KG_ALT						3  //!< KG_ALT
#define KG_ALTGR					1  //!< KG_ALTGR
#define KG_SHIFTL					4  //!< KG_SHIFTL
#define KG_SHIFTR					5  //!< KG_SHIFTR
#define KG_CTRLL					6  //!< KG_CTRLL
#define KG_CTRLR					7  //!< KG_CTRLR
#define KG_CAPSSHIFT				8  //!< KG_CAPSSHIFT
#define NR_SHIFT					9  //!< NR_SHIFT

#define NR_KEYS						128//!< NR_KEYS
#define MAX_NR_KEYMAPS				256//!< MAX_NR_KEYMAPS



#define MAX_NR_OF_USER_KEYMAPS 		256 //!< 用户键值映射表最大个数
#define E0_MACRO   					112 //!< E0_MACRO
#define E0_F13     					113 //!< E0_F13
#define E0_F14     					114 //!< E0_F14
#define E0_HELP    					115 //!< E0_HELP
#define E0_DO      					116 //!< E0_DO
#define E0_F17     					117 //!< E0_F17
#define E0_KPMINPLUS 				118 //!< E0_KPMINPLUS
#define E0_OK						124 //!< E0_OK
#define E0_MSLW						125 //!< E0_MSLW
#define E0_MSRW						126 //!< E0_MSRW
#define E0_MSTM						127 //!< E0_MSTM

#define MAX_NR_FUNC					256	//!< MAX_NR_FUNC


/*******************************************************************************
 * 
 * 按键类型
 * 共存在13中按键类型；其取值为目标码的高8位与0xf0之差
 * 
 */
#define KT_LATIN					0		    //!< 普通可打印字符 
#define KT_LETTER					11		  //!< 字符 
#define KT_FN						  1		    //!< FN功能键 
#define KT_SPEC						2	      //!< 空格键	
#define KT_PAD						3       //!< 小键盘
#define KT_DEAD						4       //!< KT_DEAD
#define KT_CONS						5       //!< KT_CONS
#define KT_CUR						6       //!< KT_CUR
#define KT_SHIFT					7       //!< KT_SHIFT
#define KT_META						8       //!< KT_META
#define KT_ASCII					9       //!< KT_ASCII
#define KT_LOCK						10      //!< KT_LOCK
#define KT_SLOCK					12      //!< KT_SLOCK

#define K(t,v)						(((t) << 8)|(v))           //!< K
#define KTYP(x)						((x) >> 8)                 //!< KTYP
#define KVAL(x)						((x) & 0xff)               //!< KVAL
#define KUP(x)						(!((x) & 0x80) ? 0 : 0x80) //!< KUP
#define KEYUP_FLAG					0x80		                 //!< BREAK CODE标识 

#define K_F1						  K(KT_FN,0)   //!< K_F1		 
#define K_F2						  K(KT_FN,1)   //!< K_F2		
#define K_F3						  K(KT_FN,2)   //!< K_F3		
#define K_F4						  K(KT_FN,3)   //!< K_F4		
#define K_F5						  K(KT_FN,4)   //!< K_F5		
#define K_F6						  K(KT_FN,5)   //!< K_F6		
#define K_F7						  K(KT_FN,6)   //!< K_F7		
#define K_F8						  K(KT_FN,7)   //!< K_F8		
#define K_F9						  K(KT_FN,8)   //!< K_F9		
#define K_F10						  K(KT_FN,9)   //!< K_F10		
#define K_F11						  K(KT_FN,10)  //!< K_F11		
#define K_F12						  K(KT_FN,11)  //!< K_F12		
#define K_F13						  K(KT_FN,12)  //!< K_F13		
#define K_F14						  K(KT_FN,13)  //!< K_F14		
#define K_F15						  K(KT_FN,14)  //!< K_F15		
#define K_F16						  K(KT_FN,15)  //!< K_F16		
#define K_F17						  K(KT_FN,16)  //!< K_F17		
#define K_F18						  K(KT_FN,17)  //!< K_F18		
#define K_F19						  K(KT_FN,18)  //!< K_F19		
#define K_F20						  K(KT_FN,19)  //!< K_F20		
#define K_FIND						K(KT_FN,20)  //!< K_FIND	
#define K_INSERT					K(KT_FN,21)  //!< K_INSERT
#define K_REMOVE					K(KT_FN,22)  //!< K_REMOVE
#define K_SELECT					K(KT_FN,23)  //!< K_SELECT
#define K_PGUP						K(KT_FN,24)  //!< PGUP is a synonym for PRIOR 
#define K_PGDN						K(KT_FN,25)  //!< PGDN is a synonym for NEXT 
#define K_MACRO	 					K(KT_FN,26)  //!< K_MACRO	
#define K_HELP						K(KT_FN,27)  //!< K_HELP	
#define K_DO						  K(KT_FN,28)  //!< K_DO		
#define K_PAUSE	 					K(KT_FN,29)  //!< K_PAUSE	
#define K_F21						K(KT_FN,30)    //!< K_F21
#define K_F22						K(KT_FN,31)    //!< K_F22
#define K_F23						K(KT_FN,32)    //!< K_F23
#define K_F24						K(KT_FN,33)    //!< K_F24
#define K_F25						K(KT_FN,34)    //!< K_F25
#define K_F26						K(KT_FN,35)    //!< K_F26
#define K_F27						K(KT_FN,36)    //!< K_F27
#define K_F28						K(KT_FN,37)    //!< K_F28
#define K_F29						K(KT_FN,38)    //!< K_F29
#define K_F30						K(KT_FN,39)    //!< K_F30
#define K_F31						K(KT_FN,40)    //!< K_F31
#define K_F32						K(KT_FN,41)    //!< K_F32
#define K_F33						K(KT_FN,42)    //!< K_F33
#define K_F34						K(KT_FN,43)    //!< K_F34
#define K_F35						K(KT_FN,44)    //!< K_F35
#define K_F36						K(KT_FN,45)    //!< K_F36
#define K_F37						K(KT_FN,46)    //!< K_F37
#define K_F38						K(KT_FN,47)    //!< K_F38
#define K_F39						K(KT_FN,48)    //!< K_F39
#define K_F40						K(KT_FN,49)    //!< K_F40
#define K_F41						K(KT_FN,50)    //!< K_F41
#define K_F42						K(KT_FN,51)    //!< K_F42
#define K_F43						K(KT_FN,52)    //!< K_F43
#define K_F44						K(KT_FN,53)    //!< K_F44
#define K_F45						K(KT_FN,54)    //!< K_F45
#define K_F46						K(KT_FN,55)    //!< K_F46
#define K_F47						K(KT_FN,56)    //!< K_F47
#define K_F48						K(KT_FN,57)    //!< K_F48
#define K_F49						K(KT_FN,58)    //!< K_F49
#define K_F50						K(KT_FN,59)    //!< K_F50
#define K_F51						K(KT_FN,60)    //!< K_F51
#define K_F52						K(KT_FN,61)    //!< K_F52
#define K_F53						K(KT_FN,62)    //!< K_F53
#define K_F54						K(KT_FN,63)    //!< K_F54
#define K_F55						K(KT_FN,64)    //!< K_F55
#define K_F56						K(KT_FN,65)    //!< K_F56
#define K_F57						K(KT_FN,66)    //!< K_F57
#define K_F58						K(KT_FN,67)    //!< K_F58
#define K_F59						K(KT_FN,68)    //!< K_F59
#define K_F60						K(KT_FN,69)    //!< K_F60
#define K_F61						K(KT_FN,70)    //!< K_F61
#define K_F62						K(KT_FN,71)    //!< K_F62
#define K_F63						K(KT_FN,72)    //!< K_F63
#define K_F64						K(KT_FN,73)    //!< K_F64
#define K_F65						K(KT_FN,74)    //!< K_F65
#define K_F66						K(KT_FN,75)    //!< K_F66
#define K_F67						K(KT_FN,76)    //!< K_F67
#define K_F68						K(KT_FN,77)    //!< K_F68
#define K_F69						K(KT_FN,78)    //!< K_F69
#define K_F70						K(KT_FN,79)    //!< K_F70
#define K_F71						K(KT_FN,80)    //!< K_F71
#define K_F72						K(KT_FN,81)    //!< K_F72
#define K_F73						K(KT_FN,82)    //!< K_F73
#define K_F74						K(KT_FN,83)    //!< K_F74
#define K_F75						K(KT_FN,84)    //!< K_F75
#define K_F76						K(KT_FN,85)    //!< K_F76
#define K_F77						K(KT_FN,86)    //!< K_F77
#define K_F78						K(KT_FN,87)    //!< K_F78
#define K_F79						K(KT_FN,88)    //!< K_F79
#define K_F80						K(KT_FN,89)    //!< K_F80
#define K_F81						K(KT_FN,90)    //!< K_F81
#define K_F82						K(KT_FN,91)    //!< K_F82
#define K_F83						K(KT_FN,92)    //!< K_F83
#define K_F84						K(KT_FN,93)    //!< K_F84
#define K_F85						K(KT_FN,94)    //!< K_F85
#define K_F86						K(KT_FN,95)    //!< K_F86
#define K_F87						K(KT_FN,96)    //!< K_F87
#define K_F88						K(KT_FN,97)    //!< K_F88
#define K_F89						K(KT_FN,98)    //!< K_F89
#define K_F90						K(KT_FN,99)    //!< K_F90 
#define K_F91						K(KT_FN,100)   //!< K_F91
#define K_F92						K(KT_FN,101)   //!< K_F92
#define K_F93						K(KT_FN,102)   //!< K_F93
#define K_F94						K(KT_FN,103)   //!< K_F94
#define K_F95						K(KT_FN,104)   //!< K_F95
#define K_F96						K(KT_FN,105)   //!< K_F96
#define K_F97						K(KT_FN,106)   //!< K_F97
#define K_F98						K(KT_FN,107)   //!< K_F98
#define K_F99						K(KT_FN,108)   //!< K_F99
#define K_F100						K(KT_FN,109)   //!< K_F100
#define K_F101						K(KT_FN,110)   //!< K_F101
#define K_F102						K(KT_FN,111)   //!< K_F102
#define K_F103						K(KT_FN,112)   //!< K_F103
#define K_F104						K(KT_FN,113)   //!< K_F104
#define K_F105						K(KT_FN,114)   //!< K_F105
#define K_F106						K(KT_FN,115)   //!< K_F106
#define K_F107						K(KT_FN,116)   //!< K_F107
#define K_F108						K(KT_FN,117)   //!< K_F108
#define K_F109						K(KT_FN,118)   //!< K_F109
#define K_F110						K(KT_FN,119)   //!< K_F110
#define K_F111						K(KT_FN,120)   //!< K_F111
#define K_F112						K(KT_FN,121)   //!< K_F112
#define K_F113						K(KT_FN,122)   //!< K_F113
#define K_F114						K(KT_FN,123)   //!< K_F114
#define K_F115						K(KT_FN,124)   //!< K_F115
#define K_F116						K(KT_FN,125)   //!< K_F116
#define K_F117						K(KT_FN,126)   //!< K_F117
#define K_F118						K(KT_FN,127)   //!< K_F118
#define K_F119						K(KT_FN,128)   //!< K_F119
#define K_F120						K(KT_FN,129)   //!< K_F120
#define K_F121						K(KT_FN,130)   //!< K_F121
#define K_F122						K(KT_FN,131)   //!< K_F122
#define K_F123						K(KT_FN,132)   //!< K_F123
#define K_F124						K(KT_FN,133)   //!< K_F124
#define K_F125						K(KT_FN,134)   //!< K_F125
#define K_F126						K(KT_FN,135)   //!< K_F126
#define K_F127						K(KT_FN,136)   //!< K_F127
#define K_F128						K(KT_FN,137)   //!< K_F128
#define K_F129						K(KT_FN,138)   //!< K_F129
#define K_F130						K(KT_FN,139)   //!< K_F130
#define K_F131						K(KT_FN,140)   //!< K_F131 
#define K_F132						K(KT_FN,141)   //!< K_F132
#define K_F133						K(KT_FN,142)   //!< K_F133
#define K_F134						K(KT_FN,143)   //!< K_F134
#define K_F135						K(KT_FN,144)   //!< K_F135
#define K_F136						K(KT_FN,145)   //!< K_F136
#define K_F137						K(KT_FN,146)   //!< K_F137
#define K_F138						K(KT_FN,147)   //!< K_F138
#define K_F139						K(KT_FN,148)   //!< K_F139
#define K_F140						K(KT_FN,149)   //!< K_F140
#define K_F141						K(KT_FN,150)   //!< K_F141
#define K_F142						K(KT_FN,151)   //!< K_F142
#define K_F143						K(KT_FN,152)   //!< K_F143
#define K_F144						K(KT_FN,153)   //!< K_F144
#define K_F145						K(KT_FN,154)   //!< K_F145
#define K_F146						K(KT_FN,155)   //!< K_F146
#define K_F147						K(KT_FN,156)   //!< K_F147
#define K_F148						K(KT_FN,157)   //!< K_F148
#define K_F149						K(KT_FN,158)   //!< K_F149
#define K_F150						K(KT_FN,159)   //!< K_F150
#define K_F151						K(KT_FN,160)   //!< K_F151
#define K_F152						K(KT_FN,161)   //!< K_F152
#define K_F153						K(KT_FN,162)   //!< K_F153
#define K_F154						K(KT_FN,163)   //!< K_F154
#define K_F155						K(KT_FN,164)   //!< K_F155
#define K_F156						K(KT_FN,165)   //!< K_F156
#define K_F157						K(KT_FN,166)   //!< K_F157
#define K_F158						K(KT_FN,167)   //!< K_F158
#define K_F159						K(KT_FN,168)   //!< K_F159
#define K_F160						K(KT_FN,169)   //!< K_F160
#define K_F161						K(KT_FN,170)   //!< K_F161
#define K_F162						K(KT_FN,171)   //!< K_F162
#define K_F163						K(KT_FN,172)   //!< K_F163
#define K_F164						K(KT_FN,173)   //!< K_F164
#define K_F165						K(KT_FN,174)   //!< K_F165
#define K_F166						K(KT_FN,175)   //!< K_F166
#define K_F167						K(KT_FN,176)   //!< K_F167
#define K_F168						K(KT_FN,177)   //!< K_F168
#define K_F169						K(KT_FN,178)   //!< K_F169
#define K_F170						K(KT_FN,179)   //!< K_F170
#define K_F171						K(KT_FN,180)   //!< K_F171
#define K_F172						K(KT_FN,181)   //!< K_F172
#define K_F173						K(KT_FN,182)   //!< K_F173
#define K_F174						K(KT_FN,183)   //!< K_F174
#define K_F175						K(KT_FN,184)   //!< K_F175
#define K_F176						K(KT_FN,185)   //!< K_F176
#define K_F177						K(KT_FN,186)   //!< K_F177
#define K_F178						K(KT_FN,187)   //!< K_F178
#define K_F179						K(KT_FN,188)   //!< K_F179
#define K_F180						K(KT_FN,189)   //!< K_F180
#define K_F181						K(KT_FN,190)   //!< K_F181
#define K_F182						K(KT_FN,191)   //!< K_F182
#define K_F183						K(KT_FN,192)   //!< K_F183
#define K_F184						K(KT_FN,193)   //!< K_F184
#define K_F185						K(KT_FN,194)   //!< K_F185
#define K_F186						K(KT_FN,195)   //!< K_F186
#define K_F187						K(KT_FN,196)   //!< K_F187
#define K_F188						K(KT_FN,197)   //!< K_F188
#define K_F189						K(KT_FN,198)   //!< K_F189
#define K_F190						K(KT_FN,199)   //!< K_F190
#define K_F191						K(KT_FN,200)   //!< K_F191
#define K_F192						K(KT_FN,201)   //!< K_F192
#define K_F193						K(KT_FN,202)   //!< K_F193
#define K_F194						K(KT_FN,203)   //!< K_F194
#define K_F195						K(KT_FN,204)   //!< K_F195
#define K_F196						K(KT_FN,205)   //!< K_F196
#define K_F197						K(KT_FN,206)   //!< K_F197   
#define K_F198						K(KT_FN,207)   //!< K_F198
#define K_F199						K(KT_FN,208)   //!< K_F199
#define K_F200						K(KT_FN,209)   //!< K_F200
#define K_F201						K(KT_FN,210)   //!< K_F201
#define K_F202						K(KT_FN,211)   //!< K_F202
#define K_F203						K(KT_FN,212)   //!< K_F203
#define K_F204						K(KT_FN,213)   //!< K_F204
#define K_F205						K(KT_FN,214)   //!< K_F205
#define K_F206						K(KT_FN,215)   //!< K_F206
#define K_F207						K(KT_FN,216)   //!< K_F207
#define K_F208						K(KT_FN,217)   //!< K_F208
#define K_F209						K(KT_FN,218)   //!< K_F209
#define K_F210						K(KT_FN,219)   //!< K_F210
#define K_F211						K(KT_FN,220)   //!< K_F211
#define K_F212						K(KT_FN,221)   //!< K_F212
#define K_F213						K(KT_FN,222)   //!< K_F213
#define K_F214						K(KT_FN,223)   //!< K_F214
#define K_F215						K(KT_FN,224)   //!< K_F215
#define K_F216						K(KT_FN,225)   //!< K_F216
#define K_F217						K(KT_FN,226)   //!< K_F217
#define K_F218						K(KT_FN,227)   //!< K_F218
#define K_F219						K(KT_FN,228)   //!< K_F219
#define K_F220						K(KT_FN,229)   //!< K_F220
#define K_F221						K(KT_FN,230)   //!< K_F221
#define K_F222						K(KT_FN,231)   //!< K_F222
#define K_F223						K(KT_FN,232)   //!< K_F223
#define K_F224						K(KT_FN,233)   //!< K_F224
#define K_F225						K(KT_FN,234)   //!< K_F225
#define K_F226						K(KT_FN,235)   //!< K_F226
#define K_F227						K(KT_FN,236)   //!< K_F227
#define K_F228						K(KT_FN,237)   //!< K_F228
#define K_F229						K(KT_FN,238)   //!< K_F229
#define K_F230						K(KT_FN,239)   //!< K_F230
#define K_F231						K(KT_FN,240)   //!< K_F231
#define K_F232						K(KT_FN,241)   //!< K_F232
#define K_F233						K(KT_FN,242)   //!< K_F233
#define K_F234						K(KT_FN,243)   //!< K_F234
#define K_F235						K(KT_FN,244)   //!< K_F235
#define K_F236						K(KT_FN,245)   //!< K_F236
#define K_F237						K(KT_FN,246)   //!< K_F237
#define K_F238						K(KT_FN,247)   //!< K_F238
#define K_F239						K(KT_FN,248)   //!< K_F239
#define K_F240						K(KT_FN,249)   //!< K_F240
#define K_F241						K(KT_FN,250)   //!< K_F241
#define K_F242						K(KT_FN,251)   //!< K_F242
#define K_F243						K(KT_FN,252)   //!< K_F243
#define K_F244						K(KT_FN,253)   //!< K_F244
#define K_F245						K(KT_FN,254)   //!< K_F245
#define K_UNDO						K(KT_FN,255)   //!< K_UNDO 

#define K_HOLE						K(KT_SPEC,0)   //!< K_HOLE
#define K_ENTER						K(KT_SPEC,1)   //!< K_ENTER
#define K_SH_REGS					K(KT_SPEC,2)   //!< K_SH_REGS
#define K_SH_MEM					K(KT_SPEC,3)   //!< K_SH_REGS
#define K_SH_STAT					K(KT_SPEC,4)   //!< K_SH_STAT
#define K_BREAK						K(KT_SPEC,5)   //!< K_BREAK
#define K_CONS						K(KT_SPEC,6)   //!< K_CONS
#define K_CAPS						K(KT_SPEC,7)   //!< K_CAPS
#define K_NUM						  K(KT_SPEC,8)   //!< K_NUM
#define K_HOLD						K(KT_SPEC,9)   //!< K_HOLD
#define K_SCROLLFORW			K(KT_SPEC,10)   //!< K_SCROLLFORW
#define K_SCROLLBACK			K(KT_SPEC,11)   //!< K_SCROLLBACK
#define K_BOOT						K(KT_SPEC,12)   //!< K_BOOT
#define K_CAPSON					K(KT_SPEC,13)   //!< K_CAPSON
#define K_COMPOSE					K(KT_SPEC,14)   //!< K_COMPOSE
#define K_SAK						  K(KT_SPEC,15)   //!< K_SAK
#define K_DECRCONSOLE			K(KT_SPEC,16)   //!< K_DECRCONSOLE
#define K_INCRCONSOLE			K(KT_SPEC,17)   //!< K_INCRCONSOLE
#define K_SPAWNCONSOLE		K(KT_SPEC,18)   //!< K_SPAWNCONSOLE
#define K_BARENUMLOCK			K(KT_SPEC,19)   //!< K_BARENUMLOCK

#define K_ALLOCATED				K(KT_SPEC,126)  //!< 自动分配 keymap 
#define K_NOSUCHMAP				K(KT_SPEC,127)  //!< 通过KDGKBENT返回 

#define K_P0						K(KT_PAD,0)     //!< 小键盘数字0
#define K_P1						K(KT_PAD,1)     //!< 小键盘数字1
#define K_P2						K(KT_PAD,2)     //!< 小键盘数字2
#define K_P3						K(KT_PAD,3)     //!< 小键盘数字3
#define K_P4						K(KT_PAD,4)     //!< 小键盘数字4
#define K_P5						K(KT_PAD,5)     //!< 小键盘数字5
#define K_P6						K(KT_PAD,6)     //!< 小键盘数字6
#define K_P7						K(KT_PAD,7)     //!< 小键盘数字7
#define K_P8						K(KT_PAD,8)     //!< 小键盘数字8
#define K_P9						K(KT_PAD,9)     //!< 小键盘数字9
#define K_PPLUS						K(KT_PAD,10)	//!< 小键盘plus 
#define K_PMINUS					K(KT_PAD,11)	//!< 小键盘 minus 
#define K_PSTAR						K(KT_PAD,12)	//!< 小键盘 asterisk (star) 
#define K_PSLASH					K(KT_PAD,13)	//!< 小键盘 slash 
#define K_PENTER					K(KT_PAD,14)	//!< 小键盘 enter 
#define K_PCOMMA					K(KT_PAD,15)	//!< 小键盘 comma: kludge... 
#define K_PDOT						K(KT_PAD,16)	//!< 小键盘 dot (period): kludge... 
#define K_PPLUSMINUS			K(KT_PAD,17)	//!< 小键盘 plus/minus 
#define K_PPARENL					K(KT_PAD,18)	//!< 小键盘 left parenthesis 
#define K_PPARENR					K(KT_PAD,19)	//!< 小键盘 right parenthesis
                                                    
#define NR_PAD						20         //!< 小键盘个数

#define K_DGRAVE					K(KT_DEAD,0) //!< K_DGRAVE
#define K_DACUTE					K(KT_DEAD,1) //!< K_DACUTE
#define K_DCIRCM					K(KT_DEAD,2) //!< K_DCIRCM
#define K_DTILDE					K(KT_DEAD,3) //!< K_DTILDE
#define K_DDIERE					K(KT_DEAD,4) //!< K_DDIERE
#define K_DCEDIL					K(KT_DEAD,5) //!< K_DCEDIL

#define NR_DEAD						6 //!< NR_DEAD

#define K_DOWN						K(KT_CUR,0) //!< 下键
#define K_LEFT						K(KT_CUR,1) //!< 左键
#define K_RIGHT						K(KT_CUR,2) //!< 右键
#define K_UP						  K(KT_CUR,3) //!< 上键

#define K_SHIFT						K(KT_SHIFT,KG_SHIFT)     //!< K_SHIFT
#define K_CTRL						K(KT_SHIFT,KG_CTRL)      //!< K_CTRL
#define K_ALT						  K(KT_SHIFT,KG_ALT)       //!< K_ALT
#define K_ALTGR						K(KT_SHIFT,KG_ALTGR)     //!< K_ALTGR
#define K_SHIFTL					K(KT_SHIFT,KG_SHIFTL)    //!< K_SHIFTL
#define K_SHIFTR					K(KT_SHIFT,KG_SHIFTR)    //!< K_SHIFTR
#define K_CTRLL	 					K(KT_SHIFT,KG_CTRLL)     //!< K_CTRLL
#define K_CTRLR	 					K(KT_SHIFT,KG_CTRLR)     //!< K_CTRLR
#define K_CAPSSHIFT					K(KT_SHIFT,KG_CAPSSHIFT) //!< K_CAPSSHIFT 

#define K_ASC0						K(KT_ASCII,0) //!< ASCII码0
#define K_ASC1						K(KT_ASCII,1) //!< ASCII码1
#define K_ASC2						K(KT_ASCII,2) //!< ASCII码2
#define K_ASC3						K(KT_ASCII,3) //!< ASCII码3
#define K_ASC4						K(KT_ASCII,4) //!< ASCII码4
#define K_ASC5						K(KT_ASCII,5) //!< ASCII码5
#define K_ASC6						K(KT_ASCII,6) //!< ASCII码6
#define K_ASC7						K(KT_ASCII,7) //!< ASCII码7
#define K_ASC8						K(KT_ASCII,8) //!< ASCII码8
#define K_ASC9						K(KT_ASCII,9) //!< ASCII码9
#define K_HEX0						K(KT_ASCII,10) //!< ASCII码10
#define K_HEX1						K(KT_ASCII,11) //!< ASCII码11
#define K_HEX2						K(KT_ASCII,12) //!< ASCII码12
#define K_HEX3						K(KT_ASCII,13) //!< ASCII码13
#define K_HEX4						K(KT_ASCII,14) //!< ASCII码14
#define K_HEX5						K(KT_ASCII,15) //!< ASCII码15
#define K_HEX6						K(KT_ASCII,16) //!< ASCII码16
#define K_HEX7						K(KT_ASCII,17) //!< ASCII码17
#define K_HEX8						K(KT_ASCII,18) //!< ASCII码18
#define K_HEX9						K(KT_ASCII,19) //!< ASCII码19
#define K_HEXa						K(KT_ASCII,20) //!< ASCII码20
#define K_HEXb						K(KT_ASCII,21) //!< ASCII码21
#define K_HEXc						K(KT_ASCII,22) //!< ASCII码22
#define K_HEXd						K(KT_ASCII,23) //!< ASCII码23
#define K_HEXe						K(KT_ASCII,24) //!< ASCII码24
#define K_HEXf						K(KT_ASCII,25) //!< ASCII码25

#define NR_ASCII					26 //!< ASCII码个数

#define K_SHIFTLOCK					K(KT_LOCK,KG_SHIFT)  //!< K_SHIFTLOCK
#define K_CTRLLOCK					K(KT_LOCK,KG_CTRL)   //!< K_CTRLLOCK
#define K_ALTLOCK					  K(KT_LOCK,KG_ALT)    //!< K_ALTLOCK
#define K_ALTGRLOCK					K(KT_LOCK,KG_ALTGR)  //!< K_ALTGRLOCK
#define K_SHIFTLLOCK				K(KT_LOCK,KG_SHIFTL) //!< K_SHIFTLLOCK
#define K_SHIFTRLOCK				K(KT_LOCK,KG_SHIFTR) //!< K_SHIFTRLOCK
#define K_CTRLLLOCK					K(KT_LOCK,KG_CTRLL)  //!< K_CTRLLLOCK 
#define K_CTRLRLOCK					K(KT_LOCK,KG_CTRLR)  //!< K_CTRLRLOCK

#define K_SHIFT_SLOCK				K(KT_SLOCK,KG_SHIFT)  //!< K_SHIFT_SLOCK
#define K_CTRL_SLOCK				K(KT_SLOCK,KG_CTRL)   //!< K_CTRL_SLOCK
#define K_ALT_SLOCK					K(KT_SLOCK,KG_ALT)    //!< K_ALT_SLOCK
#define K_ALTGR_SLOCK				K(KT_SLOCK,KG_ALTGR)  //!< K_ALTGR_SLOCK
#define K_SHIFTL_SLOCK			K(KT_SLOCK,KG_SHIFTL) //!< K_SHIFTL_SLOCK
#define K_SHIFTR_SLOCK			K(KT_SLOCK,KG_SHIFTR) //!< K_SHIFTR_SLOCK 
#define K_CTRLL_SLOCK				K(KT_SLOCK,KG_CTRLL)  //!< K_CTRLL_SLOCK
#define K_CTRLR_SLOCK				K(KT_SLOCK,KG_CTRLR)  //!< K_CTRLR_SLOCK 

#define NR_LOCK						8 //!< LOCK个数

#define U(x) ((x) ^ 0xf000) //!< 取双字高四位

#define VC_APPLIC					0				//!<  应用键模式 
#define VC_CKMODE					1				//!<  光标键模式 
#define VC_REPEAT					2				//!<  键盘重复 
#define VC_CRLF						3				//!<  0 - 发送回车, 1 - 发送回车换行F 
#define VC_META						4				//!< 0 - meta, 1 - meta=prefix with ESC 
#define VC_SHIFTLOCK				G_SHIFT			//!< shift lock 模式 
#define VC_ALTGRLOCK				KG_ALTGR		//!< altgr lock 模式 
#define VC_CTRLLOCK					KG_CTRL 		//!< control lock 模式 
#define VC_ALTLOCK					KG_ALT  		//!< alt lock 模式 
#define VC_SHIFTLLOCK				KG_SHIFTL		//!< shiftl lock 模式 
#define VC_SHIFTRLOCK				KG_SHIFTR		//!< shiftr lock 模式 
#define VC_CTRLLLOCK				KG_CTRLL 		//!< ctrll lock 模式 
#define VC_CTRLRLOCK				KG_CTRLR 		//!< ctrlr lock 模式 
#define LED_SHOW_FLAGS 				0        		//!< 传统状态 
#define LED_SHOW_IOCTL 				1        		//!< 仅当ioctl改变LED 
#define LED_SHOW_MEM 				2          		//!< `heartbeat': peek into memory 
#define VC_SCROLLOCK				0				//!< scroll-lock 模式 
#define VC_NUMLOCK					1				//!< numeric lock 模式 
#define VC_CAPSLOCK					2				//!< capslock 模式 
#define VC_XLATE					0				//!< 使用keymap翻译keycodes 
#define VC_MEDIUMRAW				1				//!< medium raw (keycode)模式 
#define VC_RAW						2				//!< raw (scancode) 模式 
#define VC_UNICODE					3				//!< Unicode 模式 
                                                     
#ifndef KBD_DEFMODE
#	define KBD_DEFMODE ((1 << VC_REPEAT) | (1 << VC_META)) //!< DEFMODE
#endif
#ifndef KBD_DEFLEDS
#	define KBD_DEFLEDS 0 //!< DEFLEDS
#endif
#ifndef KBD_DEFLOCK
#	define KBD_DEFLOCK 0 //!< KBD_DEFLOCK 
#endif
#define WAIT_MAX        				100      //!< WAIT_MAX
#define NUM             				0x0002  //!< 数字键锁标识 
#define COMMAND_8042       	 			0x64 //!< 8024命令寄存器
#define DATA_8042           			0x60 //!< 8024数据寄存器
#define STATUS_8042         			COMMAND_8042 //!< 8024状态寄存器
#define TYPES_ALLOWED_IN_RAW_MODE 		((1 << KT_SPEC) | (1 << KT_SHIFT)) //!<  RAW模式允许的类型
#define SPECIALS_ALLOWED_IN_RAW_MODE 	(1 << KVAL(K_SAK))   //!< RAW模式允许的特殊类型

/* ? */
/*!
 * \struct kbentry 
 *
 * \brief 键码描述结构体
 */
struct kbentry 
{
	unsigned char kb_table;    //!< 键码表
	unsigned char kb_index;    //!< 键码索引值
	unsigned short kb_value;   //!< 键码值
};

/**
 * 键盘设备描述结构前向声明
 */
struct kbd_device_struct;

/**
 * 设置键盘LED灯接口
 * 由键盘驱动程序赋值提供
 */
typedef void (* KBD_HW_LED_SET)(u8 newled);    //!< 设置键盘LED灯接口
extern  KBD_HW_LED_SET kbd_hw_led_set;         //!< 设置键盘LED灯接口

/**
 * 转换码输入接口
 * 由键盘驱动程序赋值提供
 */
typedef int (* KBD_DATA_INPUT)(struct kbd_device_struct *kbd, char c);//!< 转换码输入接口
extern KBD_DATA_INPUT kbd_data_input; //!< 转换码输入接口

/* 键盘设备描述结构 */
/*!
 * \struct kbd_device_struct
 *
 * \brief 键盘设备描述结构
 */
struct kbd_device_struct
{
	unsigned char 				lockstate;               //!< lock状态
	unsigned char 				slockstate;              //!< slock状态
	unsigned char 				ledmode; 	             //!< LED模式
	unsigned char 				ledflagstate;	         //!< LED状态标志		
	unsigned char 				default_ledflagstate;    //!< 默认LED状态标志
	unsigned char 				kbdmode;	             //!< 键盘模式			
	unsigned char 				modeflags;	             //!< 模式标志
	KBD_DATA_INPUT 				kbd_data_input;	         //!< 转换码输入接口
	KBD_HW_LED_SET 				kbd_hw_led_set;          //!< 设置键盘LED灯接口
};


 /*!
 * \fn int keycode_ioctl(struct kbd_device_struct *kbd, u32 cmd, void *arg)
 * 
 * \brief 键码控制接口
 * 
 * \param kbd	键盘设备
 * \param cmd 	键码控制命令
 * \param arg  	键码控制命令参数
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int keycode_ioctl(struct kbd_device_struct *kbd, u32 cmd, void *arg);


 /*!
 * \fn void keycode_handler(struct kbd_device_struct *kbd, unsigned char scancode)
 * 
 * \brief 扫描码处理接口
 * 
 * \param kbd		键盘设备
 * \param scancode 	扫描码
 * 
 * \return 无
 */
void keycode_handler(struct kbd_device_struct *kbd, unsigned char scancode);

/*!
* \fn int keycode_module_init(struct kbd_device_struct *kbd)
* 
* \brief 键码模块初始化接口
* 
* \param kbd		键盘设备
* 
* \return 成功返回0
* \return 失败返回-1
*/
int keycode_module_init(struct kbd_device_struct *kbd);

/* @} */

#ifdef __cplusplus
}
#endif 

#endif



