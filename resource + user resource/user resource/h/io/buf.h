/*! @file buf.h
 *  @brief 缓冲模块模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 
/*
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks 提供的缓冲模块的外部接口
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2013-03-28，唐立三，修改注释中的错误
 * 		 2012-05-03，唐立三，建立 
 */
#ifndef _REWORKS_IOS_BUFFER_H_
#define _REWORKS_IOS_BUFFER_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <reworks/types.h>
#include <reworks/list.h>
	
/**
 * @defgroup group_os_io_buffer 缓冲模块
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	

/*!
 * \struct buffer_struct
 * 
 * \brief 缓冲模块数据结构
 */
struct buffer_struct;


/*!
 * \fn int ubuf_read(struct buffer_struct *buf, char *data, size_t count)
 * 
 * \brief 上层接口从缓冲区读取一块数据
 * 
 * \param buf 缓冲结构指针
 * \param data 保存读取数据的缓冲
 * \param count 读取的数据量（字节）
 * 
 * \return 成功返回实际读到的数据量
 * \return 失败返回-1
 * 
 */
int ubuf_read(struct buffer_struct *buf, char *data, size_t count);
	
/*!
 * \fn int ubuf_write(struct buffer_struct *buf, const char *data, size_t count)
 * 
 * \brief 上层接口向缓冲区写入一块数据
 * 
 * \param buf 缓冲结构指针
 * \param data 保存待写入数据的缓冲
 * \param count 要写入的数据量（字节）
 * 
 * \return 成功返回实际写入的数据量
 * \return 失败返回-1
 * 
 */
int ubuf_write(struct buffer_struct *buf, const char *data, size_t count);
	
/*!
 * \fn int lbuf_read(struct buffer_struct *buf, char *c)
 * 
 * \brief 下层接口从缓冲区读取一个字节的数据。
 * 
 * \param buf 缓冲结构指针
 * \param c 存放所读取字节数据的指针
 * 
 * \return 成功返回读取的字节数，即数字1
 * \return 失败返回-1
 * 
 */
int lbuf_read(struct buffer_struct *buf, char *c);
	
/*!
 * \fn int lbuf_write(struct buffer_struct *buf, const char c)
 * 
 * \brief 下层接口向缓冲区写入一个字节的数据。
 * 
 * \param buf 缓冲结构指针
 * \param c 待写入的字符
 * 
 * \return 成功返回写入的字节数，即数字1
 * \return 失败返回-1
 * 
 */
int lbuf_write(struct buffer_struct *buf, const char c);

/*!
 * \fn int buf_ioctl(struct buffer_struct *buf, int cmd, void *arg)
 * 
 * \brief 提供缓冲模块的控制功能
 * 
 * \param buf 缓冲结构指针
 * \param cmd 控制命令
 * \param arg 命令参数
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int buf_ioctl(struct buffer_struct *buf, int cmd, void *arg);

/*!
 * \fn int buf_poll(struct buffer_struct *buf, struct list_head *node)
 * 
 * \brief 提供缓冲模块的select功能
 * 
 * \param buf 缓冲结构指针
 * \param node select唤醒节点
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int buf_poll(struct buffer_struct *buf, struct list_head *node);
	
/*!
 * \fn int buf_delete(struct buffer_struct *buf)
 * 
 * \brief 销毁创建的缓冲
 * 
 * \param buf 缓冲结构指针
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int buf_delete(struct buffer_struct *buf);
	
/*!
 * \fn struct buffer_struct *buf_create(int rbuf_size, int wbuf_size, int read_count)
 * 
 * \brief 创建缓冲模块接口
 * 
 * buffer_create用于创建一个缓冲。用户可根据实际需求设计读写缓冲的大小以及上层读接口的阈值。
 * 上层读接口的阈值”的作用为设定上层读接口必须阻塞到读到的数据量，如果读缓冲区内的数据小于此阈值，则读接口将一直阻塞。
 * 
 * \param rbuf_size 读缓冲大小；如果不需要读缓冲区可以设为0；
 * \param wbuf_size 写缓冲大小；如果不需要写缓冲区可以设为0；
 * \param read_count 上层读接口阈值；
 * 
 * \return 成功返回创建的缓冲模块
 * \return 失败返回NULL
 */
struct buffer_struct *buf_create(int rbuf_size, int wbuf_size, int read_count);	
	
/* @} */

#ifdef __cplusplus
}
#endif

#endif
