/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为selelct功能的内部信息定义头文件
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2011-02-09，唐立三，整理
 * 
 */
#ifndef _REWORKS_IOS_POLL_H_
#define _REWORKS_IOS_POLL_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <sys/types.h>
#include <reworks/list.h>
#include <sys/errno.h>
#include <semaphore.h>
#include <select.h>	



#ifndef SIGNAL_REENTERANCY_PATCH	
#	define SIGNAL_REENTERANCY_PATCH	
#endif
#define POLLIN					0x0001
#define POLLPRI					0x0002
#define POLLOUT					0x0004
#define POLLERR					0x0008
#define POLLHUP					0x0010
#define POLLNVAL				0x0020

#define POLLRDNORM				0x0040
#define POLLRDBAND				0x0080
#define POLLWRNORM				0x0100
#define POLLWRBAND				0x0200
#define POLLMSG					0x0400

#define DEFAULT_POLLMASK 		(POLLIN | POLLOUT | POLLRDNORM | POLLWRNORM)
#define POLLIN_SET 				(POLLRDNORM | POLLRDBAND | POLLIN | POLLHUP | POLLERR)
#define POLLOUT_SET 			(POLLWRBAND | POLLWRNORM | POLLOUT | POLLERR)
#define POLLEX_SET 				(POLLPRI)
#define __IN(fds, n)			(fds->in + n)
#define __OUT(fds, n)			(fds->out + n)
#define __EX(fds, n)			(fds->ex + n)
#define __RES_IN(fds, n)		(fds->res_in + n)
#define __RES_OUT(fds, n)		(fds->res_out + n)
#define __RES_EX(fds, n)		(fds->res_ex + n)
#define BITS(fds, n)			(*__IN(fds, n)|*__OUT(fds, n)|*__EX(fds, n))
#ifndef BIT
#	define BIT(i)				(1UL << ((i)&(__NFDBITS-1)))
#endif 
#define ISSET(i,m)				(((i)&*(m)) != 0)
#define SET(i,m)				(*(m) |= (i))

#define FDS_BITPERLONG	(8 * sizeof(long))
#define FDS_LONGS(nr)	(((nr) + FDS_BITPERLONG - 1) / FDS_BITPERLONG)
#define FDS_BYTES(nr)	(FDS_LONGS(nr) * sizeof(long))

#define SI_COLL	0x0001	


#ifdef __cplusplus
}
#endif 

#endif
