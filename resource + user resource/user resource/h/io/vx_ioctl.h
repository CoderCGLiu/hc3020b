/*! @file vx_ioctl.h
 *  @brief ReWorks Vx兼容控制命令头文件
 *
 * @version 4.7
 * 
 * @see 《ReWorks应用开发编程指南附录C》
 */
 
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了VxWorks 5.5提供的ioctl命令
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2012-06-27，唐立三，建立
 * 
 */
 
#ifndef _REWORKS_IOS_VXWORKS_IOCTL_H_
#define _REWORKS_IOS_VXWORKS_IOCTL_H_

#ifdef __cplusplus
extern "C" 
{
#endif
	
/**
 * @defgroup group_os_io_vxioctl VxWorks控制命令
 * @ingroup group_os_io_cmd
 * 
 * 文件系统操作相关的控制命令详细用法，可参见《ReWorks应用开发编程指南附录C》
 * @{
 * 
 */	
	
#define FIOFLUSH			2		//!< 刷缓冲区内的数据 
#define FIOOPTIONS			3		//!< 设置选项(FIOSETOPTIONS) 
#define FIOBAUDRATE			4		//!< 设置串行波特率 
#define FIODISKFORMAT		5		//!< 格式化磁盘 
#define FIODISKINIT			6		//!< 初始化磁盘目录 
#define FIOSEEK				7		//!< 设置文件偏移 
#define FIOWHERE			8		//!< 获取文件偏移 
#define FIODIRENTRY			9		//!< 返回目录项
#define FIORENAME			10		//!< 重命名目录项 
#define FIOREADYCHANGE		11		//!< 如果设备介质变化则返回TRUE 
#define FIONWRITE			12		//!< 返回可写的字符数 */
#define FIODISKCHANGE		13		//!< 设置设备上的介质变化 
#define FIOCANCEL			14		//!< 取消对设备的读或写 
#define FIOSQUEEZE			15		//!< 在rt-11文件系统上挤出空洞 
#define FIONMSGS			17		//!< 返回管道中的消息数 
#define FIOGETNAME			18		//!< 返回文件名 
#define FIOGETOPTIONS		19		//!< 获取选项 
#define FIOSETOPTIONS		FIOOPTIONS  //!< 设置选项 
#define FIOISATTY			20		//!< 检测是否为TTY设备 
#define FIOSYNC				21		//!< 同步到磁盘 
#define FIOPROTOHOOK		22		//!< 指定协议钩子函数 
#define FIOPROTOARG			23		//!< 指定协议参数 
#define FIORBUFSET			24		//!< 更改读缓冲大小 
#define FIOWBUFSET			25		//!< 更改写缓冲大小 
#define FIORFLUSH			26		//!< 刷出读缓冲中的数据 
#define FIOWFLUSH			27		//!< 刷出写缓冲中的数据 
#define FIOSELECT			28		//!< 设置select 
#define FIOUNSELECT			29		//!< 断开select 
#define FIONFREE        	30      //!< 返回设备上的空闲空间 
#define FIOMKDIR        	31      //!< 创建目录 
#define FIORMDIR        	32      //!< 删除目录 
#define FIOLABELGET     	33      //!< 获取卷标 
#define FIOLABELSET     	34      //!< 设置卷标 
#define FIOATTRIBSET    	35      //!< 设置文件属性 
#define FIOCONTIG       	36      //!< 分配连续的空间 
#define FIOREADDIR      	37      //!< 读目录项(POSIX) 
#define FIOFSTATGET_OLD     38      /* get file status info */
#define FIOUNMOUNT      	39      //!< 卸载磁盘卷 
#define FIOSCSICOMMAND  	40      //!< 执行SCSI命令 
#define FIONCONTIG      	41      //!< 获取最大配置区域大小 
#define FIOTRUNC        	42      //!< 截断文件 
#define FIOGETFL        	43		//!< 获取文件模式 
#define FIOTIMESET      	44		//!< 修改文件访问时间 
#define FIOINODETONAME  	45		//!< 以指定的inode号返回文件名
#define FIOFSTATFSGET   	46      //!< 获取文件系统信息 
#define FIOMOVE         	47      //!< 移动文件 
#define FIORESETDIR     	48		//!< 重置目录流 
#define FIOGETBLKSIZE		49		//!< 获取块设备大小 
 
 /**
 * 扩展 
 */
#define FIORDCNT			100		//!< 设置读阈值 
#define FIOWRCNT			101		//!< 设置写阈值                                    
/**                                 
 * 64位控制码
 */
#define FIOCHKDSK			(100  | 6400 )	    //!< 硬盘检测(64位控制码) 
#define FIOCONTIG64			(FIOCONTIG | 6400) 	//!< 分配连续的空间(64位控制码) 
#define FIONCONTIG64		(FIONCONTIG | 6400) //!< 获取最大配置区域大小(64位控制码) 
#define FIONFREE64			(FIONFREE | 6400)   //!< 返回设备上的空闲空间(64位控制码) 
#define FIONREAD64			(FIONREAD | 6400)	//!< 读取文件可读字节数(64位控制码) 
#define FIOSEEK64			(FIOSEEK | 6400)  	//!< 设置文件偏移(64位控制码) 
#define FIOWHERE64			(FIOWHERE | 6400)   //!< 获取文件偏移(64位控制码) 
#define FIOTRUNC64			(FIOTRUNC | 6400)   //!< 截断文件(64位控制码) 

#define FIOCOMMITFS     56
#define FIODATASYNC     57  /* sync to I/O data integrity */
#define FIOLINK         58  /* create a link */
#define FIOUNLINK       59  /* remove a link */
#define FIOACCESS       60  /* support POSIX access() */
#define FIOPATHCONF     61  /* support POSIX pathconf() */
#define FIOFCNTL        62  /* support POSIX fcntl() */
#define FIOCHMOD        63  /* support POSIX chmod() */
#define FIOFSTATGET     64  /* get stat info - POSIX  */
#define FIOUPDATE       65  /* update dosfs default create option */

/* the next two ioctls are for block-type storage media (e.g., disks) */
#define FIODISCARDGET   70  /* does hardware want DISCARDs? */
#define FIODISCARD      71  /* mark sectors (sectorRange) as deleted */
#define CONTIG_MAX			-1	//!< /* "count" for FIOCONTIG if requesting maximum contiguous space on dev
#define FOLLOW_LINK			-2	//!< 驱动返回值	

#define DEFAULT_FILE_PERM	0000640	   //!< 默认文件permissions（unix风格）
#define DEFAULT_DIR_PERM	0000750	   //!< 默认目录permissions（unix风格）	


/**
 * 文件类型定义
 */
#define FSTAT_DIR			0040000		//!< 文件类型之目录 
#define FSTAT_CHR			0020000		//!< 文件类型之字符设备 
#define FSTAT_BLK			0060000		//!< 文件类型之块设备 
#define FSTAT_REG			0100000		//!< 文件类型之常规文件 
#define FSTAT_LNK			0120000		//!< 文件类型之符号链接 
#define FSTAT_NON			0140000		//!< 文件类型之命名套接字 

/**
 * 磁盘格式
 */
#define SS_1D_8				1	//!< 磁盘格式之单面单密度, 8"     
#define SS_2D_8				2	//!< 磁盘格式之单面双密度, 8"     
#define DS_1D_8				3	//!< 磁盘格式之双面单密度, 8"     
#define DS_2D_8				4	//!< 磁盘格式之双面双密度, 8"     
#define SS_1D_5				5	//!< 磁盘格式之单面单密度, 5 1/4" 
#define SS_2D_5				6	//!< 磁盘格式之单面双密度, 5 1/4" 
#define DS_1D_5				7	//!< 磁盘格式之双面单密度, 5 1/4" 
#define DS_2D_5				8	//!< 磁盘格式之双面双密度, 5 1/4" 
                                
#define HD_1				129	//!< 硬盘类型1 
#define HD_2				130	//!< 硬盘类型2 
	
#define MAX_DIRNAMES		32			//!< 最大目录项名称
#define MAX_FILENAME_LENGTH	(255 + 1)	//!< 最大文件名

/**
 * I/O状态码
 */
#define S_ioLib_NO_DRIVER				(M_ioLib | 1)  	//!< I/O状态码 
#define S_ioLib_UNKNOWN_REQUEST			(M_ioLib | 2)  	//!< I/O状态码 
#define S_ioLib_DEVICE_ERROR			(M_ioLib | 3)	//!< I/O状态码 
#define S_ioLib_DEVICE_TIMEOUT			(M_ioLib | 4)   //!< I/O状态码 
#define S_ioLib_WRITE_PROTECTED			(M_ioLib | 5)   //!< I/O状态码 
#define S_ioLib_DISK_NOT_PRESENT		(M_ioLib | 6)   //!< I/O状态码 
#define S_ioLib_NO_FILENAME				(M_ioLib | 7)	//!< I/O状态码 
#define S_ioLib_CANCELLED				(M_ioLib | 8)   //!< I/O状态码 
#define S_ioLib_NO_DEVICE_NAME_IN_PATH	(M_ioLib | 9)   //!< I/O状态码 
#define S_ioLib_NAME_TOO_LONG			(M_ioLib | 10)	//!< I/O状态码 
#define S_ioLib_UNFORMATED				(M_ioLib | 11)  //!< I/O状态码 
#define S_ioLib_CANT_OVERWRITE_DIR      (M_ioLib | 12)  //!< I/O状态码 
	
/* @} */
	
#ifdef __cplusplus
}
#endif

#endif
