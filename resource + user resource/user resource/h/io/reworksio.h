/*! @file reworksio.h
 *  @brief ReWorks I/O模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_IOS_REWORKSIO_H_
#define _REWORKS_IOS_REWORKSIO_H_

#ifdef __cplusplus
extern "C"
{
#endif 
#include <sys/time.h>
#include <sys/fcntl.h>
#include <sys/syslimits.h>
#include <reworks/types.h>
#include <dirent.h>
#include <errno.h>
#include <stat.h>
#include <utime.h>
#include <devnum.h>
#include <device.h>
#include <io_old.h>

/**
 * @defgroup group_os_io_api I/O应用编程接口
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	
/**
 * 标准IO文件描述符
 */
#define	STD_IN			0  //!< 标准输入文件描述符
#define	STD_OUT			1  //!< 标准输出文件描述符
#define	STD_ERR			2  //!< 标准错误输出文件描述符

/*!
 * \fn int fd_used_show(void)
 * 
 * \brief 显示文件描述符的使用情况。
 * 
 * \param 无
 * 
 * \exception 无
 * 
 * \return 成功返回已使用的文件描述符量
 * \return 失败返回-1
 * 
 */
int fd_used_show(void);

/*!
 * \fn void global_std_io_show(void) 
 * 
 * \brief 显示全局标准I/O重定向设备名称。
 * 
 * \param 无
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void global_std_io_show(void);

/*!
 * \fn void task_std_io_show(pthread_t pid) 
 * 
 * \brief 显示指定任务标准I/O重定向设备名称。
 * 
 * \param pid 任务ID
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void task_std_io_show(pthread_t pid);

/*!
 * \fn void df(void)
 * 
 * \brief 显示系统中所有挂载点的信息。
 * 
 * \param 无
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void df(void);
/*!
 * \fn void df_by_mp(const char *mount_point)
 * 
 * \brief 显示指定挂载点的信息
 * 
 * \param mount_point 挂载点路径名
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void df_by_mp(const char *mount_point);

/*!
 * \fn void df_by_devname(const char *devname)
 * 
 * \brief 显示系统中指定块设备上挂载点的信息。
 * 
 * \param devname 挂载点设备名
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void df_by_devname(const char *devname);

/*!
 * \fn int mount(const char *fsname, const char *devname, const char *mount_point)
 * 
 * \brief 挂载文件系统接口。
 * 
 * 将参数devname指定的块设备使用参数fdname指定的文件系统类型挂载到路径mount_point上，然后可通过挂载点访问相应块设备上的内容。
 * 
 * \param fsname 文件系统（类型）名称
 * \param devname 块设备路径
 * \param mount_point 文件系统挂载点
 * 
 * \exception EINVAL 参数fsname无效、mount_point已被占用
 * \exception ENOENT 参数devname或mount_point为空、devname没有指向有效的文件
 * \exception ENAMETOOLONG 参数devname或mount_point超过了PATH_MAX限制或devname或mount_point中的某个目录项超过了NAME_MAX的限制
 * \exception ENODEV 参数devname对应的文件不是块设备文件
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int mount(const char *fsname, const char *devname, const char *mount_point);	


/*!
 * \fn int umount(const char *mount_point)
 * 
 * \brief 卸载文件系统接口。本接口用于卸载通过mount接口挂载的文件系统。
 * 
 * \param mount_point 文件系统挂载点
 * 
 * \exception ENOENT 参数mount_point为空
 * \exception ENAMETOOLONG 参数mount_point超过了PATH_MAX限制或mount_point中的某个目录项超过了NAME_MAX的限制
 * \exception EINVAL 参数mount_point没有mount过，或mount_point对应的文件系统不存在
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int umount(const char *mount_point);


/*!
 * \fn int format(const char *fsname, const char *devname)
 * 
 * \brief 存储设备格式化接口
 * 
 * \param fsname 存储设备要格式化的文件系统类型
 * \param devname 存储设备路径
 * 
 * \exception 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int format(const char *fsname, const char *devname);


/*!
 * \fn int open(const char *path, int oflag, ...)
 * 
 * \brief 打开指定的文件（包括设备文件）
 * 
 * 打开指定的文件（包括设备文件），并返回打开文件对应的文件描述符（File Descriptor，FD）。
 * FD用于I/O模块中的其他接口（例如read、write、close等）操作响应的文件。
 * 
 * 
 * \param path （设备）文件路径。可以为相对路径，可以为绝对路径，path长度不能超过PATH_MAX
 * \param oflag 打开选项。 
 * flag选项由以下选项“二进制或”构成，用户必须指定O_RDONLY、O_WRONLY和O_RDWR中的一个参数：<br/>
 *  O_RDONLY：以只读方式打开文件。<br/>
 *  O_WRONLY：以只写方式打开文件。<br/>
 *  O_RDWR：以可读写方式打开文件。<br/>
 * open接口还可使用以下参数的组合。<br/>
 * O_CREAT：若欲打开的文件不存在，则自动建立该文件。如果文件已经存在，则O_CREAT对open接口不产生影响<br/>
 * O_TRUNC：若打开的文件为常规文件并且存在，则将文件截断（文件长度为0，文件占用的空间释放）。
 * 如果以可写（O_WRONLY或O_RDWR）的方式打开，则截断后的文件可写；如果以只读（O_RDONLY）的方式打开，则截断
 * 后的文件只读（执行写操作时返回EBADF）<br/>
 * O_APPEND：当读文件时会从文件尾开始移动，也就是所写入的数据会以附加的方式加入到文件的后面。<br/>
 * \param ... ReWorks当前不支持此选项
 * 
 * \exception EEXIST 同时设置了O_CREAT和O_EXCL选项，且文件存在
 * \exception EINVAL 参数oflag无效
 * \exception EIO 打开文件的过程中出现错误
 * \exception EISDIR 打开的文件为目录，且设置了O_WRONLY或O_RDWR
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENFILE 系统中打开的文件超过了最大值
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * \exception ENXIO 打开的文件为特殊字符或块文件，但与此特殊文件关联的设备不存在
 * \exception EROFS 文件系统为只读类型，但设置了O_WRONLY、O_RDWR、O_CREAT或O_TRUNC
 * 
 * \return 成功返回表示当前文件描述符中最小未使用的非负整数<br/>
 * \return 失败返回-1，并且设置了相应的errno<br/>
 * 
 */
int open(const char *path, int oflag, ...);


/*!
 * \fn int creat(const char *path, mode_t mode)
 * 
 * \brief 创建指定的文件
 * 
 * 创建指定的文件，本接口等价于open(path, O_WRONLY | O_CREAT | O_TRUNC, mode)
 * 
 * \param path （设备）文件路径。
 * 可以为相对路径，可以为绝对路径，pathname长度不能超过PATH_MAX
 * \param mode ReWorks不支持此选项，用户可默认使用0777
 * 
 * \exception 异常 见open()接口
 * 
 * \return 成功返回表示当前文件描述符中最小未使用的非负整数
 * \return 失败返回-1，并且设置了相应的errno
 * 
 */
int creat(const char *path, mode_t mode);


/*!
 * 	\fn int close(int fildes)
 * 
 * \brief 关闭文件描述符对应的文件
 * 
 * 关闭文件描述符对应的文件，close()操作完成后将释放参数fd指定文件描述符，并且相关的I/O操作
 * （例如read、write、close等）所使用的文件描述符将不再有效。
 * 
 * \param fildes 要关闭的文件描述符
 * 
 * \exception EBADF 文件描述符filedes无效
 * 
 * \return 成功返回0
 * \return 失败返回-1，并且设置了相应的errno 
 * 
 */
int close(int fildes);


/*!
 * \fn int ioctl(int fildes, int request, ...) 
 * 
 * \brief 执行各种控制操作
 * 
 * ioctl接口将执行各种控制操作。ioctl接口的实际上是负责调用文件、设备的提供的
 * ioctl接口。
 * 
 * \param fildes 指定ioctl操作（设备）文件对应的文件描述符
 * \param request和... 由执行ioctl操作的（设备）文件解释和执行，具体用法可参见《ReWorks应用开发编程指南附录C》
 * 
 * \exception EBADF 文件描述符filedes无效
 * \exception ENODEV 文件描述符对应的文件不支持ioctl操作
 * \exception EIO 设备物理I/O错误
 * \exception ENXIO 对设备驱动而言，参数有效，只是不能被一些特殊子设备执行
 * 
 * \return 操作成功的返回值由文件、设备的ioctl接口决定
 * \return 失败返回-1，并且设置了相应的errno 
 * 
 */
int ioctl(int fildes, int request, ...);


/*!
 * \fn ssize_t read(int fildes, void *buf, size_t nbyte)
 * 
 * \brief 数据读取接口
 * 
 * read()接口将试图从fildes指定的（设备）文件中读取nbyte个字节存入buf指向的缓冲区中。
 * 如果nbyte等于0且read操作未发生错误则read接口将返回0.
 * 
 * \param fildes 文件描述符
 * \param buf 数据缓冲区
 * \param nbyte 待读取数据字节数
 * 
 * \exception EBADF 文件描述符filedes无效
 * \exception EINVAL 参数fildes关联的文件不适合执行read操作
 * \exception EISDIR 指定的参数文件描述符fildes为目录，而目录不允许使用read操作读取
 * \exception EIO 设备物理I/O错误
 * \exception ENODEV 文件描述符对应的文件不支持read操作
 * 
 * \return 成功返回实际读取的字节数
 * \return 失败返回-1，并且设置了相应的errno 
 * 
 */
ssize_t read(int fildes, void *buf, size_t nbyte);


/*!
 * \fn ssize_t write(int fildes, const void *buf, size_t nbyte)
 * 
 * \brief 数据写入接口
 * 
 * write接口试图将buf指向的缓冲区中的nbyte个字节写入fildes关联的（设备）文件中。
 * 对于常规文件或支持lseek操作的文件，write接口将从fildes关联的文件偏移（offset）的位置开始写入；
 * 对于不支持lseek操作的设备而言，write接口将从当前位置写入。
 * 如果nbyte等于0且write操作未发生错误则write接口将返回0.
 * 
 * \param fildes 文件描述符
 * \param buf 数据缓冲区
 * \param nbyte 待写入数据字节数
 * 
 * \exception EBADF 文件描述符filedes无效
 * \exception EINVAL 参数fildes关联的文件不适合执行write操作
 * \exception EIO 设备物理I/O错误
 * \exception ENODEV 文件描述符对应的文件不支持write操作
 * 
 * \return 成功将返回实际写入的字节数
 * \return 失败返回-1，并设置errno
 */
ssize_t write(int fildes, const void *buf, size_t nbyte);


/*!
 * \fn int remove(const char *path)
 * 
 * \brief remove接口用于移除指定的文件或目录。
 * 
 * 对于文件而言，本接口等价于unlink接口；对于目录而言；本接口等价于rmdir接口
 * 
 * \param path 待移除文件或目录路径
 * 
 * \exception ENAMETOOLONG 打开文件的路径名超过PATH_MAX或路径中的文件名超过了NAME_MAX
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置errno
 */ 
int remove(const char *path);


/*!
 * \fn off_t lseek(int fildes, off_t offset, int whence)
 * 
 * \brief 修改文件偏移接口
 * 
 * lseek接口用于设置文件描述符fildes关联的文件的文件偏移位置。
 * lseek允许设置文件偏移超过文件大小，如果设置的文件偏移超过文件大小，则以0填充扩展的区域。
 * 
 * \param fildes 文件描述符
 * \param offset 相对于方向whence的偏移
 * \param whence SEEK_SET 读写位置为offset
 * 				 SEEK_CUR 读写位置为当前位置加offset
 * 				 SEEK_END 读写位置为文件大小加offet
 * 
 * \exception EBADF 文件描述符filedes无效
 * \exception EINVAL whence非SEEK_SET、SEEK_CUR和SEEK_END其中之一，或	文件偏移为负数
 * \exception EOVERFLOW 文件偏移不能使用off_t类型表示
 * \exception ESPIPE 与fildes关联的是管道、套接字或FIFO
 * 
 * \return 成功返回文件当前的读写位置
 * \return 失败返回-1，并设置相应的errno
 *
 */
off_t lseek(int fildes, off_t offset, int whence);


/*!
 * \fn int mkdir(const char *path, mode_t mode)
 * 
 * \brief 创建目录接口
 * 
 * mkdir接口用于创建路径path指定的目录；由于ReWorks不支持权限设置，因而用户可将参数mode
 * 设置默认值0777。	
 * 
 * \param path 待创建的目录路径
 * \param mode 目录操作权限
 * 
 * \exception EEXIST 路径path已经存在
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 要打开的文件不存在或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 *  
 */
int mkdir(const char *path, mode_t mode);


/*!
 * \fn int rmdir(const char *path)
 * 
 * \brief 删除目录接口
 * 
 * \param path 移除path指向的目录
 * 
 * \exception EBUSY path为当前的工作目录
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * \exception ENOTEMPTY path目录中包括除“.”和“..”以外的目录项
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int rmdir(const char *path);

/*!
 * \fn DIR *opendir(const char *dirname)
 * 
 * \brief 打开目录接口
 * 
 * \param dirname 要打开的目录路径
 * 
 * \exception ENFILE 打开了太多文件
 * \exception ENOENT 要打开的文件不存在，或文件路径不存在，或文件路径为空
 * \exception ENOMEM 没有足够的内存
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回目录结构DIR流
 * \return 失败返回NULL，并设置相应的errno
 * 
 */
DIR *opendir(const char *dirname);


/*!
 * \fn struct dirent *readdir(DIR *dirp)
 * 
 * \brief 读取目录内容接口
 * 
 * readdir接口返回参数dirp指向目录当前位置的目录项结构dirent。
 * 
 * \param dirp 要读取的目录流
 * 
 * \exception EBADF 参数dirp未指向目录流
 * \exception ENOENT 目录流中当前位置无效
 * 
 * \return 成功返回目录当前目录项对应的目录项结构；如果DIR到达尾部则返回NULL，但不设置errno
 * \return 失败返回NULL，并设置相应的errno
 * 
 */
struct dirent *readdir(DIR *dirp);


/*!
 * \fn void rewinddir(DIR *dirp)
 * 
 * \brief 重置目录偏移接口
 * 
 * rewinddir操作重置目录流的当前位置为起始位置。rewinddir操作将目录流设置到执行opendir操作之后的状态。
 * 
 * \param dirp 要重置的目录流
 * 
 * \exception EBADF 参数dirp未指向目录流
 * 
 * \return 无 
 * 
 */
void rewinddir(DIR *dirp);


/*!
 * \fn int closedir(DIR *dirp)
 * 
 * \brief 关闭目录接口
 * 
 * closedir操作用于关闭dirp指向的目录流。关闭目录流后，dirp不再指向有效的DIR对象。
 * 
 * \param dirp 要关闭的目录流
 * 
 * \exception EBADF dirp未指向有效的目录流
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno 
 */
int closedir(DIR *dirp);


/*!
 * \fn int unlink(const char *path)
 * 
 * 
 * \brief 删除文件
 * 
 * \param path 待删除的文件
 * 
 * \exception EINVAL 参数paty指向的路径不是常规文件
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 要打开的文件不存在，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * \exception EPERM 路径为目录，但unlink不允许删除目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno 
 * 
 */
int unlink(const char *path);


/*!
 * \fn int move(const char *src, const char *dest)
 * 
 * \brief 移动文件接口
 * 
 * mv操作将src指向的文件或目录移动至dest指向的文件或目录；
 * 如果参数src和参数dest指向的路径相同，则mv不执行任何操作直接返回。
 * 
 * \param src 要移动的文件或目录
 * \param dest 要移动到的文件或目录
 * 
 * \exception ENAMETOOLONG 参数src或参数dest长度超过PATH_MAX或参数src或参数dest中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 参数src指向的文件或目录不存在
 * \exception ENOTDIR 参数src指向的是目录；而dest指向的文件已存在，且不是目录
 * \exception EINVAL 参数dest指向的路径为参数src指向路径的子目录或子文件
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int move(const char *src, const char *dest);


/*!
 * \fn ssize_t sendfile(int out_fd, int in_fd, off_t *offset, size_t count)
 * 
 * \brief 直接文件数据拷贝接口
 * 
 * sendfile接口将in_fd关联的文件的偏移offset处的count字节数据拷贝至out_fd关联
 * 的文件中。完成拷贝后，参数offset被设置为当前读取的数据的下一个字节处。sendfile接口
 * 不修改参数in_fd关联文件的偏移量。
 * 
 * \param out_fd 数据目的文件描述符
 * \param in_fd 数据来源文件描述符
 * \param offset 数据来源文件偏移
 * \param count 拷贝的数据量
 * 
 * \exception EBADF 参数文件描述符out_fd或参数文件描述符in_fd无效
 * 
 * \return 成功返回实际写入out_fd的数据字节数
 * \return 失败返回-1，并设置相应的errno
 */
ssize_t sendfile(int out_fd, int in_fd, off_t *offset, size_t count);


/*!
 * \fn ssize_t pread(int fildes, void *buf, size_t count, off_t offset)
 * 
 * \brief 不修改文件偏移的数据读取接口
 * 
 * pread接口从文件描述符fildes关联的文件的偏移offset处读取count个字节的数据，但不改变参数fildes关联的文件的偏移。
 * 
 * \param fildes 文件描述符
 * \param buf 存放读取数据的缓冲区
 * \param count 待读取数据量
 * \param offset 读取数据的偏移
 * 
 * \exception EBADF 参数文件描述符fildes无效
 * 
 * \return 成功返回读取到的数据字节数
 * \return 失败返回-1，并设置相应的errno
 * 
 */ 
ssize_t pread(int fildes, void *buf, size_t count, off_t offset);


/*!
 * \fn ssize_t pwrite(int fildes, const void *buf, size_t count, off_t offset)
 * 
 * \brief 不修改文件偏移的数据写入接口
 * 
 * pwrite接口向文件描述符fildes关联的文件的偏移offset处写入count个字节的数据，但不改变参数fildes关联的文件的偏移。
 * 
 * \param fildes 文件描述符
 * \param buf 存放读取数据的缓冲区
 * \param count 待读取数据量
 * \param offset 写数据的偏移
 * 
 * \exception EBADF 文件描述符fildes无效
 * 
 * \return 成功返回写入的数据字节数
 * \return 失败返回-1，并设置相应的errno
 * 
 */ 
ssize_t pwrite(int fildes, const void *buf, size_t count, off_t offset);


/*!
 * \fn int chdir(const char *dirname)
 * 
 * \brief 设置工作路径接口
 * 
 * chdir接口将参数dirname指向的目录设置为当前的工作路径。chdir接口功能等价于cd接口。
 * 
 * \param dirname 要设定的工作路径
 * 
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int chdir(const char *dirname);


/*!
 * \fn char *getwd(char *pathname)
 * 
 * \brief 获取当前工作路径接口
 * 
 * getwd接口返回当前工作的绝对路径。getwd接口将当前工作的绝对路径拷贝至参数pathnam中指定的缓冲区中。
 * 
 * \param pathname 存放当前工作的缓冲区
 * 
 * \exception 无
 * 
 * \return 成功返回参数pathname指向的缓冲区地址
 * \return 失败返回NULL
 * 
 */
char *getwd(char *pathname);


/*!
 * \fn char *getcwd(char *buf, size_t size)
 * 
 * \brief 获取当前工作路径接口
 * 
 * getcwd接口返回当前工作的绝对路径。getwd接口将当前工作的绝对路径拷贝至参数buf中指定的缓冲区中。缓冲区大小有参数size决定。
 * 
 * \param buf 存放当前工作的缓冲区
 * \param size 缓冲区大小
 * 
 * \exception EINVAL 参数size为0或参数buf为NULL
 * \exception ERANGE 参数size大于0，但小于当前工作的绝对路径的长度（含结束符）
 * 
 * \return 成功返回参数buf指向的缓冲区地址
 * \return 失败返回NULL
 * 
 */
char *getcwd(char *buf, size_t size);


/*!
 * \fn void pwd(void)
 * 
 * \brief 显示当前工作的绝对路径接口。
 * 
 * \param 无
 * 
 * \exception 无
 * 
 * \return 无
 * 
 */
void pwd(void);


/*!
 * \fn int rename(const char *src, const char *dest)
 * 
 * \brief rename接口用于重命名一个文件或目录 
 * 
 * 
 * (1)  如果old和new指向了相同的文件，则不做任何操作，直接返回；\n
 * (2)  如果old指向的是文件而非目录，则new不应指向目录；\n
 * (3)  如果new指向的文件（POSIX使用术语link）存在，那么应报错；\n
 * (4)  如果old指向的是目录，则new不应指向文件而也应为目录；\n
 * (5)  如果new指向的目录存在，则应首先移除，然后进行重命名；如果移除目录，那么new应为空目录；
 * 
 * \param src 待重命名的文件
 * \param dest 新文件名称
 * 
 * \exception EXDEV 参数src或dest指向的路径不属于某个文件系统或属于不同的文件系统
 * \exception EINVAL 参数src或dest的父目录不相同
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENFILE 系统中打开的文件超过了最大值
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int rename(const char *src, const char *dest);


/*!
 * \fn int fsync(int fildes)
 * 
 * \brief fsync接口将与fildes关联的文件存储到存储设备上。
 * 
 * \param fildes 文件描述符
 * 
 * \exception EBADF 文件描述符fildes无效
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 */
int fsync(int fildes);


/*!
 * \fn int fcntl(int fildes, int cmd, ...)
 * 
 * \brief fcntl接口对文件描述符fd执行cmd命令。
 * 
 * 
 * 当前ReWorks支持的cmd命令为：F_DUPFD，F_GETFD，F_SETFD，F_GETFL，F_SETFL。
 * 
 * \param fildes 文件描述符
 * \param cmd fcntl命令
 * 
 * \return 成功返回值与cmd命令相关
 * \return 失败返回-1
 * 
 */
int fcntl(int fildes, int cmd, ...);


/*!
 * \fn int fstat(int fildes, struct stat *st)
 * 
 * \brief fstat接口用于获取文件相关信息，并设置到参数st中。
 * 
 * \param fildes 要获取信息的文件描述符
 * \param st 存放文件信息的struct stat类型指针
 * 
 * \exception EBADF 文件描述符fildes无效
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int fstat(int fildes, struct stat *st);


/*!
 * \fn int stat(const char *path, struct stat *st)
 * 
 * \brief 获取文件或目录信息
 * 
 * stat用于获取文件相关信息，并设置到参数st。
 * 
 * \param path 指向获取信息的文件路径
 * \param st 存放文件信息的struct stat类型指针
 * 
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int stat(const char *path, struct stat *st);


/*!
 * \fn int isatty(int fildes)
 * 
 * \brief isatty接口用于检查文件描述符fd执行的设备是否为TTY设备
 * 
 * \param fildes 文件描述符
 * 
 * \exception EBADF 文件描述符fildes无效
 * \exception ENOTTY 与文件描述符关联的设备非TTY设备
 * 
 * \return 如果fildes关联的设备为为TTY设备则返回1
 * \return 如果fildes关联的设备非TTY设备返回0，并设置相应的errno 
 * 
 */
int isatty(int fildes);


/*!
 * \fn int ftruncate(int fildes, off_t length)
 * 
 * \brief 截断文件
 * 
 * 设置path指向的文件大小为length。如果原文件长度大于length，则超出部分的数据将被丢弃；
 * 如果原文件从长度小于length，则文件将被扩展，扩展部分用0填充。
 * truncate不修改与此文件关联的文件描述符对应的文件偏移。
 * 
 * \param fildes 待操作的文件描述符
 * \param length 新文件长度
 * 
 * \exception EBADF 文件描述符fildes无效
 * \exception EIO 读写文件过程中发生IO错误
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int ftruncate(int fildes, off_t length);


/*!
 * \fn int truncate(const char *path, off_t length)
 * 
 * \brief 截断文件
 * 
 * 设置文件描述符关联文件大小为length。如果原文件长度大于length，则超出部分的数据将被丢弃；
 * 如果原文件从长度小于length，则文件将被扩展，扩展部分用0填充。
 * truncate不修改与此文件关联的文件描述符对应的文件偏移。
 * 
 * 
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 要打开的文件不存在且未设置O_CREAT，或文件路径不存在，或文件路径为空
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * \exception EISDIR 路径path指向的文件是一个目录而非常规文件
 * \exception EIO 读写文件过程中发生IO错误
 * 
 * \param path 文件路径
 * \param length 新文件长度
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int truncate(const char *path, off_t length);


/*!
 * \fn int dup(int fildes)
 * 
 * \brief 文件描述符拷贝接口
 * 
 * dup接口用于创建一个文件描述符的拷贝，该拷贝为当前系统内未使用的最小的文件描述符。
 * 新旧文件描述符共享文件偏移、flags等信息。例如如果使用lseek改变一个文件描述符的文件偏移，则对另一个同样有效。
 * 
 * \param fildes 原文件描述符
 * 
 * \exception EBADF 文件描述符fildes无效
 * 
 * \return 成功返回新的文件描述符
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int dup(int fildes);


/*!
 * \fn int dup2(int fildes, int fildes2)
 * 
 * \brief 文件描述符拷贝接口
 * 
 * dup2接口用于创建一个文件描述符的拷贝，该拷贝由参数fildes2指定。新旧文件描述符共享文件偏移、
 * flags等信息。例如如果使用lseek改变一个文件描述符的文件偏移，则对另一个同样有效。如果参数fildes2指向的
 * 文件描述符已被使用，则首先对参数fildes2调用close操作，然后执行文件描述符拷贝dup操作。
 * 
 * \param fildes 原文件描述符
 * \param fildes2 新文件描述符 
 * 
 * \exception EBADF 文件描述符fildes无效，或参数fildes2不在有效的文件描述符范围内。
 * 
 * \return 成功返回新的文件描述符
 * \return 失败返回-1，并设置相应的errno
 * 
 */
int dup2(int fildes, int fildes2);


/*!
 * \fn int ls(char *path)
 * 
 * \brief ls接口用于显示参数path指定的文件或目录信息；
 * 
 * 
 * 如果path为文件，则显示该文件信息；\n
 * 如果path为目录，则显示该目录内文件或目录的信息；\n
 * 如果path为NULL，则显示当前工作路径信息。
 * 
 * \param path 文件或目录路径
 * 
 * \exception 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int ls(char *path);


/*!
 * \fn int cat(const char *path)
 * 
 * \brief cat接口用于显示指定文件的内容。
 * 
 * cat接口仅用于显示常规文件的内容；而不能用于显示目录、设备文件的内容。
 * 
 * \param path 文件路径
 * 
 * \exception EINVAL 无法获取文件属性或待显示文件类型不是常规文件
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int cat(const char *path);


/*!
 * \fn int mkdir_path(const char *path, mode_t mode)
 * 
 * \brief 创建目录（路径）
 * 
 * mkdir_path用于创建路径path指定的目录；由于ReWorks不支持权限设置，因而用户可对mode
 * 设置默认值0777。
 * mkdir_path与mkdir的区别在于，mkdir参数path指定路径上的目录项必须已经存在，否则创建失败；
 * 而mkdir_path检查参数path指定路径上的目录是否存在，如果不存在则创建之。
 * 
 * 
 * \param path 待创建的目录路径名
 * \param mode 目录权限
 * 
 * \exception ENAMETOOLONG 路径path长度超过PATH_MAX或path中某个目录项名称长度超过NAME_MAX
 * \exception ENOTDIR 文件路径中某个目录项不是目录
 * 
 * \return 成功返回0
 * \return 失败返回-1，并设置相应的错误号
 *  
 */
int mkdir_path(const char *path, mode_t mode);


/*!
 * \fn void global_std_set(int stdfd, int newfd)
 * 
 * \brief 全局标准I/O设置接口
 * 
 * global_std_set接口将文件描述符newfd作为全局标准IO（标准输入、标准输出和标准错误，由stdfd确定本次设置），
 * 参数stdfd用于指定当前设置的标准IO。
 * 
 * \param stdfd 标准IO（0-标准输入、1-标准输出和2-标准错误）
 * \param newfd 要设置为标准IO的文件描述符
 * 
 * \exception EBADF 文件描述符stdfd无效
 * 
 * \return 无
 * 
 */
void global_std_set(int stdfd, int newfd);


/*!
 * \fn int global_std_get(int stdfd)
 * 
 * \brief 全局标准I/O获取接口
 * 
 * global_std_get接口用于获取全局标准IO（标准输入、标准输出和标准错误，由stdfd确定本次设置），
 * 对应的实际的文件描述符
 * 
 * \param stdfd 标准IO（0-标准输入、1-标准输出和2-标准错误）
 * 
 * \exception EBADF 文件描述符stdfd无效
 * 
 * \return 如果全局标准I/O已设置则返回
 * \return 如果全局标准I/O未设置则返回-1
 * 
 */
int global_std_get(int stdfd);


/*!
 * \fn int task_std_set(int tid, int stdfd, int newfd)
 * 
 * \brief 任务标准I/O设置接口
 * 
 * task_std_set接口将文件描述符newfd作为任务标准IO（标准输入、标准输出和标准错误，由stdfd确定本次设置），
 * 参数stdfd用于指定当前设置的标准IO。
 * 
 * \param tid 要设置的任务ID
 * \param stdfd 标准IO（0-标准输入、1-标准输出和2-标准错误）
 * \param newfd 要设置为标准IO的文件描述符
 * 
 * \exception EBADF 文件描述符stdfd无效
 * 
 * \return 无
 * 
 */
int task_std_set(pthread_t tid, int stdfd, int newfd);


/*!
 * \fn int task_std_get(pthread_t tid, int stdfd)
 * 
 * \brief 任务标准I/O获取接口
 * 
 * task_std_get接口用于获取任务标准IO（标准输入、标准输出和标准错误，由stdfd确定本次设置），
 * 对应的实际的文件描述符
 * 
 * \param tid 要获取标准I/O的任务ID
 * \param stdfd 标准IO（0-标准输入、1-标准输出和2-标准错误）
 * 
 * \exception EBADF 文件描述符stdfd无效
 * 
 * \return 如果任务标准I/O已设置则返回
 * \return 如果任务标准I/O未设置则返回-1
 * 
 */
int task_std_get(pthread_t tid, int stdfd);


/*!
 * \fn int access(const char *path, int amode)
 * 
 * \brief access接口用于检查文件是否存在。
 * ReWorks access接口不区分文件的读R_OK、写W_OK、执行X_OK三个权限参数模式，而仅支持
 * 检查文件是否存在的模式F_OK；但对于读R_OK、写W_OK、执行X_OK这三个权限模式，只要
 * 文件存在也一律返回0 
 * 
 * \param path 待检查的文件
 * \param amode 待检查的访问权限，目前仅支持F_OK选项
 * 
 * \return 文件存在返回0
 * \return 文件不存在返回-1 
 */
int access(const char *path, int amode);


/*!
 * \fn int xcopy(const char *src, const char *dest)
 * 
 * \brief 将src指向的文件或目录拷贝至dest指向的位置。
 * 
 * \param src 待拷贝的文件或目录位置
 * \param dest 文件或目录拷贝至的位置
 * 
 * \exception ENAMETOOLONG 参数src或参数dest长度超过PATH_MAX或参数src或参数dest中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 参数src指向的文件或目录不存在
 * \exception ENOTDIR 参数src指向的是目录；而dest指向的文件已存在，且不是目录
 * \exception EINVAL 参数dest指向的路径为参数src指向路径的子目录或子文件
 * 
 * \return 成功返回0
 * \return 成功返回-1 
 */
int xcopy(const char *src, const char *dest);


/*!
 * \fn int xdelete(const char *src)
 * 
 * \brief 将src指向的文件或目录删除。
 * 
 * \param src 待删除的文件或目录位置
 * 
 * \return 成功返回0
 * \return 成功返回-1 
 */
int xdelete(const char *src);

/*!
 * \fn int copy(const char *src, const char *dest)
 * 
 * \brief 将src指向的文件或目录拷贝至dest指向的位置。本接口功能等价于xcopy。
 * 
 * \param src 待拷贝的文件或目录位置
 * \param dest 文件或目录拷贝至的位置
 * 
 * \exception ENAMETOOLONG 参数src或参数dest长度超过PATH_MAX或参数src或参数dest中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 参数src指向的文件或目录不存在
 * \exception ENOTDIR 参数src指向的是目录；而dest指向的文件已存在，且不是目录
 * \exception EINVAL 参数dest指向的路径为参数src指向路径的子目录或子文件
 * 
 * \return 成功返回0
 * \return 成功返回-1 
 */
int copy(const char *src, const char *dest);

/*!
 * \fn int utime(const char *path, const struct utimbuf *times)
 * 
 * \brief 设置文件的访问和修改时间
 * 
 * utimes接口用于根据参数times设置参数path文件的最后访问和修改时间。
 * 
 * \param path 文件路径
 * \param times 文件的最后访问和修改时间
 * 
 * \exception ENAMETOOLONG 参数path长度超过PATH_MAX或参数path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 参数path指向的文件或目录不存在
 * \exception ENOTDIR 参数path中某个目录项不是目录
 * \exception EINVAL 参数path不合法
 * 
 * \return 成功返回0
 * \return 成功返回-1 
 */
int utime(const char *path, const struct utimbuf *times);

/*!
 * \fn int utimes(const char *path, const struct timeval times[2])
 * 
 * \brief 设置文件的访问和修改时间
 * 
 * utimes接口用于根据参数times设置参数path文件的最后访问和修改时间。参数times为
 * struct timeval类型的数组，数组的第一个成员用于设置最后访问时间，数组的第二个成员
 * 用于设置最后修改时间。
 * 	如果参数times为空，则使用当前时间进行设置。
 * 	按照POSIX规范，最后访问和修改时间能够精确到微秒，但当前版本的ReWorks仅能够将
 * 精确到秒，ReWorks将自动丢弃掉struct timeval中的微秒时间。
 * 
 * \param path 文件路径
 * \param times 文件的最后访问和修改时间
 * 
 * \exception ENAMETOOLONG 参数path长度超过PATH_MAX或参数path中某个目录项名称长度超过NAME_MAX
 * \exception ENOENT 参数path指向的文件或目录不存在
 * \exception ENOTDIR 参数path中某个目录项不是目录
 * \exception EINVAL 参数path不合法
 * 
 * \return 成功返回0
 * \return 成功返回-1 
 */
int utimes(const char *path, const struct timeval times[2]);


/** @example serial_read.c
 * 下面的例子演示了如何使用read接口从串口设备中读取数据.
 */ 

/** @example serial_write.c
 * 下面的例子演示了如何使用write接口向串口设备中写入数据。
 */ 

/** @example serial_ctrl1.c
 * 下面的例子演示了如何使用 ioctl接口设置串口属性
 */ 

/** @example serial_ctrl2.c
 * 下面的例子演示了如何使用 ioctl接口设置串口属性
 */ 

/** @example serial_ctrl3.c
 * 下面的例子演示了如何使用 ioctl接口设置串口属性
 */ 

/** @example file_stat.c
 * 下面的例子演示了如何使用 stat接口获取文件属性
 */ 

/** @example file_ops.c
 * 下面的例子演示了如何使用IO接口操作文件
 */ 

/** @example file_lseek.c
 * 下面的例子演示了如何使用lseek接口操作文件
 */ 

/** @example dir_ops.c
 * 下面的例子演示了如何使用 IO接口操作目录
 */ 

/** @example file_prepare.c
 * 下面的例子演示了如何使用 IO接口挂载文件系统
 */ 


/* @} */

#ifdef __cplusplus
}
#endif 

#endif
