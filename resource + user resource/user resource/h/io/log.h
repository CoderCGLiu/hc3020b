/*! @file log.h
 *  @brief 日志模块头文件
 *
 * @version @reworks_version
 * 
 * @see 无
 */
/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks 提供的日志的外部接口
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2013-03-15，唐立三，建立 
 */
#ifndef _REWORKS_IOS_LOG_H_
#define _REWORKS_IOS_LOG_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
/**
 * @defgroup group_os_io_log 日志模块
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	
/*!
 * \def LOG_MAX_LEVEL
 * 
 * \brief 日志模块支持的最高日志级别
 */	
#define LOG_MAX_LEVEL		255

/*!
 * \fn void log_fd_set(int fd)
 * 
 * \brief 设置主要的记录日志的文件描述符
 *  
 * \param fd 待设置的文件描述符
 * 
 * \return 无
 * 
 */
void log_fd_set(int fd);

/*!
 * \fn int int log_fd_add(int fd)
 * 
 * \brief 添加记录日志的文件描述符
 *  
 * \param fd 待添加的文件描述符
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int log_fd_add(int fd);

/*!
 * \fn int log_fd_delete(int fd)
 * 
 * \brief 删除记录日志的文件描述符
 *  
 * \param fd 待删除的文件描述符
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int log_fd_delete(int fd);

/*!
 * \fn int log_level_set(int level)
 * 
 * \brief 设置日志的记录级别
 * 
 * 设置日志记录级别后，只有高于该级别的日志才会被记录。默认的日志记录级别为0，
 * 即所有日志均会被记录。
 *  
 * \param level 待设置日志记录的级别
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int log_level_set(int level);

/*!
 * \fn int log_msg(int level, char *fmt, ptrdiff_t arg1, ptrdiff_t arg2, ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6)
 * 
 * \brief 日志记录接口
 *  
 * \param level 写日志级别 
 * \param fmt 日志格式
 * \param arg1 日志格式对应的参数
 * \param arg2 日志格式对应的参数
 * \param arg3 日志格式对应的参数
 * \param arg4 日志格式对应的参数
 * \param arg5 日志格式对应的参数
 * \param arg6 日志格式对应的参数
 * 
 * \return 成功返回写入日志队列中的字符数
 * \return 失败返回-1
 * 
 */
int log_msg(int level, char *fmt, ptrdiff_t arg1, ptrdiff_t arg2, ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6);

/*!
 * \fn int _func_log_msg(char *fmt, ptrdiff_t arg1, ptrdiff_t arg2, ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6)
 * 
 * \brief 函数日志记录接口
 *  
 * \param fmt  函数日志格式
 * \param arg1 函数日志格式对应的参数
 * \param arg2 函数日志格式对应的参数
 * \param arg3 函数日志格式对应的参数
 * \param arg4 函数日志格式对应的参数
 * \param arg5 函数日志格式对应的参数
 * \param arg6 函数日志格式对应的参数
 * 
 * \return 成功返回写入函数日志队列中的字符数
 * \return 失败返回-1
 * 
 */
int _func_log_msg(char *fmt, ptrdiff_t arg1, ptrdiff_t arg2, ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6);

/*!
 * \fn int log_module_init(int msgs_max)
 * 
 * \brief 初始化日志模块
 * 
 * 		日志模块使用消息队列接收日志信息。如果日志记录频繁则需要将消息队列中支持的
 * 最大数量适当增加
 *  
 * \param msgs_max 日志模块使用的消息队列中最大支持的消息数
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int log_module_init(int msgs_max);

/* @} */

#ifdef __cplusplus
}
#endif
	
#endif
	
