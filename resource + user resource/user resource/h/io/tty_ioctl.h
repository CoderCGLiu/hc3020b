
 /*! @file tty_ioctl.h
 *  @brief ReWorks TTY模块控制命令头文件
 *
 * @version 4.7
 * 
 * @see 《ReWorks应用开发编程指南附录C》
 */
 
 /******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：TTY设备头文件
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 *       2013-07-02，姚秋果，添加TTY设备读写缓冲设置宏
 * 		 2012-05-10，唐立三，定义命令分组宏
 *  	 2012-03-20，唐立三，重设置TTY控制命令定义
 * 		 2011-08-24，唐立三，建立 
 */
 
#ifndef _REWORKS_IOS_TTY_IOCTL_H_
#define _REWORKS_IOS_TTY_IOCTL_H_

#ifdef __cplusplus
extern "C"
{
#endif	

#include <io_ctl.h>
#include <kbd_ioctl.h>
	
/**
 * @defgroup group_os_io_ttydriver TTY驱动模块
 * @ingroup group_os_io_cmd
 * 
 * TTY规则库和TTY设备相关的控制命令详细用法，可参见《ReWorks应用开发编程指南附录C》
 * @{
 * 
 */	
	
/*==============================================================================
 * 
 * 控制命令
 */
/** 
 * 串口驱动
 */
/*!
 * \def TTY_SET_BAUDRATE
 * \brief 设置串口设备波特率 
 */
#define TTY_SET_BAUDRATE			_IOW(TTY_GROUP, 1, int)

/*!
 * \def TTY_GET_BAUDRATE
 * \brief 获取串口设备波特率 
 */
#define TTY_GET_BAUDRATE			_IOR(TTY_GROUP, 2, int)
	
/*!
 * \def TTY_SERIAL_LCR
 * \brief 设置串口line control寄存器 
 */
#define TTY_SERIAL_LCR				_IOW(TTY_GROUP, 3, int)
	
/*!
 * \def TTY_SET_MODE
 * \brief 设置访问模式 （中断、轮询）
 */
#define TTY_SET_MODE				_IOW(TTY_GROUP, 4, int)
	
/*!
 * \def TTY_GET_MODE
 * \brief 获取当前模式（中断、轮询） 
 */	
#define TTY_GET_MODE				_IOR(TTY_GROUP, 5, int)	
	
/**
 * 终端
 */
/*!
 * \def VGA_SET_TERMFC
 * \brief 设置终端前景色
 */	
#define VGA_SET_TERMFC				_IOW(VGA_GROUP, 1, int)
	
/**
 * 规则库
 */
/*!
 * \def TTY_FLUSH_IBUF
 * \brief 清空输入缓冲区 
 */
#define TTY_FLUSH_IBUF				_IO(TTY_GROUP, 7)

/*!
 * \def TTY_FLUSH_OBUF
 * \brief 清空输出缓冲区 
 */
#define TTY_FLUSH_OBUF				_IO(TTY_GROUP, 8)	

/*!
 * \def TTY_FLUSH_BUF
 * \brief 清空输入/输出缓冲区  
 */
#define TTY_FLUSH_BUF				_IO(TTY_GROUP, 9)	

/*!
 * \def TTY_RD_TIMEOUT
 * \brief 设置读超时 
 */	
#define TTY_RD_TIMEOUT				_IOW(TTY_GROUP, 10, int)	

/*!
 * \def TTY_RD_COUNT
 * \brief 设置读取字符阈值 
 */
#define TTY_RD_COUNT				_IOW(TTY_GROUP, 11, int)	

/*!
 * \def TTY_SET_OPTIONS
 * \brief 设置控制选项 
 */
#define TTY_SET_OPTIONS				_IOW(TTY_GROUP, 12, u32)	

/*!
 * \def TTY_GET_OPTIONS
 * \brief 获取设置的选项 
 */
#define TTY_GET_OPTIONS				_IOR(TTY_GROUP, 13, u32)	

/*!
 * \def TTY_SET_LINEMODE
 * \brief 设置行模式 
 */
#define TTY_SET_LINEMODE			_IOW(TTY_GROUP, 14, u32)	

/*!
 * \def TTY_GET_LDISCNAME
 * \brief 获取当前规则库的设置 
 */
#define TTY_GET_LDISCNAME			_IOR(TTY_GROUP, 15, char)	

/*!
 * \def TTY_IS
 * \brief 是否为TTY 
 */
#define TTY_IS						_IO(TTY_GROUP, 16)
	
/*!
 * \def TTY_NONBLOCK
 * \brief 设置非阻塞型
 */
#define TTY_NONBLOCK				_IOW(TTY_GROUP, 17, u32)

/*!
 * \def TTY_BLOCK
 * \brief 设置阻塞型
 */
#define TTY_BLOCK					_IOW(TTY_GROUP, 18, u32)
	
/*!
 * \def TTY_SET_RESHELL
 * \brief 设置默认的shell重启命令
 */
#define TTY_SET_RESHELL				_IOW(TTY_GROUP, 19, u32)
	
/*!
 * \def TTY_CANCEL
 * \brief 取消当前的正在执行的读写操作
 */
#define TTY_CANCEL					_IO(TTY_GROUP, 20)
	
/*!
 * \def TTY_PROTOHOOK
 * \brief 设置接收中断钩子
 */
#define TTY_PROTOHOOK				_IOW(TTY_GROUP, 21, int)
	
/*!
 * \def TTY_PROTOARG
 * \brief 设置接收中断钩子参数
 */
#define TTY_PROTOARG				_IOW(TTY_GROUP, 22, int)
	
/*!
 * \def TTY_NREAD
 * \brief 检查TTY设备有多少读数据
 */
#define TTY_NREAD					FIONREAD
	
/*!
 * \def TTY_NWRITE	
 * \brief 检查TTY设备可写入多少数据
 */	
#define TTY_NWRITE					FIONWRITE
	
/*!
 * \def TTY_RBUFSET	
 * \brief 设置TTY设备读缓冲大小
 */
#define TTY_RBUFSET                 _IOW(TTY_GROUP, 23, int)
	
/*!
 * \def TTY_WBUFSET	
 * \brief 设置TTY设备写缓冲大小
 */
#define TTY_WBUFSET                 _IOW(TTY_GROUP, 24, int)
	

/*==============================================================================
 * 
 * 串口模式定义
 */		
/*!
 * \def INT_MODE
 * \brief 设置串口方式为中断模式 
 */
#define INT_MODE					0x1
	
/*!
 * \def POLL_MODE
 * \brief 设置串口访问方式为轮询方式 
 */
#define POLL_MODE					0x2
	
	
/*==============================================================================
 * 
 * TTY_SET_OPTIONS选项
 */	
/*!
 * \def TTY_OPT_RAW
 * \brief 使用原始模式，即取消所有选项
 */
#define TTY_OPT_RAW					0x0

/*!
 * \def TTY_IN_CR2NL
 * \brief 输入中的回车映射为新行 
 */
#define TTY_IN_CR2NL				0x1
	
/*!
 * \def TTY_IN_NL2CR
 * \brief 输入中的新行映射为回车 
 */
#define TTY_IN_NL2CR				0x2
	
/*!
 * \def TTY_IN_IGNCR
 * \brief 忽略输入中的回车 
 */	
#define TTY_IN_IGNCR				0x4
	
/*!
 * \def TTY_OUT_CR2NL
 * \brief 输出中的回车映射为新行 
 */		
#define TTY_OUT_CR2NL				0x8
	
/*!
 * \def TTY_OUT_NL2CR
 * \brief 输出中的新行映射为回车 
 */	
#define TTY_OUT_NL2CR				0x10
	
/*!
 * \def TTY_OUT_IGNCR
 * \brief 忽略输出中的回车 
 */	
#define TTY_OUT_IGNCR				0x20
	
/*!
 * \def TTY_IN_ECHO
 * \brief 回显 
 */	
#define TTY_IN_ECHO					0x40
	
/*!
 * \def TTY_IN_STRIP
 * \brief 去掉输入中第八位 
 */	
#define TTY_IN_STRIP				0x80
	
/*!
 * \def TTY_IN_UC2LC
 * \brief 输入中的大写转小写 
 */	
#define TTY_IN_UC2LC				0x100
	
/*!
 * \def TTY_IN_LC2UC
 * \brief 输入中的小写转大写 
 */	
#define TTY_IN_LC2UC				0x200
	
/*!
 * \def TTY_OUT_UC2LC
 * \brief 输出中的大写转小写 
 */	
#define TTY_OUT_UC2LC				0x400
	
/*!
 * \def TTY_OUT_LC2UC
 * \brief 输出中的小写转大写 
 */	
#define TTY_OUT_LC2UC				0x800
	
/*!
 * \def TTY_IXON
 * \brief 启动输出软流控 
 */	
#define TTY_IXON					0x1000
	
/*!
 * \def TTY_IXOFF
 * \brief 启动输入软流控 
 */	
#define TTY_IXOFF					0x2000
	
/*!
 * \def TTY_LINEMODE
 * \brief 启动行模式 
 */
#define TTY_LINEMODE				0x4000
	
/*!
 * \def TTY_ENABLE_SR
 * \brief 使能shell重启
 */
#define TTY_ENABLE_SR				0x8000
	 
/*==============================================================================
 * 
 * TTY_SERIAL_LCR选项
 */	
/*!
 * \def CS5
 * \brief 数据位为5位 
 */
#define CS5							0x5
	
/*!
 * \def CS6
 * \brief 数据位为6位 
 */
#define CS6							0x6
	
/*!
 * \def CS7
 * \brief 数据位为7位 
 */
#define CS7							0x7
	
/*!
 * \def CS8
 * \brief 数据位为8位 
 */
#define CS8							0x0
	
/*!
 * \def STOP1
 * \brief 停止位为1位 
 */
#define STOP1						0x00
	
/*!
 * \def STOP2
 * \brief 停止位为2位 
 */
#define STOP2						0x10
	
/*!
 * \def DENBPAR
 * \brief 无奇偶校验 
 */
#define DENBPAR						0x000
	
/*!
 * \def PARODD
 * \brief 奇校验 
 */
#define PARODD						0x100
	
/*!
 * \def PAREVEN
 * \brief 偶校验 
 */
#define PAREVEN						0x200	

/*!
 * \struct line_protocol
 * 
 * \brief 使用行协议的配置属性
 */
struct line_protocol
{
	char			backspace_char; 	//!< 回退字符
	char			delete_line_char;	//!< 删行字符
	char			end_line_char;		//!< 行结束字符
	unsigned char	max_line_chars;		//!< 行最大字符数
};

/** @example example_serial_set_ratebaud.c
* 下面的例子演示了如何使用ioctl()设置串口波特率.
*/

/** @example example_serial_get_ratebaud.c
* 下面的例子演示了如何使用ioctl()获取串口波特率.
*/

/** @example example_serial_set_mode.c
* 下面的例子演示了如何使用ioctl()设置串口工作模式.
*/

/** @example example_serial_serial_lcr.c
* 下面的例子演示了如何使用ioctl()配置串口LCR寄存器.
*/

/** @example example_serial_rd_count.c
* 下面的例子演示了如何使用ioctl()设置串口读取阈值
*/

/** @example example_serial_set_options.c
* 下面的例子演示了如何使用ioctl()设置串口处理选项.
*/

/* @} */

#ifdef __cplusplus
}
#endif

#endif
