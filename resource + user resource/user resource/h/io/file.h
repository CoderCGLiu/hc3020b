/*! @file file.h
 *  @brief 文件模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了根文件系统相关信息
 * 修改：
 * 		 2011-11-04，规范
 *       2013-09-04，姚秋果，与库头文件同步更新
 */
#ifndef _REWORKS_IOS_FILE_H_
#define _REWORKS_IOS_FILE_H_

#ifdef __cplusplus
extern "C"
{
#endif 
	
	
#include <reworks/types.h>
#include <reworks/list.h>
#include <sys/syslimits.h>
#include <stdarg.h>
#include <dirent.h>
#include <semaphore.h>
#include <mutex3.h>

/**
 * @defgroup group_os_io_file 文件接口
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	

/*!
 * \def FD_VALID
 * 
 * \brief 检查文件描述符是否有效
 */
#define FD_VALID(fd)	(((fd) >= 0) && ((fd) < OPEN_MAX))
	
/*!
 * \struct file
 * 
 * \brief 文件描述结构
 * 
 * struct file对应于FD（文件描述符），FD作为open的返回值，对用户提供
 * struct file作为FD的内部描述，用于实际的I/O操作。
 * 
 */
struct file
{
	struct list_head        f_list; 		//!< 链接 
	int 	                f_fd; 			//!< 文件描述符 
	int			            f_error; 		//!< 错误号 
	int		                f_count; 		//!< 引用计数 
	off_t                   f_size;			//!< 文件大小 
	off_t                   f_offset; 		//!< 读写位置 
	u32              		f_flags; 		//!< 打开方式 
	mode_t			       	f_mode;			//!< 模式、权限等 
	struct filesystem_operations *fs_ops;	//!< 文件系统操作 
	struct file_operations *f_ops; 			//!< 文件操作 
	void			        *private_data;	//!< 文件操作所需数据 
	struct xnode		    *xnode;			//!< 根文件系统 
	DEFINE_PREALLOC_PTHREAD_MUTEX(f_mutex);	//!< 互斥量
	pthread_t						f_open_tid;	//!< 打开该文件的线程id		/* 2013-1-18 added by yellowriver for monitor */
};


/* @} */

#ifdef __cplusplus
}
#endif 

#endif
