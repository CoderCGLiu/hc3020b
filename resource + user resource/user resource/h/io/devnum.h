/*! @file devnum.h
 *  @brief ReWorks中使用的主设备号定义
 *
 * @version 4.7
 * 
 * @see 无
 */
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks中使用的主设备号
 * 修改：
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 		 2012-04-26，根据部门规划，重定义设备号
 * 		 2011-11-04，规范
 * 
 */
#ifndef _REWORKS_IOS_DEVICE_MNUMBER_H_
#define _REWORKS_IOS_DEVICE_MNUMBER_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
/**
 * @defgroup group_os_io_devnum 主设备号
 * @ingroup group_os_io_drv
 * 
 * @{
 * 
 */		

/*!
 * \def MAJOR_MAX
 * 
 * \brief 主设备号最大值
 */
#define MAJOR_MAX						255
	
/*!
 * \def MINOR_MAX
 * 
 * \brief 次设备号最大值
 */
#define MINOR_MAX						255

/*!
 * \def MINORBITS
 * 
 * \brief 次设备号所占位数
 */
#define MINORBITS						8

/*!
 * \def MINORMASK
 * 
 * \brief 次设备号掩码
 */
#define MINORMASK						((1U << MINORBITS) - 1)

/*!
 * \def MAJOR
 * 
 * \brief 从设备号中获取主设备号
 */
#define MAJOR(dev)						((dev) >> MINORBITS)

/*!
 * \def MINOR
 * 
 * \brief 从设备号中获取次设备号
 */
#define MINOR(dev)						((dev) & MINORMASK)

/*!
 * \def HASHDEV
 * 
 * \brief 计算设备号散列
 */
#define HASHDEV(dev)					 (dev)

/*!
 * \def MKDEV
 * 
 * \brief 生成设备号
 */
#define MKDEV(major, minor)				(((major) << MINORBITS) | (minor))
	
	

/*******************************************************************************
 * 
 * 一般性、常用设备（0~19）
 */	
/*!
 * \def NODEV_MAJOR
 * 
 * \brief 保留
 */
#define NODEV_MAJOR						0
	
/*!
 * \def NULL_MAJOR
 * 
 * \brief NULL设备
 */
#define NULL_MAJOR						1
	
/*!
 * \def VXWORKS_MAJOR
 * 
 * \brief VxWorks兼容设备
 */
#define VXWORKS_MAJOR					2

/*!
 * \def TTY_MAJOR
 * 
 * \brief TTY设备（包括串口、终端、伪终端）
 */
#define TTY_MAJOR						3
	
/*!
 * \def MSE_MAJOR
 * 
 * \brief PS/2鼠标、导航球等鼠标类设备的主设备号
 */
#define MSE_MAJOR						4
	
/*!
 * \def KBD_MAJOR
 * 
 * \brief PS/2键盘
 */
#define KBD_MAJOR						5
	
	
/*******************************************************************************
 * 
 * 块设备、存储类（20~39）
 */	
/*!
 * \def RAMDISK_MAJOR
 * 
 * \brief 内存虚拟磁盘
 */
#define RAMDISK_MAJOR					20
	
/*!
 * \def ATA_IDE_MAJOR
 * 
 * \brief ATA/IDE硬盘、CF卡
 */
#define ATA_IDE_MAJOR					21
	
/*!
 * \def SATA_MAJOR
 * 
 * \brief SATA硬盘
 */
#define SATA_MAJOR						25

/*!
 * \def MTD_MAJOR
 * 
 * \brief MTD块设备（flash）
 */
#define MTD_MAJOR						30
	
/*!
 * \def SDCARD_MAJOR
 * 
 * \brief SD卡
 */
#define SDCARD_MAJOR					31
	
	
/*******************************************************************************
 * 
 * USB设备（40~59）
 */	
/*!
 * \def USB_MAJOR
 * 
 * \brief 一般性USB设备
 */
#define USB_MAJOR						40
	
/*!
 * \def UMASS_MAJOR
 * 
 * \brief USB盘
 */
#define UMASS_MAJOR						41
	
/*!
 * \def UHID_MAJOR
 * 
 * \brief 一般性USB人机交互设备
 */
#define UHID_MAJOR						42
	
/*!
 * \def UMSE_MAJOR
 * 
 * \brief USB鼠标
 */
#define UMSE_MAJOR						43
	
/*!
 * \def UKBD_MAJOR
 * 
 * \brief USB键盘
 */
#define UKBD_MAJOR						44
	
	
/*******************************************************************************
 * 
 * 声卡、显卡、触摸屏类设备（60~79）
 */		
/*!
 * \def FB_MAJOR
 * 
 * \brief 显卡
 */
#define FB_MAJOR						60
	
/*!
 * \def AUDIO_MAJOR
 * 
 * \brief 声卡
 */
#define AUDIO_MAJOR						61
	
/*!
 * \def TS_MAJOR
 * 
 * \brief 触摸屏
 */
#define TS_MAJOR						62
	
	
/*******************************************************************************
 * 
 * ReWorks预留（80~149）
 */	
	
	
/*******************************************************************************
 * 
 * 用户自定义（150~255）
 */		
	
/* @} */
	
#ifdef __cplusplus
}
#endif

#endif
