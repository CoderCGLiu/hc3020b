/*! @file driver.h
 *  @brief ReWorks 驱动开发所需的类型和接口定义
 *
 * @version @reworks_version
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks 驱动开发所需的类型和接口定义
 * 修改：
 * 		 2011-12-23，规范
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 
 */
#ifndef _REWORKS_IOS_DRIVER_H_
#define _REWORKS_IOS_DRIVER_H_

#ifdef __cplusplus
extern "C"
{
#endif 
	
#include <reworks/types.h>
#include <reworks/list.h>
#include <reworksio.h>
#include <poll.h>
#include <select.h>	
#include <devnum.h>
#include <select.h>
	
/**
 * @defgroup group_os_io_driver 驱动接口
 * @ingroup group_os_io_drv
 * 
 * @{
 * 
 */	
	
struct file_operations;		/* 前向声明 */

/*!
 * \fn int chrdev_minor_request(dev_t major, dev_t start, dev_t count, dev_t limit, dev_t *minor)
 * 
 * \brief 字符设备次设备号申请接口
 * 
 * \param major 主设备号
 * \param start 起始次设备号
 * \param count 申请数量
 * \param limit 结束次设备号
 * \param minor 申请成功的第一个次设备号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int chrdev_minor_request(dev_t major, dev_t start, dev_t count, dev_t limit, dev_t *minor);


/*!
 * \fn int blkdev_minor_request(dev_t major, dev_t start, dev_t count, dev_t limit, dev_t *minor)
 * 
 * \brief 块设备次设备号申请接口
 * 
 * \param major 主设备号
 * \param start 起始次设备号
 * \param count 申请数量
 * \param limit 结束次设备号
 * \param minor 申请成功的第一个次设备号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int blkdev_minor_request(dev_t major, dev_t start, dev_t count, dev_t limit, dev_t *minor);

/*!
 * \typedef file_open
 * 
 * \brief 字符设备或文件打开操作类型
 */
typedef void *(*file_open)(void *registered, const char *pathname, int flags, mode_t mode);

/*!
 * \typedef file_close
 * 
 * \brief 字符设备或文件关闭操作类型
 */
typedef int(*file_close)(void *opened);

/*!
 * \typedef file_read
 * 
 * \brief 字符设备或文件读操作类型
 */
typedef ssize_t(*file_read)(void *opened, void *buffer, size_t count);

/*!
 * \typedef file_write
 * 
 * \brief 字符设备或文件写操作类型
 */
typedef ssize_t(*file_write)(void *opened, const void *buffer, size_t count);

/*!
 * \typedef file_ioctl
 * 
 * \brief 字符设备或文件控制操作类型
 */
typedef int(*file_ioctl)(void *opened, int cmd, void *arg);

/*!
 * \typedef file_lseek
 * 
 * \brief 字符设备或文件修改文件偏移操作类型
 */
typedef int(*file_lseek)(void *opened, off_t length, int whence);

/*!
 * \typedef file_poll
 * 
 * \brief 字符设备或文件轮询操作类型
 */
typedef int(*file_poll)(void *opened, struct list_head *node);

/*!
 * \typedef file_init
 * 
 * \brief 字符设备或文件初始化操作类型
 */
typedef int(*file_init)();
		

/*!
 * \struct file_operations
 * 
 * \brief 字符设备及文件操作描述结构
 */
struct file_operations
{
	file_open 			open;			//!<打开
	file_close 			close;			//!<关闭 
	file_read 			read;			//!<读
	file_write 			write;			//!<写 
	file_ioctl 			ioctl;			//!<控制
	file_lseek			lseek;			//!<偏移
	file_poll 			poll;			//!<轮询 	
};


/*!
 * \fn int register_driver(dev_t major, struct file_operations *ops, dev_t *registered_major);
 * 
 * \brief 字符设备驱动注册接口
 * 
 * \param major 主设备号
 * \param ops 字符设备操作
 * \param registered_major 实际返回的主设备号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int register_driver(dev_t major, struct file_operations *ops, dev_t *registered_major);


/*!
 * \fn int unregister_driver(dev_t major)
 * 
 * \brief 字符设备驱动注销接口
 * 
 * \param major 要注销的驱动对应的主设备号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int unregister_driver(dev_t major);

/*!
 * \fn int register_device(const char *pathname, dev_t dev, void *registered)
 * 
 * \brief 设备注册接口
 * 
 * \param pathname 设备路径
 * \param dev 设备号
 * \param registered 注册信息，该信息在open接口中传入
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int register_device(const char *pathname, dev_t dev, void *registered);


/*!
 * \fn int unregister_device(const char *pathname)
 * 
 * \brief 设备注销接口
 * 
 * \param pathname 设备路径
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int unregister_device(const char *pathname);

/*!
 * \fn int register_chrdev(const char *pathname, dev_t dev, void *registered)
 * 
 * \brief 字符设备注册接口
 * 
 * \param pathname 设备路径
 * \param dev 设备号
 * \param registered 注册信息，该信息在open接口中传入
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int register_chrdev(const char *pathname, dev_t dev, void *registered);


/*!
 * \fn int unregister_chrdev(const char *pathname)
 * 
 * \brief 字符设备注销接口
 * 
 * \param pathname 设备路径
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int unregister_chrdev(const char *pathname);


/*!
 * \fn struct file_operations *file_operations_get(dev_t major)
 * 
 * \brief 获取字符设备驱动接口
 * 
 * \param major 主设备号
 * 
 * \return 成功返回字符设备驱动
 * \return 失败返回NULL
 * 
 */
struct file_operations *file_operations_get(dev_t major);


struct block_device;

/*!
 * \typedef blkdev_read
 * 
 * \brief 块设备读操作类型
 */
typedef int (* blkdev_read)(struct block_device *blkdev, 
								  int start, 
								  int blocks, 
								  char *buffer);

/*!
 * \typedef blkdev_write
 * 
 * \brief 块设备写操作类型
 */
typedef int (* blkdev_write)(struct block_device *blkdev, 
								   int start, 
								   int blocks, 
								   char *buffer);

/*!
 * \typedef blkdev_ioctl
 * 
 * \brief 块设备控制操作类型
 */
typedef int (* blkdev_ioctl)(struct block_device *blkdev, 
								   int cmd, 
								   unsigned long/*int*/ arg); /*ldf 20230424*/

/*!
 * \typedef blkdev_reset
 * 
 * \brief 块设备复位操作类型
 */
typedef int (* blkdev_reset)(struct block_device *blkdev);

/*!
 * \typedef blkdev_status
 * 
 * \brief 块设备获取状态操作类型
 */
typedef int (* blkdev_status)(struct block_device *blkdev);

/*!
 * \typedef blkdev_devshow
 * 
 * \brief 块设备设备显示操作类型
 */
typedef int (* blkdev_devshow)(struct block_device *blkdev);


/*!
 *\struct block_device
 *
 *\brief 块设备描述结构
 */
struct block_device
{
	blkdev_read 					bd_blkRd;		//!< 读块数据接口
	blkdev_write 					bd_blkWrt; 		//!< 写块数据接口
	blkdev_ioctl 					bd_ioctl; 		//!< 控制接口
	blkdev_reset 					bd_reset; 		//!< 复位接口
	blkdev_status 					bd_statusChk; 	//!< 状态检查接口
    blkdev_devshow 					bd_devShow;		//!< 设备信息显示接口
    int 							bd_removable; 	//!< 可移除标识
    block_t							bd_nBlocks; 	//!< 块数
    block_t							bd_nOffset;		//!< 偏移
    u32 							bd_bytesPerBlk; //!< 每块字节数
    u32 							bd_blksPerTrack;//!< 磁道块数
    u32 							bd_nHeads; 		//!< 磁头数
    int 							bd_retry; 		//!< 重试次数
    int 							bd_mode; 		//!< 读写模式
    int 							bd_readyChanged;//!< 准备标识
    int								bd_type;		//!< 类型
    int 							is_used;		//!< 是否使用
    u32                             xbd_dev;		//!< XBD设备ID
};


/*!
 * \fn int register_blkdev(const char *devname, dev_t major, struct block_device *blkdev)
 * 
 * \brief 块设备注册接口
 * 
 * \param devname 设备名称
 * \param major 主设备号
 * \param blkdev 块设备描述结构
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int register_blkdev(const char *devname, dev_t major, struct block_device *blkdev);

/*!
 * \fn int unregister_blkdev(const char *devname)
 * 
 * \brief 卸载块设备
 * 
 * \param devname 注册块设备时使用的名称
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int unregister_blkdev(const char *devname);


/*!
 * \fn struct block_device *blkdev_get(const char *devname)
 * 
 * \brief 获取块设备描述结构接口
 * 
 * \param devname 块设备名称
 * 
 * \return 成功返回块设备描述结构
 * \return 失败返回NULL
 * 
 */
struct block_device *blkdev_get(const char *devname);


/*!
 * \fn struct partition_info *blkdev_partition_get(const char *devname)
 * 
 * \brief 获取块设备上指定分区描述接口
 * 
 * \param devname 块设备分区名称
 * 
 * \return 成功返回块设备分区描述结构
 * \return 失败返回NULL
 * 
 */
struct partition_info *blkdev_partition_get(const char *devname);


/* @} */

#ifdef __cplusplus
}
#endif 

#endif
