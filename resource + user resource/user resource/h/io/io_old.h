/*! @file io_old.h
 *  @brief IO模块低版本兼容接口
 *
 * @version @reworks_version
 * 
 * @see 无
 */
 
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为兼容低版本ReWorks接口而定义
 * 修改：
 * 		 2012-08-08，唐立三，建立
 *       2013-09-04，姚秋果，与库头文件同步更新
 */
 
#ifndef _REWORKS_IOS_OLD_H_
#define _REWORKS_IOS_OLD_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <string.h>
#include <stdio.h>
#include <buf.h>

/**
 * @defgroup group_os_io_comp 兼容I/O接口
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	
/*!
 * \fn int cd(const char *pathname)
 * 
 * \brief 改变任务的工作目录
 * 
 * \param pathname 工作目录
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int cd(const char *pathname);

/*!
 * \fn int rm(const char *pathname)
 * 
 * \brief 删除指定的文件
 * 
 * \param pathname 文件路径
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int rm(const char *pathname);

/*!
 * \fn int cp(const char *source, const char *dest)
 * 
 * \brief 拷贝目录或文件
 * 
 * \param source 源文件路径
 * \param dest 目的文件路径
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int cp(const char *source, const char *dest);

/*!
 * \fn int mv(const char *src, const char *dest)
 * 
 * \brief 移动目录或文件
 * 
 * \param src 源文件路径
 * \param dest 目的文件路径
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int mv(const char *src, const char *dest);

/*!
 * \fn struct buffer_struct *buffer_create(int rbuf_size, int wbuf_size, int read_count)
 * 
 * \brief 创建缓冲接口（兼容）
 * 
 * \param rbuf_size 文件路径
 * \param wbuf_size 文件路径
 * \param read_count 文件路径
 * 
 * \return 成功返回缓冲结构体
 * \return 失败返回NULL
 */
struct buffer_struct *buffer_create(int rbuf_size, int wbuf_size, int read_count);	

/*!
 * \fn int buffer_delete(struct buffer_struct *buf)
 * 
 * \brief 删除缓冲接口（兼容）
 * 
 * \param buf 删除缓冲结构体指针
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int buffer_delete(struct buffer_struct *buf);

/* @} */

#ifdef __cplusplus
}
#endif 

#endif
