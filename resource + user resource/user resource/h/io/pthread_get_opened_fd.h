/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件为监测获取线程打开的文件描述符定义头文件
 * 
 */

#ifndef REWORKS_MONITOR_GET_OPENED_FD_H_INCLUDED__
#define REWORKS_MONITOR_GET_OPENED_FD_H_INCLUDED__
#ifdef __cplusplus
extern "C" 
{
#endif


/******************************************************************************
 * 
 * 获取线程打开的文件描述符
 * 
 * 	本程序用于获取指定线程打开的文件描述符；如果线程不存在打开的文件描述符或者错误，则返回-1
 * 	如果存在打开的文件描述符，则返回一个打开的文件描述符。
 * 
 * 输入：
 * 		tid：线程id
 * 		open_fd: 打开的fd数组首地址
 * 		size: 数组open_fd的大小
 * 输出：
 * 		存储了打开的文件描述符的数组
 * 返回：
 * 		成功返回线程打开文件描述符个数
 * 		（若大于数组open_fd的大小size，请输入该线程打开文件描述符个数大小的数组）；
 * 		不存在打开的文件描述符或失败返回0。
 */
int pthread_get_opened_fd(pthread_t tid, int *open_fd, size_t size);
#ifdef __cplusplus
}
#endif
#endif


