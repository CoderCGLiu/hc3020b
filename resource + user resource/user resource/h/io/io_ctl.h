/*! @file io_ctl.h
 *  @brief IO模块控制命令相关信息
 *
 * @version 4.7
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了IO模块控制命令相关信息
 * 修改：
 * 		 2011-11-04，规范
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 
 */
#ifndef _REWORKS_IOS_IO_CTL_H_
#define _REWORKS_IOS_IO_CTL_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#include <reworks/types.h>
#include <sys/syslimits.h>
#include <vx_ioctl.h>
#include <lx_ioctl.h>

/**
 * @defgroup group_os_io_ioctl IO控制命令
 * @ingroup group_os_io_cmd
 * 
 * @{
 * 
 */	


/*!
 * \def TTY_GROUP
 * 
 * \brief TTY命令标识
 */
#define TTY_GROUP				'T'

/*!
 * \def VGA_GROUP
 * 
 * \brief VGA命令分组
 */
#define VGA_GROUP				'V'

/*!
 * \def PS2_GROUP
 * 
 * \brief PS/2键盘/鼠标命令分组
 */
#define PS2_GROUP				'P' 
	
#include <net_ioctl.h>	
#include <kbd_ioctl.h>
#include <tty_ioctl.h>
	
/* @} */
	
#ifdef __cplusplus
}
#endif

#endif
