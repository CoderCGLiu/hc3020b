/*! @file net_ioctl.h
 *  @brief 网络协议栈ioctl命令头文件
 *
 * @version 4.7
 * 
 * @see 《ReWorks应用开发编程指南附录C》
 */
#ifndef _REWORKS_NET_IOCTL_H_
#define _REWORKS_NET_IOCTL_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <io_ctl.h>

/**
 * @defgroup group_os_io_netioctl 网络协议栈控制命令
 * @ingroup group_os_io_cmd
 * 网络协议栈或网卡驱动相关的控制命令详细用法，可参见《ReWorks应用开发编程指南附录C》
 * @{
 * 
 */	
	
/*
 *  ioctl值
 */
#define RTOS_IO_GET_ATTRIBUTES 		1   //!< 获取属性控制选项
#define RTOS_IO_SET_ATTRIBUTES 		2   //!< 设置属性控制选项
#define RTOS_IO_TCDRAIN       	 	3   //!< RTOS_IO_TCDRAIN 
#define RTOS_IO_RCVWAKEUP     		4   //!< 唤醒接受服务控制选项
#define RTOS_IO_SNDWAKEUP     	 	5   //!< 唤醒发送服务控制选项


#define FIOSETOWN       	 _IOW('f', 124, int)     //!< 设置所有者
#define FIOGETOWN      		 _IOR('f', 123, int)     //!< 获取所有者
#define SIOCSHIWAT	 			 _IOW('s',  0, int)		   //!< 设置高水印
#define SIOCGHIWAT	 			 _IOR('s',  1, int)		   //!< 获取高水印
#define SIOCSLOWAT	 			 _IOW('s',  2, int)		   //!< 设置低水印
#define SIOCGLOWAT	 			 _IOR('s',  3, int)		   //!< 获取低水印
#define SIOCATMARK	 			 _IOR('s',  7, int)		   //!< CAT掩码  
#define SIOCSPGRP	 			   _IOW('s',  8, int)		   //!< 设置进程组
#define SIOCGPGRP				   _IOR('s',  9, int)		   //!< 获取进程组
#ifdef USE_REWORKS_IPNET_69
//#include <netinet/in_var.h>
/* AF_INET ioctls: 注意：该处需与net库的ip/ipioctl.h一致*/
#define RX_IOC_VOID  0x20000000
#define RX_IOC_OUT   0x40000000
#define RX_IOC_IN    0x80000000
#define RX_IOC_INOUT (RX_IOC_IN | RX_IOC_OUT)

#define RX_IOCPARM_MASK    0x1fff      /* Parameter length 13 bits */
#define RX_IOCG_INET        5
#define RX_IOCG_NETIF       3
#define RX_IOCG_ARP         4
#define RX_IOCG_MCAST       10

/* IOCTL flags. */
#define RX_IOCF_R       0x00000000   /* read. */
#define RX_IOCF_W       0x00001000   /* write bit. */

#define RX_IOWR(g,i,f,t)  (RX_IOC_INOUT | /* in- and out-parameter */ \
        (((sizeof(t) & RX_IOCPARM_MASK) << 16) | (RX_IOCF_ ## f) | ((RX_IOCG_ ## g) << 8) | (i & 0xff)))
/* AF_INET ioctls: */
/* Get interface primary address. */
#define SIOCGIFADDR      RX_IOWR(INET,  1, R, struct ifreq)

/* Set interface primary address. */
#define SIOCSIFADDR      RX_IOWR(INET,  2, W, struct ifreq)

/* Get broadcast address. */
#define SIOCGIFBRDADDR   RX_IOWR(INET,  3, R, struct ifreq)

/* Set broadcast address. */
#define SIOCSIFBRDADDR   RX_IOWR(INET,  4, W, struct ifreq)

/* Get (sub)net address mask. */
#define SIOCGIFNETMASK   RX_IOWR(INET,  5, R, struct ifreq)

/* Set (sub)net address mask. */
#define SIOCSIFNETMASK   RX_IOWR(INET,  6, W, struct ifreq)

/* Get point-to-point IPv4 address. */
#define SIOCGIFDSTADDR   RX_IOWR(INET,  7, R, struct ifreq)

/* Set point-to-point IPv4 address. */
#define SIOCSIFDSTADDR   RX_IOWR(INET,  8, W, struct ifreq)

/* Add interface alias/address. */
#define SIOCAIFADDR      RX_IOWR(INET,  9, W, struct in_aliasreq)

/* Delete interface address. */
#define SIOCDIFADDR      RX_IOWR(INET, 10, W, struct ifreq)

/* Deprecated (BSD 4.3 compat): add route. */
#define SIOCADDRT        RX_IOWR(INET, 11, W, struct ortentry)

/* Deprecated (BSD 4.3 compat): remove route. */
#define SIOCDELRT        RX_IOWR(INET, 12, W, struct ortentry)

/* Proprietary (BSD 4.3 compat): get route. */
#define SIOCXGETRT       RX_IOWR(INET, 13, R, struct ortentry)

/* Proprietary AF_INET MIP ioctls */
/* Add a home address */
#define SIOCAHOMEADDR    RX_IOWR(INET,  14, W, struct in_aliasreq)

/* ARP ioctls: */
/* Create/modify ARP entry. */
#define SIOCSARP         RX_IOWR(ARP, 1, W, struct arpreq)

/* Get ARP entry. */
#define SIOCGARP         RX_IOWR(ARP, 2, R, struct arpreq)

/* Delete ARP entry */
#define SIOCDARP         RX_IOWR(ARP, 3, W, struct arpreq)

/* interface ioctls: */
/* Get interface flags. */
#define SIOCGIFFLAGS     RX_IOWR(NETIF,  1, R, struct ifreq)

/* Set interface flags. */
#define SIOCSIFFLAGS     RX_IOWR(NETIF,  2, W, struct ifreq)

/* Get interface MTU. */
#define SIOCGIFMTU       RX_IOWR(NETIF,  3, R, struct ifreq)

/* Set interface MTU. */
#define SIOCSIFMTU       RX_IOWR(NETIF,  4, W, struct ifreq)

/* Get link level address.  */
#define SIOCGIFLLADDR    RX_IOWR(NETIF, 13, R, struct ifreq)

/* Set link level address.  */
#define SIOCSIFLLADDR    RX_IOWR(NETIF, 14, W, struct ifreq)

/* Get interface list. */
#define SIOCGIFCONF      RX_IOWR(NETIF, 21, R, struct ifconf)

/* Get interface index */
#define SIOCGIFINDEX     RX_IOWR(NETIF, 22, R, struct ifreq)

/* Get interface capabilities */
#define SIOCGIFCAP       RX_IOWR(NETIF, 23, R, struct ifreq)

/* Multicast ioctls: */
/* get VIF statistics */
#define SIOCGETVIFCNT    RX_IOWR(MCAST,  0, R, struct sioc_vif_req)

/* get mcast route statistics */
#define SIOCGETSGCNT     RX_IOWR(MCAST,  1, R, struct sioc_sg_req)

/* interface ioctls: */
/* Get interface metric. */
#define SIOCGIFMETRIC    RX_IOWR(NETIF,  5, R, struct ifreq)

/* Set interface metric. */
#define SIOCSIFMETRIC    RX_IOWR(NETIF,  6, W, struct ifreq)


#else
#define SIOCADDRT	 			  _IOW('r', 10, struct ortentry)	   //!< 增加路由 
#define SIOCDELRT	 			  _IOW('r', 11, struct ortentry)	   //!< 删除路由 
#define SIOCGETVIFCNT			_IOWR('r', 15, struct sioc_vif_req)//!< 设置 vif pkt cnt 
#define SIOCGETSGCNT			_IOWR('r', 16, struct sioc_sg_req) //!< 获取s,g pkt cnt 
                            
#define SIOCSIFADDR	 			  _IOW('i', 12, struct ifreq)		    //!< 设置 ifnet地址 
#define OSIOCGIFADDR			  _IOWR('i', 13, struct ifreq)	    //!< 获取 ifnet 地址
#define SIOCGIFADDR				  _IOWR('i', 33, struct ifreq)	    //!< 获取 ifnet 地址
#define SIOCSIFDSTADDR	 		_IOW('i', 14, struct ifreq)		    //!< 设置 p-p 地址 
#define OSIOCGIFDSTADDR			_IOWR('i', 15, struct ifreq)	    //!< 获取 p-p 地址 
#define SIOCGIFDSTADDR			_IOWR('i', 34, struct ifreq)	    //!< 获取 p-p 地址 
#define SIOCSIFFLAGS	 		  _IOW('i', 16, struct ifreq)		    //!< 设置 ifnet 标志 
#define SIOCGIFFLAGS			  _IOWR('i', 17, struct ifreq)	    //!< 获取 ifnet 标志 
#define OSIOCGIFBRDADDR			_IOWR('i', 18, struct ifreq)	    //!< 获取广播地址 
#define SIOCGIFBRDADDR			_IOWR('i', 35, struct ifreq)	    //!< 获取广播地址 
#define SIOCSIFBRDADDR	 		_IOW('i', 19, struct ifreq)		    //!< 设置广播地址 
#define OSIOCGIFCONF			  _IOWR('i', 20, struct ifconf)	    //!< 获取 ifnet列表 
#define SIOCGIFCONF				  _IOWR('i', 36, struct ifconf)	    //!< 获取 ifnet列表 
#define OSIOCGIFNETMASK		  _IOWR('i', 21, struct ifreq)	    //!< 获取 net 地址掩码 
#define SIOCGIFNETMASK			_IOWR('i', 37, struct ifreq)	    //!< 获取 net 地址掩码 
#define SIOCSIFNETMASK	 	  _IOW('i', 22, struct ifreq)		    //!< 设置 net 地址掩码 
#define SIOCGIFMETRIC			  _IOWR('i', 23, struct ifreq)	    //!< 获取 IF metric 
#define SIOCSIFMETRIC	 		  _IOW('i', 24, struct ifreq)		    //!< 设置 IF metric 
#define SIOCDIFADDR	 			  _IOW('i', 25, struct ifreq)		    //!< 删除 IF 地址 
#define SIOCAIFADDR	 			  _IOW('i', 26, struct ifaliasreq)  //!< 增加/改变 IF别名 
#define SIOCADDMULTI	 		  _IOW('i', 49, struct ifreq)		    //!< 增加 多播地址
#define SIOCDELMULTI	 		  _IOW('i', 50, struct ifreq)		    //!< 删除多播地址 
#define SIOCGIFMTU				  _IOWR('i', 51, struct ifreq)	    //!< 获取 IF mtu 
#define SIOCSIFMTU	 			  _IOW('i', 52, struct ifreq)		    //!< 设置 IF mtu 
#define SIOCGIFPHYS				  _IOWR('i', 53, struct ifreq)	    //!< 获取 IF wire 
#define SIOCSIFPHYS	 			  _IOW('i', 54, struct ifreq)		    //!< 设置 IF wire 
#define SIOCSIFMEDIA			  _IOWR('i', 55, struct ifreq)	    //!< 设置 net media 
#define SIOCGIFMEDIA			  _IOWR('i', 56, struct ifmediareq) //!< 获取 net media 

#define SIOCSIFTAP				  _IOW('i', 80, struct ifreq)	      //!< 设置 tap函数 
#define SIOCGIFTAP				  _IOW('i', 81, struct ifreq)	      //!< 获取 tap函数 
#endif

#define TCGETS					0x5401	  //!<   TCGETS			  
#define TCSETS					0x5402	  //!<   TCSETS			  
#define TCSETSW					0x5403	  //!<   TCSETSW			
#define TCSETSF					0x5404	  //!<   TCSETSF			
#define TCGETA					0x5405	  //!<   TCGETA			  
#define TCSETA					0x5406	  //!<   TCSETA			  
#define TCSETAW					0x5407	  //!<   TCSETAW			
#define TCSETAF					0x5408	  //!<   TCSETAF			
#define TCSBRK					0x5409	  //!<   TCSBRK			  
#define TCXONC					0x540A	  //!<   TCXONC			  
#define TCFLSH					0x540B	  //!<   TCFLSH			  
#define TIOCEXCL				0x540C	  //!<   TIOCEXCL		  
#define TIOCNXCL				0x540D	  //!<   TIOCNXCL		  
#define TIOCSCTTY				0x540E	  //!<   TIOCSCTTY		
#define TIOCGPGRP				0x540F	  //!<   TIOCGPGRP		
#define TIOCSPGRP				0x5410	  //!<   TIOCSPGRP		
#define TIOCOUTQ				0x5411	  //!<   TIOCOUTQ		  
#define TIOCSTI					0x5412	  //!<   TIOCSTI			
#define TIOCGWINSZ				0x5413	//!<   TIOCGWINSZ	  
#define TIOCSWINSZ				0x5414	//!<   TIOCSWINSZ	  
#define TIOCMGET				0x5415	  //!<   TIOCMGET		  
#define TIOCMBIS				0x5416	  //!<   TIOCMBIS		  
#define TIOCMBIC				0x5417	  //!<   TIOCMBIC		  
#define TIOCMSET				0x5418	  //!<   TIOCMSET		  
#define TIOCGSOFTCAR			0x5419	//!<   TIOCGSOFTCAR 
#define TIOCSSOFTCAR			0x541A	//!<   TIOCSSOFTCAR 
#define FIONREAD				0x541B	  //!< 读取文件可读字节数(64位控制码)
#define TIOCINQ					FIONREAD	//!< 读取文件可读字节数(64位控制码)
#define TIOCLINUX				0x541C	  //!< TIOCLINUX		
#define TIOCCONS				0x541D	  //!< TIOCCONS		  
#define TIOCGSERIAL				0x541E	//!< TIOCGSERIAL	
#define TIOCSSERIAL				0x541F	//!< TIOCSSERIAL	
#define TIOCPKT					0x5420	  //!< TIOCPKT			
#define FIONBIO					0x5421	  //!< FIONBIO			
#define TIOCNOTTY				0x5422	  //!< TIOCNOTTY		
#define TIOCSETD				0x5423	  //!< TIOCSETD		  
#define TIOCGETD				0x5424	  //!< TIOCGETD		  
#define TCSBRKP					0x5425	  //!< POSIX tcsendbreak()所需 
#define TIOCTTYGSTRUCT	0x5426    //!< 仅用于调试 
#define TIOCSBRK				0x5427    //!< 兼容BSD 
#define TIOCCBRK				0x5428    //!< 兼容BSD 
#define TIOCGSID				0x5429    //!< 返回会话FD的ID 

#define FIONCLEX				0x5450	    //!< FIONCLEX 
#define FIOCLEX					0x5451	    //!< FIOCLEX	
#define FIOASYNC				0x5452	    //!< 异步I/O
#define TIOCSERCONFIG			0x5453	  //!< TIOCSERCONFIG	
#define TIOCSERGWILD			0x5454	  //!< TIOCSERGWILD	  
#define TIOCSERSWILD			0x5455	  //!< TIOCSERSWILD	  
#define TIOCGLCKTRMIOS			0x5456	//!< TIOCGLCKTRMIOS 
#define TIOCSLCKTRMIOS			0x5457	//!< TIOCSLCKTRMIOS 
#define TIOCSERGSTRUCT			0x5458  //!< 仅用于调试 
#define TIOCSERGETLSR   		0x5459  //!< 获取行状态寄存器 
#define TIOCSERGETMULTI 		0x545A  //!< 获取多端口配置 
#define TIOCSERSETMULTI 		0x545B  //!< 设置多端口配置 
                                       
#define TIOCMIWAIT				0x545C	  //!< TIOCMIWAIT		  
#define TIOCGICOUNT				0x545D	  //!< TIOCGICOUNT			
#define TIOCGHAYESESP  	 		0x545E 	//!< TIOCGHAYESESP  
#define TIOCSHAYESESP   		0x545F 	//!< TIOCSHAYESESP  

#define TIOCPKT_DATA		 	    0	    //!< TIOCPKT_DATA		 	  
#define TIOCPKT_FLUSHREAD	 	  1	    //!< TIOCPKT_FLUSHREAD	
#define TIOCPKT_FLUSHWRITE	 	2	    //!< TIOCPKT_FLUSHWRITE 
#define TIOCPKT_STOP		 	    4	    //!< TIOCPKT_STOP		 	  
#define TIOCPKT_START			    8	    //!< TIOCPKT_START			
#define TIOCPKT_NOSTOP			  16	  //!< TIOCPKT_NOSTOP		  
#define TIOCPKT_DOSTOP			  32	  //!< TIOCPKT_DOSTOP		  

#define TIOCSER_TEMT    		0x01	  //!< TIOCSER_TEMT	

/* @} */
	
#ifdef __cplusplus
}
#endif

#endif
