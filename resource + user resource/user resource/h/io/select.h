/*! @file select.h
 *  @brief ReWorks select模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为selelct功能的头文件
 * 修改：
 *      2013-09-04，姚秋果，与库头文件同步更新
 * 		2011-12-29，唐立三，整理
 * 
 */
 
#ifndef _REWORKS_IOS_SELECT_H
#define _REWORKS_IOS_SELECT_H

#ifdef __cplusplus
extern "C"
{
#endif 

#include <sys/time.h>
#include <reworks/types.h>
#include <reworks/list.h>
#include <pthread.h>

/**
 * @defgroup group_os_io_select select模块
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
	
/**	
 * 	@var SELECT_TYPE
 * 	@brief select类型
 */
typedef enum
{
    SELREAD,	//!< select读
    SELWRITE,	//!< select写
    SELEXCEPT
}SELECT_TYPE;


/*!
 * \struct poll_list_head
 * 
 * \brief 唤醒链表头
 */
struct poll_list_head
{
	struct list_head 		head; 		//!< 链接节点
	pthread_mutex_t			mutex;		//!< 互斥量
	int						selected;	//!< 标志位
};


/*!
 * 
 * \fn int poll_list_head_init(struct poll_list_head *head)
 * 
 * \brief 唤醒链表头初始化
 * 
 * \param head 唤醒链表头
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int poll_list_head_init(struct poll_list_head *head);


/*!
 * 
 * \fn int poll_list_head_destroy(struct poll_list_head *head)
 * 
 * \brief 销毁唤醒节点占用的资源，执行poll_list_head_init的逆操作
 * 
 * \param head 唤醒链表头
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int poll_list_head_destroy(struct poll_list_head *head);


/*!
 * 
 * \fn int poll_list_head_num(struct poll_list_head *head)
 * 
 * \brief 获取唤醒链表中唤醒节点的数量
 * 
 * \param head 唤醒链表头
 * 
 * \return 成功返回唤醒节点数量
 * \return 失败返回-1
 */
int poll_list_head_num(struct poll_list_head *head);


/*!
 * 
 * \fn int poll_list_node_add(struct poll_list_head *head, struct list_head *node)
 * 
 * \brief 将唤醒节点node加入唤醒链表head
 * 
 * \param head 唤醒链表头
 * \param node 唤醒节点
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int poll_list_node_add(struct poll_list_head *head, struct list_head *node);


/*!
 * 
 * \fn SELECT_TYPE select_wakeup_type(struct list_head *node)
 * 
 * \brief 获取select节点类型
 * 
 * \param node 由设备驱动维护的唤醒节点
 * 
 * \return SELECT_TYPE 读写类型 
 */
SELECT_TYPE select_wakeup_type(struct list_head *node);

/*!
 * 
 * \fn void select_terminate_all_task(struct poll_list_head *head)
 * 
 * \brief 终止所有select某个设备的线程
 * 
 * \param head 由设备驱动维护的唤醒链表头
 * 
 * \return 无 
 */
void select_terminate_all_task(struct poll_list_head *head);

/*!
 * 
 * \fn void select_wakeup_all_task(struct poll_list_head *node, register SELECT_TYPE type)
 * 
 * \brief 唤醒select某个设备的所有线程
 * 
 * \param node 由设备驱动维护的唤醒节点
 * \param type 唤醒类型
 * 
 * \return 无
 */
void select_wakeup_all_task(struct poll_list_head *node, register SELECT_TYPE type);

/*!
 * 
 * \fn void select_wakeup_task(struct list_head *node)
 * 
 * \brief 唤醒select某个设备的由node指定的线程
 * 
 * \param node 由设备驱动维护的唤醒节点
 * 
 * \return SELECT_TYPE 读写类型 
 */
void select_wakeup_task(struct list_head *node);


/*!
 * \fn int select(int width, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
 * 
 * \brief IO多路选择
 * 
 * \param width 最大文件描述符加1
 * \param readfds 读操作为文件描述符集
 * \param writefds 写操作文件描述符集
 * \param exceptfds 例外操作文件描述符集
 * \param timeout 超时时间
 * 
 * \return 文件描述符已改变（可读）的个数
 */
int select(int width, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

/* @} */

#ifdef __cplusplus
}
#endif 

#endif 
