/*! @file fs.h
 *  @brief 文件系统模块头文件
 *
 * @version 4.7
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了文件系统注册所需信息
 * 修改：
 * 		 2012-01-12，唐立三，建立
 *       2013-09-04，姚秋果，与库头文件同步更新
 * 
 */
#ifndef _REWORKS_IOS_FS_H_
#define _REWORKS_IOS_FS_H_


#ifdef __cplusplus
extern "C"
{
#endif
	
#include <pthread.h>	
#include <file.h>	
#include <driver.h>
	
/**
 * @defgroup group_os_io_fs 文件系统接口
 * @ingroup group_os_io
 * 
 * @{
 * 
 */	
/* 前向声明 */
struct xnode;	
	
/*!
 * \enum FS_TYPE
 * 
 * \brief 支持的文件系统类型
 */
typedef enum
{
	NOT_SUPPORTED,	//!< 不支持
	RFS,			//!< 根文件系统
	DOSFS,			//!< DOS文件系统
	HRFS			//!< 高可靠文件系统
}FS_TYPE;

/*!
 * \enum PARTITION_TYPE
 * 
 * \brief 分区类型
 */
typedef enum
{
	ENTIRE_BLKDEV = 1,	//!< 整个块设备
	GEN_PARTITION		//!< 块设备分区
}PARTITION_TYPE;

/*!
 * \def FS_MP_MAX
 * 
 * \brief 最大支持的挂载点数量
 */
#define FS_MP_MAX			16

/*!
 * \def FS_TYPE_MAX
 * 
 * \brief 最大支持的文件系统类型
 */
#define FS_TYPE_MAX			4

/*!
 * \struct partition_info
 *
 * \brief 定义了块设备的分区描述
 */
struct partition_info
{				
	int						minors;			//!< 次设备号 
	struct block_device		*blkdev;		//!< 块设备 
	int						type;			//!< 分区类型 
	block_t					offset;			//!< 分区偏移 
	block_t					blocks;			//!< 分区扇区数 
	int						format;			//!< 分区文件系统类型 
	FS_TYPE					fstype;			//!< 可挂载的文件系统类型 
	dev_t					devnum;			//!< 设备号 
	pthread_mutex_t			mutex;			//!<互斥量
	void					*data;			//!<私有数据
	void * 					xbdpartdev; 	//!<xbd分区设备
};
	
/*!
 * \typedef fs_open
 * 
 * \brief 文件系统打开操作类型
 */
typedef int (*fs_open)(struct file *filp, void *registered, const char *pathname, int flags, mode_t mode);

/*!
 * \typedef fs_ioctl
 * 
 * \brief 文件系统控制操作类型
 */
typedef int (*fs_ioctl)(struct file *filp, int command, void *arg);

/*!
 * \typedef fs_remove
 * 
 * \brief 文件系统移除文件或目录操作类型
 */
typedef int (*fs_remove)(void *registered, const char *pathname);

/*!
 * \typedef fs_mkdir
 * 
 * \brief 文件系统创建目录操作类型
 */
typedef int (*fs_mkdir)(void *registered, const char *pathname, mode_t mode);

/*!
 * \typedef fs_readdir
 * 
 * \brief 文件系统读目录操作类型
 */
typedef struct dirent *(*fs_readdir)(struct file *filp, DIR *dirp);

/*!
 * \struct filesystem_operations
 *
 * \brief 定义了文件系统操作结构
 */
struct filesystem_operations
{
	fs_open				open;		//!< 打开接口
	fs_ioctl			ioctl;		//!< 控制接口
	fs_remove			remove;		//!< 移除文件接口
	fs_mkdir			mkdir;		//!< 创建目录接口
	fs_readdir			readdir;	//!< 读取目录接口
};


/*!
 * \struct fs_mp
 *
 * \brief 定义了文件系统操作结构
 */
struct fs_mp
{
	struct list_head 				node;			//!< 链表 
	char 							*devname;		//!< 设备 
	int								fs_type;		//!< 文件系统类型
	struct filesystem_operations 	*fs_ops;		//!< 文件系统操作 
	char 							*mount_point;	//!< 挂载点 
	struct xnode					*xnode;			//!< xnode 
	void							*private_data;	//!< 私有数据
};

/*!
 * \fn struct fs_mp *fs_mp_get(const char *name, char **tailpath)
 * 
 * \brief 获取挂载点信息
 * 
 * \param name 文件路径
 * \param tailpath 除挂载点外的文件路径
 * 
 * \return 成功返回挂载点信息
 * \return 失败返回NULL
 */
struct fs_mp *fs_mp_get(const char *name, char **tailpath);

/*!
 * \fn struct fs_mp *fs_mp_get_by_devname(const char *devname)
 * 
 * \brief 以设备名获取挂载信息
 * 
 * \param devname 获取设备名称
 * 
 * \return 成功返回挂载点信息
 * \return 失败返回NULL
 */
struct fs_mp *fs_mp_get_by_devname(const char *devname);

/*!
 * \fn int umount_by_devname(const char *devname)
 * 
 * \brief 以设备名卸载文件系统
 * 
 * \param devname 获取设备名称
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int umount_by_devname(const char *devname);

/*!
 * \fn int fs_mp_add(const char *fs_name,
			   const char *devname,
			   char *mount_point, 
			   struct filesystem_operations *fs_ops,
			   void *registered)
 * 
 * \brief 添加挂载点
 * 
 * \param fs_name 文件系统名称
 * \param devname 设备名称
 * \param mount_point 挂载点
 * \param fs_ops 文件系统操作
 * \param registered 文件系统数据 
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int fs_mp_add(const char *fs_name,
			   const char *devname,
			   char *mount_point, 
			   struct filesystem_operations *fs_ops,
			   void *registered);

/*!
 * \typedef fs_mount
 * 
 * \brief 文件系统挂载操作类型
 */
typedef int (*fs_mount)(const char *devname, const char *mount_point);

/*!
 * \typedef fs_umount
 * 
 * \brief 文件系统卸载操作类型
 */
typedef int (*fs_umount)(const char *mount_point);

/*!
 * \typedef fs_format
 * 
 * \brief 文件系统格式化操作类型
 */
typedef int (*fs_format)(const char *devname);

/*!
 * \fn int fs_type_register(const char *fsname, 
 *					 fs_mount mount, 
 *					 fs_umount umount, 
 *					 fs_format format)
 * 
 * \brief 注册文件系统
 * 
 * \param fsname 文件系统名称
 * \param mount 挂载操作
 * \param umount 卸载操作
 * \param format 格式化操作
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int fs_type_register(const char *fsname, 
					 fs_mount mount, 
					 fs_umount umount, 
					 fs_format format);

/*!
 * \fn int fs_mode_set(const char *mount_point, 
 *                     int mode)
 * 
 * \brief 设置挂载点目录的读写模式
 * 
 * \param mount_point 挂载点路径
 * \param mode 读写模式：仅支持O_RDONLY、 O_WRONLY、O_RDWR。
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int fs_mode_set(const char *mount_point, int mode);

/*!
 * \fn int fs_mode_get(const char *mount_point);
 * 
 * \brief 获取挂载点目录的读写模式
 * 
 * \param mount_point 挂载点路径
 * 
 * \return 成功返回模式（O_RDONLY、 O_WRONLY、O_RDWR）
 * \return 失败返回-1
 */
int fs_mode_get(const char *mount_point);
/* @} */

#ifdef __cplusplus
}
#endif

#endif
