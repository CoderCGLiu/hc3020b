#ifndef __IO_FLOCK_H__
#define __IO_FLOCK_H__
#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
#include <mutex3.h>

/* LOCK_SH, 共享锁，暂不支持
* LOCK_EX, 独占锁
* LOCK_UN, 解锁 
* LOCK_NB, 非阻塞，与LOCK_EX操作一起使用 
*/
#define LOCK_SH	0x00000001
#define LOCK_EX	0x00000002
#define LOCK_NB	0x00000004
#define LOCK_UN	0x00000008

struct file_flock
{
	char *f_path;
	DEFINE_PREALLOC_PTHREAD_MUTEX(f_flock_mutex);	//!< 互斥量
};

extern FUNCPTR 			fd_set_flock_func_ptr;
extern FUNCPTR 			fd_clr_flock_func_ptr;
extern FUNCPTR 			fd_empty_flock_func_ptr ;
extern FUNCPTR 			flock_func_ptr;

extern int flock_module_init();
extern int flock(int fd, int operation);

#ifdef __cplusplus
}
#endif 
#endif
