#ifndef __LWIP_NET_IF_H__
#define __LWIP_NET_IF_H__

#include <reworks/types.h>
#include "lwip/sockets.h" 

#ifdef __cplusplus
 extern "C" {
#endif

struct	ifreq {
	char	ifr_name[16];		/* if name, e.g. "en0" */
	union {
		struct	sockaddr ifru_addr;
		struct	sockaddr ifru_dstaddr; 
		struct	sockaddr ifru_broadaddr; 
		u16	ifru_flags;
		int	ifru_metric;
		int	ifru_mtu;
		unsigned long  ifru_ifindex;
		int	ifru_phys;
		int	ifru_media;
		void *	ifru_data;   /* interface dependent data */
		u16 ifru_vr;     /* virtual router */
		int	ifru_opt;        /* option: enable=1, disable=0 */
		int	ifru_cap[2];
	} ifr_ifru;
#define	ifr_addr	ifr_ifru.ifru_addr	/* address */
#define	ifr_dstaddr	ifr_ifru.ifru_dstaddr	/* other end of p-to-p link */
#define	ifr_broadaddr	ifr_ifru.ifru_broadaddr	/* broadcast address */
#define	ifr_flags	ifr_ifru.ifru_flags	/* flags */
#define	ifr_metric	ifr_ifru.ifru_metric	/* metric */
#define ifr_ifindex     ifr_ifru.ifru_ifindex   /* Interface index */    
#define	ifr_mtu		ifr_ifru.ifru_mtu	/* mtu */
#define ifr_phys	ifr_ifru.ifru_phys	/* physical wire */
#define ifr_media	ifr_ifru.ifru_media	/* physical media */
#define	ifr_data	ifr_ifru.ifru_data	/* for use by interface */      
#define ifr_vr          ifr_ifru.ifru_vr /* ReWorks virtual router extension*/
#define ifr_opt         fr_ifru.ifru_opt   /* ReWorks option extension */
#define	ifr_reqcap	ifr_ifru.ifru_cap[0]	/* requested capabilities */
#define	ifr_curcap	ifr_ifru.ifru_cap[1]	/* current capabilities */
}; 


struct	ifconf {
	int	ifc_len;		/* size of associated buffer */
	union {
		char *	ifcu_buf;
		struct	ifreq *ifcu_req;
	} ifc_ifcu;
#define	ifc_buf	ifc_ifcu.ifcu_buf	/* buffer address */
#define	ifc_req	ifc_ifcu.ifcu_req	/* array of structures returned */
};


#define	IFF_UP		    0x1		/* interface link is up */
#define	IFF_BROADCAST	0x2		/* broadcast address valid */
#define	IFF_POINTOPOINT	0x10	/* interface is point-to-point link */
#define	IFF_RUNNING	    0x40	/* resources allocated */
#define	IFF_MULTICAST	0x8000	/* supports multicast */

 
#ifdef __cplusplus
}
#endif 

#endif
