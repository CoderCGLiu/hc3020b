#ifndef __INCLUDE_ARPA_FTP_H__
#define __INCLUDE_ARPA_FTP_H__

/*
 * 文件类型，FTP 可在数据连接上传输以下 3 种类型的文件
 */
#define	TYPE_A		1	/* ASCII 文件，这是传输文本文件的默认类型*/
#define	TYPE_E		2	/* EBCDIC 文件，作为双方使用 EBCDIC 编码情况下的文件类型*/
#define	TYPE_I		3	/* image 文件，这是传输二进制文件的默认类型 */
#define	TYPE_L		4	/* 本地文件类型，该类型在具有不同字节大小的主机间传输二进制文件 */


#endif
