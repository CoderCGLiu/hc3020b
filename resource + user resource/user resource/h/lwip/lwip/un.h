#ifndef LWIP_HDR_AF_UNIX_H
#define LWIP_HDR_AF_UNIX_H

#ifdef __cplusplus
extern "C" {
#endif
#define UNIX_PATH_MAX	108

struct sockaddr_un {
	sa_family_t sun_family;	/* AF_UNIX */
	char sun_path[UNIX_PATH_MAX];	/* pathname */
};


#ifdef __cplusplus
}
#endif
#endif /*end of LWIP_HDR_AF_UNIX_H*/
