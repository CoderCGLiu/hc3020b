
#ifndef LWIP_SYS_ARCH_H
#define LWIP_SYS_ARCH_H

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <errno.h>

typedef unsigned long sys_prot_t;
typedef unsigned long sys_sem_t;
typedef unsigned long sys_mbox_t;
typedef unsigned long sys_thread_t;
typedef unsigned long sys_mutex_t;


#ifdef __cplusplus
}
#endif

#endif /* LWIP_SYS_ARCH_H */
