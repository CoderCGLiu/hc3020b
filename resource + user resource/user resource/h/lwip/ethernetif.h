
#ifndef __INC_ETHERNETIF_H__
#define __INC_ETHERNETIF_H__


#include "lwip/opt.h"

#include "lwip/pbuf.h"
#include "lwip/netif.h"
#include "lwip/prot/ethernet.h"

typedef struct pbuf  pbuf_t;
typedef struct netif  netif_t;

typedef struct net_funcs
{
	void (* init)(struct netif* net_if);		//!< 网卡驱动初始化
	void (* send)(void *param, pbuf_t*);		//!< 网卡驱动发送数据
	pbuf_t *(* recv)(void *param);				//!< 网卡驱动接收数据
}NET_FUNCS;

    

/**
 * Helper struct to hold private data used to operate your ethernet interface.
 * Keeping the ethernet address of the MAC in this struct is not necessary
 * as it is already kept in the struct netif.
 * But this is only an example, anyway...
 */
struct ethernetif
{
	s8_t *rxbuf;
	u16_t rxlen;
	NET_FUNCS *eth_drvopts;
  /* Add whatever per-interface state that is needed here. */
};

typedef struct netif_table_entry
{
	int unit;                        
	netif_t* pNetIF;                 /* BSP private */
	struct ethernetif lwip_ethernetif;
} netif_table_entry_t;


int netif_device_register(int unit, struct netif *netif,
		NET_FUNCS *en_drvopts);


void netif_wakeup(struct netif *netif, char *rxbuf, u16_t len);

#endif
