#ifndef NET_FLUX_INFO_H_
#define NET_FLUX_INFO_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <reworks/types.h>

struct net_flux_info
{
	int if_count;//网卡数
	char if_name[16];//网卡名
	int if_baudrate;//传输速率，只有SLIP接口才设置baudrate
	long if_ibytes;//RX bytes
	long if_obytes;//TX bytes
};

int lwip_get_net_statistic(char * nic_name,u64 * rx_bytes,u64 * rx_packets,
		u64 * tx_bytes, u64 * tx_packets);

int lwip_get_net_speed(char * nic_name);

int nic_get_ifcount();

#ifdef __cplusplus
}
#endif

#endif
