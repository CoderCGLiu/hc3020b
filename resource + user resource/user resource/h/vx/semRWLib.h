#ifndef __SEMRWLIB_H__
#define __SEMRWLIB_H__
#ifdef __cplusplus
extern "C"
{
#endif 

#include <vxworks.h>
#include <reworks/types.h>
#include <object.h>
#include <wait_queue.h>

//#if defined( __loongson2__ )|| defined(__loongson2_64__)|| defined(__arm__)
#define VX_OBJ_ALIGN_SIZE 16
//#endif
typedef struct semrw_ctrl /* SEMAPHORE */
    {
	struct object_record 	obj         /* 0x00/0x00: object management */
	 __attribute__((aligned(VX_OBJ_ALIGN_SIZE))) ;
    UINT8	semType;	/* 0x44/0x80: semaphore type */
    UINT8	options;	/* 0x45/0x81: semaphore options */
    UINT16	recurse;	/* 0x46/0x82: semaphore recursive take count */
    BOOL    priInheritFlag; /* 0x48/0x84: TRUE if sem involved in */
				/*	      inheritance */
    WaitQ_Ctrl	qHead;		/* 0x4c/0x88: blocked task queue head */
    union
	{
	UINT		 count;	/* 0x5c/0xa8: current state count */
	void	 * owner;	/* 0x5c/0xa8: current state owner */
	} state;
//    EVENTS_RSRC	events;		/* 0x60/0xb0: VxWorks events */
                                /* 0x70/0xc0: sizeof SEMAPHORE */
    } SEMRW_CTRL;
    
typedef struct semRWListEntry    /* SEM_RW_LIST_ENTRY */
    {
    void		 * readerId;       /* 0x00/0x00: maximum concurrent readers */
    UINT         recurse;        /* 0x04/0x08: current reader count */
    BOOL	     priInheritFlag; /* 0x08/0x0c: sem involved in */
				     /*		   inheritance */
    } SEM_RW_LIST_ENTRY;

typedef struct semRWExtention   /* SEM_RW_EXT */
    {
    UINT                  maxReaders;     /* 0x00/0x00: maximum concurrent  */
					  /*		readers */
    UINT                  readCount;      /* 0x04/0x04: current reader count */
    WaitQ_Ctrl            readQHead;      /* 0x08/0x08: blocked task queue */
					  /*		head */
    } SEM_RW_EXT;

/*
 * High order bit in SEMAPHORE creation options byte, indicates a 'minimal'
 * implementation for reader/writer semaphores. Private and possibly
 * temporary.
 */
#define SEM_OPT_MINIMAL	 0x80

/*******************************************************************************
*
* SEM_IS_MINIMAL - check if a semaphore has the SEM_OPT_MINIMAL bit
*
* RETURNS - a nonzero value if the semphore is minimal; otherwise zero.
*
* int SEM_IS_MINIMAL (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEM_IS_MINIMAL(semId) ((semId)->options & SEM_OPT_MINIMAL)

/*******************************************************************************
*
* SEMRW_EXT_PTR_GET - return the read/write semaphore extension pointer
*
* RETURNS: A pointer to the read/write semaphore extension
*
* SEM_RW_EXT * SEMRW_EXT_PTR_GET (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_EXT_PTR_GET(semId)                                              \
	 (                                                                    \
	  (SEM_RW_EXT *) ((char *)semId + sizeof (SEMRW_CTRL))                 \
	 )

/*******************************************************************************
*
* SEMRW_READER_ENTRY_PTR - return a pointer to a read/write reader list entry
*
* RETURNS: The read/write semaphore reader list pointer
*
* UINT SEMRW_READER_LIST (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_READER_ENTRY_PTR(semId, index)                                  \
	 (                                                                    \
	  ((SEM_RW_LIST_ENTRY *)((char *) semId + sizeof (SEMRW_CTRL) +        \
				sizeof (SEM_RW_EXT) +                         \
				(index * sizeof (SEM_RW_LIST_ENTRY))))        \
	 )

/*******************************************************************************
*
* SEMRW_MAX_READERS - return the read/write semaphore maximum readers value
*
* RETURNS: The read/write semaphore maximum concurrent reader value
*
* UINT SEMRW_MAX_READERS (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_MAX_READERS(semId) (SEMRW_EXT_PTR_GET(semId)->maxReaders)

/*******************************************************************************
*
* SEMRW_READ_CNT - return the read/write semaphore read count
*
* RETURNS: The read/write semaphore read count
*
* UINT SEMRW_READ_CNT (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_READ_CNT(semId) (SEMRW_EXT_PTR_GET(semId)->readCount)

/*******************************************************************************
*
* SEMRW_RQHEAD - return the read/write semaphore read queue head
*
* RETURNS: The read/write semaphore read queue head
*
* Q_HEAD SEMRW_RQHEAD (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_RQHEAD(semId) (SEMRW_EXT_PTR_GET(semId)->readQHead)

/*******************************************************************************
*
* SEMRW_IS_FREE - check if a read/write semaphore is free
*
* RETURNS: TRUE if the semaphore is free, FALSE if not.
*
* BOOL SEMC_IS_FREE (SEM_ID semId)
*
* \NOMANUAL
*/

#define SEMRW_IS_FREE(semId)                                                  \
	 (                                                                    \
	 ((semId)->semOwner == NULL) && (SEMRW_READ_CNT(semId) == 0)          \
	 )

#ifdef __cplusplus
}
#endif 

#endif
