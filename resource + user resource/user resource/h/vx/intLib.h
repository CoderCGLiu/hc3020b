#ifndef __VXWORKS_INTLIB_H__
#define __VXWORKS_INTLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

	
#include "vxWorks.h"
#include "vwModNum.h"
#include <irq.h> /* isr_nest_level */

/* status codes */

#define S_intLib_NOT_ISR_CALLABLE		(M_intLib | 1)

#define INT_CONTEXT()	intContext()	/* same as intContext() in intLib.c */

#define INT_RESTRICT()							\
    (									\
    /* to avoid extra function call, use intContext() */		\
									\
    intContext() ? errno = S_intLib_NOT_ISR_CALLABLE, ERROR : OK	\
    )

BOOL intContext (void);

int intCount (void);
    
int  intConnect
    (
    VOIDFUNCPTR *vector,	/* interrupt vector to attach to     */
    VOIDFUNCPTR routine,	/* routine to be called              */
    _Vx_usr_arg_t parameter		/* parameter to be passed to routine */
    );

#ifdef __multi_core__
int intCpuLock();
void intCpuUnlock(int level);
#define intLock() intCpuLock()
#define intUnlock(level) intCpuUnlock(level)
#else
int intLock();
void intUnlock(int level);

#endif /* __multi_core__ */

#ifdef __cplusplus
}
#endif

#endif
