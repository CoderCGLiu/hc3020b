
/*
 * $Log: flbuffer.h,v $
 * Revision 1.1  2011/12/08 05:30:29  tls
 * ��2011-12-08��
 * ReWorks�ڲ����ò��԰�
 *
 * Revision 1.1  2011/12/08 02:08:17  tls
 * ��2011-12-08��
 * ReWorks�ڲ����ò��԰�
 *
 * Revision 1.1  2008/05/08 01:49:22  lijian
 * *** empty log message ***
 *
 * Revision 1.1  2008/05/07 10:45:25  lijian
 * *** empty log message ***
 *
   
      Rev 1.5   10 Sep 1997 16:15:52   danig
   Got rid of generic names
   
      Rev 1.4   28 Aug 1997 16:47:30   danig
   include flbase.h
   
      Rev 1.3   07 Jul 1997 15:23:28   amirban
   Ver 2.0
   
      Rev 1.2   18 Aug 1996 13:48:02   amirban
   Comments
   
      Rev 1.1   01 Jul 1996 15:41:58   amirban
   Doesn't define buffer
   
      Rev 1.0   20 Mar 1996 13:33:20   amirban
   Initial revision.
 */

/************************************************************************/
/*                                                                      */
/*		FAT-FTL Lite Software Development Kit			*/
/*		Copyright (C) M-Systems Ltd. 1995-1996			*/
/*									*/
/************************************************************************/

#ifndef FLBUFFER_H
#define FLBUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "flbase.h"

typedef struct {
  unsigned char data[SECTOR_SIZE];	/* sector buffer */
  SectorNo	sectorNo;		/* current sector in buffer */
  void		*owner;			/* owner of buffer */
  FLBoolean	dirty;			/* sector in buffer was changed */
  FLBoolean	checkPoint;		/* sector in buffer must be flushed */
} FLBuffer;

#ifdef __cplusplus
}
#endif

#endif
