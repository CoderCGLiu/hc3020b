/* sockLib.h -  UNIX BSD 4.3 compatible socket library header */

/* Copyright 1984 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
02m,24jun03,rae  add #include netVersion.h
02l,12oct01,rae  merge from truestack (update copyright)
02k,14nov00,rae  removed unused argument from sockLibAdd()
02j,09nov99,pul  integrating T2 cumulative patch
02i,17aug99,pai  changed send() prototype to use const on the second param
                 (SPR 21829)
02h,29apr99,pul  Upgraded NPT phase3 code to tor2.0.0
02g,02feb99,sgv  added bitflags to sockLibAdd().
02f,25jul95,dzb  added prototype for sockLibAdd().
02e,24jul95,dzb  changed sockInit() to sockLibInit().
02d,23jul93,jmm  changed #include "socket.h" to "sys/socket.h" (spr 2033)
02c,22sep92,rrr  added support for c++
02b,19aug92,smb  changed systime.h to sys/times.h
02a,04jul92,jcf  cleaned up.
01e,26may92,rrr  the tree shuffle
01d,16dec91,gae  added includes for ANSI.
01c,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed copyright notice
01b,23oct90,shl replaced all occurances of "int flag s" to "int s".
01a,05oct90,shl created.
*/

#ifndef __INCsockLibh
#define __INCsockLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "sys/socket.h"


#ifdef __cplusplus
}
#endif

#endif /* __INCsockLibh */
