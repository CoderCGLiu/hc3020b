/* arpLib.h - VxWorks ARP table manipulation header file */

/* Copyright 1990 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
02m,24jun03,rae  add #include netVersion.h
02l,13jan03,rae  Merged from velocecp branch
02k,09oct01,rae  merge from truestack
02j,07feb01,spm  added merge record for 30jan01 update from version 02i of
                 tor2_0_x branch (base version 02h) and fixed modification
                 history; replaced printed error message with errno value
02i,30jan01,ijm  merged ARP automatic linking fix (SPR #7576)
02h,22sep92,rrr  added support for c++
02g,07sep92,smb  added include netinet/in.h to remove ANSI warnings
02f,04jul92,jcf  cleaned up.
02e,11jun92,elh  changed parameter to arpCmd.
02d,26may92,rrr  the tree shuffle
02c,04apr92,elh  added arpFlush.
02b,06jan92,elh  passed through the ansification filter
		  -fixed #else and #endif
		  -changed copyright notice
02a,18nov91,elh  written.
*/

#ifndef __INCarpLibh
#define __INCarpLibh

#ifdef __cplusplus
extern "C" {
#endif

//#include "wrn/netVersion.h"
#include "types/vxTypesOld.h"
#include "vwModNum.h"
#include "netinet/in.h"
#include "net/if.h"
#include "net/if_dl.h"

/* permanent entry 
 * Create a permanent ARP entry which will not time out.
 * */
#define IP_ATF_PERM         (0x04)        //!< 永久ARP表项	

/* publish entry 
 * Publish this entry. The host will respond to ARP requests even if the
 * <pHost> parameter does not match a local IP address. This setting provides
 * a limited form of proxy ARP.
 * */
#define IP_ATF_PUBL         (0x08)        //!< 发布ARP表项	

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS 	arpLibInit (void);
extern STATUS 	arpAdd (char *host, char *eaddr, int flags);
extern STATUS 	arpDelete (char *host);
extern STATUS 	arpShow (void);
extern STATUS 	arpCmd (int cmd, struct in_addr * pIpAddr, u_char *pHwAddr,
			int *pFlags);
extern void 	arpFlush (void);

#else	/* __STDC__ */

extern STATUS 	arpLibInit ();
extern STATUS 	arpAdd ();
extern STATUS 	arpDelete ();
extern STATUS 	arpCmd ();
extern void 	arpFlush ();

#endif	/* __STDC__ */

/* error values */

#define S_arpLib_INVALID_ARGUMENT		(M_arpLib | 1)
#define S_arpLib_INVALID_HOST 			(M_arpLib | 2)
#define S_arpLib_INVALID_ENET_ADDRESS 	(M_arpLib | 3)
#define S_arpLib_INVALID_FLAG			(M_arpLib | 4)


/* added by zly */
#ifndef USE_REWORKS_IPNET_69
#define	SIOCSARP	_IOW('i', 30, struct arpreq)	/* set arp entry */
#define	SIOCDARP	_IOW('i', 32, struct arpreq)	/* delete arp entry */
#endif
#define	ATF_PROXY	0x10	/* proxy server entry (wildcard h/w address) */
#define ATF_INCOMPLETE	0x20	/* Complete a host entry */

struct llinfo_arp {
	struct	llinfo_arp *la_next;
	struct	llinfo_arp *la_prev;
	struct	rtentry *la_rt;
	struct	mbuf *la_hold;		/* last packet until resolved/timeout */
	long	la_asked;		/* last time we QUERIED for this addr */
#define la_timer la_rt->rt_rmx.rmx_expire /* deletion time in seconds */
};

extern u_char 	etherbroadcastaddr[6];

#if 0
/*
 * Structure of a Link-Level sockaddr:
 */
struct sockaddr_dl {
	u_char	sdl_len;	/* Total length of sockaddr */
	u_char	sdl_family;	/* AF_DLI */
	u_short	sdl_index;	/* if != 0, system given index for interface */
	u_char	sdl_type;	/* interface type */
	u_char	sdl_nlen;	/* interface name length, no trailing 0 reqd. */
	u_char	sdl_alen;	/* link level address length */
	u_char	sdl_slen;	/* link layer selector length */
	char	sdl_data[12];	/* minimum work area, can be larger;
				   contains both if name and ll address */
};
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCarpLibh */
