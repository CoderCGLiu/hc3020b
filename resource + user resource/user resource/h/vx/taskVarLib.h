#ifndef __multi_core__
#ifndef _taskVarLib_h_
#define _taskVarLib_h_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <pthread.h>
	
typedef pthread_var_t TASK_VAR;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS taskVarInit(void);

/** @defgroup group_vx_taskVar    VxWorks任务变量
* @ingroup group_os_vxworks  
VxWorks提供一个称为“任务变量”的功能，它允许4字节的变量被添加至任务的上下文，并且在每次任务切换发生的时候，
变量的值将被切换出或切换到调用任务。典型的，多个任务声明相同的变量（4字节的存储单元）作为任务变量，
并且将此存储单元作为它们自己的私有变量。例如，此功能可以用于同一个例程需要作为多个同时运行的任务而必须被多次创建的时候。
    taskVarAdd() 和 taskVarDelete() 接口用于添加或删除一个任务变量。 taskVarGet() 和 taskVarSet() 接口用于获取或设置一个任务变量的值。
*
* 头文件：
* #include<taskVarLib.h> 
*  @{ */

/**
* @brief 添加任务变量
*
*  该接口用于添加一个指定的变量pVar (4-byte memory location)至一个指定的任务上下文。在调用该接口后，变量将为任务所私有。
*任可以访问并修改该变量，但是修改值不会为其它任务所看到，而其它任务对该任务变量的修改也对此任务没有影响。
*在每次调用任务发生任务切换时，这种情况的实现，在每次系统切换至调用任务或调用任务被切换至其它任务时，通过保存和恢复变量的初时值来完成。<br/> 
*  当一个例程被作为几个独立的任务运用时，可以使用该功能。尽管每个任务都由自己的栈，并因此有自己的栈变量，
*它们仍将共享相同的静态和全局变量。要使得一个变量为非共享，例程可以调用taskVarAdd( )来为每个任务制作一个该变量的单独拷贝，但是仍在相同的物理地址。 <br/>
*  注意，任务变量会增加拥有它们的任务的任务切换时间。因此，要限制一个任务所拥有的任务变量数。
*一个有效的使用任务变量的方法是 ，一个任务使用单个的任务变量，该任务变量是一个指针，指向一个包含所有任务私有数据的动态分配的数据结构。 
*
*
* @param	tid 	任务ID
* @param	pVar 	指向要转换的任务变量的指针
* 
* @return	OK		函数执行成功
* @return 	ERROR	用于任务变量描述符的内存不足
*
* @exception 无
* 
* @see taskVarDelete, taskVarGet, taskVarSet 
*/
extern STATUS taskVarAdd(TASK_ID tid, int *pVar);

/**
* @brief 删除任务变量
*
* 该接口从指定的任务上下文删除一个指定的任务变量pVar。被删除变量的私有值丢失。
*
* @param	tid 	任务ID
* @param	pVar 	指向要被删除的任务变量的指针
* 
* @return	OK		函数执行成功
* @return 	ERROR	指定任务的任务变量不存在
*
* @exception 无
* 
* @see taskVarAdd, taskVarGet, taskVarSet 
*/
extern STATUS taskVarDelete(TASK_ID tid, int *pVar);

/**
* @brief 设置任务变量
*
* 该接口设置指定任务的任务变量私有值。指定任务通常不是调用任务，因为调用任务可以通过直接修改变量来设置它的私有值。该接口的提供首先用于调试目的。
*
*
* @param	tid		任务ID
* @param	pVar	指向要被设置的任务变量的指针
* @param	value	任务变量的新值
* 
* @return	OK		函数执行成功
* @return 	ERROR	任务没找到或者任务没有该任务变量
*
* @exception 无
* 
* @see taskVarAdd, taskVarDelete, taskVarGet 
*/
extern STATUS taskVarSet(TASK_ID tid, int *pVar, int value);

/**
* @brief 获取任务变量
*
* 该接口返回指定任务的任务变量私有值。指定任务通常不是调用任务，因为调用任务可以直接访问变量来得到它的私有值。该接口的提供首先用于调试目的。
*
* @param	tid 	任务ID
* @param 	pVar 	指向任务变量的指针
* 
* @return	任务变量的私有值
* @return 	ERROR	任务没找到或者任务没有该任务变量
*
* @exception 无
* 
* @see taskVarAdd, taskVarDelete, taskVarSet 
*/
extern int taskVarGet(TASK_ID tid, int *pVar);

/**
* @brief 获取任务变量列表
*
* 该接口为调用任务提供一个包含指定任务所有的任务变量的列表。未被排序的任务变量数组被复制到varList。
*
* @param	tid 	任务ID
* @param	varList 存放任务变量地址的数组
* @param	maxVars varList可容纳的最大任务变量数
* 
* @return	列表中的任务变量数
*
* @exception 无
*/
extern int taskVarInfo(TASK_ID tid, TASK_VAR varList[], int maxVars);
/** @example example_taskVarAdd.c
 * 下面的例子演示了如何使用 taskVarAdd().
 */

/*  @} */
	
#else /* __STDC__ */

extern STATUS taskVarAdd();
extern STATUS taskVarDelete();
extern STATUS task_var_module_init();
extern STATUS taskVarSet();
extern int taskVarGet();
extern int taskVarInfo();

#endif /* __STDC__ */

#ifdef __cplusplus
}

#endif 

#endif
#endif /* __multi_core__ */
