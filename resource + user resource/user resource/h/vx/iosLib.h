#ifndef iosLib_h
#define iosLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include "ioLib.h"
#include "vwModNum.h"
#include "dllLib.h"
	
typedef struct 		/* DEV_HDR - device header for all device structures */
    {
    DL_NODE	node;		/* device linked list node */
    short	drvNum;		/* driver number for this device */
    char *	name;		/* device name */
    } DEV_HDR;

/* function declarations */


#if defined(__STDC__) || defined(__cplusplus)

//extern STATUS 	iosInit (int max_drivers, int max_files, char *nullDevName);
//extern void 	iosShowInit (void);
extern DEV_HDR *iosDevFind (char *name, char ** pNameTail);
//extern DEV_HDR *iosFdDevFind (int fd);
//extern DEV_HDR *iosNextDevGet (DEV_HDR *pDev);
//extern STATUS 	iosClose (int fd);
extern STATUS 	iosDevAdd (DEV_HDR *pDevHdr, char *name, int drvnum);
extern STATUS 	iosDrvRemove (int drvnum, BOOL forceClose);
//extern int 	iosCreate (DEV_HDR *pDevHdr, char *fileName, int mode);
//extern int 	iosDelete (DEV_HDR *pDevHdr, char *fileName);
extern int 	iosDrvInstall (FUNCPTR pCreate, FUNCPTR pDelete, FUNCPTR pOpen,
			       FUNCPTR pClose, FUNCPTR pRead, FUNCPTR pWrite,
			       FUNCPTR pIoctl);
//extern int 	iosFdNew (DEV_HDR *pDevHdr, char *name, int value);
//extern int 	iosFdValue (int fd);
//extern int 	iosIoctl (int fd, int function, int arg);
//extern int 	iosOpen (DEV_HDR *pDevHdr, char *fileName, int flags, int mode);
//extern int 	iosRead (int fd, char *buffer, int maxbytes);
//extern int 	iosWrite (int fd, char *buffer, int nbytes);
extern void 	iosDevDelete (DEV_HDR *pDevHdr);
extern void 	iosDevShow (void);
//extern void 	iosDrvShow (void);
//extern void 	iosFdFree (int fd);
//extern STATUS 	iosFdSet (int fd, DEV_HDR *pDevHdr, char *name, int value);
//extern void 	iosFdShow (void);

#else	/* __STDC__ */

//extern STATUS 	iosInit ();
//extern void 	iosShowInit ();
extern DEV_HDR *iosDevFind ();
//extern DEV_HDR *iosFdDevFind ();
//extern DEV_HDR *iosNextDevGet ();
//extern STATUS 	iosClose ();
extern STATUS 	iosDevAdd ();
extern STATUS 	iosDrvRemove ();
//extern int 	iosCreate ();
//extern int 	iosDelete ();
extern int 	iosDrvInstall ();
//extern int 	iosFdNew ();
//extern int 	iosFdValue ();
//extern int 	iosIoctl ();
//extern int 	iosOpen ();
//extern int 	iosRead ();
//extern int 	iosWrite ();
extern void 	iosDevDelete ();
extern void 	iosDevShow ();
//extern void 	iosDrvShow ();
//extern void 	iosFdFree ();
//extern STATUS 	iosFdSet ();
//extern void 	iosFdShow ();

#endif	/* __STDC__ */
#ifdef __cplusplus
}
#endif
#endif
