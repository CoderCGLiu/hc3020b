/* archSparc.h - SPARC specific header */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
01i,16dec96,yp   va_start now passes LASTARG to builtin_next_arg
01h,22sep92,rrr  added support for c++
01g,21sep92,rrr  fixed stdargs for gcc2.x
01f,27jul92,rrr  cleanup of stdargs macros
01e,26jul92,rrr  fixed _ARCH_va_arg macro
01d,25jul92,smb  moved sparc stdarg defines from stdarg.h
01c,07jul92,jwt  added _ARCH_MULTIPLE_CACHELIB; changed archSPARC to archSparc.
01b,26may92,rrr  the tree shuffle
01a,22apr92,yao  written.
*/

#ifndef __INCarchSparch
#define __INCarchSparch

#ifdef __cplusplus
extern "C" {
#endif

#define	_ALLOC_ALIGN_SIZE	8		/* 8-byte alignment */
#define _STACK_ALIGN_SIZE	8		/* 8-byte alignment */

#define	_ARCH_MULTIPLE_CACHELIB	TRUE		/* multiple cache libraries */

#define _DYNAMIC_BUS_SIZING	FALSE		/* no dynamic bus sizing */

/* defines for stdarg.h */
/* Amount of space required in an argument list for an arg of type TYPE.
   TYPE may alternatively be an expression whose type is used.  */

#define __va_rounded_size(TYPE)  \
  (((sizeof (TYPE) + sizeof (int) - 1) / sizeof (int)) * sizeof (int))

#ifndef __sparc__
#define _ARCH_va_start(AP, LASTARG)                                     \
 (AP = ((char *) &(LASTARG) + __va_rounded_size (LASTARG)))
#else
#define _ARCH_va_start(AP, LASTARG)                                     \
  (__builtin_saveregs (), AP = ((char *) __builtin_next_arg (LASTARG)))
#endif

#define _ARCH_va_end(AP)

/* Avoid errors if compiling GCC v2 with GCC v1.  */
#if __GNUC__ == 1
#define __extension__
#endif

/* RECORD_TYPE args passed using the C calling convention are
   passed by invisible reference.  ??? RECORD_TYPE args passed
   in the stack are made to be word-aligned; for an aggregate that is
   not word-aligned, we advance the pointer to the first non-reg slot.  */
#define _ARCH_va_arg(pvar,TYPE)					\
__extension__							\
({ TYPE __va_temp;						\
   ((__builtin_classify_type (__va_temp) >= 12)			\
    ? ((pvar) += __va_rounded_size (TYPE *),			\
       **(TYPE **) ((pvar) - __va_rounded_size (TYPE *)))	\
    : __va_rounded_size (TYPE) == 8				\
    ? ({ union {TYPE d; int i[2];} u;				\
	 u.i[0] = ((int *) (pvar))[0];				\
	 u.i[1] = ((int *) (pvar))[1];				\
	 (pvar) += 8;						\
	 u.d; })						\
    : ((pvar) += __va_rounded_size (TYPE),			\
       *((TYPE *) ((pvar) - __va_rounded_size (TYPE)))));})

#ifdef __cplusplus
}
#endif

#endif /* __INCarchSparch */
