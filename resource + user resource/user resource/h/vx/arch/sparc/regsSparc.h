/* regsSparc.h - SPARC Version of Register Include File */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
02j,30dec97,dbt  added common names for registers
02i,13aug93,jwt  changed TBB_REG_SET to WIND_TCB_REGS; copyright 1993.
02h,22sep92,rrr  added support for c++
02g,29jul92,jwt  changed TCB_REG_SET from 0x110 to 0x130 to correspond to
                 taskLih.h change 03f - adding TCB entries before REG_SET.
02f,10jul92,jwt  moved register set padding from REG_SET to WIND_TCB, etc;
                 added coprocessor ancillary state registers set pointers.
02e,08jul92,jwt  changed regsSPARC.h to regsSparc.h.
02d,30jun92,yao  added PC_OFFSET.
02c,26may92,rrr  the tree shuffle
02b,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed ASMLANGUAGE to _ASMLANGUAGE
		  -changed copyright notice
02a,16nov90,jwt  written - based on SPARC 4.0.2 "wind.h".
*/

#ifndef __INCregsSparch
#define __INCregsSparch

#ifdef __cplusplus
extern "C" {
#endif

#define DOUBLE_WORD_ALIGN       0x7     /* 8-byte Alignment Mask */
#define WIM_WINDOW(n)           (0x01 << (n)) /* WIM Window Mask */

#ifndef _ASMLANGUAGE

typedef struct			/* Coprocessor Context */
    {
    UINT csr;			/* Coprocessor Status Register */
    UINT *pCpQ;			/* Queue */
    UINT cpd[32];		/* Data Registers */
    } CP_CONTEXT;

typedef struct			/* SPARC Ancillary State Register Set */
    {
    UINT  asr0;			/* ASR0 undefined */
    UINT  asrRes[15];		/* Reserved ASRs */
    UINT  asrDep[16];		/* Implementation Dependent ASRs */
    } REG_ASR;



/* some common names for registers */
#undef  SP
#undef  spReg
#undef  FP
#undef  fpReg

#define SP(pRegs)       (pRegs->out[6]) /* Stack Pointer */
#define spReg           out[6]
#define FP(pRegs)       (pRegs->in[6])  /* Frame Pointer */
#define fpReg           in[6]
#define reg_pc		pc
#define reg_npc		npc
#define reg_sp		out[6]
#define reg_fp		in[6]

#endif	/* _ASMLANGUAGE */

#define REG_SET_PC      0x00            /* Program Counter */
#define REG_SET_NPC     0x04            /* Next Program Counter */
#define REG_SET_PSR     0x08            /* Processor Status Register */
#define REG_SET_WIM     0x0C            /* Window Invalid Mask */
#define REG_SET_TBR     0x10            /* Trap Base Register */
#define REG_SET_Y       0x14            /* Y Register */
#define REG_SET_pCP     0x18            /* Coprocessor Pointer */
#define REG_SET_pASR    0x1C            /* Pointer to ASRs (%asr1 - %asr31) */

#define REG_SET_GLOBALS 0x20            /* Global Registers (%R0  - %R7)  */
#define REG_SET_OUTS    0x40            /* Out Registers    (%R8  - %R15) */
#define REG_SET_LOCALS  0x60            /* Local Registers  (%R16 - %R23) */
#define REG_SET_INS     0x80            /* In Registers     (%R24 - %R31) */

#define REG_SET_GLOBAL(n)   (REG_SET_GLOBALS + (4 * (n)))
#define REG_SET_OUT(n)      (REG_SET_OUTS + (4 * (n)))
#define REG_SET_LOCAL(n)    (REG_SET_LOCALS + (4 * (n)))
#define REG_SET_IN(n)       (REG_SET_INS + (4 * (n)))

#define pREG_COPROCESSOR(n)	(4 * (n))
#define pREG_ASR(n)      	(4 * (n))

#define PC_OFFSET	REG_SET_PC	/* referenced by pc() in usrLib */

/* TCB Register Set */

#define TCB_PC          (WIND_TCB_REGS + REG_SET_PC)   /* Program Counter */
#define TCB_NPC         (WIND_TCB_REGS + REG_SET_NPC)  /* Next Program Counter */
#define TCB_PSR         (WIND_TCB_REGS + REG_SET_PSR)  /* Status Register */
#define TCB_WIM         (WIND_TCB_REGS + REG_SET_WIM)  /* Window Invalid Mask */
#define TCB_TBR         (WIND_TCB_REGS + REG_SET_TBR)  /* Trap Base Register */
#define TCB_Y		(WIND_TCB_REGS + REG_SET_Y)    /* Y Register */
#define TCB_pCP         (WIND_TCB_REGS + REG_SET_pCP)  /* Coprocessor Set */
#define TCB_pASR        (WIND_TCB_REGS + REG_SET_pASR) /* ASR Registers */

#define TCB_GLOBALS     (WIND_TCB_REGS + REG_SET_GLOBALS)
#define TCB_OUTS        (WIND_TCB_REGS + REG_SET_OUTS)
#define TCB_LOCALS      (WIND_TCB_REGS + REG_SET_LOCALS)
#define TCB_INS         (WIND_TCB_REGS + REG_SET_INS)

#define TCB_GLOBAL(n)   (TCB_GLOBALS + (4 * (n))) /* Global Registers */
#define TCB_OUT(n)      (TCB_OUTS + (4 * (n)))    /* Out Registers    */
#define TCB_LOCAL(n)    (TCB_LOCALS + (4 * (n)))  /* Local Registers  */
#define TCB_IN(n)       (TCB_INS + (4 * (n)))     /* In Registers     */

#ifdef __cplusplus
}
#endif

#endif /* __INCregsSparch */
