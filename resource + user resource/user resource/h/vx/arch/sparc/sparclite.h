/* sparclite.h - SPARClite MB8693X header */

/* Copyright 1984-1996 Wind River Systems, Inc. */

/*
modification history
--------------------
01q,17nov95,sub  added 936 & 933h support
01p,02feb94,vin  changed macros ADDRESS_TAG_MASK,CACHE_TAG_ADDR_MASK
		 to accomodate mb86932 (8K Icache).
01o,19jul93,jwt  made CACHE_[LINE,ALIGN]_SIZE macros for '932 (8K Icache).
01n,25jan92,jwt  cacheMb930LockAuto() takes and returns void; copyright 1993.
01m,03dec92,jwt  fixed assignment of CACHE_LINE_SIZE to _STACK_ALIGN_SIZE.
01l,29sep92,jwt  merged cacheXLibInit(), cacheXReset(), and cacheXModeSet().
01k,22sep92,rrr  added support for c++
01j,05sep92,jwt  added function prototype for cacheMb930LockAuto().
01i,27aug92,jwt  added function prototype for cacheMb930TextUpdate().
01h,31jul92,jwt  changed cacheMb930Malloc() to cacheMb930DmaMalloc().
01g,08jul92,jwt  added cache library function prototypes.
01f,16jun92,jwt  config/mb86930/mb86930.h to h/arch/sparc/sparclite.h for 5.1.
01e,26may92,rrr  the tree shuffle
01d,26apr92,jwt  passed through the ansification filter
		  -changed copyright notice
01c,25mar92,jwt  renamed sparclite.h to mb86930.h;
                 enhanced interrupt and cache support; cleanup.
01b,25jan92,ccc  added include files.
01a,17jan92,jwt  created cache support definitions for MB86930 SPARClite.
*/

#ifndef __INCsparcliteh
#define __INCsparcliteh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "mb86940.h"
#include "iv.h"
#include "cacheLib.h"

/* SPARClite ASI (Alternate Space Index) Assignments */

#define	SPARCLITE_CONTROL_ASI		0x1	/* Control Registers */
#define	SPARCLITE_ICACHE_LOCK_ASI	0x2	/* Instruction Cache Lock */
#define	SPARCLITE_DCACHE_LOCK_ASI	0x3	/* Data Cache Lock */
#define	SPARCLITE_ADDRESS_CS0_ASI	0x9	/* Supervisor/Instruction */
#define	SPARCLITE_ICACHE_TAG_ASI	0xC	/* Instruction Cache Tags */
#define	SPARCLITE_ICACHE_DATA_ASI	0xD	/* Instruction Cache Data */
#define	SPARCLITE_DCACHE_TAG_ASI	0xE	/* Data Cache Tags */
#define	SPARCLITE_DCACHE_DATA_ASI	0xF	/* Data Cache Data */

/* SPARClite Control Register Addresses */

#define	CACHE_BIU_ADDR		0x00000000	/* Cache / Bus I/F Unit */
#define LOCK_CONTROL_ADDR	0x00000004	/* Cache Auto Lock Control */
#define	LOCK_CONTROL_SAVE_ADDR	0x00000008	/* Lock Control Save */
#define	CACHE_STATUS_ADDR	0x0000000C	/* Cache Status Register */
#define	LOCK_RESTORE_ADDR	0x00000010	/* Cache Restore Lock */
#define	SYSTEM_SUPPORT_ADDR	0x00000080	/* System Support Control */
#define	SAME_PAGE_ADDR		0x00000120	/* Same Page Masks */
#define	ADDRESS_CS0_ADDR	0xFFFFC000	/* Address Range CS0 */
#define	ADDRESS_CS1_ADDR	0x00000124	/* Address Range CS1 */
#define	ADDRESS_CS2_ADDR	0x00000128	/* Address Range CS2 */
#define	ADDRESS_CS3_ADDR	0x0000012C	/* Address Range CS3 */
#define	ADDRESS_CS4_ADDR	0x00000130	/* Address Range CS4 */
#define	ADDRESS_CS5_ADDR	0x00000134	/* Address Range CS5 */
#define	ADDRESS_MASK_CS0_ADDR	0x00000140	/* Address Mask for CS0 */
#define	ADDRESS_MASK_CS1_ADDR	0x00000144	/* Address Mask for CS1 */
#define	ADDRESS_MASK_CS2_ADDR	0x00000148	/* Address Mask for CS2 */
#define	ADDRESS_MASK_CS3_ADDR	0x0000014C	/* Address Mask for CS3 */
#define	ADDRESS_MASK_CS4_ADDR	0x00000150	/* Address Mask for CS4 */
#define	ADDRESS_MASK_CS5_ADDR	0x00000154	/* Address Mask for CS5 */
#define	WAIT_STATE_CS1_CS0_ADDR	0x00000160	/* Wait State Counts 1/0 */
#define	WAIT_STATE_CS3_CS2_ADDR	0x00000164	/* Wait State Counts 3/2 */
#define	WAIT_STATE_CS5_CS4_ADDR	0x00000168	/* Wait State Counts 5/4 */
#define	TIMER_ADDR		0x00000174	/* Timer Value */
#define	TIMER_PRELOAD_ADDR	0x00000178	/* Timer Pre-Load Value */

#define	CACHE_TAG_BANK1_ADDR	0x00000000	/* Cache Tag Bank 1 */
#define	CACHE_TAG_BANK2_ADDR	0x80000000	/* Cache Tag Bank 2 */

#define CACHE_BANK1_INVALIDATE_ADDR 0x00001000  /* Cache Invalidate - mb936 */
#define CACHE_BANK2_INVALIDATE_ADDR 0x80001000  /* Cache Invalidate - mb936 */

/* Cache / Bus Interface */

#define	WRITE_BUFFER_ENABLE	0x00000020	/* Write Buffer Enable */
#define	PREFETCH_BUFFER_ENABLE	0x00000010	/* Instruction Prefetch */
#define	DATA_CACHE_LOCK		0x00000008	/* Data Cache Lock */
#define	DATA_CACHE_ENABLE	0x00000004	/* Data Cache Enable */
#define	INSTR_CACHE_LOCK	0x00000002	/* Instruction Cache Lock */
#define	INSTR_CACHE_ENABLE	0x00000001	/* Instruction Cache Enable */

#define VALID_CLEAR             0x00000002      /* Clear Valid bits - mb936 */
#define LOCK_LRU_CLEAR          0x00000001      /* Clear lock/lru bits - mb936*/

/* Lock Controls */

#define	DCACHE_AUTOLOCK_ENTRY	0x00000002	/* Data Cache Lock Entry */
#define	ICACHE_AUTOLOCK_ENTRY	0x00000001	/* Instr Cache Lock Entry */
#define	CACHE_LOCK_RESTORE	0x00000001	/* Restore Cache Lock */

/* System Support Controls */

#define	SAME_PAGE_ENABLE	0x00000020	/* Same Page Enable */
#define	CHIP_SELECT_ENABLE	0x00000010	/* Chip Select Enable */
#define	WAIT_STATE_PROGRAM	0x00000008	/* Wait State Programmable */
#define	TIMER_ENABLE		0x00000004	/* Timer On/Off */
#define	SPARCLITE_ASI_MASK	0x7F800000	/* ASI Masks */
#define	SPARCLITE_ADDRESS_MASK	0x007FFFFE	/* Address Masks */

#define ADDR_MASK_ALL_ASI       0x00            /* Compare all ASI bits */
#define ADDR_MASK_16M_RANGE     0x00FFFFFF      /* Compare 16M Range */

/* Wait State Specifiers */

#define	WAIT_CSODD_COUNT2_FIELD	0xF8000000	/* CS[1|3|5] Count 2 Field */
#define	WAIT_CSODD_COUNT1_FIELD	0x07C00000	/* CS[1|3|5] Count 1 Field */
#define	WAIT_STATE_ENABLE	0x00200000	/* Wait State Enable */
#define	WAIT_STATE_SINGLE_CYCLE	0x00100000	/* Wait State Single Cycle */
#define	WAIT_STATE_OVERRIDE	0x00080000	/* Wait State Override */
#define	WAIT_CSEVN_COUNT2_FIELD	0x0007C000	/* CS[0|2|4] Count 2 Field */
#define	WAIT_CSEVN_COUNT1_FIELD	0x00003E00	/* CS[0|2|4] Count 1 Field */

/* Timer Support */

#define	TIMER_VALUE_FIELD	0x0000FFFF	/* Timer and Pre-Load Values */

/* Cache Tag Enables */

#define	TAG_LOCK_ENABLE		0x00000001	/* I/D Tag Lock Bit */
#define	SUB_BLOCK_VALID_FIELD	0x000003C0	/* Sub Block Valid */
#define	SUPERVISOR_TAG		0x00000020	/* Supervisor/User Tag */
#define	LRU_TAG_BIT		0x00000002	/* Least Recently Used */
#define	TAG_ENTRY_LOCK		0x00000001	/* Tag Entry Lock */

#define	SPARCLITE_LED_ADDR	0x02000000	/* Byte address for LED = +3 */
#define IO_ADRS_NICE            0x20000000      /* I/O addr of NICE regs */

#define	SPARC_TIMER_SETUP	TIMER_CONT_INTERNAL	| \
				TIMER_CONT_OUT_RESET	| \
				TIMER_CONT_MODE0	| \
				TIMER_CONT_IN_LOW	| \
				TIMER_CONT_COUNT_EN

#ifndef	_ASMLANGUAGE
				/* 932  I Cache 8K, D Cache 2K, 
				 * 933H I Cache 1K, D Cache 0K,
				 * 936  I Cache 4K, D Cache 2K,
				 * 930  I Cache 2K, D Cache 2K,
				 * 931  I Cache 2K, D Cache 2K.
				 */

#define	CACHE_SIZE  (  (sparcCpuId == 86932) ? \
			  ((cache  == INSTRUCTION_CACHE)?8192:2048): \
			  (  (sparcCpuId == 86933) ? \
			         ((cache == INSTRUCTION_CACHE)?1024:0): \
			         (  (sparcCpuId == 86936) ? \
     		                      ((cache == INSTRUCTION_CACHE)?4096:2048):\
			                2048 \
				  ) \
			   ) \
		     )

#define	CACHE_LINE_SIZE	 (  (sparcCpuId == 86932) ? \
			       ((cache  == INSTRUCTION_CACHE)?32:16): \
			       ( (sparcCpuId == 86936) ? \
     		                    ((cache == INSTRUCTION_CACHE)?32:16):\
			                16 \
			       ) \
			 )


#undef	_CACHE_ALIGN_SIZE

#define	_CACHE_ALIGN_SIZE (  (sparcCpuId == 86932) ? \
			       ((cache  == INSTRUCTION_CACHE)?32:16): \
			       ( (sparcCpuId == 86936) ? \
     		                    ((cache == INSTRUCTION_CACHE)?32:16):\
			                16 \
			       ) \
			  )

#define	ADDRESS_TAG_MASK  (  (sparcCpuId == 86932) ? \
			      ((cache  == INSTRUCTION_CACHE) ? 0xFFFFF000 : 0xFFFFFC00): \
		              (  (sparcCpuId == 86936) ? \
     		                 ((cache == INSTRUCTION_CACHE)?0xFFFFF800:0xFFFFFC00):\
			         0xFFFFFC00 ) \
			   )


#define	CACHE_TAG_ADDR_MASK  	(  (sparcCpuId == 86932) ? \
	     ((cache  == INSTRUCTION_CACHE)?0x00000FE0:0x000003F0): \
             (  (sparcCpuId == 86933) ? \
		   ((cache == INSTRUCTION_CACHE)?0x000001F0:0): \
	           (  (sparcCpuId == 86936) ? \
     		         ((cache == INSTRUCTION_CACHE)?0x000007E0:0x000003F0):\
		         0x000003F0 ) \
             ) \
	 )


#define	CACHE_LINES  (  (sparcCpuId == 86932) ? \
			  ((cache  == INSTRUCTION_CACHE)?256:128): \
			  (  (sparcCpuId == 86933) ? \
			         ((cache == INSTRUCTION_CACHE)?64:0): \
			         (  (sparcCpuId == 86936) ? \
     		                      ((cache == INSTRUCTION_CACHE)?128:128):\
			                128  ) \
			   ) \
		     )


IMPORT  STATUS  cacheMb930LibInit (CACHE_MODE data, CACHE_MODE inst);
IMPORT  STATUS  cacheMb930Enable (CACHE_TYPE cache);
IMPORT  STATUS  cacheMb930Disable (CACHE_TYPE cache);
IMPORT	void	cacheMb930LockAuto (void);
IMPORT  STATUS  cacheMb930Lock (CACHE_TYPE cache,
				void * address, size_t bytes);
IMPORT  STATUS  cacheMb930Unlock (CACHE_TYPE cache,
				  void * address, size_t bytes);
IMPORT  STATUS  cacheMb930Invalidate (CACHE_TYPE cache,
                                     void * address, size_t bytes);
IMPORT  STATUS  cacheMb930TextUpdate (void * address, size_t bytes);
IMPORT  void *  cacheMb930DmaMalloc (size_t bytes);
IMPORT  STATUS  cacheMb930DmaFree (void * pBuf);

IMPORT  STATUS  cacheMb930ClearLine (CACHE_TYPE cache, void * address);
IMPORT	void	cacheMb930TagShow (CACHE_TYPE cache);

IMPORT	UINT	mb930CacheGet (void * address);
IMPORT	void	mb930CacheSet (void * address, UINT regValue);
IMPORT	UINT	mb930ITagGet (void * address);
IMPORT	void	mb930ITagSet (void * address, UINT iTag);
IMPORT	void	mb930ITagLock (void * address, UINT lock);
IMPORT	UINT	mb930DTagGet (void * address);
IMPORT	void	mb930DTagSet (void * address, UINT dTag);
IMPORT	void	mb930DTagLock (void * address, UINT lock);
IMPORT	void	mb930LockAuto (void);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsparcliteh */
