/* ivSPARC.h - interrupt vectors for SPARC CPUs */

/* Copyright 1984-1995 Wind River Systems, Inc. */
/*
modification history
--------------------
02g,01may95,vin  corrected TT_DATA_ERROR.
02f,27feb95,vin  ss10 integration, changed copyright.
02e,22sep92,rrr  added support for c++
02d,26may92,rrr  the tree shuffle
02c,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed copyright notice
02b,21jan91,jwt  moved exception vector defines from configAll.h.
02a,17nov90,jwt  written - based on 4.0.2 "ivSparc.h"
*/

#ifndef __INCivSparch
#define __INCivSparch

#ifdef __cplusplus
extern "C" {
#endif


#define VECTOR_TABLE_ENTRIES    256
#define BYTES_PER_VECTOR        16
#define VECTOR_TABLE_SIZE       (VECTOR_TABLE_ENTRIES * BYTES_PER_VECTOR)

/* interrupt vectors */
#define INT_VEC_CONFIG_TBL	63	/* usrCftbl ptr */

#define TRAP_KERNEL		0	/* trap 0 - kernel trap */
#define TRAP_VXWORKS		1	/* trap 1 - vxWorks trap */
#define TRAP_0_DIVIDE		2	/* trap 2 - SPARC zero divide */
#define TRAP_DEBUG		3	/* trap 3 - breakpoint trap */
#define TRAP_DISABLE		4	/* Trap 4 - Disable Exceptions */

/* definitions of TBR fields */
#define	TBR_BASE_ADDRESS	0xfffff000
#define	TBR_TRAP_TYPE		0x00000ff0
#define	TBR_UNUSED		0x0000000f
#define	TBR_TRAP_TYPE_SHIFT	4

/* macros to convert interrupt vectors <-> interrupt numbers */
#define IVEC_TO_INUM(intVec)	((int) (intVec) >> 4)
#define INUM_TO_IVEC(intNum)	((FUNCPTR *) ((intNum) << 4))
#define TT_USER(trapNum) (128 + trapNum)
#define TRAPNUM_TO_IVEC(trapNum) INUM_TO_IVEC (TT_USER (trapNum))

/* trap type definitions */
#define TT_RESET		0	/* priority  1 */
#define TT_INSTR_ACCESS_EXCPTN	1	/* priority  2 */
#define TT_ILLEGAL_INSTR	2	/* priority  3 */
#define TT_PRIVILEGED_INSTR	3	/* priority  4 */
#define TT_FP_DISABLED		4	/* priority  5 */
#define TT_WINDOW_OVERFLOW	5	/* priority  6 */
#define TT_WINDOW_UNDERFLOW	6	/* priority  7 */
#define TT_MEM_ADDR_NOT_ALIGNED	7	/* priority  8 */
#define TT_FP_EXCPTN		8	/* priority  9 */
#define TT_DATA_ACCESS_EXCPTN	9	/* priority 10 */
#define TT_TAG_OVERFLOW		10	/* priority 11 */

/* 11 - 16	Unassigned, Reserved */
/* 17 - 31	Autovectors: (aka "interrupt_level_xx" in the documentation) */
#define TT_AUTOVEC_0		16
#define TT_AUTOVEC_1		17	/* priority 27 */
#define TT_AUTOVEC_2		18	/* priority 26 */
#define TT_AUTOVEC_3		19	/* priority 25 */
#define TT_AUTOVEC_4		20	/* priority 24 */
#define TT_AUTOVEC_5		21	/* priority 23 */
#define TT_AUTOVEC_6		22	/* priority 22 */
#define TT_AUTOVEC_7		23	/* priority 21 */
#define TT_AUTOVEC_8		24	/* priority 20 */
#define TT_AUTOVEC_9		25	/* priority 19 */
#define TT_AUTOVEC_10		26	/* priority 18 */
#define TT_AUTOVEC_11		27	/* priority 17 */
#define TT_AUTOVEC_12		28	/* priority 16 */
#define TT_AUTOVEC_13		29	/* priority 15 */
#define TT_AUTOVEC_14		30	/* priority 14 */
#define TT_AUTOVEC_15		31	/* priority 13 */

#define	TT_REG_ACCESS_ERROR	32	/* Not on SuperSPARC */	
#define TT_INSTR_ACCESS_ERROR	33	/* Not on SuperSPARC */

/* 34 - 35	Unassigned, Reserved */
#define TT_CP_DISABLED		36	/* priority  5 */
#define	TT_UNIMP_FLUSH		37	/* Not on SuperSPARC */

/* 38 - 39	Unassigned, Reserved */
#define TT_CP_EXCPTN		40	/* priority  9 */
#define TT_DATA_ACCESS_ERROR	41	/* Not on SuperSPARC priority 12*/
#define TT_DATA_ERROR		41	/* priority  12 */
#define TT_DIVISION_BY_ZERO	42	/* priority 15 */
#define	TT_DATA_STORE_ERROR	43	/* priority  2 */
#define	TT_DATA_ACCESS_MMU_MISS	44	/* Not on SuperSPARC */

/* 45 - 127	Unassigned, Reserved */
/* 128 - 255	User Defined Vectors (128) */
#define TT_USER_0		128	/* priority 12 */
#define TT_USER_127		255	/* priority 12 */

/* interrupt vector definitions */
#define IV_RESET		INUM_TO_IVEC (TT_RESET)
#define IV_INSTR_ACCESS_EXCPTN	INUM_TO_IVEC (TT_INSTR_ACCESS_EXCPTN)
#define IV_ILLEGAL_INSTR	INUM_TO_IVEC (TT_ILLEGAL_INSTR)
#define IV_PRIVILEGED_INSTR	INUM_TO_IVEC (TT_PRIVILEGED_INSTR)
#define IV_FP_DISABLED		INUM_TO_IVEC (TT_FP_DISABLED)
#define IV_WINDOW_OVERFLOW	INUM_TO_IVEC (TT_WINDOW_OVERFLOW)
#define IV_WINDOW_UNDERFLOW	INUM_TO_IVEC (TT_WINDOW_UNDERFLOW)
#define IV_MEM_ADDR_NOT_ALIGNED	INUM_TO_IVEC (TT_MEM_ADDR_NOT_ALIGNED)
#define IV_FP_EXCPTN		INUM_TO_IVEC (TT_FP_EXCPTN)
#define IV_DATA_ACCESS_EXCPTN	INUM_TO_IVEC (TT_DATA_ACCESS_EXCPTN)
#define IV_TAG_OVERFLOW		INUM_TO_IVEC (TT_TAG_OVERFLOW)

/* 11 - 16	Unassigned, Reserved */
/* 17 - 31	Autovectors: (aka "interrupt_level_xx" in the documentation) */

#define IV_AUTOVEC_1		INUM_TO_IVEC (TT_AUTOVEC_1)
#define IV_AUTOVEC_2		INUM_TO_IVEC (TT_AUTOVEC_2)
#define IV_AUTOVEC_3		INUM_TO_IVEC (TT_AUTOVEC_3)
#define IV_AUTOVEC_4		INUM_TO_IVEC (TT_AUTOVEC_4)
#define IV_AUTOVEC_5		INUM_TO_IVEC (TT_AUTOVEC_5)
#define IV_AUTOVEC_6		INUM_TO_IVEC (TT_AUTOVEC_6)
#define IV_AUTOVEC_7		INUM_TO_IVEC (TT_AUTOVEC_7)
#define IV_AUTOVEC_8		INUM_TO_IVEC (TT_AUTOVEC_8)
#define IV_AUTOVEC_9		INUM_TO_IVEC (TT_AUTOVEC_9)
#define IV_AUTOVEC_10		INUM_TO_IVEC (TT_AUTOVEC_10)
#define IV_AUTOVEC_11		INUM_TO_IVEC (TT_AUTOVEC_11)
#define IV_AUTOVEC_12		INUM_TO_IVEC (TT_AUTOVEC_12)
#define IV_AUTOVEC_13		INUM_TO_IVEC (TT_AUTOVEC_13)
#define IV_AUTOVEC_14		INUM_TO_IVEC (TT_AUTOVEC_14)
#define IV_AUTOVEC_15		INUM_TO_IVEC (TT_AUTOVEC_15)

#define	IV_REG_ACCESS_ERROR	INUM_TO_IVEC (TT_REG_ACCESS_ERROR)
#define IV_INSTR_ACCESS_ERROR	INUM_TO_IVEC (TT_INSTR_ACCESS_ERROR)

/* 34 - 35	Unassigned, Reserved */
#define IV_CP_DISABLED		INUM_TO_IVEC (TT_CP_DISABLED)
#define	IV_UNIMP_FLUSH		INUM_TO_IVEC (TT_UNIMP_FLUSH)

/* 37 - 39	Unassigned, Reserved */
#define IV_CP_EXCPTN		INUM_TO_IVEC (TT_CP_EXCPTN)
#define IV_DATA_ACCESS_ERROR	INUM_TO_IVEC (TT_DATA_ACCESS_ERROR)
#define IV_DATA_ERROR		INUM_TO_IVEC (TT_DATA_ERROR)
#define IV_DIVISION_BY_ZERO	INUM_TO_IVEC (TT_DIVISION_BY_ZERO)
#define	IV_DATA_STORE_ERROR	INUM_TO_IVEC (TT_DATA_STORE_ERROR)
#define	IV_DATA_ACCESS_MMU_MISS	INUM_TO_IVEC (TT_DATA_ACCESS_MMU_MISS)

/* 45 - 127	Unassigned, Reserved */
/* 128 - 255	User Defined Vectors (128) */
#define IV_USER_0		INUM_TO_IVEC (TT_USER_0)
#define IV_USER_127		INUM_TO_IVEC (TT_USER_127)
#define IV_USER(trapNum) 	INUM_TO_IVEC (TT_USER (trapNum))

#ifdef __cplusplus
}
#endif

#endif /* __INCivSparch */
