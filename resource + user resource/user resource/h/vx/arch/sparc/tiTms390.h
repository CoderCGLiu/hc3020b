/* tiTms390.h - TI TMS390 header for SuperSPARC. */

/* Copyright 1984-1995 Wind River Systems, Inc. */

/* Copyright 1994-1995 FORCE COMPUTERS */

/*
modification history
--------------------
01d,22mar95,vin  added copyright.	
01c,15mar95,vin  integrated with 5.2.
01b,21apr94,lsr  removed all globals for ASM.
01a,16jun93,tkj  written, derived from ti390.h 01a.
*/

#ifndef __INCtiTms390h
#define __INCtiTms390h

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE
#include "cacheLib.h"			/* CACHE_MODE, CACHE_TYPE */
#include "types/vxTypesOld.h"		/* UINT, USHORT */
#endif	/* _ASMLANGUAGE */

#include "arch/sparc/mmuSparcLib.h"	/*
					 * MMU_SFSR_L_MASK, MMU_SFSR_AT_MASK,
					 * MMU_SFSR_FT_MASK, MMU_SFSR_FAV_MASK,
					 * MMU_SFSR_OW_MASK,
					 * MMU_SFSR_VA, MMU_SFAR_VA
					 */

#include "arch/sparc/sparcASI.h"	/* ASI_MMU_PASS_THRU_BASE */

/*
 * Page Size is 4KB - 2^12 Bytes
 */

#ifndef	PAGE_SHIFT
#define PAGE_SHIFT                      12
#endif

#ifndef	PAGE_SIZE
#define PAGE_SIZE                       (1<<PAGE_SHIFT)
#endif

/*
 * The TI TMS390 has separate Instruction and Data Caches with the
 * following specifications:
 *
 * Size  Cache Type     Lines   Sets   Ways   Line Size     Mode
 *  20K   INSTRUCTION    320      64     5    2*32 bytes     never written back
 *  16K   DATA           512     128     4      32           See below
 *	MCNTL.MB == 0	With MultiCache Controller (MCC): Write-through
 *	MCNTL.MB == 1	Without MCC: Copy-back with write allocation
 *
 * For memory allocation purposes, "cache line/align size" must be the
 * larger of the two caches.
 */

/* These are needed by TI TMS390 specific routines. */
#define	INST_CACHE_LINE_SIZE		(2*32)
#define	DATA_CACHE_LINE_SIZE		32

#define	INST_CACHE_SETS			64
#define	DATA_CACHE_SETS			128

#define	INST_CACHE_LINES_PER_SET	5
#define	DATA_CACHE_LINES_PER_SET	4
					 
#define	INST_CACHE_SIZE		\
	(INST_CACHE_SETS * INST_CACHE_LINES_PER_SET * INST_CACHE_LINE_SIZE)
#define	DATA_CACHE_SIZE		\
	(DATA_CACHE_SETS * DATA_CACHE_LINES_PER_SET * DATA_CACHE_LINE_SIZE)

#define	INST_CACHE_ALL_LINES_VALID	0x1fU
#define	DATA_CACHE_ALL_LINES_VALID	0xfU

#define	INST_CACHE_ALL_LINES_LOCKED	(INST_CACHE_ALL_LINES_VALID - 1)
#define	DATA_CACHE_ALL_LINES_LOCKED	(DATA_CACHE_ALL_LINES_VALID - 1)

/* These are needed by the generic cache routines. */
#undef  _CACHE_ALIGN_SIZE
#define _CACHE_ALIGN_SIZE                    64
#define CACHE_LINE_SIZE                      _CACHE_ALIGN_SIZE

/*
 * Define the granularity of control of caching of data.  This will
 * be the same as the physical page size since cacheability is a
 * per-page concept controlled in each Page Table Entry (PTE).
 */

#define TITMS390_CACHE_PAGE_SIZE                PAGE_SIZE

/*
 * Cache invalidation is accomplished by invalidating the entire cache
 * (either Instruction or Data) first the lock bits then the valid bits
 * with two writes to an Alternate Space given by the following constants.
 *
 * WARNING: As this is a cache invalidate operation, not a cache clear
 * operation, any modified data will be lost if the data cache is in
 * copy-back mode, i.e. an external Multicache Controller is not used.
 */

#define TITMS390_INSTRUCTION_CACHE_INVALIDATE   0x36
#define TITMS390_DATA_CACHE_INVALIDATE          0x37

#define	TITMS390_INVALIDATE_LOCK_BITS		0x80000000
#define	TITMS390_INVALIDATE_VALID_BITS		0

/*
 * On-chip Caching is enabled by setting the bit corresponding to a particular
 * cache in the MMU Control Register (MCNTL) otherwise known as the
 * System Control Register (SCR) in the reference MMU implementation.
 */

/*
 * MMU Control Register (MCNTL)
 *
 * This is a superset of the System Control Register (SCR) in mmuSparcLib.h.
 */

/* MCNTL.IMPL Implementation */
#define	MMU_MCNTL_IMPL_MASK		       0xf0000000
#define	MMU_MCNTL_IMPL_SHIFT		       28

/* MCNTL.VER Version */
#define	MMU_MCNTL_VER_MASK		       0x0f000000
#define	MMU_MCNTL_VER_SHIFT		       24

/* MCNTL.TC = Table Walk Cacheable Bit */
#define MMU_MCNTL_TC_MASK                      0x00010000
#define MMU_MCNTL_TC_ENABLE                    0x00010000
#define MMU_MCNTL_TC_ENABLE_NOT                0x00000000
#define MMU_MCNTL_TC_DISABLE                   MMU_MCNTL_TC_ENABLE_NOT

/* MCNTL.AC = Alternate Cacheable Bit */
#define MMU_MCNTL_AC_MASK                      0x00008000
#define MMU_MCNTL_AC_ENABLE                    0x00008000
#define MMU_MCNTL_AC_ENABLE_NOT                0x00000000
#define MMU_MCNTL_AC_DISABLE                   MMU_MCNTL_AC_ENABLE_NOT
#define MMU_MCNTL_AC_SHIFT                     15

/* MCNTL.SE = Snoop Enable */
#define MMU_MCNTL_SE_MASK                      0x00004000
#define MMU_MCNTL_SE_ENABLE                    0x00004000
#define MMU_MCNTL_SE_ENABLE_NOT                0x00000000
#define MMU_MCNTL_SE_DISABLE                   MMU_MCNTL_SE_ENABLE_NOT

/* MCNTL.BT = Boot Mode */
#define MMU_MCNTL_BT_MASK                      0x00002000
#define MMU_MCNTL_BT_BOOT                      0x00002000
#define MMU_MCNTL_BT_BOOT_NOT                  0x00000000

/* MCNTL.PE = Parity Enable */
#define MMU_MCNTL_PE_MASK                      0x00001000
#define MMU_MCNTL_PE_EVEN                      0x00001000
#define MMU_MCNTL_PE_ODD                       0x00000000

/* MCNTL.MB = MBus Mode */
#define MMU_MCNTL_MB_MASK                      0x00000800
#define MMU_MCNTL_MB_WITHOUT_MCC               0x00000800
#define MMU_MCNTL_MB_WITH_MCC                  0x00000000

/* MCNTL.SB = Store Buffer Enable */
#define MMU_MCNTL_SB_MASK                      0x00000400
#define MMU_MCNTL_SB_ENABLE                    0x00000400
#define MMU_MCNTL_SB_ENABLE_NOT                0x00000000
#define MMU_MCNTL_SB_DISABLE                   MMU_MCNTL_SB_ENABLE_NOT
#define MMU_MCNTL_SB_SHIFT		       10

/* MCNTL.IE = Instruction Cache Enable */
#define MMU_MCNTL_IE_MASK                      0x00000200
#define MMU_MCNTL_IE_ENABLE                    0x00000200
#define MMU_MCNTL_IE_ENABLE_NOT                0x00000000
#define MMU_MCNTL_IE_DISABLE                   MMU_MCNTL_IE_ENABLE_NOT

/* MCNTL.DE = Data Cache Enable */
#define MMU_MCNTL_DE_MASK                      0x00000100
#define MMU_MCNTL_DE_ENABLE                    0x00000100
#define MMU_MCNTL_DE_ENABLE_NOT                0x00000000
#define MMU_MCNTL_DE_DISABLE                   MMU_MCNTL_DE_ENABLE_NOT

/* MCNTL.PSO = Partial Store Ordering */
#define MMU_MCNTL_PSO_MASK                     0x00000080
#define MMU_MCNTL_PSO_ENABLE                   0x00000080
#define MMU_MCNTL_PSO_ENABLE_NOT               0x00000000
#define MMU_MCNTL_PSO_DISABLE                  MMU_MCNTL_PSO_ENABLE_NOT

/* MCNTL.NF = No-Fault Bit */
#define MMU_MCNTL_NF_MASK                      0x00000002
#define MMU_MCNTL_NF_ENABLE                    0x00000002
#define MMU_MCNTL_NF_ENABLE_NOT                0x00000000
#define MMU_MCNTL_NF_DISABLE                   MMU_MCNTL_NF_ENABLE_NOT

/* MCNTL.EN = MMU Enable */
#define MMU_MCNTL_EN_MASK                      0x00000001
#define MMU_MCNTL_EN_ENABLE                    0x00000001
#define MMU_MCNTL_EN_ENABLE_NOT                0x00000000
#define MMU_MCNTL_EN_DISABLE                   MMU_MCNTL_EN_ENABLE_NOT

/*
 * MMU Fault Status Register (MFSR)
 *
 * This is the same as the Synchronous Fault Status Register (SFSR) in
 * mmuSparcLib.h.
 */

#define	MMU_MFSR_VA			MMU_SFSR_VA
#define	MMU_MFSR_RW_VA			0x1300 /* Read/Write address */

#define	TITMS390_MFSR_EM_MASK		0x00020000 /* Error Mode Reset */
#define	TITMS390_MFSR_EM_SET		0x00020000
#define	TITMS390_MFSR_EM_CLEARED	0

					/* Control Space Access Error */
#define	TITMS390_MFSR_CS_MASK		0x00010000
#define	TITMS390_MFSR_CS_SET		0x00010000
#define	TITMS390_MFSR_CS_CLEARED	0

#define	TITMS390_MFSR_SB_MASK		0x00008000 /* Store Buffer Error */
#define	TITMS390_MFSR_SB_SET		0x00008000
#define	TITMS390_MFSR_SB_CLEARED	0

#define	TITMS390_MFSR_P_MASK		0x00004000 /* Parity Error */
#define	TITMS390_MFSR_P_SET		0x00004000
#define	TITMS390_MFSR_P_CLEARED		0

#define	TITMS390_MFSR_UD_MASK		0x00002000 /* Undefined Error */
#define	TITMS390_MFSR_UD_SET		0x00002000
#define	TITMS390_MFSR_UD_CLEARED	0

#define	TITMS390_MFSR_UC_MASK		0x00001000 /* Uncorrectable Error */
#define	TITMS390_MFSR_UC_SET		0x00001000
#define	TITMS390_MFSR_UC_CLEARED	0

#define	TITMS390_MFSR_TO_MASK		0x00000800 /* Time-Out */
#define	TITMS390_MFSR_TO_SET		0x00000800
#define	TITMS390_MFSR_TO_CLEARED	0

#define	TITMS390_MFSR_BE_MASK		0x00000400 /* Bus Error */
#define	TITMS390_MFSR_BE_SET		0x00000400
#define	TITMS390_MFSR_BE_CLEARED	0

#define	TITMS390_MFSR_L_MASK		MMU_SFSR_L_MASK	/* Level */
#define	TITMS390_MFSR_L_SHIFT		8

#define	TITMS390_MFSR_AT_MASK		MMU_SFSR_AT_MASK /* Access Type */
#define	TITMS390_MFSR_AT_SHIFT		5

#define	TITMS390_MFSR_FT_MASK		MMU_SFSR_FT_MASK /* Fault Type */
#define	TITMS390_MFSR_FT_SHIFT		2

					/* Fault Address Valid */
#define	TITMS390_MFSR_FAV_MASK		MMU_SFSR_FAV_MASK
#define	TITMS390_MFSR_FAV_SET		MMU_SFSR_FAV_MASK
#define	TITMS390_MFSR_FAV_CLEARED	0

#define	TITMS390_MFSR_OW_MASK		MMU_SFSR_OW_MASK /* Overwrite */
#define	TITMS390_MFSR_OW_SET		MMU_SFSR_OW_MASK
#define	TITMS390_MFSR_OW_CLEARED	0

/*
 * MMU Fault Address Register (MFAR)
 *
 * This is the same as the Synchronous Fault Address Register (SFAR) in
 * mmuSparcLib.h.
 */

#define	MMU_MFAR_VA			MMU_SFAR_VA
#define	MMU_MFAR_RW_VA			0x1400 /* Read/Write address */

/*
 * MMU Breakpoint Value Register (BKV)
 *
 * This is a double word register.  _TOP_, and _BTM_ entries refer to the
 * most significant, and least significant words, respectively.
 */

#define	MMU_BKV_VA			0x000

#define	MMU_BKV_TOP_BKV_MASK		0x0000000f
#define	MMU_BKV_TOP_BKV_SHIFT		0

#define	MMU_BKV_BTM_BKV_MASK		0xffffffff
#define	MMU_BKV_BTM_BKV_SHIFT		0

/*
 * MMU Breakpoint Mask Register (BKM)
 *
 * This is a double word register.  _TOP_, and _BTM_ entries refer to the
 * most significant, and least significant words, respectively.
 */

#define	MMU_BKM_VA			0x100

#define	MMU_BKM_TOP_BKM_MASK		0x0000000f
#define	MMU_BKM_TOP_BKM_SHIFT		0

#define	MMU_BKM_BTM_BKM_MASK		0xffffffff
#define	MMU_BKM_BTM_BKM_SHIFT		0

/*
 * MMU Breakpoint Control Register (BKC)
 *
 * This is a double word register.  _TOP_, and _BTM_ entries refer to the
 * most significant, and least significant words, respectively.
 */

#define	MMU_BKC_VA			0x200

#define	MMU_BKC_BTM_CSPACE_MASK		0x00000040
#define	MMU_BKC_BTM_CSPACE_CODE		0x00000040
#define	MMU_BKC_BTM_CSPACE_DATA		0

#define	MMU_BKC_BTM_PAMD_MASK		0x00000020
#define	MMU_BKC_BTM_PAMD_PHYSICAL	0x00000020
#define	MMU_BKC_BTM_PAMD_VIRTUAL	0

#define	MMU_BKC_BTM_CBFEN_MASK		0x00000010
#define	MMU_BKC_BTM_CBFEN_EXCEPTION	0x00000010
#define	MMU_BKC_BTM_CBFEN_ACTION	0

#define	MMU_BKC_BTM_CBKEN_MASK		0x00000008
#define	MMU_BKC_BTM_CBKEN_ENABLE	0x00000008
#define	MMU_BKC_BTM_CBKEN_ENABLE_NOT	0
#define	MMU_BKC_BTM_CBKEN_DISABLE	MMU_BKC_BTM_CBKEN_ENABLE_NOT

#define	MMU_BKC_BTM_DBFEN_MASK		0x00000004
#define	MMU_BKC_BTM_DBFEN_EXCEPTION	0x00000004
#define	MMU_BKC_BTM_DBFEN_ACTION	0

#define	MMU_BKC_BTM_DBREN_MASK		0x00000002
#define	MMU_BKC_BTM_DBREN_ENABLE	0x00000002
#define	MMU_BKC_BTM_DBREN_ENABLE_NOT	0
#define	MMU_BKC_BTM_DBREN_DISABLE	MMU_BKC_BTM_DBREN_ENABLE_NOT

#define	MMU_BKC_BTM_DBWEN_MASK		0x00000001
#define	MMU_BKC_BTM_DBWEN_ENABLE	0x00000001
#define	MMU_BKC_BTM_DBWEN_ENABLE_NOT	0
#define	MMU_BKC_BTM_DBWEN_DISABLE	MMU_BKC_BTM_DBWEN_ENABLE_NOT

/*
 * MMU Breakpoint Status Register (BKS)
 *
 * This is a double word register.  _TOP_, and _BTM_ entries refer to the
 * most significant, and least significant words, respectively.
 */

#define	MMU_BKS_VA			0x300

#define	MMU_BKS_BTM_CBKIS_MASK		0x00000008
#define	MMU_BKS_BTM_CBKIS_SET		0x00000008
#define	MMU_BKS_BTM_CBKIS_CLEARED	0

#define	MMU_BKS_BTM_CBKFS_MASK		0x00000004
#define	MMU_BKS_BTM_CBKFS_SET		0x00000004
#define	MMU_BKS_BTM_CBKFS_CLEARED	0

#define	MMU_BKS_BTM_DBKIS_MASK		0x00000002
#define	MMU_BKS_BTM_DBKIS_SET		0x00000002
#define	MMU_BKS_BTM_DBKIS_CLEARED	0

#define	MMU_BKS_BTM_DBKFS_MASK		0x00000001
#define	MMU_BKS_BTM_DBKFS_SET		0x00000001
#define	MMU_BKS_BTM_DBKFS_CLEARED	0

/*
 * Counter Breakpoint Value Register (CTRV)
 */

#define	CTRV_ICNT_MASK			0xffff0000
#define	CTRV_ICNT_SHIFT			16

#define	CTRV_CCNT_MASK			0x0000ffff
#define	CTRV_CCNT_SHIFT			0

/*
 * Counter Breakpoint Control Register (CTRC)
 */

#define	CTRC_ICNTEN_MASK		0x00000002
#define	CTRC_ICNTEN_ENABLE		0x00000002
#define	CTRC_ICNTEN_ENABLE_NOT		0
#define	CTRC_ICNTEN_DISABLE		CTRC_ICNTEN_ENABLE_NOT

#define	CTRC_CCNTEN_MASK		0x00000001
#define	CTRC_CCNTEN_ENABLE		0x00000001
#define	CTRC_CCNTEN_ENABLE_NOT		0
#define	CTRC_CCNTEN_DISABLE		CTRC_CCNTEN_ENABLE_NOT

/*
 * Counter Breakpoint Status Register (CTRS)
 */

#define	CTRS_ZICIS_MASK			0x00000002
#define	CTRS_ZICIS_SET			0x00000002
#define	CTRS_ZICIS_CLEARED		0

#define	CTRS_ZCCIS_MASK			0x00000001
#define	CTRS_ZCCIS_SET			0x00000001
#define	CTRS_ZCCIS_CLEARED		0

/*
 * Breakpoint Action Register (ACTION)
 *
 * This is a double word register.  _TOP_, and _BTM_ entries refer to the
 * most significant, and least significant words, respectively.
 */

#define	ACTION_BTM_MIX_MASK		0x00001000
#define	ACTION_BTM_MIX_ENABLE		0x00001000
#define	ACTION_BTM_MIX_ENABLE_NOT	0
#define	ACTION_BTM_MIX_DISABLE		ACTION_BTM_MIX_ENABLE_NOT

#define	ACTION_BTM_BCIPL_MASK		0x00000f00
#define	ACTION_BTM_BCIPL_SHIFT		8

#define	ACTION_BTM_E_CBK_MASK		0x00000080
#define	ACTION_BTM_E_CBK_ENABLE		0x00000080
#define	ACTION_BTM_E_CBK_ENABLE_NOT	0
#define	ACTION_BTM_E_CBK_DISABLE	ACTION_BTM_E_CBK_ENABLE_NOT

#define	ACTION_BTM_E_ZIC_MASK		0x00000040
#define	ACTION_BTM_E_ZIC_ENABLE		0x00000040
#define	ACTION_BTM_E_ZIC_ENABLE_NOT	0
#define	ACTION_BTM_E_ZIC_DISABLE	ACTION_BTM_E_ZIC_ENABLE_NOT

#define	ACTION_BTM_E_DBK_MASK		0x00000020
#define	ACTION_BTM_E_DBK_ENABLE		0x00000020
#define	ACTION_BTM_E_DBK_ENABLE_NOT	0
#define	ACTION_BTM_E_DBK_DISABLE	ACTION_BTM_E_DBK_ENABLE_NOT

#define	ACTION_BTM_E_ZCC_MASK		0x00000010
#define	ACTION_BTM_E_ZCC_ENABLE		0x00000010
#define	ACTION_BTM_E_ZCC_ENABLE_NOT	0
#define	ACTION_BTM_E_ZCC_DISABLE	ACTION_BTM_E_ZCC_ENABLE_NOT

#define	ACTION_BTM_I_CBK_MASK		0x00000008
#define	ACTION_BTM_I_CBK_ENABLE		0x00000008
#define	ACTION_BTM_I_CBK_ENABLE_NOT	0
#define	ACTION_BTM_I_CBK_DISABLE	ACTION_BTM_I_CBK_ENABLE_NOT

#define	ACTION_BTM_I_ZIC_MASK		0x00000004
#define	ACTION_BTM_I_ZIC_ENABLE		0x00000004
#define	ACTION_BTM_I_ZIC_ENABLE_NOT	0
#define	ACTION_BTM_I_ZIC_DISABLE	ACTION_BTM_I_ZIC_ENABLE_NOT

#define	ACTION_BTM_I_DBK_MASK		0x00000002
#define	ACTION_BTM_I_DBK_ENABLE		0x00000002
#define	ACTION_BTM_I_DBK_ENABLE_NOT	0
#define	ACTION_BTM_I_DBK_DISABLE	ACTION_BTM_I_DBK_ENABLE_NOT

#define	ACTION_BTM_I_ZCC_MASK		0x00000001
#define	ACTION_BTM_I_ZCC_ENABLE		0x00000001
#define	ACTION_BTM_I_ZCC_ENABLE_NOT	0
#define	ACTION_BTM_I_ZCC_DISABLE	ACTION_BTM_I_ZCC_ENABLE_NOT

/*
 * The TI TMS390 supports 2^10 (1024) to 2^16 (65,536) entries
 * in the Context Table.
 */

#define TITMS390_MIN_SPARC_CONTEXTS              1024
#define	TITMS390_MAX_SPARC_CONTEXTS		65536

#define	TA_TYPE_STAG	1	/* Cache Tag Address Type: Set tag */
#define TA_TYPE_PTAG	2	/* Cache Tag Address Type: Physical tag */

/*
 * MMU TLB Diagnostic Access Address
 *
 * Use ASI_MMU_DIAG_INST_DATA_TLB to access this.
 */

#define	MMU_TLB_TLBE_MASK	0x0003f000
#define	MMU_TLB_TLBE_SHIFT	11

#define	MMU_TLB_SEL_MASK	0x00000700

#define	MMU_TLB_SEL_VPN		0x00000000 /* TLB Entry Virtual Page Number */
#define	MMU_TLB_VPN_MASK	0xfffff000
#define	MMU_TLB_VPN_SHIFT	12

#define	MMU_TLB_SEL_CTX		0x00000100 /* TLB Entry Context Number */
#define	MMU_TLB_CTX_MASK	0x0000ffff
#define	MMU_TLB_CTX_SHIFT	0

#define	MMU_TLB_SEL_PPN		0x00000200 /* TLB Entry Physical Page Number */
/*
 * Except for MMU_PTE_R_MASK and MMU_PTE_ET_xxx use MMU_PTE_xxx_xxx 
 * to access the bits.  Those bits are accessed by the defines below.
 */

#define	MMU_TLB_PPN_V_MASK	0x00000020
#define	MMU_TLB_PPN_V_VALID	0x00000020
#define	MMU_TLB_PPN_V_VALID_NOT	0

#define	MMU_TLB_PPN_LVL_MASK	0x00000003
#define	MMU_TLB_PPN_LVL_CTX	0	/* Context:   4G */
#define	MMU_TLB_PPN_LVL_RGN	1	/* Region:   16M */
#define	MMU_TLB_PPN_LVL_SGM	2	/* Segment: 256K */
#define	MMU_TLB_PPN_LVL_PG	3	/* Page:      4K */

#define	MMU_TLB_SEL_LCK		0x00000300 /* TLB Entry Lock Bit */
#define	MMU_TLB_LCK_MASK	0x00000001
#define	MMU_TLB_LCK_LOCKED	0x00000001
#define	MMU_TLB_LCK_LOCKED_NOT	0
#define	MMU_TLB_LCK_UNLOCKED	MMU_TLB_LCK_LOCKED_NOT

#define	MMU_TLB_SEL_RTP		0x00000400 /* Cached Root Pointer */
#define	MMU_TLB_RTP_RPR_MASK	0xfffffffc
#define	MMU_TLB_RTP_RPR_SHIFT	2

#define	MMU_TLB_RTP_V_MASK	0x00000003
#define	MMU_TLB_RTP_V_INVALID	0x00000000
#define	MMU_TLB_RTP_V_VALID	0x00000001
#define	MMU_TLB_RTP_V_UNUSED1	0x00000002
#define	MMU_TLB_RTP_V_UNUSED2	0x00000003

#define	MMU_TLB_SEL_L2P		0x00000500 /* Cached Level 2 Pointer */
#define	MMU_TLB_L2P_L2PTP_MASK	0xfffffffc
#define	MMU_TLB_L2P_L2PTP_SHIFT	2

#define	MMU_TLB_L2P_V_MASK	0x00000003
#define	MMU_TLB_L2P_V_INVALID	0x00000000
#define	MMU_TLB_L2P_V_VALID	0x00000001
#define	MMU_TLB_L2P_V_UNUSED1	0x00000002
#define	MMU_TLB_L2P_V_UNUSED2	0x00000003

#define	MMU_TLB_SEL_L2V		0x00000600 /* Cached Level 2 Virtual Address */
#define	MMU_TLB_L2V_L2VA_MASK	0xfffc0000
#define	MMU_TLB_L2V_L2VA_SHIFT	18

/*
 * Store Buffer Control Register (SBCNTL)
 */

#define	SBCNTL_SE_MASK		0x00000100
#define	SBCNTL_SE_ENABLED	0x00000100
#define	SBCNTL_SE_ENABLED_NOT	0
#define	SBCNTL_SE_DISABLED	SBCNTL_SE_ENABLED_NOT
#define	SBCNTL_SE_SHIFT		8

#define	SBCNTL_EM_MASK		0x000000080
#define	SBCNTL_EM_EMPTY		0x000000080
#define	SBCNTL_EM_EMPTY_NOT	0

#define	SBCNTL_ER_MASK		0x000000040
#define	SBCNTL_ER_ERROR		0x000000040
#define	SBCNTL_ER_ERROR_NOT	0

#define	SBCNTL_DPTR_MASK	0x000000038
#define	SBCNTL_DPTR_SHIFT	3

#define	SBCNTL_FPTR_MASK	0x000000007
#define	SBCNTL_FPTR_SHIFT	0

/*
 * Address of store buffer tag or data entries
 */

#define	SBADDR_ENTRY_MASK		0x00000038
#define	SBADDR_ENTRY_SHIFT		3

/*
 * Store Buffer Tags
 */

#define	SBTAG_TOP_SP_MASK		0x00000400
#define	SBTAG_TOP_SP_BARRIER		0x00000400
#define	SBTAG_TOP_SP_BARRIER_NOT	0

#define	SBTAG_TOP_BST_MASK		0x00000200
#define	SBTAG_TOP_BST_BURST		0x00000200
#define	SBTAG_TOP_BST_BURST_NOT		0

#define	SBTAG_TOP_V_MASK		0x00000100
#define	SBTAG_TOP_V_VALID		0x00000100
#define	SBTAG_TOP_V_VALID_NOT		0

#define	SBTAG_TOP_S_MASK		0x00000080
#define	SBTAG_TOP_S_SUP			0x00000080
#define	SBTAG_TOP_S_SUP_NOT		0
#define	SBTAG_TOP_S_USER		SBTAG_TOP_S_SUP_NOT

#define	SBTAG_TOP_C_MASK		0x00000040
#define	SBTAG_TOP_C_CACHEABLE		0x00000040
#define	SBTAG_TOP_C_CACHEABLE_NOT	0
#define	SBTAG_TOP_C_SHIFT		6

#define	SBTAG_TOP_SIZE_MASK		0x00000030
#define	SBTAG_TOP_SIZE_BYTE		0
#define	SBTAG_TOP_SIZE_HALFWORD		0x00000010
#define	SBTAG_TOP_SIZE_WORD		0x00000020
#define	SBTAG_TOP_SIZE_DOUBLEWORD	0x00000030
#define	SBTAG_TOP_SIZE_SHIFT		4

#define	SBTAG_TOP_ADDR_MASK		0x0000000f
#define	SBTAG_TOP_ADDR_SHIFT		0

#define	SBTAG_BTM_ADDR_MASK		0xffffffff
#define	SBTAG_BTM_ADDR_SHIFT		0

/*
 * Store Buffer Data
 */

#ifndef	_ASMLANGUAGE

#define	MCC_INSTALLED()		\
	    ((mmuGetSCR() & MMU_MCNTL_MB_MASK) == MMU_MCNTL_MB_WITH_MCC)

/* variable declarations */

/*
 * These MUST be set BEFORE calling cacheLibInit().
 *
 * mccEcacheSize = the e-cache size, or don't care if an MCC is not used.
 */

IMPORT int	mccEcacheSize;		/* Size of e-cache on MCC */
IMPORT int	mccLowerBound;		/* Lower physical address bound */
					/* for mccClearEntire () */

/* typedefs */

/* For on-chip Data cache */

typedef struct /* Split virtual address into fields used by the Data Cache */
    {
	unsigned	dva_page     :20; /* Convert to physical 24 bits */
					/* then dpt_padrs field of Ptag */
	unsigned	dva_set      : 7; /* Set selection 0-127 */
	unsigned	dva_byte     : 5; /* Byte selection within line 0-31 */
    } TITMS390_DATA_VA;

typedef union /* Convert virtual address to/from split form */
    {
	void *			pointer; /* Whole virtual address */
	TITMS390_DATA_VA	split;	/* Split virtual address */
    } TITMS390_DATA_VA_UNION;

typedef struct /* On-chip Data Cache Data Address for TITMS390_DD_ASI */
    {
	unsigned	dda_unused   : 4; /* First element can't be unnamed */
	unsigned	dda_line     : 2; /* Line within set 0-3 */
	unsigned	             :14;
	unsigned	dda_set      : 7; /* Set 0-127 */
	unsigned	dda_dword    : 2; /* Double word within line 0-3 */
	unsigned	             : 3;
    } TITMS390_DATA_DATA_ADRS;

typedef struct /* On-chip Data Cache Tag Address for TITMS390_DT_ASI */
    {
	unsigned	dta_type     : 2; /* Type of tag, Stag or Ptag */
	unsigned	             : 2;
	unsigned	dta_line     : 2; /* Line within set 0-3 */
	unsigned	             :14;
	unsigned	dta_set      : 7; /* Set 0-127 */
	unsigned	             : 5;
    } TITMS390_DATA_TAG_ADRS;

/* 
NOTE: All TITMS390_DATA_DATA_ADRS and TITMS390_DATA_TAG_ADRS fields that
are common are in the same place.  Each includes fields that are not in
the other.  Thus both can be combined into one union which can be used for
either or both by initializing all needed fields.
*/

typedef union /* On-chip Data Cache Tag Address Union */
    {
	UINT			word;	/* For clearing reserved fields */
	TITMS390_DATA_DATA_ADRS	data;	/* Data address */
	TITMS390_DATA_TAG_ADRS	tag;	/* Tag address */
    } TITMS390_DATA_ADRS_UNION;

typedef struct /* On-chip Data Cache Physical Tag */
    {
	unsigned	dpt_unused   : 7; /* First element can't be unnamed */
	unsigned	dpt_valid    : 1; /* Line is valid */
	unsigned		     : 7;
	unsigned	dpt_dirty    : 1; /* Line is dirty */
	unsigned		     : 7;
	unsigned	dpt_shared   : 1; /* Line is shared */
	unsigned		     : 8; /* Split to prevent extra padding */
	unsigned		     : 8;
	unsigned	dpt_padrs    :24; /* Physical address bits [35:12] */
    } TITMS390_DATA_PTAG;

typedef struct /* On-chip Data Cache Set Tag */
    {
	unsigned	dst_unused   :32; /* First element can't be unnamed */
	unsigned		     :20;
	unsigned	dst_mru      : 4; /* Most Recently Used: lines 3-0 */
	unsigned		     : 4;
	unsigned	dst_lck      : 4; /* Lock bits: lines 3-0 */
					  /* Lock bit for line 0 is always 0 */
    } TITMS390_DATA_STAG;

/* For on-chip Instruction cache */

/* Double word for write instruction cache data: tiTms390SetInstData() */
typedef UINT		DWORD[8/sizeof(UINT)];

/* Cache line for write instruction cache line: tiTms390SetInstLine() */
typedef UINT		INST_LINE[INST_CACHE_LINE_SIZE/sizeof(UINT)];

typedef struct /* Split virtual addr. into fields used by the Instr. Cache */
    {
	unsigned	iva_page     :20; /* Convert to physical 24 bits */
					/* then ipt_padrs field of Ptag */
	unsigned	iva_set      : 6; /* Set selection 0-63 */
	unsigned	iva_half     : 1; /* Half line selection 0-1 */
	unsigned	iva_byte     : 5; /* Byte selection within line 0-31 */
    } TITMS390_INST_VA;

typedef union /* Convert virtual address to/from split form */
    {
	void *			pointer; /* Whole virtual address */
	TITMS390_INST_VA	split;	/* Split virtual address */
    } TITMS390_INST_VA_UNION;

typedef struct /* On-chip Instruction Cache Data Address for TITMS390_ID_ASI */
    {
	unsigned	ida_unused   : 3; /* First element can't be unnamed */
	unsigned	ida_line     : 3; /* Line within set 0-4 */
	unsigned	             :14;
	unsigned	ida_set      : 6; /* Set 0-63 */
	unsigned	ida_dword    : 3; /* Double word within line 0-7 */
	unsigned	             : 3;
    } TITMS390_INST_DATA_ADRS;

typedef struct /* On-chip Instruction Cache Tag Address for TITMS390_IT_ASI */
    {
	unsigned	ita_type     : 2; /* Type of tag, Stag or Ptag */
	unsigned	             : 1;
	unsigned	ita_line     : 3; /* Line within set 0-4 */
	unsigned	             :14;
	unsigned	ita_set      : 6; /* Set 0-63 */
	unsigned	             : 6;
    } TITMS390_INST_TAG_ADRS;

/* 
NOTE: All TITMS390_INST_DATA_ADRS and TITMS390_INST_TAG_ADRS fields that
are common are in the same place.  Each includes fields that are not in
the other.  Thus both can be combined into one union which can be used for
either or both by initializing all needed fields.
*/

typedef union /* On-chip Instruction Cache Address Union */
    {
	UINT			word;	/* For clearing reserved fields */
	TITMS390_INST_DATA_ADRS	data;	/* Data address */
	TITMS390_INST_TAG_ADRS	tag;	/* Tag address */
    } TITMS390_INST_ADRS_UNION;

typedef struct /* On-chip Instruction Cache Physical Tag */
    {
	unsigned	ipt_unused   : 6; /* First element can't be unnamed */
	unsigned	ipt_valid    : 2; /* Half-line valid: high, low order*/
	unsigned		     :24;
	unsigned		     : 8; /* Split to prevent extra padding */
	unsigned	ipt_padrs    :24; /* Physical address bits [35:12] */
    } TITMS390_INST_PTAG;

typedef struct /* On-chip Instruction Cache Set Tag */
    {
	unsigned	ist_unused   :32; /* First element can't be unnamed */
	unsigned		     :19; /* First element can't be unnamed */
	unsigned	ist_mru	     : 5; /* Most Recently Used: lines 4-0 */
	unsigned		     : 3;
	unsigned	ist_lck	     : 5; /* Lock bits: lines 4-0 */
					  /* Lock bit for line 0 is always 0 */
    } TITMS390_INST_STAG;

#if defined(__STDC__) || defined(__cplusplus)

IMPORT char intAllDisable ();
IMPORT void intAllEnable (char);

IMPORT STATUS  cacheTiTms390LibInit   (CACHE_MODE  instMode,
                                    CACHE_MODE  dataMode);
IMPORT STATUS  cacheTiTms390Flush     (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTiTms390Lock      (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTiTms390Unlock    (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTiTms390PipeFlush ();
IMPORT void   *cacheTiTms390DmaMalloc (size_t       bytes);
IMPORT STATUS  cacheTiTms390DmaFree   (void        *pBuf);

IMPORT void   *cacheTiTms390VirtToPhys (void	   *address);
IMPORT void   *cacheTiTms390PhysToVirt (void	   *address);

IMPORT void    tiTms390ClearDataCache	   ();
IMPORT void    tiTms390InvalidateInstCache ();
IMPORT void    tiTms390InvalidateDataCache ();
IMPORT void    tiTms390GetInstTag     (UINT tagAddress,
				       void *ptagValue);
IMPORT void    tiTms390SetInstTag     (UINT tagAddress,
				       void *ptagValue);
IMPORT void    tiTms390GetInstData    (UINT dataAddress,
				       DWORD *pdataValue);
IMPORT void    tiTms390SetInstData    (UINT dataAddress,
				       DWORD *pdataValue);
IMPORT void    tiTms390SetInstLine    (UINT dataAddress,
				       INST_LINE *pdataValue);
IMPORT void    tiTms390GetDataTag     (UINT tagAddress,
				       void *ptagValue);
IMPORT void    tiTms390SetDataTag     (UINT tagAddress,
				       void *ptagValue);
IMPORT void    tiTms390GetDataData    (UINT tagAddress,
				       void *ptagValue);

IMPORT UCHAR   tiTms390AsiUCHARGet    (UINT asi,
				       void *phyAddress);
IMPORT char    tiTms390AsiCHARGet     (UINT asi,
				       void *phyAddress);
IMPORT USHORT  tiTms390AsiUSHORTGet   (UINT asi,
				       void *phyAddress);
IMPORT short   tiTms390AsiSHORTGet    (UINT asi,
				       void *phyAddress);
IMPORT UINT    tiTms390AsiUINTGet     (UINT asi,
				       void *phyAddress);
IMPORT int     tiTms390AsiINTGet      (UINT asi,
				       void *phyAddress);
IMPORT void    tiTms390AsiULINTGet    (UINT asi,
				       void *phyAddress,
				       UINT *top,
				       UINT *btm);

IMPORT void    tiTms390AsiUCHARSet    (UINT asi,
				       void *phyAddress,
				       UCHAR value);
IMPORT void    tiTms390AsiCHARSet     (UINT asi,
				       void *phyAddress,
				       char value);
IMPORT void    tiTms390AsiUSHORTSet   (UINT asi,
				       void *phyAddress,
				       USHORT value);
IMPORT void    tiTms390AsiSHORTSet    (UINT asi,
				       void *phyAddress,
				       short value);
IMPORT void    tiTms390AsiUINTSet     (UINT asi,
				       void *phyAddress,
				       UINT value);
IMPORT void    tiTms390AsiINTSet      (UINT asi,
				       void *phyAddress,
				       int value);
IMPORT void    tiTms390AsiULINTSet    (UINT asi,
				       void *phyAddress,
				       UINT top,
				       UINT btm);

IMPORT UCHAR   tiTms390PhyUCHARGet    (UINT ppn,
				       void *phyAddress);
IMPORT char    tiTms390PhyCHARGet     (UINT ppn,
				       void *phyAddress);
IMPORT USHORT  tiTms390PhyUSHORTGet   (UINT ppn,
				       void *phyAddress);
IMPORT short   tiTms390PhySHORTGet    (UINT ppn,
				       void *phyAddress);
IMPORT UINT    tiTms390PhyUINTGet     (UINT ppn,
				       void *phyAddress);
IMPORT int     tiTms390PhyINTGet      (UINT ppn,
				       void *phyAddress);

IMPORT void    tiTms390PhyUCHARSet    (UINT ppn,
				       void *phyAddress,
				       UCHAR value);
IMPORT void    tiTms390PhyCHARSet     (UINT ppn,
				       void *phyAddress,
				       char value);
IMPORT void    tiTms390PhyUSHORTSet   (UINT ppn,
				       void *phyAddress,
				       USHORT value);
IMPORT void    tiTms390PhySHORTSet    (UINT ppn,
				       void *phyAddress,
				       short value);
IMPORT void    tiTms390PhyUINTSet     (UINT ppn,
				       void *phyAddress,
				       UINT value);
IMPORT void    tiTms390PhyINTSet      (UINT ppn,
				       void *phyAddress,
				       int value);

IMPORT void	tiTms390PhyVrtBcopyBytes (UINT ppn,
				       char *source,
				       char *dest,
				       int nbytes);
IMPORT void	tiTms390VrtPhyBcopyBytes (char *source,
				       UINT ppn,
				       char *dest,
				       int nbytes);

#else   /* __STDC__ */

IMPORT char intAllDisable ();
IMPORT void intAllEnable ();

IMPORT STATUS  cacheTiTms390LibInit   ();
IMPORT STATUS  cacheTiTms390Flush     ();
IMPORT STATUS  cacheTiTms390Lock      ();
IMPORT STATUS  cacheTiTms390Unlock    ();
IMPORT STATUS  cacheTiTms390PipeFlush ();
IMPORT void   *cacheTiTms390DmaMalloc ();
IMPORT STATUS  cacheTiTms390DmaFree   ();

IMPORT void   *cacheTiTms390VirtToPhys ();
IMPORT void   *cacheTiTms390PhysToVirt ();

IMPORT void    tiTms390ClearDataCache ();
IMPORT void    tiTms390InvalidateInstCache ();
IMPORT void    tiTms390InvalidateDataCache ();

IMPORT void    tiTms390GetInstTag     ();
IMPORT void    tiTms390SetInstTag     ();
IMPORT void    tiTms390GetInstData    ();
IMPORT void    tiTms390SetInstData    ();
IMPORT void    tiTms390SetInstLine    ();
IMPORT void    tiTms390GetDataTag     ();
IMPORT void    tiTms390SetDataTag     ();
IMPORT void    tiTms390GetDataData    ();

IMPORT UCHAR   tiTms390AsiUCHARGet    ();
IMPORT char    tiTms390AsiCHARGet     ();
IMPORT USHORT  tiTms390AsiUSHORTGet   ();
IMPORT short   tiTms390AsiSHORTGet    ();
IMPORT UINT    tiTms390AsiUINTGet     ();
IMPORT int     tiTms390AsiINTGet      ();
IMPORT void    tiTms390AsiULINTGet    ();

IMPORT UCHAR   tiTms390AsiUCHARSet    ();
IMPORT char    tiTms390AsiCHARSet     ();
IMPORT USHORT  tiTms390AsiUSHORTSet   ();
IMPORT short   tiTms390AsiSHORTSet    ();
IMPORT UINT    tiTms390AsiUINTSet     ();
IMPORT int     tiTms390AsiINTSet      ();
IMPORT void    tiTms390AsiULINTSet    ();

IMPORT UCHAR   tiTms390PhyUCHARGet    ();
IMPORT char    tiTms390PhyCHARGet     ();
IMPORT USHORT  tiTms390PhyUSHORTGet   ();
IMPORT short   tiTms390PhySHORTGet    ();
IMPORT UINT    tiTms390PhyUINTGet     ();
IMPORT int     tiTms390PhyINTGet      ();

IMPORT void    tiTms390PhyUCHARSet    ();
IMPORT void    tiTms390PhyCHARSet     ();
IMPORT void    tiTms390PhyUSHORTSet   ();
IMPORT void    tiTms390PhySHORTSet    ();
IMPORT void    tiTms390PhyUINTSet     ();
IMPORT void    tiTms390PhyINTSet      ();

IMPORT void	tiTms390PhyVrtBcopyBytes ();
IMPORT void	tiTms390VrtPhyBcopyBytes ();

#endif  /* __STDC__ */

#endif	/* ~ _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCtiTms390h */
