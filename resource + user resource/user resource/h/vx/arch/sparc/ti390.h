/* ti390.h - TI TMS390S10 header for SPARC. */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,16aug93,ccc  submitted to wise.
01b,12may93,ccc  changed paths to standard.
01a,07apr93,edm  written.
    26apr93,ccc  changed path to build in sunec directory.
*/

#ifndef __INCti390h
#define __INCti390h

#ifdef __cplusplus
extern "C" {
#endif

#include "cacheLib.h"
#include "arch/sparc/mmuSparcLib.h"

/*
 * The TI TMS390S10 has separate Instruction and Data Caches with the
 * following specifications:
 *
 *    Cache Type     Lines     Line Size (bytes)     Mode
 *     INSTRUCTION    128       32                    never written back
 *     DATA           128       16                    only Write-Thru
 *
 * For memory allocation purposes, "cache line/align size" must be the
 * larger of the two caches.
 */

#undef  _CACHE_ALIGN_SIZE
#define _CACHE_ALIGN_SIZE                    32
#define CACHE_LINE_SIZE                      _CACHE_ALIGN_SIZE

/*
 * Define the granularity of control of caching of data.  This will
 * be the same as the physical page size since cacheability is a
 * per-page concept controlled in each Page Table Entry (PTE).
 */

#define TI390_CACHE_PAGE_SIZE                PAGE_SIZE

/*
 * Cache flushing is accomplished by invalidating the entire cache
 * (either Instruction or Data) with a single Write to an Alternate
 * Space given by the following constants.
 */

#define TI390_INSTRUCTION_CACHE_FLUSH_CLEAR  0x36
#define TI390_DATA_CACHE_FLUSH_CLEAR         0x37

/*
 * Caching is enabled by setting the bit corresponding to a particular
 * cache in the Processor Control Register (CR) otherwise known as the
 * System Control Register (SCR) in the reference MMU implementation.
 */

#define MMU_SCR_IC_MASK                      0x00000200
#define MMU_SCR_IC_ENABLE                    0x00000200
#define MMU_SCR_IC_ENABLE_NOT                0x00000000
#define MMU_SCR_IC_DISABLE                   MMU_SCR_IC_ENABLE_NOT

#define MMU_SCR_DC_MASK                      0x00000100
#define MMU_SCR_DC_ENABLE                    0x00000100
#define MMU_SCR_DC_ENABLE_NOT                0x00000000
#define MMU_SCR_DC_DISABLE                   MMU_SCR_DC_ENABLE_NOT

/*
 * The TI TMS390S10 supports Sixty-Four (64) entries in the Context Table.
 */

#define TI390_SPARC_CONTEXTS                 64

/*
 * The S-Bus I/O devices use "virtual" addresses which are translated
 * by an I/O MMU.  Hence, an I/O Page Table must be maintained.
 */

#define TI390_IOMMU_CONTROL_ADDR             0x10000000
#define TI390_IOMMU_CONTROL_SIZE             0x00003000

/*
 * I/O MMU Control Register (IOCR)
 */

#define TI390_IOMMU_IOCR_VA                  (TI390_IOMMU_CONTROL_ADDR+0x0000)

#define TI390_IOMMU_IOCR_RANGE_MASK          0x0000001c
#define TI390_IOMMU_IOCR_RANGE_SHIFT         2
#define TI390_IOMMU_IOCR_RANGE_16MB          (0<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_32MB          (1<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_64MB          (2<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_128MB         (3<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_256MB         (4<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_512MB         (5<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_1GB           (6<<TI390_IOMMU_IOCR_RANGE_SHIFT)
#define TI390_IOMMU_IOCR_RANGE_2GB           (7<<TI390_IOMMU_IOCR_RANGE_SHIFT)

#define TI390_IOMMU_IOCR_ME_MASK             0x00000001
#define TI390_IOMMU_IOCR_ME_ENABLE           0x00000001
#define TI390_IOMMU_IOCR_ME_ENABLE_NOT       0x00000000
#define TI390_IOMMU_IOCR_ME_DISABLE          TI390_IOMMU_IOCR_ME_ENABLE_NOT

/*
 * I/O MMU Base Address Register (IBAR)
 */

#define TI390_IOMMU_IBAR_VA                  (TI390_IOMMU_CONTROL_ADDR+0x0004)

#define TI390_IOMMU_IBAR_MASK                0x07fffc00
#define TI390_IOMMU_IBAR_SHIFT               4

/*
 * I/O MMU Flush All TLB Entries (Flash Clear)
 */

#define TI390_IOMMU_FLUSH_VA                 (TI390_IOMMU_CONTROL_ADDR+0x0014)

/*
 * I/O MMU S-Bus Slot Configuration Registers (SSCR[0:3])
 */

#define TI390_IOMMU_SSCR_0_VA                (TI390_IOMMU_CONTROL_ADDR+0x1010)
#define TI390_IOMMU_SSCR_1_VA                (TI390_IOMMU_CONTROL_ADDR+0x1014)
#define TI390_IOMMU_SSCR_2_VA                (TI390_IOMMU_CONTROL_ADDR+0x1018)
#define TI390_IOMMU_SSCR_3_VA                (TI390_IOMMU_CONTROL_ADDR+0x101c)

#define TI390_IOMMU_SSCR_SA30_MASK           0x00010000
#define TI390_IOMMU_SSCR_SA30_ENABLE         0x00010000
#define TI390_IOMMU_SSCR_SA30_ENABLE_NOT     0x00000000
#define TI390_IOMMU_SSCR_SA30_DISABLE        TI390_IOMMU_SSCR_SA30_ENABLE_NOT

#define TI390_IOMMU_SSCR_BA16_MASK           0x00000004
#define TI390_IOMMU_SSCR_BA16_ENABLE         0x00000004
#define TI390_IOMMU_SSCR_BA16_ENABLE_NOT     0x00000000
#define TI390_IOMMU_SSCR_BA16_DISABLE        TI390_IOMMU_SSCR_BA16_ENABLE_NOT

#define TI390_IOMMU_SSCR_BA8_MASK            0x00000002
#define TI390_IOMMU_SSCR_BA8_ENABLE          0x00000002
#define TI390_IOMMU_SSCR_BA8_ENABLE_NOT      0x00000000
#define TI390_IOMMU_SSCR_BA8_DISABLE         TI390_IOMMU_SSCR_BA8_ENABLE_NOT

#define TI390_IOMMU_SSCR_BY_MASK             0x00000001
#define TI390_IOMMU_SSCR_BY_ENABLE           0x00000001
#define TI390_IOMMU_SSCR_BY_ENABLE_NOT       0x00000000
#define TI390_IOMMU_SSCR_BY_DISABLE          TI390_IOMMU_SSCR_BY_ENABLE_NOT

/*
 * I/O MMU Page Table Entry (IOPTE)
 */

#define TI390_IOMMU_PTE_PPN_MASK             0x07ffff00
#define TI390_IOMMU_PTE_PPN_SHIFT            8

#define TI390_IOMMU_PTE_W_MASK               0x00000004
#define TI390_IOMMU_PTE_W_ENABLE             0x00000004
#define TI390_IOMMU_PTE_W_ENABLE_NOT         0x00000000
#define TI390_IOMMU_PTE_W_DISABLE            TI390_IOMMU_PTE_W_ENABLE_NOT
#define TI390_IOMMU_PTE_WRITEABLE            TI390_IOMMU_PTE_W_ENABLE
#define TI390_IOMMU_PTE_READONLY             TI390_IOMMU_PTE_W_DISABLE

#define TI390_IOMMU_PTE_V_MASK               0x00000002
#define TI390_IOMMU_PTE_VALID                0x00000002
#define TI390_IOMMU_PTE_VALID_NOT            0x00000000
#define TI390_IOMMU_PTE_INVALID              TI390_IOMMU_PTE_VALID_NOT

#define TI390_IOMMU_PTE_WAZ_MASK             0x00000001

#ifndef	_ASMLANGUAGE

IMPORT STATUS  cacheTi390LibInit   (CACHE_MODE  instMode,
                                    CACHE_MODE  dataMode);
IMPORT STATUS  cacheTi390Enable    (CACHE_TYPE  cache);
IMPORT STATUS  cacheTi390Disable   (CACHE_TYPE  cache);
IMPORT STATUS  cacheTi390Lock      (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTi390Unlock    (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTi390Clear     (CACHE_TYPE  cache,
                                    void       *address,
                                    size_t      bytes);
IMPORT STATUS  cacheTi390PipeFlush ();
IMPORT STATUS  cacheTi390TextFlush (void       *address,
                                    size_t      bytes);
IMPORT void   *cacheTi390DmaMalloc (size_t       bytes);
IMPORT STATUS  cacheTi390DmaFree   (void        *pBuf);

IMPORT void    ti390FlushInstCache ();
IMPORT void    ti390FlushDataCache ();

IMPORT STATUS  mmuTi390DmaInit     (void        *physBase,
                                    UINT         range);
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCti390h */

