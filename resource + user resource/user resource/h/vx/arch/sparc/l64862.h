/* l64862.h - (MSI) LSI Logic L64862 Mbus to Sbus Interface header. */

/* Copyright 1984-1995 Wind River Systems, Inc. */

/* Copyright 1994-1995 FORCE COMPUTERS */

/*
modification history
--------------------
01c,22mar95,vin  added copyright.	
01b,15mar95,vin  integrated with 5.2.
01a,14jun93,tkj  written
*/

/*
 * NOTE: Before including this file MMU_MSI_VA and MMU_MSI_IOMMU_VA must
 * be defined.
 */

/* _PFN = 24 bit Page frame number = Physical address >> PAGE_SHIFT */
/* This is needed because the full physical address is 36 bits. */

/* _OFS = Offset from preceeding _PFN entry. */
/* This can be used to compute both the virtual and physical addresses. */

#ifndef __INCl64862h
#define __INCl64862h

#ifdef __cplusplus
extern "C" {
#endif

/* Address space of S-bus devices
 */

#define	L64862_SBUS_SLOT0_PFN	0xe00000	/* S-bus slot 0 */
#define	L64862_SBUS_SLOT1_PFN	0xe10000	/* S-bus slot 1 */
#define	L64862_SBUS_SLOT2_PFN	0xe20000	/* S-bus slot 2 */
#define	L64862_SBUS_SLOT3_PFN	0xe30000	/* S-bus slot 3 */
#define	L64862_LANCE_SCSI_PFN	0xef0000	/* Lance & SCSI chips */

/* System control space
 */

#define	L64862_VIC_PFN		0xfd0000	/* VIC VME chip */
#define	L64862_MSI_IOMMU_PFN	0xfe0000	/* I/O MMU Registers */
#define	L64862_MSI_PFN		0xfe0001	/* Mbus to Sbus Interface */
#define	L64862_MSI_MID_PFN	0xfe0002	/* MID register */
#define	L64862_EPROM_PFN	0xff0000	/* EPROM via SEC chip */
#define	L64862_SEC_PFN		0xff1000	/* SEC chip registers */

/*
 * MSI Control Registers other than I/O MMU Registers.
 */

/*
 * Module ID Register (MID)
 */

#define	L64862_MID_OFS		0x0000
#define L64862_MID_VA           (MMU_MSI_MID_VA + L64862_MID_OFS)


/*
 * Arbiter Enable Register (AER)
 */

#define	L64862_AER_OFS		0x0008
#define L64862_AER_VA           (MMU_MSI_VA + L64862_AER_OFS)

#define	L64862_AER_SBW_MASK			0x80000000
#define	L64862_AER_SBW_ENABLE			0x80000000
#define	L64862_AER_SBW_ENABLE_NOT		0
#define	L64862_AER_SBW_DISABLE			L64862_AER_SBW_ENABLE_NOT

#define	L64862_AER_EN4_MASK			0x00100000
#define	L64862_AER_EN4_ENABLE			0x00100000
#define	L64862_AER_EN4_ENABLE_NOT		0
#define	L64862_AER_EN4_DISABLE			L64862_AER_EN4_ENABLE_NOT

#define	L64862_AER_EN3_MASK			0x00080000
#define	L64862_AER_EN3_ENABLE			0x00080000
#define	L64862_AER_EN3_ENABLE_NOT		0
#define	L64862_AER_EN3_DISABLE			L64862_AER_EN3_ENABLE_NOT

#define	L64862_AER_EN2_MASK			0x00040000
#define	L64862_AER_EN2_ENABLE			0x00040000
#define	L64862_AER_EN2_ENABLE_NOT		0
#define	L64862_AER_EN2_DISABLE			L64862_AER_EN2_ENABLE_NOT

#define	L64862_AER_EN1_MASK			0x00020000
#define	L64862_AER_EN1_ENABLE			0x00020000
#define	L64862_AER_EN1_ENABLE_NOT		0
#define	L64862_AER_EN1_DISABLE			L64862_AER_EN1_ENABLE_NOT

#define	L64862_AER_EN0_MASK			0x00010000
#define	L64862_AER_EN0_ENABLE			0x00010000
#define	L64862_AER_EN0_ENABLE_NOT		0
#define	L64862_AER_EN0_DISABLE			L64862_AER_EN4_ENABLE_NOT

#define	L64862_AER_EN_MARB3_MASK		0x00000008
#define	L64862_AER_EN_MARB3_ENABLE		0x00000008
#define	L64862_AER_EN_MARB3_ENABLE_NOT		0
#define	L64862_AER_EN_MARB3_DISABLE		L64862_AER_EN_MARB3_ENABLE_NOT

#define	L64862_AER_EN_MARB2_MASK		0x00000004
#define	L64862_AER_EN_MARB2_ENABLE		0x00000004
#define	L64862_AER_EN_MARB2_ENABLE_NOT		0
#define	L64862_AER_EN_MARB2_DISABLE		L64862_AER_EN_MARB2_ENABLE_NOT

#define	L64862_AER_EN_MARB1_MASK		0x00000002
#define	L64862_AER_EN_MARB1_ENABLE		0x00000002
#define	L64862_AER_EN_MARB1_ENABLE_NOT		0
#define	L64862_AER_EN_MARB1_DISABLE		L64862_AER_EN_MARB1_ENABLE_NOT

/*
 * M-to-S Asynchronous Fault Address Register (AFAR)
 */

#define	L64862_AFAR_OFS		0x0004

/*
 * M-to-S Asynchronous Fault Status Register (AFSR)
 */

#define	L64862_AFSR_OFS		0x0000

#define	L64862_AFSR_ERR_MASK			0x80000000
#define	L64862_AFSR_ERR_SET			0x80000000
#define	L64862_AFSR_ERR_CLEARED			0

#define	L64862_AFSR_LERR_MASK			0x40000000
#define	L64862_AFSR_LERR_SET			0x40000000
#define	L64862_AFSR_LERR_CLEARED		0

#define	L64862_AFSR_TO_MASK			0x20000000
#define	L64862_AFSR_TO_SET			0x20000000
#define	L64862_AFSR_TO_CLEARED			0

#define	L64862_AFSR_BERR_MASK			0x10000000
#define	L64862_AFSR_BERR_SET			0x10000000
#define	L64862_AFSR_BERR_CLEARED		0

#define	L64862_AFSR_SIZ_MASK			0x0e000000
#define	L64862_AFSR_SIZ_SHIFT			25

#define	L64862_AFSR_S_MASK			0x01000000
#define	L64862_AFSR_S_SET			0x01000000
#define	L64862_AFSR_S_SUPERVISOR		L64862_AFSR_S_SET
#define	L64862_AFSR_S_CLEARED			0
#define	L64862_AFSR_S_USER			L64862_AFSR_S_CLEARED

#define	L64862_AFSR_MID_MASK			0x00f00000
#define	L64862_AFSR_MID_SHIFT			20

#define	L64862_AFSR_MERR_MASK			0x00080000
#define	L64862_AFSR_MERR_SET			0x00080000
#define	L64862_AFSR_MERR_CLEARED		0

#define	L64862_AFSR_MRD_MASK			0x00040000
#define	L64862_AFSR_MRD_SET			0x00040000
#define	L64862_AFSR_MRD_READ			L64862_AFSR_MRD_SET
#define	L64862_AFSR_MRD_CLEARED			0
#define	L64862_AFSR_MRD_WRITE			L64862_AFSR_MRD_CLEARED

#define	L64862_AFSR_SA_MASK			0x0001f000
#define	L64862_AFSR_SA_SHIFT			12

#define	L64862_AFSR_SSIZE_MASK			0x00000e00
#define	L64862_AFSR_SSIZE_SHIFT			9

#define	L64862_AFSR_PA_MASK			0x0000000f
#define	L64862_AFSR_PA_SHIFT			0

/*
 * I/O MMU S-Bus Slot Configuration Registers (SSCR[0:3])
 */

#define L64862_SSCR_0_OFS			0x0010
#define L64862_SSCR_1_OFS			0x0014
#define L64862_SSCR_2_OFS			0x0018
#define L64862_SSCR_3_OFS			0x001c

#define L64862_SSCR_SEGA_MASK           	0x003f0000
#define L64862_SSCR_SEGA_SHIFT			16

#define L64862_SSCR_CP_MASK             	0x00008000
#define L64862_SSCR_CP_ENABLE           	0x00008000
#define L64862_SSCR_CP_ENABLE_NOT       	0
#define L64862_SSCR_CP_DISABLE          	L64862_SSCR_CP_ENABLE_NOT

#define L64862_SSCR_BA64_MASK           	0x00000010
#define L64862_SSCR_BA64_ENABLE         	0x00000010
#define L64862_SSCR_BA64_ENABLE_NOT     	0
#define L64862_SSCR_BA64_DISABLE        	L64862_SSCR_BA64_ENABLE_NOT

#define L64862_SSCR_BA32_MASK           	0x00000008
#define L64862_SSCR_BA32_ENABLE         	0x00000008
#define L64862_SSCR_BA32_ENABLE_NOT     	0
#define L64862_SSCR_BA32_DISABLE        	L64862_SSCR_BA32_ENABLE_NOT

#define L64862_SSCR_BA16_MASK           	0x00000004
#define L64862_SSCR_BA16_ENABLE         	0x00000004
#define L64862_SSCR_BA16_ENABLE_NOT     	0
#define L64862_SSCR_BA16_DISABLE        	L64862_SSCR_BA16_ENABLE_NOT

#define L64862_SSCR_BA8_MASK            	0x00000002
#define L64862_SSCR_BA8_ENABLE          	0x00000002
#define L64862_SSCR_BA8_ENABLE_NOT      	0
#define L64862_SSCR_BA8_DISABLE         	L64862_SSCR_BA8_ENABLE_NOT

#define L64862_SSCR_BY_MASK             	0x00000001
#define L64862_SSCR_BY_ENABLE           	0x00000001
#define L64862_SSCR_BY_ENABLE_NOT       	0
#define L64862_SSCR_BY_DISABLE          	L64862_SSCR_BY_ENABLE_NOT

/*
 * The S-Bus I/O devices use "virtual" addresses which are translated
 * by an I/O MMU.  Hence, an I/O Page Table must be maintained.
 */

#define L64862_IOMMU_CONTROL_ADDR             MMU_MSI_IOMMU_VA
#define L64862_IOMMU_CONTROL_SIZE             0x00003000

/*
 * I/O MMU Control Register (IOCR)
 */

#define	L64862_IOMMU_IOCR_OFS		      0x0000
#define L64862_IOMMU_IOCR_VA                  (L64862_IOMMU_CONTROL_ADDR+L64862_IOMMU_IOCR_OFS)

#define	L64862_IOMMU_IOCR_IMPL_MASK	      0xf0000000
#define	L64862_IOMMU_IOCR_IMPL_SHIFT	      28

#define	L64862_IOMMU_IOCR_VER_MASK	      0x0f000000
#define	L64862_IOMMU_IOCR_VER_SHIFT	      24

#define L64862_IOMMU_IOCR_RANGE_MASK          0x0000001c
#define L64862_IOMMU_IOCR_RANGE_SHIFT         2
#define L64862_IOMMU_IOCR_RANGE_16MB          (0<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_32MB          (1<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_64MB          (2<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_128MB         (3<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_256MB         (4<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_512MB         (5<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_1GB           (6<<L64862_IOMMU_IOCR_RANGE_SHIFT)
#define L64862_IOMMU_IOCR_RANGE_2GB           (7<<L64862_IOMMU_IOCR_RANGE_SHIFT)

#define L64862_IOMMU_IOCR_DE_MASK             0x00000002
#define L64862_IOMMU_IOCR_DE_ENABLE           0x00000002
#define L64862_IOMMU_IOCR_DE_ENABLE_NOT       0
#define L64862_IOMMU_IOCR_DE_DISABLE          L64862_IOMMU_IOCR_DE_ENABLE_NOT

#define L64862_IOMMU_IOCR_ME_MASK             0x00000001
#define L64862_IOMMU_IOCR_ME_ENABLE           0x00000001
#define L64862_IOMMU_IOCR_ME_ENABLE_NOT       0
#define L64862_IOMMU_IOCR_ME_DISABLE          L64862_IOMMU_IOCR_ME_ENABLE_NOT

/*
 * I/O MMU Base Address Register (IBAR)
 */

#define	L64862_IOMMU_IBAR_OFS		      0x0004
#define L64862_IOMMU_IBAR_VA                  (L64862_IOMMU_CONTROL_ADDR+L64862_IOMMU_IBAR_OFS)

#define L64862_IOMMU_IBAR_MASK                0xfffffc00
#define L64862_IOMMU_IBAR_SHIFT               4	/* ? */

/*
 * I/O MMU Flush All TLB Entries (Flash Invalidate)
 */

#define	L64862_IOMMU_FLUSH_ALL_OFS	      0x0014
#define L64862_IOMMU_FLUSH_ALL_VA	      (L64862_IOMMU_CONTROL_ADDR+L64862_IOMMU_FLUSH_ALL_OFS)

/*
 * I/O MMU Flush Address Match TLB Entries (Flash Invalidate)
 */

#define	L64862_IOMMU_FLUSH_MATCH_OFS	      0x0018
#define L64862_IOMMU_FLUSH_MATCH_VA	      (L64862_IOMMU_CONTROL_ADDR+L64862_IOMMU_FLUSH_MATCH_OFS)

/*
 * I/O MMU Page Table Entry (IOPTE)
 */

#define L64862_IOMMU_PTE_PPN_MASK             0xffffff00
#define L64862_IOMMU_PTE_PPN_SHIFT            8
/* Bits to shift a PPN to make it align with a physical address. */
#define	L64862_IOMMU_PPN_PHY_SHIFT (PAGE_SHIFT - L64862_IOMMU_PTE_PPN_SHIFT)

#define L64862_IOMMU_PTE_C_MASK               0x00000080
#define L64862_IOMMU_PTE_C_ENABLE             0x00000080
#define L64862_IOMMU_PTE_C_ENABLE_NOT         0
#define L64862_IOMMU_PTE_C_DISABLE            L64862_IOMMU_PTE_C_ENABLE_NOT
#define L64862_IOMMU_PTE_CACHEABLE            L64862_IOMMU_PTE_C_ENABLE
#define L64862_IOMMU_PTE_CACHEABLE_NOT        L64862_IOMMU_PTE_C_DISABLE

#define L64862_IOMMU_PTE_W_MASK               0x00000004
#define L64862_IOMMU_PTE_W_ENABLE             0x00000004
#define L64862_IOMMU_PTE_W_ENABLE_NOT         0
#define L64862_IOMMU_PTE_W_DISABLE            L64862_IOMMU_PTE_W_ENABLE_NOT
#define L64862_IOMMU_PTE_WRITEABLE            L64862_IOMMU_PTE_W_ENABLE
#define L64862_IOMMU_PTE_READONLY             L64862_IOMMU_PTE_W_DISABLE

#define L64862_IOMMU_PTE_V_MASK               0x00000002
#define L64862_IOMMU_PTE_VALID                0x00000002
#define L64862_IOMMU_PTE_VALID_NOT            0
#define L64862_IOMMU_PTE_INVALID              L64862_IOMMU_PTE_VALID_NOT

#ifndef	_ASMLANGUAGE

#if defined(__STDC__) || defined(__cplusplus)

IMPORT STATUS  mmuL64862DmaInit     (void        *vrtBase,
				     void	 *vrtTop,
				     UINT         range);

#else   /* __STDC__ */

IMPORT STATUS  mmuL64862DmaInit     ();
#endif  /* __STDC__ */
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCl64862h */
