/* l64860.h - (EMC) LSI Logic l64860 Memory Controller header for SPARC. */

/* Copyright 1984-1995 Wind River Systems, Inc. */

/* Copyright 1994-1995 FORCE COMPUTERS */

/*
modification history
--------------------
01c,22mar95,vin  added copyright.	
01b,15mar95,vin  integrated with 5.2.
01a,14jun93,tkj  written.
*/

/* _PFN = 24 bit Page frame number = Physical address >> PAGE_SHIFT */
/* This is needed because the full physical address is 36 bits. */

/* _OFS = Offset from preceeding _PFN entry. */
/* This can be used to compute both the virtual and physical addresses. */

#ifndef __INCl64860h
#define __INCl64860h

#ifdef __cplusplus
extern "C" {
#endif

/* ---------------- Memory Physical Address Definitions -------------------- */
#define	EMC0_DRAM_STRT_PFN	0x000000	/* EMC0: DRAM Start */
#define	EMC0_DRAM_END_PFN	0x01ffff	/* EMC0: DRAM End */
#define	EMC1_DRAM_STRT_PFN	0x100000	/* EMC1: DRAM Start */
#define	EMC1_DRAM_END_PFN	0x1fffff	/* EMC1: DRAM End */

#define	EMC0_VIO_STRT_PFN	0x080000	/* EMC0: Video I/O Start */
#define	EMC0_VIO_END_PFN	0x09ffff	/* EMC0: Video I/O End */
#define	EMC1_VIO_STRT_PFN	0x180000	/* EMC1: Video I/O Start */
#define	EMC1_VIO_END_PFN	0x19ffff	/* EMC1: Video I/O End */

#define	EMC0_VREG_PFN		0x090000	/* EMCO: Video Registers */
#define	EMC1_VREG_PFN		0x190000	/* EMC1: Video Registers */
#define	EMC_MDI_CSR_OFS		0x00ff		/* MDI CSRs */
#define	EMC_PCG_OFS		0x01f0		/* PCG */
#define	EMC_VBC_OFS		0x02ff		/* VBC */
#define	EMC_MDI_C_OFS		0x1fff		/* MDI Cursor */
#define	EMC_DAC_OFS		0x2300		/* DAC */
#define	EMC_MDI_LUT_OFS		0x5fff		/* MDI LUTs */

#define	EMC0_VRAM_STRT_PFN	0x0e0000	/* EMC0: VRAM Start */
#define	EMC0_VRAM_END_PFN	0x0fffff	/* EMC0: VRAM End */
#define	EMC1_VRAM_STRT_PFN	0x1e0000	/* EMC1: VRAM Start */
#define	EMC1_VRAM_END_PFN	0x1fffff	/* EMC1: VRAM End */

#define	EMC0_MREG_PFN	0xf00000	/* EMC0: All memory registers */
#define	EMC1_MREG_PFN	0xf10000	/* EMC1: All memory registers */

/* ------------------------- Register Definitions -------------------------- */

/*
 * Memory Enable Register (ME)
 */

#define	EMC_ME_OFS	0x000		/* Memory Enable Register */

/* ME.IMPL = Implementation Number */
#define	EMC_ME_IMPL_MASK	0xf0000000
#define	EMC_ME_IMPL_SHIFT	28
#define	EMC_ME_IMPL_EMC		1 /* EMC Error Correcting Memory Controller */
#define	EMC_ME_IMPL_SMC		2 /* SMC includes Graphics */

/* ME.VER = Version Number */
#define	EMC_ME_VER_MASK		0x0f000000
#define	EMC_ME_VER_SHIFT	24

/* ME.CI = Coherent Invalidate Enable */
#define	EMC_ME_CI_MASK		0x00000800
#define	EMC_ME_CI_ENABLE	0
#define	EMC_ME_CI_EMABLE_NOT	0x00000800
#define	EMC_ME_CI_DISABLE	EMC_ME_CI_ENABLE_NOT

/* ME.ACONFIG = Memory Controller Address Map Select */
#define	EMC_ME_ACONFIG_MASK	0x00000400
#define	EMC_ME_ACONFIG_EMC0	0
#define	EMC_ME_ACONFIG_EMC1	0x00000400
#define	EMC_ME_ACONFIG_SHIFT	10

/* ME.MRR = Memory Row Refresh Enable */
#define	EMC_ME_MRR_MASK		0x000003fc
#define	EMC_ME_MRR_SHIFT	2

#define	EMC_ME_MRR7_MASK	0x00000200
#define	EMC_ME_MRR7_ENABLE	0x00000200
#define	EMC_ME_MRR7_ENABLE_NOT	0
#define	EMC_ME_MRR7_DISABLE	EMC_ME_MRR7_ENABLE_NOT

#define	EMC_ME_MRR6_MASK	0x00000100
#define	EMC_ME_MRR6_ENABLE	0x00000100
#define	EMC_ME_MRR6_ENABLE_NOT	0
#define	EMC_ME_MRR6_DISABLE	EMC_ME_MRR6_ENABLE_NOT

#define	EMC_ME_MRR5_MASK	0x00000080
#define	EMC_ME_MRR5_ENABLE	0x00000080
#define	EMC_ME_MRR5_ENABLE_NOT	0
#define	EMC_ME_MRR5_DISABLE	EMC_ME_MRR5_ENABLE_NOT

#define	EMC_ME_MRR4_MASK	0x00000040
#define	EMC_ME_MRR4_ENABLE	0x00000040
#define	EMC_ME_MRR4_ENABLE_NOT	0
#define	EMC_ME_MRR4_DISABLE	EMC_ME_MRR4_ENABLE_NOT

#define	EMC_ME_MRR3_MASK	0x00000020
#define	EMC_ME_MRR3_ENABLE	0x00000020
#define	EMC_ME_MRR3_ENABLE_NOT	0
#define	EMC_ME_MRR3_DISABLE	EMC_ME_MRR3_ENABLE_NOT

#define	EMC_ME_MRR2_MASK	0x00000010
#define	EMC_ME_MRR2_ENABLE	0x00000010
#define	EMC_ME_MRR2_ENABLE_NOT	0
#define	EMC_ME_MRR2_DISABLE	EMC_ME_MRR2_ENABLE_NOT

#define	EMC_ME_MRR1_MASK	0x00000008
#define	EMC_ME_MRR1_ENABLE	0x00000008
#define	EMC_ME_MRR1_ENABLE_NOT	0
#define	EMC_ME_MRR1_DISABLE	EMC_ME_MRR1_ENABLE_NOT

#define	EMC_ME_MRR0_MASK	0x00000004
#define	EMC_ME_MRR0_ENABLE	0x00000004
#define	EMC_ME_MRR0_ENABLE_NOT	0
#define	EMC_ME_MRR0_DISABLE	EMC_ME_MRR0_ENABLE_NOT

/* ME.EI = Enable Correctable Error Interrupt */
#define	EMC_ME_EI_MASK		0x00000002
#define	EMC_ME_EI_ENABLE	0x00000002
#define	EMC_ME_EI_ENABLE_NOT	0
#define	EMC_ME_EI_DISABLE	EMC_ME_EI_ENABLE_NOT

/* ME.EE = Enable ECC Detection and Correction */
#define	EMC_ME_EE_MASK		0x00000001
#define	EMC_ME_EE_ENABLE	0x00000001
#define	EMC_ME_EE_ENABLE_NOT	0
#define	EMC_ME_EE_DISABLE	EMC_ME_EE_ENABLE_NOT

/*
 * Memory Delay Register (MD)
 */

#define	EMC_MD_OFS	0x004		/* Memory Delay Register */

/* MD.RSC = Refresh Interval Counter */
#define	EMC_MD_RSC_MASK		0x80000000
#define	EMC_MD_RSC_ENABLE	0x80000000
#define	EMC_MD_RSC_ENABLE_NOT	0
#define	EMC_MD_RSC_DISABLE	EMC_MD_RSC_ENABLE_NOT

/* MD.GAD = Graphics Arbitration Delay */
#define	EMC_MD_GAD_MASK		0x7c000000
#define	EMC_MD_GAD_SHIFT	26

/* MD.MDH = MBus Master Arbitration Delay MID [8:f] */
#define	EMC_MD_MDH_MASK		0x03e00000
#define	EMC_MD_MDH_SHIFT	21

/* MD.MDL = MBus Master Arbitration Delay MID[0:7] */
#define	EMC_MD_MDL_MASK		0x001f0000
#define	EMC_MD_MDL_SHIFT	16

/* MD.CIDEL = Coherent Invalidate Delay */
#define	EMC_MD_CIDEL_MASK	0x0000e000
#define	EMC_MD_CIDEL_SHIFT	13

/* MD.MIHDEL = Memory Delay */
#define	EMC_MD_MIHDEL_MASK	0x00001c00
#define	EMC_MD_MIHDEL_SHIFT	10

/* MD.RRI = Request Refresh Interval */
#define	EMC_MD_RRI_MASK		0x000003ff
#define	EMC_MD_RRI_SHIFT	0

/*
 * Fault Status Register (FS)
 */

#define	EMC_FS_OFS	0x008		/* Fault Status */

/* FS.GE = Graphics Error */
#define	EMC_FS_GE_MASK		0x00020000
#define	EMC_FS_GE_SET		0x00020000
#define	EMC_FS_GE_CLEARED	0

/* FS.ME = Multiple Error */
#define	EMC_FS_ME_MASK		0x00010000
#define	EMC_FS_ME_SET		0x00010000
#define	EMC_FS_ME_CLEARED	0

/* FS.SYNCB = Syndrome Code or Stored Check Bits */
#define	EMC_FS_SYNCB_MASK	0x0000ff00
#define	EMC_FS_SYNCB_SHIFT	8

/* FS.DW = Pointer to Double Word in Error */
#define	EMC_FS_DW_MASK		0x000000f0
#define	EMC_FS_DW_SHIFT		4

/* FS.UE = Uncorrectable Error */
#define	EMC_FS_UE_MASK		0x00000008
#define	EMC_FS_UE_SET		0x00000008
#define	EMC_FS_UE_CLEARED	0

/* FS.SE = Slot Error */
#define	EMC_FS_SE_MASK		0x00000002
#define	EMC_FS_SE_SET		0x00000002
#define	EMC_FS_SE_CLEARED	0

/* FS.CE = Correctable ECC Error */
#define	EMC_FS_CE_MASK		0x00000001
#define	EMC_FS_CE_SET		0x00000001
#define	EMC_FS_CE_CLEARED	0

/*
 * Video Configuration (VC)
 */

#define	EMC_VC_OFS	0x00c		/* Video Configuration */

/* VC.VCONFIGx = Video Configuration Bits */
#define	EMC_VC_VCONFIG_DRAM	0
#define	EMC_VC_VCONFIG_2MB	1
#define	EMC_VC_VCONFIG_4MB	2
#define	EMC_VC_VCONFIG_8MB	3

#define	EMC_VC_VCONFIG7_MASK	0x0000c000
#define	EMC_VC_VCONFIG7_SHIFT	14
#define	EMC_VC_VCONFIG7_DRAM	(EMC_VC_VCONFIG_DRAM << EMC_VC_VCONFIG7_SHIFT)
#define	EMC_VC_VCONFIG7_2MB	(EMC_VC_VCONFIG_2MB  << EMC_VC_VCONFIG7_SHIFT)
#define	EMC_VC_VCONFIG7_4MB	(EMC_VC_VCONFIG_4MB  << EMC_VC_VCONFIG7_SHIFT)
#define	EMC_VC_VCONFIG7_8MB	(EMC_VC_VCONFIG_8MB  << EMC_VC_VCONFIG7_SHIFT)

#define	EMC_VC_VCONFIG6_MASK	0x00003000
#define	EMC_VC_VCONFIG6_SHIFT	12
#define	EMC_VC_VCONFIG6_DRAM	(EMC_VC_VCONFIG_DRAM << EMC_VC_VCONFIG6_SHIFT)
#define	EMC_VC_VCONFIG6_2MB	(EMC_VC_VCONFIG_2MB  << EMC_VC_VCONFIG6_SHIFT)
#define	EMC_VC_VCONFIG6_4MB	(EMC_VC_VCONFIG_4MB  << EMC_VC_VCONFIG6_SHIFT)
#define	EMC_VC_VCONFIG6_8MB	(EMC_VC_VCONFIG_8MB  << EMC_VC_VCONFIG6_SHIFT)

#define	EMC_VC_VCONFIG5_MASK	0x00000c00
#define	EMC_VC_VCONFIG5_SHIFT	10
#define	EMC_VC_VCONFIG5_DRAM	(EMC_VC_VCONFIG_DRAM << EMC_VC_VCONFIG5_SHIFT)
#define	EMC_VC_VCONFIG5_2MB	(EMC_VC_VCONFIG_2MB  << EMC_VC_VCONFIG5_SHIFT)
#define	EMC_VC_VCONFIG5_4MB	(EMC_VC_VCONFIG_4MB  << EMC_VC_VCONFIG5_SHIFT)
#define	EMC_VC_VCONFIG5_8MB	(EMC_VC_VCONFIG_8MB  << EMC_VC_VCONFIG5_SHIFT)

#define	EMC_VC_VCONFIG4_MASK	0x00000300
#define	EMC_VC_VCONFIG4_SHIFT	8
#define	EMC_VC_VCONFIG4_DRAM	(EMC_VC_VCONFIG_DRAM << EMC_VC_VCONFIG4_SHIFT)
#define	EMC_VC_VCONFIG4_2MB	(EMC_VC_VCONFIG_2MB  << EMC_VC_VCONFIG4_SHIFT)
#define	EMC_VC_VCONFIG4_4MB	(EMC_VC_VCONFIG_4MB  << EMC_VC_VCONFIG4_SHIFT)
#define	EMC_VC_VCONFIG4_8MB	(EMC_VC_VCONFIG_8MB  << EMC_VC_VCONFIG4_SHIFT)

/*
 * Fault Address 0
 */

#define	EMC_FA0_OFS	0x010		/* Fault Address 0 */

/* FA0.MID = Module Identifier */
#define	EMC_FA0_MID_MASK	0xf0000000
#define	EMC_FA0_MID_SHIFT	28

/* FA0.SUP = Suppervisor Access Indicator */
#define	EMC_FA0_SUP_MASK	0x08000000
#define	EMC_FA0_SUP_SUPERVISOR	0x08000000
#define	EMC_FA0_SUP_USER	0

/* FA0.VA = Virtual Address (NA for Viking processor) */
#define	EMC_FA0_VA_MASK		0x003fc000
#define	EMC_FA0_VA_SHIFT	14

/* FA0.L = Bus Lock Indicator */
#define	EMC_FA0_L_MASK		0x00001000
#define	EMC_FA0_L_LOCKED	0x00001000
#define	EMC_FA0_L_UNLOCKED	0

/* FA0.C = Data Cacheable */
#define	EMC_FA0_C_MASK		0x00000800
#define	EMC_FA0_C_CACHEABLE	0x00000800
#define	EMC_FA0_C_CACHEABLE_NOT	0

#define	EMC_FA0_SIZE_MASK	0x00000700
#define	EMC_FA0_SIZE_SHIFT	8
#define	EMC_FA0_SIZE_1		0
#define	EMC_FA0_SIZE_2		0x00000100
#define	EMC_FA0_SIZE_4		0x00000200
#define	EMC_FA0_SIZE_8		0x00000300
#define	EMC_FA0_SIZE_16		0x00000400
#define	EMC_FA0_SIZE_32		0x00000500
#define	EMC_FA0_SIZE_64		0x00000600
#define	EMC_FA0_SIZE_128	0x00000700

/* FA0.TYPE = Transaction Type */
#define	EMC_FA0_TYPE_MASK	0x000000f0
#define	EMC_FA0_TYPE_SHIFT	4
#define	EMC_FA0_TYPE_WR		0
#define	EMC_FA0_TYPE_RD		0x00000010
#define	EMC_FA0_TYPE_CI		0x00000020
#define	EMC_FA0_TYPE_CR		0x00000030
#define	EMC_FA0_TYPE_CWI	0x00000040
#define	EMC_FA0_TYPE_CRI	0x00000050

/* FA0.PA = Higher 4 bits of the faulty Physical Address */
#define	EMC_FA0_PA_MASK		0x0000000f
#define	EMC_FA0_PA_SHIFT	0

/*
 * Fault Address 1
 */

/* Lower 32 bits of the faulty Physical Address */
#define	EMC_FA1_OFS	0x014		/* Fault Address 1 */

/*
 * ECC Diagnostics Register (EC)
 */

#define	EMC_EC_OFS	0x018		/* ECC Diagnostics */

/* EC.DMODE = ECC Diagnostic Mode */
#define	EMC_EC_DMODE_MASK	0x00000400
#define	EMC_EC_DMODE_ENABLE	0x00000400
#define	EMC_EC_DMODE_ENABLE_NOT	0
#define	EMC_EC_DMODE_DISABLE	EMC_EC_DMODE_ENABLE_NOT
#define	EMC_EC_DMODE_DIAGNOSTIC	EMC_EC_DMODE_ENABLE
#define	EMC_EC_DMODE_NORMAL	EMC_EC_DMODE_ENABLE_NOT

/* EC.CB - Forced Check Bit Data */
#define	EMC_EC_CB_MASK		0x000000ff
#define	EMC_EC_CB_SHIFT		0

#ifdef __cplusplus
}
#endif

#endif /* __INCl64860h */

