/* esfSparc.h - SPARC Version of Exception Stack Frame */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
02h,22sep92,rrr  added support for c++
02g,29jul92,jwt  moved INIT_STACK_FRAME from src/arch/sparc/taskArchLib.c.
02f,10jul92,jwt  removed padding from REG_SET and hence ESF.
02e,08jul92,jwt  changed esfSPARC.h to esfSparc.h.
02d,26may92,rrr  the tree shuffle
02c,20dec91,jwt  parenthesized "n" in register macros to force ordering.
02b,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed ASMLANGUAGE to _ASMLANGUAGE
		  -changed copyright notice
02a,16nov90,jwt  written - derived from 4.0.2 SPARC "wind.h" and "excLib.h".
*/

#ifndef __INCesfSparch
#define __INCesfSparch

#ifdef __cplusplus
extern "C" {
#endif

#include "regs.h"

#ifndef _ASMLANGUAGE

typedef struct                          /* Exception Stack Frame */
    {
    UINT    sf_locals[8];               /* 0x00: LOCALs */
    UINT    sf_ins[8];                  /* 0x20: INs */
    UINT    hidden;                     /* 0x40: Hidden Word */
    UINT    sf_parameters[6];           /* 0x44: Parameters */
    UINT    sf_spare[1];                /* 0x5C: Stack Frame Spare */
    REG_SET esf;                        /* 0x60: Register Set */
    } ESF;

#endif	/* _ASMLANGUAGE */

/* Overflow/Underflow Stack Frame */

#define STACK_FRAME      0x00                  /* Stack Frame */
#define SF_LOCALS        (STACK_FRAME + 0x00)  /* Local Registers */
#define SF_INS           (STACK_FRAME + 0x20)  /* In Registers */
#define SF_SPARE         (STACK_FRAME + 0x5C)  /* Spare Slot */
#define STACK_FRAME_SIZE 0x60

#define SF_LOCAL(n)      (SF_LOCALS + (4 * (n)))
#define SF_IN(n)         (SF_INS + (4 * (n)))

/* Macro for determining and aligning the initial stack frame */

#define	INIT_STACK_FRAME	STACK_ROUND_UP ((((MAX_TASK_ARGS - 6) \
				* sizeof (int)) + (STACK_FRAME_SIZE - 4)))

/* Exception Stack Frame */

#define ESF_PC		(STACK_FRAME_SIZE + REG_SET_PC)
#define ESF_NPC		(STACK_FRAME_SIZE + REG_SET_NPC)
#define ESF_PSR		(STACK_FRAME_SIZE + REG_SET_PSR)
#define ESF_WIM		(STACK_FRAME_SIZE + REG_SET_WIM)
#define ESF_TBR		(STACK_FRAME_SIZE + REG_SET_TBR)
#define ESF_Y		(STACK_FRAME_SIZE + REG_SET_Y)
#define ESF_pCP		(STACK_FRAME_SIZE + REG_SET_pCP)
#define ESF_pASR	(STACK_FRAME_SIZE + REG_SET_pASR)

#define ESF_GLOBALS	(STACK_FRAME_SIZE + REG_SET_GLOBALS)
#define ESF_OUTS	(STACK_FRAME_SIZE + REG_SET_OUTS)

#define ESF_SIZE	0xC0		/* Exception Stack Frame Size */
#define ESF_LOCALS	(STACK_FRAME_SIZE + REG_SET_LOCALS)
#define ESF_INS		(STACK_FRAME_SIZE + REG_SET_INS)

/* _trapEnter() - Controls, GLOBALs, OUTs */

#define ESF_GLOBAL(n)    (ESF_GLOBALS + (4 * (n)))
#define ESF_OUT(n)       (ESF_OUTS + (4 * (n)))

/* Exception handler overflow - defer LOCALs, INs */

#define ESF_LOCAL(n)     (ESF_LOCALS + (4 * (n)))
#define ESF_IN(n)        (ESF_INS + (4 * (n)))

#ifdef __cplusplus
}
#endif

#endif /* __INCesfSparch */
