/* sparcASI.h - Address Space Identifiers (ASIs) header for SPARC. */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,27feb95,vin	 added super sparc specific ASIs.
01b,29nov93,ccc  added three previously undefined ASIs (0x36, 0x37, 0x39).
01a,04mar93,edm  written.
    26apr93,ccc  SPECIAL for sunec.
*/

#ifndef __INCsparcASIh
#define __INCsparcASIh

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Address Space Identifiers (ASIs) for SPARC
 */

#define ASI_MBUS_EXT_ADDR_SPACE		0x00
#define ASI_CNTRL_SPACE			0x02
#define ASI_MCC				0x02
#define ASI_MMU_FLUSH_PROBE		0x03
#define ASI_MMU_REGS			0x04
#define ASI_MMU_DIAG_INST_ONLY_TLB	0x05
#define ASI_MMU_DIAG_INST_DATA_TLB	0x06
#define ASI_MMU_DIAG_I_O_TLB		0x07
#define ASI_USER_CODE			0x08
#define ASI_SUPER_CODE			0x09
#define ASI_USER_DATA			0x0A
#define ASI_SUPER_DATA			0x0B
#define ASI_I_CACHE_TAG			0x0C
#define ASI_I_CACHE_DATA		0x0D
#define ASI_CACHE_TAG			0x0E
#define ASI_CACHE_DATA			0x0F
#define ASI_D_CACHE_TAG			0x0E
#define ASI_D_CACHE_DATA		0x0F
#define ASI_FLUSH_CACHE_LINE_PAGE	0x10
#define ASI_FLUSH_CACHE_LINE_SEGMENT	0x11
#define ASI_FLUSH_CACHE_LINE_REGION	0x12
#define ASI_FLUSH_CACHE_LINE_CONTEXT	0x13
#define ASI_FLUSH_CACHE_LINE_USER	0x14
#define ASI_BLOCK_COPY			0x17
#define ASI_FLUSH_D_CACHE_LINE_PAGE	0x18
#define ASI_FLUSH_D_CACHE_LINE_SEGMENT	0x19
#define ASI_FLUSH_D_CACHE_LINE_REGION	0x1A
#define ASI_FLUSH_D_CACHE_LINE_CONTEXT	0x1B
#define ASI_FLUSH_D_CACHE_LINE_USER	0x1C
#define ASI_BLOCK_ZERO			0x1F
#define ASI_MMU_PASS_THRU_BASE		0x20
#define ASI_MPTAG			0x30
#define ASI_SB_TAG			0x30
#define ASI_SB_DATA			0x31
#define ASI_SB_CNTL			0x32
#define ASI_FLUSH_I_CACHE               0x36
#define ASI_FLUSH_CACHE                 0x37
#define ASI_FLUSH_D_CACHE		0x37
#define ASI_MMU_BRK_PT_DIAG		0x38		
#define ASI_MMU_DIAG                    0x39
#define ASI_BIST_DIAG			0x39
#define ASI_EMUL_TEMPS_1		0x40
#define ASI_EMUL_TEMPS_2		0x41
#define ASI_EMUL_DATA_IN		0x44
#define ASI_EMUL_DATA_OUT		0x46
#define ASI_EMUL_EXIT_PC		0x47
#define ASI_EMUL_EXIT_nPC		0x48
#define ASI_EMUL_CNT_VAL		0x49
#define ASI_EMUL_CNT_CNTL		0x4A
#define ASI_EMUL_CNT_STAT		0x4B
#define ASI_ACTION_REG			0x4c



#ifdef __cplusplus
}
#endif

#endif /* __INCsparcASIh */
