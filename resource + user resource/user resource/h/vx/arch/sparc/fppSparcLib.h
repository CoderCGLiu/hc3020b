/* fppSparcLib.h - SPARC Version of Floating-Point Include File */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
02j,22sep92,rrr  added support for c++
02i,06jul92,jwt  converted fppSPARCLib to fppSparcLib.
02h,04jul92,jcf  added variable declarations; changed fp*RegIndex to fp*RegName
02g,26may92,rrr  the tree shuffle
02f,23feb92,yao  added FPREG_SET.  removed ANSI function definition for
		 fppInit(), fppTaskRegs{S,G}et(), fppSave(), fppRestore(),
		 fppProbe().  added ANSI definition for fppNan (). moved
		 fppFsrDefault, fppQueueSize to sparc/fppLib.c. renamed
		 FP_NUM_REGS as FP_NUM_DREGS and changed to 16.  removed
		 duplicated definitions for fppState constants.
02e,10jan92,jwt  added ANSI function prototypes.
02d,19dec91,jwt  cleaned up ANSI compiler warnings/errors.
02c,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed VOID to void
		  -changed ASMLANGUAGE to _ASMLANGUAGE
		  -changed copyright notice
02b,06jun91,jwt  added FP queue dynamic sizing table.
02a,16nov90,jwt  written - derived from 4.0.2 SPARC "fppSparc.h".
*/

#ifndef __INCfppSparcLibh
#define __INCfppSparcLibh

#ifdef __cplusplus
extern "C" {
#endif

//#include "reg.h"
#include "vxWorks.h"

#define  FP_NUM_DREGS    16              /* Floating-Point Data Registers */

/* equates for fppState, fpstate */

#define UNKNOWN          0x00           /* default */
#define FPU_TESTED       0x01           /* set by fppProbe */
#define FPU_TESTED_TRUE  0x02           /* set by fppProbe */
#define FPU_TESTED_FALSE 0x00           /* set by fppProbe */
#define FPU_AVAILABLE    (FPU_TESTED + FPU_TESTED_TRUE)
#define NO_FPU_AVAILABLE (FPU_TESTED + FPU_TESTED_FALSE)
#define PROBING          0x10
#define SWITCHING        0x20
#define INQUIRING        0x40

#ifndef _ASMLANGUAGE

typedef struct fpContext                /* Floating-Point Context */
    {
#if 0
    UINT  fp_align[0];                  /* Double-Word Alignment */
#endif
    UINT  fpd[FP_NUM_DREGS*2];             /* Data Registers */
    UINT  *pFpQ;                        /* Queue Entries */
    UINT  fsr;                          /* Status Register */
    } FP_CONTEXT;

#define FPREG_SET	FP_CONTEXT


/* variable declarations */

extern REG_INDEX fpRegName[];		/* f-point data register table */
extern REG_INDEX fpCtlRegName[];	/* f-point control register table */
extern WIND_TCB *pFppTaskIdPrevious;	/* task id for deferred exceptions */
extern FUNCPTR	 fppCreateHookRtn;	/* arch dependent create hook routine */
extern FUNCPTR	 fppDisplayHookRtn;	/* arch dependent display routine */


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void	fppArchInit (void);
extern void	fppArchTaskCreateInit (FP_CONTEXT *pFpContext);
extern void	fsrShow (UINT fsrValue);
extern void     fppFlushInit (void);
extern UINT     fppQueueInit (void);
extern STATUS   fppProbeSup (void);
extern void     fppNan (FP_CONTEXT *pFpContext, ULONG fppFsr);

#else

extern void	fppArchInit ();
extern void	fppArchTaskCreateInit ();
extern void	fsrShow ();
extern void     fppFlushInit ();
extern UINT     fppQueueInit ();
extern STATUS   fppProbeSup ();
extern void     fppNan ();

#endif	/* __STDC__ */

#endif	/* _ASMLANGUAGE */

#define FP_DATA         0x00           		/* Data Registers */
#define FPD(n)          (FP_DATA + (4 * (n)))
#define FP_QUEUE        (FPD(FP_NUM_DREGS*2))	/* Q Registers (dword aligned)*/
#define FSR             (FP_QUEUE + 4)		/* Status Register */

#ifdef __cplusplus
}
#endif

#endif /* __INCfppSparcLibh */
