/* mcc.h - Header: MCC MultiCache Controller for use with TMS390 SuperSPARC */

/* Copyright 1984-1995 Wind River Systems, Inc. */

/* Copyright 1994-1995 FORCE COMPUTERS */

/*
modification history
--------------------
01c,22mar95,vin  added copyright.	
01b,15mar95,vin  integrated with 5.2.
01a,14jun93,tkj  written
*/

/* _PFN = 24 bit Page frame number = Physical address >> PAGE_SHIFT */
/* This is needed because the full physical address is 36 bits. */

/* _OFS = Offset from preceeding _PFN entry. */
/* This can be used to compute both the virtual and physical addresses. */

#ifndef	__INCmcch
#define __INCmcch

#define	MCC_BUS_MBUS	0
#define	MCC_BUS_XBUS	1

#ifndef	MCC_BUS_TYPE
#define	MCC_BUS_TYPE	MCC_BUS_MBUS
#endif	/* ! MCC_BUS_TYPE */

/*
 * The MCC has separate Instruction and Data Caches.  The configuration
 * varies depending on the external bus: MBus, or XBus.  The specifications
 * follow:
 *
 * Bus  E-cache size  Blocks      Sub-block size  Block size
 * X    512K, 1M, 2M  2K, 4K, 8K  64 bytes        4 sub-blocks = 256 bytes
 * M    None, 1M      0, 8K       32 bytes        4 sub-blocks = 128 bytes
 */

#if	MCC_BUS_TYPE == MCC_BUS_MBUS
#define	MCC_BLOCK_SHIFT		7
#define	MCC_SUBBLOCK_SIZE	32
#define	MCC_MAX_SIZE		(1024*1024)
#else	/* MCC_BUS_TYPE == MCC_BUS_MBUS */
#define	MCC_BLOCK_SHIFT		8
#define	MCC_SUBBLOCK_SIZE	64
#define	MCC_MAX_SIZE		(2*1024*1024)
#endif	/* MCC_BUS_TYPE == MCC_BUS_MBUS */

#define	MCC_SUBBLOCKS_PER_BLOCK	4

#define	MCC_BLOCK_SIZE		(MCC_SUBBLOCKS_PER_BLOCK*MCC_SUBBLOCK_SIZE)

#define	MCC_BLOCKS_PER_SET	1

#define	MCC_MAX_NUMBER_BLOCKS	(MCC_MAX_SIZE/MCC_BLOCK_SIZE)

			/* Bytes copied in one stream data operation */
#define	MCC_STREAM_SIZE		MCC_SUBBLOCK_SIZE

#ifdef __cplusplus
extern "C" {
#endif

/*
 * MCC E-cache data, E-cache tags, and Registers are accessed from either
 * the VBus or the system bus, e.g. the MBus.  The _VA values below are
 * for accesses from the VBus.  For accesses from the MBus use
 * MCC_MBUS_VA().
 */

#define	MCC_VBUS_ASI		0x02	/* Accesses from the VBus */
#define	MCC_MBUS_ASI		0x2f	/* Accesses from the MBus */

#define	MCC_ASI		MCC_VBUS_ASI	/* ASI for MCC accesses */

#define	MCC_MBUS_VA(VA, MID)	\
			(((VA) & ~0x0f000000) | 0xf0000000 | ((MID) << 24))

#define MCC_ECD_VA	0x01000000	/* E-cache Data */
#define	MCC_ECT_VA	0x01800000	/* E-cache Tags */

/* SDA = Stream Data Register: [MBus 2x64, XBus 4x64] */
#define	MCC_SDA_VA	0x01c00000	/* Stream Data Register */

/* SSO = Stream Source Register [64 bits] */
#define	MCC_SSO_VA	0x01c00100	/* Stream Source Register */

/* Top SSO.R = Ready */
#define	MCC_SSO_R_MASK		     0x80000000
#define MCC_SSO_R_IDLE               0x80000000
#define MCC_SSO_R_BUSY               0

/* Top SSO.C = Cacheable */
#define	MCC_SSO_C_MASK		     0x00000010
#define MCC_SSO_C_CACHEABLE          0x00000010
#define MCC_SSO_C_CACHEABLE_NOT      0

/* Top/Btm SSO.PA = Physical Address */
#define	MCC_SSO_PA_TOP_MASK	     0x0000000f
#define MCC_SSO_PA_TOP_SHIFT	     0
#define	MCC_SSO_PA_BTM_MASK	     0xfffffff8
#define MCC_SSO_PA_BTM_SHIFT	     3

/* SDE = Stream Destination Register [64 bits] */
#define	MCC_SDE_VA	0x01c00200	/* Stream Destination Reg. [64 bits] */

/* Top SDE.R = Ready */
#define	MCC_SDE_R_MASK		     0x80000000
#define MCC_SDE_R_IDLE               0x80000000
#define MCC_SDE_R_BUSY               0

/* Top SDE.C = Cacheable */
#define	MCC_SDE_C_MASK		     0x00000010
#define MCC_SDE_C_CACHEABLE          0x00000010
#define MCC_SDE_C_CACHEABLE_NOT      0

/* Top/Btm SDE.PA = Physical Address */
#define	MCC_SDE_PA_TOP_MASK	     0x0000000f
#define MCC_SDE_PA_TOP_SHIFT	     0
#define	MCC_SDE_PA_BTM_MASK	     0xfffffff8
#define MCC_SDE_PA_BTM_SHIFT	     3

#define	MCC_CMC_CRC_VA	0x01c00300	/* Reference/Miss Count [64 bits] */

/* IP = Interrupt Pending Register (Only XBus) [16 bits] */
#define	MCC_IP_VA	0x01c00406	/* Interrupt Pending Register */

/* IM = Interrupt Mask Register (Only XBus) [16 bits] */
#define	MCC_IM_VA	0x01c00506	/* Interrupt Mask */

/* IPC = Interrupt Pending Clear Register (Only XBus) [16 bits] */
#define	MCC_IPC_VA	0x01c00606	/* Interrupt Pending Clear */

/* IG = Interrupt Generation Register (Only XBus) [32 bits] */
#define	MCC_IG_VA	0x01c00704	/* Interrupt Generation */

/* BIST = BIST Register [32 bits] */
#define	MCC_BIST_VA	0x01c00804	/* BIST */

/* CCCR = MCC Control Register [32 bits] */
#define MCC_CCCR_VA	0x01c00a04	/* MCC Control */

/* CCCR.RC = Read Reference Count: Possibly wrong */
#define	MCC_CCCR_RC_MASK		0x00000200
#define	MCC_CCCR_RC_ONLY_READ		0x00000200
#define	MCC_CCCR_RC_READ_WRITE		0

/* CCCR.BWC = Bus watcher count (Only XBus) */
#define	MCC_CCCR_BWC_MASK		0x00000180
#define	MCC_CCCR_BWC_1			0
#define	MCC_CCCR_BWC_2			0x00000080
#define	MCC_CCCR_BWC_4			0x00000100

/* CCCR.WI = Write invalidate (Only XBus) */
#define	MCC_CCCR_WI_MASK		0x00000040
#define MCC_CCCR_WI_ENABLE              0x00000040
#define MCC_CCCR_WI_ENABLE_NOT          0x00000000
#define MCC_CCCR_WI_DISABLE             MCC_CCCR_WI_ENABLE_NOT

/* CCCR.PF = Prefetch enable */
#define	MCC_CCCR_PF_MASK		0x00000020
#define MCC_CCCR_PF_ENABLE              0x00000020
#define MCC_CCCR_PF_ENABLE_NOT          0x00000000
#define MCC_CCCR_PF_DISABLE             MCC_CCCR_PF_ENABLE_NOT

/* CCCR.MC = Multiple command enable */
#define	MCC_CCCR_MC_MASK		0x00000010
#define MCC_CCCR_MC_ENABLE              0x00000010
#define MCC_CCCR_MC_ENABLE_NOT          0x00000000
#define MCC_CCCR_MC_DISABLE             MCC_CCCR_MC_ENABLE_NOT

/* CCCR.PE = Parity enable */
#define	MCC_CCCR_PE_MASK		0x00000008
#define MCC_CCCR_PE_ENABLE              0x00000008
#define MCC_CCCR_PE_ENABLE_NOT          0x00000000
#define MCC_CCCR_PE_DISABLE             MCC_CCCR_PE_ENABLE_NOT

/* CCCR.CE = E-cache enable */
#define	MCC_CCCR_CE_MASK		0x00000004
#define MCC_CCCR_CE_ENABLE              0x00000004
#define MCC_CCCR_CE_ENABLE_NOT          0x00000000
#define MCC_CCCR_CE_DISABLE             MCC_CCCR_CE_ENABLE_NOT

/* CCCR.CS = E-cache size (Only XBus) */
#define	MCC_CCCR_CS_MASK		0x00000002
#define MCC_CCCR_CS_2MB                 0x00000002
#define MCC_CCCR_CS_1MB                 0x00000000

/* CCCR.HC = Half cache (Only XBus) */
#define	MCC_CCCR_HC_MASK		0x00000001
#define MCC_CCCR_HC_ENABLE              0x00000001
#define MCC_CCCR_HC_ENABLE_NOT          0x00000000
#define MCC_CCCR_HC_DISABLE             MCC_CCCR_HC_ENABLE_NOT

/* CCSR = MCC Status Register [64 bits] */
#define	MCC_CCSR_VA	0x01c00b00	/* MCC Status */

/* Top CCSR.SXP = Store Exception Pending */
#define	MCC_CCSR_SXP_MASK		0x00000080
#define MCC_CCSR_SXP_ENABLE             0x00000080
#define MCC_CCSR_SXP_ENABLE_NOT         0x00000000
#define MCC_CCSR_SXP_DISABLE            MCC_CCSR_SXP_ENABLE_NOT

/* Top CCSR.SM = Synchronous Mode */
#define	MCC_CCSR_SM_MASK		0x00000040
#define MCC_CCSR_SM_ENABLE              0x00000040
#define MCC_CCSR_SM_ENABLE_NOT          0x00000000
#define MCC_CCSR_SM_DISABLE             MCC_CCSR_SM_ENABLE_NOT

/* Top CCSR.NCSID = Non-cacheable store bus watcher ID (Only XBus) */
#define	MCC_CCSR_NCSID_MASK		0x00000030
#define MCC_CCSR_NCSID_SHIFT		4

/* Top/Btm CCSR.NCSPA = Non-cacheable store page address */
#define	MCC_CCSR_NCSPA_TOP_MASK		0x0000000f
#define MCC_CCSR_NCSPA_TOP_SHIFT	0
#define	MCC_CCSR_NCSPA_BTM_MASK		0xfffff000
#define MCC_CCSR_NCSPA_BTM_SHIFT	12

/* Btm CCSR.NCSPC = Non-cacheable store pending count */
#define	MCC_CCSR_NCSPC_MASK		0x00000f00
#define MCC_CCSR_NCSPC_SHIFT		8

/* Btm CCSR.SPC = Store pending count */
#define	MCC_CCSR_SPC_MASK		0x000000f0
#define MCC_CCSR_SPC_SHIFT		4

/* Btm CCSR.BC = Boot communication */
#define	MCC_CCSR_BC_MASK		0x00000008
#define MCC_CCSR_BC_ENABLE              0x00000008
#define MCC_CCSR_BC_ENABLE_NOT          0x00000000
#define MCC_CCSR_BC_DISABLE             MCC_CCSR_BC_ENABLE_NOT

/* Btm CCSR.WP = Write miss pending */
#define	MCC_CCSR_WP_MASK		0x00000004
#define MCC_CCSR_WP_ENABLE              0x00000004
#define MCC_CCSR_WP_ENABLE_NOT          0x00000000
#define MCC_CCSR_WP_DISABLE             MCC_CCSR_WP_ENABLE_NOT

/* Btm CCSR.RP = Read pending */
#define	MCC_CCSR_RP_MASK		0x00000002
#define MCC_CCSR_RP_ENABLE              0x00000002
#define MCC_CCSR_RP_ENABLE_NOT          0x00000000
#define MCC_CCSR_RP_DISABLE             MCC_CCSR_RP_ENABLE_NOT

/* Btm CCSR.PP = Prefetch pending */
#define	MCC_CCSR_PP_MASK		0x00000001
#define MCC_CCSR_PP_ENABLE              0x00000001
#define MCC_CCSR_PP_ENABLE_NOT          0x00000000
#define MCC_CCSR_PP_DISABLE             MCC_CCSR_PP_ENABLE_NOT

/* R = Reset Register [32 bits] */
#define MCC_R_VA	0x01c00c04	/* Reset */

#define	MCC_R_MASK			0x00000006
#define MCC_R_HARDWARE              	0x00000000

/* R.WD = Watchdog Reset */
#define	MCC_R_WD_MASK			0x00000004
#define MCC_R_WD_ENABLE              	0x00000004
#define MCC_R_WD_ENABLE_NOT          	0x00000000
#define MCC_R_WD_DISABLE             	MCC_R_WD_ENABLE_NOT

/* R.SI = Software Internal Reset */
#define	MCC_R_SI_MASK			0x00000002
#define MCC_R_SI_ENABLE              	0x00000002
#define MCC_R_SI_ENABLE_NOT          	0x00000000
#define MCC_R_SI_DISABLE             	MCC_R_SI_ENABLE_NOT

/* CCER = Error Register [64 bits] */
#define	MCC_CCER_VA	0x01c00e00	/* Error */

/* Top CCER.ME = Multiple Errors */
#define	MCC_CCER_ME_MASK		0x80000000
#define MCC_CCER_ME_ENABLE             	0x80000000
#define MCC_CCER_ME_ENABLE_NOT         	0x00000000
#define MCC_CCER_ME_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.XP = XBus Parity Error */
#define	MCC_CCER_XP_MASK		0x40000000
#define MCC_CCER_XP_ENABLE             	0x40000000
#define MCC_CCER_XP_ENABLE_NOT         	0x00000000
#define MCC_CCER_XP_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.CC = Cache Consistency Error */
#define	MCC_CCER_CC_MASK		0x20000000
#define MCC_CCER_CC_ENABLE             	0x20000000
#define MCC_CCER_CC_ENABLE_NOT         	0x00000000
#define MCC_CCER_CC_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.VP = VBus Parity Error */
#define	MCC_CCER_VP_MASK		0x10000000
#define MCC_CCER_VP_ENABLE             	0x10000000
#define MCC_CCER_VP_ENABLE_NOT         	0x00000000
#define MCC_CCER_VP_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.CP = VBus Parity Error with the MCC as the bus master */
#define	MCC_CCER_CP_MASK		0x08000000
#define MCC_CCER_CP_ENABLE             	0x08000000
#define MCC_CCER_CP_ENABLE_NOT         	0x00000000
#define MCC_CCER_CP_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.AE = Asynchronous Error */
#define	MCC_CCER_AE_MASK		0x04000000
#define MCC_CCER_AE_ENABLE             	0x04000000
#define MCC_CCER_AE_ENABLE_NOT         	0x00000000
#define MCC_CCER_AE_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.EV = Error Information Valid */
#define	MCC_CCER_EV_MASK		0x02000000
#define MCC_CCER_EV_ENABLE             	0x02000000
#define MCC_CCER_EV_ENABLE_NOT         	0x00000000
#define MCC_CCER_EV_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top CCER.ERR = Error Code */
#define	MCC_CCER_ERR_MASK		0x00007f80
#define	MCC_CCER_ERR_SHIFT		6
#define	MCC_CCER_ERR_UC			0x00000080
#define	MCC_CCER_ERR_TO			0x00000100
#define	MCC_CCER_ERR_BE			0x00000180
#define	MCC_CCER_ERR_UD			0x00000200

/* Top CCER.S = Supervisor Bit */
#define	MCC_CCER_S_MASK			0x00000040
#define MCC_CCER_S_ENABLE             	0x00000040
#define MCC_CCER_S_ENABLE_NOT         	0x00000000
#define MCC_CCER_S_DISABLE            	MCC_R_WD_ENABLE_NOT

/* Top/Btm CCER.PA = Physical Address */
#define	MCC_CCER_PA_TOP_MASK	     0x0000000f
#define MCC_CCER_PA_TOP_SHIFT	     0
#define	MCC_CCER_PA_BTM_MASK	     0xffffffff
#define MCC_CCER_PA_BTM_SHIFT	     0

/* MP = MBus Port Register [32 bits] */
#define MCC_MP_VA	0x01c00f04	/* MBus port */
#define	MCC_MP_MID_MASK			0x0f000000
#define MCC_MP_MID_SHIFT		24
	
/* MCC Tag SOVP */
/* Bit definitions */
#define	MCC_SOVP_S	0x8		/* Shared */
#define	MCC_SOVP_O	0x4		/* Owner or Dirty */
#define	MCC_SOVP_V	0x2		/* Valid */
#define	MCC_SOVP_P	0x1		/* Pending */
/* Definitions for each subblock */
#define	MCC_SOVP_0_S	MCC_SOVP_S
#define	MCC_SOVP_0_O	MCC_SOVP_O
#define	MCC_SOVP_0_V	MCC_SOVP_V
#define	MCC_SOVP_0_P	MCC_SOVP_P

#define	MCC_SOVP_1_S	(MCC_SOVP_S << 1*4)
#define	MCC_SOVP_1_O	(MCC_SOVP_O << 1*4)
#define	MCC_SOVP_1_V	(MCC_SOVP_V << 1*4)
#define	MCC_SOVP_1_P	(MCC_SOVP_P << 1*4)

#define	MCC_SOVP_2_S	(MCC_SOVP_S << 2*4)
#define	MCC_SOVP_2_O	(MCC_SOVP_O << 2*4)
#define	MCC_SOVP_2_V	(MCC_SOVP_V << 2*4)
#define	MCC_SOVP_2_P	(MCC_SOVP_P << 2*4)

#define	MCC_SOVP_3_S	(MCC_SOVP_S << 3*4)
#define	MCC_SOVP_3_O	(MCC_SOVP_O << 3*4)
#define	MCC_SOVP_3_V	(MCC_SOVP_V << 3*4)
#define	MCC_SOVP_3_P	(MCC_SOVP_P << 3*4)

#ifndef	_ASMLANGUAGE

/* typedefs */

/* Double word for MCC tags */

typedef struct
    {
    UINT high;
    UINT low;
    } mccTag;

typedef struct /* Split physical addr. [31:0] for the MCC on MBus */
    {
	unsigned	mpa_AddTBtm    :12; /* Bottom of AddT */
	unsigned	mpa_blk        :13; /* Block selection 0-8191 */
	unsigned	mpa_sub        : 2; /* Subblock selection 0-3 */
	unsigned	mpa_dbl        : 2; /* Double word selection 0-3 */
	unsigned	mpa_byte       : 3; /* Byte selection 0-7 */
    } MCC_MBUS_PA;

typedef struct /* Split physical addr. [31:0] for 512K MCC on XBus */
    {
	unsigned	x512pa_AddTBtm :13; /* Bottom of AddT */
	unsigned	x512pa_blk     :11; /* Block selection 0-2047 */
	unsigned	x512pa_sub     : 2; /* Subblock selection 0-3 */
	unsigned	x512pa_dbl     : 3; /* Double word selection 0-3 */
	unsigned	x512pa_byte    : 3; /* Byte selection 0-7 */
    } MCC_XBUS_512K_PA;

typedef struct /* Split physical addr. [31:0] for 1MB MCC on XBus */
    {
	unsigned	x1pa_AddTBtm   :12; /* Bottom of AddT */
	unsigned	x1pa_blk       :12; /* Block selection 0-4095 */
	unsigned	x1pa_sub       : 2; /* Subblock selection 0-3 */
	unsigned	x1pa_dbl       : 3; /* Double word selection 0-3 */
	unsigned	x1pa_byte      : 3; /* Byte selection 0-7 */
    } MCC_XBUS_1M_PA;

typedef struct /* Split physical addr. [31:0] for 2MB MCC on XBus */
    {
	unsigned	x2pa_AddTBtm   :11; /* Bottom of AddT */
	unsigned	x2pa_blk       :13; /* Block selection 0-8191 */
	unsigned	x2pa_sub       : 2; /* Subblock selection 0-3 */
	unsigned	x2pa_dbl       : 3; /* Double word selection 0-3 */
	unsigned	x2pa_byte      : 3; /* Byte selection 0-7 */
    } MCC_XBUS_2M_PA;

typedef union /* Convert physical address [31:0] to/from split form */
    {
	UINT		word;		/* Physical address [31:0] */
	MCC_MBUS_PA	mbus;		/* MCC MBus split physical address */
	MCC_XBUS_512K_PA xbus512K;	/* 512K MCC XBus split */
	MCC_XBUS_1M_PA	xbus1M;		/* 1M MCC XBus split */
	MCC_XBUS_2M_PA	xbus2M;		/* 2M MCC XBus split */
    } MCC_PA_UNION;

typedef struct /* MCC on MBus MCC Data Address for MBUS_VBUS_ASI */
    {
	unsigned	mda_cspaceBtm: 8; /* Must be one */
	unsigned	mda_type     : 2; /* Must be zero */
	unsigned		     : 2;
	unsigned	mda_blk      :13; /* Block selection 0-8191 */
	unsigned	mda_sub      : 2; /* Subblock selection 0-3 */
	unsigned	mda_dbl      : 2; /* Double word selection 0-3 */
	unsigned	mda_byte     : 3; /* Byte selection 0-7 */
    } MCC_MBUS_DATA_ADRS;

typedef struct /* MBus MCC Tag Address for MBUS_VBUS_ASI */
    {
	unsigned	mta_cspaceBtm: 8; /* Must be one */
	unsigned	mta_type     : 2; /* Must be two */
	unsigned		     : 2;
	unsigned	mta_blk      :13; /* Block selection 0-8191 */
	unsigned		     : 7;
    } MCC_MBUS_TAG_ADRS;

typedef struct /* 2MB MCC on XBus MCC Data Address for MBUS_VBUS_ASI */
    {
	unsigned	xda_cspaceBtm: 8; /* Must be one */
	unsigned	xda_type     : 2; /* Must be zero */
	unsigned		     : 1;
	unsigned	xda_blk      :13; /* Block selection 0-8191 */
	unsigned	xda_sub      : 2; /* Subblock selection 0-3 */
	unsigned	xda_dbl      : 3; /* Double word selection 0-7 */
	unsigned	xda_byte     : 3; /* Byte selection 0-7 */
    } MCC_XBUS_DATA_ADRS;

typedef struct /* 2MB MCC on XBus MCC Tag Address for MBUS_VBUS_ASI */
    {
	unsigned	xta_cspaceBtm: 8; /* Must be one */
	unsigned	xta_type     : 2; /* Must be two */
	unsigned		     : 1;
	unsigned	xta_blk      :13; /* Block selection 0-8191 */
	unsigned		     : 8;
    } MCC_XBUS_TAG_ADRS;

/* 
NOTE: All MCC_MBUS_DATA_ADRS throuhg MCC_XBUS_TAG_ADRS fields that are
common are in the same place or overlay consistently.  Thus all can be
combined into one union which can be used for any by initializing all
needed fields. In addition, the 2MB XBus can be used for all XBus sizes
since the only difference is the maximum allowed block number.
*/

typedef union /* MCC Cache Address Union */
    {
	UINT			word;	/* For clearing reserved fields */
	MCC_MBUS_DATA_ADRS	mbusData; /* MBus Data address */
	MCC_MBUS_TAG_ADRS	mbusTag; /* MBus Tag address */
	MCC_XBUS_DATA_ADRS	xbusData; /* XBus Data address */
	MCC_XBUS_TAG_ADRS	xbusTag; /* XBus Tag address */
    } MCC_ADRS_UNION;

typedef struct /* MCC on MBus Tag */
    {
	unsigned	mmt_unused   :28; /* First element can't be unnamed */
	unsigned	mmt_AddTTop  : 4; /* Physical address tag */
	unsigned	mmt_AddTBtm  :12;
	unsigned		     : 4;
				/* Blocks 3-0: Shared/Owner/Valid/Pending */
	unsigned	mmt_sovp     :16;
    } MCC_MBUS_TAG;

typedef struct /* 512K MCC on XBus Tag */
    {
	unsigned    x512mt_unused    :28; /* First element can't be unnamed */
	unsigned    x512mt_AddTTop   : 4; /* Physical address tag */
	unsigned    x512mt_AddTBtm   :13;
	unsigned		     : 3;
				/* Blocks 3-0: Shared/Owner/Valid/Pending */
	unsigned char    x512mt_sovp[4];
    } MCC_XBUS_512K_TAG;

typedef struct /* 1M MCC on XBus Tag */
    {
	unsigned    x1mt_unused      :28; /* First element can't be unnamed */
	unsigned    x1mt_AddTTop     : 4; /* Physical address tag */
	unsigned    x1mt_AddTBtm     :12;
	unsigned		     : 4;
				/* Blocks 3-0: Shared/Owner/Valid/Pending */
	unsigned char    x1mt_sovp[4];
    } MCC_XBUS_1M_TAG;

typedef struct /* 2M MCC on XBus Tag */
    {
	unsigned    x2mt_unused      :28; /* First element can't be unnamed */
	unsigned    x2mt_AddTTop     : 4; /* Physical address tag */
	unsigned    x2mt_AddTBtm     :11;
	unsigned		     : 5;
				/* Blocks 3-0: Shared/Owner/Valid/Pending */
	unsigned char    x2mt_sovp[4];
    } MCC_XBUS_2M_TAG;

typedef union /* MCC Tag Union */
    {
	mccTag			mccTag;	/* For clearing reserved fields */
	MCC_MBUS_TAG		mbus;	/* MBus Tag */
	MCC_XBUS_512K_TAG	xbus512K; /* 512K XBus Tag */
	MCC_XBUS_1M_TAG		xbus1M; /* 1M XBus Tag */
	MCC_XBUS_2M_TAG		xbus2M; /* 2M XBus Tag */
    } MCC_TAG_UNION;

#if defined(__STDC__) || defined(__cplusplus)

IMPORT void    mccFlushEntire  (UINT lowerBase);
IMPORT void    mccGetTag       (UINT blockNumber, mccTag *ptagValue);
IMPORT void    mccInvalidateEntire ();
IMPORT UINT    mccGetCCCR      ();
IMPORT UINT    mccUpdateCCCR   (UINT modifyMask,
				UINT modifyValue);

#else   /* __STDC__ */

IMPORT void    mccFlushEntire  ();
IMPORT void    mccGetTag       ();
IMPORT void    mccInvalidateEntire ();
IMPORT UINT    mccGetCCCR      ();
IMPORT UINT    mccUpdateCCCR   ();

#endif  /* __STDC__ */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmcch */
