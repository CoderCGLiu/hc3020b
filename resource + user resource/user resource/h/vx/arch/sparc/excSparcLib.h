/* excSparcLib.h - exception library header */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
01i,18aug93,jmm  added external declaration for excExcepHook
01h,22sep92,rrr  added support for c++
01g,06jul92,jwt  added function prototypes - excWindowInit(), trapEnable().
01f,26may92,rrr  the tree shuffle
01e,14jan92,yao  included esf.h.
01d,19dec91,jwt  cleaned up ANSI compiler warnings/errors.
01c,04oct91,rrr  passed through the ansification filter
		  -changed copyright notice
01b,20sep91,jwt  added pTcb to EXC_INFO for excMemAdrsGet().
01a,29nov90,jwt  written from "h/excLib.h" to "h/<CPU>/excLib.h".

 * generic exception information -
 * kept in the tcb of tasks that have caused exceptions */

#ifndef __INCexcSparcLibh
#define __INCexcSparcLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "esf.h"

/* variable declarations */

extern FUNCPTR  excExcepHook;   /* add'l rtn to call when exceptions occur */

typedef struct
    {
    UINT16  valid;	/* indicators that following fields are valid */
    UINT16  trapType;	/* trap type (0 - 255) */
    INSTR   *pc;	/* program counter */
    INSTR   *npc;	/* next program counter */
    ULONG   psr;	/* processor status register */
    char    *pTcb;      /* Pointer to TCB */
    } EXC_INFO;

/* exception info valid bits */

#define EXC_TRAP_TYPE		0x01	/* trap type valid */
#define EXC_PC			0x02	/* pc valid */
#define EXC_NPC			0x04	/* npc valid */
#define EXC_PSR			0x08	/* status register valid */
#define EXC_ALL			0x0f

#ifndef	_ASMLANGUAGE
extern	void	excWindowInit (void);
extern	void	trapEnable (void);
extern	void	excFppKernelResume (ESF *pESF);
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCexcSparcLibh */
