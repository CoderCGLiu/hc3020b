/* cy604.h - Ironics IV-SPRC  cy604 MMU/CC constants */

/*
modification history
--------------------
01i,03dec92,jwt  #undef/#define of _STACK_ALIGN_SIZE to _CACHE_ALIGN_SIZE.
01h,29sep92,jwt  merged cacheXLibInit(), cacheXReset(), and cacheXModeSet().
01g,22sep92,rrr  added support for c++
01f,31jul92,jwt  changed cacheCy604Malloc() to cacheCy604DmaMalloc().
01e,07jul92,jwt  added cache library function prototypes.
01d,16jun92,jwt  passed through the ansification filter
		  -changed copyright notice
01c,16jan92,jwt	 added additional MMU and cache definitions.
    20aug91,jpb  Reformatted according to WRS conventions.
01b,02may91,rjt  updated
01a,16dec90,rjt  copied and cleaned up from ironics monitor code
*/

#ifndef __INCcy604h
#define __INCcy604h

#ifdef __cplusplus
extern "C" {
#endif

#include "cacheLib.h"

#define		SPARC_MMU_REG_ASI	0x04	/* Reference MMU Registers */
#define		SPARC_BYPASS_ASI	0x20	/* Bypass (not cached) Space */

#define 	SCR_INDEX			0x0
#define		MMU_ENABLE			0x00000001
#define		MMU_CACHE_ENABLE		0x00000100
#define		CACHE_LOCK_GLOBAL		0x00000200
#define		CACHE_MODE_FIELD		0x00000400
#define		CACHE_MODE_WRITE_THRU		0x00000000
#define		CACHE_MODE_COPY_BACK		0x00000400
#define		CACHEABLE			0x00002000

#define 	CTPR_INDEX	0x100
#define 	CXR_INDEX	0x200
#define 	SFSR_INDEX	0x300
#define		OW_BIT		0x1
#define		FAV_BIT		0x2
#define		FT_BIT_0	0x4
#define		FT_BIT_1	0x8
#define		FT_BIT_2	0x10
#define		AT_BIT_0	0x20
#define		AT_BIT_1	0x40
#define		AT_BIT_2	0x80
#define		LEV_BIT_0	0x100
#define		LEV_BIT_1	0x200
#define		BE_BIT		0x400
#define		TO_BIT		0x800
#define		UC_BIT		0x1000
#define		CBT_BIT		0x2000
#define 	SFAR_INDEX	0x400
#define 	AFSR_INDEX	0x500
#define		AFO_BIT		0x1
#define		AFA_BIT_0	0x10
#define		AFA_BIT_1	0x20
#define		AFA_BIT_2	0x40
#define		AFA_BIT_3	0x80
#define		BE_BIT		0x400
#define		TO_BIT		0x800
#define		UC_BIT		0x1000
#define 	AFAR_INDEX	0x600
#define 	RR_INDEX	0x700
#define 	SOFT_EXT_RESET  0x1
#define 	SOFT_INT_RESET  0x2
#define 	WATCHDOG_RESET  0x4
#define 	RPR_INDEX	0x1000
#define 	IPTP_INDEX	0x1100
#define 	DPTP_INDEX	0x1200
#define 	ITR_INDEX	0x1300
#define 	TRCR_INDEX	0x1400

/*
 * TLB DEFINES
 */

#define TLB_FLUSH_ASI   0x3
#define	TLB_MEMORY_ASI	0x6

#define TLB_FLUSH_PG   0x000	/* Page flush.      */
#define TLB_FLUSH_SEG  0x100	/* Segment flush.   */
#define TLB_FLUSH_REG  0x200	/* Region flush.    */
#define TLB_FLUSH_CX   0x300	/* Context flush.   */
#define TLB_FLUSH_ALL  0x400	/* Entire flush.    */


/* valid bit */

#define TLB_VALID       0x1

#define PAGE_SIZE_4K    0x0
#define PAGE_SIZE_256K  0x2
#define PAGE_SIZE_16M   0x4
#define PAGE_SIZE_4G    0x6

/* actual TLB acc bit mask - supervisor has all access rights of user */
/* R=read W=write, E=execute */

#define PROT_USR_R          0x00
#define PROT_USR_RW         0x08
#define PROT_USR_RE         0x10
#define PROT_USR_RWE        0x18
#define PROT_USR_E          0x20
#define PROT_USR_R_SUP_RW   0x28
#define PROT_SUP_RE         0x30
#define PROT_SUP_RWE        0x38

/* cache bits */

#define TLB_CACHE_ENABLE    0x80
#define TLB_CACHE_DISABLE   0x00
#define	CACHE_INHIBITED_RAM_TLB		16
#define	CACHE_INHIBITED_RAM_ADRS	0x80000000

/* Cache Flush Support */

#define	CACHE_FLUSH_PAGE	0x10		/* Page Flush ASI */
#define	CACHE_FLUSH_SEGMENT	0x11		/* Segment Flush ASI */
#define	CACHE_FLUSH_REGION	0x12		/* Region Flush ASI */
#define	CACHE_FLUSH_CONTEXT	0x13		/* Context Flush ASI */
#define	CACHE_FLUSH_USER	0x14		/* User Flush ASI */

#define	CY604_CACHE_SIZE	0x00010000	/* 64K bytes / chip */
#define	CACHE_LINES		2048		/* 2 ** 11 */
#undef	_CACHE_ALIGN_SIZE
#define	_CACHE_ALIGN_SIZE	32		/* 32 bytes */
#define	CACHE_LINE_SIZE		_CACHE_ALIGN_SIZE

#define	CACHE_CLEAR_REGION	(CACHE_CLEAR_CONTEXT + 1)
#define	CACHE_CLEAR_USER	(CACHE_CLEAR_CONTEXT + 2)
#define	CACHE_LOCK		(CACHE_CLEAR_CONTEXT + 3)
#define	CACHE_UNLOCK		(CACHE_CLEAR_CONTEXT + 4)

#define	CY604_CACHE_PAGE_SIZE	 0x00001000	/* 4K bytes */
#define	CY604_CACHE_SEGMENT_SIZE 0x00040000	/* 256K bytes */
#define	CY604_CACHE_REGION_SIZE	 0x01000000	/* 16M bytes */
#define	CY604_CACHE_CONTEXT_SIZE 0x00000000	/* 4G bytes */
#define	CY604_SPARC_CONTEXTS	 4096

#ifndef	_ASMLANGUAGE
IMPORT	STATUS cacheCy604LibInit (CACHE_MODE data, CACHE_MODE inst);
IMPORT	STATUS cacheCy604Enable (CACHE_TYPE cache);
IMPORT	STATUS cacheCy604Disable (CACHE_TYPE cache);
IMPORT	STATUS cacheCy604Lock (CACHE_TYPE cache,
			       void * address, size_t bytes);
IMPORT	STATUS cacheCy604Unlock (CACHE_TYPE cache,
				 void * address, size_t bytes);
IMPORT	STATUS cacheCy604Clear (CACHE_TYPE cache,
				void * address, size_t bytes);
IMPORT	void * cacheCy604DmaMalloc (size_t bytes);
IMPORT	STATUS cacheCy604DmaFree (void * pBuf);

IMPORT	STATUS cacheCy604ClearLine (CACHE_TYPE cache, void * address);
IMPORT	STATUS cacheCy604ClearPage (CACHE_TYPE cache, void * address);
IMPORT	STATUS cacheCy604ClearSegment (CACHE_TYPE cache, void * address);
IMPORT	STATUS cacheCy604ClearRegion (CACHE_TYPE cache, void * address);
IMPORT	STATUS cacheCy604ClearContext (CACHE_TYPE cache, void * address);
IMPORT	STATUS cacheCy604ClearUser (CACHE_TYPE cache, void * address);

IMPORT	UINT   cy604Get (void * address);
IMPORT	void   cy604Set (void * address, UINT reg);
IMPORT	void   cy604LineFlush (void * address);
IMPORT	void   cy604PageFlush (void * address);
IMPORT	void   cy604SegmentFlush (void * address);
IMPORT	void   cy604RegionFlush (void * address);
IMPORT	void   cy604WriteBufferFlush (void);
IMPORT	ULONG  cy604TlbRegGet (int index);
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCcy604h */
