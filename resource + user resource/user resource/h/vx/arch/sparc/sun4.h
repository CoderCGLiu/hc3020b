/* sun4.h - Sun-4 system header file */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,30jul93,jwt  resolved conflicts with sun.h; added include sun.h.
    29nov93,ccc  added copyright notice.
01h,16oct92,jwt  added prototype for mmuSun4LibInit().
01g,29sep92,jwt  merged cacheXLibInit(), cacheXReset(), and cacheXModeSet().
01f,22sep92,rrr  added support for c++
01e,27aug92,jwt  fixed MMU accessed and modified bit field masks.
01d,31jul92,jwt  changed cacheSun4Malloc() to cacheSun4DmaMalloc().
01c,03jul92,jwt  added function prototypes for cache library support.
01b,16jun92,jwt  passed through the ansification filter
		  -fixed #else and #endif
		  -changed ASMLANGUAGE to _ASMLANGUAGE
		  -changed copyright notice
01a,01apr92,jwt  created from sun1e.h
*/

/*
This file contains I/O address and related constants
for the Sun-1/E CPU board.
*/

#ifndef __INCsun4h
#define __INCsun4h

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "cacheLib.h"
//#include "sun.h"

/* Alternate Space Indices (ASI) */

#define	SUN4_SYSTEM_ASI		 2		/* System Control Space */
#define	SUN4_SEGMENT_ASI	 3		/* Segment Map */
#define	SUN4_PAGE_ASI		 4		/* Page Map */
#define	SUN4_SEGMENT_FLUSH_ASI	12		/* Segment Flush */
#define	SUN4_PAGE_FLUSH_ASI	13		/* Page Flush */
#define	SUN4_CONTEXT_FLUSH_ASI	14		/* Context Flush */

/* cache and memory management */

#define CACHE_SIZE              0x00010000      /* 64K bytes */
#define CACHE_LINE_SIZE         _CACHE_ALIGN_SIZE	/* 16 bytes */

#define ROOT_DESC       0       /* Root (Context) Descriptor - level 0 */
#define SEGMENT_DESC    1       /* Segment Descriptor - level 1 */
#define PAGE_DESC       2       /* Page Descriptor - level 2 */

#define SUN4_PAGE_SIZE          0x00002000      /* 8K bytes */
#define PAGES_PER_SEGMENT       32              /* 32 * 8K = 256K */
#define SUN4_SEGMENT_SIZE       (SUN4_PAGE_SIZE * PAGES_PER_SEGMENT)
#define SUN4_CONTEXT_SIZE	0x40000000	/* 1 Gigabyte */
#define	SUN4_SPARC_CONTEXTS	8

#define VIRTUAL_ADDRESS_MASK    0x3FFC0000      /* Virtual Address Mask */
#define PHYSICAL_ADDRESS_MASK   0x1FFC0000      /* Physical Address Mask */
#define SUN_DMA			0xFF000000      /* DMA Virtual Address */
#define SUN_DVMA                0xFFF00000      /* DVMA Virtual Address */

#define SUN_PAGE_BASE		0x00000000	/* This is in control space */
#define SUN_SEG_BASE		0x00000000	/* This is in control space */

#define MMU_PTE_ATTR    	0xFF000000	/* MMU PTE Attribute Field */
#define MMU_PTE_VALID   	0x80000000	/* MMU PTE Valid Bit */
#define MMU_PAGE_ALL_OK		0xc0000000	/* Total priv page entry */
#define MMU_WRITE_PROTECT	0x80000000	/* Write protect page entry */
#define MMU_WRITEPROTECT_MASK	0x40000000	/* Write protect page entry */
#define MMU_PRIVILEGE_MASK	0x20000000	/* Supervisor/user bit */
#define MMU_DONT_CACHE		0x10000000	/* Total priv page entry */
#define	MMU_ACCESSED_MASK	0x02000000	/* Accessed Bit */
#define	MMU_MODIFIED_MASK	0x01000000	/* Modified Bit */

#define	N_PMEG		0x100
#define	MIN_PMEG	0x00
#define	MAX_PMEG	0xff
#define	INVALID_PMEG	0xff

/* space types */

#define	SUN_TYPE_MASK	0x0C000000		/* Space type field mask */
#define	SUN_TYPE_ADDR	0xE0000000		/* Type address bits <31:29> */

#define SUN4_MEM	(SUN_MEM   << 26)	/* Memory */
#define	SUN4_IO		(SUN_IO    << 26)	/* Onboard I/O */
#define	SUN4_VME16	(SUN_VME16 << 26)
#define	SUN4_VME32	(SUN_VME32 << 26)

/* --------------------- Control Space Address Definitions ----------------- */

#define SUN_CONTEXT		0x30000000	/* Context register */
#define SUN_ENABLE		0x40000000	/* System enable register */
#define SUN_BUS_ERROR		0x60000000	/* Bus error register */
#define SUN_CACHE_TAGS		0x80000000	/* Cache tags */
#define SUN_CACHE_DATA          0x90000000      /* Cache Data SRAM */
#define SUN_UART_BACKDOOR	0xf0000000	/* Avoids MMU translation */

#define SUN_TAG_INVALID         0x00080000
#define SUN_TAG_VALID           0x00080000

/* masks for system enable register */

#define SUN_ENABLE_DIAG		0x01		/* Enable diagnostic LEDs */
#define SUN_ENABLE_RESET	0x04		/* Reset */
#define SUN_ENABLE_CACHE	0x10		/* Enable cache */
#define SUN_ENABLE_SDVMA	0x20		/* Enable system DVMA */
#define SUN_ENABLE_BOOT		0x00		/* Enable boot state */
#define SUN_DISABLE_BOOT	0x80		/* Disable boot state */

#define SUN_SLAVE_BLOCK_MODE    0x80            /* Slave Block Mode */
#define SUN_SLAVE_DVMA_DISABLE  0x40            /* Slave Interface Disable */

#ifndef	_ASMLANGUAGE

typedef	unsigned char	PMEG;
typedef UINT PTE;

typedef union   {
        PMEG pmeg;      /* Page Map Entry Group */
        PTE  pte;       /* Pate Table Entry */
        } DESC;         /* Descriptor */

IMPORT	STATUS	mmuSun4LibInit (int pageSize);

IMPORT	STATUS	cacheSun4LibInit (CACHE_MODE data, CACHE_MODE inst);
IMPORT	STATUS	cacheSun4Enable (CACHE_TYPE cache);
IMPORT	STATUS	cacheSun4Disable (CACHE_TYPE cache);
IMPORT	STATUS	cacheSun4Invalidate (CACHE_TYPE cache,
                                     void * address, size_t bytes);
IMPORT	void *	cacheSun4DmaMalloc (size_t bytes);
IMPORT	STATUS	cacheSun4DmaFree (void * pBuf);

IMPORT	STATUS	cacheSun4ClearLine (CACHE_TYPE cache, void * address);
IMPORT	STATUS	cacheSun4ClearPage (CACHE_TYPE cache, void * address);
IMPORT	STATUS	cacheSun4AddrEnable (void * address, size_t bytes);
IMPORT	STATUS	cacheSun4AddrDisable (void * address, size_t bytes);

IMPORT	UCHAR	sun4SysGetB (void * address);
IMPORT	void	sun4SysSetB (void * address, UCHAR byte);
IMPORT	UINT	sun4SysGet (void * address);
IMPORT	void	sun4SysSet (void * address, UINT word);
IMPORT	UINT	sun4PageGet (void * address);
IMPORT	void	sun4PageSet (void * address, UINT page);
IMPORT	void	sun4PageFlush (void * address);
IMPORT	PMEG	sun4SegGet (void * address);
IMPORT	void	sun4SegSet (void * address, unsigned pmeg);
IMPORT  UCHAR   sun4SegmentGet (void * address);
IMPORT  void    sun4SegmentSet (void * address, PMEG pmeg);
IMPORT	void	sun4SegmentFlush (void * address);
IMPORT	void	sun4ContextFlush (void * ix);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsun4h */
