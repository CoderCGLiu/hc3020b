/* ussSun4.h - US Software to Sun-4 conversion header */

/*
modification history
--------------------
01b,22sep92,rrr  added support for c++
01a,01jul92,jwt  written.
*/

#ifndef __INCussSun4h
#define __INCussSun4h

#ifdef __cplusplus
extern "C" {
#endif

/*
DESCRIPTION
This module provides the hooks to allow the US Software GOFAST SPARClite 
floating-point library to be assembled on a Sun-4 Unix workstation.
*/

#if 0
#define	scan	!scan
#define	/*	.macro
#define	*/	.endm
#define	/* */	.end
#define	.global	.extern
#endif

#define	NOP	.word	0x01000000
#define	SCAN	.word	0x81602000
#define	DIVSCC	.word	0x80e80000
#define	UMUL	.word	0x80500000

#define	gl0	0
#define	gl1	1
#define	gl2	2
#define	gl3	3
#define	gl4	4
#define	gl5	5
#define	gl6	6
#define	gl7	7

#define	out0	8
#define	out1	9
#define	out2	10
#define	out3	11
#define	out4	12
#define	out5	13
#define	SP	14
#define	out7	15

#define	lo0	16
#define	lo1	17
#define	lo2	18
#define	lo3	19
#define	lo4	20
#define	lo5	21
#define	lo6	22
#define	lo7	23

#define	in0	24
#define	in1	25
#define	in2	26
#define	in3	27
#define	in4	28
#define	in5	29
#define	FP	30
#define	in7	31

#ifdef __cplusplus
}
#endif

#endif /* __INCussSun4h */
