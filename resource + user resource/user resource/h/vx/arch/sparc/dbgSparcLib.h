/* dbgSparcLib.h - SPARC debugger header */

/* Copyright 1984-1996 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,30dec97,dbt  modified for new breakpoint scheme
01d,15mar96,dat  changed RD to DR, name conflict with streams pkg
01c,05aug93,jwt  removed DBG_SAVE structure - not used; copyright 1993.
01b,22sep92,rrr  added support for c++
01a,05may92,yao  written based on sparc/dbgLib.c.
*/

#ifndef __INCdbgSparcLibh
#define __INCdbgSparcLibh

#ifdef __cplusplus
extern "C" {
#endif

//#include "dsmLib.h"

typedef struct breakEsf
    {
    INSTR * pc;		/* program counter */
    INSTR * npc;	/* next program counter */
    UINT   psr;		/* processor status registerr */
    } BREAK_ESF;

#define DBG_NO_SINGLE_STEP	1	/* no trace support */
#define DBG_TRAP_NUM		3
#define DBG_BREAK_INST		(TRAP0_INST + (DBG_TRAP_NUM & 0x7f))

#define	TRACE_ESF	BREAK_ESF		/* in case needed */
#define INST_CALL       OP_1
#define INST_CALL_MASK  OP
#define JMPL_o7         (OP_2 + RD_o7 + OP3_38)
#define JMPL_o7_MASK    (OP + DR + OP3)

/* opcode for trap 0 */

#define TRAP0_INST      (OP_2 + RD_g0 + COND_8 + OP3_3A + RS1_g0 + I_1)

/* stack definitions from trcLib.c */

#define L0_OFFSET       0x00    /* offset from base of stack, in ints */
#define L7_OFFSET       0x07    /* offset from base of stack, in ints */
#define I0_OFFSET       0x08    /* offset from base of stack, in ints */
#define I6_OFFSET       0x0e    /* offset from base of stack, in ints */
#define I7_OFFSET       0x0f    /* offset from base of stack, in ints */

#define I6_CONTENTS(sp) ((int *)*(sp + I6_OFFSET))
						/* from current stack frame */
#define I7_CONTENTS(sp) ((INSTR *)*(sp + I7_OFFSET))
						/* from current stack frame */
#ifdef __cplusplus
}
#endif

#endif /* __INCdbgSparcLibh */
