/* mmuSun4Lib.h - header file for Sun4 (sun1e) mmuLib */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,30jul93,jwt  changed SUN types to SUN4 types for sun.h/sun4.h conflict.
01c,27sep92,kdl  added prototype for mmuSunLibInit().
01b,25sep92,kdl  changed name from mmu1eLib.h to mmuSun4Lib.h.
01a,22sep92,rdc  written.
*/

#ifndef __INCmmuSun4Libh
#define __INCmmuSun4Libh

#ifdef __cplusplus
extern "C" {
#endif

#define MMU_STATE_MASK_VALID		0x80000000
#define MMU_STATE_MASK_WRITABLE		0x40000000
#define MMU_STATE_MASK_CACHEABLE	0x10000000
#define MMU_STATE_MASK_DEVICE_SPACE     0x0c000000

#define MMU_STATE_VALID			0x80000000
#define MMU_STATE_VALID_NOT		0

#define MMU_STATE_WRITABLE		0x40000000
#define MMU_STATE_WRITABLE_NOT		0

#define MMU_STATE_CACHEABLE		0
#define MMU_STATE_CACHEABLE_NOT		0x10000000

#define MMU_STATE_DS_MEM		SUN4_MEM
#define MMU_STATE_DS_IO			SUN4_IO
#define MMU_STATE_DS_VME_D16		SUN4_VME16
#define MMU_STATE_DS_VME_D32		SUN4_VME32

#define NUM_PAGE_BLOCKS 4096
#define PAGE_BLOCK_SIZE 1048576

#define NUM_CONTEXTS 8
#define NUM_SEGMENTS 4096
#define SEG_SIZE 262144
#define NUM_PMEGS 256
#define NUM_PTES_PER_PMEG 32

typedef struct
    {
    UINT context;
    LIST privatePmegList;
    } MMU_TRANS_TBL;


/* function prototypes */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS mmuSun4LibInit (int pageSize);

#else   /* __STDC__ */

extern STATUS mmuSun4LibInit ();

#endif  /* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCmmuSun4Libh */
