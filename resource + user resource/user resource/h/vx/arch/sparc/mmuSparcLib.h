/* mmuSparcLib.h - mmuLib header for SPARC. */

/* Copyright 1984-1993 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,16aug93,ccc  submitted to wise.
01b,12may93,ccc  changed paths to standard.
01a,02apr93,edm  written.
    26apr93,ccc  SPECIAL for sunec.
*/

#ifndef __INCmmuSparcLibh
#define __INCmmuSparcLibh

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Include necessary utilities .h files
 */

#include "lstLib.h"
#include "memLib.h"

/*
 * Include Address Space Identifiers (ASIs) for MMU
 */

#include "sparcASI.h"

/*
 * System Control Register (SCR)
 */

#define MMU_SCR_VA                      0x00000000

#define MMU_SCR_IMPL_MASK               0xf0000000
#define MMU_SCR_VER_MASK                0x0f000000

/* Boot Mode                          */
/* Not in SPARC Reference MMU Specification, but in all implementations */
#define MMU_SCR_BM_MASK                 0x00004000
#define MMU_SCR_BM_BOOT                 0x00004000
#define MMU_SCR_BM_BOOT_NOT             0x00000000
#define MMU_SCR_BM_NORMAL               MMU_SCR_BM_BOOT_NOT

/* MMU Enable                         */
#define MMU_SCR_ME_MASK                 0x00000001
#define MMU_SCR_ME_MMU_ENABLE           0x00000001
#define MMU_SCR_ME_MMU_ENABLE_NOT       0x00000000
#define MMU_SCR_ME_MMU_DISABLE          MMU_SCR_ME_MMU_ENABLE_NOT

/*
 * Context Table Pointer Register (CTPR)
 */

#define MMU_CTPR_VA                     0x00000100

#define MMU_CTPR_CTP_MASK               0xfffffffc

/*
 * Context Register (CXR)
 */

#define MMU_CXR_VA                      0x00000200

#define MMU_CXR_CXN_MASK                0xffffffff

/*
 * Synchronous Fault Status Register (SFSR)
 */

#define MMU_SFSR_VA                     0x00000300

/* Level                              */
#define MMU_SFSR_L_MASK                 0x00000300

/* Access Type                        */
#define MMU_SFSR_AT_MASK                0x000000e0

/* Fault Type                         */
#define MMU_SFSR_FT_MASK                0x0000001c

/* Fault Address Valid                */
#define MMU_SFSR_FAV_MASK               0x00000002

/* Over Write                         */
#define MMU_SFSR_OW_MASK                0x00000001

/*
 * Synchronous Fault Address Register (SFAR)
 */

#define MMU_SFAR_VA                     0x00000400

/* Synchronous Fault Address          */
#define MMU_SFAR_SFA_MASK               0xffffffff

/*
 * Page Table Pointer (PTP)
 */

/* Page Table Pointer                 */
#define MMU_PTP_PTP_MASK                0xfffffff0
#define MMU_PTP_PTP_SHIFT               4

/* Entry Type                         */
/* Overloaded with PTE_ET field below */
/* See MMU_PT_ET_* below for values   */

/*
 * Page Table Entry (PTE)
 */

/* Physical Page Number               */
#define MMU_PTE_PPN_MASK                0xffffff00
#define MMU_PTE_PPN_35_32_MASK          0xf0000000
#define MMU_PTE_PPN_35_32_SHIFT         28
#define MMU_PTE_PPN_31_0_MASK           0x0fffff00
#define MMU_PTE_PPN_SHIFT               8

/* Cacheable bit                      */
#define MMU_PTE_C_MASK                  0x00000080

/* Modified bit                       */
#define MMU_PTE_M_MASK                  0x00000040

/* Referenced bit                     */
#define MMU_PTE_R_MASK                  0x00000020

/* Access Protection bits             */
#define MMU_PTE_ACC_MASK                0x0000001c
#define MMU_PTE_ACC_USER_R_SUPR_R       0x00000000
#define MMU_PTE_ACC_USER_RW_SUPR_RW     0x00000004
#define MMU_PTE_ACC_USER_RX_SUPR_RX     0x00000008
#define MMU_PTE_ACC_USER_RWX_SUPR_RWX   0x0000000c
#define MMU_PTE_ACC_USER_X_SUPR_X       0x00000010
#define MMU_PTE_ACC_USER_R_SUPR_RW      0x00000014
#define MMU_PTE_ACC_USER_NONE_SUPR_RX   0x00000018
#define MMU_PTE_ACC_USER_NONE_SUPR_RWX  0x0000001c

/* Entry Type                         */
/* Overloaded with PTP_ET field above */
/* See MMU_PT_ET_* below for values   */

#define MMU_PT_ET_MASK                  0x00000003
#define MMU_PT_ET_INVALID               0x00000000
#define MMU_PT_ET_PTR                   0x00000001
#define MMU_PT_ET_DESC                  0x00000002

/*
 * Page Table Pointers (PTPs) and Page Table Entries (PTEs) are the
 * two "interpretations", or "variants", of the elements of each of
 * the Region, Segment, and Page Tables.  A new data type, PTV, must
 * be declared to be the C-equivalent of either representation.  This
 * new data type is equivalent to a 32-bit Unsigned Integer (UINT).
 */

#ifndef _ASMLANGUAGE
typedef unsigned int                    PTV;
#endif /* ~ _ASMLANGUAGE */
#define sizeofPTV                       4

#define MMU_STATE_MASK_VALID            MMU_PT_ET_MASK
#define MMU_STATE_MASK_WRITABLE         MMU_PTE_ACC_MASK
#define MMU_STATE_MASK_CACHEABLE        MMU_PTE_C_MASK

#define MMU_STATE_VALID                 MMU_PT_ET_DESC
#define MMU_STATE_VALID_NOT             MMU_PT_ET_INVALID

#define MMU_STATE_WRITABLE              MMU_PTE_ACC_USER_RWX_SUPR_RWX
#define MMU_STATE_WRITABLE_NOT          MMU_PTE_ACC_USER_RX_SUPR_RX

#define MMU_STATE_CACHEABLE             MMU_STATE_MASK_CACHEABLE
#define MMU_STATE_CACHEABLE_NOT         0

/*
 * Translations (PTEs) are cached in the Translation Lookaside Buffer (TLB).
 * Upon modification of the Page Tables, the TLB must be flushed (invalidated)
 * as the information in (atleast) one of them may be stale.
 */

#define MMU_TLB_FLUSH_MASK              0x00000f00
#define MMU_TLB_FLUSH_PAGE              0x00000000
#define MMU_TLB_FLUSH_SEGMENT           0x00000100
#define MMU_TLB_FLUSH_REGION            0x00000200
#define MMU_TLB_FLUSH_CONTEXT           0x00000300
#define MMU_TLB_FLUSH_ENTIRE            0x00000400

/*
 * Macros to extract table indeces from a virtual address
 */

/* Context Table is indexed by the Context Number */

#define REGION_TABLE_INDEX(virtAddr)    (((UINT) virtAddr & 0xff000000) >> 24)
#define SEGMENT_TABLE_INDEX(virtAddr)   (((UINT) virtAddr & 0x00fc0000) >> 18)
#define PAGE_TABLE_INDEX(virtAddr)      (((UINT) virtAddr & 0x0003f000) >> 12)

/*
 * 256 Segment Table Pointers per Region  Table
 *  64 Page    Table Pointers per Segment Table
 *  64 Page    Table Entries  per Page    Table
 */

#define NUM_REGION_TABLE_DESCRIPTORS    (1<<8)
#define NUM_SEGMENT_TABLE_DESCRIPTORS   (1<<6)
#define NUM_PAGE_TABLE_DESCRIPTORS      (1<<6)

/*
 * Page Size is 4KB - 2^12 Bytes
 */

#define PAGE_SHIFT                      12
#define PAGE_SIZE                       (1<<PAGE_SHIFT)

/*
 * Page Block (Region) Size is 16MB
 */

#define PAGE_BLOCK_SIZE                 (NUM_SEGMENT_TABLE_DESCRIPTORS \
                                         * NUM_PAGE_TABLE_DESCRIPTORS \
                                         * PAGE_SIZE)

/*
 * Global Context will always be context number ZERO (0)
 */

#define GLOBAL_CONTEXT                  0

#ifndef _ASMLANGUAGE

/*
 * MMU-specific List Management types
 */

typedef struct
    {
    PTV *head;
    PTV *tail;
    } MMULIST;

/*
 * Context structure
 */

typedef struct mmuTransTblStruct
    {
    NODE    link;
    UINT    context;
    MMULIST freePageTableList;
    MMULIST memPageList;
    } MMU_TRANS_TBL;

/*
 * Exported functions from mmuSparcLib.o and mmuSparcALib.o
 */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS  mmuSparcLibInit (int   pageSize);
extern void    mmuSetPTV       (PTV  *ptvAddress,
                                PTV   ptvValue);
extern UINT    mmuGetSCR       ();
extern UINT    mmuUpdateSCR    (UINT  modifyMask,
                                UINT  modifyValue);
extern PTV    *mmuGetCTPR      ();
extern void    mmuSetCTPR      (PTV  *newCTPR);
extern UINT    mmuGetCXN       ();
extern void    mmuSetCXN       (UINT  newCXN);

#else   /* __STDC__ */

extern STATUS  mmuSparcLibInit ();
extern void    mmuSetPTV       ();
extern UINT    mmuGetSCR       ();
extern UINT    mmuUpdateSCR    ();
extern PTV    *mmuGetCTPR      ();
extern void    mmuSetCTPR      ();
extern UINT    mmuGetCXN       ();
extern void    mmuSetCXN       ();

#endif  /* __STDC__ */
#endif /* ~ _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmmuSparcLibh */
