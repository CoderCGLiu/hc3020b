/* sigSparcCodes.h - Sparc codes sent to signal handlers */

/* Copyright 1984-1992 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,22sep92,rrr  added support for c++
01a,31aug92,rrr  written
*/

#ifndef __INCsigSparcCodesh
#define __INCsigSparcCodesh

#ifdef __cplusplus
extern "C" {
#endif

#include "iv.h"

#define BUS_INSTR_ACCESS	TT_INSTR_ACCESS_EXCPTN
#define ILL_ILLINSTR_FAULT	TT_ILLEGAL_INSTR
#define ILL_PRIVINSTR_FAULT	TT_PRIVILEGED_INSTR
#define FPE_FPA_ENABLE		TT_FP_DISABLED
#define BUS_ALIGN		TT_MEM_ADDR_NOT_ALIGNED
#define FPE_FPA_ERROR		TT_FP_EXCPTN
#define BUS_DATA_ACCESS		TT_DATA_ACCESS_EXCPTN
#define EMT_TAG			TT_TAG_OVERFLOW
#define ILL_COPROC_DISABLED	TT_CP_DISABLED
#define ILL_COPROC_EXCPTN	TT_CP_EXCPTN
#define FPE_INTDIV_TRAP		TT_USER (2)
#define ILL_TRAP_FAULT(n)	TT_USER (n)		/* trap n fault */

#ifdef __cplusplus
}
#endif

#endif /* __INCsigSparcCodesh */
