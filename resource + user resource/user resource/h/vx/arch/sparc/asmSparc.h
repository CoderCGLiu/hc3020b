/* asmSparc.h - SPARC assembler definitions header file */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
01f,22sep92,rrr  added support for c++
01e,08jul92,jwt  changed asmSPARC.h to asmSparc.h.
01d,26may92,rrr  the tree shuffle
01c,04oct91,rrr  passed through the ansification filter
		  -changed copyright notice
01b,05oct90,shl  added copyright notice.
                 made #endif ANSI style.
01a,07may90,gae  written.
*/

#ifndef __INCasmSparch
#define __INCasmSparch

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCasmSparch */
