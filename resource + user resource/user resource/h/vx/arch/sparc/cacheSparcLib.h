/* cacheSparcLib.h - SPARC cache library header file */

/* Copyright 1984-1992 Wind River Systems, Inc. */
/*
modification history
--------------------
01e,22sep92,rrr  added support for c++
01d,06jul92,jwt  converted cacheSPARCLib to cacheSparcLib.
01c,16jun92,jwt  made safe for assembly language files.
01b,26may92,rrr  the tree shuffle
01a,09jan92,jwt  created from h/cacheLib.h.
*/

#ifndef __INCcacheSparcLibh
#define __INCcacheSparcLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

#define SPARC_CACHE_NULL		0
#define SPARC_CACHE_RESET		1
#define SPARC_CACHE_ENABLE		2
#define SPARC_CACHE_DISABLE		3
#define SPARC_CACHE_BURST_ENABLE	4
#define SPARC_CACHE_BURST_DISABLE	5
#define SPARC_CACHE_FREEZE		6
#define SPARC_CACHE_UNFREEZE		7
#define SPARC_CACHE_FLUSH		8
#define SPARC_CACHE_INVALIDATE		9
#define SPARC_CACHE_CLEAR		10
#define SPARC_CACHE_CLEAR_ENTRY		11
#define SPARC_CACHE_CLEAR_LINE		12
#define SPARC_CACHE_CLEAR_PAGE		13
#define SPARC_CACHE_CLEAR_SEGMENT	14
#define SPARC_CACHE_CLEAR_CONTEXT	15

#ifndef	_ASMLANGUAGE
extern	FUNCPTR	sysCacheLibInit;
extern	STATUS	cacheClearEntry (CACHE_TYPE cache, int entry);
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCcacheSparcLibh */
