/* archArm.h - ARM specific header */

/* Copyright 1996-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,13mar01,sn   SPR 73723 - define supported toolchains
01c,20jan99,cdp  removed support for old ARM libraries.
01b,17aug98,cdp  added big-endian and multiple cachelib support.
01a,08may96,cdp  written based on archI86.h
*/

#ifndef __INCarchArmh
#define __INCarchArmh

#ifdef __cplusplus
extern "C" {
#endif

#define _ARCH_SUPPORTS_GCC
#define _ARCH_SUPPORTS_DCC

#if defined(ARMEB) || defined(__ARMEB__)
#define	_BYTE_ORDER		_BIG_ENDIAN
#else
#define	_BYTE_ORDER		_LITTLE_ENDIAN
#endif

#define	_DYNAMIC_BUS_SIZING	FALSE		/* require alignment for swap */

#define _ARCH_MULTIPLE_CACHELIB	TRUE		/* multiple cache libraries */

#include "arm.h"

#ifdef __cplusplus
}
#endif

#endif /* __INCarchArmh */
