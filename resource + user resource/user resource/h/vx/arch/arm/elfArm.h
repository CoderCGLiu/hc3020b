/* elfArm.h - ARM-specific elf loader header */

/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,30nov01,pad  Moved definitions of CHECK_FITS and SIGN_EXTEND into
                 loadElfLibP.h
01a,24aug01,jn   created, based on vxWorksAE 1.1 
                 share/src/loader/elfArm.h@@/main/tor3_x/5
*/ 

#ifndef __INCelfArmh
#define __INCelfArmh

#ifdef __cplusplus
extern "C" {
#endif

/* macros to insert the low n bits of a value into a buffer. */

#define LOW11_INSERT(buf,value)	INSERT ((buf), (value), 0x000007ff)

#define LOW8_INSERT(buf,value)	INSERT ((buf), (value), 0x000000ff)

#define EM_ARCH_MACHINE		EM_ARM
#define EM_ARCH_MACH_ALT	EM_ARM    	/* not used */

/*
 * Relocation Type Definitions
 *
 */

#define R_ARM_NONE           0  /* No reloc */
#define R_ARM_PC24           1
#define R_ARM_ABS32          2
#define R_ARM_REL32          3
#define R_ARM_PC13           4
#define R_ARM_ABS16          5
#define R_ARM_ABS12          6
#define R_ARM_THM_ABS5       7
#define R_ARM_ABS8           8
#define R_ARM_SBREL32        9
#define R_ARM_THM_PC22      10
#define R_ARM_THM_PC8       11
#define R_ARM_AMP_VCALL9    12
#define R_ARM_SWI24         13
#define R_ARM_THM_SWI8      14
#define R_ARM_XPC25         15
#define R_ARM_THM_XPC22     16
#define R_ARM_COPY          20  /* copy symbol at runtime */
#define R_ARM_GLOB_DAT      21  /* create GOT entry */
#define R_ARM_JUMP_SLOT     22  /* create PLT entry */
#define R_ARM_RELATIVE      23  /* adjust by program base */
#define R_ARM_GOTOFF        24  /* 32 bit offset to GOT */
#define R_ARM_GOTPC         25  /* 32 bit PC relative offset to GOT */
#define R_ARM_GOT32         26  /* 32 bit GOT entry */
#define R_ARM_PLT32         27  /* 32 bit PLT address */

/* These are GNU extensions to enable C++ vtable garbage collection.  */

#define R_ARM_GNU_VTENTRY   100
#define R_ARM_GNU_VTINHERIT 101

#define R_ARM_THM_PC11      102       /* cygnus extension to abi: thumb unconditional branch */
#define R_ARM_THM_PC9       103       /* cygnus extension to abi: thumb conditional branch */

#define R_ARM_RXPC25        249
#define R_ARM_RSBREL32      250
#define R_ARM_THM_RPC22     251
#define R_ARM_RREL32        252
#define R_ARM_RABS32        253
#define R_ARM_RPC24         254
#define R_ARM_RBASE         255

#ifdef __cplusplus
}
#endif

#endif /* __INCelfArmh */
