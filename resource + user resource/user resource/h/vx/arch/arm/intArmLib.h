/* intArmLib.h - ARM-specific interrupt library header file */

/* Copyright 1997-1998 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,20aug98,cdp  added intIFLock(), intIFUnlock().
01a,25nov97,cdp  created.
*/

#ifndef __INCintArmLibh
#define __INCintArmLibh

#ifdef __cplusplus
extern "C" {
#endif

/* interrupt model feature selection bits (OR'd together) */

#define INT_NON_PREEMPT_MODEL 0
#define INT_PREEMPT_MODEL 1
#define INT_HDWE_LOCK_LVL 2


/* functions in intArchLib.c */

extern STATUS intDisable (int);
extern STATUS intEnable (int);
extern STATUS intLibInit (int, int, int);
extern int    intIFLock (void);
extern void   intIFUnlock (int);


/* function pointers used in intArchLib.c and interrupt driver */
 
IMPORT FUNCPTR sysIntLvlVecChkRtn;
IMPORT FUNCPTR sysIntLvlVecAckRtn;
IMPORT FUNCPTR sysIntLvlChgRtn;
IMPORT FUNCPTR sysIntLvlEnableRtn;
IMPORT FUNCPTR sysIntLvlDisableRtn;

#ifdef __cplusplus
}
#endif

#endif /* __INCintArmLibh */
