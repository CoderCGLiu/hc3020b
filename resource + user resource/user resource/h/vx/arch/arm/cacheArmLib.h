/* cacheArmLib.h - ARM cache library header file */

/* Copyright 1996-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01j,02oct01,jpd  added definitions to support simplifications in source.
01i,12nov01,to   added cacheArchIntMask (merged from AE)
01h,23jul01,scm  change XScale name to conform to coding standards...
01g,11dec00,scm  replace references to ARMSA2 with XScale
01f,01sep00,scm  add sa2 support...
01e,07sep99,jpd  added ARM920T support.
01d,24nov98,jpd  added ARM940T, ARMSA1100, ARMSA1500 support;
	    cdp  restructured for multiple cache/MMU libraries.
01c,09mar98,jpd  tidied.
01b,16sep97,jpd  added cacheIMBRange(), 810 definitions for cacheDClear() and
		 cacheArchIntLock().
01a,09may96,cdp  created
*/

/*
 * N.B. although this library contains code written for the ARM810
 * CPU, at the time of writing, this code has not been fully tested on
 * that CPU. YOU HAVE BEEN WARNED.
 */


#ifndef	__INCcacheArmLibh
#define	__INCcacheArmLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE
/*
 * The way in which _CACHE_ALIGN_SIZE is defined has been changed
 * somewhat and is no longer strictly related to the CPU type defined,
 * but is instead related to the cache type selected.  Therefore, it would
 * be better if drivers that need to know the cache line size, use this
 * variable, rather than the constant.  The variable will be initialised
 * to the appropriate cache-type-dependent value by the variant of the
 * cache library initialisation code.
 */

IMPORT UINT32	cacheArchAlignSize;

/*
 * Variable used to hold the interrupt mask used when disabling
 * interrupts for lengthy cache operations such as cache flushing.  This
 * is preinitialised to a mask that disables IRQs and FIQs.  The BSP may
 * change this.  Do not do so unless you are sure that you understand the
 * consequences.  In particular, if interrupts are allowed to occur
 * during these operations, the state of the cache afterwards may be
 * indeterminate.
 */

IMPORT UINT32	cacheArchIntMask;


/* routine that locks interrupts according to the mask above */

IMPORT UINT32	cacheArchIntLock (void);
#endif	/* _ASMLANGUAGE */

/*
 * Some cache designs (e.g. Harvard) have D-cache either fully or
 * partially not coherent with I-cache
 */

#if ((ARMCACHE == ARMCACHE_810)    || (ARMCACHE == ARMCACHE_SA110)  || \
     (ARMCACHE == ARMCACHE_SA1100) || (ARMCACHE == ARMCACHE_SA1500) || \
     (ARMCACHE == ARMCACHE_920T)   || (ARMCACHE == ARMCACHE_940T)   || \
     (ARMCACHE == ARMCACHE_946E)   || (ARMCACHE == ARMCACHE_XSCALE))

#define ARMCACHE_NOT_COHERENT TRUE

#elif ((ARMCACHE == ARMCACHE_NONE) || (ARMCACHE == ARMCACHE_710A) || \
       (ARMCACHE == ARMCACHE_720T) || (ARMCACHE == ARMCACHE_740T))

#define ARMCACHE_NOT_COHERENT FALSE

#else
#error ARMCACHE value not supported in definition of ARMCACHE_NOT_COHERENT
#endif

/*
 * We used only to allow writethrough on 710A, 720T, 740T and XScale. In fact,
 * 810, 920T, 940T and 946E allow pages to be marked as writethough, even
 * though the mode of the cache cannot be set globally.
 *
 * For time being, do not change behaviour on 920T, 940T.
 */

#if ((ARMCACHE == ARMCACHE_710A) || (ARMCACHE == ARMCACHE_720T) || \
     (ARMCACHE == ARMCACHE_740T) || (ARMCACHE == ARMCACHE_946E) || \
     (ARMCACHE == ARMCACHE_XSCALE))

#define ARMCACHE_HAS_WRITETHROUGH TRUE

#elif ((ARMCACHE == ARMCACHE_NONE)   || (ARMCACHE == ARMCACHE_810)    || \
       (ARMCACHE == ARMCACHE_SA110)  || (ARMCACHE == ARMCACHE_SA1100) || \
       (ARMCACHE == ARMCACHE_SA1500) || (ARMCACHE == ARMCACHE_920T)   || \
       (ARMCACHE == ARMCACHE_940T))

#define ARMCACHE_HAS_WRITETHROUGH FALSE

#else
#error ARMCACHE value not supported in definition of ARMCACHE_HAS_WRITETHROUGH
#endif

#if ((ARMCACHE == ARMCACHE_SA1100) || (ARMCACHE == ARMCACHE_SA1500) || \
     (ARMCACHE == ARMCACHE_XSCALE))
#define ARMCACHE_HAS_MINICACHE TRUE
#else
#define ARMCACHE_HAS_MINICACHE FALSE
#endif

#if (ARMCACHE == ARMCACHE_810)

#define ARMCACHE_NEEDS_IMB TRUE

#elif ((ARMCACHE == ARMCACHE_NONE)   || (ARMCACHE == ARMCACHE_710A)   || \
       (ARMCACHE == ARMCACHE_720T)   || (ARMCACHE == ARMCACHE_740T)   || \
       (ARMCACHE == ARMCACHE_SA110)  || (ARMCACHE == ARMCACHE_SA1100) || \
       (ARMCACHE == ARMCACHE_SA1500) || (ARMCACHE == ARMCACHE_920T)   || \
       (ARMCACHE == ARMCACHE_940T)   || (ARMCACHE == ARMCACHE_946E)   || \
       (ARMCACHE == ARMCACHE_XSCALE))

#define ARMCACHE_NEEDS_IMB FALSE

#else
#error ARMCACHE value not supported in definition of ARMCACHE_NEEDS_IMB
#endif

#if	(ARMCACHE == ARMCACHE_710A)
#include "cacheArm710aLib.h"
#elif	(ARMCACHE == ARMCACHE_720T)
#include "cacheArm720tLib.h"
#elif	(ARMCACHE == ARMCACHE_740T)
#include "cacheArm740tLib.h"
#elif	(ARMCACHE == ARMCACHE_810)
#include "cacheArm810Lib.h"
#elif	(ARMCACHE == ARMCACHE_SA110)
#include "cacheArmSA110Lib.h"
#elif	(ARMCACHE == ARMCACHE_SA1100)
#include "cacheArmSA1100Lib.h"
#elif	(ARMCACHE == ARMCACHE_SA1500)
#include "cacheArmSA1500Lib.h"
#elif	(ARMCACHE == ARMCACHE_920T)
#include "cacheArm920tLib.h"
#elif	(ARMCACHE == ARMCACHE_940T)
#include "cacheArm940tLib.h"
#elif	(ARMCACHE == ARMCACHE_946E)
#include "cacheArm946eLib.h"
#elif	(ARMCACHE == ARMCACHE_XSCALE)
#include "cacheArmXSCALELib.h"
#endif	/* ARMCACHE == ARMCACHE_710A */


#ifdef __cplusplus
}
#endif

#endif	/* __INCcacheArmLibh */
