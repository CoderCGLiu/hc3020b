/* esfArm.h - ARM exception stack frames */

/* Copyright 1996-1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,09may96,cdp  created
*/

#ifndef	__INCesfArmh
#define	__INCesfArmh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

/* The Exception Stack Frame
 * On the ARM, this is faked by a stub and the pointer
 * passed back points to the PC and CPSR at the end of the
 * register structure. If that changes, so must this.
 */

typedef struct
    {
    INSTR *pc;		/* program counter */
    ULONG cpsr;		/* current PSR */
    UINT32 vecAddr;	/* address of exception vector => type */
    } ESF;

#endif	/* _ASMLANGUAGE */


#ifdef __cplusplus
}
#endif

#endif	/* __INCesfArmh */
