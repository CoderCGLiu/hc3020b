/* mmuArm920tLib.h - ARM 920T MMU library header file */

/* Copyright 1998-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,04feb99,jpd  created.
*/

#ifndef	__INCmmuArm920tLibh
#define	__INCmmuArm920tLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

IMPORT	void	mmuArm920tLibInstall (void * (* physToVirt) (void * addr),
				      void * (* virtToPhys) (void * addr));
IMPORT	void	mmuArm920tTtbrSet (LEVEL_1_DESC *);
IMPORT	void	mmuArm920tDacrSet (UINT32 dacrVal);
IMPORT	void	mmuArm920tTLBIDFlushEntry (void *addr);
IMPORT	void	mmuArm920tTLBIDFlushAll (void);
IMPORT	void	mmuArm920tAEnable (UINT32 cacheState);
IMPORT	void	mmuArm920tADisable (void);

#endif	/* _ASMLANGUAGE */


#ifdef __cplusplus
}
#endif

#endif	/* __INCmmuArm920tLibh */
