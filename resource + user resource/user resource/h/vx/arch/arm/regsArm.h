/* regsArm.h - ARM registers */

/* Copyright 1996-1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,15apr98,kkk	 added reg_pc, reg_sp, and reg_fp.
01a,08may96,cdp  written, based on I86 version
*/

#ifndef	__INCregsArmh
#define	__INCregsArmh

#ifdef __cplusplus
extern "C" {
#endif

#define GREG_NUM	15	/* has 15 32-bit general registers */

#ifndef	_ASMLANGUAGE

/* REG_SET - ARM Register set
 * Note that the exception stack frame relies on the order of items
 * in this so don't change this without changing the ESF and the stub
 * in excALib which creates it.
 */
	
#if 0 //lijian
typedef struct			/* REG_SET - ARM register set */
    {
    ULONG r[GREG_NUM];		/* general purpose registers 0-14 */
    INSTR *pc;			/* program counter */
    ULONG cpsr;			/* current PSR */
    } REG_SET;
#endif
    
#define fpReg		r[11]	/* frame pointer */
#define spReg		r[13]	/* stack pointer */
#define reg_pc		pc	/* program counter */
#define reg_sp		spReg	/* stack pointer */
#define reg_fp		fpReg	/* frame pointer */

#endif	/* _ASMLANGUAGE */

#define	ARM_REG_SIZE	4
#define REG_SET_G_REG_BASE	0x00	/* data reg's base offset to REG_SET */
#define REG_SET_G_REG_OFFSET(n)	(REG_SET_G_REG_BASE + (n)*ARM_REG_SIZE)
#define REG_SET_PC_OFFSET	(REG_SET_G_REG_OFFSET(GREG_NUM))
#define REG_SET_CPSR_OFFSET	(REG_SET_PC_OFFSET + ARM_REG_SIZE)

#define PC_OFFSET		REG_SET_PC_OFFSET


#ifdef __cplusplus
}
#endif

#endif	/* __INCregsArmh */
