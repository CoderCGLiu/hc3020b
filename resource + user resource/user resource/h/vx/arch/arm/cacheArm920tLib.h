/* cacheArm920tLib.h - ARM 920T cache library header file */

/* Copyright 1998-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,04feb99,jpd  created
*/

#ifndef	__INCcacheArm920tLibh
#define	__INCcacheArm920tLibh

#ifdef __cplusplus
extern "C" {
#endif

#define D_CACHE_SIZE	(16*1024) /* 16 kbytes DCache */
#define I_CACHE_SIZE	(16*1024) /* 16 kbytes ICache */


#ifndef _ASMLANGUAGE

IMPORT	void	cacheArm920tLibInstall (void * (* physToVirt) (void * addr),
					void * (* virtToPhys) (void * addr));
IMPORT	void	cacheArm920tDClearDisable (void);
IMPORT	void	cacheArm920tDInvalidate (void * addr);
IMPORT	void	cacheArm920tDInvalidateAll (void);
IMPORT	void	cacheArm920tDFlush (void * addr);
IMPORT	void	cacheArm920tDFlushAll (void);
IMPORT	void	cacheArm920tDClear (void * addr);
IMPORT	void	cacheArm920tDClearAll (void);
IMPORT	void	cacheArm920tIClearDisable (void);
IMPORT	void	cacheArm920tIInvalidate (void * addr);
IMPORT	void	cacheArm920tIInvalidateAll (void);
IMPORT	void	cacheArm920tArchPipeFlush (void);

#endif	/* _ASMLANGUAGE */


#ifdef __cplusplus
}
#endif

#endif	/* __INCcacheArm920tLibh */
