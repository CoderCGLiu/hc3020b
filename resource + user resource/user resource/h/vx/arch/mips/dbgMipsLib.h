/* dbgMipsLib.h - MIPS debugger header */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
01j,16jul01,ros  add CofE comment
01i,08sep99,myz  added a few macros for CW4000_16  
01h,17mar98,dbt  added definition of DBG_CRET (no cret() support).
01g,30dec97,dbt  modified for new breakpoint scheme
01f,15jul96,kkk  added R4650 support
01e,27sep93,cd   added R4000 support, including hardware breakpoint.
01d,25jul93,caf  removed definition of DBG_TT.  tt() is now supported.
01c,05aug93,jwt  removed DBG_SAVE structure - not used; copyright 1993.
01b,22sep92,rrr  added support for c++
01a,05may92,yao  written based on sparc/dbgLib.c.
*/

#ifndef __INCdbgMipsLibh
#define __INCdbgMipsLibh

#ifdef __cplusplus
extern "C" {
#endif

//#include "dsmLib.h"
#include "iv.h"
#include "esf.h"

#define DBG_TRAP_NUM		IV_BP_VEC
#define	DBG_BREAK_INST		0x0000000d
#define DBG_NO_SINGLE_STEP	1	/* no single step support */
#define DBG_CRET		FALSE	/* no cret support */


#define	DBG_BREAK_INST		0x0000000d

#if defined(_WRS_MIPS16)

#define DBG_BREAK_INST_16       0xe805   /* mips16 break instruction */
#define DBG_INST_ALIGN          2

#define WDB_NPC_SIZE_GET(pRegs) wdbNpcSizeGet(pRegs)

extern int   wdbNpcSizeGet(REG_SET *);

#endif /* _WRS_MIPS16 */

#define DBG_HARDWARE_BP		1	/* support of hardware breakpoint */

#ifndef	_ASMLANGUAGE
typedef struct
    {
    UINT32	watchLo;	/* watch low register */
    UINT32	watchHi;	/* watch high register */
    UINT32	cause;		/* cause register */
    } DBG_REGS;
/* for R4650 */
#define iWatch watchLo
#define dWatch watchHi

#endif /* _ASMLANGUAGE */

#define BRK_INST	0x0	/* hardware instruction breakpoint */
#define BRK_WRITE	0x1	/* hardware data breakpoint for write */
#define BRK_READ	0x2	/* hardware data breakpoint for read */
#define BRK_RW		0x3	/* hardware data breakpoint for read/write */

#define DEFAULT_HW_BP	BRK_RW	/* default hardware breakpoint */

#define BRK_HARDWARE    0x10	/* hardware breakpoint bit */
#define BRK_HARDMASK    0x03	/* hardware breakpoint mask */

#ifdef __cplusplus
}
#endif

#endif /* __INCdbgMipsLibh */
