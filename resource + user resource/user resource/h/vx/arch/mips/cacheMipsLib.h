/* cacheMipsLib.h - MIPS cache library header file */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
01j,01oct02,jmt  added tx79xx support and made mods for comments from
                 design review
01i,17may02,zmm  Global au1000 name changes. SPR 77333.
01i,14may02,jmt  Added Tx49xx cache library support
01h,13dec01,sru  add prototype for Vr4131 cache lib
01g,16jul01,ros  add CofE comment
01f,09jul01,mem  Added generic MIPS cache op prototypes.
01e,19jan99,dra  added CW4000, CW4011, VR4100, VR5000 and VR5400 support.
01d,15jul96,kkk  added R4650 support
01c,15oct93,cd   added R4000 supoort.
01b,22sep92,rrr  added support for c++
01a,07jul92,ajm  created.
*/

#ifndef __INCcacheMipsLibh
#define __INCcacheMipsLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "cacheLib.h"

#ifndef	_ASMLANGUAGE

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

IMPORT void * cacheMipsMalloc (size_t bytes);
IMPORT STATUS cacheMipsFree (void * pBuf);
IMPORT void * cacheMipsVirtToPhys (void * address);
IMPORT void * cacheMipsPhysToVirt (void * address);
IMPORT STATUS cacheMipsPipeFlush (void);

IMPORT STATUS cache4kcLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			       UINT32 iCacheSize, UINT32 iCacheLineSize,
			       UINT32 dCacheSize, UINT32 dCacheLineSize);

IMPORT STATUS cacheAuLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
				  UINT32 iCacheSize, UINT32 iCacheLineSize,
				  UINT32 dCacheSize, UINT32 dCacheLineSize);

IMPORT STATUS cacheCW400xLibInit (CACHE_MODE instMode, CACHE_MODE dataMode);

IMPORT STATUS cacheCW4011LibInit (CACHE_MODE instMode, CACHE_MODE dataMode);

IMPORT STATUS cacheR32kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode);

IMPORT STATUS cacheR33kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
				UINT32 iCacheSize, UINT32 iCacheLineSize,
				UINT32 dCacheSize, UINT32 dCacheLineSize);

IMPORT STATUS cacheR3kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode);

IMPORT STATUS cacheR4kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			       UINT32 iCacheSize, UINT32 iCacheLineSize,
			       UINT32 dCacheSize, UINT32 dCacheLineSize,
			       UINT32 sCacheSize, UINT32 sCacheLineSize);

IMPORT STATUS cacheR5kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			       UINT32 iCacheSize, UINT32 iCacheLineSize,
			       UINT32 dCacheSize, UINT32 dCacheLineSize,
			       UINT32 sCacheSize, UINT32 sCacheLineSize);

IMPORT STATUS cacheR7kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			       UINT32 iCacheSize, UINT32 iCacheLineSize,
			       UINT32 dCacheSize, UINT32 dCacheLineSize,
			       UINT32 sCacheSize, UINT32 sCacheLineSize,
			       UINT32 tCacheSize, UINT32 tCacheLineSize);

IMPORT STATUS cacheR10kLibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
				UINT32 iCacheSize, UINT32 iCacheLineSize,
				UINT32 dCacheSize, UINT32 dCacheLineSize,
				UINT32 sCacheSize, UINT32 sCacheLineSize);

IMPORT STATUS cacheTx49LibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			        UINT32 iCacheSize, UINT32 iCacheLineSize,
			        UINT32 dCacheSize, UINT32 dCacheLineSize);

IMPORT STATUS cacheTx79LibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			        UINT32 iCacheSize, UINT32 iCacheLineSize,
			        UINT32 dCacheSize, UINT32 dCacheLineSize);

IMPORT STATUS cacheVr4131LibInit (CACHE_MODE instMode, CACHE_MODE dataMode,
			          UINT32 iCacheSize, UINT32 iCacheLineSize,
			          UINT32 dCacheSize, UINT32 dCacheLineSize,
			          UINT32 sCacheSize, UINT32 sCacheLineSize);


#else	/* __STDC__ */

IMPORT void * cacheMipsMalloc ();
IMPORT STATUS cacheMipsFree ();
IMPORT void * cacheMipsVirtToPhys ();
IMPORT void * cacheMipsPhysToVirt ();
IMPORT STATUS cacheMipsPipeFlush ();

IMPORT STATUS cache4kcLibInit ();
IMPORT STATUS cacheAuLibInit ();
IMPORT STATUS cacheCW400xLibInit ();
IMPORT STATUS cacheCW4011LibInit ();
IMPORT STATUS cacheR32kLibInit ();
IMPORT STATUS cacheR33kLibInit ();
IMPORT STATUS cacheR3kLibInit ();
IMPORT STATUS cacheR4kLibInit ();
IMPORT STATUS cacheR5kLibInit ();
IMPORT STATUS cacheR7kLibInit ();
IMPORT STATUS cacheR10kLibInit ();
IMPORT STATUS cacheTx49LibInit ();
IMPORT STATUS cacheTx79LibInit ();
IMPORT STATUS cacheVr4131LibInit ();

#endif	/* __STDC__ */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCcacheMipsLibh */
