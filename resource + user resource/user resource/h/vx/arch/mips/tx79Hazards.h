/* tx79Hazards.h - assembler definitions header file */

/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
01a,11nov02,jmt  Modified to handle TX79 hazards
*/

/* These hazard macros are intended for use with MIPS architecture-
 * dependent assembly files which require handling hazards.
 * These macros support the Toshiba TX79 core
 * This file is included by the asmMips.h file when the HAZARD_FILE macro
 * is defined using
 * #define HAZARD_FILE "tx79Hazards.h"
 */

#ifndef __INCtx79Hazardsh
#define __INCtx79Hazardsh

#ifdef	__cplusplus
extern "C" {
#endif

/* Hazard macros */

#define SYNC_P           .word 0x0000040F
#define HAZARD_TLB       SYNC_P
#define HAZARD_ERET      SYNC_P
#define HAZARD_CP_READ   CPU_CYCLES_ONE
#define HAZARD_CP_WRITE  SYNC_P
#define HAZARD_CACHE_TAG CPU_CYCLES_ONE
#define HAZARD_CACHE     CPU_CYCLES_TWO
#define HAZARD_INTERRUPT SYNC_P

#ifdef	__cplusplus
}
#endif

#endif /* __INCtx79Hazardsh */

