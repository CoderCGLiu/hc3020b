/* excAlphaLib.h - Alpha exception library header file */

/* Copyright 1996-1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,23jul01,scm  change XScale name to conform to coding standards...
01a,11dec00,scm  replace references to ARMSA2 with XScale
01c,16oct00,scm  reverse back I & F bits for SA2
01b,15sep00,scm  update for SA2 support...
01a,09may96,cdp  created
*/

#ifndef	__INCexcAlphaLibh
#define	__INCexcAlphaLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* __INCexcAlphaLibh */



