/* archAlpha.h - alpha architecture specific header */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
01x,03oct02,jmt  Allow for override of _CACHE_ALIGN_SIZE
01w,13mar01,sn   SPR 73723 - define supported toolchains
01v,09jan02,tlc  Remove inappropriate TLB definitions.
01u,04oct01,agf  add TLB definitions and constants
01t,16jul01,ros  add CofE comment
01s,07jun01,mem  Re-introduce optimized kernel.
01r,13feb01,pes  Add support for RM7000 extended interrupts
01q,05jan01,mem  Remove decl of _wdbDbgCtx routines.
01p,04jan01,agf  Adding TLB_ENTRIES define
01o,03jan01,pes  Remove definition of IS_KSEGM. This is not supported in
                 Tornado 2.
01n,20dec00,pes  Merge some definitions from target/h/arch/r4000.h.
01m,12oct99,yvp  Merge from .../tor2_0_0.coretools-baseline branch to make
                 Tor2 code work with T3 main/LATEST compiler
01l,07sep99,myz  added CW4000_16 compiler switch
01k,22jan97,alp  added  CW4000 and CW4010 support.
01l,11nov99,dra  Updated for T3 toolchain.
01k,18oct99,dra  added CW4000, CW4011, VR4100, VR5000 and VR5400 support.
		 added test for both MIPSEB and MIPSEL defined.
01k,00oct00,sn   removed varargs/typedefs for GNUC
01j,14oct96,kkk  added R4650 support.
01i,05aug96,kkk  Changed _ALLOC_ALIGN_SIZE & _STACK_ALIGN_SIZE to 16.
01i,27jun96,kkk  undo 01h.
01h,30apr96,mem  fixed varargs support for R4000, use 64 bit stack slots.
01g,27may95,cd	 switched from MIPSE[BL] to _BYTE_ORDER for ansi compliance
01f,15oct93,cd   added support for R4000, SDE compiler and 64 bit kernel.
01e,22sep92,rrr  added support for c++
01d,07jun92,ajm  added _ARCH_MULTIPLE_CACHES for cache init
01c,03jun92,ajm  updated file name referenced to match real name
01b,26may92,rrr  the tree shuffle
01a,22apr92,yao  written.
*/

#ifndef __INCarchAlphash
#define __INCarchAlphash

#ifdef __cplusplus
extern "C" {
#endif
#ifndef _WRS_SPY_TASK_SIZE
#define _WRS_SPY_TASK_SIZE	10000			/* spyLib.c */
#endif /* _WRS_SPY_TASK_SIZE */

#define _ARCH_SUPPORTS_GCC
#define _ARCH_SUPPORTS_DCC

#define	_ARCH_MULTIPLE_CACHELIB		TRUE
#define _DYNAMIC_BUS_SIZING		FALSE	/* no dynamic bus sizing */
#define	_ALLOC_ALIGN_SIZE		16
#define _STACK_ALIGN_SIZE		16


#define _CACHE_ALIGN_SIZE		128 	/* max cache line size */

#define	_BYTE_ORDER		_LITTLE_ENDIAN

#ifdef __GNUC__
#define _ARCH_LONG_MIN			(-LONG_MAX-1)
#define _ARCH_INT_MIN			(-INT_MAX-1)
#endif


#define _WRS_INT_REGISTER_SIZE		8
#define _WRS_FP_REGISTER_SIZE		8

#ifndef _ASMLANGUAGE
typedef unsigned long long _RType;		/* registers are 64 bits */
#endif


#define _RTypeSize			_WRS_INT_REGISTER_SIZE

#define SR_INT_ENABLE	

/*
 * Cache alignment macros
 *
 * NOTE: These definitions may migrate to vxWorks.h in a future release.
 */
#define	CACHE_ROUND_UP(x)	ROUND_UP(x, _CACHE_ALIGN_SIZE)
#define	CACHE_ROUND_DOWN(x)	ROUND_DOWN(x, _CACHE_ALIGN_SIZE)


#ifdef __cplusplus
}
#endif

#endif /* __INCarchAlphash */
