/*! @file moduleLib.h
 *	@brief VxWorks符号表模块操作接口的头文件
 *
 *	本文件定义了与VxWorks兼容的符号表模块的操作接口，该模块的操作需要开发与运行支持库符号表模块的支持。
 *	
 * @version @reworks_version
 * 
 * @see 无
 */

#ifndef __INCmoduleLibh
#define __INCmoduleLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <symtbl.h>
#include <sysSymbol.h>
#include <vxWorks.h>	

/** @defgroup group_vx_module  VxWorks符号表模块
 * @ingroup group_symtbl_vxworks
 *  该库提供了符号表功能。 
 *   
 * 头文件：
 * #include<moduleLib.h> 
 *  @{ */


/*为了兼容vx*/
#if defined(__HUARUI2__) || defined(__HUARUI3__) //add HR3 2023-03-06
typedef void * 		SYMTAB_ID;	//!<  VxWorks符号表指针
#endif
typedef Symbol 		SYMBOL_ID;	//!<  VxWorks符号指针
typedef SymType 	SYM_TYPE;	//!<  VxWorks符号类型

typedef struct		/* RTN_DESC - routine descriptor */
{
	FUNCPTR	routine;	/* user routine passed to symEach() */
	int		routineArg;	/* user routine arg passed to symEach() */
} RTN_DESC;


/**
 * @brief 获取已加载的moduleId列表
 *	
 * 该接口用于获取已加载的moduleId列表，列表项至多为maxModules个。
 *
 * @param      idList		moduleId列表头指针
 * @param      maxModules	获取moduleId列表项的最大个数
 *
 * @return     返回获取moduleId列表项的个数
 *
 */
 int moduleIdListGet(
     MODULE_ID * idList, /* Array of module IDs to be filled in */
     int maxModules /* Max modules <idList> can accommodate */
  );

 /**
  * @brief 根据module的ID获取模块的名称
  *	
  * 该接口用于根据module的ID号获取指定模块的名称。
  *
  * @param      id			module的ID
  *
  * @return     成功返回指向模块名称的字符串
  * @return		失败返回空指针
  *
  */
 char *moduleNameGet(MODULE_ID id);
 
 /**
  * @brief 根据符号的ID获取符号的名称
  *	
  * 该接口用于根据符号的ID号获取指定符号的名称。
  *
  * @param      symbolId	符号的ID
  * @param		pName		指向符号名称字符串的指针
  *
  * @return     OK			成功
  * @return		ERROR		失败
  *
  */
STATUS symNameGet(SYMBOL_ID  symbolId, char **    pName);

/**
 * @brief 根据符号的ID获取符号的地址
 *	
 * 该接口用于根据符号的ID号获取指定符号的地址。
 *
 * @param      symbolId		符号的ID
 * @param	   pValue		指向符号地址的指针
 *
 * @return     OK			成功
 * @return	   ERROR		失败
 *
 */
STATUS symValueGet (SYMBOL_ID  symbolId, void **    pValue);

/**
 * @brief 根据符号的ID获取符号的类型
 *	
 * 该接口用于根据符号的ID号获取指定符号的类型。
 *
 * @param      symbolId		符号的ID
 * @param	   pType		指向符号类型的指针
 *
 * @return     OK			成功
 * @return	   ERROR		失败
 *
 */
STATUS symTypeGet (SYMBOL_ID  symbolId, SYM_TYPE * pType);


/**
 * @brief 确认模块ID是否有效
 *	
 * 该接口用于确认指定的模块ID是否有效。
 *
 * @param      moduleid		模块的ID
 *
 * @return     OK			模块存在
 * @return	   ERROR		模块不存在
 *
 */
STATUS moduleIdVerify(MODULE_ID	moduleid);

/**
 * @brief 获取模块信息
 *	
 * 该接口用于根据模块ID获取指定模块的相关信息。
 *
 * @param      moduleid		模块的ID
 * @param	   pModuleInfo	指向模块信息的指针
 *
 * @return     OK			成功
 * @return	   ERROR		失败
 *
 */
STATUS moduleInfoGet(MODULE_ID	  moduleid,	MODULE_INFO * pModuleInfo);


/**
 * @brief 获取符合用户筛选条件的第一个符号指针
 * 
 * 该接口用于扫描整张符号表，根据用户定义的筛选条件，筛选出符合条件的符号。
 * 
 * @param 	symTblId 		全局符号表链表的头指针
 * @param 	routine			用户定义的筛选符号的接口
 * @param 	routineArg		用户定义接口需要使用的参数
 * 
 * @return 成功返回符合用户筛选条件的符号指针
 * @return 失败返回空指针
 * 
 * @attention routine接口参数及返回值格式如下：
 * @attention BOOL routine
 * @attention (
 * @attention char *		name,		符号的名称
 * @attention int 			val,		符号的值
 * @attention SYM_TYPE		type,		符号的类型
 * @attention int			arg,		用户指定的routineArg参数
 * @attention int	        group,		符号对应的MODULE_ID		
 * @attention SYMBOL_ID		pSymbol		指向符号的指针
 * @attention );
 * @attention 当routine条件不成立，即返回FALSE时，扫描终止，返回该符号的指针。
 */
SYMBOL_ID symEach(SYMTAB_ID   symTblId,       /* pointer to symbol table */
    FUNCPTR     routine,        /* func to call for each tbl entry */
    int         routineArg      /* arbitrary user-supplied arg */
    );

/** @example example_symEach.c
 * 下面的例子演示了如何使用 moduleIdListGet(), moduleNameGet(), symValueGet(), symNameGet(), 
 * symTypeGet(), moduleIdVerify(), moduleInfoGet(), symEach().
 * 
 */

/*  @}*/
#ifdef __cplusplus
}
#endif

#endif /* __INCmoduleLibh */


