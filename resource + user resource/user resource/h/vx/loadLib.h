
#ifndef __INCloadLibh
#define __INCloadLibh

#ifdef __cplusplus
extern "C" {
#endif

#if 0

#include "moduleLib.h"

#define LOAD_NO_SYMBOLS	        2
#define LOAD_LOCAL_SYMBOLS	4
#define LOAD_GLOBAL_SYMBOLS	8
#define LOAD_ALL_SYMBOLS	(LOAD_LOCAL_SYMBOLS | LOAD_GLOBAL_SYMBOLS)

extern MODULE_ID loadModule (int fd, int symFlag);
extern MODULE_ID loadModuleByName (char *file_name, int symFlag);
extern MODULE_ID loadModuleAt (int fd, int symFlag, char **ppText,
			       char **ppData, char **ppBss);

#endif /* #if 0 */

#ifdef __cplusplus
}
#endif

#endif /* __INCloadLibh */

