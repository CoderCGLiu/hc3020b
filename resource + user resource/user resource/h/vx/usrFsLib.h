#ifndef __INCusrFsLibh
#define __INCusrFsLibh

#ifdef __cplusplus
extern "C" {
#endif


#include <vxWorks.h>
#include <sys/types.h>


/* usrLib status codes */

#define S_usrLib_NOT_ENOUGH_ARGS	(M_usrLib | 1)


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

/* usrFsLib.c functions below this comment */
extern STATUS copyStreams (int inFd, int outFd);
extern STATUS diskFormat (const char *devName);
extern STATUS diskInit (const char *devName);
extern STATUS chkdsk (const char * pDevName, u_int repairLevel, u_int verbose);
extern STATUS ll (char * dirName);
extern STATUS lsr (char * dirName);
extern STATUS llr (char * dirName);
extern STATUS xcopy (const char * src, const char *dest);
extern STATUS xdelete (const char * src);
extern STATUS attrib (const char * fileName, const char * attr);
extern STATUS xattrib (const char * fileName, const char * attr);
extern void ioHelp (void);
extern STATUS dirList (int fd, char * dirName, BOOL doLong, BOOL doTree);
/* usrFsLib.c functions above this comment */

#else

/* usrFsLib.c functions below this comment */
extern STATUS copyStreams ();
extern STATUS diskFormat ();
extern STATUS diskInit ();
extern STATUS chkdsk ();
extern STATUS ll ();
extern STATUS lsr  ();
extern STATUS llr ();
extern STATUS xcopy  ();
extern STATUS xdelete  ();
extern STATUS attrib  ();
extern STATUS xattrib  ();
extern void ioHelp (void);
extern STATUS dirList  ();
/* usrFsLib.c functions above this comment */

#endif	/* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCusrLibh */
