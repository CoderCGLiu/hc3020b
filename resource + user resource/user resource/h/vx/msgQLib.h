#ifndef msgQLib_h
#define msgQLib_h
#include <vxWorks.h>
#include <vwModNum.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
//typedef  unsigned int  	msg_q;           /*xujun ------ 2003.10.27*/
typedef size_t  msg_q;
typedef msg_q MSG_Q;
typedef msg_q *      MSG_Q_ID;

/* generic status codes */

#define S_msgQLib_INVALID_MSG_LENGTH		(M_msgQLib | 1)
#define S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL	(M_msgQLib | 2)
#define S_msgQLib_INVALID_QUEUE_TYPE		(M_msgQLib | 3)

#define MSG_PRI_NORMAL	0	/* normal priority message */
#define MSG_PRI_URGENT	1	/* urgent priority message */


/* message queue options */
#define MSG_Q_TYPE_MASK	0x01	/* mask for pend queue type in options */
#define MSG_Q_FIFO		0x00	/* tasks wait in FIFO order */
#define MSG_Q_PRIORITY	0x01	/* tasks wait in PRIORITY order */
#define MSG_Q_EVENTSEND_ERR_NOTIFY 0x02 /* notify when eventRsrcSend fails,but for reworks eventRsrcSend is not implented */


#define ERROR (-1)
#define WAIT_FOREVER (-1)

MSG_Q_ID msgQCreate(
int maxMsgs,
int maxMsgLength,
int options
);

STATUS msgQDelete(
MSG_Q_ID msgQId
);

int msgQNumMsgs(
MSG_Q_ID msgQId
);

STATUS  _msgQSend (
MSG_Q_ID msgQId, 
char *buffer, 
UINT nBytes,
int timeout, 
int pri
);

STATUS  msgQSend (
MSG_Q_ID msgQId, 
char *buffer, 
UINT nBytes,
int timeout, 
int pri
);

int msgQReceive(
MSG_Q_ID msgQId,
 char *buffer, 
 UINT maxNBytes,
 int timeout
 );
 
extern STATUS 	msgQShow (MSG_Q_ID msgQId, int level);

void msgQModuleInit();

#ifdef __cplusplus
}
#endif




#endif



