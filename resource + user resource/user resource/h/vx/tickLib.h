#ifndef _TICKLIB_H_
#define _TICKLIB_H_
#include <vxWorks.h>

#ifdef __cplusplus
extern "C" {
#endif

extern ULONG tickGet ();
extern void tickSet (ULONG);
extern void tickAnnounce ();

#ifdef __cplusplus
}
#endif
#endif
