#ifndef wdLib_h
#define wdLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <vxWorks.h>

typedef size_t WDOG_ID;

extern void wdModuleInit();
extern WDOG_ID 	wdCreate (void);
extern STATUS 	wdDelete(WDOG_ID wdId);
extern STATUS 	wdStart(WDOG_ID wdId, _Vx_ticks_t delay,FUNCPTR pRoutine, _Vx_usr_arg_t parameter);
extern STATUS 	wdCancel(WDOG_ID wdId);

#ifdef __cplusplus
}
#endif
#endif
