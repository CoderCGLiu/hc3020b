/* memPartLib.h - memory partition management library header file */

/* Copyright 2001-2003 Wind River Systems, Inc. */
/*
modification history
--------------------
01b,18feb03,vvv  added include for stdlib.h
01a,25sep01,tam  created from AE 1.1 memPartLib.h.
*/

#ifndef __INCmemPartLibh
#define __INCmemPartLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "stdlib.h"
#include "memLib.h"

#ifdef __cplusplus
}
#endif

#endif /* __INCmemPartLibh */
