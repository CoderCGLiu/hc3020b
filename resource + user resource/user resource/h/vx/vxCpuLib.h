/* vxCpuLib.h - runtime identification header file */

/* 
 * Copyright (c) 2006, 2009 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 


/*
modification history
--------------------
01c,02feb09,cww  Add dynamic CPU reservation API
01b,25oct06,lei  moved macros to private/vxCpuLibP.h
01a,05jul06,mmi  Created based on ../i86/vxCpuLib.h
*/

#ifndef __INCvxCpuLibh
#define __INCvxCpuLibh

#include <vwModNum.h>
#include <cpuset.h>

#ifdef __cplusplus
extern "C" {
#endif

//#define S_vxCpuLib_INVALID_ARGUMENT	(M_vxCpuLib | 1)
//#define S_vxCpuLib_NO_CPU_AVAILABLE	(M_vxCpuLib | 2)

/* function declarations */
extern unsigned int vxCpuIdGet (void);
extern cpuset_t     vxCpuEnabledGet (void);
extern unsigned int vxCpuConfiguredGet (void);
//extern STATUS       vxCpuReservedGet (cpuset_t*);
//extern STATUS	    vxCpuReserve (cpuset_t, cpuset_t*);
//extern STATUS	    vxCpuUnreserve (cpuset_t);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxCpuLibh */
