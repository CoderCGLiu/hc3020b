/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了单向链表头文件
 * 修改：
 * 		 2012-06-07，唐立三，建立 
 * 
 */
#ifndef __INCsllLibh
#define __INCsllLibh

#ifdef __cplusplus
extern "C" {
#endif


/**
 * 单向链表节点定义
 */
typedef struct slnode	
{
    struct slnode 		*next;	
} SL_NODE;


/**
 * 单向链表头
 */
typedef struct	
{
    SL_NODE 			*head;	
    SL_NODE 			*tail;	
} SL_LIST;

/**
 * 返回单向链表中的第一个节点
 */
#define SLL_FIRST(pList)			\
    (								\
    (((SL_LIST *)pList)->head)		\
    )

/**
 * 返回单向链表中的最后一个节点
 */
#define SLL_LAST(pList)				\
    (								\
    (((SL_LIST *)pList)->tail)		\
    )

/**
 * 返回当前节点的下一个节点
 */
#define SLL_NEXT(pNode)				\
    (								\
    (((SL_NODE *)pNode)->next)		\
    )

/**
 * 检查链表是否为空
 */
#define SLL_EMPTY(pList)			\
    (						\
    (((SL_LIST *)pList)->head == NULL)		\
    )



extern SL_LIST *sll_create (void);
extern SL_NODE *sll_each (SL_LIST *pList, FUNCPTR routine, int routineArg);
extern SL_NODE *sll_get (SL_LIST *pList);
extern SL_NODE *sll_previous (SL_LIST *pList, SL_NODE *pNode);
extern STATUS 	sll_delete (SL_LIST *pList);
extern STATUS 	sll_init (SL_LIST *pList);
extern STATUS 	sll_terminate (SL_LIST *pList);
extern int 		sll_count (SL_LIST *pList);
extern void 	sll_put_at_head (SL_LIST *pList, SL_NODE *pNode);
extern void 	sll_put_at_tail (SL_LIST *pList, SL_NODE *pNode);
extern void 	sll_remove (SL_LIST *pList, SL_NODE *pDeleteNode, SL_NODE *pPrevNode);

#ifdef __cplusplus
}
#endif

#endif 
