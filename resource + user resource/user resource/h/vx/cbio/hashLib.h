/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为MS DOSFS HASH头文件
 * 修改：
 * 		 2012-06-07，唐立三，建立
 * 
 */
#ifndef hashLib_h
#define hashLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <vxMacro.h>
#include "vwModNum.h"
#include "sllLib.h"
#include "classLib.h"
#include "objLibP.h"


/**
 * HASH表定义
 */
typedef struct hashtbl		
{
    OBJ_CORE		objCore;			/* 对象管理 */
    int				elements;			/* 表内元素数量 */
    FUNCPTR			keyCmpRtn;			/* 比较函数 */
    FUNCPTR			keyRtn;				/* HASH函数 */
    int				keyArg;				/* HASH函数参数 */
    SL_LIST			*pHashTbl;			/* HASH表数组 */
}HASH_TBL;

typedef SL_NODE HASH_NODE;		
    
typedef HASH_TBL *HASH_ID;

/**
 * HASH节点定义
 */
typedef struct	
{
    HASH_NODE		node;			/* HASH节点链 */
    int				key;			/* HASH节点键值 */
    int				data;			/* HASH节点数据 */
} H_NODE_INT;

/**
 * HASH节点字符串
 */
typedef struct		
{
    HASH_NODE		node;			/* 链接点 */
    char			*string;		/* HASH节点键值 */
    int				data;			/* HASH节点数据 */
} H_NODE_STRING;

/**
 * 创建HASH表
 */
extern HASH_ID 		hash_table_create (int sizeLog2, FUNCPTR keyCmpRtn,
				       FUNCPTR keyRtn, int keyArg);

/**
 * 销毁HASH表
 */
extern STATUS 		hash_table_destroy (HASH_ID hashId, BOOL dealloc);

/**
 * 初始化HASH表
 */
extern STATUS 		hash_table_init (HASH_TBL *pHashTbl, SL_LIST *pTblMem,
				     int sizeLog2, FUNCPTR keyCmpRtn,
				     FUNCPTR keyRtn, int keyArg);

#ifdef __cplusplus
}
#endif

#endif
