
#ifndef __INCpathLibh
#define __INCpathLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"

extern STATUS 	pathBuild (char ** nameArray, char ** nameArrayEnd, char *destString);
extern STATUS 	pathCat (char *dirName, char *fileName, char *result);
extern char *	pathLastNamePtr (char *pathName);
extern void 	pathCondense (char *pathName);
extern void 	pathLastName (char *pathName, char ** pLastName);
extern void 	pathParse (char *longName, char ** nameArray, char *nameBuf);
extern void 	pathSplit (char *fullFileName, char *dirName, char *fileName);

#ifdef __cplusplus
}
#endif

#endif
