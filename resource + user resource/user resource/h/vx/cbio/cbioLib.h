/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为CBIO模块头文件
 * 修改：
 * 		 2012-05-26，唐立三，建立
 * 
 */
#ifndef __INCcbioLibh
#define __INCcbioLibh

#ifdef __cplusplus
extern "C" {
#endif


#include "vxMacro.h"	
#include <reworksio.h>
#include <driver.h>
#include <fs.h>
#include <sys/types.h>

typedef	unsigned long cookie_t ;		

/**
 * 定义CBIO句柄
 */
typedef struct cbioDev *CBIO_DEV_ID;

/**
 * CBIO读写方向
 */
typedef enum                            
{
	CBIO_READ = 0, 			/* read */
	CBIO_WRITE = 1,			/* copy back write */
	CBIO_WRITE_THROUGH = 2		/* write through write */
}CBIO_RW;   
    

/**
 * 定义CBIO参数
 */
typedef struct cbioparams	
{
    BOOL			cbioRemovable;	/* 可移除标识 */
    block_t 		nBlocks;		/* 块数*/
    size_t 			bytesPerBlk;	/* 每块字节数 */
    size_t			blockOffset;	/* 偏移 */
    short			blocksPerTrack;	/* 磁道块数 */
    short			nHeads;			/* 磁头数 */
    short			retryCnt;		/* 错误重试次数 */
    block_t 		lastErrBlk;		/* 最后错误发生的块号 */
    int 			lastErrno;		/* 最后错误的错误号 */
}CBIO_PARAMS;


/**
 * 库初始化接口
 */
extern int buffer_cache_init(void);

/**
 * CBIO块读写
 */
extern	int buffer_cache_block_rw(CBIO_DEV_ID	dev,
								  block_t		startBlock,
								  block_t		numBlocks,
								  addr_t		buffer,
								  CBIO_RW		rw,
								  cookie_t *	pCookie);


/**
 * CBIO字节读写 
 */
extern int buffer_cache_bytes_rw(CBIO_DEV_ID 	dev,
								 block_t		startBlock,
								 off_t			offset,
								 addr_t			buffer,
								 size_t			nBytes,
								 CBIO_RW		rw,
								 cookie_t *		pCookie);


/**
 * 块到块的拷贝
 */
extern int buffer_cache_block_copy(CBIO_DEV_ID 	dev,
									block_t		srcBlock,
									block_t		dstBlock,
									block_t		numBlocks);


/**
 * ioctl 
 */
extern int buffer_cache_ioctl(CBIO_DEV_ID		dev,
							  int				command,
							  addr_t			arg);


/**
 * 获取模式 - O_RDONLY, O_WRONLY, O_RDWR
 */
extern int buffer_cache_get_mode(CBIO_DEV_ID dev );


/**
 * 设置模式 - O_RDONLY, O_WRONLY, or O_RDWR 
 */
extern int buffer_cache_set_mode(CBIO_DEV_ID dev, int mode);


/**
 * 获取设备准备状态
 */
extern int buffer_cache_get_bit(CBIO_DEV_ID dev);


/**
 * 强制改变准备状态 
 */
extern int buffer_cache_set_bit(CBIO_DEV_ID dev, BOOL status);


/**
 * 获取设备互斥锁 
 */
extern int buffer_cache_lock(CBIO_DEV_ID dev, int timeout);


/**
 * 释放设备互斥锁
 */
extern int buffer_cache_unlock(CBIO_DEV_ID	dev);


/**
 * 获取CBIO参数 
 */
extern int buffer_cache_get_param(CBIO_DEV_ID dev, CBIO_PARAMS *pCbioparams);


/**
 * 显示CBIO信息
 */
extern int buffer_cache_show_info(CBIO_DEV_ID dev);

/**
 * 检查CBIO是否有效 
 */             
extern int buffer_cache_verify_dev(CBIO_DEV_ID device);                                          
                                                
/* 创建封装CBIO */       
extern CBIO_DEV_ID buffer_cache_wrap_block(struct partition_info *partition);                                   


/**
 * CBIO控制命令
 */
#define	CBIO_RESET			0xcb100000	/* 复位设备e */
#define	CBIO_STATUS_CHK		0xcb100001	/* 检查设备状态 */
#define	CBIO_DEVICE_LOCK	0xcb100002	/* 放置设备移除l */
#define	CBIO_DEVICE_UNLOCK	0xcb100003	/* 允许设备移除 */
#define	CBIO_DEVICE_EJECT	0xcb100004	/* 卸载设备e */
#define	CBIO_CACHE_FLUSH	0xcb100010	/* 刷脏Cache */
#define	CBIO_CACHE_INVAL	0xcb100030	/* 刷并使所有Cache无效 */
#define	CBIO_CACHE_NEWBLK	0xcb100050	/* 分配scratch块 */

#define CBIO_DELETE			0xcb100060	/* Delete a Block from underlying layers */

#define CBIO_TRANS_COMMIT	0xcb100070	/* Commit a transaction */
#define CBIO_TRANS_FORMAT	0xcb100071	/* Format a transaction device */
#define CBIO_TRANS_DEFRAG	0xcb100072	/* defrag a transaction device for use as DOS FAT */
#define CBIO_TRANS_ROLLBACK     0xcb100073      /* rollback to last transaction point */
/**
 * CBIO错误号
 */
#define S_cbioLib_INVALID_CBIO_DEV_ID	(M_cbioLib | 1)


#ifdef __cplusplus
}
#endif


#endif
