
#ifndef __INCcbioLibPh
#define __INCcbioLibPh

#ifdef __cplusplus
extern "C" 
{
#endif

#include "vxMacro.h"
#include "cbioLib.h" 
#include "objLibP.h"
#include "classLib.h"
#include <reworksio.h>
#include <pthread.h>
#include <sys/types.h>

#define CBIO_READYCHANGED(x) 	((x)->readyChanged)
#define CBIO_REMOVABLE(x) 	((x)->cbioParams.cbioRemovable)
#define CBIO_MODE(x) 		((x)->cbioMode)

#ifndef CBIO_DEV_EXTRA 
#	define CBIO_DEV_EXTRA void	
#endif

/**
 * 文件系统使用的CBIO名称
 */
#define PART_CBIO		"partcbio"
#define CACHE_CBIO		"cachecbio"
#define WRAP_CBIO		"wrapcbio"
	
/**
 * CBIO接口
 */
typedef struct cbioFuncs	
{
    STATUS	(* cbioDevBlkRW)		
		(CBIO_DEV_ID	dev,
		 block_t	startBlock,
		 block_t	numBlocks,
		 addr_t		buffer,
		 CBIO_RW	rw,
		 cookie_t *     pCookie);
    STATUS	(* cbioDevBytesRW)
		(CBIO_DEV_ID 	dev,
		 block_t	startBlock,
		 off_t		offset,
		 addr_t		buffer,
		 size_t		nBytes,
		 CBIO_RW	rw,
		 cookie_t * 	pCookie);
    STATUS	(* cbioDevBlkCopy)
		(CBIO_DEV_ID 	dev,
		 block_t	srcBlock,
		 block_t	dstBlock,
		 block_t	numBlocks);
    STATUS	(* cbioDevIoctl)	
		(CBIO_DEV_ID	dev,
		 int		command,
		 addr_t		arg);
} CBIO_FUNCS;


/**
 * CBIO设备定义
 */
typedef struct	cbioDev	
{
#ifdef _WRS_DOSFS2_VXWORKS_AE
    HANDLE	cbioHandle; 		
#else
    OBJ_CORE			objCore; 			/* VxWorks 5.x 对象 */
#endif 

    pthread_mutex_t 	cbioMutex;			/* 本层使用的互斥量 */
    struct cbioFuncs 	*pFuncs; 			/* 本层提供的操作接口 */
    char 				*cbioDesc;			/* 文件描述 */
    short				cbioMode;			/* 读写模式 */
    BOOL       			readyChanged;      	/* 设备准备状态 */
    CBIO_PARAMS 		cbioParams;			/* 物理参数 */
    caddr_t				cbioMemBase;		/* 内存池基址 */
    size_t				cbioMemSize;		/* 内存池大小 */
    u_long				cbioPriv0;			/* 依赖于实现 */
    u_long				cbioPriv1;			/* 依赖于实现 */
    u_long				cbioPriv2;			/* 依赖于实现 */
    u_long				cbioPriv3;			/* 依赖于实现 */
    u_long				cbioPriv4;			/* 依赖于实现 */
    u_long				cbioPriv5;			/* 依赖于实现 */
    u_long				cbioPriv6;			/* 依赖于实现 */
    u_long				cbioPriv7;			/* 依赖于实现 */
    CBIO_DEV_EXTRA 		*pDc;				/* 依赖于实现 */
    CBIO_DEV_ID 		cbioSubDev;	       	/* 下一层CBIO  */
    struct block_device	*blkSubDev;	       	/* 设备驱动描述结构 */
    BOOL       			isDriver;	       	/* 是否为驱动 */
    struct partition_info *partition;		/* 分区描述 */				      
} CBIO_DEV;

IMPORT CLASS_ID	buffer_cache_class_id ;

CBIO_DEV_ID buffer_cache_create_dev(caddr_t  ramAddr, size_t	ramSiz);

#ifdef __cplusplus
}
#endif

#endif
