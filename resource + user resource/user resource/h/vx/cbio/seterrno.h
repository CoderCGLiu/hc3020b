/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了错误号设置头文件
 * 修改：
 * 		 2012-06-07，唐立三，建立 
 * 
 */
#ifndef _REWORKS_DOSFS_SETERRNO_H_
#define _REWORKS_DOSFS_SETERRNO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>


#define set_errno_retn(_errno, _retn)	\
	do{ \
		errno = _errno;	\
		return _retn;	\
	while(0);

#ifdef __cplusplus
    }
#endif
#endif
