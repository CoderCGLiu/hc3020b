
#ifndef __INCobjLibh
#define __INCobjLibh

#ifdef __cplusplus
extern "C" 
{
#endif

#include "vxMacro.h"
#include "errno.h"
#include "vwModNum.h"



#define S_objLib_OBJ_ID_ERROR			(M_objLib | 1)
#define S_objLib_OBJ_UNAVAILABLE		(M_objLib | 2)
#define S_objLib_OBJ_DELETED			(M_objLib | 3)
#define S_objLib_OBJ_TIMEOUT			(M_objLib | 4)
#define S_objLib_OBJ_NO_METHOD			(M_objLib | 5)


#ifndef _ASMLANGUAGE


typedef struct obj_core  *OBJ_ID;	


/*******************************************************************************
 * 
 * 
 * 验证指定的对象指针是否有效
 * 
 * 		本宏定义通过比较对象ID与指定的classid来验证指定的对象是否存在。如果对象不存在
 * 则设置相应的错误号。
 * 		如果有效则返回0；否则返回-1
 */
#define OBJ_VERIFY(objId,classId)					 						\
    (                                                                    	\
      (									 									\
      (((OBJ_ID)(objId))->pObjClass == (struct obj_class *)(classId)) || 	\
       (								 									\
       ((OBJ_ID)(objId))->pObjClass &&					 					\
         (((OBJ_ID)(objId))->pObjClass ==                                	\
        (CLASS_ID)(((struct obj_class *)(classId))->initRtn))  		 		\
       )								 									\
      )                                                                  	\
        ?                                                                	\
            OK                                                           	\
        :                                                                	\
            (errno = S_objLib_OBJ_ID_ERROR, ERROR)                       	\
    )



extern STATUS 	objShow (OBJ_ID objId, int showType);


#endif 

#ifdef __cplusplus
}
#endif

#endif 
