

#ifndef __INCdpartCbioh
#define __INCdpartCbioh

#ifdef __cplusplus
extern "C" 
{
#endif

/**
 * 分区表项
 */
typedef struct
{
    u_long			offset ;	
    u_long			nBlocks ;
    int				flags ;	
    int				spare ;	
} PART_TABLE_ENTRY ;

/**
 * 最大分区数
 */
#define	PART_MAX_ENTRIES	24	


IMPORT CBIO_DEV_ID disk_part_create_dev(CBIO_DEV_ID subDev,
										int nPart, 
										FUNCPTR pPartDecodeFunc);

IMPORT CBIO_DEV_ID disk_part_get (CBIO_DEV_ID masterHandle, 
								  int partNum);

#ifdef __cplusplus
}
#endif

#endif 
