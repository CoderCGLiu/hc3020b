

#ifndef __INCclassLibPh
#define __INCclassLibPh

#ifdef __cplusplus
extern "C" 
{
#endif

#ifndef _ASMLANGUAGE
#include "vxMacro.h"
#include "vwModNum.h"
#include "classLib.h"
#include "objLibP.h"


#define OBJ_INST_RTN	/* include redefinition of obj_class instRtn field */

/**
 * ������
 */
typedef struct obj_class	
{
    OBJ_CORE			objCore;		/* object core of class object */
    struct mem_part		*objPartId;		/* memory partition to allocate from */
    unsigned			objSize;		/* size of object */
    unsigned			obj_allocCnt;	/* number of allocated objects */
    unsigned			obj_freeCnt;		/* number of deallocated objects */
    unsigned			objInitCnt;		/* number of initialized objects */
    unsigned			objTerminateCnt;/* number of terminiated objects */
    int					coreOffset;		/* offset from object start to objCore*/
    FUNCPTR				createRtn;		/* object creation routine */
    FUNCPTR				initRtn;		/* object initialization routine */
    FUNCPTR				destroyRtn;		/* object destroy routine */
    FUNCPTR				showRtn;		/* object show routine */
    FUNCPTR				instRtn;		/* object inst routine */
} OBJ_CLASS;




extern CLASS_ID	class_create (unsigned objectSize, int coreOffset,
			     FUNCPTR createRtn, FUNCPTR initRtn,
			     FUNCPTR destroyRtn);
extern STATUS	classInit (OBJ_CLASS *pObjClass, unsigned objectSize,
				   int coreOffset, FUNCPTR createRtn, FUNCPTR initRtn,
				   FUNCPTR destroyRtn);
extern STATUS	class_destroy (CLASS_ID classId);
extern STATUS	class_inst_connect (CLASS_ID classId, FUNCPTR instRtn);
extern STATUS	class_show_connect (CLASS_ID classId, FUNCPTR showRtn);
extern STATUS   class_instrument ( OBJ_CLASS * pObjClass, 
				  OBJ_CLASS * pObjInstClass );



#else   /* _ASMLANGUAGE */

#if (CPU_FAMILY == ARM)
#define OBJ_CLASS_initRtn	0x24	/* Offsets into OBJ_CLASS */
#endif

#endif  /* _ASMLANGUAGE */
   

#ifdef __cplusplus
}
#endif

#endif /* __INCclassLibPh */

