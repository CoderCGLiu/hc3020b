/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义DOSFS错误号设置接口
 * 修改：
 * 		 2012-06-07，唐立三，建立 
 * 
 */
#ifndef __INCerrnoLibh
#define __INCerrnoLibh

#ifdef __cplusplus
extern "C" 
{
#endif

#include "vxMacro.h"
#include "errno.h"


//#define S_errnoLib_NO_STAT_SYM_TBL      (M_errnoLib | 1)


extern STATUS   errnoOfTaskSet (pthread_t taskId, int errorValue);
extern int   	errno_set (int errorValue);
extern int      errno_get (void);
extern int      errnoOfTaskGet (pthread_t taskId);


#ifdef __cplusplus
}
#endif

#endif 
