/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了双向链表头文件
 * 修改：
 * 		 2012-06-07，唐立三，建立 
 * 
 */
#ifndef __INCdllLibh
#define __INCdllLibh

#ifdef __cplusplus
extern "C" 
{
#endif


/**
 * 节点定义
 */
typedef struct dlnode		
{
    struct dlnode 			*next;	
    struct dlnode 			*previous;	
} DL_NODE;

/**
 * 链表头
 */
typedef struct
{
    DL_NODE 				*head;
    DL_NODE 				*tail;
} DL_LIST;

/**
 * 获取链表中第一个节点
 */
#define DLL_FIRST(pList)			\
    (								\
    (((DL_LIST *)(pList))->head)	\
    )

/**
 * 获取链表中最后一个节点
 */
#define DLL_LAST(pList)				\
    (								\
    (((DL_LIST *)(pList))->tail)	\
    )

/**
 * 获取指定节点的下一个节点
 */
#define DLL_NEXT(pNode)				\
    (								\
    (((DL_NODE *)(pNode))->next)	\
    )

/**
 * 获取指定节点的上一个节点
 */
#define DLL_PREVIOUS(pNode)				\
    (									\
    (((DL_NODE *)(pNode))->previous)	\
    )

/**
 * 检查指定链表是否为空
 */
#define DLL_EMPTY(pList)					\
    (										\
    (((DL_LIST *)pList)->head == NULL)		\
    )



extern DL_LIST *dllCreate (void);
extern DL_NODE *dllEach (DL_LIST *pList, FUNCPTR routine, int routineArg);
extern DL_NODE *dllGet (DL_LIST *pList);
extern STATUS 	dllDelete (DL_LIST *pList);
extern STATUS 	dllInit (DL_LIST *pList);
extern STATUS 	dllTerminate (DL_LIST *pList);
extern int 		dllCount (DL_LIST *pList);
extern void 	dllAdd (DL_LIST *pList, DL_NODE *pNode);
extern void 	dllInsert (DL_LIST *pList, DL_NODE *pPrev, DL_NODE *pNode);
extern void 	dllRemove (DL_LIST *pList, DL_NODE *pNode);


#ifdef __cplusplus
}
#endif

#endif 
