/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义Cache层CBIO头文件
 * 修改：
 * 		 2012-06-07，唐立三，建立 
 * 
 */

#ifndef __INCdcacheCbioh
#define __INCdcacheCbioh

#ifdef __cplusplus
extern "C" 
{
#endif

/**
 * 最大支持的CACHE CBIO，即缓冲CBIO控制块个数
 */
#define	DCACHE_MAX_DEVS	    16	



IMPORT CBIO_DEV_ID dentry_cache_dev_create(CBIO_DEV_ID  subDev, 
										   struct partition_info *partition,
										   char  *   pRamAddr,  
										   int       memSize,
										   char *    pDesc);

IMPORT STATUS dentry_cache_dev_tune (CBIO_DEV_ID dev,
									int		dirtyMax,
									int		bypassCount,
									int		readAhead,
									int		syncInterval) ;

IMPORT void dentry_cache_info_show (CBIO_DEV_ID dev,
									int verbose) ;

IMPORT STATUS dentry_cache_mem_resize(CBIO_DEV_ID dev,	
									  size_t	newSize) ;

IMPORT STATUS dentry_cache_dev_enable(CBIO_DEV_ID dev);

IMPORT STATUS dentry_cache_dev_disable(CBIO_DEV_ID dev);

#ifdef __cplusplus
}
#endif

#endif
