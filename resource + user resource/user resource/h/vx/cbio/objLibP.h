
#ifndef __INCobjLibPh
#define __INCobjLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxMacro.h"
#include "classLib.h"


#ifndef _ASMLANGUAGE

#if CPU_FAMILY==I960
#pragma align 1			/* 通知gcc960不进行优化对齐 */
#endif	


typedef struct obj_core		
{
    struct obj_class *pObjClass;	/* 对象类指针 */
} OBJ_CORE;

#if CPU_FAMILY==I960
#pragma align 0			/* 关闭对齐 */
#endif	



extern OBJ_ID 	objCreate (CLASS_ID classId, ...);
extern STATUS 	objInit (CLASS_ID classId, OBJ_ID objId, ...);
extern STATUS 	objDelete (OBJ_ID objId);
extern STATUS 	objDestroy (OBJ_ID objId, BOOL dealloc, int timeout);
extern STATUS 	obj_free (CLASS_ID classId, char *pObject);
extern STATUS 	objTerminate (OBJ_ID objId);
extern void *	obj_alloc (CLASS_ID classId);
extern void *	obj_alloc_extra (CLASS_ID classId, unsigned nExtraBytes,
			       void ** ppExtra);
extern void 	obj_core_init (OBJ_CORE *pObjCore, CLASS_ID pObjClass);
extern void 	obj_core_termiate (OBJ_CORE *pObjCore);



#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCobjLibPh */

