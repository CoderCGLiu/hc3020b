#ifndef __INCvxWorksh
#define __INCvxWorksh

#ifdef __cplusplus
extern "C" {
#endif


#include <reworks/types.h>
#include <cpu.h>
#include "seterrno.h"

	
#ifndef NULL
#	define NULL ((void *)0)
#endif

#if	!defined(EOF) || (EOF!=(-1))
#	define EOF		(-1)
#endif

#if	!defined(FALSE) || (FALSE!=0)
#	define FALSE		0
#endif

#if	!defined(TRUE) || (TRUE!=1)
#	define TRUE		1
#endif


#define NONE			(-1)	/* for times when NULL won't do */
#define EOS				'\0'	/* C�ַ�����ֹ�� */


#define OK				0
#define ERROR			(-1)

#define WAIT_FOREVER	(-1)

#define	STD_IN			0
#define	STD_OUT			1
#define	STD_ERR			2


#define VX_READ			0
#define VX_WRITE		1

#define V7				1	/* ATT version 7 */
#define SYS_V			2	/* ATT System 5 */
#define BSD_4_2			3	/* Berkeley BSD 4.2 */

#define	BUS_TYPE_NONE	    NONE
#define BUS_TYPE_VME	    1
#define BUS_TYPE_MULTIBUS   2
#define BUS_TYPE_PCI	    3

#define VME_BUS		    	BUS_TYPE_VME	/* for backward compat. */
#define MULTI_BUS	    	BUS_TYPE_MULTIBUS

#define	BSDDEBUG	
#define	GATEWAY		

#define MSB(x)				(((x) >> 8) & 0xff)	  /* most signif byte of 2-byte integer */
#define LSB(x)				((x) & 0xff)		  /* least signif byte of 2-byte integer*/
#define MSW(x) 				(((x) >> 16) & 0xffff) /* most signif word of 2-word integer */
#define LSW(x) 				((x) & 0xffff) 		  /* least signif byte of 2-word integer*/


#define WORDSWAP(x) (MSW(x) | (LSW(x) << 16))

#define LLSB(x)	((x) & 0xff)		/* 32bit word byte/word swap macros */
#define LNLSB(x) (((x) >> 8) & 0xff)
#define LNMSB(x) (((x) >> 16) & 0xff)
#define LMSB(x)	 (((x) >> 24) & 0xff)
#define LONGSWAP(x) ((LLSB(x) << 24) | \
		     (LNLSB(x) << 16)| \
		     (LNMSB(x) << 8) | \
		     (LMSB(x)))

#define OFFSET(structure, member)	/* byte offset of member in structure*/\
		((ptrdiff_t) &(((structure *) 0) -> member))

#define MEMBER_SIZE(structure, member)	/* size of a member of a structure */\
		(sizeof (((structure *) 0) -> member))

#define NELEMENTS(array)		/* number of elements in an array */ \
		(sizeof (array) / sizeof ((array) [0]))

#define FOREVER	for (;;)

#define max(x, y)	(((x) < (y)) ? (y) : (x))
#define min(x, y)	(((x) < (y)) ? (x) : (y))



/* storage class specifier definitions */

#define FAST	register
#define IMPORT	extern
#define LOCAL	static


#ifdef _BIG_ENDIAN
#	undef _BIG_ENDIAN
#endif
#define _BIG_ENDIAN			1234

#ifdef _LITTLE_ENDIAN
#	undef _LITTLE_ENDIAN
#endif
#define _LITTLE_ENDIAN		4321

#define _STACK_GROWS_DOWN	(-1)
#define _STACK_GROWS_UP		1
#define I80X86	80	

#define	CPU_FAMILY			I80X86

#ifndef _BYTE_ORDER
#define	_BYTE_ORDER		_BIG_ENDIAN
#endif 


#ifndef	_ALLOC_ALIGN_SIZE
#	define	_ALLOC_ALIGN_SIZE	4	
#endif	


#ifndef	_STACK_ALIGN_SIZE
#define	_STACK_ALIGN_SIZE	4	
#endif	

#ifndef _ASMLANGUAGE

//typedef	char *	addr_t;
typedef	long	swblk_t;


#include "sys/types.h"

#ifdef __cplusplus
//typedef int 		(*FUNCPTR) (...);     /* ptr to function returning int */
//typedef void 		(*VOIDFUNCPTR) (...); /* ptr to function returning void */
typedef double 		(*DBLFUNCPTR) (...);  /* ptr to function returning double*/
typedef float 		(*FLTFUNCPTR) (...);  /* ptr to function returning float */
#else
//typedef int 		(*FUNCPTR) ();	   /* ptr to function returning int */
//typedef void 		(*VOIDFUNCPTR) (); /* ptr to function returning void */
typedef double 		(*DBLFUNCPTR) ();  /* ptr to function returning double*/
typedef float 		(*FLTFUNCPTR) ();  /* ptr to function returning float */
#endif			

typedef	int			BOOL;
typedef	int			STATUS;
typedef int 		ARGINT;

typedef void		VOID;

#endif	

#define ROUND_UP(x, align)		(((int) (x) + (align - 1)) & ~(align - 1))
#define ROUND_DOWN(x, align)	((int)(x) & ~(align - 1))
#define ALIGNED(x, align)		(((int)(x) & (align - 1)) == 0)

#define MEM_ROUND_UP(x)			ROUND_UP(x, _ALLOC_ALIGN_SIZE)
#define MEM_ROUND_DOWN(x)		ROUND_DOWN(x, _ALLOC_ALIGN_SIZE)
#define STACK_ROUND_UP(x)		ROUND_UP(x, _STACK_ALIGN_SIZE)
#define STACK_ROUND_DOWN(x)		ROUND_DOWN(x, _STACK_ALIGN_SIZE)
#define MEM_ALIGNED(x)			ALIGNED(x, _ALLOC_ALIGN_SIZE)

#include <memory.h>
#include "vwModNum.h"
#define KHEAP_ALLOC(nBytes)                                             \
        malloc((nBytes))
#define KHEAP_FREE(pChar)                                               \
        free((pChar))


#ifndef TOOL_FAMILY
#   define TOOL_FAMILY gnu
#endif


#define TOOL_HDR_STRINGIFY(x)  #x
#define TOOL_HDR(tc, file) TOOL_HDR_STRINGIFY(tool/tc/file)
#define TOOL_HEADER(file) TOOL_HDR(TOOL_FAMILY,file)


#define _WRS_VXWORKS_5_X


#ifndef __P
#if defined(__STDC__) || defined(__cplusplus)
#	define	__P(protos)	protos		/* ʹ��ANSI Cԭ�� */
#else
#	define	__P(protos)     ()
#endif
#endif


#ifndef _ASMLANGUAGE
#if defined(__STDC__) || defined(__cplusplus)

extern int print_Err(const char *  fmt,  ...);

#else 
extern int	printErr ();
extern void 	bfill ();
#endif
#endif


#ifdef __cplusplus
}
#endif

#endif

