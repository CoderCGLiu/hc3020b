/* vxAtomicLib.h - header file for atomic operators */

/*
 * Copyright (c) 2006-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01r,15may09,gls  made memory barriers available in user space
01r,29apr09,cww  Update atomicXxx APIs with fixed sizing
01q,19mar09,gls  added atomicXXX() inline definitions
01p,19feb09,kk   make the AMP atomic APIs public
01o,01sep08,kk   add AMP atomicXXX() APIs
01n,16jun08,zl   added VX_CODE_BARRIER() definition.
01m,16jan08,gls  added _WRS_INLINE_ macros
01l,08jun07,jmg  Modified for user side
01k,07may07,m_h  Add ARM  to list that support vxAtomicArchLib.h
01j,28mar07,mmi  Removed obsolete memory barrier macros
01f,05dec06,scm  add hook for IA32 memory barrier support...
01e,04dec06,slk  add MIPS to list that support vxAtomicArchLib.h
01d,01dec06,rfr  Changed prototypes to use atomicVal_t
01c,27nov06,dbt  Added VxSim support.
01b,16nov06,mmi Add memory barrier Macros
01a,28sep06,kk  created.
*/

#ifndef __INCvxAtomicLibh
#define __INCvxAtomicLibh

#ifdef __cplusplus
extern "C" {
#endif
#ifndef	_ASMLANGUAGE

#ifdef __multi_core__
#include "atomic.h"
#include "types/vxTypesOld.h"
#else
#include "vxWorks.h"
#include <atomic.h>
#endif

typedef atomic_t	atomicVal_t;

#if 1
typedef atomic_t	atomic32_t;
typedef atomicVal_t	atomic32Val_t;
#endif

#include <cpu.h>

#define VX_MEM_BARRIER_R()	    MEM_BARRIER_R();
#define VX_MEM_BARRIER_W()	    MEM_BARRIER_W();
#define VX_MEM_BARRIER_RW()	    MEM_BARRIER_RW();

/* 
 * The following could be replaced by #include ARCH_HEADER(vxAtomicArchLib.h),
 * but as of now only architectures that support SMP have vxAtomicArchLib.h
 */ 

//#if	CPU_FAMILY == PPC
//#include <arch/ppc/vxAtomicArchLib.h>
//#elif   CPU_FAMILY == I80X86
//#include <arch/i86/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == MIPS
//#include <arch/mips/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == ARM
//#include <arch/arm/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == SIMSPARCSOLARIS
//#include <arch/simsolaris/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == SIMLINUX
//#include <arch/simlinux/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == SIMNT
//#include <arch/simnt/vxAtomicArchLib.h>
//#elif	CPU_FAMILY == SIMPENTIUM
//#include <arch/simpentium/vxAtomicArchLib.h>
//#endif	/* CPU_FAMILY == SIMPENTIUM */


/* definitions */
#ifdef __multi_core__
#define VX_CODE_BARRIER()	_WRS_BARRIER("")
#endif
/* prototypes */

extern atomicVal_t vxAtomicAdd (atomic_t * target, atomicVal_t value); 
extern atomicVal_t vxAtomicAnd (atomic_t * target, atomicVal_t value);
extern atomicVal_t vxAtomicDec (atomic_t * target);
extern atomicVal_t vxAtomicInc (atomic_t * target);
extern atomicVal_t vxAtomicNand (atomic_t * target, atomicVal_t value);
extern atomicVal_t vxAtomicOr (atomic_t * target, atomicVal_t value);
extern atomicVal_t vxAtomicSub (atomic_t * target, atomicVal_t value);
extern atomicVal_t vxAtomicXor (atomic_t * target, atomicVal_t value);

extern atomicVal_t vxAtomicClear (atomic_t * target);
extern atomicVal_t vxAtomicGet (atomic_t * target);
extern atomicVal_t vxAtomicSet (atomic_t * target, atomicVal_t value);

extern BOOL vxCas (atomic_t * target, atomicVal_t oldValue, 
		   atomicVal_t newValue);
extern BOOL vxTas (void * address);
#ifdef __multi_core__
extern atomic32Val_t atomic32Add (atomic32_t * target, atomic32Val_t value); 
extern atomic32Val_t atomic32And (atomic32_t * target, atomic32Val_t value);
extern atomic32Val_t atomic32Dec (atomic32_t * target);
extern atomic32Val_t atomic32Inc (atomic32_t * target);
extern atomic32Val_t atomic32Nand (atomic32_t * target, atomic32Val_t value);
extern atomic32Val_t atomic32Or (atomic32_t * target, atomic32Val_t value);
extern atomic32Val_t atomic32Sub (atomic32_t * target, atomic32Val_t value);
extern atomic32Val_t atomic32Xor (atomic32_t * target, atomic32Val_t value);

extern atomic32Val_t atomic32Clear (atomic32_t * target);
extern atomic32Val_t atomic32Get (atomic32_t * target);
extern atomic32Val_t atomic32Set (atomic32_t * target, atomic32Val_t value);

extern BOOL atomic32Cas (atomic32_t * target, atomic32Val_t oldValue, 
		   atomic32Val_t newValue);

#define _VX_ATOMIC_INIT(i) {(i)}

#ifdef _WRS_KERNEL
/* inline definitions */

#ifndef _WRS_INLINE_vxAtomicAdd
#define vxAtomicAdd_inline vxAtomicAdd
#endif

#ifndef _WRS_INLINE_vxAtomicAnd
#define vxAtomicAnd_inline vxAtomicAnd
#endif

#ifndef _WRS_INLINE_vxAtomicDec
#define vxAtomicDec_inline vxAtomicDec
#endif

#ifndef _WRS_INLINE_vxAtomicInc
#define vxAtomicInc_inline vxAtomicInc
#endif

#ifndef _WRS_INLINE_vxAtomicNand
#define vxAtomicNand_inline vxAtomicNand
#endif

#ifndef _WRS_INLINE_vxAtomicOr
#define vxAtomicOr_inline vxAtomicOr
#endif

#ifndef _WRS_INLINE_vxAtomicSub
#define vxAtomicSub_inline vxAtomicSub
#endif

#ifndef _WRS_INLINE_vxAtomicXor
#define vxAtomicXor_inline vxAtomicXor
#endif

#ifndef _WRS_INLINE_vxAtomicClear
#define vxAtomicClear_inline vxAtomicClear
#endif

#ifndef _WRS_INLINE_vxAtomicGet
#define vxAtomicGet_inline vxAtomicGet
#endif

#ifndef _WRS_INLINE_vxAtomicSet
#define vxAtomicSet_inline vxAtomicSet
#endif

#ifndef _WRS_INLINE_vxCas
#define vxCas_inline vxCas
#endif

#ifndef _WRS_INLINE_atomicAdd
#define atomic32Add_inline atomic32Add
#endif

#ifndef _WRS_INLINE_atomicAnd
#define atomic32And_inline atomic32And
#endif

#ifndef _WRS_INLINE_atomicDec
#define atomic32Dec_inline atomic32Dec
#endif

#ifndef _WRS_INLINE_atomicInc
#define atomic32Inc_inline atomic32Inc
#endif

#ifndef _WRS_INLINE_atomicNand
#define atomic32Nand_inline atomic32Nand
#endif

#ifndef _WRS_INLINE_atomicOr
#define atomic32Or_inline atomic32Or
#endif

#ifndef _WRS_INLINE_atomicSub
#define atomic32Sub_inline atomic32Sub
#endif

#ifndef _WRS_INLINE_atomicXor
#define atomic32Xor_inline atomic32Xor
#endif

#ifndef _WRS_INLINE_atomicClear
#define atomic32Clear_inline atomic32Clear
#endif

#ifndef _WRS_INLINE_atomicGet
#define atomic32Get_inline atomic32Get
#endif

#ifndef _WRS_INLINE_atomicSet
#define atomic32Set_inline atomic32Set
#endif

#ifndef _WRS_INLINE_atomicCas
#define atomic32Cas_inline atomic32Cas
#endif
#endif /* _WRS_KERNEL */

#endif /* _ASMLANGUAGE */
#endif
#ifdef __cplusplus
}
#endif

#endif /* __INCvxAtomicLibh */
