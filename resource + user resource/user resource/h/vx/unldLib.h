

#ifndef __INCunldLibh
#define __INCunldLibh

#ifdef __cplusplus
extern "C" {
#endif

#if 0

#include "moduleLib.h"

#define UNLD_KEEP_BREAKPOINTS	1 /* don't delete breakpoints from unld() */
#define UNLD_SYNC		2 /* already synchronizing a module */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS unld (char *name);
extern STATUS unldByNameAndPath (char *name, char *path, int options);
extern STATUS unldByGroup (UINT16 group, int options);
extern STATUS unldByModuleId (MODULE_ID moduleId, int options);

#else   /* __STDC__ */

extern STATUS unld ();
extern STATUS unldByNameAndPath ();
extern STATUS unldByGroup ();
extern STATUS unldByModuleId ();

#endif /* #if 0 */

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCunldLibh */

