/* vmLib.h - header for architecture independent mmu interface */

/* Copyright 1984-1998 Wind River Systems, Inc. */

/*
modification history
--------------------
01s,26jul01,scm  add extended small page table support for XScale...
01r,13nov98,jpd  added vmMpuLib routine definitions and new error numbers.
01q,09apr98,hdn  added support for Pentium and PentiumPro.
01p,09mar98,jpd  added extra ARM VM_STATEs.
01n,28nov96,jpd  added comment about VM_STATE_CACHEABLE_WRITETHROUGH for
                 ARM710A as well as MC68040.
01m,01jan96,tpr	 changed value of the PPC specific macro.
01l,18sep95,tpr  added VM_STATE_MASK_MEM_COHERENCY, VM_STATE_MASK_GUARDED,
		 VM_STATE_MASK_GUARDED and VM_STATE_MASK_EXECUTE for powerPc.
01k,26jul94,tpr  added VM_STATE_CACHEABLE_NOT_IMPRECISE for MC68060.
01j,09feb93,rdc  added prototypes for vmBaseStateSet and vmBasePageSizeGet;
01i,19oct92,pme  replaced _STDC__ by __STDC__.
01h,19oct92,jcf  cleanup. added vmContextDelete() prototype.
01g,22sep92,rrr  added support for c++
01f,22sep92,rdc  changed prototype for vmGlobalInfoGet.
		 Added prototype for vmPageBlockSizeGet.
		 Added support for sun1e.
01e,30jul92,rdc  added prototype for vmContextShow.
01d,28jul92,rdc  added non-ansi function prototypes and prototypes for 
		 vmBaseLib.c and vmShow.c.
01c,13jul92,rdc  moved private data structures to vmLibP.h.
01b,09jul92,rdc  added VM_LIB_INFO and PHYS_MEM_DESC.
01a,08jul92,rdc  written.
*/

#ifndef __INCvmLibh
#define __INCvmLibh

#include <vxWorks.h>
#include <mmuLib.h>
#include <mmuAttr.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "memLib.h"
#include "lstLib.h"
/*#include "private/objLibP.h"
#include "private/classLibP.h"
#include "private/semLibP.h"*/
#include "semLib.h"
#include "mmuLib.h"
#include "objLib.h"

/* status codes */
#define S_vmLib_NOT_PAGE_ALIGNED		(M_vmLib | 1)
#define S_vmLib_BAD_STATE_PARAM			(M_vmLib | 2)
#define S_vmLib_BAD_MASK_PARAM			(M_vmLib | 3)
#define S_vmLib_ADDR_IN_GLOBAL_SPACE		(M_vmLib | 4)
#define S_vmLib_TEXT_PROTECTION_UNAVAILABLE	(M_vmLib | 5)
#define S_vmLib_KERNEL_TEXT_NOT_ALIGNED		(M_vmLib | 6)
#define S_vmLib_FUNCTION_UNSUPPORTED		(M_vmLib | 7)
#define S_vmLib_PHYS_PAGES_ALREADY_MAPPED	(M_vmLib | 8)
#define S_vmLib_NOALIAS_SUPPORT_NOT_INCLUDED	(M_vmLib | 9)

/* the following data structure in used internally in mmuLib to define
 * the values for the architecture dependent states
 */

typedef struct
    {
    UINT archIndepMask;
    UINT archDepMask;
    UINT archIndepState;
    UINT archDepState;
    } STATE_TRANS_TUPLE;

/* physical memory descriptor is used to map virtual memory in sysLib 
 * and usrConfig.
 */

typedef struct phys_mem_desc
    {
    void *virtualAddr;
    void *physicalAddr;
    UINT len;
    UINT initialStateMask;      /* mask parameter to vmStateSet */
    UINT initialState;          /* state parameter to vmStateSet */
    } PHYS_MEM_DESC;

IMPORT PHYS_MEM_DESC sysPhysMemDesc[];
IMPORT int sysPhysMemDescNumEnt;

typedef struct vm_context
    {
	struct vm_object_record obj;
    MMU_TRANS_TBL_ID mmuTransTbl;   
    SEMAPHORE sem;
    NODE links;
    } VM_CONTEXT;

typedef VM_CONTEXT *VM_CONTEXT_ID;

/* state mask constants for vmStateSet vmStateGet. These
 * constants define the bit fields in the page state. 
 */

#define VM_STATE_MASK_VALID		MMU_ATTR_VALID_MSK
#define VM_STATE_MASK_WRITABLE		MMU_ATTR_PROT_MSK
#define VM_STATE_MASK_CACHEABLE		MMU_ATTR_CACHE_MSK

#define VM_STATE_VALID			MMU_ATTR_VALID
#define VM_STATE_VALID_NOT		MMU_ATTR_VALID_NOT
#define VM_STATE_WRITABLE		MMU_ATTR_SUP_RWX
#define VM_STATE_WRITABLE_NOT		(MMU_ATTR_PROT_SUP_READ | \
					 MMU_ATTR_PROT_SUP_EXE)
#define VM_STATE_CACHEABLE		MMU_ATTR_CACHE_DEFAULT
#define VM_STATE_CACHEABLE_NOT		MMU_ATTR_CACHE_OFF
#define VM_STATE_CACHEABLE_WRITETHROUGH	MMU_ATTR_CACHE_WRITETHRU

/* additional cache states for PowerPC arch. */

#define VM_STATE_MASK_MEM_COHERENCY	MMU_ATTR_CACHE_MSK
#define VM_STATE_MEM_COHERENCY		MMU_ATTR_CACHE_COHERENCY
#define VM_STATE_MEM_COHERENCY_NOT	0

#define VM_STATE_MASK_GUARDED		MMU_ATTR_CACHE_MSK
#define VM_STATE_GUARDED		MMU_ATTR_CACHE_GUARDED
#define VM_STATE_GUARDED_NOT		0

/* additional cache states for i86 arch */

#define VM_STATE_WBACK                  MMU_ATTR_CACHE_COPYBACK
#define VM_STATE_WBACK_NOT              MMU_ATTR_CACHE_WRITETHRU

#define VM_STATE_MASK_WBACK		VM_STATE_MASK_MEM_COHERENCY
#define VM_STATE_MASK_GLOBAL		MMU_ATTR_SPL_MSK
#define VM_STATE_GLOBAL			MMU_ATTR_SPL_0
#define VM_STATE_GLOBAL_NOT		0

#if (CPU_FAMILY == I80X86 && _VX_CPU != _VX_PENTIUM)
/* Write Combine attribute */

#define VM_STATE_MASK_WCOMBINE          MMU_ATTR_SPL_MSK
#define VM_STATE_WCOMBINE               MMU_ATTR_SPL_1
#define VM_STATE_WCOMBINE_NOT           0
#endif /* (CPU_FAMILY == I80X86 && _VX_CPU != _VX_PENTIUM) */

/* additional cache states for ARM/XScale arch. */

#if (CPU == ARMARCH6)

#define VM_STATE_MASK_BUFFERABLE	(MMU_ATTR_CACHE_MSK)
#define VM_STATE_BUFFERABLE		(MMU_ATTR_CACHE_GUARDED | \
                           		 MMU_ATTR_CACHE_OFF)  /* Cache I/O */
#define VM_STATE_BUFFERABLE_NOT		0x00

#else

#define VM_STATE_MASK_BUFFERABLE	(MMU_ATTR_CACHE_MSK | MMU_ATTR_SPL_MSK)
#define VM_STATE_BUFFERABLE		(MMU_ATTR_SPL_0)
#define VM_STATE_BUFFERABLE_NOT		0x00

#endif /* CPU == ARMARCH6 */

#if (CPU == XSCALE)

#define VM_STATE_MASK_EX_CACHEABLE      (MMU_ATTR_CACHE_MSK | MMU_ATTR_SPL_MSK)
#define VM_STATE_NO_COALESCE		(MMU_ATTR_SPL_2 | MMU_ATTR_CACHE_OFF)
#define VM_STATE_WRITEALLOCATE		(MMU_ATTR_SPL_3 | MMU_ATTR_CACHE_COPYBACK)
#define VM_STATE_CACHEABLE_MINICACHE	(MMU_ATTR_SPL_1)
#define VM_STATE_EX_CACHEABLE_NOT	0x00

/* backwards compatability for vxWorks 5.5 */

#define VM_STATE_MASK_EX_BUFFERABLE	VM_STATE_MASK_EX_CACHEABLE
#define VM_STATE_EX_BUFFERABLE		VM_STATE_NO_COALESCE
#define VM_STATE_EX_CACHEABLE		VM_STATE_WRITEALLOCATE
#define VM_STATE_EX_BUFFERABLE_NOT	VM_STATE_EX_CACHEABLE_NOT

#else

/* additional cache states for ARM SA-1100 */

#define VM_STATE_CACHEABLE_MINICACHE	0x30	

#endif /* CPU == XSCALE */

/* function declarations */

struct mmuTransTblStruct;
#if defined(__STDC__) || defined(__cplusplus)

extern STATUS		vm_module_init (int pageSize);
extern VM_CONTEXT_ID 	sys_memory_map (PHYS_MEM_DESC *pMemDescArray, 
			  		 int numDescArrayElements, BOOL enable);
extern VM_CONTEXT *	vm_context_create ();
extern STATUS 		vm_context_delete (VM_CONTEXT_ID context);
extern STATUS 		vm_context_init (VM_CONTEXT *context);
extern STATUS 		vm_state_set (VM_CONTEXT *context, void *pVirtual,
				    int len, UINT stateMask, UINT state);
extern STATUS 		vm_state_get (VM_CONTEXT *context, void *pageAddr,
				    UINT *state);
extern STATUS 		vm_map (VM_CONTEXT *context, void *virtualAddr,
			       void *physicalAddr, UINT len);
extern STATUS 		vm_global_map (void *virtualAddr,
			       void *physicalAddr, UINT len);
extern UINT8 *		vmGlobalInfoGet ();
extern STATUS 		vm_translate (VM_CONTEXT *context, void *virtualAddr, 
		    		     void **physicalAddr);
extern int 		vm_page_size_get ();
extern VM_CONTEXT *	vm_current_get ();
extern STATUS 		vm_current_set (VM_CONTEXT *context);
extern STATUS 		vm_text_protect ();

extern STATUS 		vmBaseLibInit (int pageSize);
extern VM_CONTEXT_ID 	vmBaseGlobalMapInit (PHYS_MEM_DESC *pMemDescArray, 
				      	     int numDescArrayElements,
					     BOOL enable);
extern STATUS    	vmBaseStateSet (VM_CONTEXT *context, void *pVirtual, 
				        int len, UINT stateMask, UINT state);
extern int       	vmBasePageSizeGet (void);

extern STATUS 		vmMpuLibInit (int pageSize);
extern VM_CONTEXT_ID 	vmMpuGlobalMapInit (PHYS_MEM_DESC *pMemDescArray, 
				      	     int numDescArrayElements,
					     BOOL enable);
extern STATUS    	vmMpuStateSet (VM_CONTEXT *context, void *pVirtual, 
				       int len, UINT stateMask, UINT state);
extern int       	vmMpuPageSizeGet (void);

extern void 		vmShowInit (void);
extern STATUS 		vmContextShow (VM_CONTEXT_ID context);

extern struct mmuTransTblStruct *	mmuTransTblCreate ();
extern STATUS		mmuTransTblDelete (struct mmuTransTblStruct *transTbl);
extern STATUS		mmuEnable (BOOL enable);
extern STATUS		mmuStateSet (struct mmuTransTblStruct *transTbl, void *pageAddr,
				        UINT stateMask, UINT state);
extern STATUS		mmuStateGet (struct mmuTransTblStruct *transTbl, void *pageAddr,
				        UINT *state);
extern STATUS		mmuPageMap (struct mmuTransTblStruct *transTbl,
				       void * virtualAddress, void *physPage);
extern STATUS		mmuGlobalPageMap (void * virtualAddress,
				             void * physPage);
extern STATUS		mmuTranslate (struct mmuTransTblStruct * transTbl,
				         void * virtAddress,
					 void ** physAddress);
extern void		mmuCurrentSet (struct mmuTransTblStruct * transTbl);

#else	/* __STDC__ */

extern STATUS 		vm_module_init ();
extern VM_CONTEXT_ID 	sys_memory_map ();
extern VM_CONTEXT *	vm_context_create ();
extern STATUS 		vm_context_init ();
extern STATUS 		vm_context_delete ();
extern STATUS 		vm_state_set ();
extern STATUS 		vm_state_get ();
extern STATUS 		vm_map ();
extern STATUS 		vm_global_map ();
extern UINT8 *		vmGlobalInfoGet ();
extern STATUS 		vm_translate ();
extern int 		vm_page_size_get ();
extern VM_CONTEXT *	vm_current_get ();
extern STATUS 		vm_current_set ();
extern STATUS 		vm_text_protect ();

extern STATUS 		vmBaseLibInit ();
extern VM_CONTEXT_ID	vmBaseGlobalMapInit ();
extern STATUS    	vmBaseStateSet ();
extern int       	vmBasePageSizeGet ();

extern STATUS 		vmMpuLibInit ();
extern VM_CONTEXT_ID	vmMpuGlobalMapInit ();
extern STATUS    	vmMpuStateSet ();
extern int       	vmMpuPageSizeGet ();
	
extern void 		vmShowInit ();
extern STATUS 		vmContextShow ();
extern MMU_TRANS_TBL *	mmuPpcTransTblCreate ();
extern STATUS		mmuTransTblDelete ();
extern STATUS		mmuEnable ();
extern STATUS		mmuStateSet ();
extern STATUS		mmuStateGet ();
extern STATUS		mmuPageMap ();
extern STATUS		mmuGlobalPageMap ();
extern STATUS		mmuTranslate ();
extern void		mmuCurrentSet ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCvmLibh */

