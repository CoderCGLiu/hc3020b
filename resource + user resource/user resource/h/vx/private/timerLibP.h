#ifndef __INCtimerLibPh
#define __INCtimerLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <vxWorks.h>

extern int 	timer_cancel (timer_t timerid);
extern int 	timer_connect (timer_t timerid, VOIDFUNCPTR routine, _Vx_usr_arg_t arg);

#ifdef __cplusplus
}
#endif

#endif /* __INCtimerLibPh */
