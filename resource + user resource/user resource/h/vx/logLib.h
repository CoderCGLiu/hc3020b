#ifndef logLib_h
#define logLib_h

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS 	logFdAdd (int fd);
extern STATUS 	logFdDelete (int fd);
extern STATUS 	logInit (int fd, int maxMsgs);
extern int	logMsg (char *fmt, ptrdiff_t arg1, ptrdiff_t arg2,ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6);
extern int (*_func_logMsg)(char *fmt, ptrdiff_t arg1, ptrdiff_t arg2,
		ptrdiff_t arg3, ptrdiff_t arg4, ptrdiff_t arg5, ptrdiff_t arg6);
extern void 	logFdSet (int fd);
extern void 	logShow (void);
//extern void 	logTask (void);

#else	/* __STDC__ */

extern STATUS 	logFdAdd ();
extern STATUS 	logFdDelete ();
extern STATUS 	logInit ();
extern int 	logMsg ();
extern void 	logFdSet ();
extern void 	logShow ();
//extern void 	logTask ();

#endif	/* __STDC__ */


#ifdef __cplusplus
}

#endif

#endif  /* logLib_h  */


