/* blkIo.h - block I/O header file */

/* Copyright 1984-1992 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,22sep92,rrr  added support for c++
01h,04jul92,jcf  cleaned up.
01g,26may92,rrr  the tree shuffle
01f,04oct91,rrr  passed through the ansification filter
		  -changed READ, WRITE and UPDATE to O_RDONLY O_WRONLY O_RDWR
		  -changed copyright notice
01e,05oct90,shl  added copyright notice.
                 made #endif ANSI style.
01d,12jul90,kdl  added bd_statusChk routine field in BLK_DEV.
01c,04may90,kdl  added bd_mode and bd_readyChanged in BLK_DEV.
01b,23mar90,kdl  changed types for lint, changed BLK_DEV field names.
01a,15mar90,kdl  written
*/

#ifndef __INCblkIoh
#define __INCblkIoh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include <driver.h>
#include <reworksio.h>
	
/**
 * ReWorks块设备描述结构定义为struct block_deivice
 * 
 * 如需VxWorks块设备描述结构BLK_DEV，需自行定义；
 * 
 * 但需注意：ReWorks块设备描述结构与VxWorks块设备描述结构不同 
 */	
typedef struct block_device BLK_DEV;

#ifdef __cplusplus
}
#endif

#endif /* __INCblkIoh */
