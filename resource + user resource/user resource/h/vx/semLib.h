#ifndef semLib_h
#define semLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <vxWorks.h>
#include <reworks/thread.h>
#include <vwModNum.h>

typedef size_t SEMAPHORE; /* pthread_mutex_t */
typedef SEMAPHORE *        SEM_ID;

#define SEM_ID_NULL	((SEM_ID) 0)

//#ifdef __multi_core__
#define SEM_KERNEL              0x100   /* semaphore created in kernel space */
#define SEM_USER                0x200   /* semaphore created in user space   */
#define SEM_KUTYPE_DEFAULT      0x000   /* semaphore created in default space*/
#define SEM_KUTYPE_MASK         0x300   /* mask for semaphore creation space */

/* option masks for the types of semaphores */

#if !defined (_WRS_KERNEL)

#define SEM_BIN_OPTIONS_MASK	(SEM_Q_FIFO | 			\
				 SEM_Q_PRIORITY | 		\
				 SEM_EVENTSEND_ERR_NOTIFY | 	\
				 SEM_INTERRUPTIBLE | 		\
				 SEM_KUTYPE_MASK)

#else
#define SEM_BIN_OPTIONS_MASK	(SEM_Q_FIFO | 			\
				 SEM_Q_PRIORITY | 		\
				 SEM_EVENTSEND_ERR_NOTIFY | 	\
				 SEM_INTERRUPTIBLE)
#endif /* !_WRS_KERNEL */

#define SEM_CNT_OPTIONS_MASK	SEM_BIN_OPTIONS_MASK

#define SEM_MUT_OPTIONS_MASK	(SEM_BIN_OPTIONS_MASK | 	\
				 SEM_DELETE_SAFE |		\
				 SEM_INVERSION_SAFE)

#define SEM_RW_OPTIONS_MASK    (SEM_MUT_OPTIONS_MASK)
//#endif
/* generic status codes */

#define S_semLib_INVALID_STATE			(M_semLib | 101)
#define S_semLib_INVALID_OPTION			(M_semLib | 102)
#define S_semLib_INVALID_QUEUE_TYPE		(M_semLib | 103)
#define S_semLib_INVALID_OPERATION		(M_semLib | 104)
#define S_semLib_INVALID_INITIAL_COUNT	(M_semLib | 105)

typedef enum 		/* SEM_TYPE */
    {
    SEM_TYPE_BINARY,         /* 0: binary semaphore */
    SEM_TYPE_MUTEX,          /* 1: mutual exclusion semaphore */
    SEM_TYPE_COUNTING,       /* 2: counting semaphore */
    SEM_TYPE_RW,	     /* 3: read/write semaphore */
    /*
     * Reduce SEM_TYPE_MAX for small footprint, which allows only the
     * above 4 types, to eliminate wasted space in semaphore tables.
     */
    SEM_TYPE_MAX = 4

    } SEM_TYPE;
/* semaphore options */

#define SEM_Q_MASK		0x3	/* q-type mask */
#define SEM_Q_FIFO		0x0	/* first in first out queue */
#define SEM_Q_PRIORITY		0x1	/* priority sorted queue */
#define SEM_DELETE_SAFE		0x4	/* owner delete safe (mutex opt.) */
#define SEM_INVERSION_SAFE	0x8	/* no priority inversion (mutex opt.) */
#define SEM_INTERRUPTIBLE        0x20   /* interruptible on RTP signal */
//#ifdef __multi_core__
#define SEM_EVENTSEND_ERR_NOTIFY 0x10	/* notify when eventRsrcSend fails */
#define SEM_NO_ID_VALIDATE       0x40   /* inline: no semaphore validation */
#define SEM_NO_ERROR_CHECK       0x80   /* inline: no error checking */
#define SEM_NO_EVENT_SEND        0x100  /* inline: do not send events */
#define SEM_NO_SYSTEM_VIEWER     0x200  /* inline: no system viewer events */
#define SEM_NO_RECURSE           0x400  /* inline (mutex opt.): no recursion */
//#endif
typedef enum
    {
    SEM_EMPTY,
    SEM_FULL
    } SEM_B_STATE;


#if defined(__STDC__) || defined(__cplusplus)
extern void semModuleInit();
extern STATUS semBInit(SEMAPHORE  * id,int options,SEM_B_STATE initialState);
extern STATUS semCInit(SEMAPHORE  *id,int options,int initialCount);
extern STATUS semMInit(SEMAPHORE  *id,int options);
extern SEM_ID semBCreate(int options,SEM_B_STATE initialState);
extern SEM_ID semCCreate(int options,int initialCount);
extern SEM_ID semMCreate(int options);  
extern SEM_ID semRWCreate(int, int);
extern STATUS semWTake(SEM_ID, int);
extern STATUS semRTake(SEM_ID, int);
extern STATUS semRWGive(SEM_ID);
extern STATUS semGive(SEM_ID semId);
extern STATUS semTake(SEM_ID semId,int timeout);
extern STATUS semFlush(SEM_ID semId);
extern STATUS semDelete(SEM_ID semId);
extern STATUS semMGiveForce(SEM_ID semId);
extern STATUS semTerminate (SEM_ID semId);
extern OS_STATUS semShow(SEM_ID id, int level);
extern int 	semInfo (SEM_ID semId, TASK_ID idList[], int maxTasks);
#else 
extern void semModuleInit()
extern STATUS semBInit( );
extern STATUS semCInit( );
extern STATUS semMInit( );
extern SEM_ID semBCreate( );
extern SEM_ID semCCreate( );
extern SEM_ID semMCreate( );  
extern STATUS semGive( );
extern STATUS semTake( );
extern STATUS semFlush( );
extern STATUS semDelete( );
extern STATUS semMGiveForce( );
extern STATUS semTerminate ( );
#endif 


#ifdef __cplusplus
}
#endif
#endif
