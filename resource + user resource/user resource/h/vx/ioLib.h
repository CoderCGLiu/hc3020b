#ifndef ioLib_h
#define ioLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <sys/syslimits.h>
#include <sysLib.h>
#include <ioctl.h>

/*#define FIONREAD		1*/		/* get num chars available to read */
#define FIOFLUSH		2		/* flush any chars in buffers */
#define FIOOPTIONS		3		/* set options (FIOSETOPTIONS) */
#define FIOBAUDRATE		4		/* set serial baud rate */
#define FIODISKFORMAT	5		/* format disk */
#define FIODISKINIT		6		/* initialize disk directory */
#define FIOSEEK			7		/* set current file char position */
#define FIOWHERE		8		/* get current file char position */
#define FIODIRENTRY		9		/* return a directory entry (obsolete)*/
#define FIORENAME		10		/* rename a directory entry */
#define FIOREADYCHANGE	11		/* return TRUE if there has been a
					   			 * media change on the device */
#define FIONWRITE		12		/* get num chars still to be written */
#define FIODISKCHANGE	13		/* set a media change on the device */
#define FIOCANCEL		14		/* cancel read or write on the device */
#define FIOSQUEEZE		15		/* squeeze out empty holes in rt-11
					 			 * file system */
/*#define FIONBIO			16*/		/* set non-blocking I/O; SOCKETS ONLY!*/
#define FIONMSGS		17		/* return num msgs in pipe */
#define FIOGETNAME		18		/* return file name in arg */
#define FIOGETOPTIONS	19		/* get options */
#define FIOSETOPTIONS	FIOOPTIONS	/* set options */
#define FIOISATTY		20		/* is a tty */
#define FIOSYNC			21		/* sync to disk */
#define FIOPROTOHOOK	22		/* specify protocol hook routine */
#define FIOPROTOARG		23		/* specify protocol argument */
#define FIORBUFSET		24		/* alter the size of read buffer  */
#define FIOWBUFSET		25		/* alter the size of write buffer */
#define FIORFLUSH		26		/* flush any chars in read buffers */
#define FIOWFLUSH		27		/* flush any chars in write buffers */
#define FIOSELECT		28		/* wake up process in select on I/O */
#define FIOUNSELECT		29		/* wake up process in select on I/O */
#define FIONFREE        30      /* get free byte count on device */
#define FIOMKDIR        31      /* create a directory */
#define FIORMDIR        32      /* remove a directory */
#define FIOLABELGET     33      /* get volume label */
#define FIOLABELSET     34      /* set volume label */
#define FIOATTRIBSET    35      /* set file attribute */
#define FIOCONTIG       36      /* allocate contiguous space */
#define FIOREADDIR      37      /* read a directory entry (POSIX) */
#define FIOFSTATGET_OLD     38      /* get file status info */
#define FIOUNMOUNT      39      /* unmount disk volume */
#define FIOSCSICOMMAND  40      /* issue a SCSI command */
#define FIONCONTIG      41      /* get size of max contig area on dev */
#define FIOTRUNC        42      /* truncate file to specified length */
#define FIOGETFL        43		/* get file mode, like fcntl(F_GETFL) */
#define FIOTIMESET      44		/* change times on a file for utime() */
#define FIOINODETONAME  45		/* given inode number, return filename*/
#define FIOFSTATFSGET   46      /* get file system status info */
#define FIOMOVE         47      /* move file, ala mv, (mv not rename) */

/* 64-bit ioctl codes, "long long *" expected as 3rd argument */

#define FIOCHKDSK	(100  | 6400 )	/* salvage file system */
#define FIOCONTIG64	(FIOCONTIG | 6400)
#define FIONCONTIG64	(FIONCONTIG | 6400)
#define FIONFREE64	(FIONFREE | 6400)
#define FIONREAD64	(FIONREAD | 6400)
#define FIOSEEK64	(FIOSEEK | 6400)
#define FIOWHERE64	(FIOWHERE | 6400)
#define FIOTRUNC64	(FIOTRUNC | 6400)

#define FIOCOMMITFS     56
#define FIODATASYNC     57  /* sync to I/O data integrity */
#define FIOLINK         58  /* create a link */
#define FIOUNLINK       59  /* remove a link */
#define FIOACCESS       60  /* support POSIX access() */
#define FIOPATHCONF     61  /* support POSIX pathconf() */
#define FIOFCNTL        62  /* support POSIX fcntl() */
#define FIOCHMOD        63  /* support POSIX chmod() */
#define FIOFSTATGET     64  /* get stat info - POSIX  */
#define FIOUPDATE       65  /* update dosfs default create option */

/* the next two ioctls are for block-type storage media (e.g., disks) */
#define FIODISCARDGET   70  /* does hardware want DISCARDs? */
#define FIODISCARD      71  /* mark sectors (sectorRange) as deleted */

/* structure passed through "mark unused, can discard these sectors" ioctl */
typedef struct
    {
    long long       startSector;
    unsigned int    nSectors;   /* or unsigned long long? */
    } SECTOR_RANGE;
/* ioctl option values */

#define OPT_ECHO	0x01		/* echo input */
#define OPT_CRMOD	0x02		/* lf -> crlf */
#define OPT_TANDEM	0x04		/* ^S/^Q flow control protocol */
#define OPT_7_BIT	0x08		/* strip parity bit from 8 bit input */
#define OPT_MON_TRAP	0x10		/* enable trap to monitor */
#define OPT_ABORT	0x20		/* enable shell restart */
#define OPT_LINE	0x40		/* enable basic line protocol */

#define OPT_RAW		0		/* raw mode */

#define OPT_TERMINAL	(OPT_ECHO | OPT_CRMOD | OPT_TANDEM | \
			 OPT_MON_TRAP | OPT_7_BIT | OPT_ABORT | OPT_LINE)

#define CONTIG_MAX	-1		/* "count" for FIOCONTIG if requesting
					 *  maximum contiguous space on dev
					 */

/* miscellaneous */

#define FOLLOW_LINK	-2			/* value for driver to return */

#define DEFAULT_FILE_PERM	0000640		/* default file permissions
						   unix style rw-r----- */
#define DEFAULT_DIR_PERM	0000750		/* default directory permissions
						   unix style rwxr-x--- */

/* file types -- NOTE:  these values are specified in the NFS protocol spec */

#define FSTAT_DIR		0040000		/* directory */
#define FSTAT_CHR		0020000		/* character special file */
#define FSTAT_BLK		0060000		/* block special file */
#define FSTAT_REG		0100000		/* regular file */
#define FSTAT_LNK		0120000		/* symbolic link file */
#define FSTAT_NON		0140000		/* named socket */

/* disk formats */

#define SS_1D_8		1	/* single side, single density, 8"     */
#define SS_2D_8		2	/* single side, double density, 8"     */
#define DS_1D_8		3	/* double side, single density, 8"     */
#define DS_2D_8		4	/* double side, double density, 8"     */
#define SS_1D_5		5	/* single side, single density, 5 1/4" */
#define SS_2D_5		6	/* single side, double density, 5 1/4" */
#define DS_1D_5		7	/* double side, single density, 5 1/4" */
#define DS_2D_5		8	/* double side, double density, 5 1/4" */

#define HD_1		129	/* hard disk - type 1 */
#define HD_2		130	/* hard disk - type 2 */

#define MAX_DIRNAMES		32		/* max directory names in path*/
#define MAX_FILENAME_LENGTH	(255 + 1)	/* max chars in filename
						 * including EOS*/
#ifndef _ASMLANGUAGE
    /* device driver prototype */

#ifdef REWORKS_LP64
	typedef long  _Vx_ioctl_arg_t;
#else
	typedef int   _Vx_ioctl_arg_t;
#endif  /*REWORKS_LP64*/
/*
 * Used to provide for both new and old file names in rename and move.
 * Device driver ioctl can cast the 3rd arg as a pointer to
 * RENAME_STRUCT. If not interested, it remains compatible
 * with a pointer to new, destination file name string.
 */
typedef struct
	{
	char    newname[MAX_FILENAME_LENGTH];   /* New, destination file name */
	char *  oldname;                        /* Old, original file name */
	} RENAME_STRUCT;
/* I/O status codes */
#endif
#define S_ioLib_NO_DRIVER		(M_ioLib | 1)
#define S_ioLib_UNKNOWN_REQUEST		(M_ioLib | 2)
#define S_ioLib_DEVICE_ERROR		(M_ioLib | 3)
#define S_ioLib_DEVICE_TIMEOUT		(M_ioLib | 4)
#define S_ioLib_WRITE_PROTECTED		(M_ioLib | 5)
#define S_ioLib_DISK_NOT_PRESENT	(M_ioLib | 6)
#define S_ioLib_NO_FILENAME		(M_ioLib | 7)
#define S_ioLib_CANCELLED		(M_ioLib | 8)
#define S_ioLib_NO_DEVICE_NAME_IN_PATH	(M_ioLib | 9)
#define S_ioLib_NAME_TOO_LONG		(M_ioLib | 10)
#define S_ioLib_UNFORMATED		(M_ioLib | 11)
#define S_ioLib_CANT_OVERWRITE_DIR      (M_ioLib | 12)

/* 
* This errno is used by drivers to communicate the media changed condition to
* Core I/O - it should not be used outside of the I/O system
*/
#define S_ioLib_MEDIA_CHANGED		(M_ioLib | 1)

//int readv (int fd, const struct iovec *iov, int iovcnt );
//int writev ( int fd, register const struct iovec *iov, int iovcnt );
typedef FUNCPTR DRV_CREATE_PTR;
typedef FUNCPTR DRV_OPEN_PTR;
typedef FUNCPTR DRV_REMOVE_PTR;
typedef FUNCPTR DRV_CLOSE_PTR;
typedef FUNCPTR DRV_READ_PTR;
typedef FUNCPTR DRV_WRITE_PTR;
typedef FUNCPTR DRV_IOCTL_PTR;
    
//int readv (int fd, const struct iovec *iov, int iovcnt );
//int writev ( int fd, register const struct iovec *iov, int iovcnt );
 
#ifdef __cplusplus
}
#endif
#endif
