
#ifndef ifLib_h
#define ifLib_h
#include <vxWorks.h>
#include <net/if.h>
#ifdef __cplusplus
extern "C" {
#endif

extern STATUS   ifAddrAdd (char *interfaceName, char *interfaceAddress,  char *broadcastAddress, int subnetMask);
extern STATUS 	ifAddrSet (char *interfaceName, char *interfaceAddress);
extern STATUS 	ifAddrDelete (char *interfaceName, char *interfaceAddress);
extern STATUS 	ifAddrGet (char *interfaceName, char *interfaceAddress);
extern STATUS 	ifBroadcastSet (char *interfaceName, char *broadcastAddress);
extern STATUS 	ifBroadcastGet (char *interfaceName, char *broadcastAddress);
extern STATUS 	ifDstAddrSet (char *interfaceName, char *dstAddress);
extern STATUS 	ifDstAddrGet (char *interfaceName, char *dstAddress);
extern STATUS 	ifMaskSet (char *interfaceName, int netMask);
extern STATUS 	ifMaskGet (char *interfaceName, int *netMask);
extern STATUS 	ifFlagChange (char *interfaceName, int flags, BOOL on);
extern STATUS 	ifFlagSet (char *interfaceName, int flags);
extern STATUS 	ifFlagGet (char *interfaceName, int *flags);
extern STATUS 	ifMetricSet (char *interfaceName, int metric);
extern STATUS 	ifMetricGet (char *interfaceName, int *pMetric);
extern int 	ifRouteDelete (char *ifName, int unit);
extern struct 	ifnet *ifunit (char *ifname);
extern struct ifnet * ifIndexToIfp   ( int ifIndex  );
#ifdef __cplusplus
}
#endif
#endif
 
