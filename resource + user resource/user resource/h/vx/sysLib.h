#ifndef sysLib_h
#define sysLib_h

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE
#include "vxWorks.h"
#include "sioLib.h"
//#include <sys/uio.h>

#if 0
struct iovec {
	char	*iov_base;	/* Base address. */
	size_t	 iov_len;	/* Length. */
};
#endif
/* system restart types */

#define	BOOT_NORMAL		0x00	/* normal reboot with countdown */
#define BOOT_NO_AUTOBOOT	0x01	/* no autoboot if set */
#define BOOT_CLEAR		0x02	/* clear memory if set */
#define BOOT_QUICK_AUTOBOOT	0x04	/* fast autoboot if set */

/* for backward compatibility */

#define BOOT_WARM_AUTOBOOT		BOOT_NORMAL
#define BOOT_WARM_NO_AUTOBOOT		BOOT_NO_AUTOBOOT
#define BOOT_WARM_QUICK_AUTOBOOT	BOOT_QUICK_AUTOBOOT
#define BOOT_COLD			BOOT_CLEAR


#if defined(__STDC__) || defined(__cplusplus)
extern char *	sysModel (void);
extern char *	sysBspRev (void);
/*
 * These prototypes are for the x86 family only.  They are incorrect,
 * but we do not want to change the API for the entire ARCH_FAMILY.
 * They should be unsigned arguments.
 */

#if 0
extern UCHAR	sysInByte		(int port);
extern USHORT	sysInWord		(int port);
extern ULONG	sysInLong		(int port);
extern void	sysInWordString		(int port, short *pData, int count);
extern void	sysInLongString		(int port, long *pData, int count);
extern void	sysOutByte		(int port, char data);
extern void	sysOutWord		(int port, short data);
extern void	sysOutLong		(int port, long data);
extern void	sysOutWordString	(int port, short *pData, int count);
extern void	sysOutLongString	(int port, long *pData, int count);

extern void	sysReboot		(void);
extern void	sysDelay		(void);
extern void	sysWait			(void);
#endif

extern STATUS	sysIntDisablePIC	(int intLevel);
extern STATUS	sysIntEnablePIC		(int intLevel);

//IMPORT	STATUS	sysClkConnect (FUNCPTR routine, int arg);
//IMPORT	void	sysClkDisable (void);
//IMPORT	void	sysClkEnable (void);
IMPORT	int	sysClkRateGet (void);
IMPORT	STATUS	sysClkRateSet (int ticksPerSecond);

#if 0
//	STATUS	sysAuxClkConnect (FUNCPTR routine, int arg);
//IMPORT	void	sysAuxClkDisable (void);
//IMPORT	void	sysAuxClkEnable (void);
//IMPORT	int	sysAuxClkRateGet (void);
//IMPORT	STATUS	sysAuxClkRateSet (int ticksPerSecond);
//IMPORT	STATUS	sysTimestampConnect (FUNCPTR routine, int arg);
//IMPORT	STATUS	sysTimestampEnable (void);
//IMPORT	STATUS	sysTimestampDisable (void);
//IMPORT	UINT32	sysTimestampFreq (void);
//IMPORT	UINT32	sysTimestampPeriod (void);
//IMPORT	UINT32	sysTimestamp (void);
//IMPORT	UINT32	sysTimestampLock (void);
//extern void	sysSerialHwInit (void);
//extern void	sysSerialHwInit2 (void);
//extern SIO_CHAN * sysSerialChanGet (int channel);
#endif

#else
#if 0
extern UCHAR	sysInByte		();
extern USHORT	sysInWord		();
extern ULONG	sysInLong		();
extern void	sysInWordString		();
extern void	sysInLongString		();
extern void	sysOutByte		();
extern void	sysOutWord		();
extern void	sysOutLong		();
extern void	sysOutWordString	();
extern void	sysOutLongString	();

extern void	sysReboot		();
extern void	sysDelay		();
extern void	sysWait			();
#endif

extern STATUS	sysIntDisablePIC	();
extern STATUS	sysIntEnablePIC();

//IMPORT	STATUS	sysClkConnect ();
//IMPORT	void	sysClkDisable ();
//IMPORT	void	sysClkEnable ();
IMPORT	int	sysClkRateGet ();
IMPORT	STATUS	sysClkRateSet ();
#if 0
IMPORT	STATUS	sysAuxClkConnect ();
IMPORT	void	sysAuxClkDisable ();
IMPORT	void	sysAuxClkEnable ();
IMPORT	int	sysAuxClkRateGet ();
IMPORT	STATUS	sysAuxClkRateSet ();
IMPORT	STATUS	sysTimestampConnect ();
IMPORT	STATUS	sysTimestampEnable ();
IMPORT	STATUS	sysTimestampDisable ();
IMPORT	UINT32	sysTimestampFreq ();
IMPORT	UINT32	sysTimestampPeriod ();
IMPORT	UINT32	sysTimestamp ();
IMPORT	UINT32	sysTimestampLock ();
extern void	sysSerialHwInit (void);
extern void	sysSerialHwInit2 (void);
extern SIO_CHAN *sysSerialChanGet ();
#endif
#endif
#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif
