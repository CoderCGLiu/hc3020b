#ifdef __multi_core__
#ifndef twdLib_h
#define twdLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <vxWorks.h>

extern void twdModuleInit();

extern STATUS 	twdStart(WDOG_ID wdId,int delay,FUNCPTR pRoutine,int parameter);
extern STATUS 	twdCancel(WDOG_ID wdId);
extern int twdg_start(wdg_t wdg_id, int ticks, void (*func)(void *arg), void *arg);
extern int twdg_cancel(wdg_t wdg_id);
#ifdef __cplusplus
}
#endif
#endif
#endif