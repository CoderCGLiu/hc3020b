#ifndef __INCmemLibh
#define __INCmemLibh

#include <reworks/types.h>
#include "vxWorks.h"
#include "vwModNum.h"


#ifdef __cplusplus
extern "C" {
#endif

/* status codes */

#define S_memLib_NOT_ENOUGH_MEMORY		(M_memLib | 1)
#define S_memLib_INVALID_NBYTES			(M_memLib | 2)
#define S_memLib_BLOCK_ERROR			(M_memLib | 3)
#define S_memLib_NO_PARTITION_DESTROY		(M_memLib | 4)
#define S_memLib_PAGE_SIZE_UNAVAILABLE		(M_memLib | 5)
	
#ifndef	ALLOC_ALIGN_SIZE
#define	ALLOC_ALIGN_SIZE	4	
#endif	

#if 0
#ifndef	MAX_POOLS
#define	MAX_POOLS   256	
#endif	

typedef struct mem_part
{		
	u32 	    sem;	
	void        *pools[MAX_POOLS];   
    
}PARTITION;

typedef PARTITION * PART_ID;


#endif


extern PART_ID memSysPartId;

/*
 * KHEAP macros:
 * These macros are defined simply for cosmetic reasons, to make it more 
 * obvious that buffers are allocated or freed from the kernel heap, also 
 * called the system memory partition.
 * No guaranty is given that the buffer allocated from the Kernel
 * Heap via KHEAP_ALLOC has a known and constant virtual to physical mapping
 * (1 to 1 mapping for instance) if virtual memory support is included (i.e.
 * INCLUDE_MMU_BASIC or INCLUDE_MMU_FULL components).
 *
 * NOTE: KHEAP_ALIGNED_ALLOC() uses memPartAlignedAlloc() instead of memalign()
 *       to prevent any requirements on INCLUDE_MEM_MGR_FULL when using the 
 *	 macro KHEAP_ALIGNED_ALLOC.
 */

#define KHEAP_ALLOC(nBytes)                                             \
        malloc((nBytes))
#define KHEAP_FREE(pChar)                                               \
        free((pChar))
#define KHEAP_ALIGNED_ALLOC(nBytes, alignment)                          \
        memPartAlignedAlloc(memSysPartId, (nBytes), (alignment))
#define KHEAP_REALLOC(pChar, newSize)                                    \
        memPartRealloc(memSysPartId, (pChar), (newSize))


/*
 * KMEM macros:
 * If a known and constant virtual to physical mapping is required for buffer
 * allocated, KMEM macros should be used instead of KHEAP macros.
 * For instance memory block dynamically allocated that may be accessed when
 * the processor MMU is turn off or on should always be allocated using 
 * KMEM_ALLOC() or KMEM_ALIGNED_ALLOC(). Good examples are memory blocks used 
 * to store MMU translation information on some processor, or memory blocks
 * accessed by DMA devices.
 * Good care should be taken before using KMEM macros. If no constant virtual 
 * to physical mapping is required, then KHEAP macros should always be used 
 * instead.
 *
 * NOTE: With VxWorks 5.5.x, there's no differences between KHEAP_XXX and 
 * KMEM_XXX macros. However this will change in future releases.
 */

#define KMEM_ALLOC(nBytes)      					\
	memPartAlloc(memSysPartId, (nBytes))
#define KMEM_FREE(pChar)						\
	memPartFree(memSysPartId, (pChar))
#define KMEM_ALIGNED_ALLOC(nBytes, alignment)				\
        memPartAlignedAlloc(memSysPartId, (nBytes), (alignment))


#if defined(__STDC__) || defined(__cplusplus)

typedef struct 
{
	unsigned long numBytesFree, /* Number of Free Bytes in Partition       */
	numBlocksFree, /* Number of Free Blocks in Partition      */
	maxBlockSizeFree,/* Maximum block size that is free.	      */
	numBytesAlloc, /* Number of Allocated Bytes in Partition  */
	numBlocksAlloc; /* Number of Allocated Blocks in Partition */
} MEM_PART_STATS;
    
//extern STATUS 	memInit (char *pPool, unsigned poolSize);
//extern STATUS 	memPartLibInit (char *pPool, unsigned poolSize);
extern PART_ID 	memPartCreate (char *pPool, unsigned poolSize);
//extern void 	memPartInit (PART_ID partId, char *pPool, unsigned poolSize);
extern STATUS 	memPartAddToPool (PART_ID partId, char *pPool,
				  unsigned poolSize);
extern void 	memAddToPool (char *pPool, unsigned poolSize);
extern void *	memPartAlloc (PART_ID partId, unsigned nBytes);
extern void *   memPartAlignedAlloc (PART_ID partId, unsigned nBytes,
				     unsigned alignment);
/*Start of 李健 on 2006-12-4 15:7 3.0*/
#if 0
extern void *	memalign (unsigned alignment, unsigned size);
extern void *   valloc (unsigned size);
#endif
/*End of 李健 on 2006-12-4 15:7 3.0*/
extern STATUS 	memPartFree (PART_ID partId, char *pBlock);
//extern STATUS 	memPartOptionsSet (PART_ID partId, unsigned options);
extern ssize_t 	memFindMax (void);
extern int 	memPartFindMax (PART_ID partId);
extern void *	memPartRealloc (PART_ID partId, char *pBlock, unsigned nBytes);
//extern void 	memOptionsSet (unsigned options);
/*Start of 李健 on 2006-11-28 11:15 3.0*/
#if 0
extern STATUS 	cfree (char *pBlock);
#endif
/*End of 李健 on 2006-11-28 11:15 3.0*/
extern void 	memShowInit (void);
extern void 	memShow (int type);
extern STATUS 	memPartShow (PART_ID partId, int type);
extern STATUS   memPartInfoGet (PART_ID	partId, MEM_PART_STATS * ppartStats);
extern char* 	sysMemTop (void);
extern void part_module_init(void);
extern int posix_memalign(void **memptr, size_t alignment, size_t size);
extern void *memalign(size_t boundary, size_t size);
extern void *valloc(size_t size);


#else	/* __STDC__ */

//extern STATUS 	memInit ();
//extern STATUS 	memPartLibInit ();
extern PART_ID 	memPartCreate ();
//extern void 	memPartInit ();
extern STATUS 	memPartAddToPool ();
extern void 	memAddToPool ();
extern void *	memPartAlloc ();
extern void *   memPartAlignedAlloc ();
extern int posix_memalign();
extern void *	memalign ();
extern void *   valloc ();
extern STATUS 	memPartFree ();
//extern STATUS 	memPartOptionsSet ();
extern int 	memFindMax ();
extern int 	memPartFindMax ();
extern void *	memPartRealloc ();
//extern void 	memOptionsSet ();

/*Start of 李健 on 2006-11-28 11:15 3.0*/
#if 0
extern STATUS 	cfree ();
#endif
/*End of 李健 on 2006-11-28 11:15 3.0*/
extern void 	memShowInit ();
extern void 	memShow ();
extern STATUS 	memPartShow ();
//extern STATUS   memPartInfoGet ();
extern void part_module_init(void);
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif


#endif

