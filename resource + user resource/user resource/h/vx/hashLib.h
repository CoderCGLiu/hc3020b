/* hashLib.h - hash table library header */

/* Copyright 1984-1995 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,01mar95,pad  removed old style error codes.
01h,20jan95,jcf  made more portable.
01g,22sep92,rrr  added support for c++
01f,04jul92,jcf  cleaned up.
01e,26may92,rrr  the tree shuffle
01d,04oct91,rrr  passed through the ansification filter
		  -changed copyright notice
01c,05oct90,shl  added ANSI function prototypes.
                 made #endif ANSI style.
		 added copyright notice.
01b,26jun90,jcf  remove hash id error status.
01a,17nov89,jcf  written.
*/

#ifndef __INChashLibh
#define __INChashLibh

#ifdef __cplusplus
extern "C" {
#endif
#include <vxWorks.h>
#include "sllLib.h"
#include "objLib.h"
#include <reworks/list.h>

/* type definitions */

/* HIDDEN */

/* 所有核心对象都需包括的对象头，置于所在对象数据结构的最开头 */
/* huangyuan20130328: 原先self_ref既可以作校验用，也可以给vx的XXXCreate()使用，也可以给posix的sem_open()使用
 * 现在self_ref的校验功能分离至magic_num，使得释放对象后再度访问对象时，只要内存不重新使用，校验对象不报错 。*/
typedef struct
{
	void				*magic_num; /* 校验用魔数 */
	u32 				type; /* 对象类型 */
	void				*self_ref; /* 引用该结构体所在对象（即其起始地址） */
	struct list_head    node; /* 链表头 */
	void                *name; /* 对象名称 */
} OBJ_HASH;
typedef struct hashtbl		/* HASH_TBL */
    {
    OBJ_HASH	obj;		/* object management */  
    int		elements;		/* number of elements in table */
    FUNCPTR	keyCmpRtn;		/* comparator function */
    FUNCPTR	keyRtn;			/* hash function */
    _Vx_usr_arg_t		keyArg;			/* hash function argument */
    SL_LIST	*pHashTbl;		/* pointer to hash table array */
    } HASH_TBL;

typedef SL_NODE HASH_NODE;	/* HASH_NODE */

typedef HASH_TBL *HASH_ID;

/* END_HIDDEN */

/* These hash nodes are used by the hashing functions in hashLib(1) */

typedef struct			/* H_NODE_INT */
    {
    HASH_NODE	node;			/* linked list node (must be first) */
    _Vx_usr_arg_t key;			/* hash node key */
    _Vx_usr_arg_t data;			/* hash node data */
    } H_NODE_INT;

typedef struct			/* H_NODE_STRING */
    {
    HASH_NODE	node;			/* linked list node (must be first) */
    char	*string;		/* hash node key */
    _Vx_usr_arg_t data;			/* hash node data */
    } H_NODE_STRING;


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern BOOL 		hashKeyCmp (H_NODE_INT *pMatchHNode,
				    H_NODE_INT *pHNode, _Vx_usr_arg_t keyCmpArg);
extern BOOL 		hashKeyStrCmp (H_NODE_STRING *pMatchHNode,
				       H_NODE_STRING *pHNode, _Vx_usr_arg_t keyCmpArg);
extern HASH_ID 		hashTblCreate (int sizeLog2, FUNCPTR keyCmpRtn,
				       FUNCPTR keyRtn, _Vx_usr_arg_t keyArg);
extern HASH_NODE *	hashTblEach (HASH_ID hashId, FUNCPTR routine,
				     _Vx_usr_arg_t routineArg);
extern HASH_NODE *	hashTblFind (HASH_ID hashId, HASH_NODE *pMatchNode,
				     _Vx_usr_arg_t keyCmpArg);
extern STATUS 		hashLibInit (void);
extern STATUS 		hashTblDelete (HASH_ID hashId);
extern STATUS 		hashTblDestroy (HASH_ID hashId, BOOL dealloc);
extern STATUS 		hashTblInit (HASH_TBL *pHashTbl, SL_LIST *pTblMem,
				     int sizeLog2, FUNCPTR keyCmpRtn,
				     FUNCPTR keyRtn, _Vx_usr_arg_t keyArg);
extern STATUS 		hashTblPut (HASH_ID hashId, HASH_NODE *pHashNode);
extern STATUS 		hashTblRemove (HASH_ID hashId, HASH_NODE *pHashNode);
extern STATUS 		hashTblTerminate (HASH_ID hashId);
extern int 		hashFuncIterScale (int elements, H_NODE_STRING *pHNode,
					   _Vx_usr_arg_t seed);
extern int 		hashFuncModulo (int elements, H_NODE_INT *pHNode,
					_Vx_usr_arg_t divisor);
extern int 		hashFuncMultiply (int elements, H_NODE_INT *pHNode,
					  _Vx_usr_arg_t multiplier);

#else	/* __STDC__ */

extern BOOL 	hashKeyCmp ();
extern BOOL 	hashKeyStrCmp ();
extern HASH_ID 	hashTblCreate ();
extern HASH_NODE *hashTblEach ();
extern HASH_NODE *hashTblFind ();
extern STATUS 	hashLibInit ();
extern STATUS 	hashTblDelete ();
extern STATUS 	hashTblDestroy ();
extern STATUS 	hashTblInit ();
extern STATUS 	hashTblPut ();
extern STATUS 	hashTblRemove ();
extern STATUS 	hashTblTerminate ();
extern int 	hashFuncIterScale ();
extern int 	hashFuncModulo ();
extern int 	hashFuncMultiply ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INChashLibh */
