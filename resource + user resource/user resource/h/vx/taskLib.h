

#ifndef taskLib_h
#define taskLib_h


#include "vxWorks.h"
#include "vwModNum.h"
#include "classLib.h"
#include "private/objLibP.h"
#include "types/vxTypes.h"
#include "stdlib.h"
#include <reworks/thread.h>
#include <cpu.h>
#ifdef __multi_core__
#include <cpuset.h>
#endif

#ifdef BOOL
#undef BOOL
#endif
#define BOOL int

#define taskIdCurrent ((WIND_TCB *)(thread_get2(0, RE_TASK, USR_THREAD_T_GET)))

#define VX_TASK_MIN_PRI PRI_MIN
#define VX_TASK_MAX_PRI PRI_MAX

#define M_taskLib				(3 << 16)

#define S_taskLib_NAME_NOT_FOUND			(M_taskLib | 101)
#define S_taskLib_TASK_HOOK_TABLE_FULL		(M_taskLib | 102)
#define S_taskLib_TASK_HOOK_NOT_FOUND		(M_taskLib | 103)
#define S_taskLib_TASK_SWAP_HOOK_REFERENCED	(M_taskLib | 104)
#define S_taskLib_TASK_SWAP_HOOK_SET		(M_taskLib | 105)
#define S_taskLib_TASK_SWAP_HOOK_CLEAR		(M_taskLib | 106)
#define S_taskLib_TASK_VAR_NOT_FOUND		(M_taskLib | 107)
#define S_taskLib_TASK_UNDELAYED			(M_taskLib | 108)
#define S_taskLib_ILLEGAL_PRIORITY			(M_taskLib | 109)
#define S_taskLib_ILLEGAL_OPERATION		    (M_taskLib | 112)
#define S_taskLib_ILLEGAL_STACK_INFO        (M_taskLib | 113)

/* miscellaneous */

#define MAX_TASK_ARGS			10	/* max args passed to a task */
#define VX_MAX_TASK_SWITCH_RTNS	16	/* max task switch callout routines */
#define VX_MAX_TASK_SWAP_RTNS	16	/* max task swap callout routines */
#define VX_MAX_TASK_DELETE_RTNS	16	/* max task delete callout routines */
#define VX_MAX_TASK_CREATE_RTNS	16	/* max task create callout routines */

/* task option bits */

#define VX_SUPERVISOR_MODE	0x0001	        /* OBSOLETE: tasks always in sup mode */
#define VX_UNBREAKABLE	  	RE_UNBREAKABLE	        /* INTERNAL: breakpoints ignored */
#define VX_DEALLOC_STACK  	RE_DEALLOC_STACK/* INTERNAL: deallocate stack */
#if defined(__HUARUI2__) || defined(__HUARUI3__) //add HR3 2023-03-06s
#define VX_FP_TASK	   		RE_FP_TASK | RE_VEC_TASK	    /* 1 = f-point coprocessor support */
#else
#define VX_FP_TASK	   		RE_FP_TASK
#endif
#define VX_STDIO	   		0x0010	        /* OBSOLETE: need not be set for stdio*/
#define VX_ADA_DEBUG	   	0x0020	        /* 1 = VADS debugger support */
#define VX_FORTRAN	   		0x0040	        /* 1 = NKR FORTRAN support */
#define VX_PRIVATE_ENV 		0x0080	        /* 1 = private environment variables */
#define VX_NO_STACK_FILL	RE_NO_STACK_FILL/* 1 = avoid stack fill of 0xee */
#define VX_DSP_TASK			0x0200	        /* 1 = dsp coprocessor support */
#define VX_ALTIVEC_TASK		0x0400          /* 1 = ALTIVEC coprocessor support */
#define VX_USER_ALLOC_TCB   RE_USER_ALLOC_TCB

typedef struct
{
	void     (*entryPt)();		/* entry point of new task */
//	int     arg1;			/* 1st of 10 req'd task args to pass to func */
//	int     arg2;
//	int     arg3;
//	int     arg4;
//	int     arg5;
//	int     arg6;
//	int     arg7;
//	int     arg8;
//	int     arg9;
//	int     arg10;	
} vx_task_entry_arg; /* huangyuan20081013：该数据结构只是为了taskTcbTest.c而保留 */

/* huangyuan20090108：pTaskVar搬迁到核心中去 */
//typedef struct taskVar	/* TASK_VAR */
//    {
//    struct taskVar *	next;	/* ptr to next task variable */
//    int *		address;/* address of variable to swap */
//    int			value;	/* when task is not running: save of task's val;
//				 * when task is running: save of orig. val */
//    } TASK_VAR;

typedef struct wind_tcb 
{
/* lijuan 20120410：将safe_count移至核心中，以合并POSIX和VX的互斥量删除保护机制 */
//    u32                 safe_count;         /* 任务保护的嵌套层次 */
//    WaitQ_Ctrl          task_safe_queue;   /* 任务被保护而不被删除时，等待去删除任务的队列 */
/* 从等待队列task_safe_queue出队时的返回值 */
//#define TASK_DELETED (1) /* 因为任务被删除而从等待队列中出队 */
//#define TASK_UNSAFED (2) /* 因为任务被取消保护而从等待队列中出队 */
/* huangyuan20080901：请参见wait_queue.h中定义的公用返回值，请不要使用相同的值 */
    vx_task_entry_arg   vx_task_entry; /* huangyuan20081013：该字段只是为了taskTcbTest.c而保留 */
/* huangyuan20090108：pTaskVar搬迁到核心中去 */
//    struct taskVar *	pTaskVar;	/* 0x9c: ptr to task variable list */
    int 	            spare1;
    int 	            spare2;
    int 	            spare3;
    int 	            spare4;
#ifdef __multi_core__
    struct selContext *	pSelectContext;	/* 0xdc: ptr to select info for task */
#endif
    u32				    priority;		/* ???????? */
    u32 priNormal;
    u32				    status;           /* ???????? */
    char * pExcStackBase;
    char * pExcStackEnd;
    int		taskTicks;	/* 0xe4/0x190: total number of ticks */
    int		taskIncTicks;	/* 0xe8/0x194: number of ticks in */
    FUNCPTR		entry;		/* 0xc4/0x150: entry point of task */
    TASK_ID taskId;
 } WIND_TCB;
 
 /* task status values */

 #define WIND_READY              0x00    /* ready to run */
 #define WIND_SUSPEND            0x01    /* explicitly suspended */
 #define WIND_PEND               0x02    /* pending on semaphore */
 #define WIND_DELAY              0x04    /* task delay (or timeout) */
 #define WIND_RUNNING            0x40    /* running */

 typedef struct 			/* TASK_DESC - information structure */
     {
	 thread_t			td_id;		/* task id */
     char *		td_name;	/* name of task */
     int			td_priority;	/* task priority */
     int			td_status;	/* task status */
     int			td_options;	/* task option bits (see below) */
     FUNCPTR		td_entry;	/* original entry point of task */
     char *		td_sp;		/* saved stack pointer */
     char *		td_pStackBase;	/* the bottom of the stack */
     char *		td_pStackLimit;	/* the effective end of the stack */
     char *		td_pStackEnd;	/* the actual end of the stack */
     size_t			td_stackSize;	/* size of stack in bytes */
     size_t			td_stackCurrent;/* current stack usage in bytes */
     size_t			td_stackHigh;	/* maximum stack usage in bytes */
     size_t			td_stackMargin;	/* current stack margin in bytes */
     int			td_errorStatus;	/* most recent task error status */
     int			td_delay;	/* delay/timeout ticks */
     } TASK_DESC;

#define OM_CREATE               0x10000000
#define OM_EXCL                 0x20000000
#define OM_DELETE_ON_LAST_CLOSE 0x40000000
#ifdef __cplusplus
extern "C" {
#endif


#if defined(__STDC__) || defined(__cplusplus)

extern STATUS 	taskLibInit (void);

extern TASK_ID taskSpawn (char * name, int priority, int options,
		size_t stackSize, FUNCPTR entryPt,
		_Vx_usr_arg_t arg1, _Vx_usr_arg_t arg2,
		_Vx_usr_arg_t arg3, _Vx_usr_arg_t arg4,
		_Vx_usr_arg_t arg5, _Vx_usr_arg_t arg6,
		_Vx_usr_arg_t arg7, _Vx_usr_arg_t arg8,
		_Vx_usr_arg_t arg9, _Vx_usr_arg_t arg10);

extern TASK_ID taskCreate (char * name, int priority, int options,
		size_t stackSize, FUNCPTR entryPt,
		_Vx_usr_arg_t arg1, _Vx_usr_arg_t arg2,
		_Vx_usr_arg_t arg3, _Vx_usr_arg_t arg4,
		_Vx_usr_arg_t arg5, _Vx_usr_arg_t arg6,
		_Vx_usr_arg_t arg7, _Vx_usr_arg_t arg8,
		_Vx_usr_arg_t arg9, _Vx_usr_arg_t arg10);

STATUS taskInit
    (
	WIND_TCB * pTcb, char *     name, int        priority,
	int        options, char *     pStackBase,
	int        stack_size, FUNCPTR    entryPt,
	_Vx_usr_arg_t        arg1, 	_Vx_usr_arg_t        arg2,
	_Vx_usr_arg_t        arg3, 	_Vx_usr_arg_t        arg4,
	_Vx_usr_arg_t        arg5, 	_Vx_usr_arg_t        arg6,
	_Vx_usr_arg_t        arg7, 	_Vx_usr_arg_t        arg8,
	_Vx_usr_arg_t        arg9, 	_Vx_usr_arg_t        arg10);
    
extern STATUS 	taskActivate (TASK_ID tid);
extern STATUS 	taskDelete (TASK_ID tid);
extern STATUS 	taskDeleteForce (TASK_ID tid);
extern STATUS 	taskSuspend (TASK_ID tid);
extern STATUS 	taskResume (TASK_ID tid);
extern STATUS 	taskRestart (TASK_ID tid);
extern STATUS 	taskPrioritySet (TASK_ID tid, int newPriority);
extern STATUS 	taskPriorityGet (TASK_ID tid, int *pPriority);
#ifdef __multi_core__
extern STATUS taskCpuLock (void);
extern STATUS taskCpuUnlock (void);
#define taskLock taskCpuLock
#define taskUnlock taskCpuUnlock
#else
extern STATUS 	taskLock (void);
extern STATUS 	taskUnlock (void);
#define taskCpuLock taskLock
#define taskCpuUnlock taskUnlock
#endif

extern STATUS 	taskSafe (void);
extern STATUS 	taskUnsafe (void);
extern STATUS 	taskDelay (int ticks);
extern STATUS	taskUndelay (TASK_ID tid);
extern TASK_ID	taskIdSelf (void);
extern int		taskIdListGet (TASK_ID idList [ ], int maxTasks);
extern WIND_TCB *taskTcb (TASK_ID tid);
extern BOOL 	taskIsReady (TASK_ID tid);
extern BOOL     taskIsPended(TASK_ID tid);
extern BOOL 	taskIsSuspended (TASK_ID tid);
extern BOOL     taskIsDelayed(TASK_ID tid, int * pDelayTicks);
extern STATUS   taskRegsGet(TASK_ID tid, REG_SET *pRegs );
extern char * 	taskName(TASK_ID tid);
extern TASK_ID	taskNameToId (char *name);
extern STATUS 	taskInfoGet (TASK_ID tid, TASK_DESC *pTaskDesc);
extern STATUS 	taskIdVerify (TASK_ID tid);
extern STATUS   taskOptionsGet(TASK_ID tid, int *pOptions);
extern STATUS   taskOptionsSet(TASK_ID tid, int mask, int newOptions);
extern STATUS   taskStatusString(TASK_ID  tid, char *pString);

#ifdef __multi_core__
extern STATUS taskCpuAffinitySet (TASK_ID tid, cpuset_t newAffinity);
extern STATUS taskCpuAffinityGet (TASK_ID tid, cpuset_t*  pAffinity);
#endif /* __multi_core__ */

extern void vx_fp_task_always_on(int is_on);

#else	

/* __STDC__ */



extern STATUS 	taskLibInit ();
extern int 	taskSpawn ();
extern STATUS 	taskInit ();

extern STATUS 	taskActivate ();
extern STATUS 	taskDelete ();
extern STATUS 	taskDeleteForce ();
extern STATUS 	taskSuspend ();
extern STATUS 	taskResume ();
extern STATUS 	taskRestart ();
extern STATUS 	taskPrioritySet ();
extern STATUS 	taskPriorityGet ();
extern STATUS 	taskLock ();
extern STATUS 	taskUnlock ();
extern STATUS 	taskSafe ();
extern STATUS 	taskUnsafe ();
extern STATUS 	taskDelay ();
extern STATUS	taskUndelay();
extern BOOL 	taskIsReady ();
extern BOOL 	taskIsSuspended ();
extern WIND_TCB *taskTcb ();
extern int 	taskIdListGet ();
//extern STATUS 	taskRegsSet ();
//extern void 	taskRegsShow ();
//extern void *	taskStackAllot ();
//extern void 	taskShowInit ();
extern int 	taskNameToId ();
extern STATUS 	taskInfoGet ();
extern STATUS 	taskIdVerify ();
extern void vx_fp_task_always_on();
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* taskLib_h */
