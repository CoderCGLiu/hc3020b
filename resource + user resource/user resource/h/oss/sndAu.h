/* sndAu.h - Au audio support header file */

/* Copyright 2000 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,06dec00,jlb  Fixed spelling for auHeaderRead
01a,14nov00,jlb  written
*/


#ifndef __INCsndauh
#define __INCsndauh


#ifdef __cplusplus
extern "C" {
#endif

/*

DESCRIPTION 
 
This file defines miscellaneous definitions to process Au format audio. 

 
*/
#define bswapl(x) ((((x) & 0xff) << 24) |\
		   (((x) & 0xff00) << 8) |\
		   (((x) & 0xff0000) >> 8) |\
		   (((x) >> 24) & 0xff))

/* Au header */
typedef  struct
{   u32     magic;          /* magic field - identifier (.snd) */
	u32     offset;         /* offset to data */
	u32     size;           /* size of audio sample */
	u32     encoding;       /* encoding */
    u32     rate;           /* sample rate */
    u32     channels;       /* number of channels */
} AUFORMAT_HDR;

#define AU_HEADER_SIZE        sizeof (AUFORMAT_HDR)

#define AU_MAGIC_NUMBER       0x2e736e64         /* ASCII .snd */
#define AU_UNKNOWN_SIZE       ((unsigned)(~0))	 /* Unspecified data size */

/* Type of AU forms */
#define AU_FMT_ULAW             1   /* u-law encoding */
#define AU_FMT_LIN_8            2   /* Linear 8 bits */
#define AU_FMT_LIN_16           3   /* Linear 16 bits */
#define AU_FMT_LIN_24           4   /* Linear 24 bits */
#define AU_FMT_LIN_32           5   /* Linear 32 bits */
#define AU_FMT_FLOAT            6   /* IEEE FP 32 bits */
#define AU_FMT_DOUBLE           7   /* IEEE FP 64 bits */
#define AU_FMT_G721             23  /* CCITT G.721 4-bits ADPCM */
#define AU_FMT_G723_3           25  /* CCITT G.723 3-bits ADPCM */
#define AU_FMT_G723_5           26  /* CCITT G.723 5-bits ADPCM */



int auHeaderRead (int fd, u32 *pChannels, u32 *pSampleRate,
                      u32 *pEncoding, u32 *pSamples, u32 *pDataStart);
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif  /* __INCsndwavh */
