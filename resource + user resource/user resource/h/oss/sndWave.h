/* sndWave.h - waveform audio support header file */

/* Copyright 2000 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,15mar00,jlb  written
*/


#ifndef __INCsndwavh
#define __INCsndwavh


#ifdef __cplusplus
extern "C" {
#endif

/* Type of wave forms */
#define WAVE_FORMAT_PCM     1
/*

DESCRIPTION 
 
This file defines miscellaneous definitions to process wave form audio. 

 
*/

/* Waveform header */
struct RIFF_HEADER
{
	u8 szRiffID[4];  /* 'R','I','F','F'*/
	u32 dwRiffSize;
	u8 szRiffFormat[4]; /* 'W','A','V','E'*/
};
typedef  struct
{	u_long     size;
	u_short    formatTag;           /* type of wave form data */
	u_short    channels;            /* number of audio channels */
	u_long     samplesPerSec;       /* audio samples per second */
	u_long     avgBytesPerSec;      /* average transfer rate */
	u_short    blockAlign;          /* bytes required for a single sample */
	u_short    bitsPerSample;       /* bits per sample */
} WAVEFORMAT;


struct WAVE_FORMAT
{
	u16 wFormatTag;
	u16 wChannels;
	u32 dwSamplesPerSec;
	u32 dwAvgBytesPerSec;
	u16 wBlockAlign;
	u16 wBitsPerSample;
};

struct FMT_BLOCK
{
	u8  szFmtID[4]; /* 'f','m','t',' '*/
	u32  dwFmtSize;
	struct WAVE_FORMAT wavFormat;
}; 

struct DATA_BLOCK
{
	u8 szDataID[4]; /* 'd','a','t','a'*/
	u32 dwDataSize;
}; 


#define WAV_HEADER_LEN	(sizeof(struct RIFF_HEADER) + sizeof(struct FMT_BLOCK) + sizeof(struct DATA_BLOCK))

int wavHeaderRead (int fd, int *pChannels, u32 *pSampleRate,
                      int *pSampleBits, u32 *pSamples, u32 *pDataStart);
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif  /* __INCsndwavh */
