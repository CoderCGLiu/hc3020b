#ifndef BUILDID_H
#define BUILDID_H
#ifdef __cplusplus
extern "C"
{
#endif 
	
#define OSS_BUILD_ID "rc3"
#define __OPENOSS__
#define OSS_LICENSE "GPL"

#ifdef __cplusplus
}
#endif 
#endif
