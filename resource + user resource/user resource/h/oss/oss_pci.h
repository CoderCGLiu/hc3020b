/*
 * Purpose: Definitions of various PCI specific constants and functions
 *
 * All drivers for PCI devices should included this file.
 */
/*
 *
 * This file is part of Open Sound System.
 *
 * Copyright (C) 4Front Technologies 1996-2008.
 *
 * This this source file is released under GPL v2 license (no other versions).
 * See the COPYING file included in the main directory of this source
 * distribution for the license terms and conditions.
 *
 */
#ifndef __arm__
#ifndef OSS_PCI_H
#define OSS_PCI_H
#ifdef __cplusplus
extern "C"
{
#endif 
#define OSS_HAVE_PCI
#define PCIBIOS_SUCCESSFUL		0x00
#define PCIBIOS_FAILED			-1

#define PCI_CLASS_MULTIMEDIA_AUDIO	0x0401
#define PCI_CLASS_MULTIMEDIA_OTHER	0x0480

extern int oss_pci_read_config_byte (oss_device_t * osdev, offset_t where,
				 unsigned char *val);
extern int oss_pci_read_config_irq (oss_device_t * osdev, offset_t where,
				unsigned char *val);
extern int oss_pci_read_config_word (oss_device_t * osdev, offset_t where,
				 unsigned short *val);
extern int oss_pci_read_config_dword (oss_device_t * osdev, offset_t where,
				  unsigned int *val);
extern int oss_pci_write_config_byte (oss_device_t * osdev, offset_t where,
				  unsigned char val);
extern int oss_pci_write_config_word (oss_device_t * osdev, offset_t where,
				  unsigned short val);
extern int oss_pci_write_config_dword (oss_device_t * osdev, offset_t where,
				   unsigned int val);
#ifdef __cplusplus
}
#endif 
#endif
#endif
