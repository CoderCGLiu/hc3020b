/*
 * Purpose: OSS version ID
 */
#ifndef OSS_VERSION_H
#define OSS_VERSION_H
#ifdef __cplusplus
extern "C"
{
#endif 
#include "buildid.h"
#include "timestamp.h"

#ifndef OSS_LICENSE
#define OSS_LICENSE "" /* Empty means commercial license */
#endif 

#define OSS_VERSION_ID "4.2"
#define OSS_VERSION_STRING OSS_VERSION_ID " (b " OSS_BUILD_ID "/" OSS_COMPILE_DATE ")"
#define OSS_INTERNAL_VERSION 0x040199
#ifdef __cplusplus
}
#endif 
#endif
