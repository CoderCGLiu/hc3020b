/*
   (c) Copyright 2001-2009  The world wide DirectFB Open Source Community (directfb.org)
   (c) Copyright 2000-2004  Convergence (integrated media) GmbH

   All rights reserved.

   Written by Denis Oliver Kropp <dok@directfb.org>,
              Andreas Hundt <andi@fischlustig.de>,
              Sven Neumann <neo@directfb.org>,
              Ville Syrjälä <syrjala@sci.fi> and
              Claudio Ciccani <klan@users.sf.net>.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#ifndef __DIRECT__MESSAGES_H__
#define __DIRECT__MESSAGES_H__

#include <direct/build.h>
#include <direct/types.h>


#if __GNUC__ >= 3
#define D_FORMAT_PRINTF(n)         __attribute__((__format__ (__printf__, n, n+1)))
#else
#define D_FORMAT_PRINTF(n)
#endif

typedef enum {
     DMT_NONE           = 0x00000000, /* No message type. */

     DMT_BANNER         = 0x00000001, /* Startup banner. */
     DMT_INFO           = 0x00000002, /* Info messages. */
     DMT_WARNING        = 0x00000004, /* Warnings. */
     DMT_ERROR          = 0x00000008, /* Error messages: regular, with DFBResult, bugs,
                                         system call errors, dlopen errors */
     DMT_UNIMPLEMENTED  = 0x00000010, /* Messages notifying unimplemented functionality. */
     DMT_ONCE           = 0x00000020, /* One-shot messages .*/

     DMT_ALL            = 0x0000003f  /* All types. */
} DirectMessageType;

     #define D_INFO(x...)          do { } while (0)
     #define D_ERROR(x...)         do { } while (0)
     #define D_DERROR(x...)        do { } while (0)
     #define D_PERROR(x...)        do { } while (0)
     #define D_DLERROR(x...)       do { } while (0)
     #define D_ONCE(x...)          do { } while (0)
     #define D_UNIMPLEMENTED()     do { } while (0)
     #define D_BUG(x...)           do { } while (0)
     #define D_WARN(x...)          do { } while (0)
     #define D_OOM()               (printf("out of memory\n"), DR_NOLOCALMEMORY)

#endif

