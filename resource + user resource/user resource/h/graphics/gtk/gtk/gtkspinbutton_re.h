#ifndef __GTK_SPIN_BUTTON_Re_H__
#define __GTK_SPIN_BUTTON_Re_H__


#include <gdk/gdk.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkadjustment.h>



#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_SPIN_BUTTON_RE                  (gtk_spin_button_re_get_type ())
#define GTK_SPIN_BUTTON_RE(obj)                  (GTK_CHECK_CAST ((obj), GTK_TYPE_SPIN_BUTTON_RE, GtkSpinButton_Re))
#define GTK_SPIN_BUTTON_RE_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_SPIN_BUTTON_RE, GtkSpinButtonClass_Re))
#define GTK_IS_SPIN_BUTTON_RE(obj)               (GTK_CHECK_TYPE ((obj), GTK_TYPE_SPIN_BUTTON_RE))
#define GTK_IS_SPIN_BUTTON_RE_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_SPIN_BUTTON_RE))
#define GTK_SPIN_BUTTON_RE_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_SPIN_BUTTON_RE, GtkSpinButtonClass_Re))

#define GTK_INPUT_ERROR -1

typedef enum
{
  GTK_UPDATE_ALWAYS_Re,
  GTK_UPDATE_IF_VALID_Re
} GtkSpinButtonReUpdatePolicy;

typedef enum
{
  GTK_SPIN_STEP_FORWARD_Re,
  GTK_SPIN_STEP_BACKWARD_Re,
  GTK_SPIN_PAGE_FORWARD_Re,
  GTK_SPIN_PAGE_BACKWARD_Re,
  GTK_SPIN_HOME_Re,
  GTK_SPIN_END_Re,
  GTK_SPIN_USER_DEFINED_Re
} GtkSpinReType;

typedef struct _GtkSpinButton_Re	    GtkSpinButton_Re;
typedef struct _GtkSpinButtonClass_Re  GtkSpinButtonClass_Re;


struct _GtkSpinButton_Re
{
  GtkEntry entry;
  
  GtkAdjustment *adjustment;
  
  GdkWindow *inc_panel;
  GdkWindow *dec_panel;
  
  guint32 timer;
  
  gdouble climb_rate;
  gdouble timer_step;
  
  GtkSpinButtonReUpdatePolicy update_policy;
  
  guint in_child : 2;
  guint click_child : 3; /* valid: GTK_ARROW_UP=0, GTK_ARROW_DOWN=1 or 2=NONE/BOTH */
  guint button : 2;
  guint need_timer : 1;
  guint timer_calls : 3;
  guint digits : 10;
  guint numeric : 1;
  guint wrap : 1;
  guint snap_to_ticks : 1;
};

struct _GtkSpinButtonClass_Re
{
  GtkEntryClass parent_class;

  gint (*input)  (GtkSpinButton_Re *spin_button,
		  gdouble       *new_value);
  gint (*output) (GtkSpinButton_Re *spin_button);
  void (*value_changed) (GtkSpinButton_Re *spin_button);

  /* Action signals for keybindings, do not connect to these */
  void (*change_value) (GtkSpinButton_Re *spin_button,
			GtkScrollType  scroll);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GtkType		gtk_spin_button_re_get_type	   (void) G_GNUC_CONST;

void		gtk_spin_button_re_configure	   (GtkSpinButton_Re  *spin_button,
						    GtkAdjustment  *adjustment,
						    gdouble	    climb_rate,
						    guint	    digits);

GtkWidget*	gtk_spin_button_re_new		   (GtkAdjustment  *adjustment,
						    gdouble	    climb_rate,
						    guint	    digits);

GtkWidget*	gtk_spin_button_re_new_with_range	   (gdouble  min,
						    gdouble  max,
						    gdouble  step);

void		gtk_spin_button_re_set_adjustment	   (GtkSpinButton_Re  *spin_button,
						    GtkAdjustment  *adjustment);

GtkAdjustment*	gtk_spin_button_re_get_adjustment	   (GtkSpinButton_Re  *spin_button);

void		gtk_spin_button_re_set_digits	   (GtkSpinButton_Re  *spin_button,
						    guint	    digits);
guint           gtk_spin_button_re_get_digits         (GtkSpinButton_Re  *spin_button);

void		gtk_spin_button_re_set_increments	   (GtkSpinButton_Re  *spin_button,
						    gdouble         step,
						    gdouble         page);
void            gtk_spin_button_re_get_increments     (GtkSpinButton_Re  *spin_button,
						    gdouble        *step,
						    gdouble        *page);

void		gtk_spin_button_re_set_range	   (GtkSpinButton_Re  *spin_button,
						    gdouble         min,
						    gdouble         max);
void            gtk_spin_button_re_get_range          (GtkSpinButton_Re  *spin_button,
						    gdouble        *min,
						    gdouble        *max);

gdouble		gtk_spin_button_re_get_value          (GtkSpinButton_Re  *spin_button);

gint		gtk_spin_button_re_get_value_as_int   (GtkSpinButton_Re  *spin_button);

void		gtk_spin_button_re_set_value	   (GtkSpinButton_Re  *spin_button, 
						    gdouble	    value);

void		gtk_spin_button_re_set_update_policy  (GtkSpinButton_Re  *spin_button,
						    GtkSpinButtonReUpdatePolicy  policy);
GtkSpinButtonReUpdatePolicy gtk_spin_button_re_get_update_policy (GtkSpinButton_Re *spin_button);

void		gtk_spin_button_re_set_numeric	   (GtkSpinButton_Re  *spin_button,
						    gboolean	    numeric);
gboolean        gtk_spin_button_re_get_numeric        (GtkSpinButton_Re  *spin_button);

void		gtk_spin_button_re_spin		   (GtkSpinButton_Re  *spin_button,
						    GtkSpinReType     direction,
						    gdouble	    increment);

void		gtk_spin_button_re_set_wrap	   (GtkSpinButton_Re  *spin_button,
						    gboolean	    wrap);
gboolean        gtk_spin_button_re_get_wrap           (GtkSpinButton_Re  *spin_button);

void		gtk_spin_button_re_set_snap_to_ticks  (GtkSpinButton_Re  *spin_button,
						    gboolean	    snap_to_ticks);
gboolean        gtk_spin_button_re_get_snap_to_ticks  (GtkSpinButton_Re  *spin_button);
void            gtk_spin_button_re_update             (GtkSpinButton_Re  *spin_button);


#ifndef GTK_DISABLE_DEPRECATED
#define gtk_spin_button_re_get_value_as_float gtk_spin_button_re_get_value
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ECCT_SPAN_BUTTON_H_ */
