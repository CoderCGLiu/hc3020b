#ifndef __GTK_COLOR_SELECTION_H__
#define __GTK_COLOR_SELECTION_H__

#include <gdk/gdk.h>
//#include <gtk/gtkdialog.h>
#include <gtk/gtkvbox.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_COLOR_SELECTION             (gtk_color_selection_get_type ())
#define GTK_COLOR_SELECTION(obj)             (GTK_CHECK_CAST ((obj), GTK_TYPE_COLOR_SELECTION, GtkColorSelection))
#define GTK_COLOR_SELECTION_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_COLOR_SELECTION, GtkColorSelection_Class))
#define GTK_IS_COLOR_SELECTION(obj)          (GTK_CHECK_TYPE ((obj), GTK_TYPE_COLOR_SELECTION))
#define GTK_IS_COLOR_SELECTION_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_COLOR_SELECTION))
#define GTK_COLOR_SELECTION_GET_CLASS(obj)   (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_COLOR_SELECTION, GtkColorSelection_Class))

typedef struct _GtkColorSelection           GtkColorSelection;
typedef struct _GtkColorSelection_Class     GtkColorSelection_Class;

typedef void (* GtkColorSelectionChangePaletteFunc)(const GdkColor *colors, gint n_colors);

struct _GtkColorSelection
{
    GtkVBox     parent_instance;
        
    GtkWidget*  paletteFrame;
    GtkWidget*  paletteTable;
    gboolean    hasPalette;
    GdkColor*   paletteColors;
    gint        n_paletteColors;
    
    GtkWidget*  previewFrame;
    GtkWidget*  previewDrawingArea;
    GtkWidget*  previewEntry;
    
    GtkWidget*  customFrame;
    GtkWidget*  rAdjustment;
    GtkWidget*  gAdjustment;
    GtkWidget*  bAdjustment;
    
    GtkWidget*  spacer0;
    GtkWidget*  spacer1;
    
    //GtkWidget*  okButton;
    //GtkWidget*  cancelButton;
    
    GdkColor*   currentColor;
    GdkColor*   previousColor;
};

struct _GtkColorSelection_Class
{
    GtkVBoxClass  parent_class;

    void (*color_changed)   (GtkColorSelection *color_selection);

    /* Padding for future expansion */
    void (*_gtk_reserved1)  (void);
    void (*_gtk_reserved2)  (void);
    void (*_gtk_reserved3)  (void);
    void (*_gtk_reserved4)  (void);
};

GtkType     gtk_color_selection_get_type                (void) G_GNUC_CONST;
GtkWidget*  gtk_color_selection_new                     (void);

void        gtk_color_selection_set_current_color       (GtkColorSelection *colorsel, const GdkColor *color);
void        gtk_color_selection_get_current_color       (GtkColorSelection *colorsel, GdkColor *color);

void        gtk_color_selection_set_previous_color      (GtkColorSelection *colorsel, const GdkColor *color);
void        gtk_color_selection_get_previous_color      (GtkColorSelection *colorsel, GdkColor *color);

void        gtk_color_selection_set_has_palette         (GtkColorSelection *colorsel, gboolean has_palette);
gboolean    gtk_color_selection_get_has_palette         (GtkColorSelection *colorsel);

boolean     gtk_color_selection_set_palette_string      (GtkColorSelection *colorsel, const gchar *paletteString);
gchar*      gtk_color_selection_get_palette_string      (GtkColorSelection *colorsel);

gboolean    gtk_color_selection_palette_from_string     (const gchar *str, GdkColor **colors, gint *n_colors);
gchar*      gtk_color_selection_palette_to_string       (const GdkColor *colors, gint n_colors);

void       gtk_color_selection_set_has_opacity_control  (GtkColorSelection *colorsel, gboolean has_opacity);
gboolean   gtk_color_selection_get_has_opacity_control  (GtkColorSelection *colorsel);

void        gtk_color_selection_set_current_alpha       (GtkColorSelection *colorsel, guint16 alpha);
guint16     gtk_color_selection_get_current_alpha       (GtkColorSelection *colorsel);

void        gtk_color_selection_set_previous_alpha      (GtkColorSelection *colorsel, guint16 alpha);
guint16     gtk_color_selection_get_previous_alpha      (GtkColorSelection *colorsel);

gboolean    gtk_color_selection_is_adjusting            (GtkColorSelection *colorsel);

#ifndef GTK_DISABLE_DEPRECATED
/* Deprecated calls: */
void        gtk_color_selection_set_color               (GtkColorSelection *colorsel, gdouble *color);
void        gtk_color_selection_get_color               (GtkColorSelection *colorsel, gdouble *color);
void        gtk_color_selection_set_update_policy       (GtkColorSelection *colorsel, GtkUpdateType policy);
#endif /* GTK_DISABLE_DEPRECATED */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __gtk_color_selection_H__ */
