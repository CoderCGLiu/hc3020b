
/*-------------------------------------------------------------------------+
|
|        文件名：GtkCanvas.h
|          描述：Gtk中自定义的画布控件，参考GtkDrawingArea实现。通过该控件，
|                用户可以直接利用DirectFB后端实现绘图等2D操作。
|        版本号：1.0
|      修改记录：
|  软件版权所有：中国电子科技集团公司第三十二研究所
|          备注：
+--------------------------------------------------------------------------*/

#ifndef __GTK_CANVAS_H__
#define __GTK_CANVAS_H__

#include "gdk/gdkdirectfb.h"
#include "gdk/gdkprivate-directfb.h"
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C"{
#endif

#define GTK_TYPE_CANVAS					(gtk_canvas_get_type())
#define GTK_CANVAS(obj)					(GTK_CHECK_CAST ((obj), GTK_TYPE_CANVAS, GtkCanvas))
#define GTK_CANVAS_CLASS(kclass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_CANVAS, GtkCanvasClass))
#define GTK_IS_CANVAS(obj)				(GTK_CHECK_TYPE ((obj), GTK_TYPE_CANVAS))
#define GTK_IS_CANVAS_CLASS(kclass)		(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_CANVAS))
#define GTK_CANVAS_GET_CLASS(obj)		(GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_CANVAS, GtkCanvasClass))

typedef struct _GtkCanvas		       	GtkCanvas;
typedef struct _GtkCanvasClass			GtkCanvasClass;

struct _GtkCanvas
{
	GtkWidget widget;
};

struct _GtkCanvasClass
{
	GtkWidgetClass parent_class;
};

IDirectFBSurface*	gtk_canvas_get_surface(GtkCanvas *canvas);
GtkType 			gtk_canvas_get_type	(void);
GtkWidget* 			gtk_canvas_new        (void);

#ifdef 	__cplusplus
}
#endif

#endif /* __GTK_CANVAS_H__ */

