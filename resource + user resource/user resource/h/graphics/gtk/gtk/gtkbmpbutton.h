/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_BMP_BUTTON_H__
#define __GTK_BMP_BUTTON_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_BMPBUTTON(obj)          GTK_CHECK_CAST (obj, gtk_bmpbutton_get_type (), GtkBmpButton)
#define GTK_BMPBUTTON_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_bmpbutton_get_type (), GtkBmpButtonClass)
#define GTK_IS_BMPBUTTON(obj)       GTK_CHECK_TYPE (obj, gtk_bmpbutton_get_type ())


typedef struct _GtkBmpButton        GtkBmpButton;
typedef struct _GtkBmpButtonClass   GtkBmpButtonClass;

struct _GtkBmpButton
{
  GtkWidget widget;

  // normal background bitmap of button
  GtkWidget* normal_bg;

  // select background bitmap of button
  GtkWidget* select_bg;

  // icon of button
  GtkWidget* icon;

  // display text of button
  gchar* text;

  // is pressed
  gboolean pressed;

  // is enable
  gboolean enable;

  // is activate
  gboolean active;

  // layout for draw text
  PangoLayout* layout;
};

struct _GtkBmpButtonClass
{
  GtkWidgetClass parent_class;

  void (* on_bmpbtn_clicked) (GtkBmpButton *widget, gpointer data);
};


GtkWidget* 	gtk_bmpbutton_new ();
GtkWidget* 	gtk_bmpbutton_new_with_string (gchar* text);

GtkType 	gtk_bmpbutton_get_type (void);

GtkWidget* 	gtk_bmpbutton_get_normal_bg (GtkBmpButton *bmpbutton);
GtkWidget* 	gtk_bmpbutton_get_select_bg (GtkBmpButton *bmpbutton);
GtkWidget* 	gtk_bmpbutton_get_icon (GtkBmpButton *bmpbutton);
gchar* 		gtk_bmpbutton_get_text (GtkBmpButton *bmpbutton);

void 		gtk_bmpbutton_set_text (GtkBmpButton *bmpbutton, gchar* text);
void 		gtk_bmpbutton_set_normal_bg (GtkBmpButton *bmpbutton, GtkWidget* normal_bg);
void 		gtk_bmpbutton_set_select_bg (GtkBmpButton *bmpbutton, GtkWidget* select_bg);
void 		gtk_bmpbutton_set_icon (GtkBmpButton *bmpbutton, GtkWidget* icon);

void 		gtk_bmpbutton_set_enable (GtkBmpButton *bmpbutton, gboolean flag);
void 		gtk_bmpbutton_set_active (GtkBmpButton *bmpbutton, gboolean flag);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // __GTK_BMP_BUTTON_H__
