#ifndef __GTK_FILESEL_H__
#define __GTK_FILESEL_H__

#include <gtk/gtkdialog.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkentry.h>
#include <gtk/gtktooltips.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_FILE_SELECTION            (gtk_file_selection_get_type ())
#define GTK_FILE_SELECTION(obj)            (GTK_CHECK_CAST ((obj), GTK_TYPE_FILE_SELECTION, GtkFileSelection))
#define GTK_FILE_SELECTION_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_FILE_SELECTION, GtkFileSelectionClass))
#define GTK_IS_FILE_SELECTION(obj)         (GTK_CHECK_TYPE ((obj), GTK_TYPE_FILE_SELECTION))
#define GTK_IS_FILE_SELECTION_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_FILE_SELECTION))
#define GTK_FILE_SELECTION_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_FILE_SELECTION, GtkFileSelectionClass))

typedef struct _GtkFileSelection       GtkFileSelection;
typedef struct _GtkFileSelectionClass  GtkFileSelectionClass;

typedef struct _GtkFileSelectionFilter  GtkFileSelectionFilter;

struct _GtkFileSelection
{
    GtkDialog       parent_instance;
    
//    GtkOptionMenu   *historyPulldown;
    GtkOptionMenu   *history_pulldown;
//    GtkMenu         *historyMenu;
    GtkMenu         *history_menu;
    GtkButton       *createDirBtn;
    GtkButton       *deleteBtn;
    GtkTreeView     *list;
//    GtkLabel        *selectionText;
    GtkLabel        *selection_text;
//    GtkEntry        *selectionEntry;
    GtkEntry        *selection_entry;
//    GtkButton       *okBtn;
    GtkButton       *ok_button;
//    GtkButton       *cancelBtn;
    GtkButton       *cancel_button;
    GtkTooltips     *tooltips;
    
    gchar           *currentDir;
    gchar           *selectedFile;
    gchar           **selectedItems;
    
    GtkDialog       *createDirDialog;
    GtkEntry        *createDirNameEntry;
    GtkDialog       *deleteItemDialog;
    GtkLabel        *deleteItemLabel;
    GtkDialog       *operationFailedDialog;
    GtkLabel        *operationFailedLabel;
    
    gchar           *lastSelectedDir;   // directory saved here is UTF-8 encoded, which is only used for GtkTreeView without output
    gboolean        selectTwice;
    
    GtkFileSelectionFilter
                    *file_filter;
    GtkOptionMenu   *filter_pulldown;
    GtkMenu         *filter_menu;
    gchar           **current_extensions;
};

struct _GtkFileSelectionClass
{
  GtkDialogClass parent_class;

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};

GtkType    gtk_file_selection_get_type            (void) G_GNUC_CONST;
GtkWidget* gtk_file_selection_new                 (void);
GtkWidget* gtk_file_selection_new_with_title      (const gchar      *title);
void       gtk_file_selection_set_filename        (GtkFileSelection *filesel,
                           const gchar      *filename);
/* This function returns the selected filename in the C runtime's
 * multibyte string encoding, which may or may not be the same as that
 * used by GDK (UTF-8). To convert to UTF-8, call g_filename_to_utf8().
 * The returned string points to a statically allocated buffer and
 * should be copied away.
 */
G_CONST_RETURN gchar* gtk_file_selection_get_filename        (GtkFileSelection *filesel);

void       gtk_file_selection_complete        (GtkFileSelection *filesel,
                           const gchar      *pattern);
void       gtk_file_selection_show_fileop_buttons (GtkFileSelection *filesel);
void       gtk_file_selection_hide_fileop_buttons (GtkFileSelection *filesel);
gchar**    gtk_file_selection_get_selections      (GtkFileSelection *filesel);
void       gtk_file_selection_set_select_multiple (GtkFileSelection *filesel,
                           gboolean          select_multiple);
gboolean   gtk_file_selection_get_select_multiple (GtkFileSelection *filesel);

struct _GtkFileSelectionFilter
{
    GList* file_extension_list;
    GList* file_type_description_list;
    gboolean show_accept_all_filter;
};

/* gtk_file_selection_filter_append, gtk_file_selection_filter_prepend:
 * @filesel: a GtkFileSelection pointer to add item into filter list
 * @first_extension: the file extension for the first filter item to add
 * @Varargs: the description for the first filter item, then additional filters, ending with %NULL
 * 
 * Add filter item(s) into the file filter list of a #GtkFileSelection. First argument #filesel is a pointer of file selection.
 * then pairs of file extension and description should be given, with a %NULL pointer ending the list.
 * Note: file extensions can be connected using commas
 * Here's a simple example:
 *      gtk_file_selection_filter_append( filesel, 
 *                                        "txt",              "text files",
 *                                        "bmp,jpeg,gif,png", "image files",
 *                                        NULL );
 **/
/* @first_extension and latter arguments should be UTF-8 encoded.  */
void        gtk_file_selection_filter_append    (GtkFileSelection* filesel, const gchar* first_extension, ...);
/* @first_extension and latter arguments should be UTF-8 encoded.  */
void        gtk_file_selection_filter_prepend   (GtkFileSelection* filesel, const gchar* first_extension, ...);
void        gtk_file_selection_filter_clear     (GtkFileSelection* filesel);
void        gtk_file_selection_filter_set_accept_all_filter (GtkFileSelection* filesel, gboolean value);
gboolean    gtk_file_selection_filter_get_accept_all_filter (GtkFileSelection* filesel);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_FILESEL_H__ */
