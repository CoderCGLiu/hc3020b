/*
 *  This file registers the FreeType modules compiled into the library.
 *
 *  If you use GNU make, this file IS NOT USED!  Instead, it is created in
 *  the objects directory (normally `<topdir>/objs/') based on information
 *  from `<topdir>/modules.cfg'.
 *
 *  Please read `docs/INSTALL.ANY' and `docs/CUSTOMIZE' how to compile
 *  FreeType without GNU make.
 *
 */

FT_USE_MODULE(autofit_module_class)	// 没有字体会难看：扭曲
FT_USE_MODULE(tt_driver_class)		// TrueType 矢量字体
FT_USE_MODULE(sfnt_module_class)	// 没有，TrueType会失败

FT_USE_MODULE(ft_raster1_renderer_class)
FT_USE_MODULE(ft_smooth_renderer_class)			// 没有，字无法显示


/* EOF */
