/*-------------------------------------------------------------------------+
|
|        文件名：iconv.h
|          描述：声明字符集间转换函数
|        版本号：1.0
|      修改记录：
|  软件版权所有：中国电子科技集团公司第三十二研究所
|          备注：
|
+--------------------------------------------------------------------------*/
#ifndef _LIBICONV_H
#define _LIBICONV_H

#define _LIBICONV_VERSION 0x010D    /* version number: (major<<8) + minor */
extern int _libiconv_version; /* Likewise */

#undef iconv_t
#define iconv_t libiconv_t
typedef void *iconv_t;

#include <stddef.h>

#include <errno.h>

#ifndef EILSEQ
#define EILSEQ
#endif


#ifdef __cplusplus
extern "C"
{
#endif

#ifndef LIBICONV_PLUG
#define iconv_open libiconv_open
#endif
extern iconv_t iconv_open(const char *tocode, const char *fromcode);

#ifndef LIBICONV_PLUG
#define iconv libiconv
#endif
extern size_t iconv(iconv_t cd, char **inbuf, size_t *inbytesleft, char **outbuf, size_t *outbytesleft);

#ifndef LIBICONV_PLUG
#define iconv_close libiconv_close
#endif
extern int iconv_close(iconv_t cd);

#ifndef LIBICONV_PLUG

typedef struct
  {
    void *dummy1[28];
  } iconv_allocation_t;

#define iconv_open_into libiconv_open_into
extern int iconv_open_into(const char *tocode, const char *fromcode,
    iconv_allocation_t *resultp);

  /* Control of attributes. */
#define iconvctl libiconvctl
extern int iconvctl(iconv_t cd, int request, void *argument);

#define iconv_convert libiconv_convert
extern int iconv_convert(char *from_charset, char *to_charset, char *inbuf,
    int inlen, char *outbuf, int outlen);
extern char *iconv_gbk2utf8(char *inbuf);
extern char * iconv_utf82gbk(char *inbuf);
extern void iconv_destroy(char *ptr);

  /* Hook performed after every successful conversion of a Unicode character. */
typedef void(*iconv_unicode_char_hook)(unsigned int uc, void *data);
  /* Hook performed after every successful conversion of a wide character. */
typedef void(*iconv_wide_char_hook)(wchar_t wc, void *data);
  /* Set of hooks. */
struct iconv_hooks
{
  iconv_unicode_char_hook uc_hook;
  iconv_wide_char_hook wc_hook;
  void *data;
};

typedef void(*iconv_unicode_mb_to_uc_fallback)(const char *inbuf, size_t
    inbufsize, void(*write_replacement)(const unsigned int *buf, size_t buflen,
    void *callback_arg), void *callback_arg, void *data);

typedef void(*iconv_unicode_uc_to_mb_fallback)(unsigned int code, void
    (*write_replacement)(const char *buf, size_t buflen, void *callback_arg),
    void *callback_arg, void *data);

#if 1
typedef void(*iconv_wchar_mb_to_wc_fallback)(const char *inbuf, size_t
    inbufsize, void(*write_replacement)(const wchar_t *buf, size_t buflen, void
    *callback_arg), void *callback_arg, void *data);

typedef void(*iconv_wchar_wc_to_mb_fallback)(wchar_t code, void
    (*write_replacement)(const char *buf, size_t buflen, void *callback_arg),
    void *callback_arg, void *data);
#else
  /* If the wchar_t type does not exist, these two fallback functions are never
  invoked.  Their argument list therefore does not matter.  */
  typedef void(*iconv_wchar_mb_to_wc_fallback)();
  typedef void(*iconv_wchar_wc_to_mb_fallback)();
#endif

  /* Set of fallbacks. */
struct iconv_fallbacks
{
  iconv_unicode_mb_to_uc_fallback mb_to_uc_fallback;
  iconv_unicode_uc_to_mb_fallback uc_to_mb_fallback;
  iconv_wchar_mb_to_wc_fallback mb_to_wc_fallback;
  iconv_wchar_wc_to_mb_fallback wc_to_mb_fallback;
  void *data;
};

  /* Requests for iconvctl. */
#define ICONV_TRIVIALP            0  /* int *argument */
#define ICONV_GET_TRANSLITERATE   1  /* int *argument */
#define ICONV_SET_TRANSLITERATE   2  /* const int *argument */
#define ICONV_GET_DISCARD_ILSEQ   3  /* int *argument */
#define ICONV_SET_DISCARD_ILSEQ   4  /* const int *argument */
#define ICONV_SET_HOOKS           5  /* const struct iconv_hooks *argument */
#define ICONV_SET_FALLBACKS       6  /* const struct iconv_fallbacks *argument */


#define iconvlist libiconvlist
extern void iconvlist(int(*do_one)(unsigned int namescount, const char*const * names, void *data), void *data);

extern const char *iconv_canonicalize(const char *name);

extern void libiconv_set_relocation_prefix(const char *orig_prefix, const char *curr_prefix);

#endif


#ifdef __cplusplus
}

#endif


#endif /* _LIBICONV_H */
