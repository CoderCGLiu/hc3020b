#ifndef _FFX_CONFIG_H_
#define _FFX_CONFIG_H_

#ifdef __cplusplus
extern "C"
{
#endif 
#include <reworks/list.h>              
//#include <vxworks.h>

/* NORFLASH_FIM  */
/* 枚举结构USER_FIM_TYPE中定义的是Flash的FIM类型*/
typedef enum {
 
	FIM_asux8 = 0x01,
	FIM_asux8_2,
	FIM_asu4x8,
	FIM_asu4x8_2,
	FIM_ambx16_2,
	FIM_ambx16,
	FIM_amb2x16_2,
	FIM_amb2x16,
	FIM_ambx16_be,
	FIM_amb2x16_be,
	FIM_asbx16,
	FIM_asbx16_2,
	FIM_asb2x16,
	FIM_asb2x16_2,
	FIM_asbx16_be,
	FIM_asb2x16_be,
	FIM_iffx8,
	FIM_iffx8_2,
	FIM_iffx16,
	FIM_iffx16_2,
	FIM_iff2x16,
	FIM_iff2x16_2,
	FIM_iffx16_be,
	FIM_iff2x16_be,
	FIM_isfx16,
	FIM_isfx16_2,
	FIM_isf2x16,
	FIM_isf2x16_2,
	FIM_isfx16_be,
	FIM_isf2x16_be,	
	FIM_isf4x8,
	FIM_isf4x8_2,
	FIM_iswfx16,
	FIM_iswfx16_2,
	FIM_iswfx16_be,
	FIM_norram,
	FIM_norfile,
	FIM_nand
}USER_FIM_TYPE;

/* NANDFLASH_NTM */
/* 枚举结构USER_NTM_TYPE中定义的是Flash的NTM类型*/
typedef enum {
	
	NTM_cad = 0x01,
	NTM_cadmlc,
	NTM_lsi,
	NTM_micron,
	NTM_mx31,
	NTM_onenand,
	NTM_pageio,
	NTM_pxa320,
	NTM_ram
}USER_NTM_TYPE;

typedef struct flash_chip_info
{
	struct list_head fl_chip_list;        /* 2012.6 */        
	USER_FIM_TYPE fl_fim;			/* Flash Interface Module */
	USER_NTM_TYPE fl_ntm;			/* NAND Technology Module */
	void *fl_base_addr;				/* base address of the flash array, for flash types such as NAND, 
										or the NOR "ram" or "file" FIMS, this value should be set to FFX_BADADDRESS */
	u32 fl_reserved_lo_kb;			/* the amount of reserved flash at the beginning of the flash array in KB */  
	u32 fl_reserved_hi_kb;			/* the amount of reserved flash at the end of the flash array in KB */ 
	u32 fl_dev_size_kb;					/* the maximum amount of flash in KB after removing any reserved space */
	
	void (*attrAndfuncSet)();    /* 解决多个flash设备参数初始化时，共用同一变量问题。add by yaoqiuguo @2014.11.11 */
}FLASH_CHIP_INFO;


/* 用户自定义NAND Flash芯片属性结构体.
 * 注意：无论用户的Flash是否为Flash库支持的芯片，DevIDLength以及之后的字段均需要根据芯片手册进行设置.
 */
typedef struct USERNANDCHIPCLASS
{
	/* uPageSize：除去spare区之后的页大小，以字节为单位*/
	unsigned short	uPageSize;
	
	/* uSpareSize：spare区的大小，以字节为单位*/
	unsigned short  uSpareSize;
	
	/* uLinearPageAddrMSB：为获取页(page)索引而需要将Flash地址(Column Address)向右移的位数*/
	unsigned char   uLinearPageAddrMSB; 
	
	/* uChipPageIndexLSB：*/
	unsigned char   uChipPageIndexLSB;   /* how much to << chip page index before sending to chip */
	
	/* uLinearBlockAddrLSB：为获取块(block)索引而需要将Flash地址(Row Address)向右移的位数*/
	unsigned char   uLinearBlockAddrLSB; 
	
	/* uLinearChipAddrMSB：为确定片选而需要将flash地址向右移的位数*/
	unsigned char   uLinearChipAddrMSB;  
	
	/* ulChipBlocks：每块flash芯片的块数(erase block).*/
	unsigned long   ulChipBlocks;       
	
	/* ulBlockSize：块大小(erase block)，不包括spare区.*/
	unsigned long   ulBlockSize;        
	
	/* uEdcRequirement：纠错位数.用户根据flash芯片手册确定合适的纠错位数.*/
	unsigned short  uEdcRequirement;     
	
	/* uEdcCapability：纠错能力.该字段表示flash芯片支持的最大纠错位数.
	 * NAND Flash中常用的纠错方式：Hanming,RS,BCH.
	 * 因为闪存中会有出错的可能，如果没有使用ECC模块，读出的数据和写入的数据会有不匹配的可能，也许一个文件中只有一两个bit不匹配，这也是
	 * 不能容忍的。相对来说SLC中出错概率比较低，所以使用一个纠错能力不强的Hanming码就可以了，在MLC中Hanming码就显得力部从心了，需要纠错
	 * 能力更强的RS或者BCH纠错方式了。RS是按多位的组合编码，而BCH按位编码。
	 * 例如在NAND Flash中某个字节应该是0x00，但是读出的却是0xf0,因此RS只认为出现了一个错误，而BCH则认为出现了4个错误。
	 * RS方式有着不错的突发随机错误和随机错误的能力。BCH擅长处理随机错误，因此在MLC中应用比较多的是BCH.纠错需要额外的存储空间，这个存储
	 * 空间从flash的spare区分配。
	 * 目前的FLASH库暂时不支持ECC模块，因此uEdcCapability应设置为0.
	 */
	unsigned short  uEdcCapability;      
	
	/* ulEraseCycleRating：Flash的最大擦除周期数.(用户可根据Flash芯片手册中的FEATURES章节可以找到该参数)*/
	unsigned long   ulEraseCycleRating;  
	
	/*ulBBMReservedRating：用于坏块管理的块数.该字段的确定需要参考FLASH库支持的FLASH对应的ulBBMReservedRating字段.*/
	unsigned long   ulBBMReservedRating; 
	
	
	/* 以下8个数据成员表示的是某些芯片的特殊属性*/
	
	/* ResetBeforeProgram:是否支持STMicro Flash的写前重置操作.
	 * 如果支持STMicro Flash的写前重置操作，则设置ResetBeforeProgram = 1;否则设置ResetBeforeProgram = 0.
	 * (如果用户新添加的芯片是意法半导体公司的STMicro Flash，而且芯片是由两片512Mbit Flash组成，则设置ResetBeforeProgram = 1.)
	 */
	unsigned ResetBeforeProgram : 1;
	
	/* ReadConfirm:读确认命令.
	 * 如果支持读确认命令触发读操作,则设置ReadConfirm = 1;否则设置ReadConfirm = 0.
	 * (现在很多比较新的，比较大的flash芯片的读操作命令一般包括两个字节的命令，需要先后发送两个字节的命令以执行读操作，
	 * 发送的第二字节的命令称作读确认命令。)
	 */
	unsigned ReadConfirm : 1;

	/* Samsung2kOps:是否支持三星Flash的扩展操作.
	 * 如果支持三星Flash的扩展操作，则设置Samsung2kOps = 1;否则设置Samsung2kOps = 0.
	 * (如果Flash支持类似三星Flash的Random Data Input、Random Data Output等操作,则应设置Samsung2kOps = 1.)
	 */
	unsigned Samsung2kOps : 1;
	
	/* ORNANDPartialPageReads:是否支持快速读取小型数据块.
	 * 如果Flash支持快速读取小型数据块,则设置ORNANDPartialPageReads = 1;否则设置ORNANDPartialPageReads = 0.
	 * (传统的NAND Flash必须以页为单位读取数据，但是现在比较新的Spansion ORNAND支持局部页面读命令，
	 * 该命令支持以四分之一页面为增量的更快的数据访问速度.)
	 */
	unsigned ORNANDPartialPageReads : 1;
	
	/* fMicronDualPlaneOps:是否支持dual-plane操作.
	 * 如果支持dual-plane操作,则设置fMicronDualPlaneOps = 1;否则设置fMicronDualPlaneOps = 0.
	 * (现在部分比较新的Micron Flash支持dual-plane操作)
	 */
	unsigned fMicronDualPlaneOps : 1;
	
	/* BlockLockSupport:是否支持块锁机制.
	 * 如果支持块锁机制，则设置BlockLockSupport = 1;否则设置BlockLockSupport = 0.
	 * (现在部分比较新的STMicro flash芯片支持块锁机制)
	 */
	unsigned BlockLockSupport : 1;
	
	/* fProgramOnce:写操作的类型.
	 * 如果Flash芯片仅支持单页写操作,则设置fProgramOnce = 1;否则设置fProgramOnce = 0.
	 * (一般情况下,MLC NAND仅支持单页写操作;而其他一些flash还支持cache program)
	 */
	unsigned fProgramOnce : 1;
	
	/* fLastPageFBB:坏块标记是否定义在坏块的最后一页.
	 * 如果坏块标记定义在坏块的最后一页，则设置fLastPageFBB = 1;否则设置fLastPageFBB = 0.
	 */
	unsigned fLastPageFBB : 1;
	
		
	/* 如果用户的Flash是Flash库本身就支持的芯片，则以上的字段可全部设置成NULL；否则就要按照芯片手册以及Flash库给定的参考属性进行设置.
	 * 对于以下的字段，无论用户的Flash是否为Flash库支持的芯片，均需要根据芯片手册进行设置.
	 */
		
	/* Flash ID的长度.例如型号为K9F2G08U0M的NAND Flash,其ID长度为2.*/
	unsigned char DevIDLength;
	
	/* DevID[6]:Flash ID数组,包括厂商ID和设备ID.例如型号为K9F2G08U0M的NAND Flash,其厂商ID为0xEC,设备ID为0xDA,则DevID[0]=0xEC,DevID[1]=0xDA.*/
	unsigned char DevID[6];

	/* FbbType：出厂坏块类型(Factory Bad Block或者Initial Invalid Block).
	 * 如果坏块标记被定义在spare区的第一个字节(X8 device)或者第一个字(X16 device),则设置FbbType = 0.
	 * 如果坏块标记被定义在spare区的第六个字节(X8 device)或者第六个字(X16 device),则设置FbbType = 1.
	 * 如果坏块标记被定义在一个block内的任意字节处,则设置FbbType = 2.(目前这个设置仅出现在比较旧的Toshiba Flash中)(NTpageio模式不支持)
	 * 如果没有出厂坏块，且不需要进行坏块管理，则设置FbbType = 3.(目前这个设置仅出现在Spansion Flash中)(NTpageio模式不支持)
	 */	
	unsigned short	FbbType;	
	
	/* Chip_Interface_Width：Flash芯片数据接口宽度*/
	unsigned short	Chip_Interface_Width;	
	
	/* UnsupportedChip：如果用户的Flash是FlashFX库支持的芯片类型，则设置UnsupportedChip = 0;否则设置UnsupportedChip = 1.*/
	int UnsupportedChip;
	
	/* 测试字段*/
	/* DriverAutoTest: 测试程序开启字段.
	 * 如果需要进行测试，则将设置DriverAutoTest = 1；否则设置DriverAutoTest = 0.
	 */
	int DriverAutoTest;
	
	/* DclTestParams：Flash协议单元测试.
	 * 如果需要进行测试，则将设置DclTestParams = 1；否则设置DclTestParams = 0.
	 */
	int DclTestParams;
	
	/* FmslTestParams: Flash驱动单元测试.
	 * 如果需要进行测试，则将设置DclTestParams = 1；否则设置DclTestParams = 0.
	 */
	int FmslTestParams;
	
} USERNANDCHIPCLASS;
extern struct USERNANDCHIPCLASS *UserNandFlashAttribute;

/*Flash库中定义的关于NAND Flash驱动接口函数指针*/
typedef int (*_func_flash_pageio_init)();
typedef int (*_func_flash_pageio_read)(u32 ulStartPage, u8* pPageBuff, u8* pSpareBuff);
typedef int (*_func_flash_pageio_write)(u32 sector_num, u8* pPageBuff, u8* pSpareBuff);
typedef int (*_func_flash_pageio_erase)(u32 block_num);

extern _func_flash_pageio_init ptr_func_flash_pageio_init;
extern _func_flash_pageio_read ptr_func_flash_pageio_read;
extern _func_flash_pageio_write ptr_func_flash_pageio_write;
extern _func_flash_pageio_erase ptr_func_flash_pageio_erase;


#if NOR_FLASHCHIP_WIDTH == 8
    typedef unsigned char NORFLASHID;
#elif NOR_FLASHCHIP_WIDTH == 16
    typedef unsigned short  NORFLASHID;
#elif NOR_FLASHCHIP_WIDTH == 32
    typedef unsigned int  NORFLASHID;
#else    
    typedef unsigned short  NORFLASHID;
#endif
    
/*---------------------------------------------------------
    FIM ID structure and flags
---------------------------------------------------------*/
/* 如果你开发NOR Flash文件系统时，选用的FIM类型为如下的类型请在ffx_config.c中使用下面的AMD_NOR_CHIPPARAMS属性结构体：
 * FIM_asux8、FIM_asu4x8、FIM_asbx16、FIM_asb2x16、FIM_asbx16_be、FIM_asb2x16_be。
 * */    
struct AMD_NOR_CHIPPARAMS
{
    NORFLASHID     idMfg;/*厂商ID*/
    NORFLASHID     idDev;/*设备ID*/
    unsigned int    ulChipSize;/*芯片可用的最大容量*/
    unsigned int    ulBlockSize;/*块大小*/
    unsigned int    ulBootBlockSize;/*boot区的块大小*/
    unsigned short    uLowBootBlocks;     /* count of full blocks divided into boot blocks */
    unsigned short    uHighBootBlocks;    /* count of full blocks divided into boot blocks */
    unsigned short    uDevNotMlc;/*是否是MLC FLASH。*/
    unsigned short    uIDFlags;/*ID标志*/
    
    int UnsupportedChip;/*Flash芯片是否为Flash库支持*/
    int IsModefied;/*若Flash芯片是Flash库支持的，是否需要修改属性*/
};
extern struct AMD_NOR_CHIPPARAMS *USER_AMD_NOR_CHIPPARAMS;


/* 如果你开发NOR Flash文件系统时，选用的FIM类型为如下的类型请在ffx_config.c中使用下面的INTEL_NOR_CHIPPARAMS属性结构体：
 * FIM_iffx8、FIM_iffx16、FIM_iff2x16、FIM_iffx16_be、FIM_iff2x16_be、FIM_isfx16、FIM_isf2x16、
 * FIM_isfx16_be、FIM_isf2x16_be、FIM_isf4x8、FFXFIM_iswfx16、FFXFIM_iswfx16_be。
 * */
struct INTEL_NOR_CHIPPARAMS
{
	NORFLASHID     idMfg;/*厂商ID*/
	NORFLASHID     idDev;/*设备ID*/
    unsigned int    ulChipSize;/*芯片可用的最大容量*/
    unsigned short    uLowBootBlocks;     /* count of full blocks divided into boot blocks */
    unsigned short    uHighBootBlocks;    /* count of full blocks divided into boot blocks */
    unsigned short    uFlags;/*Flash特殊标记*/
    int UnsupportedChip;/*Flash芯片是否为Flash库支持*/
    int IsModefied;/*若Flash芯片是Flash库支持的，是否需要修改属性*/
};
extern struct INTEL_NOR_CHIPPARAMS *USER_INTEL_NOR_CHIPPARAMS;


/* 如果你开发NOR Flash文件系统时，选用的FIM类型为如下的类型请在ffx_config.c中使用下面的OTHER_NOR_CHIPPARAMS属性结构体：
 * FIM_ambx16、FIM_amb2x16、FIM_ambx16_be、FIM_amb2x16_be。
 * */
struct OTHER_NOR_CHIPPARAMS
{
	unsigned int        ulChipSize;/*芯片可用的最大容量*/
	unsigned int        ulEraseBlockSize;/*块大小*/
	unsigned short      uManufCode;/*厂商ID*/
	unsigned short      uDeviceCode1;/*设备ID1*/
	unsigned short      uDeviceCode2;/*设备ID2*/
	unsigned short      uDeviceCode3;/*设备ID3*/
	int UnsupportedChip;/*Flash芯片是否为Flash库支持*/
	int IsModefied;/*若Flash芯片是Flash库支持的，是否需要修改属性*/
};
extern struct OTHER_NOR_CHIPPARAMS *USER_OTHER_NOR_CHIPPARAMS;


/*  Used for ID codes with 3 words of Device IDs
*/
typedef struct
{
    unsigned short  uManufCode;
    unsigned short  uDeviceCode1;
    unsigned short  uDeviceCode2;
    unsigned short  uDeviceCode3;
} ID_4_TYPE;

typedef struct
{
    NORFLASHID     idMfg;/*厂商ID*/
    NORFLASHID     idDev;/*设备ID*/	
}ID_2_TYPE;




/*Flash库中定义的关于NOR Flash驱动接口函数指针*/
typedef int (*_func_nor_readid)(unsigned int ulAddress, ID_4_TYPE *idCode);
typedef int (*_func_nor_reset)();
typedef int (*_func_nor_init)();
typedef int (*_func_nor_unlock)(unsigned int ulAddress);
typedef int (*_func_nor_read)(u32 ulStart, u16 uLength, void *pBuffer);
typedef int (*_func_nor_write)(u32 ulStart, u16 uLength, void *pBuffer);
typedef int (*_func_nor_erase)(u32 ulStart, u32 uLength);

extern _func_nor_readid ptr_func_nor_readid;
extern _func_nor_reset ptr_func_nor_reset;
extern _func_nor_init ptr_func_nor_init;
extern _func_nor_unlock ptr_func_nor_unlock;
extern _func_nor_read ptr_func_nor_read;
extern _func_nor_write ptr_func_nor_write;
extern _func_nor_erase ptr_func_nor_erase;

/*
 * Flash设备用户参数结构体
 * */

struct FlashFX_DEV_USER_Param
{
	struct list_head list;
	char DiskMntName[32];
	unsigned nDiskNum;
	unsigned nDeviceNum;
	unsigned nOffsetKB;
	unsigned nLengthKB;
	unsigned nFormatState;
	unsigned nFileSystemID;
	unsigned fsFormat;
	unsigned uCacheSize;
};

/*
 * 用于多个flash设备注册用的链表
 * */
void flash_dev_user_param_add(struct FlashFX_DEV_USER_Param *parm);
int flash_dev_num_get();
int flash_disk_num_get();
void ffxConfigInit(void);

#define DEFAULT_FL_REMAINING       ( 0xFFFFFFFFUL )
#define DEFAULT_FL_BASEADDR        ((void*)0xFFFFFFFFUL)

void flash_config_init(void);
void flash_add_dev(FLASH_CHIP_INFO * fDev);
int flash_dev_setup( void );
extern void usrFlashFXComponentInit (void);
extern int usrFlashFXDeviceInit(char *pszDevName, unsigned nDiskNum, unsigned nDeviceNum, unsigned nOffsetKB, 
	unsigned nLengthKB, unsigned nFormatState, unsigned nFileSystemID, unsigned fsFormat, unsigned nCacheSize);
extern int userFlashFXInit();

#ifdef __cplusplus
}
#endif

#endif
