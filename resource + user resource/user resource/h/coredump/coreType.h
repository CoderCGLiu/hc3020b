/*
 * coreType.h
 *
 *  Created on: 2013-12-18
 *      Author: hewei
 */

#ifndef TYPE_H_
#define TYPE_H_

#include <sys/types.h>
#include <reworks/types.h>


//typedef unsigned long CORE_ADDR ;//ptrdiff_t是long
typedef ptrdiff_t CORE_ADDR ;
typedef size_t CORE_SIZE ;
typedef int CORE_FILE ;
typedef int CORE_PID ;




#ifndef ERROR
#define ERROR -1
#endif

#ifndef OK
#define OK 0
#endif

#ifndef STATUS
#define STATUS int
#endif

#ifndef BOOL
#define BOOL int
#endif

#ifndef LOCAL
#define LOCAL static
#endif

#define CORE_DUMP_MEM_FILTER_MAX 10
#define CORE_DUMP_MEM_FILTER_SYSTEM_COUNT 100

#define CORE_MAGIC 0x4678 //魔数
#define CORE_FILE_MAX 	1	//预留内存中保存的core文件的最大个数
#define CORE_DUMP_MAX 	20	//core dump最大次数
#define CORE_FILE_LIMIT 0x4000000 //core文件最大长度64M
#define MAX_LOAD_NUM	200 //LOAD段最大个数

#define ENABLE_CORE_DUMP_DEBUG 0 //调试开关  


typedef struct _CORE_DUMP_ARGS
{
		int auto_make_core;
		int auto_save_place;
		int compression_level;
		CORE_SIZE reserve_mem_size;
		char *disk_name;
		char *extra_memory;
}CORE_DUMP_ARG;

#endif /* TYPE_H_ */
