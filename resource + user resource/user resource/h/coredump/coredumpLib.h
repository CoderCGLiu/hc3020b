﻿/*! @file coredumpLib.h
    @brief ReWorks系统coredump头文件
     
 * 本文件定义了ReWorks系统coredump用户操作接口。
 * 
 * @version @reworks_version
 * 
 * @see 无
 */

#ifndef CORE_DUMPLIB_H_
#define CORE_DUMPLIB_H_

#include <reworks/types.h>

#ifdef __cplusplus
extern "C" 
{
#endif

/**
 * @defgroup group_os_io_coredump coredump接口
 * @ingroup group_os_develop
 * 
 * @{
 * 
 */	

/*!
 * \fn int core_dump_copy(u32 coreDumpIndex, const char *destPath)
 * 
 * \brief 将指定的core文件保存到目标位置
 * 
 * \param coreDumpIndex 指定了需要复制的core dump文件的索引号
 * \param destPath 指定了放置core dump文件的目标位置
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int core_dump_copy(u32 coreDumpIndex, const char *destPath);


/*!
 * \fn void format_core_dump()
 * 
 * \brief 清空coredump文件头信息
 * 
 * \return 无
 */
void format_core_dump();

/*!
 * \fn int core_dump_dev_show (void)
 * 
 * \brief 显示core dump 设备信息。包括：设备当前保存的core dump 文件数，设备最大能保存的
 * core dump 文件数，core dump 文件最大长度，以及设备连续core dump 的最大次数。
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int core_dump_dev_show (void);

/*!
 * \fn int core_dump_show(u32 coreDumpIndex, u32 level)
 * 
 * \brief 显示core dump 内核信息。
 * 
 * \param coreDumpIndex 指定了需要显示的core dump文件的索引号
 * \param level 指定信息详细程度
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int core_dump_show(u32 coreDumpIndex, u32 level);

/*!
 * \fn int core_dump_is_available (void)
 * 
 * \brief 检测是否存在可用的core dump文件。
 * 
 * \return 成功返回TRUE
 * \return 失败返回FALSE
 * 
 */
int core_dump_is_available (void);

/* @} */


#ifdef __cplusplus
}
#endif
#endif /* CORE_DUMP_HLIB_ */
