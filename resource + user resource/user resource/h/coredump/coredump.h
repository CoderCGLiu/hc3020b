/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2014-1-22
 * 
 */

#ifndef CORE_DUMP_H_
#define CORE_DUMP_H_

#include <sys/types.h>

#ifdef __cplusplus
extern "C" 
{
#endif
/* 定义coredump异常处理任务函数指针类型 */
typedef void (*CORE_DUMP_EXC_HANDLER)(int exp_vector,void *regset,void *retain);

typedef struct dump_sys_info{
	ptrdiff_t reserve_addr;  	/* 由OS预留内存的开始地址，Coredump用它来保存core文件 */
	size_t reserve_size;
	
	ptrdiff_t text_start; 	/* 代码段起始地址 */
	size_t text_size;		/* 代码段大小 */
	
	ptrdiff_t data_start;		/* 数据段起始地址 */
	size_t data_size;		/* 数据段大小 */
	
	ptrdiff_t bss_start;		/* BSS段起始地址 */
	size_t bss_size;		/* BSS段大小 */
	
	ptrdiff_t sys_heap_start;	/* 核心堆起始地址 */
	size_t sys_heap_size;	/* 核心堆大小 */
	
	ptrdiff_t user_heap_start;/* 用户堆起始地址 */
} dump_sys_info_t;

/* 
 * 设置coredump异常处理函数 
 */
CORE_DUMP_EXC_HANDLER coredump_set_exception_handler(CORE_DUMP_EXC_HANDLER the_exc_handler);

/* 
 * 设置预留内存 ，当前的处理方式：指定用户堆顶端的地址为预留内存
 * 
 * 参数：
 * 	reserved_mem_size - 预留内存大小，单位为MB，
 * 		如果其值大于用户堆大小或等于0，则预留内存大小设为用户堆大小的一半 
 */
void *coredump_set_reserved_addr(size_t reserved_mem_size);

/* 
 * 获取预留内存的地址 
 */
ptrdiff_t coredump_get_reserved_addr(void);

/*
 * 获取系统信息
 * 成功返回0，失败返回-1。
 * reserveAddr是由OS预留内存的开始地址，预留内存大小暂时设置为32M ，Coredump用它来保存core文件。
 * textStart、textSize等是text、data、bss段的开始地址和大小，获取这几个值是为了确定Coredump应该拷贝哪些内存段。
 * heapStart、heapCurrent是堆的开始地址和当前值(获取malloc分配的地址)
 */
int coredump_get_sysinfo(dump_sys_info_t * info);

/* 
 * 重启机器 
 */
void coredump_reboot();

#ifdef __cplusplus
}
#endif
#endif /* CORE_DUMP_H_ */
