/*
 * hr3SysTools.h
 *
 *  Created on: 2018-6-6
 *      Author: wangwei
 */

#ifndef HR2SYSTOOLS_H_
#define HR2SYSTOOLS_H_
//#include "vxWorks.h"
#include <time.h>
#include "../porting.h"
#include "../uart_reg_def.h"

#define DRV_PCIE /*网络协议栈里需要这个宏*/


#define PRINT_DIO

//#define HR2_UART_PORT0 			ADDR_LP64(0xbf0c0000)
#define HR2_UART_PORT0              UART0_BASE_ADDR


//#define P2V(addr) ((addr & 0x1FFFFFFF) | 0xc0000000 )
//#define V2P(addr)  (addr & 0x1FFFFFFF)

/*FPGA REG */
#define FPGA_BAR0_BASE_ADDR 	ADDR_LP64(0xb8300000)
#define FPGA_BAR1_BASE_ADDR 	ADDR_LP64(0xb8400000)

/* Module GPIO*/
#define MODULE_GPIO_INPUT 	(FPGA_BAR0_BASE_ADDR + 0x10)
#define MODULE_GPIO_OUTPUT 	(FPGA_BAR0_BASE_ADDR + 0x18)
#define MODULE_GPIO_STATE 	(FPGA_BAR0_BASE_ADDR + 0x1c)

#define MODULE_ID_REG 		(FPGA_BAR0_BASE_ADDR + 0x0)
#define TIMESTAMP_LOW_REG 		(FPGA_BAR0_BASE_ADDR + 0x8)
#define TIMESTAMP_HIGH_REG 		(FPGA_BAR0_BASE_ADDR + 0xc)

#define RESET_CTRL_REG 		(FPGA_BAR0_BASE_ADDR + 0x100)
#define UART_SWITCH_TX_REG 		(FPGA_BAR0_BASE_ADDR + 0x104)
#define UART_SWITCH_RX_REG 		(FPGA_BAR0_BASE_ADDR + 0x108)

#define PCIE_PARAM0_REG 		(FPGA_BAR0_BASE_ADDR + 0x194)
#define PCIE_PARAM1_REG 		(FPGA_BAR0_BASE_ADDR + 0x198)
#define PCIE_PARAM2_REG 		(FPGA_BAR0_BASE_ADDR + 0x19c)
//BOOTLINE_BAK_ADDR，此空间到偏移量0x2FF，保存最新的bootline，以便于B、C、D片使用
#define BOOTLINE_BAK_ADDR		(FPGA_BAR0_BASE_ADDR + 0x200) 

#define PUBIT_BASE_ADDR		(FPGA_BAR0_BASE_ADDR + 0x300) 

#define DSPB_BOOT_READY_REG	(FPGA_BAR0_BASE_ADDR + 0x400)
#define DSPC_BOOT_READY_REG	(FPGA_BAR0_BASE_ADDR + 0x404)
#define DSPD_BOOT_READY_REG	(FPGA_BAR0_BASE_ADDR + 0x408)
#define VX_READY_REG		(FPGA_BAR0_BASE_ADDR + 0x40c)

#define CORE_VCC_REG 		(FPGA_BAR1_BASE_ADDR + 0x8)
#define ZYNQ_TEMP_REG 		(FPGA_BAR1_BASE_ADDR + 0x30)

/* 定义各监测设备名称 */
#define DBG_CORE0_NAME		"dbgcore0" 
#define DBG_CORE1_NAME		"dbgcore1"  
#define DBG_CORE2_NAME		"dbgcore2"  
#define DBG_CORE3_NAME		"dbgcore3" 

#define DBG_CORE0_DMA_NAME	"dbgcdma0"  
#define DBG_CORE1_DMA_NAME	"dbgcdma1"  
#define DBG_CORE2_DMA_NAME	"dbgcdma2" 
#define DBG_CORE3_DMA_NAME	"dbgcdma3"  

#define DBG_GDMA_NAME		"dbggdma"  

#define DBG_RIO0_NAME		"dbgrio0"  
#define DBG_RIO1_NAME		"dbgrio1" 

#define DBG_GMAC0_NAME		"dbggmac0"  
#define DBG_GMAC1_NAME		"dbggmac1"
#define DBG_GMAC2_NAME		"dbggmac2"

#define DBG_GPIO1_NAME		"dbggpio1"
#define DBG_GPIO2_NAME		"dbggpio2" 
#define DBG_GPIO3_NAME		"dbggpio3" 
#define DBG_GPIO4_NAME		"dbggpio4"
#define DBG_GPIO5_NAME		"dbggpio5" 
#define DBG_GPIO6_NAME		"dbggpio6"
#define DBG_GPIO7_NAME		"dbggpio7" 

#define DBG_PCIE_NAME		"dbgpcie" 

#define DBG_UART_NAME		"dbguart" 

#define DBG_RASP0_NAME		"dbgrasp0" 
#define DBG_RASP1_NAME		"dbgrasp1" 
#define DBG_RASP2_NAME		"dbgrasp2" 
#define DBG_RASP3_NAME		"dbgrasp3" 

#define DBG_NANDFLASH_NAME	"dbgnand" 
#define DBG_NORFLASH_NAME	"dbgnor"

#define DBG_CAN_NAME		"dbgcan" 

#define DBG_IIC_NAME		"dbgi2c"  

#define DBG_WDOG_NAME		"dbgwdog"  

#define DBG_TIMER0_NAME		"dbgtimer0"  
#define DBG_TIMER1_NAME		"dbgtimer1"  

/* 定义各设备监测信息存储在用户DDR空间的基地址 */
#define DBG_CORE0_REG_BASE_ADDR		ADDR_LP64(0xaf000000) /* 记录核0寄存器内容，例外或重启时写入 */
#define DBG_CORE1_REG_BASE_ADDR		ADDR_LP64(0xaf000200) /* 记录核1寄存器内容，例外或重启时写入 */
#define DBG_CORE2_REG_BASE_ADDR		ADDR_LP64(0xaf000400) /* 记录核2寄存器内容，例外或重启时写入 */
#define DBG_CORE3_REG_BASE_ADDR		ADDR_LP64(0xaf000600) /* 记录核3寄存器内容，例外或重启时写入 */

#define DBG_CORE0_DMA_BASE_ADDR		ADDR_LP64(0xaf000800) /* 记录核0DMA信息，使用DMA传输时写入 */
#define DBG_CORE1_DMA_BASE_ADDR		ADDR_LP64(0xaf000900) /* 记录核1DMA信息，使用DMA传输时写入 */
#define DBG_CORE2_DMA_BASE_ADDR		ADDR_LP64(0xaf000a00) /* 记录核2DMA信息，使用DMA传输时写入 */
#define DBG_CORE3_DMA_BASE_ADDR		ADDR_LP64(0xaf000b00) /* 记录核3DMA信息，使用DMA传输时写入 */
#define DBG_CORE4_DMA_BASE_ADDR		ADDR_LP64(0xaf000c00) /* 记录核4DMA信息，使用DMA传输时写入 */
#define DBG_CORE5_DMA_BASE_ADDR		ADDR_LP64(0xaf000d00) /* 记录核5DMA信息，使用DMA传输时写入 */
#define DBG_CORE6_DMA_BASE_ADDR		ADDR_LP64(0xaf000e00) /* 记录核6DMA信息，使用DMA传输时写入 */
#define DBG_CORE7_DMA_BASE_ADDR		ADDR_LP64(0xaf000f00) /* 记录核7DMA信息，使用DMA传输时写入 */

#define DBG_GDMA_BASE_ADDR			ADDR_LP64(0xaf001000) /* 记录通用DMA信息，使用DMA传输时写入 */

#define DBG_RIO0_BASE_ADDR			ADDR_LP64(0xaf001200) /* 记录rapidIO0信息，使用rapidIO传输时写入 */
#define DBG_RIO1_BASE_ADDR			ADDR_LP64(0xaf001900) /* 记录rapidIO1信息，使用rapidIO传输时写入 */

#define DBG_GMAC0_BASE_ADDR			ADDR_LP64(0xaf002000) /* 记录gmac0信息，使用gmac传输时写入 */
#define DBG_GMAC1_BASE_ADDR			ADDR_LP64(0xaf002400) /* 记录gmac1信息，使用gmac传输时写入 */
#define DBG_GMAC2_BASE_ADDR			ADDR_LP64(0xaf002800) /* 记录gmac2信息，使用gmac传输时写入 */

#define DBG_GPIO1_BASE_ADDR			ADDR_LP64(0xaf002c00) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO2_BASE_ADDR			ADDR_LP64(0xaf002c10) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO3_BASE_ADDR			ADDR_LP64(0xaf002c20) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO4_BASE_ADDR			ADDR_LP64(0xaf002c30) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO5_BASE_ADDR			ADDR_LP64(0xaf002c40) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO6_BASE_ADDR			ADDR_LP64(0xaf002c50) /* 记录GPIO信息，使用片间中断时写入 */
#define DBG_GPIO7_BASE_ADDR			ADDR_LP64(0xaf002c60) /* 记录GPIO信息，使用片间中断时写入 */

#define DBG_PCIE_BASE_ADDR			ADDR_LP64(0xaf003500) /* 记录PCIE信息，使用PCIE DMA传输时写入 */

#define DBG_UART_BASE_ADDR			ADDR_LP64(0xaf004000) /* 记录串口信息 */

#define DBG_RASP0_BASE_ADDR			ADDR_LP64(0xaf004300) /* 记录RASP0信息 */
#define DBG_RASP1_BASE_ADDR			ADDR_LP64(0xaf004380) /* 记录RASP1信息 */
#define DBG_RASP2_BASE_ADDR			ADDR_LP64(0xaf004400) /* 记录RASP2信息 */
#define DBG_RASP3_BASE_ADDR			ADDR_LP64(0xaf004480) /* 记录RASP3信息 */

#define DBG_NANDFLASH_BASE_ADDR		ADDR_LP64(0xaf004500) /* 记录nandFlash信息 */
#define DBG_NORFLASH_BASE_ADDR		ADDR_LP64(0xaf004600) /* 记录norFlash信息 */

#define DBG_CAN_BASE_ADDR			ADDR_LP64(0xaf004700) /* 记录CAN信息 */

#define DBG_IIC_BASE_ADDR			ADDR_LP64(0xaf004800) /* 记录IIC信息 */

#define DBG_WDOG_BASE_ADDR			ADDR_LP64(0xaf004a00) /* 记录watchdog信息 */

#define DBG_TIMER0_BASE_ADDR		ADDR_LP64(0xaf004c00) /* 记录Timer信息 */
#define DBG_TIMER1_BASE_ADDR		ADDR_LP64(0xaf004e00) /* 记录Timer信息 */

/* 热重启信息 */
#define DBG_HOT_REBOOT_SIGN_ADDR	ADDR_LP64(0xaf005000) /* 记录模块热重启信息的地址 */
#define DBG_HOT_REBOOT_SIGN_VAL		ADDR_LP64(0xA5A5AA55) /* 记录模块热重启信息的值 */

#define DBG_STARTUP_PHASE_ADDR		ADDR_LP64(0xaf005004) /* 记录芯片启动阶段的地址 */
/* 记录上电BIT信息 */
#define PUBIT_DSPB_DETECTED_ADDR	ADDR_LP64(0xaf005010) /* 记录DSPB是否被探测到的地址 */
#define PUBIT_DSPC_DETECTED_ADDR	ADDR_LP64(0xaf005014) /* 记录DSPC是否被探测到的地址 */
#define PUBIT_DSPD_DETECTED_ADDR	ADDR_LP64(0xaf005018) /* 记录DSPD是否被探测到的地址 */
#define PUBIT_ZYNQ_DETECTED_ADDR	ADDR_LP64(0xaf00501c) /* 记录ZYNQ是否被探测到的地址 */
/* 记录CPS1848各端口信息 */
#define DBG_CPS1848_BASE_ADDR		ADDR_LP64(0xaf005100) /* 记录CPS1848端口信息 */

/* 定义各性能测试设备的名称 */
#define PERF_MEM_NAME		"perfmem" 
#define PERF_NAND_NAME		"perfnand" 
#define PERF_NOR_NAME		"perfnor" 
#define PERF_IPI_NAME		"perfipi" 
#define PERF_PCIE_NAME		"perfpcie" 
#define PERF_TIMER_NAME		"perftimer" 
#define PERF_GMAC_NAME		"perfgmac" 
#define PERF_SRIO_NAME		"perfsrio" 
#define PERF_SNDPIPE_NAME		"perfsndpipe" 
#define PERF_RCVPIPE_NAME		"perfrcvpipe" 
/* 定义各设备性能测试信息存储在用户DDR空间的基地址 ，各设备预留64B*/
#define PERF_MEM_BASE_ADDR		ADDR_LP64(0xaf006000) /* 记录memory性能测试结果  */
#define PERF_NAND_BASE_ADDR		ADDR_LP64(0xaf006040) /* 记录Nand性能测试结果  */
#define PERF_NOR_BASE_ADDR		ADDR_LP64(0xaf006080) /* 记录Nor性能测试结果  */
#define PERF_IPI_BASE_ADDR		ADDR_LP64(0xaf0060c0) /* 记录中断响应性能测试结果  */
#define PERF_PCIE_BASE_ADDR		ADDR_LP64(0xaf006100) /* 记录PCIe传输性能测试结果  */
#define PERF_TIMER_BASE_ADDR	ADDR_LP64(0xaf006140) /* 记录Timer计时性能测试结果  */
#define PERF_GMAC_BASE_ADDR		ADDR_LP64(0xaf006180) /* 记录GMAC性能测试结果  */
#define PERF_SRIO_BASE_ADDR		ADDR_LP64(0xaf0061c0) /* 记录SRIO性能测试结果  */
#define PERF_SNDPIPE_BASE_ADDR	ADDR_LP64(0xaf006200) /* 记录PIPE性能测试结果 ，需要256B */
#define PERF_RCVPIPE_BASE_ADDR	ADDR_LP64(0xaf006300) /* 记录PIPE性能测试结果 ，需要128B */

/* 定义BIT函数repeat模式下，记录各测试项测试失败的次数 */
#define RPTBIT_FUNC_BASE_ADDR	ADDR_LP64(0xaf006400) /* 256B */
#define RPTBIT_PERF_BASE_ADDR	ADDR_LP64(0xaf006500) /* 128B */

/* 说明：从0xaf006600~0xaf007600的4KB用于net(lw)调试信息存储空间 */
/* 说明：从0xaf007600~0xaf008600的4KB用于DDS检测例外信息存储空间 */

/* 说明：从0xaf0e0000~0xaf0fffff的128KB用于rapidIO调试信息存储空间 */

/* 定义应用调试空间基地址，容量大小为15MB */
#define DBG_USER_BASE_ADDR			ADDR_LP64(0xaf100000) /* 记录应用调试信息信息 */

/* sysctrl reg */
#define DSP_ID_REG			ADDR_LP64(0xbf0c0000)
#define CORE_ENABLE_REG		ADDR_LP64(0xbf0c0004)
#define SYSTEM_CFG2_REG		ADDR_LP64(0xbf0c000c)
#define DSP_NUM_REG			ADDR_LP64(0xbf0c0030)
/**/
#define DSP_FREQ_REG 		ADDR_LP64(0xbf0c0040)
#define ACE_FREQ_REG 		ADDR_LP64(0xbf0c0044)
#define DDR_FREQ_REG 		ADDR_LP64(0xbf0c0048)
#define AXI_FREQ_REG 		ADDR_LP64(0xbf0c004c)
/**/
/**/
#define LOOP_TIMER_OUT 		5 /* while循环，timeout设置为5s，避免死循环导致程序无反应 */
/**/
#define LOGGER_ASSERT(x) \
{\
  if (!(x)) {\
    fprintf(stderr, "Assertion failed at %s:%d\n", __FILE__, __LINE__);\
    printf("Assertion failed at %s:%d\n", __FILE__, __LINE__);\
    exit(1);\
  }\
}

#define SWAP_16(x)	\
	(u16)((((u16)(x)&0x00ff)<<8)|	\
	(((u16)(x)&0xff00)>>8))
	
#define SWAP_32(x)					\
        ((u32)((((u32)(x) & 0x000000FFU) << 24) |	\
	       (((u32)(x) & 0x0000FF00U) <<  8) |	\
	       (((u32)(x) & 0x00FF0000U) >>  8) |	\
	       (((u32)(x) & 0xFF000000U) >> 24)))
		   
#define LED_CTRL_REG		ADDR_LP64(0xbf088804)

#define NORFLASH_CTRL_ADDR		ADDR_LP64(0xbf0d8000)


#define NELEMENTS(array)		/* number of elements in an array */ \
		(sizeof (array) / sizeof ((array) [0]))
#define PHYS_TO_KM_MEMMAP_ITEMS (NELEMENTS(PHYS_TO_KM_MEMMAP)/2)

extern unsigned int sysRead32(unsigned int addr);
extern void sysWrite32(unsigned int addr, unsigned int data);
extern void sysRead64(unsigned int addrh, unsigned int addrl, unsigned int *datah, unsigned int *datal);
extern void sysWrite64(unsigned int addrh, unsigned int addrl, unsigned int datah, unsigned int datal);
extern unsigned int sysCpuGetID();
extern void sysLEDSetState(unsigned int state);

#endif /* HR2SYSTOOLS_H_ */
