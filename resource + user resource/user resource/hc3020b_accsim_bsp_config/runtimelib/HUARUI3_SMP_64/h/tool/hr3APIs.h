/*!************************************************************
 * @file: hr2APIs.h
 * @brief: 根据驱动封装的上层接口头文件
 * @author: cetc
 * @date: 06,01,2020
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2020/01/06 | xupeng | 代码整理与规范
 * 
 **************************************************************/
 
#ifndef _HR2APIS_H
#define _HR2APIS_H

#ifdef __cplusplus
extern "C"{
#endif
#include "hr3Macro.h"
#include "vxWorks.h"
#include "types/vxTypes.h"
#ifdef INCLUDE_UNAME
#include <osinfo.h>
#endif

#define ERROR_PARA_INPUT 	-1	/* 参数输入错误 */
#define ERROR_FUNC_EXEC 	-2	/* 函数执行错误 */

typedef unsigned long CardAddress;	/* Physical offset on card */

/*定义logMsg接口给用户使用*/
//#define logMsg printf
extern int logMsg  ( 
	char *  fmt,  
	int  arg1,  
	int  arg2,  
	int  arg3,  
	int  arg4,  
	int  arg5,  
	int  arg6  
);


/*
 * 功能：LPC空间读接口。
 * 输入：
 * 		add:地址；
 * 		len:字节数
 * 输出：
 *		buf
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslLpcReadData(unsigned int addr, unsigned int len, unsigned char *buf);
/*
 * 功能：LPC空间写接口。
 * 输入：
 * 		add:地址；
 * 		len:字节数
 * 		buf: 要写入的数据
 * 输出：
 *		
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslLpcWriteData(unsigned int addr, unsigned int len, unsigned char *buf);

		
/*
 * 功能：获取槽位号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回槽位号，失败返回-1。
 */
unsigned int bslGetSlotNum();
/*
 * 功能：模块板级复位或整个插箱复位。
 * 输入：
 * 		resetType，复位类型，0为板级复位，1为整个插箱复位。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0，失败返回-1。
 */
int bslBoardReset(unsigned int resetType);

/*
 * 功能：获取模块的机柜号、插箱号、槽位号。
 * 输入：
 * 		无。
 * 输出：
 * 		cabinetNum，机柜号；
 * 		caseNum，	插箱号；
 * 		slotNum，	槽位号。
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslModuleGetInfo(unsigned int *cabinetNum, unsigned int *caseNum,unsigned int * slotNum);

/*
 * 功能：获取模块的基准时钟(单位为1us)。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	模块当前的基准时钟。
 */
unsigned long long bslGetTimeUsec(void);

/*
 * 功能：获取CPU CORE电压和ZYNQ的温度。
 * 输入：
 * 		无。
 * 输出：
 * 		coreVcc，	CPU CORE电压；
 * 		zynqTemp，	ZYNQ的温度。
 * 返回值：
 * 	 	函数执行成功，返回0。
 */
int bslFPGAGetSense(float * coreVcc, float * zynqTemp);

/*
 * 功能：板上4片CPU的串口切换。
 * 输入：
 * 		cpuIndex，CPU的序号，取值0、1、2、3。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */

int bslUartSwitch(unsigned int cpuIndex);


/*
 * 功能：读取温度传感器模块测量的华睿2号芯片温度值。
 * 输入：
 * 		无。
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回华睿2号芯片温度值。
 */
unsigned int bslSenseGetTemp(void);

/*
 * 功能：绑核执行任务。
 * 输入：
 * 		name，		任务名称；
 * 		priority，	任务优先级；
 * 		stackSize，	任务堆栈大小；
 * 		entryPt，	任务函数入口；
 * 		core，		绑核的核号；
 * 		arg0~arg9，	任务函数参数1~10。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回任务ID；函数执行失败，返回-1。
 */
//for hr2 app compiling, tmp remove by fukai,2018-5-28
TASK_ID bslTaskCreateCore(char * name,int priority,int options,int stackSize,FUNCPTR entryPt,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9);

/*
 * 功能：获取芯片ID号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	芯片ID，取值范围为0、1、2、3。
 */
unsigned int bslProcGetId(void);

/*
 * 功能：获取芯片各个核的ID号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	核ID，取值范围为0 ~ 7。
 */
unsigned int bslCoreGetId(void);

/*
 * 功能：设置LED灯的状态。
 * 输入：
 * 		state，LED状态，0为灯灭，1为灯亮，大于1时为闪烁。
 * 		freq，   LED闪烁的频率	。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
void bslLEDSetState(unsigned int state,unsigned int freq);
/*
 * 功能：转换LED灯的状态，即从灯亮->熄灭或者由熄灭->灯亮。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
void bslLEDToggle(void);


/*
 * 功能：DSPA norflash与nandflash(文件系统)之间的切换。
 * 输入：
 * 		flashIndex，flash的序号，取值0、1，0表示norflash，1表示nandflash。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslFlashSwitch(unsigned int flashIndex);


/*
 * 功能：通过网络烧写bootrom。
 * 输入：
 * 		fileName，烧写的bootrom的文件名称		。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslBurnBootrom(char * fileName);

/*
 * 功能：格式化flash并建立文件系统。该函数会对flash进行tffs格式化和dosfs格式化，
 * 		若文件系统已存在，此操作会清空原文件系统里的所有内容。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */

int bslFlashCreateFileSys(void); 

/*
 * 功能：配置文件系统。该操作挂载文件系统，不会格式化flash。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */

int bslFlashOpen(void); 

/*
 * 功能：启动通用DMA一维传输。
 * 输入：
 * 		srcAddr，	源地址；
 * 		dstAddr，	目的地址；
 * 		totalSize，	传输数据量大小(B)。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
 
 int bslGDMA1DStart( char * srcAddr, char * dstAddr,unsigned int totalSize);
 
 /*********************************************************************************************************
 ** 函数名称: bslGDMA2DStart
 ** 功能描述: 启动通用 DMA 二维传输
 **           通用 DMA 二维传输时，数据既可以是连续地址的，也可以是不连续地址的。
 **           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
 **           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
 ** 输　入  : uiTransType        传输模式，0 为二维至二维传输；1 为二维至一维传输；2 为一维至二维传输；
 **           pcSrcAddr          源地址；
 **           uiSrcRowSize       每次传输的串大小；
 **           uiSrcRowSpan       源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
 **           pcDstAddr          目的地址；
 **           uiDstRowSpan       目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
 **           uiTotalSize        传输数据量大小(B)，最大支持 4GB 数据传输。。
 ** 输　出  : 函数执行成功，返回 0，失败返回 -1。
 ** 全局变量:
 ** 调用模块:
 *********************************************************************************************************/
 int bslGDMA2DStart(unsigned int transType,  char * srcAddr,
		unsigned int srcRowSize,unsigned int srcRowSpan, char * dstAddr,
		unsigned int dstRowSpan,unsigned int totalSize);
		
 /*********************************************************************************************************
 ** 函数名称: bslGDMAMtxStart
 ** 功能描述: 启动通用 DMA 矩阵转置传输(简化版)
 **           在源矩阵和目的矩阵的数据都是连续的情况下使用。
 **           若需要对源矩阵或目的矩阵的数据是不连续的情况进行操作，则调用函数 bslGDMAMtxStartCpx。
 **           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
 ** 输　入  : pcSrcAddr          源地址；
 **           pcDstAddr          目的地址；
 **           uiSrcRowNum        源矩阵的行数；
 **           uiSrcColNum        源矩阵的列数;
 **           uiElementSign      矩阵元素大小(B)，取值4、8。
 ** 输　出  : 函数执行成功，返回 0，失败返回 -1。
 ** 全局变量:
 ** 调用模块:
 *********************************************************************************************************/
int bslGDMAMtxStart( char * srcAddr, char * dstAddr,unsigned int srcRowNum,
		unsigned int srcColNum,unsigned int elementSize);
	
/*********************************************************************************************************
** 函数名称: bslGDMAMtxStartCpx
** 功能描述: 启动通用 DMA 矩阵转置传输
**           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
**           通用 DMA 矩阵转置传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr          源地址；
**           uiSrcRowSize       每次传输的串大小；
**           uiSrcRowSpan       源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           pcDstAddr          目的地址；
**           uiDstRowSpan       目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiSrcRowNum        源矩阵的行数；
**           uiSrcColNum        源矩阵的列数;
**           uiElementSize      矩阵元素大小(B)，取值4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int bslGDMAMtxStartCpx( char * srcAddr,unsigned int srcRowSize, unsigned int srcRowSpan,
         char * dstAddr,unsigned int dstRowSpan,
		unsigned int srcRowNum,unsigned int srcColNum,unsigned int elementSize);

/*********************************************************************************************************
** 函数名称: bslCDMA1DStart
** 功能描述: 启动核内 DMA 一维传输
** 输　入  : pcSrcAddr    源地址
**           pcDstAddr    目的地址
**           totalSize    传输数据量大小(B)。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int bslCDMA1DStart( char * srcAddr, char * dstAddr,unsigned int totalSize);		
		
/*********************************************************************************************************
** 函数名称: bslCDMA2DStart
** 功能描述: 启动核内 DMA 二维传输
**           核内 DMA 二维传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr        源地址；
**           uiSrcRowSize     每次传输的串大小；
**           uiSrcRowSpan     源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           pcDstAddr        目的地址；
**           uiDstRowSpan     目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiTotalSize      传输数据量大小(B)。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int bslCDMA2DStart( char * srcAddr,unsigned int srcRowSize,
		unsigned int srcRowSpan, char * dstAddr, unsigned int dstRowSpan,
		unsigned int totalSize);		
		
/*********************************************************************************************************
** 函数名称: bslCDMAMtxStart
** 功能描述: 启动核内 DMA 矩阵转置传输(简化版)，在源矩阵和目的矩阵的数据都是连续的情况下使用。
**           若需要对源矩阵或目的矩阵的数据是不连续的情况进行操作，则调用函数 bslCDMAMtxStart 即可实现。
** 输　入  : pcSrcAddr        源地址；
**           pcDstAddr        目的地址；
**           uiSrcRowNum      源矩阵的行数；
**           uiSrcColNum      源矩阵的列数；
**           uiElementSize    矩阵元素大小(B)，取值1、2、4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int bslCDMAMtxStart( char * srcAddr, char * dstAddr,unsigned int srcRowNum,
		unsigned int srcColNum, unsigned int elementSize);
		
/*********************************************************************************************************
** 函数名称: bslCDMAMtxStartCpx
** 功能描述: 启动核内 DMA 矩阵转置传输。
**           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
**           核内 DMA 矩阵转置传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr        源地址；
**           uiSrcRowSize     每次传输的串大小；
**           uiSrcRowSpan     源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiDstAddr        目的地址；
**           uiDstRowSpan     目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiSrcRowNum      源矩阵的行数；
**           uiSrcColNum      源矩阵的列数；
**           uiWrCacheType    dma 写的模式，0 为 cache write; 1 为 uncache write；
**           uiRdCacheType    dma 读的模式，0 为 cache read;  1 为 uncache read；
**           uiElementSize    矩阵元素大小(B)，取值1、2、4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int bslCDMAMtxStartCpx( char * srcAddr,unsigned int srcRowSize,
		unsigned int srcRowSpan,  char * dstAddr,unsigned int dstRowSpan,
		unsigned int srcRowNum,unsigned int srcColNum, unsigned int wrCacheType,
		unsigned int rdCacheType,unsigned int elementSize);
				
 
   
#define TIMER_MODE_ONCE		1//周期性的循环计数
#define TIMER_MODE_CYCLE	0//一次性的计数
/*
 * 功能：Timer的初始化，并完成用户程序的挂接。
 * 输入：
 * 		timerIndex，	Timer的序号，取值0或1；
 * 		mode，		工作模式，取值0或1，设置为0时，表示周期性的循环计数；设置为1时，表示一次性的计数；
 * 		value，		Timer要初始化的值；
 * 		callback，	用户程序，即计时中断产生时用户要做的工作；
 * 		param，		用户程序的参数。
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1 
 */
int bslTimerInit(unsigned int timerIndex, unsigned int mode,unsigned long long value,FUNCPTR callback,int param);

/*
 * 功能：设置定时器状态(开始计时或停止计时)。
 * 输入：
 * 		timerIndex，Timer的序号，取值0或1；
 * 		state,		定时器状态，0为停止计时，1为开始计时，大于1时为起state次计时中断。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslTimerSetState(unsigned int timerIndex,unsigned int state);


/*
 * 功能：设置PCIE INBOUND窗口。
 * 输入：
 * 		devType，		PCIE设备类型，0表示EP，1表示RC；
 * 		dstAddr，		INBOUND窗对应的本地DDR地址。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
int bslPcieSetInbound(unsigned int devType,/*unsigned char barNum,*/ unsigned int dstAddr);

/*
 * 功能：初始化PCIE的接收任务。
 * 输入：
 * 		func，		用户函数，即PCIE 接收中断产生时用户要做的工作；
 * 		param0，		发送方 CPU ID 号,
 * 		param1，		接收方偏移地址
 * 		param2，		接收数据包大小
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
typedef int (*PFUNC_PCIE_CBACK) (void*,void*,void*);
int bslPcieRecInit(PFUNC_PCIE_CBACK func,void *param0,void *param1,void *param2);

/*
 * 功能：PCIE DMA实现片间数据传输。
 * 默认数据接收区：0x20000000；
 * 默认数据发送区：0x30000000。
 * 目前片间传输方式如下：
 * A片可以访问B、C、D (此时的DMA读或写的数据量不能大于1MB)；
 * B片可以访问C		(此时的DMA读或写的数据量不能大于1MB)；
 * C片可以访问D		(此时的DMA读或写的数据量不能大于1MB)；
 * D片可以访问A		(此时的DMA读或写的数据量不能大于1MB)。
 * 输入：
 * 		dmaType，		dma传输类型，0表示dma读，1表示dma写；
 * 		localAddr，		本地DDR地址，需128B对齐；
 * 		remoteCpuId，	CPU ID号，取值0,1,2,3；
 * 		offset，			DMA操作时remoteCpu端的偏移量，DMA读时为OUTBOUND窗口的偏移量，DMA写时为INBOUND窗口的偏移量，需128B对齐；
 * 		size，			传输数据量大小(B)。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslPcieDMAStart(unsigned int dmaType, unsigned long localAddr, unsigned int remoteCpuId,unsigned int offset,unsigned int size);

/*********************************************************************************************************
** 函数名称: bslUsrgGpioInit
** 功能描述: GPIO 初始化
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void bslUsrgGpioInit(void);

/*GPIO 初始化*/
extern void  hrGpioInstInit (void);

/* 获取外部中断信号量  */
extern int hrGpioGetIntSem(unsigned int gpioIndex);

/*
 * 设置GPIO的传输方向，1为output，0为input
 */
extern int hrGpioSetTransDirection(u32 gpioIndex, u32 direction);

/*
 * outputVal = 1, 输出高电平
 */
extern u32 hrGpioSetOutputVal(u32 gpioIndex, u32 outputVal);

/*
 * 值仅低24位有效
 */
extern u32 hrGpioGetOutputVal(u32 gpioIndex);

/*  使能 GPIO 中断              */
extern void  hrGpioIntEnable (int  gpioIndex);

/*  禁能 GPIO 中断              */
extern void  hrGpioIntDisable (int  gpioIndex);

extern u32 hrGpioGetIntNum(u32 gpioIndex);

/*
 * 设置GPIO的中断模式，0为边沿触发产生中断，1为电平产生中断
 */
extern int hrGpioSetIntMode(u32 gpioIndex, u32 mode);

/*
 * 设置GPIO的中断控制
 * 0为falling edge or low level，1为rising edge or high level。
 */
extern int hrGpioSetIntEdge(u32 gpioIndex, u32 mode);

/*
 * 读取每条gpio通道是否产生过中断。
 * 产生过中断，返回1；没有产生过中断，返回0。
 */
extern  int hrGpioGetRawIntStatus(u32 gpioIndex);

/*
 * 读取每条gpio通道产生的中断状态。
 * 返回值0：当前对应的bit位为对应的gpio没有中断；
 * 返回值1：当前对应的bit位为对应的gpio正在中断状态。
 */
extern int hrGpioGetIntStatus(u32 gpioIndex);

/*
 * 功能：获取GPIO的调试信息
 * 输入：
 *         无。
 * 输出：
 *         无。
 * 返回值：
 *          无。
 */
extern int hrGpioGetDbgInfo();



/*
 * 功能：获取外部中断产生时释放的信号量。
 * 输入：
 *		gpioIndex，GPIO ID号，取值1、2、3、4、5、6；
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslGpioGetIntSem(unsigned int gpioIndex);

typedef void (*GPIO_INT_HANDLER)(void *arg);

/*挂接gpio中断处理函数*/
void bslUsrgGpioConnect(u32 gpioIndex,GPIO_INT_HANDLER routine,void *param);

/*
 * 功能：
 * 		对于4核芯片：
 * 		初始化发送或接收缓存，默认情况下，系统建立四块发送缓存，
 * 		其中两块在DDR0地址空间，基地址分别为0x10000000和0x20000000，大小为0x10000000，
 * 		另外两块在DDR1地址空间， 基地址分别是0x60000000和0x70000000，大小均是0x10000000。
 * 		 系统建立四块接收缓存，
 * 		 其中两块在DDR0地址空间，基地址分别为0x30000000和0x40000000，大小为0x10000000，
 * 		 另外两块在DDR1地址空间，基地址分别是0xd0000000和0xe0000000，大小均是0x10000000。
 * 		应用可以调用此函数重新定义发送和接收缓存的起始地址和空间大小。
 * 输入：
 * 		bufType， 	缓存类型，0为发送缓存，1为接收缓存；
 * 		initType，	初始化类型，0为重新建立缓存，1为接着建立缓存；
 *		buf0，		缓存0基地址；
 *		buf0Size，	缓存0容量；
 *		buf1，		缓存1基地址；
 *		buf1Size，	缓存1容量；
 *		buf2，		缓存2基地址；
 *		buf2Size，	缓存2容量；
 *		buf3，		缓存3基地址；
 *		buf3Size，	缓存3容量。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslUserMemBufInit(unsigned int bufType,unsigned int initType,
		void * buf0, unsigned int buf0Size,
		void * buf1, unsigned int buf1Size,
		void * buf2, unsigned int buf2Size,
		void * buf3, unsigned int buf3Size);

/*
 * 功能：默认情况下，在DDR0中分配了512MB的发送和接收缓存，在DDR1中分配了512MB的发送和接收缓存，
 * 		均定义为循环缓冲区,这个函数从其中一个缓冲区分配指定长度的DDR空间。注意，这个循环
 * 		缓冲区不检测空间使用状态，用户需要控制在一次数据循环需要缓存的总数据量不得溢出，
 * 		溢出后旧数据被抛弃，不管是否被使用。申请到的缓存地址为USER_MEM_ALLOC_ALIGN 
 * 		(默认为128字节)对齐，每次分配的长度大小受限于宏USER_MEM_ALLOC_MAX_LEN(默认为8MB)
 * 		的定义，若大于该值，则按该值指定空间申请。应用程序可以用这个地址作为计算结果地址，
 * 		而不用考虑自己去建乒乓或环形缓冲区。
 *	输入：
 *		bufType， 	缓存类型，0为发送缓存，1为接收缓存；
 *		bufIndex， 	发送缓存区序号；
 *		align，		对齐方式，必须为4的倍数，作为RIO发送地址时，要求至少128字节对齐；
 *		nBytes，		申请的空间大小(字节)。
 *	输出：无。
 *	返回值：成功，0； 
 *         失败，-1。
 */
void * bslUserMemAllocBuf(unsigned int bufType,unsigned int bufIndex,  unsigned int align, unsigned int nBytes);


/*
 * 功能：挂接片间中断服务程序。
 * 输入：
 *		func，	用户函数，默认用户函数的第一个参数为源CPU ID号；
 *		param，	用户函数参数。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslRaspGetDoneSem(int raspId);


/*
 * 功能：初始化I2C。
 * 输入：
 *		devIndex，	设备序号，取值0、1；
 *		mode，		设备工作模式，0为master，1为slave；
 *		slaveId，	设备ID号，只在slave模式下有小。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslI2CInit(unsigned int devIndex,unsigned int mode,unsigned int slaveId);

/*********************************************************************************************************
** 函数名称: i2cBusTransfer
** 功能描述: i2c从设备传输数据
** 输　入  :   channel         i2c 通道号0/1
**           dev_addr        i2c 从设备地址
**           start_addr      i2c 读取/写入数据起始地址
**           buf             i2c 读/写数据缓冲区
**           len             i2c 每次读/写数据的长度
**           rd1_wr0		 i2c读操作 1 /写操作 0 
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int i2cBusTransfer(int channel, u16 dev_addr, u32 start_addr, u8 *buf, u16 len, int rd1_wr0);

/*
 * 功能：设置I2C slave模式下RAM空间值。
 * 输入：
 *		devIndex，	设备序号，取值0、1；
 *		offset，		偏移量，取值0~255；
 *		data，		要设置的值。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslI2CSSetData(unsigned int devIndex,unsigned int offset,unsigned int data);
/*
 * 功能：I2C master模式下与slave设备传输数据。
 * 输入：
 *		devIndex，	设备序号，取值0、1；
 *		transType，	传输类型，0为从slave设备读取值，1为向slave设备写入值；
 *		ddrAddr，	本地DDR地址，slave设备读取时为目标地址，向slave设备写入时为源数据地址；
 *		length，		传输数据长度(字节)；
 *		slaveId，	slave设备ID地址；
 *		memAddr，	slave设备RAM地址，取值0~255。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslI2CMTransData(unsigned int devIndex,unsigned int transType,unsigned char* ddrAddr,unsigned int length,unsigned int slaveId,unsigned int memAddr);


/*
 * rapidio发送参数
 */
typedef struct 
{
	unsigned short rw;				//表示读操作(nread包)还是写操作(nwrite包)，0：表示读，1：表示写
	unsigned short controller;		//要求(0-1)
	unsigned int memAddr_H;			//64位地址高位
	unsigned int memAddr_L;			//要求4字节对齐
	unsigned int rioAddr;			//要求4字节对齐
	unsigned int transferLen;		//目前最大8M-32字节
	unsigned short dstID;			//目的id
	unsigned short dbInfo;			//结束门铃信息
	unsigned short dbInfo_s;        //起始门铃信息，针对接口板进行的添加,如果不需要起始门铃，该值填0
} RapidIO_Snd;

/*
 * rapidio接收参数
 */
typedef struct 
{
	unsigned int controller;	
	unsigned short srcID;
	unsigned short dbInfo;
} RapidIO_Recv;


/*
 * 功能：接收pipe初始化
 * 参数：count：接收缓存通道的个数
 * 特殊说明：如果调用了bslUsrRapidIOInit接口，会自动将pipe0和pipe1设为rapidio的发送队列和接收队列，（发送队列和接收队列默认都是50的深度，不能修改）因此至少创建3个以上的pipe才可用
*/
int bslRecvMPipeInit(int count);

/*
 * 功能：从系统内存中，创建多个等长缓存组成的缓存通道
 * 参数：pipeNum表示哪个发送缓存，必须小于RecvMPipeInit的参数count
 * 参数：blockSize表示每个缓存的大小
 * 参数：num：表示有多少个blockSize，即最大的缓存个数
 * 参数：mode缓存模式，0-表示缓存通道满时，直接丢弃；1-表示缓存通道满时，丢弃最久的数据
 * 参数：myFun：回调函数
 * 回调函数参数：pData:当前出缓存通道的数据指针;dataLen：数据长度
 * 返回值：0-表示创建失败；1-表示创建成功 
 */
int bslRecvCreateMPipe(unsigned int pipeNum,int blockSize,int num,unsigned int mode,void (*myFun)(void * pData,unsigned int dataLen));

/*
 * 功能：由用户指定地址，创建多个等长缓存组成的缓存通道
 * 参数：pipeNum表示哪个发送缓存，必须小于RecvMPipeInit的参数count
 * 参数：pPipeBuf：用户指定的缓存首地址，
 * 参数：blockSize表示每个缓存的大小
 * 参数：num：表示有多少个blockSize，即最大的缓存个数
 * 参数：mode缓存模式，0-表示缓存通道满时，直接丢弃；1-表示缓存通道满时，丢弃最久的数据
 * 参数：myFun：回调函数
 * 回调函数参数：pData:当前出缓存通道的数据指针;dataLen：数据长度
 * 返回值：0-表示创建失败；1-表示创建成功 
 */
int bslRecvCreateMBufPipe(unsigned int pipeNum,char *pPipeBuf,int blockSize,int num,unsigned int mode,void (*myFun)(void * pData,unsigned int dataLen));

/*
 * 功能：将数据存入缓存通道中
 * 参数：pipeNum表示哪个发送缓存，必须小于RecvMPipeInit的参数count
 * 参数：pBuf：存入数据的指针，
 * 参数：len：存入数据的长度，应该小于创建该缓存通道的blockSize的大小
 * 返回值：0-表示存入失败；1-表示存入成功 
 */
int bslSndToRecvMPipe(unsigned int pipeNum,char *pBuf,unsigned int len);

/*
 * 功能：删除已经创建的缓存通道，释放缓存空间
 */
void bslRecvMPipeRelease();

/*
 * 功能：显示MPipe的信息状态
 * 参数：pipeNum表示哪个发送缓存，必须小于RecvMPipeInit的参数count
 */
void bslRecvMPipeInfo(unsigned int pipeNum);

/*
 * 功能：显示MPipe的最大缓存个数
 * 参数：pipeNum表示哪个缓存，必须小于RecvMPipeInit的参数count
 * 返回值：返回当前MPipe的最大缓存个数
 */
int bslGetRecvMPipeMaxNum(unsigned int pipeNum);


/*
 * 功能：显示MPipe的可用缓存个数
 * 参数：pipeNum表示哪个缓存，必须小于RecvMPipeInit的参数count
 * 返回值：返回当前MPipe的可用缓存个数
 */
int bslGetRecvMPipeFreeNum(unsigned int pipeNum);

/*
 * 功能：显示进入MPipe的所有信息内容
 * 参数：pipeNum表示哪个缓存，必须小于RecvMPipeInit的参数count
 */
void bslRecvMPipeShowAll(unsigned int pipeNum);


/*
 * 功能:指定显示进入MPipe的信息内容
 * 参数：pipeNum表示哪个缓存，必须小于RecvMPipeInit的参数count
 * 参数：pos：从哪个位置开始显示，0-最大缓存个数-1之间
 * 参数：direction：方向，表明是正向还是反向，必须是1或-1；1表明正向，-1表明反向
 * 参数：count：需要显示的个数     
 */
void bslRecvMPipeShowNum(unsigned int pipeNum,unsigned int pos,int direction,unsigned int count);

/*
 * rapidio初始化，
 * 参数：usrRabRecv回调函数，接收数据回调函数
 * RapidIO_Recv回调函数参数
 * 成功返回0，失败返回-1
 */
int bslUsrRapidIOInit(void(*usrRabRecv)(RapidIO_Recv));

/*
 * rapidio挂接回调函数，
 * 参数：usrRabRecv回调函数，接收数据回调函数
 * RapidIO_Recv回调函数参数
 * 该函数可以在bslUsrRapidIOInit函数调用后再次将回调函数重新挂接
 */
void bslUsrRapidIOConnect(void(*usrRabRecv)(RapidIO_Recv));

/*
 * 功能:设置本地rapidio的节点及路由
 * 参数:
 * u16 rio0ID:rio0的节点号
 * u16 rio1ID:rio1的节点号
 */
void bslRioSetLocalRoute(u16 rio0ID,u16 rio1ID);

/*
 * rapidio配置接收窗口
 * 参数：
 u8 controller:控制器号，(0-1)
 u32 rioAddr：需1M字节对齐
 union datacast_64 memAddr：映射目的LOCAL MEMORY 地址
				   要求2的windowSize次方对齐 
				   该地址如果是32位地址则为程序地址，非物理地址	
				   如果是64位地址，则是物理地址		
 windowSize：映射内存的大小（0-8）
    0- 1 M 
	1- 2 M 
	2- 4 M
	3- 8 M
	4- 16M
	5- 32M
	6- 64M
	7- 128M
	8- 256M
 * 返回值：0:成功；-1失败
 */
int bslUsrRapidIOSetIW(u8 controller,u32 rioAddr,union datacast_64 memAddr,u8 windowSize);

/*
功能：映射INBOUND WINDOWS，
即：将Rapidio 地址映射到LOCAL MEMORY。
参数：
u8 controller:控制器号，(0-1)
u32 rioAddr：需1M字节对齐
u64 memPhyAddr：映射目的LOCAL MEMORY 地址
                要求映射大小是多少就多少字节对齐
                要求是物理地址
u8 windowSize：映射的窗口大小(0-11)
对于34位rapidio地址只能用到8(256M)
后面三个在50位地址时可用
    0- 1 M  (0x100000)
    1- 2 M  (0x200000)
    2- 4 M  (0x400000)
    3- 8 M  (0x800000)
    4- 16M  (0x1000000)
    5- 32M  (0x2000000)
    6- 64M  (0x4000000)
    7- 128M (0x8000000)
    8- 256M (0x10000000)
    9- 512M (0x20000000)
    10 -1G  (0x40000000)
    11 -2G  (0x80000000)
*/
int bslRioSetIW(u8 controller, u32 rioAddr, u64 memPhyAddr, u32 windowSize);

/*
功能：映射Maintenance OUTBOUND WINDOWS，
即：把本地发送的AXI地址映射成维护包发送窗口
参数：
u8 controller:控制器号，(0-1)
修改RAB_APIO_MAINTENANCE_BASE：发送方本地的AXI地址
默认将0窗口配置成维护包窗口
*/

/* 映射的窗口大小,最小1k;默认1M */
void bslRioSetMaintOW(u8 controller);

/*1848初始化*/
int bslUsrRioInit(void);

/*
  * 维护读操作
  * 对于AXI寄存器，是小端模式不需要进行高低字节倒序>=0x20000
*/
void bslRioMaintRead(u8 controller, u16 dstId, u8 hopcount, u32 offset, void  *rddata);

/*
  * 维护写操作
  * 对于AXI寄存器，是小端模式不需要进行高低字节倒序>=0x20000
*/
void bslRioMaintWrite(u8 controller, u16 dstId, u8 hopcount, u32 offset, u32 writedata);

/*
 * rapidio发送接口,该接口为非阻塞接口
 * 参数：rapidio发送参数
 * 成功返回1，失败返回-1
 */
int bslUsrSndRapidIO(RapidIO_Snd param);

/*
 * rapidio发送接口,该接口为阻塞接口
 * controller:控制器号，(0-1)
 * dmaIndex:dma的索引号(0-3)
 * memAddr:64位内存地址，如果高32位为0，则低32位需要使用程序地址；如果高32位是0x20，则低32位需要使用物理地址
 * rioAddr:Rio地址
 * transferLen:传输长度
 * destID:目的id
 * 成功返回1，失败返回-1
 */
int bslRioDmaWrite(u8 controller,union datacast_64 memAddr, u32 rioAddr,u32 transferLen,u16 destID);

/*
功能：获取当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
返回值：返回当前RapidIO的ID号
若为0xff,则当前RapidIO设备没有初始化成功
*/
u16 bslRioGetID(u8 controller);

/*
功能：设置当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
u16 id:设置的rapidio  id号
返回值：
*/
u16 bslRioSetID(u8 controller,u16 id);

/*
功能：发送门铃
参数：
u8 controller:控制器号，(0-1)
int (*dbFuc)(u8)：门铃回调函数指针
返回值:
*/
void bslRioDBInit(u8 controller, int (*dbFuc)(u8));
/*
功能：发送门铃
参数：
u8 controller:控制器号，(0-1)
u16 targetID：门铃发送目的设备的ID号
u16 dbInfo：门铃的信息内容
返回值:0表示成功;-1表示失败
*/
int  bslRioSendDB(u8 controller,u16 targetID,u16 dbInfo); 

/*接受门铃*/
void bslUsrRioDBRecvQuick(u8 controller, RapidIO_Recv *_param);
/*
 * rapidio自动枚举，该函数在没有交换板时使用，只能使用一次,在调用该函数之前，必须先调用bslUsrRapidIOInit
 */
void bslSrioInit();


int nandflashBlockErase(int firstErasableBlock,int numOfErasableBlocks);
int nandflashPageRead(CardAddress address, int intnum,unsigned int * buffer,BOOL eccEnable);
int nandflashPageWrite(CardAddress address, int intnum,unsigned int *buffer,BOOL eccEnable);

/*******************************************************************************
 * flash_pageio_init：
 * 初始化 NAND Flash设备，包括Flash控制器的初始化，相关结构体的分配，数据缓冲的分配等。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		0：   初始化成功
 * 		-1：初始化失败
 */
extern int flash_pageio_init(void);

/*******************************************************************************
 * flash_pageio_read：
 * 从NAND Flash设备读取一页数据。根据相关参数的判定来读取Flash设备的一整页数据(main区和spare区)、单页(main区)、spare区。
 * 
 * 如果(pPageBuff != 0) && (pSpareBuff != 0)，则读取main区和spare区的数据。
 * 如果(pPageBuff != 0) && (pSpareBuff == 0)，则读取main区的数据。
 * 如果(pPageBuff == 0) && (pSpareBuff != 0)，则读取spare区的数据。
 * 
 * 输入：
 * 		 ulStartPage:从Flash设备读取数据的页号。
 *       pPageBuff：存放读取到的main区数据的首地址。
 *       pSpareBuff：存放读取到的spare区数据的首地址。
 * 输出：
 * 		无
 * 返回：
 * 		0：   成功
 * 		-1：失败
 */
extern int flash_pageio_read(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);

/*******************************************************************************
 * flash_pageio_write：
 * 向NAND Flash设备写入一页数据。根据相关参数的判定写入Flash设备的一整页数据(main区和spare区)、单页(main区)、spare区。
 * 
 * 如果(pPageBuff != 0) && (pSpareBuff != 0)，则向main区和spare区写数据。
 * 如果(pPageBuff != 0) && (pSpareBuff == 0)，则向main区写数据。
 * 如果(pPageBuff == 0) && (pSpareBuff != 0)，则向spare区写数据。
 * 
 * 输入：
 * 		 ulStartPage:向Flash设备写入数据的页号。
 *       pPageBuff：存放需写入到Flash设备main区数据的首地址。
 *       pSpareBuff：存放需写入到Flash设备spare区数据的首地址。
 * 输出：
 * 		无
 * 返回：
 * 		0：   成功
 * 		-1：失败
 */
extern int flash_pageio_write(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);

/*******************************************************************************
 * flash_pageio_erase：
 * 擦除NAND Flash设备的一块数据。
 * 
 * 输入：
 * 		 block_num:需擦除的块号。
 * 输出：
 * 		无
 * 返回：
 * 		0：   成功
 * 		-1：失败
 */
extern int flash_pageio_erase(u32 block_num);

/*---SPI 用户接口-------------------------------------------------------------------------------*/
extern int NorFlashErase(int controller, unsigned int addr, unsigned int len);
extern int NorFlashWrite(int controller, unsigned int addr, unsigned char* wbuf, unsigned int len);
extern int NorFlashRead(int controller, unsigned int addr, unsigned char* rbuf, unsigned int len);
extern unsigned int NorFlashReadid(int controller);
extern int NorFlashEraseAll(int controller);
/*---SPI 用户接口-------------------------------------------------------------------------------*/

/*---QSPI 用户接口-------------------------------------------------------------------------------*/
extern int QspiFlashErase(unsigned int addr, unsigned int len);
extern int QspiFlashRead(unsigned int addr, unsigned char* rbuf, unsigned int len);
extern int QspiFlashWrite(unsigned int addr, unsigned char* wbuf, unsigned int len);
extern unsigned int QspiFlashReadid(void);
extern int QspiFlashEraseAll(void);
/*---QSPI 用户接口-------------------------------------------------------------------------------*/


/*打印BSP版本信息*/
void bslBspVersionRead(void);

#ifdef __cplusplus
}
#endif

#endif
