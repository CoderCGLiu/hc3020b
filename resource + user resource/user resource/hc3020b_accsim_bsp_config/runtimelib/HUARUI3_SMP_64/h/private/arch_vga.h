/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为VGA显示头文件
 * 
 */
#ifndef _REWORKS_BSP_VGA_DRIVER_H_
#define _REWORKS_BSP_VGA_DRIVER_H_


#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>

#define MONO_BASE    				0xB0000L   	/* 单色显存基址 */
#define COLOR_BASE   				0xB8000L   	/* 彩色显存基址 */
#define MONO_SIZE    				0x1000    	/* 4K单色显存 */
#define COLOR_SIZE    				0x4000    	/* 16K彩色显存 */
#define EGA_SIZE      				0x8000    	/* EGA & VGA 至少32K */

/* 控制芯片定义*/
#define M_6845         				0x3B4    	/* 6845单色端口 */
#define C_6845         				0x3D4    	/* 6845彩色端口 */
#define EGA            				0x3C4    	/* EGA卡端口 */
#define VID_ORG           			12    		/* 6845起始地址 */
	
#define INDEX              			0    		/* 6845索引寄存器 */
#define DATA               			1    		/* 6845数据寄存器 */

#define CURSOR            			0x0e   		/* 光标寄存器编号 */	
#define CURSOR_SHAPE				0x0a		/* 光标起始扫描行 */

/* 显示选项地址，由BIOS维护 */
#define VIDEO_MODE_ADDR        	 	0x449

/* 显示最大行列数的地址定义 */
#define NB_MAX_COL_ADDR				0x44a
#define NB_MAX_ROW_ADDR				0x484


/* 显示模式信息 */
#define VGAMODE7					0x7			/* VGA mode 7 */

/* 文本模式的前景色和背景色编码 */
#define BLACK       				0x0
#define BLUE        				0x1
#define GREEN       				0x2
#define CYAN       					0x3
#define RED         				0x4
#define MAGENTA     				0x5
#define BROWN       				0x6
#define WHITE       				0x7
#define GRAY        				0x8
#define LT_BLUE     				0x9
#define LT_GREEN    				0xa
#define LT_CYAN     				0xb
#define LT_RED      				0xc
#define LT_MAGENTA  				0xd
#define YELLOW      				0xe
#define LT_WHITE    				0xf
	
#define BLINK      					0x8	/* Mask used to determine blinking */
#define TAB_SPACE 					4	/* TAB键为4个空格 */
	
/* 定义VGA显卡数量 */
#define MAX_VGA_NUM					4
	
#ifdef __cplusplus
}
#endif

#endif
