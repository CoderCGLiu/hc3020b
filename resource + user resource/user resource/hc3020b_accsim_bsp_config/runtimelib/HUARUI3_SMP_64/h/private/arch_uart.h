/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件为串口显示头文件
 * 
 */
#ifndef _REWORKS_BSP_UART_DRIVER_H_
#define _REWORKS_BSP_UART_DRIVER_H_


#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>
	
#define mips_io_port_base 0xbfe00000
#define COM1_BASE_ADDR	(mips_io_port_base + /*0x3f8*/0x1e0)
#define UART_REG1(reg)  (COM1_BASE_ADDR + reg)

#define	SERIAL_BASE(_x)		((_x) ? COM1_BASE_ADDR : COM1_BASE_ADDR)
#define UART_REG(reg,chan)	(SERIAL_BASE(chan) + reg)

//#define XTAL 6272000
//
//#define BAUD_LO(baud)  ((XTAL/baud) & 0xff)
//#define BAUD_HI(baud)  (((XTAL/baud) & 0xff00) >> 8)
	
#define XTAL 6272000

#define BAUD_LO(baud)  ((XTAL/(16*baud)) & 0xff)
#define BAUD_HI(baud)  (((XTAL/(16*baud)) & 0xff00) >> 8)
	
#define DCR			0x11
#define DCR_SDM		0x01
/* Bits definition of the Line Status Register (LSR) */
#define DR	    0x01	/* Data Ready */
#define OE	    0x02	/* Overrun Error */
#define PE	    0x04	/* Parity Error */
#define FE	    0x08	/* Framing Error */
#define BI	    0x10	/* Break Interrupt */
#define THRE	0x20	/* Transmitter Holding Register Empty */
#define TEMT	0x40	/* Transmitter Empty */
#define ERFIFO	0x80	/* Error receive Fifo */
/* Bits definition of the Line Control Register (LCR) */
#define CHR_5_BITS 0
#define CHR_6_BITS 1
#define CHR_7_BITS 2
#define CHR_8_BITS 3

#define WL	    0x03	/* Word length mask */
#define STB	0x04	/* 1 Stop Bit, otherwise 2 Stop Bits */
#define PEN	0x08	/* Parity Enabled */
#define EPS	0x10	/* Even Parity Select, otherwise Odd */
#define SP	    0x20	/* Stick Parity */
#define BCB	0x40	/* Break Control Bit */
#define DLAB	0x80	/* Enable Divisor Latch Access */

/* Register offsets from base address */

#define RBR		0x00	/* receiver buffer register */
#define THR		0x00	/* transmit holding register */
#define DLL		0x00	/* divisor latch */
#define IER		0x01	/* interrupt enable register */
#define DLM		0x01	/* divisor latch(MS) */
#define IIR		0x02	/* interrupt identification register */
#define FCR		0x02	/* FIFO control register */
#define LCR		0x03	/* line control register */
#define MCR		0x04 	/* modem control register */
#define LSR		0x05	/* line status register */
#define MSR		0x06	/* modem status register */
#define SCR		0x07	/* scratch register */

/* Line Status Register */

#define LSR_DR		0x01	/* data ready */
#define RxCHAR_AVAIL	LSR_DR	/* data ready */
#define LSR_OE		0x02	/* overrun error */
#define LSR_PE		0x04	/* parity error */
#define LSR_FE		0x08	/* framing error */
#define LSR_BI		0x10	/* break interrupt */
#define LSR_THRE	0x20	/* transmit holding register empty */
#define LSR_TEMT	0x40	/* transmitter empty */
#define LSR_FERR	0x80	/* in fifo mode, set when PE,FE or BI error */
	
	
#ifdef __cplusplus
}
#endif

#endif
