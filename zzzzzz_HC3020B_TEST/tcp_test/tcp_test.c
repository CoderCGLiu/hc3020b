#include <stdio.h>
#include <stdlib.h>
#include <reworks/types.h>
#include <irq.h>
#include <clock.h>
//#include <gpio.h>
#include <cpu.h>
#include <semaphore.h>
#include <string.h>
#include <sys/socket.h>
#include <ip/sockLib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <net/if.h>

extern unsigned long long ldf_size;
extern unsigned long long ldf_blen;
extern unsigned long long ldf_udelay;
extern unsigned long long ldf_is_bind_nic;
extern char* ldf_send_nic_name;
extern char* ldf_send_ip;
extern char* ldf_recv_nic_name;
extern char* ldf_recv_ip;
extern int ldf_port;


#define LOOP			10000
#define TCP_SERVER_PORT  ldf_port//8503
#define MAX_DATA_LEN    ldf_size//(1*1024)//(16*1024)//(8*1024)
#define TCP_BUF_LEN		ldf_blen
#define SERVER_IP		ldf_recv_ip//"192.168.100.12"
//#define MAX_DATA_LEN    

typedef		int					SOCKET;
typedef 	struct in_addr 		IN_ADDR;
typedef 	struct sockaddr_in 	SOCKADDR_IN;

typedef union
{
	unsigned long long timeVal;
	unsigned long val[2];
}tcpSysTime;

void tcp_rcv()
{
//	if (cpu_id_get() != 0)
//	{
//		
//		return;
//	}
	
	SOCKET			tcpServerSock;
	SOCKADDR_IN		srvInfo,clientInfo;
	char recvBuf[MAX_DATA_LEN];
	int recvNum = 0,recvTotalNum = 0,loop=0;
	int ret = 0,clientInfoLen = 0,cnt = 0;
	
	tcpServerSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(tcpServerSock == 0)
	{
		printf("tcpClient create failed!\n");
		return;
	}
	
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strcpy(ifr.ifr_name, ldf_recv_nic_name);
		printk("ifr.ifr_name : %s\n",ifr.ifr_name);
		if(setsockopt(tcpServerSock, SOL_SOCKET, SO_BINDTODEVICE, &ifr, 16) < 0)
		{
			printk("tcp-recv NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
	
	//设置socket的接收缓冲区为1MB
	int sockRecvBufLen = TCP_BUF_LEN;//(64*1024);//65535;//0x100000;
	ret = setsockopt(tcpServerSock, SOL_SOCKET, SO_RCVBUF,(char *)&sockRecvBufLen, sizeof(sockRecvBufLen));
	if(ret == -1)
	{
		printf("tcpSendSocket_get: socket recv buffer setup failed.\r\n");
	}
	
	//设置服务器的套接字属性
	memset((void *)&srvInfo,0,sizeof(srvInfo));
	memset((void *)&clientInfo,0,sizeof(clientInfo));
	srvInfo.sin_family = AF_INET;
//	srvInfo.sin_addr.s_addr = htonl(INADDR_ANY);
	srvInfo.sin_port = htons(TCP_SERVER_PORT);
	
	//绑定本地端口信息
	ret = bind(tcpServerSock, (struct sockaddr *)&srvInfo, sizeof(srvInfo));
	if(ret < 0)
	{
		printf("tcpServer bind localPort failed!\n");
		close(tcpServerSock);
		return;
	}
	
	//开始侦听，连接数限定为1
	ret = listen(tcpServerSock, 10);	
	if(ret < 0)
	{
		printf("tcpServer can not listen the localPort!\n");
		close(tcpServerSock);
		return;
	}
	printf("connect ok!\n");
	
	SOCKET tcpConnSock;
	//等待远程客户端的连接请求
	clientInfoLen = sizeof(clientInfo);
	tcpConnSock = accept(tcpServerSock, (struct sockaddr *) &clientInfo, &clientInfoLen);
	memset(recvBuf,0,MAX_DATA_LEN);
	while(1)
	{
		
		while(1)
		{
			
			recvNum = recv(tcpConnSock,recvBuf,MAX_DATA_LEN,0);
//			if(recvNum < 0)
//			{
//				printf("tcp recieve failed.\r\n");
//			}
//			else
//			{
//				recvTotalNum+=recvNum;
//			}
//			if(recvTotalNum >= MAX_DATA_LEN)
//			{
//				recvTotalNum = 0;
//				if(recvBuf[0] != cnt)
//				{
//					printf("tcp verify failed,loop = %d.recvBuf[0] = %d,cnt = %d\n",loop,recvBuf[0],cnt);
//	//					close(tcpConnSock);
//					return;
//				}
//			}
//
//			cnt++;
		}

	}
	close(tcpConnSock);
}

int sendPacket(int tcpClientSock,char *sendBuf, int len)
{
	int sendNum = 0,totalNum = 0;
	
	while(totalNum < len)
	{
		sendNum = send(tcpClientSock,(const char *)(sendBuf),len-totalNum,0);
		if(sendNum < 0)
		{
			printf("tcp send failed\r\n");
			return sendNum;
		}
		else
		{
			totalNum = totalNum+sendNum;
		}
	}
	return totalNum;	
}

void tcp_send()
{
//	if (cpu_id_get() != 0)
//	{
//		
//		return;
//	}
	
	SOCKET			tcpClientSock;
	SOCKADDR_IN		srvInfo;
	char sendBuf[MAX_DATA_LEN];
	int sendNum = 0,ret = 0,sendTotalNum = 0,cnt = 0,loop = 0;
	unsigned int coreId = 0;
	coreId = cpu_id_get();
	
	tcpSysTime startTime,stopTime,timeCost;
	float msVal = 0.0;		
	float tcpSpeed = 0.0;
	
	//设置远程服务器信息
	memset((void *)&srvInfo,0,sizeof(srvInfo));
	srvInfo.sin_family = AF_INET;
	srvInfo.sin_port = htons(TCP_SERVER_PORT);
	srvInfo.sin_addr.s_addr = inet_addr(SERVER_IP);
	
	//创建客户端套接字
	tcpClientSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(tcpClientSock < 0)
	{
		printf("tcpClient create failed!\n");
		return;
	}
	
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_send_nic_name, IFNAMSIZ);
		if(setsockopt(tcpClientSock, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("tcp-send NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
	
	
	//设置socket的发送缓冲区为64KB
	int sockSendBufLen = TCP_BUF_LEN;//(64*1024);//65535;//0x2000000;
	ret = setsockopt(tcpClientSock, SOL_SOCKET, SO_SNDBUF,(char *)&sockSendBufLen, sizeof(sockSendBufLen));
	if(ret == -1)
	{
		printf("SND buffer set failed!\n");
	}
	while(1)
	{	
		//尝试连接远程服务器
		//printf("client will try to connect server(0x%x).\n",srvIP);
		ret = connect(tcpClientSock, (struct sockaddr *) &srvInfo, sizeof(srvInfo));
		if(ret < 0)
		{
			printf("Connect to server failed!\n");
			close(tcpClientSock);
			pthread_delay(1);
			printf("++++++++++++++++++++++++++++connect failed!  repeat !!\r\n");
			continue;
		}
		else
			break;
	}	//while	

	cnt = 0;
	sendTotalNum = 0;
//	while(cnt<50)
	while(1)
	{
		
		//准备发送数据
		for(sendNum = 0; sendNum < MAX_DATA_LEN; sendNum++)
		{
			sendBuf[sendNum] = cnt;
		}
		startTime.timeVal = sys_timestamp();

		for(loop=0;loop<LOOP;loop++)
		{
			sendBuf[0] = cnt+loop;
			sendNum = sendPacket(tcpClientSock,(const char *)(sendBuf),MAX_DATA_LEN);
			if(sendNum < 0)
			{
				printf("tcp send failed\r\n");
			}
			else
			{
				sendTotalNum += sendNum;
			}
			loop++;
//			udelay(10);
		}

		stopTime.timeVal =  sys_timestamp();
		
		timeCost.timeVal = stopTime.timeVal - startTime.timeVal;
		msVal = (float)(((double)timeCost.timeVal) / (double)(sys_timestamp_freq() / 1000000));	//us
		tcpSpeed = ((float)sendTotalNum) / (msVal);	//单位：MB/S

		printf("tcpSpeed = %f MB/s\r\n",tcpSpeed);
	
		cnt++;
		sendTotalNum = 0;
	}
	close(tcpClientSock);

}



void tcp_test_main(int is_send)//0-recv, 1-send
{
	/* 添加用户代码 */
	//	int fd = open("/dev/null", 0x2,0777);
	//	global_std_set(0, fd);
	//	global_std_set(1, fd);
	//	global_std_set(2, fd);
		/* 添加用户代码 */ 
	int coreId = 0;//cpu_id_get();
	int cpuId = sysCpuGetID();
//	int slotId = get_local_slotid();
	pthread_t mcSendThd, mcRecvThd;
	int ret; 
	sleep(1);	
	
	if(is_send == 0)//recv
//	if((cpuId==1)&&(coreId==0)) 
//	if((slotId == 2)&&(cpuId == 1)&&(coreId==0))
	{  	   	  
		printk("\ncore%d: create and run tcp recv task.\r\n",coreId);
		
		ret = pthread_create3(&mcRecvThd, "tcpRecvThd", /*157 174 155*/156, 0, 1*1024*1024, 1<<1, tcp_rcv,0);
		if(ret != 0)
		{        
			printk("core_%d: bcRecv task create failed.\r\n",coreId);
		}	
		pthread_detach(mcRecvThd); 
	}	  	                                       
//	sleep(5);                                          
#if 1 
	if(is_send == 1)//send
//	if((cpuId==2)&&(coreId==0))
//  if((slotId == 2)&&(cpuId==4)&&(coreId==0))                 
    { 		
	   printk("\ncore%d: create and run tcp send task.\r\n",coreId); 
	   ret = pthread_create3(&mcSendThd, "tcpSendThd", 155, 0, 1*1024*1024, 1<<1, tcp_send,0);
	   if(ret != 0)
	   {      
		  printk("core_%d: bcSend task create failed.\r\n",coreId);
	   }	
	   pthread_detach(mcSendThd);       
    }  
#endif
	return;
}


void test_tcp_loop(void)
{
	pthread_t mcSendThd, mcRecvThd;
	int ret;
	
	ldf_udelay = 0;
	ldf_size = 1024;
	ldf_blen = 65536;
	
	/*recv*/
	printk("\ncreate and run tcp recv task.\r\n");
	ret = pthread_create3(&mcRecvThd, "tcpRecvThd", 175, 0, 1*1024*1024, 1<<1, tcp_rcv,0);
	if(ret != 0)
	{
		printk("bcRecv task create failed.\r\n");
	}
	pthread_detach(mcRecvThd);
	
	sleep(3);
	
	/*send*/
	printk("\ncreate and run tcp send task.\r\n");
	ret = pthread_create3(&mcSendThd, "tcpSendThd", 175, 0, 1*1024*1024, 1<<2, tcp_send,0);
	if(ret != 0)
	{
	  printk("bcSend task create failed.\r\n");
	}
	pthread_detach(mcSendThd);
}
