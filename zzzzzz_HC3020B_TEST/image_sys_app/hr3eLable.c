/*******************************************************************************
 *
 * 版权：江苏华创微系统有限公司
 * 描述：增加电子标签
 * 创建：2022-07-06  chenghsunzu
 * 修改记录：
 */
#include <stdio.h>
#include <vxWorks.h>
#include <taskLib.h>
#include "string.h"
#include "hr3APIs.h"

#define STRLEN   700
char g_cChardate[STRLEN];

/*
 * 文本解析
 */
static int parseCfgFile_elabel(char *cfgFilePath)
{
    FILE *fd = NULL;
    int i = 0;

    //解析elable.txt脚本
    fd = fopen(cfgFilePath, "r+");
    if(fd == NULL)
    {
        printf("<E>%s not exist!\n","elable.txt");
        return -1;
    }

    printf("parseCfgFile_elable\n");
    memset(g_cChardate, 0, sizeof(g_cChardate));
    while(1)
    {
		if(fgets(g_cChardate + strlen(g_cChardate), 500, fd) == 0)
			break;
		strcat(g_cChardate, "#");
	}
    strcat(g_cChardate, "END!");

	for(i = 0; i < strlen(g_cChardate); i ++)
	{
		if(g_cChardate[i] == '#')
		{
			if((g_cChardate[i + 1] == 'E') && (g_cChardate[i + 2] == 'N') && (g_cChardate[i + 3] == 'D') && (g_cChardate[i + 4] == '!'))
			{
				break;
			}
			printf("\r");
		}
		else
			printf("%c",g_cChardate[i]);
	}
    fclose(fd);
    return 0;
}

/*
 * 功能：通过网络读取电子标签配置文件，并且将文件进行解析，存到Norflash中。
 * 输入：
 *      host: PC机(上位机)IP
 *      fileName，读取电子标签配置文件
 * 输出：
 *      无。
 * 返回值：
 *      函数执行成功，返回0；函数执行失败，返回-1。
 * author: chengshunzu 20220706，参照xyg fl修改
 */
int bslBurneLabel(char * host, char * fileName)
{
    unsigned int i = 0;
    char * pbuf = NULL;
    char * pbuf1 = NULL;
    unsigned int fileLength = 0;
    unsigned int mallocLength = 0;
    unsigned int MErase = 128 * 1024;//擦除128KB空间
    unsigned int flashStart = 0x7fe0000;//开始擦除地址，在最后一个扇区里写电子标签，norflash总共128M(0x100000--0x7ffffff 地址空间)，
    char cmd[100] = {0};
    int iErr = 0;
#if 0
    /* 内存中创建文件夹用于存放镜像 */
    if (0 != system("mkdir /apps/elabel"))
    {
        printf("[ERROR] create elabel failed\n");
        return -1;
    }

    /* 从ftp加载文件 */
    char str[100];

    strcpy(str, "/apps/elabel/");
    strcat(str, fileName);
    sprintf(cmd,"tftp -i %s get %s %s", host, fileName, str);
    printf("%s\n",cmd);
    system(cmd);//从ftp获取文件并存放在elable文件夹中
#endif
	if (0 != ramdev_create("/dev/imags2", 8))
	{
		printf("[ERROR] create ramdev failed\n");
		return ERROR_FUNC_EXEC; 
	}	      
	format("dosfs", "/dev/imags2");	      
	if (0 != mount("dosfs", "/dev/imags2p1", "/imag2"))
	{
		printf("[ERROR] mount ramdev failed\n");
		return ERROR_FUNC_EXEC;  
   }
	
	if(strcmp(fileName, "eLabel.txt") != 0)
	{
		printf("Please Input Correct bootrom image..\n\r");
		return -1;
	}
	
	char str[100];
	strcpy(str, "/imag2/");
	strcat(str, fileName);
	
	ftpGetFile(host,"hr2","hr2",fileName,"/imag2/",1);
	
    parseCfgFile_elabel(str);//解析文件内容

    mallocLength = strlen(g_cChardate);
    pbuf = malloc(mallocLength);//写入的buf
    pbuf1 = malloc(mallocLength);//读出来校验的buf
    if(pbuf1 == NULL || pbuf == NULL)
	{
		printf("malloc memery for elabel tmp failed !\n");
		return -1;
	}
    memset(pbuf, 0x0, mallocLength);
    memset(pbuf1, 0x0, mallocLength);
    strcpy(pbuf, g_cChardate);

    /*这里切换到nor很重要，写入到Nor结束切回到nand*/
    switchToNor();

    printf("Erase = %u Bytes\n",MErase);
    printf("flash erase begin \n");
//    hr2_erase_block(flashStart, MErase);
    NorFlashErase(0, flashStart, MErase);/*ldf 20230605 add*/
    printf("flash erase finish \n");

    fileLength = mallocLength;
//    hr2_norflash_write(flashStart, fileLength, pbuf);
    NorFlashWrite(0, flashStart, pbuf, fileLength);/*ldf 20230605 add*/

    printf("check elabel.txt \n");
//    hr2_norflash_read(flashStart, fileLength, pbuf1);
    NorFlashRead(0, flashStart, pbuf1, fileLength);/*ldf 20230605 add*/

    for(i = 0; i < fileLength; i++)
    {
        if(pbuf1[i] != pbuf[i])
        {
            printf("Flash Check error!\n");
            iErr = -1;
            break;
        }
    }
    free(pbuf);
	free(pbuf1);
	if(iErr == -1)
	{
    	switchToNand();
		return -1;

	}
	else
	{
		printf("bslBurnelabel Finish \n");
		switchToNand();
#if 0
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd,"rm /apps/elabel/%s", fileName);
		system(cmd);
		system("rm /apps/elabel");
#endif
		printf("eLabel Success!\n\r");
		return 1;
	}
}

/*
 * 打印电子标签
 */
int bslShoweLabel(void)
{
	unsigned int flashStart = 0x7fe0000;
	unsigned int fileLength = 0;
	char pBuf[STRLEN];
	int i = 0;
	switchToNor();
//	hr2_norflash_read(flashStart, STRLEN, pBuf);
	NorFlashRead(0, flashStart, pBuf, STRLEN);/*ldf 20230605 add*/
	fileLength = strlen(pBuf);
	for(i = 0; i < fileLength; i ++)
	{
		if(pBuf[i] == '#')
		{
			if((pBuf[i + 1] == 'E') && (pBuf[i + 2] == 'N') && (pBuf[i + 3] == 'D') && (pBuf[i + 4] == '!'))
			{
				break;
			}
			printf("\r");
		}
		else
			printf("%c", pBuf[i]);
	}
	switchToNand();
	return 0;
}
