/*-------------------------------------------文件信息---------------------------------------------------
**
** 文   件   名: hr2reset.c
**
** 创   建   人: xianing
**
** 文件创建日期: 2019 年 09 月 01 日
**
** 描        述: 华睿2号单板复位功能
*********************************************************************************************************/
//#include "SylixOS.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hr3reset.h"
#include <taskLib.h>
#include "hr3APIs.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include "porting.h"
#define AGINGCOUNTMAX 12//应用喂狗的次数循环值未变换下，可继续发送的心跳包数最大值

#define BIT_PCIE_ADDRESS ADDR_LP64(0xffe00000)


//全局变量
struct REGISTER_PKT regPkt;            //注册包
struct REGISTER_ACK_PKT regAckPkt;     //注册回复包
struct HEARTBEAT_PKT hbPkt;            //心跳包
unsigned int curSwap;                   //当前交换板的索引
char localIPAddr0[20];
char castIPAddr0[20];
char sw1IPAddr[20];

unsigned int curSwap = 0;
struct MDL_CFG_INFO mdlCfgInfo[MUDULE_NUM];

void createRegPkt(struct REGISTER_PKT  *registerPkt, unsigned int type);
void createHeartBeat(struct HEARTBEAT_PKT *heartBeat);
void HeartBeatTask();

extern struct sockaddr_in	ResetserverAddr, ResetCastAddr;
extern  int ResetRcvPeerSock,ResetSendCastSock; 
static unsigned int changeCount = 0;//应用喂狗的次数循环值
static unsigned int lastchangeCount = 0;//上一次发送时，应用喂狗的次数循环值
unsigned int heartBeatFlag = 0;//应用喂狗的标志位
unsigned int heartBeatPrintFlag = 0; //0为默认不打印心跳发送，1为打开心跳发送打印
unsigned long long heartBeatCntOS = 0;//操作系统接管进行心跳发送
unsigned long long heartBeatCntOSFail = 0;
unsigned int heartDelayCnt = 50;//500ms

#define MAX_RESPONSE_NUM		5

pthread_t Cast_Thread_ID0,Peer_Thread_ID;
int Cast_Sock_ID0,Peer_Sock_ID;
void hr2Reset(void)
{
    int res,len =0;
    pthread_t tid = 0;
    int sendcout=0;
    unsigned int tryTimes = 0;
    unsigned int slotNum = 0;
    struct REGISTER_PKT  registerPkt;

    //1.网络初始化
    netInit(); 
 
    createRegPkt(&registerPkt, PKT_TYPE_REG);

    printRegPkt(&registerPkt);
    //sendcout=udpSendPeerEx(nCastSocketID0, castIPAddr0,SOCKET0_CAST_PORT, &registerPkt, sizeof(registerPkt));

    while((tryTimes < TRY_TIMES) && (getRegAckPktRecvFlag()!=1))//while(getRegAckPktRecvFlag()!=1)
    {
        tryTimes ++;   
        printf("<L>HeartBeatTask Info:[DSP%u] register packet tryTimes:%u\n", sysCpuGetID(),tryTimes);

        sendcout=udpSendPeerEx(nCastSocketID0, castIPAddr0,SOCKET0_CAST_PORT, &registerPkt, sizeof(registerPkt));
        if(sendcout < 0)
        {
        	printf("udpSendPeerEx failed!\n");
        }
        taskDelay(100*3);
    }

    if(getRegAckPktRecvFlag()!=1)
    {
        printf("<L>HeartBeatTask Info: Send %d times and not recv a RegAckPkt from Switch, Reg failed!\n",TRY_TIMES);
    }else
    {
        printf("<L>HeartBeatTask Info: recv a RegAckPkt from Switch!\n");
    }
    
//	close(Peer_Sock_ID);	
//	pthread_cancelforce(Peer_Thread_ID);
	
     //4.起任务发送心跳包 //22.04.28绑3核，未采用taskSpawn
    res = bslTaskCreateCore("_HeartBeatTask",80,0,0x80000,(FUNCPTR)HeartBeatTask,3,0,1,2,3,4,5,6,7,8,9);
	//心任务优先级105(150)高于tmain任务,但比ftp任务低86(169)，修改为80(175)
    //res = taskSpawn("_HeartBeatTask",80,VX_FP_TASK,0x80000,(FUNCPTR)HeartBeatTask,0,0,0,0,0,0,0,0,0,0);//尝试小于nfs的60
//    res = pthread_create2(&tid, "_HeartBeatTask", 55, RE_FP_TASK, 0x80000, (void *)HeartBeatTask, (void *)0);
    if(res != ERROR)
    {
        printf("<L>HeartBeatTask Info: Create HeartBeat Task Successful!\n");
    }else
    {
        printf("<L>HeartBeatTask Info: Create HeartBeat Task failure!\n");
    }

}

//创建注册包或者修复完成包
void createRegPkt(struct REGISTER_PKT  *registerPkt, unsigned int type)
{
    unsigned int cabnNum = 0;    //机柜号
    unsigned int caseNum = 0;    //机箱号
    unsigned int slotNum = 0;    //槽位号

    cabnNum = sysGetCabnNum();
    caseNum = sysGetCaseNum();
    slotNum = sysGetSlotNum();// - 3;

    //构造结构体
    registerPkt->pktHeader = REGISTER_PKT_HEADER;
    strcpy(registerPkt->mdlType, MODULE_TYPE);
    registerPkt->pktType = type;
    registerPkt->cabnNum = (unsigned char)cabnNum;
    registerPkt->caseNum = (unsigned char)caseNum;
    registerPkt->slotNum = (unsigned char)slotNum;
    registerPkt->dspCnt  = DSP_CNT;
    registerPkt->dspNum  = (unsigned char)sysCpuGetID();
    registerPkt->pktTail = REGISTER_PKT_TAIL;
}

//创建心跳包
void createHeartBeat(struct HEARTBEAT_PKT *heartBeat)
{
    unsigned int cabnNum = 0;    //机柜号
    unsigned int caseNum = 0;    //机箱号
    unsigned int slotNum = 0;    //槽位号

    cabnNum = sysGetCabnNum();
    caseNum = sysGetCaseNum();
    slotNum = sysGetSlotNum() ;//- 3;

    //构造结构体
    heartBeat->pktHeader = HEARTBEAT_PKT_HEADER;
    strcpy(heartBeat->mdlType, MODULE_TYPE);
    heartBeat->pktType = PKT_TYPE_HEARTBEAT;
    heartBeat->cabnNum = (unsigned char)cabnNum;
    heartBeat->caseNum = (unsigned char)caseNum;
    heartBeat->slotNum = (unsigned char)slotNum;
    heartBeat->dspNum  =  (unsigned char)sysCpuGetID();
    heartBeat->hbIndex = 0;
    heartBeat->pktTail = HEARTBEAT_PKT_TAIL;
}

void changeHeartBeatIndex(struct HEARTBEAT_PKT *heartBeat)
{
    heartBeat->hbIndex ++;
}

unsigned int getHeartBeatFlag()
{
    return heartBeatFlag;
}

void setHeartBeatFlag()
{
    heartBeatFlag = 1;
    changeCount = (changeCount + 1);
}

unsigned int getHeartBeatPrintFlag()
{
	if(heartBeatPrintFlag == 0)
	{
		printf("关闭心跳打印.\n");
	}
	else
	{
		printf("放开心跳打印.\n");
	}
    return heartBeatPrintFlag;
}


void setHeartBeatPrintFlag(unsigned int num)
{
	if(num == 0)
	{
		heartBeatPrintFlag = 0;
		printf("设置关闭心跳打印.\n");
	}
	else
	{
		heartBeatPrintFlag = 1;
		printf("设置放开心跳打印.\n");
	}
}

void setHeartDelayCnt(unsigned int num)
{
	if(num == 0)
	{
		printf("设置的heartDelayCnt错误.\n");
		return;
	}
	printf("正在使用的heartDelayCnt= %u, 间隔延时 = %u ms.\n", heartDelayCnt, heartDelayCnt * 10);
	heartDelayCnt = num;
	printf("设置的heartDelayCnt= %u, 间隔延时 = %u ms.\n", heartDelayCnt, heartDelayCnt * 10 );
}

void close_old_socket(void)
{
	close(Cast_Sock_ID0);

	pthread_cancelforce(Cast_Thread_ID0);
}

void creat_new_socket(unsigned char *newlocalIP)
{
	printf("Create New Socket!\n\r");
	
    nCastSocketID0 = udpCreateSocket(localIPAddr0, castIPAddr0, SOCKET0_CAST_PORT);

    if(nCastSocketID0 < 0)
    {
        printf("<E>HeartBeatTask Info:udp cast socket create fail!\n");
        return;
    }
    else
    {
        printf("<L>HeartBeatTask Info:udp cast socket create ok\n");
    }

	Cast_Thread_ID0  = udpGetRecvThreadID(nCastSocketID0);

	Cast_Sock_ID0   = udpGetSockID(nCastSocketID0);
    
}

//心跳任务
void HeartBeatTask()
{
    int ret = 0;
    unsigned int sendfailcount = 0;
    struct HEARTBEAT_PKT heartBeat;
    unsigned int lastChangeCount = 0;
    unsigned int cpuNum = sysCpuGetID();
    createHeartBeat(&heartBeat);
    printHeartBeatPkt(&heartBeat);
    int count = 0;
   
    //操作系统刚启动时接管心跳发送
    while(getHeartBeatFlag() == 0 )
    {
        ret=udpSendPeerEx(nCastSocketID0, castIPAddr0,SOCKET0_CAST_PORT, &heartBeat, sizeof(heartBeat));
        if(ret==-1)
        {
        	heartBeatCntOSFail++;
            printf("<E>系统接管下心跳任务发送失败, cnt=%llu, cpunum=%u !\n", heartBeatCntOSFail, cpuNum);
            while(ret == -1)
            {
            	count++;
            	sleep(1);
            	ret=udpSendPeerEx(nCastSocketID0, castIPAddr0,SOCKET0_CAST_PORT, &heartBeat, sizeof(heartBeat));
            	printf("udp重发心跳count = %d.\n", count);
            	if(count >= 4)
            	{
            		printf("udp心跳重发4次失败.\n");
            		break;
            	}
            }
        }else{
        	if(heartBeatPrintFlag != 0)
        	{
        		printf("发送心跳，Cpu[%u]：heartBeatCntOS=%llu, fail=%llu.\n", cpuNum, heartBeatCntOS,heartBeatCntOSFail);
        	}
            heartBeatCntOS ++;
//            *(unsigned int *)(0xffffffffae600000 + 140) = 100+heartBeatCntOS;
//            *(unsigned int *)(0xffffffffae600000 + 144) = 100+heartBeatCntOSFail;
        }
        if(getHeartBeatFlag() != 0 )
        {
        	printf("<E>系统接管下心跳任务发送失败!\n");
        	close_old_socket();
        	creat_new_socket(localIPAddr0);
            break;
        }
//         taskDelay(HEART_BEAT_DELAY);//taskDelay(1)表示10ms
        taskDelay(heartDelayCnt);
     }
}
