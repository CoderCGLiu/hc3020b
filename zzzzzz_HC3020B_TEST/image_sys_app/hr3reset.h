#ifndef HR2_RESET_H
#define HR2_RESET_H

/************************** 宏定义 *****************************/
//#define HR24V1_BIT_NETWORK
#define MUDULE_NUM 12  //插箱中的模块数量
#define HEART_BEAT_DELAY     500//5000
#define BIT_INTERVAL_DELAY   2500//5000
#define TRY_TIMES 10
#define DSP_CNT 4


//模块类型
#define  MODULE_TYPE "HR24V1"

//注册包包头包尾定义
#define REGISTER_PKT_HEADER 0xA5A5AA55
#define REGISTER_PKT_TAIL   0xAA55A5A5

//回复注册包包头包尾定义
#define REGISTER_ACK_PKT_HEADER 0x5A5A55AA
#define REGISTER_ACK_PKT_TAIL   0x55AA5A5A

//心跳包包头包尾定义
#define HEARTBEAT_PKT_HEADER 0x5A5ACCDD
#define HEARTBEAT_PKT_TAIL   0xDDCC5A5A



//网络包类型
#define PKT_TYPE_REG            0x1     //注册包
#define PKT_TYPE_REP            0x2     //修复完成包
#define PKT_TYPE_REG_ACK        0x3     //注册应答包
#define PKT_TYPE_REP_ACK        0x4     //修复完成应答包
#define PKT_TYPE_HEARTBEAT      0x5     //心跳包
#define PKT_TYPE_SWTOVER        0x6     //交换切换包
#define PKT_TYPE_SWTOVER_ACK    0x7     //交换切换应答包
#define PKT_TYPE_SRIO_FAILED    0x8    //SRIO链路修复失败
#define PKT_TYPE_ACK_SRIO_FAILED  0x9    //SRIO链路修复失败应答
//点播端口号
#define SOCKET0_PEER_PORT 5000
//组播端口号
//#define SOCKET0_CAST_IP 5010
//#define SOCKET1_CAST_IP 5011
//#define SOCKET2_CAST_IP 5012

#define SOCKET0_CAST_PORT 5010
#define SOCKET1_CAST_PORT 5011
#define SOCKET2_CAST_PORT 5012
#define SOCKET3_CAST_PORT 7005

#define SOCKET4_CAST_PORT  5760 //bit
#define __TEST_    1
#define MAX_SLOT_NUM		18
#define MAX_BIT_BYTE		64
#define NET_BIT_LEN			458
#define BOARD_BIT_SIZE     (NET_BIT_LEN*2)

/************************** 全局变量****************************/
struct MDL_CFG_INFO
{
    char mdlType[16];       //模块类型
    unsigned char cabnNum;  //机柜号，暂设置为0
    unsigned char caseNum;  //插箱号
    unsigned char slotNum;  //槽位号
    unsigned char isCfg;    //标示该模块是否在配置文件中，0标示不在，1标示在配置文件中
    unsigned char isReg;    //标示该模块是否注册，0标示未注册，1标示已注册
    unsigned char bak0[3];  //备份
    unsigned int bootNum;   //记录启动次数，标示是否首次启动
    unsigned int bak1;      //备份
};

extern struct MDL_CFG_INFO mdlCfgInfo[MUDULE_NUM];

//注册包结构体定义
struct REGISTER_PKT
{
    unsigned int pktHeader;
    unsigned char mdlType[16];  //模块类型
    unsigned char pktType;      //包类型   0x1表示注册，0x2表示链路修复完成,3,表示SRIO链路修复不成功
    unsigned char cabnNum;      //机柜号
    unsigned char caseNum;      //插箱号
    unsigned char slotNum;      //槽位号
    unsigned char dspCnt;       //dsp总数
    unsigned char dspNum;       //DSP号
    unsigned char bak[2];       //备份
    unsigned int pktTail;
};

//回复注册包结构体定义
struct REGISTER_ACK_PKT
{
    unsigned int pktHeader;
    unsigned char pktType;      //包类型   0x3表示回复注册包(是否完成枚举)，0x4表示修复完成应答包
    unsigned char res;          //包类型为0x3时表示是否枚举完成，包类型为0x4时表示复位的原因
    unsigned char isMastrSlot;  //是否是主管理槽位
    unsigned char bak;       //备份
    unsigned int pktTail;
};

//心跳包结构体定义
struct HEARTBEAT_PKT
{
    unsigned int pktHeader;
    unsigned char mdlType[16];  //模块类型
    unsigned char pktType;      //包类型 0x5
    unsigned char cabnNum;      //机柜号
    unsigned char caseNum;      //插箱号
    unsigned char slotNum;      //槽位号
    unsigned char dspNum;       //DSP号
    unsigned char bak0[3];      //备份
    unsigned int hbIndex;       //心跳包序号
    unsigned int bak1;           //备份
    unsigned int pktTail;
};

//BIT包结构体定义
struct BITNet_PKT
{
    unsigned int pktHeader;
    unsigned char mdlType[16];  //模块类型
    unsigned char pktType;      //包类型 0x9
    unsigned char cabnNum;      //机柜号
    unsigned char caseNum;      //插箱号
    unsigned char slotNum;      //槽位号
    unsigned char dspNum;       //DSP号
    unsigned char buf[64];      //64字节BIT
    unsigned int pktTail;
};

//全局变量
extern struct REGISTER_PKT regPkt;            //注册包
extern struct REGISTER_ACK_PKT regAckPkt;     //注册回复包
extern struct HEARTBEAT_PKT hbPkt;            //心跳包
extern struct BITNet_PKT bitPkt;              //BIT包
extern unsigned int repairFlag;         //是否修复的标记
extern unsigned int regAckPktRecvFlag;  //是否收到注册回复包的标记
extern unsigned int heartBeatFlag;      //心跳包标记
extern unsigned int repOkAckPktRecvFlag;//修复完成应答包标记
extern unsigned int curSwap;                   //当前交换板的索引
extern char localIPAddr0[20];
extern char castIPAddr0[20];
extern char castIPAddr1[20];//bit组播地址
extern char sw1IPAddr[20];
extern char sw2IPAddr[20];
extern unsigned int curSwap;
extern pthread_t Cast_Thread_ID2,Cast_Thread_ID1,Cast_Thread_ID0,Peer_Thread_ID;
extern int Cast_Sock_ID0,Cast_Sock_ID1,Cast_Sock_ID2,Peer_Sock_ID;
extern void netInit();
extern unsigned int getRepairFlag();
extern unsigned int getRegAckPktRecvFlag();
extern unsigned int getRepOkAckPktRecvFlag();


extern int nPeerSocketID0;
//组播socket
extern int nCastSocketID0;

extern void OnPeerSocket0(void * pParent,int nID,char * buf,int len);

//接收到注册包的标志，然后开始计时
static unsigned int s_regStart = 0;
//从接收到第一个注册包开始计时，等待s_regTimeout(秒)的时间，对没有收到注册包的相应模块进行复位
static unsigned int s_regTimeout = 10;

#endif
