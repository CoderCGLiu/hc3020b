#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFLEN   1024
#define MCAST_RECV_IP	"192.168.1.100"

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_MC1
 * 用例版本：S-2021-05-24
 * 测试内容：组播功能测试。
 * 测试说明：ReWorks作为接收端接收组播包。 
 ***********************************************/
void multcast_recv(char *groupIp, int port)
{
	int sock_id;
	struct sockaddr_in addr;
	struct ip_mreq ipmr;
	char buf[BUFLEN];
	int len;
	int ret;
	
	if((sock_id = socket(AF_INET,SOCK_DGRAM,0))<0)
	{
		printf("socket error\n");
		return;
	}
	         
	memset((void *)&addr ,0 ,sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(groupIp);
	
	if(bind(sock_id,(struct sockaddr *)&addr,sizeof(addr))<0)
	{
		printf("bind error\n");
		return;
	}
	
	memset((void *)&ipmr,0,sizeof(ipmr));
	
	ipmr.imr_multiaddr.s_addr = inet_addr(groupIp);
	
	ipmr.imr_interface.s_addr = inet_addr(MCAST_RECV_IP);
	
	if(setsockopt(sock_id, IPPROTO_IP, IP_ADD_MEMBERSHIP, &ipmr, sizeof(ipmr))<0)
	{
		printf("setsockpot :IP_ADD_MEMBERSHIP\n");
		return;
	}
	
	int blen = 65535;
    setsockopt(sock_id, SOL_SOCKET, SO_RCVBUF,(char *)&blen, sizeof(blen));
	
    len = sizeof(addr);
	
    
	while(1)
	{
		
		ret = recvfrom(sock_id, buf, BUFLEN, 0, (struct sockaddr *)&addr, &len);
		if(ret <0)
		{
			printf("core %d recvfrom error\n",cpu_id_get());
			break;
		}
		else 
		{
			buf[ret]='\0';
			printf("core %d receive data is %s\n",cpu_id_get(), buf);
		}
	}

	if(setsockopt(sock_id,IPPROTO_IP,IP_DROP_MEMBERSHIP,&ipmr,sizeof(ipmr))<0)
	{
		printf("setsockopt :IP_DROP_MEMBERSHIP\n");
	}
	
	close(sock_id);
	return;
}
