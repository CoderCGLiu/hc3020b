#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define NET_PASS        0
#define NET_FAIL        1
#define TEST_PORT 10412

/** 
* 
* @addtogroup group_Broadcast 广播(Broadcast)功能测试
* @ingroup group_os_net_notauto
* @{ */

/** 
 *
 * @brief 测试广播功能
 *
 * @par 测试规程：
 *<table>
 *<tr><th>序号</th><th>测试步骤</th><th>预期结果</th></tr>
 *      <tr><td>1.</td><td>客户端调用socket产生一个UDP套接字sockFd</td><td>成功返回套接字描述符 </td></tr>  
 *\n    <tr><td>2.</td><td>客户端调用bzero对地址sockAddr清零并调用  htons 将端口转换为网络端口  </td><td>接收地址设置成功</td></tr>
 *\n    <tr><td>3.</td><td>客户端调用 bind将接收地址sockAddr和套接字sockFd联系起来 </td><td>成功，返回0</td></tr>
 *\n    <tr><td>4.</td><td>服务器调用socket产生一个UDP套接字</td><td>成功返回套接字描述符 </td></tr> 
 *\n    <tr><td>5.</td><td>服务器调用routeAdd手动添加宿主机网址至路由目的地址  </td><td>成功返回0 </td></tr> 
 *\n    <tr><td>6.</td><td>服务器调用setsockopt设置套接字sockFd选项为SO_BROADCAST用支持广播   </td><td>成功返回0 </td></tr> 
 *\n    <tr><td>7.</td><td>服务器调用bzero对地址sockAddr清零并调用  htons 将端口转换为网络端口  </td><td>发送地址设置成功</td></tr>
 *\n    <tr><td>8.</td><td>服务器调用sendto发数据报至只是同一局域网内的客户端</td><td>成功返回发送的字节长度</td></tr>
 *\n    <tr><td>9.</td><td>客户端调用recv从服务器接收广播数据报  </td><td>成功返回接收到的字节数</td></tr>  
 *\n    <tr><td>10.</td><td>服务器、客户端调用close关闭该套接字 </td><td>成功返回0 </td></tr>
 *</table>
 *
 * @par 结果评价标准：
 *    广播功能正常，客户端可接收到服务器发送的数据报。
 *
 * @return 	NET_PASS  测试通过
 * @return 	NET_FAIL  测试失败
 */ 
/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_BC1
 * 用例版本：S-2021-05-24
 * 测试内容：广播功能测试。
 * 测试说明：ReWorks作为接收端接收广播包。 
 ***********************************************/
void brocast_recv()
{
	int sockFd;                   /* socket fd */                   
	struct sockaddr_in sockAddr;  /* socket address to recv from */ 
	char message[50];             /* buffer for broadcast message */
	int   recvNum;                /* number of bytes received */           
	int i;
	
	/* open UDP socket */
	if((sockFd = socket(AF_INET,SOCK_DGRAM,0))<0)
	{ 
		printf ("socket not opened \n");
		return NET_FAIL;
	}

	/* Demonstrates the usage of SIOCGIFCONF ioctl option. 
	 * This SIOCGIFCONF ioctl option is not really needed for broadcasting.  
	 *
	 * Following code segment demonstrates the use of SIOCGIFCONF ioctl option.
	 * Here SIOCGIFCONF option is used to obtain the name of the network
	 * interface. This code segment is not required for receiving broadcasting 
	 * message.
	 */
	/* Zero out and  fill in sockaddr_in structure to receive from*/
//	bzero ((char *) &sockAddr, sizeof (struct sockaddr_in));
	memset((void *)&sockAddr ,0 ,sizeof(sockAddr));
	
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = htons (TEST_PORT);
	sockAddr.sin_addr.s_addr = INADDR_ANY; 

	/* bind to the socket */ 
	if (bind (sockFd, (struct sockaddr *)& sockAddr, sizeof (sockAddr)) == OS_ERROR)
	{
		printf ("bind failed\n");
		close (sockFd); 
		return NET_FAIL;
	}

	
	for(i = 0; i< 10;i++)
	{
		/* receive the broadcast message */
		if ((recvNum = recv (sockFd, message, sizeof (message), 0)) == OS_ERROR)
		{
			printf("recv broadcast failed\n");
			close (sockFd); 
			return NET_FAIL;
		}
		else
			printf ("counter = %d, received %d bytes of broadcast message:  %s\n", i, recvNum, message);
	}
	close (sockFd); 
	
	return NET_PASS;
}
/* @}*/
