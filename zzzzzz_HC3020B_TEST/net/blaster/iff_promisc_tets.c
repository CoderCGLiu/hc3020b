#include <drv/net_board_com.h>
#include "../basic/testing.h"

//#include "../basic/ipcom_pkt.h"

static struct sockaddr_ll
{
    u_int8_t    sll_len;        /* Length of this structure */
    u_int8_t    sll_family;     /* Always AF_PACKET */

    /* 
     * Link layer protocol (frame type) in network order, not used when 
     * sending frames on an SOCK_RAW type socket 
     */
    u_int16_t   sll_protocol;  
    u_int32_t   sll_ifindex;    /* Interface index, must not be zero */

    /* Link layer type (IFT_xxx), only used for received frames */
    u_int16_t   sll_hatype;     

    /* 
     * Packet type (PACKET_xxx), only used for received frames. Valid
     * types are:
     * PACKET_HOST - for packets addressed to the local host
     * PACKET_BROADCAST  - for physical layer broadcast packets to all 
     *                     hosts on this link
     * PACKET_MULTICAST - for packets sent to a physical layer multicast
     *                   address
     * PACKET_OTHERHOST - for packets send to some other host that has
     *                    been caught by a device driver in promiscuous
     *                    mode
     * PACKET_OUTGOING - for packets originated from local host that
     *                   is looped back to a packet socket.
     */
    u_int8_t    sll_pkttype;  
    
    u_int8_t    sll_halen;      /* Length of link layer address */
    u_int8_t    sll_addr[8];    /* Link layer address */
};

#define PRO_BUFFER_MAX 2048
static int recv_test_quit = 0;
static int send_test_quit = 0;
static int print_flag = 0;

#define PRO_ETH_ALEN 6
#define T_ETH_PROTOCOL_ALL 0
#define PRO_ETH_TYPE 0x0800 //以太网协议type

//被测网卡（接收端）信息
#define PRO_NC_NAME_R  "gmac0" 
#define PRO_NC_MAC_R {0x00,0x55,0x7a,0xb5,0x7d,0xf7}
//发送端网卡信息
#define PRO_NC_NAME_W  "gmac1"
#define PRO_NC_MAC_W {0x00,0x55,0x7a,0xb5,0x7d,0xf8}
//发送到其他非被测网卡的mac地址
#define PRO_NC_MAC_OTH {0x00,0x55,0x7a,0xb5,0x7d,0xf9}
		
void PromiscTest_W(void)
{
	int i = 0;
	int datalen,frame_length;
	unsigned char send_buf[PRO_BUFFER_MAX];
	unsigned char data[PRO_BUFFER_MAX];
	unsigned char ether_frame[PRO_BUFFER_MAX];
	unsigned char dst_mac[PRO_ETH_ALEN] = PRO_NC_MAC_OTH; //RAW_NC_MAC_R;	//ljc对方mac
	unsigned char src_mac[PRO_ETH_ALEN] = PRO_NC_MAC_W;

	int send_len = 0;
	int RawSock = -1;
	struct sockaddr_ll sll,TempAddr;
	struct ifreq ifstruct;
	int sockAddrSize = sizeof(struct sockaddr);
	
	
	RawSock = socket(PF_PACKET, SOCK_RAW, htons(T_ETH_PROTOCOL_ALL));
	if(RawSock == -1)
	{   
		printf("create RawSock failed\n");
		return;
	}

	//get net card index
	memset(&ifstruct, 0, sizeof (ifstruct));
	strcpy(ifstruct.ifr_name,PRO_NC_NAME_W);
	if(-1 == ioctl(RawSock,SIOCGIFINDEX,&ifstruct))
	{
		printf("ioctl SIOCGIFINDEX failed\n");
		return;
	}

	memset(&sll,0,sizeof(sll));
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex = ifstruct.ifr_ifindex;
	memcpy (sll.sll_addr, src_mac, PRO_ETH_ALEN);
//	printf("MAC address: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",sll.sll_addr[0],sll.sll_addr[1],sll.sll_addr[2],sll.sll_addr[3],sll.sll_addr[4],sll.sll_addr[5]);
	sll.sll_halen = htons(PRO_ETH_ALEN);
	
	datalen = 12;
	data[0] = 'h';
	data[1] = 'e';
	data[2] = 'l';
	data[3] = 'l';
	data[4] = 'o';
	data[5] = ' ';
	data[6] = 'w';
	data[7] = 'o';
	data[8] = 'r';
	data[9] = 'l';
	data[10] = 'd';
	data[11] = '!';
	 // Fill out ethernet frame header.
	frame_length = 6 + 6 + 2 + datalen;
	// Destination and Source MAC addresses
	memcpy (ether_frame, dst_mac, 6);
	memcpy (ether_frame + 6, src_mac, 6);
    ether_frame[12] = PRO_ETH_TYPE / 256;
    ether_frame[13] = PRO_ETH_TYPE % 256;
    // data
    memcpy (ether_frame + 14 , data, datalen);
#if 1
	if(-1 == bind(RawSock,(struct sockaddr *)&sll,sizeof(sll)))
	{
		printf("bind RawSock failed\n");
		return;
	}
#endif
	
	while(1)
	{
		if(send_test_quit)
			break;
		printf("send packet\n");
		if(print_flag) printf("send start\n");
		send_len = sendto(RawSock,ether_frame,frame_length,0,(struct sockaddr *)&sll,sizeof(sll));
		if(print_flag) printf("send end\n");
		if(send_len <= 0){
			if(print_flag)
				printf("sendto failed send_len = %d errno = %d\n",send_len,errno);
		}else{
			//将print_flag置1可打印发送数据
			if(print_flag)
			{
				printf("send_len %d: ",send_len);
				for(i = 0;i < send_len; i++)
					printf("0x%x ",ether_frame[i]);
				printf("\n");
			}
			send_len = 0;
		}
		pthread_delay(3*sys_clk_rate_get());
	}
	printf("test end\n");
	close(RawSock);
	return;
}

void PromiscTest_R(void)
{
	int i = 0;
	unsigned char recv_buf[PRO_BUFFER_MAX] = {0};

	int recv_len = 0;
	unsigned char src_mac[PRO_ETH_ALEN] = PRO_NC_MAC_R;
	unsigned char oth_mac[PRO_ETH_ALEN] = PRO_NC_MAC_OTH;
	int RawSock = -1;
	struct sockaddr_ll sll,TempAddr;
	int sockAddrSize = sizeof(struct sockaddr);
	struct ifreq ifstruct;
	int PassFlag = TS_FAIL;
//	int equalflag = 0;
	
	RawSock = socket(PF_PACKET, SOCK_RAW, htons(T_ETH_PROTOCOL_ALL));
	if(RawSock == -1)
	{   
		printf("create RawSock failed\n");
		return;
	}

	//get net card index
	memset(&ifstruct, 0, sizeof (ifstruct));
	strcpy(ifstruct.ifr_name,PRO_NC_NAME_R);
	if(-1 == ioctl(RawSock,SIOCGIFINDEX,&ifstruct))
	{
		printf("ioctl SIOCGIFINDEX failed\n");
		return;
	}

	memset(&sll,0,sizeof(sll));
	sll.sll_family = PF_PACKET; 
	sll.sll_ifindex = ifstruct.ifr_ifindex;
	memcpy (sll.sll_addr, src_mac, PRO_ETH_ALEN);
//	printf("MAC address: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",sll.sll_addr[0],sll.sll_addr[1],sll.sll_addr[2],sll.sll_addr[3],sll.sll_addr[4],sll.sll_addr[5]);
	sll.sll_halen = htons(PRO_ETH_ALEN);

	// Destination and Source MAC addresses
	if(-1 == bind(RawSock,(struct sockaddr *)&sll,sizeof(sll)))
	{
		printf("bind RawSock failed\n");
		return;
	}

	//get the netcard work mode
	if(-1 == ioctl(RawSock,SIOCGIFFLAGS,&ifstruct))
	{
		printf("get flag failed\n");
		return;
	}
	//set the netcard work mode 
	ifstruct.ifr_flags |= IFF_PROMISC; 
	if(-1 == ioctl(RawSock,SIOCSIFFLAGS,&ifstruct))
	{
		printf("set flag failed\n");  
		return; 
	} 

	while(1)
	{
		if(recv_test_quit)
			break;
		memset(recv_buf,0,sizeof(recv_buf));
		if(print_flag) printf("recv packet start...\n");
		recv_len = recvfrom(RawSock,recv_buf,sizeof(recv_buf),0,(struct sockaddr *)&TempAddr,(int *)&sockAddrSize); 
//		if(print_flag) printf("recv end\n");
		if(recv_len <= 0){
			PassFlag = TS_FAIL;
			printf("recvfrom error\n");
		}else{
			printf("recv packet success!\n");
			if(print_flag)
			{
				printf("recv_len %d: ",recv_len);
				for(i = 0;i < recv_len; i++)
					printf("0x%x ",recv_buf[i]);
				printf("\n");
			}
			PassFlag = TS_PASS;
			for(i = 0;i < PRO_ETH_ALEN;i++){
				if(recv_buf[i] != oth_mac[i]){
					PassFlag = TS_FAIL;
					break;
				}
			}
			if(TS_PASS == PassFlag) break;
		}
		recv_len = 0;
		pthread_delay(3*sys_clk_rate_get());
	}

//	printf("test end, flag = %d\n",PassFlag);
	Other_GoOnChoice( "test_promisc",PassFlag);
	close(RawSock);
	return;
}

void promisc_send_stop_flag()
{
	send_test_quit = 1;
}
void promisc_recv_stop_flag()
{
	recv_test_quit = 1;
}
//static void *raw_socket_test_task_w(void *arg)
//{ 
//	RawSocketTest_W(); 
//	return;
//}
//
//static void *raw_socket_test_task_r(void *arg)
//{ 
//
//	RawSocketTest_R();	
//	return;
//}

int test_promisc_w()
{
	pthread_t tid;
	int ret = pthread_create2(&tid, "PromiscTest_W", 155, 0, 0x10000, PromiscTest_W, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printf("pthread_create2 -error %d!\n", ret);
		return ERROR;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printf("pthread_detach failed\n");
		return ERROR;
	}
}
/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_PROMISC
 * 用例版本：2021-11-04
 * 测试内容：混杂模式测试。
 * 测试说明：测试网卡设置混杂模式下能否接收非发往本机的包。 
 ***********************************************/
int test_promisc_r()
{
	pthread_t tid;
	int ret = pthread_create2(&tid, "PromiscTest_R", 155, 0, 0x10000, PromiscTest_R, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printf("pthread_create2 -error %d!\n", ret);
		return ERROR;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printf("pthread_detach failed\n");
		return ERROR;
	}
}
