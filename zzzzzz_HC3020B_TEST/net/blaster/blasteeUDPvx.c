/* blasteeUDPvx.c - UDP ethernet transfer benchmark for VxWorks */
                    
/* Copyright 1986-2001 Wind River Systems, Inc. */

/*               
modification history
--------------------
04a,10may01,jgn  checkin for AE
01a,18May99,gow  Created from blastee.c
*/                               
                                 
/*
DESCRIPTION
   With this module, a VxWorks target ("blasteeUDP") receives
   blasts of UDP packets and reports network throughput on 10
   second intervals.

SYNOPSIS
   blasteeUDP (port, size, bufsize, zbuf)

   where:

   port = UDP port for receiving packets
   size = number of bytes per "blast"
   bufsize = size of receive-buffer for blasteeUDP's BSD socket
             (usually, size == bufsize)
   zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)

EXAMPLE
   To start this test, issue the following VxWorks command on the
   target:

   sp blasteeUDP,5000,1000,1000

   then issue the following UNIX commands on the host:

   gcc -O -Wall -o blasterUDP blasterUDPux.c -lsocket   (solaris)
   gcc -O -Wall -DLINUX -o blasterUDP blasterUDPux.c    (linux)
   blasterUDP 10.255.255.8 5000 1000 1000  (use appropriate IP address)

   Note: blasteeUDP should be started before blasterUDP because
   blasterUDP needs a port to send UDP packets to.

   To stop this test, call blasteeUDPQuit() in VxWorks or kill
   blasterUDP on UNIX with a control-c.

   The "blasterUDP"/"blasteeUDP" roles can also be reversed. That is,
   the VxWorks target can be the "blasterUDP" and the UNIX host can
   be the "blasteeUDP". In this case, issue the following UNIX
   commands on the host:

   gcc -O -Wall -o blasteeUDP blasteeUDPux.c -lsocket    (solaris)
   gcc -O -Wall -DLINUX -o blasteeUDP blasteeUDPux.c     (linux)
   blasteeUDP 5000 1000 1000

   and issue the following VxWorks command on the target
   (use appropriate IP address):

   sp blasterUDP,"10.255.255.4",5000,1000,1000

   To stop the test, call blasterUDPQuit() in VxWorks or kill
   blasteeUDP on Unix with a control-c.

CAVEATS
   Since this test loads the network heavily, the target and host
   should have a dedicated ethernet link.

   Be sure to compile VxWorks with zbuf support (INCLUDE_ZBUF_SOCK).

   The bufsize parameter is disabled for VxWorks. Changing UDP socket
   buffer size with setsockopt() in VxWorks somehow breaks the socket.
*/

//#include <vxworks.h>
//#include <ioLib.h>
//#include <logLib.h>
#include <memLib.h>
#include <sys/socket.h>
#include <netinet/in.h>
//#include <sockLib.h>
//#include "zbufSockLib.h"
//#include "zbufLib.h"
#include <stdio.h>		
#include <stdlib.h>
#include <string.h>
//#include <sysLib.h>
//#include "tftpLib.h"
//#include <wdLib.h>
//#include <taskLib.h>
#include <errno.h>
#include <clock.h>
#include <reworks/printk.h>
#include <wdg.h>
#include <pthread.h>
#include <vxworks.h>
#include <taskLib.h>
#include <net/if.h>

static pthread_t tid;          /* task ID for cleanup purposes */
static wdg_t blastWd;    /* for periodic throughput reports */
static char *buffer;     /* receive buffer */
static int sock;    	 /* receiving socket descriptor */
static int blastNum;   	 /* number of bytes read per 10 second interval */
static int quitFlag;   	 /* set to 1 to quit blasteeUDP gracefully */
static int blastCount;   /*带宽计算计数*/

/* forward declarations */

static void blastRate ();
void blasteeUDPQuit(void);       /* global for shell accessibility */

static unsigned long long recv_count = 0;
static unsigned long long pkt_complete = 0;
static unsigned long long pkt_imcomplete = 0;
static unsigned long long pkt_failed = 0;
static unsigned long long total_byte = 0;
static unsigned long long interval_time = 5;//5s
static unsigned int sw_count = 0;
static unsigned long long pkt_dropped = 0;
static unsigned long long total_pkt_dropped = 0;
//static unsigned long long pkt_count_loop = 0, pkt_num_tmp = 0;
//static float dropped_rate = 0.0;
//#include <semLib.h>
//static SEM_ID sem_rate_print = 0;


/*****************************************************************
 *
 * blasteeUDP - Accepts blasts of UDP packets from blasterUDP
 * 
 * This function accepts blasts of UDP packets from blasterUDP
 * and reports the throughput every 10 seconds.
 * 
 * blasteeUDP (port, size, bufsize, zbuf)
 * 
 * where:
 * 
 * port = UDP port to connect with on "blasterUDP"
 * size = number of bytes per "blast"
 * bufsize = size of receive-buffer for blasteeUDP's BSD socket
 * zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)
 * 
 * RETURNS:  N/A
 */

#if 0
void blasteeUDP_func(int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in	serverAddr, clientAddr;
    int nrecv;  /* number of bytes received */
    int sockAddrSize = sizeof(struct sockaddr);
 //   ZBUF_ID             zid;    /* zero-copy buffer ID */

    tid = pthread_self();
//    buffer = (char *) malloc (size);
    buffer = (char *)memalign(256,size);
    
    if(buffer == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
        return;
	}

    /* setup watchdog to periodically report network throughput
     */
    if ((wdg_create(&blastWd)) != 0)
	{
    	printf("cannot create blast watchdog\n");
    	free(buffer);
    	return;
	}

    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
    	printf("cannot open socket\n");
        wdg_delete(blastWd);
        free(buffer);
        return;
	}

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port	= htons(port);
    serverAddr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */
    bzero((char *)&serverAddr.sin_zero, 8);  /* zero rest of struct */

    if(bind(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
	{
    	printf("bind OS_ERROR\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        return;
	}

/* setting buffer size breaks UDP sockets for some reason.  This
 * happens on Linux too (but not Solaris). Hence setsockopt() is
 * commented till further notice.
 */

   if(setsockopt(sock, SOL_SOCKET, SO_RCVBUF,
                   (char *)&blen, sizeof(blen)) < 0)
	{
	   printf("setsockopt SO_RCVBUF failed\n");
       close(sock);
       wdg_delete(blastWd);
       free(buffer);
       exit(1);
	}

    blastNum = 0;
    quitFlag = 0;
    blastCount=0;

    /* loop that reads UDP blasts */
    while(1)
	{
        if(quitFlag) break;
 /*       if (zbuf)
            {
            nrecv = size;
            if ((zid = zbufSockRecvfrom(sock, 0, &nrecv,
                                        (struct sockaddr *)&clientAddr,
                                        (int *)&sockAddrSize)) == NULL)
                nrecv = -1;
            else
                zbufDelete(zid);
            }
        else  */
        nrecv = recvfrom(sock, (char *)buffer, size, 0, 
                              (struct sockaddr *)&clientAddr,
                              (int *)&sockAddrSize);
//        printk("setsockopt receive\n");/*LYT*/
        if(nrecv > 0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum += nrecv;
        	total_byte += nrecv;
        	if(nrecv == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }
        else
        {
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:udp recv failed!! ret=0x%x\n", __FUNCTION__, __LINE__,recv_count,nrecv);
        }
        
        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
        
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", recv_count, pkt_complete,pkt_imcomplete,pkt_failed);
        recv_count++;
	}

    close(sock);
    wdg_delete(blastWd);
    free(buffer);
    printf("blasteeUDP end.\n");
}
void blasteeUDP (int port, int size, int blen, unsigned long zbuf)
{
	int tid;
	
	tid = taskSpawn("blasteeUDP", 170, VX_FP_TASK, 0x100000, (FUNCPTR)blasteeUDP_func,
			port, size, blen, zbuf, 0, 0, 0, 0, 0, 0);
	
	pthread_detach((pthread_t)tid);
	
	return;
}
#else
static void do_blastRate(void);
void blasteeUDP(int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in	serverAddr, clientAddr;
    int nrecv;  /* number of bytes received */
    int sockAddrSize = sizeof(struct sockaddr);

    tid = pthread_self();
//    buffer = (char *) malloc (size);
    buffer = (char *)memalign(256,size);
    
    if(buffer == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
        return;
	}
   
    

    /* setup watchdog to periodically report network throughput
     */
    if ((wdg_create(&blastWd)) != 0)
	{
    	printf("cannot create blast watchdog\n");
    	free(buffer);
    	return;
	}

    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
    	printf("cannot open socket\n");
        wdg_delete(blastWd);
        free(buffer);
        return;
	}
    
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_recv_nic_name, IFNAMSIZ);
		if(setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("udp-recv NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
    
    int reuse = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
	{
    	printf("setsockopt SO_REUSEADDR failed\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port	= htons(port);
    serverAddr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */
    bzero((char *)&serverAddr.sin_zero, 8);  /* zero rest of struct */

    if(bind(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
	{
    	printf("bind OS_ERROR\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        return;
	}

/* setting buffer size breaks UDP sockets for some reason.  This
 * happens on Linux too (but not Solaris). Hence setsockopt() is
 * commented till further notice.
 */

   if(setsockopt(sock, SOL_SOCKET, SO_RCVBUF,
                   (char *)&blen, sizeof(blen)) < 0)
	{
	   printf("setsockopt SO_RCVBUF failed\n");
       close(sock);
       wdg_delete(blastWd);
       free(buffer);
       exit(1);
	}

    blastNum = 0;
    quitFlag = 0;
    blastCount = 0;
    recv_count = 0;
    pkt_complete = 0;
    pkt_imcomplete = 0;
    pkt_failed = 0;
    total_byte = 0;

    /* loop that reads UDP blasts */
    while(1)
	{
        if(quitFlag) break;

        nrecv = recvfrom(sock, (char *)buffer, size, 0, 
                              (struct sockaddr *)&clientAddr,
                              (int *)&sockAddrSize);

        if(nrecv > 0){
        	blastNum += nrecv;
        	total_byte += nrecv;
        	if(nrecv == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }else{
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:udp recv failed!! ret=0x%x\n", __FUNCTION__, __LINE__,recv_count,nrecv);
        }
        
        
  	   if((*(unsigned int*)buffer) != sw_count){ 
 		   pkt_dropped = *(unsigned int*)buffer - sw_count;
 		   total_pkt_dropped = total_pkt_dropped + pkt_dropped;
 		   sw_count = *(unsigned int*)buffer;
  	   }	   
   
  	   if(sw_count == 0xffffffff)
  		   sw_count = 0;
  	   else
 	       sw_count++;
  	   
//		pkt_count_loop = (*(unsigned int*)buffer) - pkt_num_tmp;
//		pkt_num_tmp = *(unsigned int*)buffer;
//		dropped_rate = (float)(((float)total_pkt_dropped/(float)pkt_count_loop)*100);
        
        extern unsigned long long ldf_udelay;
        usleep(ldf_udelay);
        
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", recv_count, pkt_complete,pkt_imcomplete,pkt_failed);
        recv_count++;
	}

    close(sock);
    wdg_delete(blastWd);
    free(buffer);
    printf("blasteeUDP end.\n");
}
#endif

static void blastRate(void)
{
    if(blastNum > 0)
	{
    	printk("UDP-RECV: NO.%u pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d  |  speed:%d byte/s  total:%u MB  | total_pkt_dropped:%d\n", 
    			recv_count,pkt_complete,pkt_imcomplete,pkt_failed,blastNum/interval_time,total_byte/1024/1024,total_pkt_dropped);
    	blastNum = 0;    	
	}
    else
    	printk("No bytes send in the last %u seconds.\n", interval_time);
    blastCount++;
//    if(blastCount > 30) {
//    	quitFlag = 1;
//    	return;
//    }
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);
}


/* make blasteeUDP stop */

void blasteeUDPQuit(void)
{
    quitFlag = 1;
    pthread_delay(60);      
    /* try to end gracefully */
    if(pthread_verifyid(tid) == OS_OK) /* blasteeUDP still trying to receive */
    {
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        pthread_cancel(tid);
        printf("blasteeUDP forced stop.\n");
    }
}
