
#include <vxWorks.h>
#include <reworks/types.h>
#include <taskLib.h>
#include <msgQLib.h>
#include <sysLib.h> 
#include <loadLib.h>
#include <ioLib.h>
#include <stdio.h>
//#include <sysSymTbl.h>
#include <symLib.h>
#include <stdarg.h>
#include <arpa/inet.h>
#include <types/vxTypesOld.h>

typedef unsigned int    Ip_u32;
#define IPCOM_SUCCESS   0
typedef int             Ip_err;

#define IP_AF_INET      2    /* IPv4 */
typedef struct Ipdhcps_poolinfo_struct
{
	Ip_u32	first_addr;      /* First address in pool. Network byte order */
	Ip_u32  last_addr;       /* Last address in pool. Network byte order  */
    char    *classname;      /* Allowed classname                         */
}
Ipdhcps_poolinfo;

#define NFS_PATH "192.168.1.61/ftproot"
#define NFS_MOUNT "/e"

//#define DHCP_TEST
#define VLAN_TEST
//#define NFS_TEST
//#define SQLITE_TEST

#ifdef DHCP_TEST
/************************************************
 * 测试名称：网络服务测试
 * 测试标识：NS_DHCPC
 * 用例版本：2021-09-24
 * 测试内容：DHCP客户端测试。
 * 测试说明：无。 
 ***********************************************/
int dhcpclient_test( )
{
    Ip_err ret;
    ret = ipnet_ipdhcpc_start();
    if(IPCOM_SUCCESS != ret)
    {
        printf("dhcpc start failed! %d\r\n", ret);
        return -1;
    }
    
    return 0;
}

/************************************************
 * 测试名称：网络服务测试
 * 测试标识：NS_DHCPS
 * 用例版本：2021-09-24
 * 测试内容：DHCP服务器测试。
 * 测试说明：无。 
 ***********************************************/
#define DHCP_IFNAME "gmac1"
#define DHCP_STARTIP "192.168.10.26"
#define DHCP_ENDIP "192.168.10.50"
int dhcpserver_test( )
{
    Ip_err ret;
    Ipdhcps_poolinfo pool;
    UINT32 addr = htonl(0xc0a80a00);
    UINT8 mask_opt[7];

    ret = ipnet_ipdhcps_start();
    if(IPCOM_SUCCESS != ret)
    {
        printf("dhcps start failed! %d\r\n", ret);
        return -1;
    }

    ipcom_sleep(3);

    printf("--------DHCP server-----------------\r\n");

    ret = ipdhcps_interface_status_set(DHCP_IFNAME, 1);
    if(IPCOM_SUCCESS != ret)
    {
        printf("interface set failed! %d\r\n", ret);
        return -1;
    }

    if(ipcom_inet_pton(IP_AF_INET, "255.255.255.0", &mask_opt[2])!= 1)
    {
        printf("ipcom_inet_pton0:error\r\n");
        return ERROR;
    }
    
    mask_opt[0] = 1;
    mask_opt[1] = sizeof(UINT32);
    mask_opt[6] = 255;

    ret = ipdhcps_subnet_add(addr, NULL, 0, mask_opt, NULL);
    if(IPCOM_SUCCESS != ret)
    {
        printf("subnet add failed! %d\r\n", ret);
        return -1;
    }

    memset(&pool, 0, sizeof(pool));
    if(ipcom_inet_pton(IP_AF_INET, DHCP_STARTIP, &pool.first_addr) != 1)
    {
        printf("ipcom_inet_pton1:error\r\n");
        return ERROR;
    }
    if(ipcom_inet_pton(IP_AF_INET, DHCP_ENDIP, &pool.last_addr) != 1)
    {
        printf("ipcom_inet_pton2:error\n");
        return ERROR;
    }

    ret = ipdhcps_pool_add(addr, &pool);
    if(IPCOM_SUCCESS != ret)
    {
        printf("interface set failed! %d\r\n", ret);
        return -1;
    }

    return 0;
}
#endif

/************************************************
 * 测试名称：网络服务测试
 * 测试标识：NS_VLAN
 * 用例版本：2021-09-26
 * 测试内容：测试虚拟局域网功能。
 * 测试说明：在reworks端和PC端分别配置ID相同的虚拟局域网，查看能否通信。 
 ***********************************************/
#ifdef VLAN_TEST
int vlan_test(int vid, char* ip, char* mask, char* intf)
{
	char   buf[512], cIP[20], cMask[20];
	struct in_addr iaddr;
	
	iaddr.s_addr = inet_addr(ip);
	inet_ntoa_b(iaddr, cIP);
	iaddr.s_addr = inet_addr(mask);
	inet_ntoa_b(iaddr, cMask);
	
	if(ip == 0)/*无编号*/
		sprintf(buf, "vlan%d create vlan %d vlanif %s", vid, vid, intf);/*lina modify 2016092*/
	else   /*有编号*/
		sprintf(buf, "vlan%d create vlan %d vlanif %s %s netmask %s ", vid, vid, intf, cIP, cMask);/*lina modify 2016092*/
	if(ifconfig(buf)==ERROR)
		return ERROR;
	
	sprintf(buf, "vlan%d up", vid);
	if(ifconfig(buf)==ERROR)
		return ERROR;
	
	return OK;
}
#endif

/************************************************
 * 测试名称：网络服务测试
 * 测试标识：NS_NFSC
 * 用例版本：2021-09-27
 * 测试内容：测试NFS客户端功能。
 * 测试说明：在PC端配置NFS服务器，在reworks挂载nfs至文件系统，
 *        查看reworks端能否读写挂载点的数据。 
 ***********************************************/
#ifdef NFS_TEST
void nfsclient_test(void)
{
	mount("nfs",NFS_PATH,NFS_MOUNT);
}
#endif

/************************************************
 * 测试名称：网络服务测试
 * 测试标识：NS_SQL
 * 用例版本：2021-09-28
 * 测试内容：测试SQLite的基本功能。
 * 测试说明：使用SQLite命令在数据库中创建一个表，用于记录学生的ID
 *        和姓名信息，往该表中写入三组学生信息，然后获取该表的所有
 *        数据，获取到的数据应与写入一致。 
 ***********************************************/
#ifdef SQLITE_TEST 
#include "sqlite3.h" 
int sqlite_test()
{  
	    sqlite3 *db = NULL;  
	    char *zErrMsg = 0;  
	    int rc;  
	    char dbdate[3][20] = {"sunwukong","zhubajie","shaseng"};
	    char dbdate_new[3][20] = {"sunwukong","baigujing","shaseng"};
	    //char *szWorkPath = "/dev/sqlitetest.db";
	    char *szWorkPath = ":memory:";
	    int flag = 0;
	    rc = sqlite3_open(szWorkPath, &db); // 打开指定的数据库文件.  
	    if (rc)  
	    {  
	        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));  
	        sqlite3_close(db);  
	        return (1);  
	    }  
	    else   
	    {  
	        printf("You have opened a sqlite3 database named Sample.db3 successfully!\n");  
	    }  
	    
	    //向数据库写入数据
	    const char* pszSql = "create table student(ID integer primary key,name text);";
	    sqlite3_exec(db,pszSql,NULL,NULL,NULL);
	    const char* pszInsert1 = "insert into student values(1,'sunwukong');";
	    sqlite3_exec(db,pszInsert1,NULL,NULL,NULL); 
	    
	    const char* pszInsert2 = "insert into student values(2,'zhubajie');";
	    sqlite3_exec(db,pszInsert2,NULL,NULL,NULL); 
	    
	    const char* pszInsert3 = "insert into student values(3,'shaseng');";
	    sqlite3_exec(db,pszInsert3,NULL,NULL,NULL);
	    
	    int nrow = 0, ncolumn = 0;  
	    char **azResult; // 二维数组存放结果  
	      
        // 查询数据  
	    char *sql = "select * from student";  
	    sqlite3_get_table(db, sql , &azResult , &nrow , &ncolumn , &zErrMsg);  
	    int i = 0 ;  
        printf("\nrow:%d column=%d \n" , nrow , ncolumn);  
        printf("The result of querying is : \n");  
	    for (i = 0 ; i < ( nrow + 1 ) * ncolumn ; i++)  
	        printf("azResult[%d] = %s\n", i , azResult[i]); 
	    for(i = 0;i < 3;i++)
	    {
	    	if(0 != strcmp(dbdate[i],azResult[i*2+3]))
	    	{
	    		flag = -1;
	    		printf("db date error: azResult[%d] = %s\n",i*2+3,azResult[i*2+3]);
	    	}
	    }
	    
	    
	    //修改数据
	    const char* pszInsert4 = "update student set name='baigujing' where ID='2';";
	    sqlite3_exec(db,pszInsert4,NULL,NULL,NULL);
	    
	 
	    sqlite3_get_table(db, sql , &azResult , &nrow , &ncolumn , &zErrMsg);  
	    printf("\nrow:%d column=%d \n" , nrow , ncolumn);  
	    printf("The result of querying is : \n");  
	    for (i = 0 ; i < ( nrow + 1 ) * ncolumn ; i++)  
	    	printf("azResult[%d] = %s\n", i , azResult[i]);  
	    for(i = 0;i < 3;i++)
	    {
	    	if(0 != strcmp(dbdate_new[i],azResult[i*2+3]))
	    	{
	    		flag = -1;
	    		printf("db date error: azResult[%d] = %s\n",i*2+3,azResult[i*2+3]);
	    	}
	    }
	      
	    // 释放掉  azResult 的内存空间  
	    sqlite3_free_table(azResult);  
	      
	#ifdef _DEBUG_  
	    printf("zErrMsg = %s \n", zErrMsg);  
	#endif  
	      
	    sqlite3_close(db); // 关闭数据库  
	    
	    if(flag == 0){
	    	printf("SQLite test:[PASS]");
	    	return 0;
	    }
	    else{
	    	printf("SQLite test:[FAIL]");
	    	return -1;
	    }
} 
#endif
