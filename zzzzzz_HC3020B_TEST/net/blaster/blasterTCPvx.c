/* blasterTCPvx.c - TCP ethernet transfer benchmark for VxWorks */

/* Copyright 1986-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
04a,10may01,jgn  checkin for AE
02c,20May99,gow  Split off from blaster.c
02b,17May99,gow  Extended to zero-copy sockets
02a,02Mar99,gow  Better documentation. Tested Linux as "blastee".
01b,15May95,ism  Added header
01a,24Apr95,ism  Unknown Date of origin
*/

/*
DESCRIPTION
   With this module, VxWorks transmits blasts of TCP packets to
   a specified target IP/port.

SYNOPSIS
   blasterTCP (targetname, port, size, bufsize, zbuf)

   where:

   targetname = network address of "blasteeTCP"
   port = TCP port to connect with on "blasteeTCP"
   size = number of bytes per "blast"
   bufsize = size of transmit-buffer for blasterTCP's BSD socket
             (usually, size == bufsize)
   zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)

EXAMPLE
   To start this test, issue the following VxWorks command on the
   target:

   sp blasteeTCP,5000,16000,16000

   then issue the following UNIX commands on the host:

   gcc -O -Wall -o blasterTCP blasterTCPux.c -lsocket   (solaris)
   gcc -O -Wall -DLINUX -o blasterTCP blasterTCPux.c    (linux)
   blasterTCP 10.255.255.8 5000 16000 16000  (use appropriate IP address)

   Note: blasteeTCP should be started before blasterTCP because
   blasterTCP needs a port to connect to.

   To stop this test, call blasteeTCPQuit() in VxWorks or kill
   blasterTCP on UNIX with a control-c.

   The "blasterTCP"/"blasteeTCP" roles can also be reversed. That is,
   the VxWorks target can be the "blasterTCP" and the UNIX host can
   be the "blasteeTCP". In this case, issue the following UNIX
   commands on the host:

   gcc -O -Wall -o blasteeTCP blasteeTCPux.c -lsocket    (solaris)
   gcc -O -Wall -DLINUX -o blasteeTCP blasteeTCPux.c     (linux)
   blasteeTCP 5000 16000 16000

   and issue the following VxWorks command on the target
   (use appropriate IP address):

   sp blasterTCP,"10.255.255.4",5000,16000,16000

   To stop the test, call blasterTCPQuit() in VxWorks or kill
   blasteeTCP on Unix with a control-c.

CAVEATS
   Since this test loads the network heavily, the target and host
   should have a dedicated ethernet link.

   Be sure to compile VxWorks with zbuf support (INCLUDE_ZBUF_SOCK).
*/

//#include <vxworks.h>
#include <reworks/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//#include <sockLib.h>
//#include "zbufSockLib.h"
//#include <inetLib.h>
//#include <ioLib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <vxworks.h>
#include <taskLib.h>
#include <wdg.h>
#include <net/if.h>

static wdg_t blastWd;    /* for periodic throughput reports */
static int quitFlag;     /* set to 1 to quit blasterUDP gracefully */
static int blastNum;     /* number of bytes read per 10 second interval */
static int blastCount;   /*带宽计算计数*/

void blasterTCPQuit(void);   /* global for shell accessibility */
static void blastRate(void);

static unsigned long long send_count = 0;
static unsigned long long pkt_complete = 0;
static unsigned long long pkt_imcomplete = 0;
static unsigned long long pkt_failed = 0;
static unsigned long long total_byte = 0;
static unsigned long long interval_time = 5;//5s

/*****************************************************************
 *
 * blasterTCP - Transmit blasts of TCP packets to blasteeTCP
 * 
 * blasterTCP (targetname, port, size, bufsize, zbuf)
 * 
 * where:
 * 
 * targetname = network address of "blasteeTCP"
 * port = TCP port on "blasteeTCP" to receive packets
 * size = number of bytes per "blast"
 * bufsize = size of transmit-buffer for blasterTCP's BSD socket
 * zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)
 * 
 * RETURNS:  N/A
 */

void blasterTCP(char *targetAddr, int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in	sin;
    int s;      /* socket descriptor */
    int nsent; /* how many bytes sent */
    char *buffer;
    int i = 0;

    /* setup BSD socket for transmitting blasts */

    bzero((void *)&sin, sizeof(sin));

    if((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
    	printf("cannot create socket\n");
        return;
    }
    
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_send_nic_name, IFNAMSIZ);
		if(setsockopt(s, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("tcp-send NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif

    sin.sin_addr.s_addr	= inet_addr(targetAddr);
    sin.sin_port	= htons(port);
    sin.sin_family 	= AF_INET;

    if((buffer = (char *)malloc(size)) == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
        close(s);
        return;
	}
    
    for(i = 0; i < size; i++)
    {
    	buffer[i] = 0x12 + i;
    }
    
    /* setup watchdog to periodically report network throughput */

	if((wdg_create(&blastWd)) != 0)
	{
		printf("cannot create blast watchdog\n");
		free(buffer);
		
		return;
	}
	
	wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);

    if(setsockopt(s, SOL_SOCKET, SO_SNDBUF, (void *)&blen,
                    sizeof (blen)) < 0)
	{
    	printf("setsockopt SO_SNDBUF failed\n");
    	free(buffer);
        close(s);
        return;
	}

    if(connect(s, (struct sockaddr *)&sin, sizeof (sin)) < 0)
	{
    	printf("connect failed: host %s port %d\n",
        inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
    	free(buffer);
        close(s);
        return;
	}
    printf("connect ok: [%s:%d]\n", inet_ntoa(sin.sin_addr),ntohs(sin.sin_port));
    
    quitFlag = 0;
    blastNum = 0;
    blastCount = 0;
    send_count = 0;
    pkt_complete = 0;
    pkt_imcomplete = 0;
    pkt_failed = 0;
    total_byte = 0;

    /* Loop that transmits blasts */
    while(1)
	{
    	if(quitFlag) break;
    	
    	*(unsigned int*)buffer = (unsigned int)send_count;
    	
        nsent = write(s, buffer, size);  

        if(nsent > 0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum += nsent;
        	total_byte += nsent;
        	if(nsent == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }
        else
        {
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:tcp send failed!! ret=0x%x\n", __FUNCTION__, __LINE__,send_count,nsent);
        }
        
        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
        
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", send_count, pkt_complete,pkt_imcomplete,pkt_failed);
		send_count++;
	}
    
    /* cleanup */
    free(buffer);
    close(s);
    wdg_delete(blastWd);
    printf("blasterTCP exit.\n");
}

/* watchdog handler. reports network throughput */

static void blastRate ()
{
    if(blastNum > 0)
	{
    	printk("TCP-SEND: NO.%u pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d  |  speed:%d bytes/sec  total:%u MB\r\n", 
    			send_count,pkt_complete,pkt_imcomplete,pkt_failed,blastNum/interval_time,total_byte/1024/1024);
    	blastNum = 0;    	
	}
    else
    	printk("No bytes send in the last %u seconds.\n", interval_time);
    blastCount++;
//    if(blastCount > 30) {
//    	quitFlag = 1;
//    	return;
//    }
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);
}

/* make blasterTCP stop */

void blasterTCPQuit(void)
{
    quitFlag = 1;
}
