#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BROADCAST_ADDR "192.168.0.255" /* your subnet network's broadcast 
                                          * inet address
                                          */ 
#define TEST_PORT 10412

/************************************************
 * �������ƣ��������
 * ���Ա�ʶ��BSP_NET_BC2
 * �����汾��S-2021-05-24
 * �������ݣ��㲥���ܲ��ԡ�
 * ����˵����ReWorks��Ϊ���Ͷ˷��͹㲥���� 
 ***********************************************/
int broadcast_send()
{
	int sockFd; /* socket fd */
	struct sockaddr_in sendToAddr; /* receiver's addresss */
	int sendNum;
	int on;
	char buf[] = "Hello, world!!!\n";   
	
	/* Open UDP socket */
	sockFd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockFd == OS_ERROR)
	{
		perror ("socket not opened ");
		return (OS_ERROR);
	}

//	routeAdd("0.0.0.0", TESTIP);

	/* Use the SO_BROADCAST option when an application needs to broadcast data
	 */

	on = 1; /* turn ON SO_BROADCAST option*/
	if (setsockopt (sockFd, SOL_SOCKET, SO_BROADCAST, (char *) &on, sizeof(int)) ==
			OS_ERROR)
	{
		perror ("setsockopt failed ");
		close (sockFd); 
		return (OS_ERROR);
	}

	/* zero out the sockaddr_in structures and setup receivers' address */
//	bzero ((char *) &sendToAddr, sizeof (struct sockaddr_in));
	memset((void *)&sendToAddr ,0 ,sizeof(sendToAddr));
	
	sendToAddr.sin_family = AF_INET;
	sendToAddr.sin_port = htons (TEST_PORT);
	sendToAddr.sin_addr.s_addr = inet_addr (BROADCAST_ADDR);

	while(1)
	{
		/* send the broadcast message to other systems in the same network */
		if ((sendNum = sendto (sockFd, buf, sizeof (buf), 0, (struct sockaddr *) &sendToAddr,
								sizeof (struct sockaddr_in))) == OS_ERROR)
		{
			perror ("sendto broadcast failed ");
			close (sockFd); 
			return (OS_ERROR);
		}
		sleep(3);
		printf ("%d bytes of broadcast message sent: %s\n", sendNum,
				buf);
	}
	close (sockFd);
	return(TRUE);
}
