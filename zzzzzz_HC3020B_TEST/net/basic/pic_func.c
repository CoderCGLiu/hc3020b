#include "testing.h" 


extern u32 get_c0_sr();
extern void set_c0_sr(u32 val);

void enable_pic()                     
{     
#ifdef TARGET_PENTIUM
	
	/* 开其他中断 */
	int_enable_pic(1);
	int_enable_pic(2);
	int_enable_pic(3);
	int_enable_pic(4);
	int_enable_pic(5);
	int_enable_pic(6);
	int_enable_pic(7);
	int_enable_pic(8);
	int_enable_pic(9);
	int_enable_pic(10);
	int_enable_pic(11);
	int_enable_pic(12);
	int_enable_pic(13);
	int_enable_pic(14);
	int_enable_pic(15);	
	
#endif
	
#ifdef TARGET_SBC8641D
	
	int_enable_pic(39);
	int_enable_pic(25);
	
#endif
	
#ifdef TARGET_LOONGSON2F
	
	/*使能其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr |=((1 << 10) | (1 << 14));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOONGSON2H
	
	/*使能其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr |=((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOONGSON1B
	
	/*关闭其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr &=~((1 << 10) | (1 << 14));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOOGSON3A
	
	/*使能其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr |=((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_HUARUI
	
	/*使能其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr |=((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
}                        
 
void disable_pic()                     
{     
#ifdef TARGET_PENTIUM
	
	/* 关其他中断 */
	int_disable_pic(1);
	int_disable_pic(2);
	int_disable_pic(3);
	int_disable_pic(4);
	int_disable_pic(5);
	int_disable_pic(6);
	int_disable_pic(7);
	int_disable_pic(8);
	int_disable_pic(9);
	int_disable_pic(10);
	int_disable_pic(11);
	int_disable_pic(12);
	int_disable_pic(13);
	int_disable_pic(14);
	int_disable_pic(15);
	
#endif
	
#ifdef TARGET_SBC8641D
	
	int_disable_pic(39);
	int_disable_pic(25);
	
#endif
	
#ifdef TARGET_LOONGSON2F
	
	/*关闭其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr &=~((1 << 10) | (1 << 14));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOONGSON1B
	
	/*关闭其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr &=~((1 << 10) | (1 << 14));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOONGSON2H
	
	/*使能其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr |=((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_LOOGSON3A
	
	/*关闭其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr &=~((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
	
#ifdef TARGET_HUARUI
	
	/*关闭其他外部中断 IP2 IP3*/
	u32 sr = get_c0_sr();
	sr &=~((1 << 10) | (1 << 11));
	set_c0_sr(sr);
	
#endif
}    
