#include "testing.h"

#ifdef _USER_TIME_TEST
#ifdef __PENTIUM__
u64 user_timestamp()                           
{    
	u64 value;                                  
	asm volatile(".byte 0x0F, 0x31" : "=A"(value));                                                 
	return value;  
}   
#endif 

u32 user_timestamp_freq()                      
{     
#ifdef __PENTIUM__
	return 1795375863;  
#endif
	
#ifdef TARGET_SBC8641D
	return 100000000;  
#endif
	
#ifdef TARGET_P1020
	return 50000000;  
#endif	
	
#ifdef TARGET_LOONGSON2F
	return 400000000;  
#endif
	
#ifdef TARGET_LOONGSON1B
	return 126100000; 
#endif
	
#ifdef TARGET_LOONGSON2H
	return 800000000; //并不是每块板子都一样
#endif
	
#ifdef TARGET_LOOGSON3A /* 龙芯3a 开发板的频率是 900m，而套件是 800 m*/
	return 800000000;  
#endif
	
#ifdef TARGET_HUARUI
	return 500000000;  
#endif
}     

#endif
