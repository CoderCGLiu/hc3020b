#include "../basic/testing.h"
/***
 * C66x core
 */  

#if defined FT_M6678 || defined TI_C6678
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR (0x1F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 8
#endif

/* 
 * PowerPC
 * 
 * */
#if defined(PPC_8641D_PLAT) || defined(PPC_8640D_PLAT)
	/* 后来版本修改后，4，8 修改为16、16； PPC835与8640d相同，835使用PPC_8640D_PLAT */
    #define MEM_ALIGN_SIZE 16
    #define SIZEOF_MEM_STRUCT 16 /* for First-fit */
	#define INVALID_MEM_ADDR  (0x1F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 2
#endif


/***
 * 飞腾1500A 
 */  
#if defined ARM7_CC_FT1500_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR 0xD0000000
	#define VX_MAX_SMP_CPUS 4
	#define PARTFD_ALIGNMENT 8
#endif

#if defined ARM7_CET32_FT2000_PLAT 
	#define MEM_ALIGN_SIZE 4
	#define INVALID_MEM_ADDR 0xD0000000
	#define VX_MAX_SMP_CPUS 2
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 16
	#define SIZEOF_MEM_STRUCT 16 /* for First-fit */
#else
	#define PARTFD_ALIGNMENT 8
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
#endif
#endif

#if defined ARM7_NEW_FT2000_PLAT 
	#define MEM_ALIGN_SIZE 4
	#define INVALID_MEM_ADDR 0xD0000000
	#define VX_MAX_SMP_CPUS 4
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 16
	#define SIZEOF_MEM_STRUCT 16 /* for First-fit */
#else
	#define PARTFD_ALIGNMENT 8
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
#endif
#endif

#if defined ARM7_CET32_FT1500_PLAT 
	#define MEM_ALIGN_SIZE 4
	#define INVALID_MEM_ADDR 0xD0000000
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 16
	#define SIZEOF_MEM_STRUCT 16 /* for First-fit */
	#define VX_MAX_SMP_CPUS 16
#else
	#define PARTFD_ALIGNMENT 8
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define VX_MAX_SMP_CPUS 16
#endif
#endif
#ifdef ARM7_IMX6D_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR 0xD0000000
	#define VX_MAX_SMP_CPUS 4
#endif

#ifdef ULTRASCALE_PLAT
#ifdef __multi_core__
	#define SIZEOF_MEM_STRUCT 16/* for First-fit */
    #define MEM_ALIGN_SIZE 16
	#define VX_MAX_SMP_CPUS 4
	#define PARTFD_ALIGNMENT 32
#else
	#define SIZEOF_MEM_STRUCT 8/* for First-fit */
    #define MEM_ALIGN_SIZE 4
	#define PARTFD_ALIGNMENT 16
#endif
#define INVALID_MEM_ADDR 0xD0000000

#endif

#ifdef ARM_EM8301_PLAT
	#define MEM_ALIGN_SIZE 4
	#define INVALID_MEM_ADDR 0xD0000000
	#define PARTFD_ALIGNMENT 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
#endif

#ifdef PPC_1020_PLAT
	#define MEM_ALIGN_SIZE 8
    #define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR  (0x5F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 2
	#define PARTFD_ALIGNMENT 8 //??
#endif

#ifdef PPC2041_PLAT 
	#define MEM_ALIGN_SIZE 8
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR  (0x5F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 8
#endif


#ifdef PPC4080_PLAT
#define MEM_ALIGN_SIZE 8
#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
#define INVALID_MEM_ADDR (0x80000000 + 0x1000000)
#define VX_MAX_SMP_CPUS 8
#endif

/* http://192.168.1.194:8080/browse/VERSION-1685，ABI标准修改PPCE300和PPC603 cpu对齐字节数*/
#ifdef PPC_8245_PLAT
	#define MEM_ALIGN_SIZE  8	
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR  0xa0000000
#endif

#ifdef PPC8377_PLAT
	/* 单核版本 */
	#define MEM_ALIGN_SIZE  8	
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR (0x80000000 + 0x1000000)
#endif

#ifdef  PPC_8247_PLAT
	#define MEM_ALIGN_SIZE  4	
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR  0xa0000000
#endif

#ifdef PPC_7448
#endif


/* 
 * ARM
 * 
 * */
#ifdef ARM_9G45
#endif

#ifdef ARM7_3730_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */	
 	#define INVALID_MEM_ADDR  (0x1F000000 + 0x1000000)
#endif

#ifdef ARMv7_IMX6Q_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */	
 	#define INVALID_MEM_ADDR  (0x1F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 2
	#define PARTFD_ALIGNMENT 8
#endif

#ifdef ARMv7_ZYNQ7K_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */	
 	#define INVALID_MEM_ADDR  (0x1F000000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 2
	#define PARTFD_ALIGNMENT 8
#endif


#ifdef ARM7_FT2000_PLAT
	#define MEM_ALIGN_SIZE 4
	#define SIZEOF_MEM_STRUCT 8 /* for First-fit */
	#define INVALID_MEM_ADDR 0xD0000000
	#define VX_MAX_SMP_CPUS 2
	#define PARTFD_ALIGNMENT 8
#endif
/* 
 * 
 * MIPS
 * 
 * */
#ifdef LOONGSON_2F_PLAT
	#define SIZEOF_MEM_STRUCT 8/* for First-fit */
    #define MEM_ALIGN_SIZE 8
    #define INVALID_MEM_ADDR (0x200000 + 0x3000)
#endif

#ifdef LOONGSON_1B_PLAT
	#define SIZEOF_MEM_STRUCT 8/* for First-fit */
    #define MEM_ALIGN_SIZE 8
    #define INVALID_MEM_ADDR (0x200000 + 0x3000)
#endif

#ifdef LOONGSON_1A_PLAT
	#define SIZEOF_MEM_STRUCT 8/* for First-fit */
    #define MEM_ALIGN_SIZE 8
    #define INVALID_MEM_ADDR (0x200000 + 0x3000)
#endif

#ifdef LOONGSON_2H_PLAT
	#define SIZEOF_MEM_STRUCT 8/* for First-fit */
    #define MEM_ALIGN_SIZE 8
    #define INVALID_MEM_ADDR (0x200000 + 0x3000)
#endif

#ifdef LOONGSON_3A_PLAT 
	#define SIZEOF_MEM_STRUCT 16/* for First-fit */
    #define MEM_ALIGN_SIZE 16
    #define INVALID_MEM_ADDR  (0x80200000 + 0x80000000)
	#define VX_MAX_SMP_CPUS 4
	/*在32位系统中，BLOCK_HDR所占的内存为16B，在64位系统中增长到32B */
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 32
#else
	#define PARTFD_ALIGNMENT 16
#endif
#endif

#ifdef LOONGSON_2K_PLAT 
	#define SIZEOF_MEM_STRUCT 16/* for First-fit */
    #define MEM_ALIGN_SIZE 16
    #define INVALID_MEM_ADDR  (0x80200000 + 0x80000000)
	#define VX_MAX_SMP_CPUS 2
	/*在32位系统中，BLOCK_HDR所占的内存为16B，在64位系统中增长到32B */
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 32
#else
	#define PARTFD_ALIGNMENT 16
#endif
#endif

#ifdef SW410_PLAT 
    #define MEM_ALIGN_SIZE 32
    #define SIZEOF_MEM_STRUCT 32/* for First-fit */
	 #define INVALID_MEM_ADDR  (0x80200000 + 0x80000000)
	#define VX_MAX_SMP_CPUS 4
    #define CPU_ALIGNMENT 32
#endif

#ifdef SW121_PLAT
	#define SIZEOF_MEM_STRUCT 32/* for First-fit */
    #define MEM_ALIGN_SIZE 16
    #define INVALID_MEM_ADDR  (0x80200000 + 0x80000000)
	/*在32位系统中，BLOCK_HDR所占的内存为16B，在64位系统中增长到32B */
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 32
#else
	#define PARTFD_ALIGNMENT 16
#endif
#endif

#ifdef HUARUI2_SMP_64_PLAT 
	#define SIZEOF_MEM_STRUCT 32/* for First-fit */
    #define MEM_ALIGN_SIZE 16
    #define INVALID_MEM_ADDR  (0x80200000 + 0x80000000)
	#define VX_MAX_SMP_CPUS 4
	/*在32位系统中，BLOCK_HDR所占的内存为16B，在64位系统中增长到32B */
#ifdef _WRS_CONFIG_LP64
	#define PARTFD_ALIGNMENT 32
#else
	#define PARTFD_ALIGNMENT 16
#endif
#endif
/* 
 * 
 * PENTIUM
 * 
 * */
#ifdef PENTIUM_PLAT
    #define SIZEOF_MEM_STRUCT 8 /* for First-fit */
    #define MEM_ALIGN_SIZE 8
	#define PARTFD_ALIGNMENT 8
	#define CPU_ALIGNMENT MEM_ALIGN_SIZE
	#define INVALID_MEM_ADDR  (0x00100000 + 0x1000000)
	#define VX_MAX_SMP_CPUS 2
#endif

#ifdef BM3803_PLAT
    #define MEM_ALIGN_SIZE 8
    #define SIZEOF_MEM_STRUCT 8/* for First-fit */
	#define INVALID_MEM_ADDR  (0x1F000000 + 0x1000000)
#endif

#ifdef BM3823_PLAT
    #define SIZEOF_MEM_STRUCT 8 /* for First-fit */
    #define MEM_ALIGN_SIZE 8
	#define PARTFD_ALIGNMENT 8
	#define CPU_ALIGNMENT MEM_ALIGN_SIZE
	#define INVALID_MEM_ADDR  (0x30003000 + 0x1000000)
#endif

#ifdef S698PM_PLAT
    #define MEM_ALIGN_SIZE 8
    #define SIZEOF_MEM_STRUCT 8/* for First-fit */
	#define VX_MAX_SMP_CPUS 4
#endif
