#ifdef __VXWORKS__
	#include <vxWorks.h>
	#include <taskLib.h>
	#include <stdio.h>
	#include <vxAtomicLib.h>
	#include <vxCpuLib.h>
	#include <cpuset.h>
	#include <unistd.h>
	#include <intLib.h>
	#define	u32		UINT32
	#define	u64		UINT64
#else
	#include <stdio.h>
	#include <reworks/types.h>
	#include <reworks/thread.h>
	#include <atomic.h>
#endif

#include "testing.h"

static char syn_value[4] = {0x01,0x03,0x07,0x0F};

extern u64 user_timestamp();
extern u32 vxCpuIdGet (void);

volatile atomic_t sync_flag_ls = 0;

__attribute__ ((aligned(32))) static u64 t[4*4] = {0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee};

static void systm_sync_task(void *arg)
{	
#ifdef 	__VXWORKS__
	register unsigned int cpu_id_value = 1 << vxCpuIdGet();
	
//	unsigned int level = intCpuLock();

	asm volatile("sync");
	if (_WRS_LIKELY((sync_flag_ls | cpu_id_value) == syn_value[CPU_MAX_NUM-1]))
	{
		sync_flag_ls |= cpu_id_value;
	}
	else
	{
		vxAtomicOr(&sync_flag_ls, cpu_id_value);
		
		do{
				asm volatile("sync");
		}while (sync_flag_ls != syn_value[CPU_MAX_NUM-1]);
	}

#else
	register u32 cpu_id_value = 1 << cpu_id_get();
	u32 level = int_lock();
	
	asm volatile("sync");
	if (LIKELY((sync_flag_ls | cpu_id_value) == syn_value[CPU_MAX_NUM-1]))
	{
		sync_flag_ls |= cpu_id_value;
	}
	else
	{
		atomic_or(&sync_flag_ls, cpu_id_value);
		
		do{
				asm volatile("sync");
		}while (sync_flag_ls != syn_value[CPU_MAX_NUM-1]);
	}

#endif

#ifdef TARGET_LOONGSON3A_R2 
	user_timestamp_init(0xc000100f);
#elif defined TARGET_LOONGSON3A_R3
	user_timestamp_init(0xc000100f);     //ajm
#else
	user_timestamp_init(0xc000000f);
#endif

#ifdef 	__VXWORKS__   
//    intCpuUnlock(level);
#else
    int_unlock(level);
#endif
}

static void systm_sync_check(void *arg)
{	
#ifdef 	__VXWORKS__
	register unsigned int cpu_id = vxCpuIdGet();
	register unsigned int cpu_id_value = 1 << vxCpuIdGet();
	
//	unsigned int level = intCpuLock();

	asm volatile("sync");
	if (_WRS_LIKELY((sync_flag_ls | cpu_id_value) == syn_value[CPU_MAX_NUM-1]))
	{
		sync_flag_ls |= cpu_id_value;
	}
	else
	{
		vxAtomicOr(&sync_flag_ls, cpu_id_value);
		
		do{
				asm volatile("sync");
		}while (sync_flag_ls != syn_value[CPU_MAX_NUM-1]);
	}

#else
	register u32 cpu_id = cpu_id_get();
	register u32 cpu_id_value = 1 << cpu_id_get();

	u32 level = int_lock();
	
	asm volatile("sync");
	if (LIKELY((sync_flag_ls | cpu_id_value) == syn_value[CPU_MAX_NUM-1]))
	{
		sync_flag_ls |= cpu_id_value;
	}
	else
	{
		atomic_or(&sync_flag_ls, cpu_id_value);
		
		do
		{
			asm volatile("sync");
		}while (sync_flag_ls != syn_value[CPU_MAX_NUM-1]);
	}

#endif
    t[cpu_id*4] = user_timestamp();

#ifdef 	__VXWORKS__   
//    intCpuUnlock(level);
#else
    int_unlock(level);
#endif
}

int systm_sync_ls()
{
    cpuset_t affinity;
    int tid, pri;
    int i;
    
    sync_flag_ls = 0;
    
#ifdef __VXWORKS__    
    taskPriorityGet(0, &pri);
    pri = pri + 1;
#else
    pthread_getschedprio(pthread_self(), &pri);
    pri = pri - 1;
#endif

    for(i = 0; i < CPU_MAX_NUM; i++) { 
#ifdef __VXWORKS__    	
		/* Create the task but only activate it after setting its affinity */
		tid = taskCreate ("TBSync", pri, 0, 5000, (FUNCPTR)systm_sync_task,
					  0, 0, 0, 0,0, 0, 0, 0, 0, 0);
#else
		//pthread_create3(&tid, "TBSync", 255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK, 
		pthread_create3(&tid, "TBSync", pri, RE_UNBREAKABLE | RE_KERNEL_TASK,
				4096, 1<<i, systm_sync_task, 0);
#endif		
		if (tid == 0) {        
			printf("taskCreate faild\n");
			goto FAILD;
		}

#ifndef __VXWORKS__
		pthread_detach(tid);
#endif
		
#ifdef __VXWORKS__ 	
		/* Clear the affinity CPU set and set index for CPU 1 */
		CPUSET_ZERO(affinity);
		CPUSET_SET(affinity, i);
	
		if (taskCpuAffinitySet(tid, affinity) == ERROR) {
			/* Either CPUs are not enabled or we are in UP mode */
			taskDelete (tid);
			printf("taskCpuAffinitySet faild\n");
			goto FAILD;
		}
	
//		/* Now let the task run on CPU 1 */
		taskActivate (tid);
#endif		
    }

#ifdef __VXWORKS__
    taskDelay(10);
#else
//    printk("pthread_delay enter \n");//zhaoyf
    pthread_delay(10);
//    printk("pthread_delay out \n");//zhaoyf
#endif
    
    return 0;    
FAILD:
	return -1;
}

int systm_check_ls()
{
    cpuset_t affinity;
    int tid, pri;
    int i;
    
    sync_flag_ls = 0;
    
#ifdef __VXWORKS__    
    taskPriorityGet(0, &pri);
    pri = pri + 1;
#else
    pthread_getschedprio(pthread_self(), &pri);
    pri = pri - 1;
#endif

    for(i = 0; i < CPU_MAX_NUM; i++) {    	
#ifdef __VXWORKS__    	
		/* Create the task but only activate it after setting its affinity */
		tid = taskCreate ("TBSync", pri, 0, 5000, (FUNCPTR)systm_sync_check,
					  0, 0, 0, 0,0, 0, 0, 0, 0, 0);
#else
		//pthread_create3(&tid, "TBSync", 255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK, 
		pthread_create3(&tid, "TBSync", pri, RE_UNBREAKABLE | RE_KERNEL_TASK,
				4096, 1<<i, systm_sync_check, 0);
#endif		
		if (tid == 0) {        
			printf("taskCreate faild\n");
			goto FAILD;
		}

#ifndef __VXWORKS__
		pthread_detach(tid); //
#endif
		
#ifdef __VXWORKS__ 	
		/* Clear the affinity CPU set and set index for CPU 1 */
		CPUSET_ZERO(affinity);
		CPUSET_SET(affinity, i);
	
		if (taskCpuAffinitySet(tid, affinity) == ERROR) {
			/* Either CPUs are not enabled or we are in UP mode */
			taskDelete (tid);
			printf("taskCpuAffinitySet faild\n");
			goto FAILD;
		}
	
//		/* Now let the task run on CPU 1 */
		taskActivate (tid);
#endif		
    }

#ifdef __VXWORKS__
    taskDelay(10);
#else
    pthread_delay(10);
#endif
    
    printf("t[0] = %lld\n", t[0]);
    printf("t[1] = %lld\n", t[4]);
    if(CPU_MAX_NUM == 4)
    {
    	printf("t[2] = %lld\n", t[8]);
    	printf("t[3] = %lld\n", t[12]);
    }
    
    return 0;    
FAILD:
	return -1;
}

void multicore_ts_sync()
{
	systm_sync_ls();
	systm_check_ls();
}
