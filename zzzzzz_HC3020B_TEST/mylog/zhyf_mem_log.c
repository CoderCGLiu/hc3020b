/*******************************************************************************
 *
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：用户应用入口程序
 * 修改：
 *
 */

#include <stdio.h>
#include <pthread.h>
#include <pthread_hook.h>

/*
 *zhyf_open_log_mem  
 *zhyf_stop_log_mem 
 */

typedef struct _log_mem
{
	u64 alloc_time;
	u64 free_time;
	u64 alloc_addr;
	int alloc_size;
	u64 free_addr;
	u32 type;	//0 alloc 1 free
	pthread_t alloc_task_id;
	pthread_t free_task_id;
	char executing_thread_name[32];
//	char file_path[128];
//	char func_name[32];
//	int line;
}LOG_MEM;


#define LOG_MEM_NUM (20000)

LOG_MEM log_mem_data[LOG_MEM_NUM];

unsigned int log_mem_data_index=0;
int zhyf_log_mem_stop_flag = 1;

static void *alloc_log_hook(void *addr, int size);
static void *free_log_hook(void *addr);
void zhyf_open_log_mem()
{
	if(zhyf_log_mem_stop_flag)
	{
		zhyf_log_mem_stop_flag = 0;
		mem_stat_hook_add (alloc_log_hook,free_log_hook);
		printf("mem_stat_hook_add!\n");
	}
	else{
//		zhyf_log_mem_stop_flag = 0;
		printf("mem_stat_hook_add! restart\n");
	}
	return;
}

static void *alloc_log_hook(void *addr, int size)
{
	if( ((unsigned long)addr >= 0xf7025640UL) && \
			((unsigned long)addr <= 0xf70302c0UL)
	)
	{
		printek("<**DEBUG**> [%s():_%d_]:: addr=0x%lx, size=%d\n", __FUNCTION__, __LINE__,addr,size);
	}
	
#if 0
	if(size == 64)
		printek("size64\n");
#else
	if(size == 8)
		printk("alloc_task_id:0x%x\n",pthread_self());
	if(zhyf_log_mem_stop_flag != 1)
	{
		log_mem_data[log_mem_data_index].type = 0;
		log_mem_data[log_mem_data_index].alloc_time = sys_timestamp();
		log_mem_data[log_mem_data_index].alloc_task_id = pthread_self();
		pthread_getname((pthread_t)log_mem_data[log_mem_data_index].alloc_task_id,log_mem_data[log_mem_data_index].executing_thread_name);
		log_mem_data[log_mem_data_index].alloc_addr = (u64)addr;
		log_mem_data[log_mem_data_index].alloc_size = size;
		if(log_mem_data_index < (LOG_MEM_NUM-1))
			log_mem_data_index++;
		else
			log_mem_data_index=0;
	}
#endif
}
static void *free_log_hook(void *addr)
{
	if(zhyf_log_mem_stop_flag != 1)
	{
		log_mem_data[log_mem_data_index].type = 1;
		log_mem_data[log_mem_data_index].free_time = sys_timestamp();
		log_mem_data[log_mem_data_index].free_addr = (u64)addr;
		log_mem_data[log_mem_data_index].free_task_id = pthread_self();
		pthread_getname((pthread_t)log_mem_data[log_mem_data_index].free_task_id,log_mem_data[log_mem_data_index].executing_thread_name);
		if(log_mem_data_index < (LOG_MEM_NUM-1))
			log_mem_data_index++;
		else
			log_mem_data_index=0;
	}
}
		
void zhyf_stop_log_mem_task();
void zhyf_stop_log_mem_task_c();
void zhyf_stop_log_mem()
{
	pthread_t new_th,new_th1; 
	int ret;
	if(zhyf_log_mem_stop_flag == 0)
	{
		zhyf_log_mem_stop_flag=1;
//		pthread_create2(&new_th, "zhyf_stop_log_mem", 240, 0,0x10000, (void*)zhyf_stop_log_mem_task, 0);
		pthread_create2(&new_th1, "zhyf_stop_log_mem_c", 239, 0,0x10000, (void*)zhyf_stop_log_mem_task_c, 0);
		log_mem_data_index = 0;
//		ret = pthread_detach(new_th);
//		if(0 != ret)
//		{
//			printf("pthread_detach failed\n");
//			return;
//		}
		ret = pthread_detach(new_th1);
		if(0 != ret)
		{
			printf("pthread_detach failed\n");
			return;
		}
	}
}

void zhyf_stop_log_mem_task()
{
	int res1 = -1;
	FILE *fp = fopen("/log_mem.txt", "w+");
	
	if(fp == NULL)
	{
	printf("open log_mem.txt error\n");
	return;
	}
	
	int i = 0;
	for(i = 0 ;i < LOG_MEM_NUM; i++)
	{
		if(log_mem_data[i].type==0)
		{
		   fprintf(fp, "ALLOC:0x%lx [%10d]: %s %llu\n", 
				   log_mem_data[i].alloc_addr,
				   log_mem_data[i].alloc_size,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].alloc_time);
		}
		else if(log_mem_data[i].type==1)
		{
		   fprintf(fp, "%60s FREE :0x%lx     : %s %llu\n", 
				   "",
				   log_mem_data[i].free_addr,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].free_time);
		} 
		
		fflush(fp);
	}
	
	fprintf(fp,"\n"); 
	fflush(fp);
	
	fclose(fp);       
          
}

void zhyf_stop_log_mem_task_c()
{
	int res1 = -1;
	FILE *fp = fopen("/ffx1/log_mem_c.txt", "w+");
	
	if(fp == NULL)
	{
	printf("open log_mem.txt error\n");
	return;
	}
	
	int i = 0;
	for(i = 0 ;i < LOG_MEM_NUM; i++)
	{
		if(log_mem_data[i].type==0)
		{
		   fprintf(fp, "ALLOC:0x%lx [%10d]: %s %llu\n", 
				   log_mem_data[i].alloc_addr,
				   log_mem_data[i].alloc_size,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].alloc_time);
		}
		else if(log_mem_data[i].type==1)
		{
		   fprintf(fp, "%60s FREE :0x%lx     : %s %llu\n", 
				   "",
				   log_mem_data[i].free_addr,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].free_time);
		} 
		
		fflush(fp);
	}
	
	fprintf(fp,"\n"); 
	fflush(fp);
	
	fclose(fp);          
          
}
static char log_buffer[1024];
void zhyf_stop_log_mem_printk()
{
	zhyf_log_mem_stop_flag = 1;
	int i;
	for(i = 0 ;i < LOG_MEM_NUM; i++)
	{
		if(log_mem_data[i].type==0)
		{
			sprintf(log_buffer, "ALLOC:0x%lx [%10d]: %s %llu\n", 
				   log_mem_data[i].alloc_addr,
				   log_mem_data[i].alloc_size,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].alloc_time);
			printk("%s",log_buffer);
		}
		else if(log_mem_data[i].type==1)
		{
			sprintf(log_buffer, "%60s FREE :0x%lx     : %s %llu\n", 
				   "",
				   log_mem_data[i].free_addr,
				   log_mem_data[i].executing_thread_name,log_mem_data[i].free_time);
			printk("%s",log_buffer);
		} 
	}
}
