#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cpu.h>
#include <time.h>
#include <reworks/printk.h>
#include "mylog.h"

#define MYLOG_PRINT  printk
static int g_current_mylog_level = MYLOG_LEVEL_NONE; /*最低打印级别*/
static int g_mylog_switch = MYLOG_ON; /*总打印开关*/

static int mylog_module_switch[MYLOG_MODULE_END] = { /*模块打印开关*/
		[MYLOG_USER] = MYLOG_ON,
		[MYLOG_CACHE] = MYLOG_OFF,
		[MYLOG_CDMA] = MYLOG_OFF,
		[MYLOG_GDMA] = MYLOG_OFF,
		[MYLOG_CAN] = MYLOG_OFF,
		[MYLOG_UART] = MYLOG_OFF,
		[MYLOG_I2C] = MYLOG_OFF,
		[MYLOG_SPI] = MYLOG_OFF,
		[MYLOG_QSPI] = MYLOG_OFF,
		[MYLOG_NAND] = MYLOG_OFF,
		[MYLOG_LPC] = MYLOG_OFF,
		[MYLOG_GPIO] = MYLOG_OFF,
		[MYLOG_GMAC] = MYLOG_OFF,
		[MYLOG_SRIO] = MYLOG_OFF,
		[MYLOG_PCIE] = MYLOG_OFF,
		[MYLOG_TIMER] = MYLOG_OFF,
		[MYLOG_WATCHDOG] = MYLOG_OFF,
		[MYLOG_RTC] = MYLOG_OFF,
		[MYLOG_SDIO] = MYLOG_OFF,
};


extern int cpu_id_get();

/*禁止在ISR中使用本接口*/
void mylog(int mod, int level, const char *func, int line, const char *fmt, ...)
{
	if (g_mylog_switch == MYLOG_OFF)
		return;
	
	if ((level < MYLOG_LEVEL_END) && (level == MYLOG_LEVEL_NONE))
		return;
	
	if ((mod < MYLOG_MODULE_END) && (mylog_module_switch[mod] == MYLOG_OFF))
		return;
	
    if (level >= g_current_mylog_level) 
    {
    	unsigned long long usec = sys_timestamp() * 1000000 / sys_timestamp_freq();
    	int cur_cpu_id = cpu_id_get();
		char msg_buf[20*1024] = {0};
		char opt[10] = {0};
		switch(level)
		{
			case MYLOG_LEVEL_DEBUG:
				strcpy(opt, "DEBUG");
				break;
			case MYLOG_LEVEL_INFO:
				strcpy(opt, "INFO");
				break;
			case MYLOG_LEVEL_WARN:
				strcpy(opt, "WARNNING");
				break;
			case MYLOG_LEVEL_ERROR:
				strcpy(opt, "ERROR");
				break;
			default:;
		}
		va_list ap;
		va_start(ap,fmt);                                                                     
		sprintf(msg_buf, "<**%s**> [%lluus][CPU%d][%s():_%d_]:: ", opt, usec, cur_cpu_id, func, line);
		vsprintf(msg_buf + strlen(msg_buf), fmt, ap);
		va_end(ap);
		MYLOG_PRINT("%s\n",msg_buf); /* 输出到标准输出 */
    }
}

/* 设置 模块打印开关和级别 */
void mylog_ctrl(int mod, int on1_or_off0, int level)
{
	if ((level >= MYLOG_LEVEL_END) || (mod >= MYLOG_MODULE_END))
		return;
	
//	g_mylog_switch = on1_or_off0;
	mylog_module_switch[mod] = on1_or_off0;
	
	if (on1_or_off0 == MYLOG_ON)
		g_current_mylog_level = level;
}
