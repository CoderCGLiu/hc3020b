#if 1
#include <malloc.h>
#include <string.h>
#include "hr3APIs.h"
#include <stdio.h>

#ifdef __REWORKS__
#include <taskLib.h>
#endif

#ifdef SYLIXOS
#include "taskLibCommon.h"
#include "logLib.h"
#include <spinlockLib.h>
#endif
#include <pthread.h>

typedef    void   *PVOID;


#define LW_NULL                         ((PVOID)0)                      /*  C   濠电偞鍨堕幐鎼侇敄閸パ�灁闁跨喓绁琔LL               */
#define ERROR_NONE    0

#define LW_SPINLOCK_TICKET_SHIFT    16
#define GCC_OFF_SMALL_ASM()     "R"
#define LW_ACCESS_ONCE(type, x)     (*(volatile type *)&(x))


typedef union {
    volatile UINT32 SLD_uiLock;

    struct {
#if LW_CFG_CPU_ENDIAN > 0
        UINT16      SLDQ_usTicket;
        UINT16      SLDQ_usSvcNow;
#else
        UINT16      SLDQ_usSvcNow;
        UINT16      SLDQ_usTicket;
#endif                                                                  /*  LW_CFG_CPU_ENDIAN           */
    } q;
#define SLD_usTicket    q.SLDQ_usTicket
#define SLD_usSvcNow    q.SLDQ_usSvcNow
} SPINLOCKTYPE;

typedef struct {

    SPINLOCKTYPE                SL_sltData;                             /*  闂備胶鍘ч〃銉︾濠靛柈娑㈠幢濞戞瑧鍙嗛梺璺ㄥ櫐閹凤拷                   */
#define SL_uiLock               SL_sltData.SLD_uiLock
#define SL_usTicket             SL_sltData.SLD_usTicket
#define SL_usSvcNow             SL_sltData.SLD_usSvcNow
    volatile struct __lw_cpu   *SL_pcpuOwner;                           /*  CPU 闂備礁鎼粔鏉懨洪埡鍛剨闁跨噦鎷�                 */

                                                              

    ULONG                       SL_ulCounter;                           /*  闂傚倷绀侀妵妯肩矆娓氾拷鍋╅柛鎾楀嫬顎涢梺鏂ユ櫅閸燁垰鈻撻崼鏇熺厱闁挎繂绻戠�锟�               */
    PVOID                       SL_pvReserved;                          /*  濠电儑绲藉ú锔炬崲閸曨垰姹查柨鐕傛嫹                      */
} __spinlock_t;

typedef struct __spinlockTask_t {
    __spinlock_t  lock;
    int         flags;
} __spinlockTask_t;

typedef struct __spinlock_list_t __spinlock_list_t;
struct __spinlock_list_t {
    __spinlockTask_t *lock;
    __spinlock_list_t *next;
};

extern UINT64 bslGetTimeUsec(void);
extern unsigned char dead_flag;

static int test(int iCaseNum);

#if 0
pthread_mutex_init(&(pagelocks_[i]), NULL);
pthread_mutex_lock(&(pagelocks_[i]));
#endif

/* SpinLockInit, START */
static VOID  __archSpinInit (__spinlock_t  *psl)
{
    psl->SL_sltData.SLD_uiLock = 0;                                     /*  0: 闂備礁鎼悧婊勭椤忓牆缁╅柨婵嗘祫閹烽攱鎱ㄥΟ鍝勮埞缁楁垿姊虹拠鈥冲⒕闁瑰嚖鎷�: 闂傚倷绀侀妵妯肩矆娓氾拷鍋╁Δ锝呭暞閸嬫劙鏌ら崫銉殰闁瑰嚖鎷�*/
    psl->SL_pcpuOwner          = LW_NULL;
    psl->SL_ulCounter          = 0;
    psl->SL_pvReserved         = LW_NULL;
    __asm__ __volatile__ ("sync" : : : "memory");
}

static VOID  __SmpSpinInit (__spinlock_t *psl)
{
    __archSpinInit(psl);
    __asm__ __volatile__ ("sync" : : : "memory");
}

static int  __pthread_spin_init (__spinlock_t  *pspinlock, int  pshare)
{
    if (pspinlock == LW_NULL) {
        errno = EINVAL;
        return  (EINVAL);
    }

    __SmpSpinInit(pspinlock);

    return  (ERROR_NONE);
}

static void __spinLockTaskInit (__spinlockTask_t * lock, int flags)
{
    if (lock) {
        __pthread_spin_init(&lock->lock, 1);
        lock->flags = flags;
    }
}

void * SpinLockInit()
{
    __spinlockTask_t * pLock;
    pLock = (__spinlockTask_t *)malloc(sizeof(__spinlockTask_t));
    if(pLock == NULL) return NULL;
    __spinLockTaskInit (pLock, 0); 
    return (void *)pLock;
}
/* SpinLockInit, END */

/* SpinLockTake, START */

static VOID  __mipsSpinLock (SPINLOCKTYPE *psld, VOIDFUNCPTR  pfuncPoll, PVOID  pvArg)
{
    UINT32          uiNewVal;
    UINT32          uiInc = 1 << LW_SPINLOCK_TICKET_SHIFT;
    SPINLOCKTYPE    sldVal;

    __asm__ __volatile__(
        "   .set push                                   \n"
        "   .set noreorder                              \n"
        "1:                                             \n"
        "   ll      %[oldvalue], %[slock]               \n"
        "   addu    %[newvalue], %[oldvalue], %[inc]    \n"
        "   sc      %[newvalue], %[slock]               \n"
        /* FIXME, Add by zhangdx, START, STEP 3 */
            "sync\n\t"
        /* FIXME, Add by zhangdx, END, STEP 3 */

        "   beqz    %[newvalue], 1b                     \n"
        "   nop                                         \n"
        "   .set pop"
        : [slock] "+" GCC_OFF_SMALL_ASM() (psld->SLD_uiLock),
          [oldvalue] "=&r" (sldVal),
          [newvalue] "=&r" (uiNewVal)
        : [inc] "r" (uiInc));

    while (sldVal.SLD_usTicket != sldVal.SLD_usSvcNow) {
        if (pfuncPoll) {
            pfuncPoll(pvArg);
        }
        sldVal.SLD_usSvcNow = LW_ACCESS_ONCE(UINT16, psld->SLD_usSvcNow);
    }
}

static int  __archSpinLockRaw (__spinlock_t  *psl)
{

    __mipsSpinLock(&psl->SL_sltData, LW_NULL, LW_NULL);

    return  (1);                                                        /*  闂備礁鎲″缁樼箾閿熻棄缁╅柨婵嗩樆缁狅綁鏌熼柇锕�妞ゎ剨鎷�                  */
}

static VOID  __SmpSpinLockTask (__spinlock_t *psl)
{
    __archSpinLockRaw(psl);                                          /*  濠碉紕鍋熼幊鎾崇暦椤掍焦鍙忛柍鍝勫�閳瑰秵绻濋棃娑冲姛婵℃彃鐗撳鐑樺緞閸濄儺妲婚梺浼欑稻瀹曟绮诲☉銏犲窛婵烇綆鍓ㄩ幏鐑芥⒑閻熸壆鎽犻柛鏃�礃閹便劑鏁撻敓锟�    */
    __asm__ __volatile__ ("sync" : : : "memory");
                                                                 /*  !LW_CFG_MIPS_CPU_LOONGSON2K */
}

static int  __pthread_spin_lock (__spinlock_t  *pspinlock)
{
    if (pspinlock == LW_NULL) {
        errno = EINVAL;
        return  (EINVAL);
    }

    __SmpSpinLockTask(pspinlock);

    return  (ERROR_NONE);                                               /*  婵犳鍠楃换鎰緤娴犲鍋夐柨鐔烘媴ock                   */
}

static void __spinLockTaskTake (__spinlockTask_t * lock)
{
    if (lock) {
        __pthread_spin_lock(&lock->lock);
    }
}

void SpinLockTake(void * lock_id)
{
    if(lock_id == NULL) return;
    __spinLockTaskTake((__spinlockTask_t *)lock_id);
}
/* SpinLockTake, END */

/* SpinLockGive, START */
static VOID  __mipsSpinUnlock (SPINLOCKTYPE *psld)
{
    UINT  uiSvcNow = psld->SLD_usSvcNow + 1;

    __asm__ __volatile__ ("sync" : : : "memory");
    psld->SLD_usSvcNow = (UINT16)uiSvcNow;

    __asm__ __volatile__ ("sync" : : : "memory");
}

static int  __archSpinUnlockRaw (__spinlock_t  *psl)
{
    __mipsSpinUnlock(&psl->SL_sltData);                                   /*  闂佽崵鍠愰悷杈╃礊娓氾拷缁╅柨鐕傛嫹                      */

    return  (1);
}

static int  __SmpSpinUnlockTask (__spinlock_t *psl)
{
    /* FIXME, Add by Zhangdx, START, STEP 0 */
    asm volatile(
            ".set push\n\t"
            ".set mips64r2\n\t"
            "sync\n\t"
            "synci 0($0)\n\t"
            ".set pop\n\t"
            );
    /* FIXME, Add by Zhangdx, END, STEP 0 */

    __asm__ __volatile__ ("sync" : : : "memory");
    __archSpinUnlockRaw(psl);

    /* FIXME, Add by Zhangdx, START, STEP 0 */
    asm volatile(
            ".set push\n\t"
            ".set mips64r2\n\t"
            "sync\n\t"
            "synci 0($0)\n\t"
            ".set pop\n\t"
            );
    /* FIXME, Add by Zhangdx, END, STEP 0 */

        return  (ERROR_NONE);
}

static int  __pthread_spin_unlock (__spinlock_t  *pspinlock)
{
    if (pspinlock == LW_NULL) {
        errno = EINVAL;
        return  (EINVAL);
    }

    __SmpSpinUnlockTask(pspinlock);

    return  (ERROR_NONE);                                               /*  婵犳鍠楃换鎰緤娴犲鍋夐柨鐔烘儺nlock                 */
}

static void __spinLockTaskGive (__spinlockTask_t * lock)
{
    if (lock) {
        __pthread_spin_unlock(&lock->lock);
    }
}

void SpinLockGive(void * lock_id)
{
    if(lock_id == NULL) return;
    __spinLockTaskGive((__spinlockTask_t *)lock_id);
}
/* SpinLockGive, END */

int my_spinlock()   //int argc, char *argv[]
{
	int argc=6;
	char *argv[6];
	//argv[0]="1";
	argv[1]="1";
	argv[2]="0";
	argv[3]="1";
	argv[4]="2";
	argv[5]="3";
    int i = 0;
    char taskName[32];
    int taskNum = 0;
    int core[16];
//    TASK_ID ret;
    int flag = 0;
    if (argc > 1)
    {
        flag = atoi(argv[1]);
        if (flag == 0)
        {
            taskNum = atoi(argv[2]);
        }
        else
        {
            taskNum = argc - 2;
            for (i = 0; i < taskNum; ++i)
            {
                core[i] = atoi(argv[i + 2]);
            }

        }
    }

    //printf("<D>Current Mode %s, ",(flag==0)?"Task Not bind":"Task bind");
    if (flag == 0)
        logMsg("<D>Current Mode Task Not bind,\n", 0, 1, 2, 3, 4, 5);
    else
        logMsg("<D>Current Mode Task bind,\n", 0, 1, 2, 3, 4, 5);

    //printf("Task Num =%d ",taskNum);
    logMsg("Task Num =%d\n ", (long)taskNum, 1, 2, 3, 4, 5);
    if(1==flag)
    {
        for(i=0;i<taskNum;i++)
        {
//            printf("[%d] ",core[i]);
            logMsg("[%d] \n", (long)core[i], 1, 2, 3, 4, 5);
        }
    }
//    printf("\n");
   


    if (flag == 0)
    {
//    	 printf("in_test\n");
        for (i = 0; i < taskNum; i++)  
        {
            sleep(i+2);
            sprintf(taskName, "LOAD_TASK_%d\n", i);
            taskSpawn(taskName, 200, VX_FP_TASK, 0x2000, (FUNCPTR)test, i, 0, 0, 0, 0, 0, 0, 0, 0, 0);   //0x2000
        }
    }
    else
    {
        for (i = 0; i < taskNum; i++)
        {
            sleep(i+2);
            sprintf(taskName, "LOAD_TASK_%d\n", i);
            bslTaskCreateCore(taskName, 200, VX_FP_TASK, 0x20000,(FUNCPTR)test, core[i], core[i],1,2,3,4,5,6,7,8,9);
        }
    }
    if(dead_flag == 0)
	{
        while(1) sleep(1);
	}
    return 0;
}

// As load_prj10, use malloc create a spinlock list.
int test(int coreid)
{
    unsigned long long i = 0;
    int cnt = 0;
    size_t nbyte = 0;
    UINT64 stime = 0, etime = 0;
    double average_time = 0;

    __spinlock_list_t *head = NULL;
    __spinlock_list_t *cur = NULL;
    __spinlock_list_t *next = NULL;

#define LOCKS_BASE_ADDR (0xb000000020000000ULL)


   // unsigned long long num_locks = 0xffFFFFFFFF;
    unsigned long long num_locks = 0x000000FFFF;

    
//    printf("in_test2\n");
 //   /* Step 1, core down */
    for(i=0;i<num_locks;i++) //num_locks
    {
        next = (__spinlock_list_t *)malloc(sizeof(__spinlock_list_t));

        if (cur != NULL) {
            cur->next = next;
        }

        if (next == NULL) {
            num_locks = i;
            break;
        }

        next->lock = (void *)SpinLockInit();
        next->next = NULL;
        if (head == NULL) {
            head = next;
        }
        cur = next;
    }
    //printf("in_test3\n");

    //bspDebugMsg("over!\r\n");
    printf("core[%d] spin lock inited [%llu:0x%llx], start test\n", coreid, num_locks, num_locks);

    sleep(2);
    for (;;) {
//        taskCpuLock();
         stime = bslGetTimeUsec();

         cur = head;
         while (cur->next != NULL) {
             SpinLockTake(cur->lock);
             cur = cur->next;
         }

         cur = head;
         while (cur->next != NULL) {
             SpinLockGive(cur->lock);
             cur = cur->next;
         }

         etime = bslGetTimeUsec();

         if(coreid == 0)
         {
             bslLEDToggle();
         }

#define LOG_LOOPS 1
        if (cnt == LOG_LOOPS)
        {
            average_time = etime - stime;
            printf("SpinLockTest core[%d] time : %f us\n", coreid, average_time);
            if(coreid == 0)
            {
                bslLEDToggle();
            }
			if(dead_flag == 1) 
                sleep(2);
        }

        ++cnt;
        if (cnt > LOG_LOOPS)
        {
            cnt = 0;
        }
//        taskCpuUnlock();
    }


    return 0;
}
#endif

