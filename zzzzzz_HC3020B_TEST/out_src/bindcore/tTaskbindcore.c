/*!************************************************************
 * @file: tTaskbindcore.c
 * @brief: 该文件演示了reworks下如何绑定特定的核运行task的方法,每个核绑定不同的任务和多个任务绑定一个核运行
 * @author: 
 * @date: 30,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/30 |   | 创建
 * 
 **************************************************************/
/*
 * 头文件
 * */
#include <stdlib.h>
#include <vxWorks.h>
#include <taskLib.h>
#include <stdio.h>
#include <reworks/types.h> 
#include <pthread.h>
//#include "hr3APIs.h"

// hr3APIs.h 178
extern unsigned int bslCoreGetId(void);
// hr3APIs.h 167
extern unsigned int bslProcGetId(void);
// hr3APIs.h 156
extern TASK_ID bslTaskCreateCore(char * name, int priority,int options,int stackSize,FUNCPTR entryPT,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9);

extern unsigned int sleep(unsigned int seconds);


void task_0(void)
{
	int i = 3;
	while(i--)
	{
		sleep(1);
		printf("[CORE%d]:Running Task0!.\n",bslCoreGetId());
	}
}


void task_1(void)
{
	int i = 3;
	while(i--)
	{
		sleep(1);
		printf("[CORE%d]:Running Task1!.\n",bslCoreGetId());
	}
}


void task_2(void)
{
	int i = 3;
	while(i--)
	{
		sleep(1);
		printf("[CORE%d]:Running Task2!.\n",bslCoreGetId());
	}
}


void task_3(void)
{
	int i = 3; 
	while(i--)
	{
		sleep(1);
		printf("[CORE%d]:Running Task3!.\n",bslCoreGetId());
	}
}


int task_bind_core_demo(void) 
{
	TASK_ID id[4];
	
	printf("[DSP%d]:Starting Bind Task!.\n",bslProcGetId());
	
	
	id[0] = bslTaskCreateCore("task0", 50, 0, 0x10000, (void *)task_0, 0,0,1,2,3,4,5,6,7,8,9);
	if(id[0] == -1)
	{
		printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
	}
	id[1] = bslTaskCreateCore("task1", 50, 0, 0x10000, (void *)task_1, 1,0,1,2,3,4,5,6,7,8,9);
	if(id[1] == -1)
	{
		printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
	}
	id[2] = bslTaskCreateCore("task2", 50, 0, 0x10000, (void *)task_2, 2,0,1,2,3,4,5,6,7,8,9);
	if(id[2] == -1)
	{
		printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
	}
	id[3] = bslTaskCreateCore("task3", 50, 0, 0x10000, (void *)task_3, 3,0,1,2,3,4,5,6,7,8,9);
	if(id[3] == -1)
	{
		printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId()); 
	}
	
	printf("[DSP%d]:Bind Task Success, Watching Task Running!.\n",bslProcGetId());
	printf("waiting excute over...\n\r");
	sleep(5);
	
	return 0;
}


int task_bind_core_demo2(void)
{
	int i = 0;
	TASK_ID id[4];
	
	printf("[DSP%d]:Starting Bind 4 Tasks On One Core!.\n",bslProcGetId());

	for(i = 0; i < 4; i++)
	{
		id[0] = bslTaskCreateCore("task0", 50, 0, 0x10000, (void *)task_0, i,0,1,2,3,4,5,6,7,8,9);
		if(id[0] == -1)
		{
			printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
		}
	
		id[1] = bslTaskCreateCore("task1", 50, 0, 0x10000, (void *)task_1, i,0,1,2,3,4,5,6,7,8,9);
		if(id[1] == -1)
		{
			printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
		}
		id[2] = bslTaskCreateCore("task2", 50, 0, 0x10000, (void *)task_2, i,0,1,2,3,4,5,6,7,8,9);
		if(id[2] == -1)
		{
			printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId());
		}
		id[3] = bslTaskCreateCore("task3", 50, 0, 0x10000, (void *)task_3, i,0,1,2,3,4,5,6,7,8,9);
		if(id[3] == -1)
		{
			printf("[DSP%d]:Bind Task Err!.\n",bslProcGetId()); 
		}
		printf("waiting excute over...\n\r");
		sleep(5);
	}
	
	return 0;
}


int task_bind_core_test(void)
{
	task_bind_core_demo();
	
	task_bind_core_demo2();
	
	return 0;
}


