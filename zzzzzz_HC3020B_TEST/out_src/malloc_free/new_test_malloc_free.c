#include <stdlib.h>
#include <string.h>
#include <vxWorks.h>
#include <taskLib.h>
#include <stdio.h>
#include "hr3SysTools.h"
#include "hr3APIs.h"

#define CORENUM 8

void ddrEccInfo2()
{
    #define DDR0_CTL_BASE  0x900000001f010000
    int eccerrcnt = *(unsigned int *)(DDR0_CTL_BASE + 0x80);
    int *eccclr = (unsigned int *)(DDR0_CTL_BASE + 0x7c);
    int eccstat = *(unsigned int *)(DDR0_CTL_BASE + 0x78);

    printf("<D>ECC report:\n");
    printf("<D>state: uncorrected_err = 0x%x\n", (eccstat >> 16) & 0xf);
    printf("<D>state: corrected_err = 0x%x\n", (eccstat >> 8) & 0xf);
    printf("\n");
    printf("<D>state: corrected_bit_num = 0x%x\n", (eccstat >> 0) & 0xf);
    printf("<D>cnt: uncorrected_err_cnt = 0x%x\n", (eccerrcnt >> 16) & 0xffff);
    printf("<D>cnt: corrected_err_cnt = 0x%x\n", (eccerrcnt) & 0xffff);
    printf("\n");
    *eccclr = 0xf;
    eccerrcnt = *(unsigned int *)(DDR0_CTL_BASE + 0x80);
    printf("<D>after clr, error cnt is 0x%x\n", eccerrcnt);
}

void tstMallocFreeFunc2(int coreid)
{
            int len = 32*1024;
            unsigned int BindDestoeyCnt = 0;
            float* src1 = NULL;
            int*    src2 = NULL;
            short*src3  = NULL;
            void *data_a1;
            void *data_a2;
            void *data_a3;
            void *data_a4;
            void *data_a5;

            void *data_b1;
            void *data_b2;
            void *data_b3;
            void *data_b4;
            void *data_b5;

            void *data_c1;
            void *data_c2;
            void *data_c3;
            void *data_c4;
            void *data_c5;

            void *block_a1;
            void *block_a2;
            void *block_a3;
            void *block_a4;
            void *block_a5;

            void *block_b1;
            void *block_b2;
            void *block_b3;
            void *block_b4;
            void *block_b5;

            void *block_c1;
            void *block_c2;
            void *block_c3;
            void *block_c4;
            void *block_c5;

            void *view_a1;
            void *view_a2;
            void *view_a3;
            void *view_a4;
            void *view_a5;

            void *view_b1;
            void *view_b2;
            void *view_b3;
            void *view_b4;
            void *view_b5;

            void *view_c1;
            void *view_c2;
            void *view_c3;
            void *view_c4;
            void *view_c5;


            printf("<D>Malloc Free test core%d\n",coreid);

            while(1)
            {
//                    if(dead_flag == 0)
//                    {
//                        src1 = (float *)memalign(32,0x1000000);
//                        src2 = (int*)memalign(32,0x1000000);
//                        src3 =  (short*)memalign(32,0x1000000);
//                    }


                    *(unsigned int *)(0xffffffffafb00500 + coreid*4) = __LINE__;

//                    data_a1 = (void *)src1;
//                    data_a2 = (void *)(src1+0x10000);
//                    data_a3 = (void *)(src1+0x20000);
//                    data_a4 = (void *)(src1+0x30000);
//                    data_a5 = (void *)(src1+0x40000);
//
//
//                    data_b1 = (void *)src2;
//                    data_b2 = (void *)(src2+0x10000);
//                    data_b3 = (void *)(src2+0x20000);
//                    data_b4 = (void *)(src2+0x30000);
//                    data_b5 = (void *)(src2+0x40000);
//
//                    data_c1 = (void *)src3;
//                    data_c2 = (void *)(src3+0x10000);
//                    data_c3 = (void *)(src3+0x20000);
//                    data_c4 = (void *)(src3+0x30000);
//                    data_c5 = (void *)(src3+0x40000);

                    block_a1 =malloc(48+40*2);
                    block_a2 =malloc(48+40*2);
                    block_a3 =malloc(48+40*2);
                    block_a4 =malloc(48+40*2);
                    block_a5 =malloc(48+40*2);


                    block_b1 =malloc(48+40*2);
                    block_b2 =malloc(48+40*2);
                    block_b3 =malloc(48+40*2);
                    block_b4 =malloc(48+40*2);
                    block_b5 =malloc(48+40*2);


                    block_c1 =malloc(48+40*2);
                    block_c2 =malloc(48+40*2);
                    block_c3 =malloc(48+40*2);
                    block_c4 =malloc(48+40*2);
                    block_c5 =malloc(48+40*2);

                    view_a1=malloc(24);
                    view_a2=malloc(24);
                    view_a3=malloc(24);
                    view_a4=malloc(24);
                    view_a5=malloc(24);

                    view_b1=malloc(24);
                    view_b2=malloc(24);
                    view_b3=malloc(24);
                    view_b4=malloc(24);
                    view_b5=malloc(24);


                    view_c1=malloc(24);
                    view_c2=malloc(24);
                    view_c3=malloc(24);
                    view_c4=malloc(24);
                    view_c5=malloc(24);


#if 0
                    *(float *)src1 = 0.12345;
                    *(int *)src2 = 100;
                    *(short *)src3 = 100;

                    *(int *)block_a1 = 0x1234;
                    *(int *)block_a2 = 0x1234;
                    *(int *)block_a3 = 0x1234;
                    *(int *)block_a4 = 0x1234;
                    *(int *)block_a5 = 0x1234;

                    *(int *)block_b1 = 0x1234;
                    *(int *)block_b2 = 0x1234;
                    *(int *)block_b3 = 0x1234;
                    *(int *)block_b4 = 0x1234;
                    *(int *)block_b5 = 0x1234;

                    *(int *)block_c1 = 0x1234;
                    *(int *)block_c2 = 0x1234;
                    *(int *)block_c3 = 0x1234;
                    *(int *)block_c4 = 0x1234;
                    *(int *)block_c5 = 0x1234;

                    *(int *)view_a1 = 0x1234;
                    *(int *)view_a1 = 0x1234;
                    *(int *)view_a1 = 0x1234;
                    *(int *)view_a1 = 0x1234;
                    *(int *)view_a1 = 0x1234;


                    *(int *)view_b1 = 0x1234;
                    *(int *)view_b1 = 0x1234;
                    *(int *)view_b1 = 0x1234;
                    *(int *)view_b1 = 0x1234;
                    *(int *)view_b1 = 0x1234;


                    *(int *)view_c1 = 0x1234;
                    *(int *)view_c1 = 0x1234;
                    *(int *)view_c1 = 0x1234;
                    *(int *)view_c1 = 0x1234;
                    *(int *)view_c1 = 0x1234;
#endif


                    free(view_a1);
                    view_a1 =NULL;
                    free(view_a2);
                    view_a2 =NULL;
                    free(view_a3);
                    view_a3 =NULL;
                    free(view_a4);
                    view_a4 =NULL;
                    free(view_a5);
                    view_a5 =NULL;

                    free(view_b1);
                    view_b1 =NULL;
                    free(view_b2);
                    view_b2 =NULL;
                    free(view_b3);
                    view_b3 =NULL;
                    free(view_b4);
                    view_b4 =NULL;
                    free(view_b5);
                    view_b5 =NULL;

                    free(view_c1);
                    view_c1 =NULL;
                    free(view_c2);
                    view_c2 =NULL;
                    free(view_c3);
                    view_c3 =NULL;
                    free(view_c4);
                    view_c4 =NULL;
                    free(view_c5);
                    view_c5 =NULL;
                    *(unsigned int *)(0xffffffffafb00500 + coreid*4) = __LINE__;



                    free(block_a1);
                    block_a1 =NULL;
                    free(block_a2);
                    block_a2 =NULL;
                    free(block_a3);
                    block_a3 =NULL;
                    free(block_a4);
                    block_a4 =NULL;
                    free(block_a5);
                    block_a5 =NULL;

                    free(block_b1);
                    block_b1 =NULL;
                    free(block_b2);
                    block_b2 =NULL;
                    free(block_b3);
                    block_b3 =NULL;
                    free(block_b4);
                    block_b4 =NULL;
                    free(block_b5);
                    block_b5 =NULL;

                    free(block_c1);
                    block_c1 =NULL;
                    free(block_c2);
                    block_c2 =NULL;
                    free(block_c3);
                    block_c3 =NULL;
                    free(block_c4);
                    block_c4 =NULL;
                    free(block_c5);
                    block_c5 =NULL;

//                    if(dead_flag == 0)
//                    {
//                        free(src1);
//                        free(src2);
//                        free(src3);
//                    }
                    *(unsigned int *)(0xffffffffafb00500 + coreid*4) = __LINE__;


                    BindDestoeyCnt ++;
                    if((BindDestoeyCnt%3000 == 0))
                    {
                        printf("[core-%d]MallocFreeCnt = %u\n",bslCoreGetId(), BindDestoeyCnt);
                        if(coreid == 0)
                        {
//                            bslLEDToggle();
//                            ddrEccInfo2();
                        }
//                        if(dead_flag == 1)
//                        {
                            sleep(2);
//                        }
                    }

            }
}

void test_malloc_free(void)
{
	int i,res;
	
    for(i=0;i<CORENUM;i++)
    {
            res = bslTaskCreateCore("tstMallocFreeTask", 200, 0,0x20000,(FUNCPTR)tstMallocFreeFunc2, i,0,1,2,3,4,5,6,7,8,9);
            if(res == -1)
            {
                logMsg(" Create tstBindDestoryFunc ERROR!\n",0,1,2,3,4,5);
                return;
            }
            else
            {
            	printf("Create Test Task Success!\n\r");
            }
    }
	return;
}
