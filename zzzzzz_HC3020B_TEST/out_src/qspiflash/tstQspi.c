#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "../hr3_demo_cfg.h"


#ifndef OK
#define OK 0
#endif

#ifndef ERROR
#define ERROR -1
#endif

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(u8 *pAddr, u32 length)
{
    int index = 0;

    printf("pAddr: 0x%08lX, length: %lu = 0x%08lX", (u32)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(u8 *)(pAddr + index));
    }

    printf("\n");
}

extern int QspiFlashErase(unsigned int addr, unsigned int len);
extern int QspiFlashRead(unsigned int addr, unsigned char* rbuf, unsigned int len);
extern int QspiFlashWrite(unsigned int addr, unsigned char* wbuf, unsigned int len);
extern unsigned int QspiFlashReadid(void);
extern int QspiFlashEraseAll(void);

// Qspi Flash 功能测试
int tstQspiflashFunc()
{
	unsigned char * pBuffer_w = NULL;
	unsigned char * pBuffer_r = NULL;
	unsigned char * pBackUp = NULL;
    unsigned int i = 0;
    int err_count = 0;
    unsigned int test_start_addr = QSPI_TEST_START;
    unsigned int test_len = QSPI_TEST_SIZE;
    
    pBuffer_r = malloc(test_len);
    if(pBuffer_r == NULL)
    {
        printf("memory malloc error.\n");
        return ERROR;
    }
    memset((void *)pBuffer_r, 0, test_len);
    
    pBuffer_w = malloc(test_len);
    if(pBuffer_w == NULL)
    {
        printf("memory malloc error.\n");
        return ERROR;
    }
    memset((void *)pBuffer_w, 0, test_len);
    
    pBackUp = malloc(test_len);
    if(pBackUp == NULL)
    {
        printf("memory malloc error.\n");
        return ERROR;
    }
    memset((void *)pBackUp, 0, test_len);
    


    printf("备份QSPI Flash原始数据\n");
    QspiFlashRead(test_start_addr, pBackUp, test_len);
    printf("QSPI Flash已备份原始数据\n");

    printf("开始擦除qspi flash (测试0x%x bytes).\n",test_len);
    QspiFlashErase(test_start_addr, test_len);
    printf("擦除qspi flash完成.\n");

    printf("读取并校验擦除后qspi flash 内容:\n");
    memset((void *)pBuffer_r, 0, test_len);
    memset((void *)pBuffer_w, 0xff, test_len);
    QspiFlashRead(test_start_addr, pBuffer_r, test_len);
    err_count = CheckUint8(pBuffer_w, pBuffer_r, test_len);
    if(err_count == 0)
    	printf("QSPI Flash擦除测试通过\n");
    else
    	printf("QSPI Flash擦除测试不通过, err_count = 0x%x\n",err_count);

    /*准备测试数据*/
    for(i = 0; i < test_len; i++)
    {
    	*(pBuffer_w + i) = i + 0x12;
    }
    err_count = 0;
    printf("开始写qspi flash (测试0x%x bytes).\n",test_len);
    printf("    向qspi flash空间写入长度为0x%x bytes的递增数.\n",test_len);
    printf("    并读取qspi flash空间进行检验\n");
    QspiFlashWrite(test_start_addr, pBuffer_w, test_len);
    memset((void *)pBuffer_r, 0, test_len);
    QspiFlashRead(test_start_addr, pBuffer_r, test_len);
    err_count = CheckUint8(pBuffer_w, pBuffer_r, test_len);
    if(err_count == 0)
    	printf("QSPI Flash写测试通过\n");
    else
    	printf("QSPI Flash写测试不通过, err_count = 0x%x\n",err_count);
    
#if 0
    err_count = 0;
    printf("恢复并校验QSPI Flash原始数据\n");
    QspiFlashErase(test_start_addr, test_len);
    QspiFlashWrite(test_start_addr, pBackUp, test_len);
    memset((void *)pBuffer_r, 0, test_len);
    QspiFlashRead(test_start_addr, pBuffer_r, test_len);
    err_count = CheckUint8(pBuffer_w, pBuffer_r, test_len);
    if(err_count == 0)
    	printf("QSPI Flash已恢复原始数据\n");
    else
    	printf("QSPI Flash恢复原始数据校验出错, err_count = 0x%x\n",err_count);
#endif
 
    free(pBuffer_r);
    pBuffer_r = NULL;
    free(pBuffer_w);
    pBuffer_w = NULL;
    free(pBackUp);
    pBackUp = NULL;

    return OK;
}

/*
 * 功能： 测试qspi flash的写、读性能。
 */
int tstQspiflashPerf()
{
    unsigned long long t1 = 0;
    unsigned long long t2 = 0;
    unsigned long long total = 0;
    struct timespec ts;
    int tstNum,i,j;
    double result = 0;
    unsigned char * pBuffer = NULL;
    double time = 0;
    unsigned int test_start_addr = QSPI_TEST_START;
    unsigned int test_len = QSPI_TEST_SIZE;
    
    pBuffer = malloc(test_len);
    if(pBuffer == NULL)
    {
        printf("memory malloc error.\n");
        return ERROR;
    }
    memset((void *)pBuffer, 0, test_len);
    
    /*准备测试数据*/
    for(i = 0; i < test_len; i++)
    {
    	*(pBuffer + i) = i + 0x12;
    }
    

    tstNum = 5;
    printf("测试qspi flash的读写性能\n");
    printf("测试包大小0x%x bytes,测试次数:%d\n",test_len,tstNum);

    for(i=0; i<tstNum; i++)
    {
    	QspiFlashErase(test_start_addr, test_len);

#ifdef _1HR3_
		t1 = usr_get_time(&ts);
#else
		t1 = bslGetTimeUsec();
#endif
		QspiFlashWrite(test_start_addr, pBuffer, test_len);
#ifdef _1HR3_
		t2 = usr_get_time(&ts);
#else
		t2 = bslGetTimeUsec();
#endif
        total += (t2 - t1); 
    }

    result = (double)(1000000 * tstNum * 1.0 * test_len) / total / 1024;
    printf("    qspi flash 写速度 = %.1lf KB/s \n",result);
    t1 = t2 = 0;

    sleep(1);
    total = 0;
    for(i=0; i<tstNum; i++) 
    {
#ifdef __4HR3_VPXOLD__
        t1 = usr_get_time(&ts); 
#else		
        t1 = bslGetTimeUsec();
#endif
//        QspiFlashRead(test_start_addr, pBuffer, test_len); 
//        memcpy(pBuffer, 0xffffffffb6000000+test_start_addr, test_len);
//		for(j=0; j<test_len; j++)/*ldf 20230628 add:: 从QSPI boot地址读取,一次1字节*/
//		{
//			*((unsigned char*)(pBuffer + j)) = *((unsigned char*)(0xffffffffb6000000+test_start_addr+j));
//		}
        for(j=0; j<test_len; j+=4)/*ldf 20230628 add:: 从QSPI boot地址读取,一次4字节*/
        {
        	*((unsigned int*)(pBuffer + j)) = *((unsigned int*)(0xffffffffb6000000+test_start_addr+j));
        }
#ifdef __4HR3_VPXOLD__
        t2 = usr_get_time(&ts);
#else		
        t2 = bslGetTimeUsec();
#endif
        total += (t2 - t1);
    }
    result = (double)(1000000 * tstNum * 1.0 * test_len) / total / 1024 / 1024;
    printf("    qspi flash 读速度 = %.1lf MB/s \n",result);
    t1 = t2 = 0;

    free(pBuffer);
    pBuffer = NULL;
    return OK;
}

/*
 * 功能：获取Qspiflash容量(MB)
 * 输入：
 *      无
 * 输出：
 *      无
 * 返回值：
 *      无
 */
void getFlashVolQspiFlash()
{
	unsigned int tmp;
	
	tmp = QspiFlashReadid();
	printf("    Qspi  Flash id：0x%x\n", tmp);//0x1019bb20
	
	tmp = tmp >> 16;
	
    if(tmp == 0x17)
        printf("    Qspi  Flash 容量：64Mbit\n");
    else if(tmp == 0x18)
        printf("    Qspi  Flash 容量：128Mbit\n");
    else if(tmp == 0x19)
        printf("    Qspi  Flash 容量：256Mbit\n");
    else if(tmp == 0x20)
        printf("    Qspi  Flash 容量：512Mbit\n");
    else if(tmp == 0x21)
        printf("    Qspi  Flash 容量：1Gbit\n");
    else if(tmp == 0x22)
        printf("    Qspi  Flash 容量：2Gbit\n");
    else
        printf("    Qspi  Flash 容量：无法识别\n");
    return;

}
