/*!************************************************************
 * @file: ddr.c
 * @brief: 该文件为hr3模块核内DDR的测试例子
 * @author: 
 * @date: 15,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/15 |  | 创建
 * 
 **************************************************************/

/*
 * 头文件
 * */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <taskLib.h>
//#include "hr3APIs.h"
#include "../hr3_demo_cfg.h"

/*
 * 宏定义
 * */

#define TEST_LOOP 10
#define TEST_MEM_SIZE	(2*1024*1024)

/*
 * 外部函数声明
 * */

//extern double sys_get_runtime(u64 presys_time, int useval);
//extern void set_testcase_res(int core_id, char* res);
//extern void data_statistic(int* array, int num, int* max_v, int* min_v, float* avr_v);

/*
 * 函数声明
 * */

// DDR空间读写校验测试
void tstHR3DDRTest();
// memcpy测试
void tstMemcpy(void * dstAddr,void * srcAddr,unsigned int bytes);

/*
 * 函数实现
 * */

/*
 * 功能：测试内存空间读写功能。
 * 参数：无。
 * 返回值：无。
 */
void tstHR3DDRTest()
{
    unsigned int i = 0;
    unsigned int errNum;
    unsigned long addr = 0x0;
    unsigned long size = 0x0;
    static int errPrintCnt = 16;

    
    //DDR0 Read and Write
#if defined(DDR0_1G_TEST) || defined(DDR0_2G_TEST) || defined(DDR0_4G_TEST)
    addr = DDR0_VIRTUAL_ADDR;
    printf("DDR0 空间读写测试\n");
#ifdef DDR0_1G_TEST
    errNum = 0;
    size = 0x70000000UL;//DDR_SIZE_1G; //1GB空间
    printf("    开始写DDR0地址0x%lx，向DDR0空间填入自增整数，1,2,3 ... 0x10000000，共1GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");

    printf("    读取DDR0地址0x%lx数据进行校验\n",addr);
    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
         {
             errNum = errNum + 1;
         }
    }


    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR0 1GB 空间读写测试通过\n");
    else
        printf("DDR0 1GB 空间读写测试失败\n"); 
#endif
    
#ifdef DDR0_2G_TEST
    errNum = 0;
    size = DDR_SIZE_2G; //2GB空间
    printf("    开始写DDR0地址0x%lx，向DDR0空间填入自增整数，1,2,3 ... 0x20000000，共2GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");

    printf("    读取DDR0地址0x%lx数据进行校验\n",addr);
    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
             errNum = errNum + 1;
    }


    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR0 2GB 空间读写测试通过\n");
    else
        printf("DDR0 2GB 空间读写测试失败\n"); 
#endif
    
#ifdef DDR0_4G_TEST
    errNum = 0;
    size = DDR_SIZE_4G; //4GB空间
    printf("    开始写DDR0地址0x%lx，向DDR0空间填入自增整数，1,2,3 ... 0x40000000，共4GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");

    printf("    读取DDR0地址0x%lx数据进行校验\n",addr);
    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
             errNum = errNum + 1;
    }


    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR0 4GB 空间读写测试通过\n");
    else
        printf("DDR0 4GB 空间读写测试失败\n"); 
#endif

#endif
    
    //DDR1 Read and Write
#if defined(DDR1_1G_TEST) || defined(DDR1_2G_TEST) || defined(DDR1_4G_TEST)
    addr = DDR1_VIRTUAL_ADDR;
    printf("DDR1 空间读写测试\n");
#ifdef DDR1_1G_TEST
    errNum = 0;
    size = DDR_SIZE_1G; //1GB空间
    
    printf("    开始写DDR1地址0x%lx, 向DDR空间填入递增整数，1,2,3 ... 0x10000000，共1GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");
    printf("    读取DDR1地址0x%lx处数据进行校验\n",addr);

    errPrintCnt = 16;
    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
         {
             errNum = errNum + 1;
             if(errPrintCnt > 0)
             {
            	 errPrintCnt--;
            	 printf("<ERROR> 数据校验错误: [0x%lx]=0x%x, except=0x%x\n",(addr + i*4), (*(unsigned int *)(addr + i*4)), i+1);
             }
         }
    }

    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR1 1GB 空间读写测试通过\n");
    else
        printf("DDR1 1GB 空间读写测试失败\n");
#endif
    
#ifdef DDR1_2G_TEST
    errNum = 0;
    size = DDR_SIZE_2G; //2GB空间

    
    printf("    开始写DDR1地址0x%lx, 向DDR空间填入递增整数，1,2,3 ... 0x20000000，共2GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");
    printf("    读取DDR1地址0x%lx处数据进行校验\n",addr);

    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
             errNum = errNum + 1;
    }

    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR1 2GB 空间读写测试通过\n");
    else
        printf("DDR1 2GB 空间读写测试失败\n");
#endif
    
#ifdef DDR1_4G_TEST
    errNum = 0;
    size = DDR_SIZE_4G; //4GB空间

    
    printf("    开始写DDR1地址0x%lx, 向DDR空间填入递增整数，1,2,3 ... 0x40000000，共4GB空间\n",addr);
    for(i = 0; i < size/4; i++)
    {
          *(unsigned int *)(addr + i*4) = i+1;
    }
    printf("    数据写入完成\n");
    printf("    读取DDR1地址0x%lx处数据进行校验\n",addr);

    for(i = 0; i < size/4; i++)
    {
         if(*(unsigned int *)(addr + i*4) != i+1)
             errNum = errNum + 1;
    }

    printf("    数据校验完成\n");
    if(errNum == 0)
        printf("DDR1 4GB 空间读写测试通过\n");
    else
        printf("DDR1 4GB 空间读写测试失败\n");
#endif
    
#endif
}

/*
 * 功能：测试内存拷贝功能和性能。
 * 参数：
 *   dstAddr，	目的地址；
 *   srcAddr，	源地址；
 *   bytes，		拷贝的字节数。
 * 返回值：无。
 */
void tstMemcpy(void * dstAddr,void * srcAddr,unsigned int bytes)
{
	u64 t1,t2 = 0;
	t1 = t2;
	double diff_time = 0.0;
	int status = 0;
	double res = 0.0;
	
	printf("内存拷贝测试...\n");
	
	t1 = bslGetTimeUsec();
	status = memcpy((void *)dstAddr,(void *)srcAddr,bytes);
	t2 = bslGetTimeUsec();
	if(status != NULL)
		printf("内存拷贝功能正常\n");
	else
		printf("内存拷贝功能错误\n");  
	
//	diff_time = ((t2 - t1)*1000.0*1000.0)/ sys_timestamp_freq();
	diff_time = t2 - t1;
	
	status = memcmp((void *)dstAddr,(void *)srcAddr,bytes);
	if(status == NULL)
		printf("数据校验正常\n");
	else
		printf("数据校验错误\n");
	
//	printf("t1 = %u,t2 = %u\n",t1,t2);
//	res = (float)bytes * sysClkRateGet()/(t2 - t1)/1024/1024;
	res = (double)(bytes * 1.0 * 1000000)/diff_time/1024/1024;
	printf("内存拷贝性能：%.2f MB/s\n",res);
}


/*
 * 未使用
 * 屏蔽于2023/05/16
 */
//void test_mem_cpy()
//{
//	u64 t1,t2 = 0;
//	int i, tick, min, max, avr;
//	double diff_time = 0.0;
//
//	int* res = (int*)malloc(sizeof(int)*TEST_LOOP);
//
//	unsigned char* src = memalign(128, TEST_MEM_SIZE);
//	unsigned char* dst = memalign(128, TEST_MEM_SIZE);
//	printf("memcpy test start!\n\r");
//	// 循环测试
//	for (i = 0; i < TEST_LOOP;)
//	{
//		// 获取当前的tick
//		tick = tick_get();
//		
//		// 设置初始值
//		memset(src, i+1, TEST_MEM_SIZE);
//		memset(dst, 0, TEST_MEM_SIZE);	 
//
//		// 开始时间和结束时间
//		t1 = bslGetTimeUsec();
//		memcpy(dst, src, TEST_MEM_SIZE);
//		t2 = bslGetTimeUsec();
//		diff_time = t2 - t1;
//		
//		res[i] = diff_time;
//		
//		// 检测结果的有效性
//		if (memcmp(src, dst, TEST_MEM_SIZE) != 0)
//		{
//
//			free(res);
//			return;
//		}
//
//		// 只有在未发生调度时数据有效
////		if (tick == tick_get())
////		{
//			i++;
////		}
//	}
//
//	data_statistic(res, TEST_LOOP, &max, &min, &avr); 	
//
//
//	printf("memcpy runtime: max %ld, min %ld, avr %ld", max, min, avr); 
//	
//	printf("内存拷贝性能：%.2f MB/s\n",(((double)TEST_MEM_SIZE/1024/1024/(avr/(1000.0*1000.0)))));
//	
//	free(res);
//
//}




