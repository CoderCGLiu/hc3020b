
#ifndef SRC_HR3_GVAR_H_
#define SRC_HR3_GVAR_H_

#include "hr3Macro.h"

#define A 0
#define B 1
#define C 2
#define D 3

#define K32 (32 * 1024)
#define B512 (512)

typedef          int                BOOL;                               /*  布尔变量定义                */

typedef          void              *PVOID;                              /*  void * 类型                 */
typedef const    void              *CPVOID;                             /*  const void  *               */

typedef          char               CHAR;                               /*  8 位字符变量                */
typedef unsigned char               UCHAR;                              /*  8 位无符号字符变量          */
typedef unsigned char              *PUCHAR;                             /*  8 位无符号字符变量指针      */
typedef          char              *PCHAR;                              /*  8 位字符指针变量            */
typedef const    char              *CPCHAR;                             /*  const char  *               */

//enum SRIOTYPE;
enum SRIOTYPE{SRIO0 = 0, SRIO1};
extern u32 cpuID;

extern unsigned long long usr_get_time(struct timespec *ts);


extern void tstCDMAtest(void);
extern void tstCDMAperf(void);

extern int tstNandflashErase(void);
extern int tstNandflashFunc(void);
extern int tstNandflashPerf(void);



extern void srioInit(void);
extern void srioTestFun(u8 controller, u8 type);

extern void tstGDMAtest(void);
extern void tstGDMAperf(void);
























#endif /* SRC_HR3_GVAR_H_ */

