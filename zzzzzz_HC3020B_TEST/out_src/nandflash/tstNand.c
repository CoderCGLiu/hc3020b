/*!************************************************************
 * @file: tstNand.c
 * @brief: 该文件为hr3模块nand flash的测试例子，功能和性能测试
 * @author: 
 * @date: 18,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/18 | | 创建
 * 
 **************************************************************/
/*
 * 头文件
 * */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
//#include "hr3APIs.h"
//#include "hr3SysTools.h"
#include <time.h>

/*
 * 宏定义
 * */
#define FILE_PATH	"/ffx0/file"
#define BUFFER_LEN	0x1000000

#ifndef OK
#define OK 0
#endif

#ifndef ERROR
#define ERROR -1
#endif

/*
 * 别名
 * */
// hr3NandFlash.c 35
typedef unsigned long CardAddress;
// vxMacro.h 153
typedef int BOOL;
 
/*
 * 外部函数声明
 * */
extern int nandIsBadBlock(u32 block_num);
extern int nandflashPageRead(CardAddress, int,unsigned int*,BOOL);
extern int nandflashPageWrite(CardAddress, int,unsigned int*,BOOL);
extern int nandflashBlockErase(int,int);

extern int flash_pageio_init(void);
extern int flash_pageio_read(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);
extern int flash_pageio_write(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);
extern int flash_pageio_erase(u32 block_num);



/*
 * 全局变量
 * */
unsigned int bad_block[1000] = {0};
unsigned int bad_block_num = 0;


int tstNandflashErase(void)
{
    int i = 0;
    unsigned int intnum = 512;
    unsigned int * buffer = NULL;

    char tmp[2048];

    buffer = (unsigned int *)malloc(intnum*4);
    if(buffer == NULL)
    {
        printf("malloc mem error.\n");
        return ERROR;
    }
    memset((void *)tmp,0xff,2048);

    printf("测试 nandflash 擦除功能.\n");

    printf("    擦除1024个Blocks(共128MB)\n");
    nandflashBlockErase(0,1024);   /* 擦除1K个block(128KB)*/
    for(i = 0; i < 64*1024; i ++)
    {
        nandflashPageRead(i*(2048+64),512,buffer,true);
        if(memcmp(buffer,tmp,2048) != 0)
        {
            printf("    nandflash 擦除功能错误.\n");
            return ERROR;
        }
    }
    printf("    nandflash 擦除功能正常.\n\n");

    return OK;
}


int nadBadBlockTest()
{
	unsigned int i = 0;
	unsigned err_count = 0;
	float block_val = 0;
	for(i = 0; i < 4096; i++)
	{
		if(nandIsBadBlock(i) == 1)
		{
			err_count++;
			printf("bad block:%d.\n", i);
		}
	}
	if(err_count == 0)
	{
		printf("nandflash无坏块.\n");
	}
	else
	{
		block_val = (float)err_count/4096;
		
		if(block_val>0.02)
		{
			printf("errblocks=%d, err比例为 %.5f，nandflash异常.\n", err_count, block_val);
			return ERROR;
		}
		printf("errblocks=%d, err比例为 %.5f,在合理范围0.02内.\n", err_count, block_val);
	}
	return OK;
}
void nandBadBlock(int firstErasableBlock,int numOfErasableBlocks)
{
	unsigned int i = 0;
	unsigned err_count = 0;
	unsigned int num = firstErasableBlock + numOfErasableBlocks;
	float block_val = 0;
	for(i = firstErasableBlock; i < num; i++)
	{
		if(nandIsBadBlock(i) == 1)
		{
			bad_block[err_count++] = i;
//			printf("bad block:%d.\n", i);
		}
	}
	bad_block_num = err_count;
}

// Nandflash功能测试
int tstNandflashFunc()
{
    int i = 0;
    int j = 0;
    int num = 0;
    int skip_num = 0;
    int ret = 0;
    unsigned int intnum = 512;
    unsigned int * buffer = NULL;
    unsigned int * bufferTest = NULL;
    unsigned int wrVal = 0;
    char tmp[2048];
    
    printf("坏块测试begin: \n");
    ret = nadBadBlockTest();
    if(ret == ERROR)
    {
    	printf("坏块超出合理范围，退出nandflash功能测试.\n");
    	return ERROR;
    }
    printf("坏块测试end, 继续执行nandflash功能测试.\n\n");

    nandBadBlock(0, 1024);
    buffer = (unsigned int *)malloc(intnum*4);
    if(buffer == NULL)
    {
        printf("malloc mem error.\n");
        return ERROR;
    }
    bufferTest = (unsigned int *)malloc(intnum*4);
    if(bufferTest == NULL)
    {
        printf("malloc mem error.\n");
        return ERROR;
    }

    memset((void *)tmp,0xff,2048);

    
    printf("读取nandflash page空间（2KB）原来内容.\n");
    nandflashPageRead(0, 512, bufferTest,false);
    for (i = 0; i < 128; i = i + 4)
    {
        printf("val: 0x%8x\n", *(unsigned int *)(bufferTest + i));
    }

    printf("测试 nandflash 擦除功能.\n");

    printf("    擦除1024个Blocks(共128MB)\n");
    nandflashBlockErase(0,1024);   /* 擦除1K个block(128KB)*/
    for(i = 0; i < 64*1024; i++)
    {
		if((i>=(bad_block[num])*64) && (i<(bad_block[num]+1)*64) && (bad_block_num > 0))
		{
			skip_num++;
			if(skip_num == 64)
			{
				num++;
				skip_num = 0;
			}
//			printf("擦除校验跳过该block %d, skip_num=%d.\n", bad_block[num],skip_num);
			continue;
		}
		nandflashPageRead(i*(2048+64),512,buffer,false);
		if(memcmp(buffer,tmp,2048) != 0)
		{
			printf("i=%d,block=%d.\n",i,i/64);
			printf("    nandflash 擦除功能错误.\n");
			return ERROR;
		}
    }
    printf("    nandflash 擦除功能正常.\n\n");

    printf("读取nandflash page空间(2KB)擦除后的内容.\n");
    nandflashPageRead(0, 512, bufferTest,false);
    for (i = 0; i < 128; i = i + 4)
    {
        printf("val: 0x%8x\n", *(unsigned int *)(bufferTest + i));
    }

    for(i = 0; i < intnum; i ++)
    {
        buffer[i] =  i;
    }

    printf("测试 nandflash 读写功能.\n");
    printf("    向nandflash page空间（2KB）填入自增数,1,2,3...512\n");
    num = 0;
    skip_num = 0;
    for (i = 0; i <  64*1024; i++)
    {
		if((i>=(bad_block[num])*64) && (i<(bad_block[num]+1)*64) && (bad_block_num > 0))
		{
			skip_num++;
			if(skip_num == 64)
			{
				num++;
				skip_num = 0;
			}
//			printf("flash写跳过该block %d, skip_num=%d.\n", bad_block[num],skip_num);
			continue;
		}
		nandflashPageWrite(i * (2048 + 64), 512, buffer,false);
    }

    printf("    读取nandflash page空间（2KB）的自增数进行校验.\n");
    num = 0;
    skip_num = 0;
    for (i = 0; i < 64*1024; i++)
    {
		if((i>=(bad_block[num])*64) && (i<(bad_block[num]+1)*64) && (bad_block_num > 0))
		{
			skip_num++;
			if(skip_num == 64)
			{
				num++;
				skip_num = 0;
			}
//			printf("flash读跳过该block %d, skip_num=%d.\n", bad_block[num],skip_num);
			continue;
		}
		nandflashPageRead(i * (2048 + 64), 512, bufferTest,false);
		if (memcmp(buffer, bufferTest, 2048) != 0)
		{
			printf("    nandflash 读写功能错误.\n\n");
			return ERROR;
		}
    }
    printf("    nandflash 读写功能正常.\n\n");

    printf("读取nandflash page空间(2KB)写入后的内容.\n");
    nandflashPageRead(0, 512, bufferTest,false);
    for (i = 0; i < 128; i = i + 4)
    {
        printf("val: 0x%8x\n", *(unsigned int *)(bufferTest + i));
    }

    //2019.12.19 add begin
    printf("    擦除8个Blocks(共1MB)\n");
    num = 0;
    skip_num = 0;
    nandflashBlockErase(0,8);   /* 擦除8个block(128KB)*/
    for(i = 0; i < 512; i ++)
    {
		if((i>=(bad_block[num])*64) && (i<(bad_block[num]+1)*64) && (bad_block_num > 0))
		{
			skip_num++;
			if(skip_num == 64)
			{
				num++;
				skip_num = 0;
			}
//			printf("擦除跳过该block %d, skip_num=%d.\n", bad_block[num],skip_num);
			continue;
		}
        nandflashPageRead(i*(2048+64),512,buffer,false);
        if(memcmp(buffer,tmp,2048) != 0)
        {
            printf("    nandflash 擦除功能错误.\n");
            return ERROR;
        }
    }
    printf("    nandflash 擦除功能正常.\n\n");

    free(buffer);
    buffer = NULL;

    free(bufferTest);
    bufferTest = NULL;

    return OK;
}

// Nandflash性能测试
int tstNandflashPerf()
{
    unsigned int i = 0, j =0;
    unsigned int intnum = 512;
    u64 t1 = 0;
    u64 t2 = 0;
    u64 total = 0;
    double time = 0;
    int tstNum;
    struct timespec ts;
    unsigned int * buffer = NULL;

    buffer = (unsigned int *)malloc(intnum*4);
    if(buffer == NULL)
    {
        printf("malloc mem error.\n");
        return ERROR;
    }
    for(i = 0; i < intnum; i ++)
    {
        buffer[i] = 0x12345000 + i;
    }


    //printf("测试 nandflash 擦除性能.\n");
//    t1 = sys_timestamp();
//    nandflashBlockErase(0,8);   /* 擦除8个block(1MB)*/
//    t2 = sys_timestamp();
//    printf("nandflash 擦除速度 = %.1f MB/s \n",(float)clkRateSet/(t2-t1));
    tstNum = 10;
    printf("测试 nandflash 读写性能.\n");
    printf("测试包大小1MB,测试次数:%d\n", tstNum);

    for(j=0; j<tstNum; j++)
    {
        nandflashBlockErase(0,8); 
#ifdef __4HR3_VPXOLD__
        t1 = usr_get_time(&ts);
#else		
        t1 = bslGetTimeUsec(); 
#endif
        for (i = 0; i < 512; i++)
        {
            nandflashPageWrite(i * (2048 + 64), 512, buffer,true); 
        }
#ifdef __4HR3_VPXOLD__
        t2 = usr_get_time(&ts);
#else		
        t2 = bslGetTimeUsec();
#endif
        total += (t2 -t1);
    }
//    time = (double)(total) / sys_timestamp_freq();
//    printf("    nandflash 写速度 = %.1f MB/s\n",(double)(tstNum)/(time));
    printf("    nandflash 写速度 = %.1f MB/s\n",(double)(tstNum * 1.0 * 1000000)/(total));
    t1 = t2 = 0;

    total = 0;
    for(j=0; j<tstNum; j++)
    {
#ifdef __4HR3_VPXOLD__
        t1 = usr_get_time(&ts);
#else		
        t1 = bslGetTimeUsec();
#endif
        for (i = 0; i < 512; i++)
        {
            nandflashPageRead(i * (2048 + 64), 512, buffer,true);
        }
#ifdef __4HR3_VPXOLD__
        t2 = usr_get_time(&ts);
#else		
        t2 = bslGetTimeUsec();
#endif
        total += (t2 -t1);
    }
//    time = (double)(total) / sys_timestamp_freq();
//    printf("    nandflash 读速度 = %.1f MB/s\n",(double)(tstNum)/(time));
    printf("    nandflash 读速度 = %.1f MB/s\n",(double)(tstNum * 1.0 * 1000000)/(total));
    t1 = t2 = 0;

    free(buffer);
    buffer = NULL;

    return OK;
}

/*******************************************************************************
 * 
 * 功能：
 *      打印文件数据接口。实现以十六进制方式打印读到的文件数据
 * 输入：
 *              buffer：数据缓冲区
 *              len：数据缓冲区内的数据量
 * 输出：
 *              无
 * 返回：
 *              无
 */
static void file_data_print(char *buffer, int len)
{
        int i;                                  /* 局部变量 */
        unsigned char *data;    /* 以无符号数打印 */
        
        data = (unsigned char *)buffer;
        
        /* 逐字节打印数据 */
        for (i = 0; i < len; i++)
        {
                printf("0x%x ", data[i]);
        }
        printf("\n");
        
        return;
}

