/*!************************************************************
 * @file: tSystemInfo.c
 * @brief: 该文件演示了用户接口中板级信息获取接口的功能和使用方法
 * @author: 
 * @date: 18,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/18 |   | 创建
 * 
 **************************************************************/
/*
 * 头文件
 * */
#include <stdlib.h>
#include <vxWorks.h>
#include <taskLib.h>
#include <stdio.h>
#include <reworks/types.h>
#include "hr3SysTools.h"
//#include "hr3APIs.h"
#include "../hr3_demo_cfg.h"

//hr2SysTool.h 272 ->
extern void sysLEDSetState(unsigned int state);
//
extern int sysClkRateGet(void);

extern u64 sys_timestamp();
extern u32 sys_timestamp_freq();

extern void udelay(u32);
extern int usleep(useconds_t useconds);
extern unsigned int sleep(unsigned int seconds);

/*LPC空间总大小为512KB，对LPC空间的用户空间进行读写测试，测试范围为末尾8KB大小
 * LPC基地址为0xFFE00000*/

#define LPC_BASE_ADDR	(0xFFE7D600)
#define LPC_SLOT_NUM	(0xFFE7D400)

extern int g_slotNum;

void show_demo_out_version()
{
	u32 _cpuID = 0;
	_cpuID = bslProcGetId();
	g_slotNum = bslGetSlotNum();
	printf("SlotNum = %d CpuID = %d\n",g_slotNum, _cpuID);
	printf("hr2_demo_out,version: V1.1.2\n");
	printf("date: %s, time: %s\n", __DATE__, __TIME__);
}

void show_software_prj(void)
{
#ifdef TEST_MODE	
	printf("gc4hr Software demo Prj:\n");
#elif (ALONE_MODE == 0)
	printf("DSP403 rede v6.1.0 gc4hr Software Prj:\n");
#else
	printf("gc4hr Software Prj litao:\n");
#endif
	printf("version: V1.1.13\n");
	printf("date: %s, time: %s\n", __DATE__, __TIME__);
}

int board_info_func(void) 
{
	u32 cab_num;
	u32 case_num;
	u32 slot_num;
	printf("[DSP%d]:Board Info Func Test Start!.\n",bslProcGetId());
	/*单板复位和整板复位接口，当前测试不测试该接口，需要与FPGA对接该功能是否实现*/
//	bslBoardReset(0);
	bslModuleGetInfo(&cab_num, &case_num, &slot_num);
	printf("[DSP%d]:槽位号: %d\n",bslProcGetId(),slot_num);
	printf("[DSP%d]:插箱号: %d\n",bslProcGetId(),case_num); 
	printf("[DSP%d]:机柜号: %d\n",bslProcGetId(),cab_num);
	printf("[DSP%d]:本节点ID号: %d\n",bslProcGetId(),bslProcGetId());
	printf("[DSP%d]:本模块槽位号: %d\n",bslProcGetId(),bslGetSlotNum()); 
	printf("[DSP%d]:Board Info Func Test Success!.\n",bslProcGetId());
	return 0;
}


void tstBHC()
{
    unsigned int tmp = 0;
    UINT  uiRegValue  = 0;

    printf("处理器信息：\n");
    tmp = *(unsigned int *)DSP_ID_REG;
    if(((tmp >> 16) == 0x8803) || ((tmp >> 16) == 0x0))
        printf("    DSP Type: 华睿3号 HC3080\n");
    else
        printf("    DSP Type: 不能识别(0x%x)\n", (tmp >> 16));

#if 1
    extern unsigned int  sysDspGetConfig (void);
    uiRegValue = sysDspGetConfig();
    printf("    DSP FREQ: %u MHz\n", uiRegValue);
#else
    uiRegValue = *(UINT *)DSP_FREQ_REG;
    switch (uiRegValue) {
    case 0x00014032:
        printf("    DSP FREQ: 208.33 MHz\n");
        break;

    case 0x0001203c:
        printf("    DSP FREQ: 500 MHz\n");
        break;

    case 0x00012048:
        printf("    DSP FREQ: 600 MHz\n");
        break;

    case 0x0001204e:
        printf("    DSP FREQ: 650 MHz\n");
        break;

    case 0x00012054:
        printf("    DSP FREQ: 700 MHz\n");
        break;

    case 0x0001205a:
        printf("    DSP FREQ: 750 MHz\n");
        break;

    case 0x00012060:
        printf("    DSP FREQ: 800 MHz\n");
        break;

    case 0x0001904c:
        printf("    DSP FREQ: 844.44 MHz\n");
        break;

    case 0x00011036:
        printf("    DSP FREQ: 900 MHz\n");
        break;

    case 0x00019056:
        printf("    DSP FREQ: 955.56 MHz\n");
        break;

    case 0x0001103c:
        printf("    DSP FREQ: 1000 MHz\n");
        break;

    case 0x00011040:
        printf("    DSP FREQ: 1066.67 MHz\n");
        break;

    case 0x00011042:
        printf("    DSP FREQ: 1100 MHz\n");
        break;

    case 0x00011046:
        printf("    DSP FREQ: 1166.67 MHz\n");
        break;

    case 0x00011048:
        printf("    DSP FREQ: 1199.88 MHz\n");
        break;

    default:
        printf("    DSP FREQ: 非法值！\n");
        break;
    }
#endif
    
    printf("内存信息：\n");
    uiRegValue = *(UINT *)DDR_FREQ_REG;
    switch (uiRegValue) {
    case 0x00024060:
        printf("    DDR FREQ: 800 MHz\n");
        break;

    case 0x00014040:
        printf("    DDR FREQ: 1066 MHz\n");
        break;

    case 0x00014048:
        printf("    DDR FREQ: 1200 MHz\n");
        break;

    case 0x00014050:
        printf("    DDR FREQ: 1333 MHz\n");
        break;

    case 0x00014060:
        printf("    DDR FREQ: 1600 MHz\n");
        break;

    case 0x00012038:
        printf("    DDR FREQ: 1867 MHz\n");
        break;

    case 0x00012040:
        printf("    DDR FREQ: 2133 MHz\n");
        break;

    default:
        printf("    DDR FREQ: 非法值！0x%x\n",uiRegValue);
        break;
    }
    
//    printf("    单通道DDR容量：4GB\n");
    printf("    DDR通道数：     2\n");
    printf("    板载总内存容量：8GB\n");

    printf("Flash信息：\n");
//    if(bslProcGetId() == 0)
//    {
    	getFlashVolNorFlash();
    	getFlashVolQspiFlash();
        printf("    Nand Flash 容量：4Gbit\n");
//    }else
//    {
//        printf("    DSP未挂接 FLash.\n");
//    }

    printf("Bsp version:\n");
    bslBspVersionRead();
    
    printf("Bootrom version:\n");
//    bslBootRomVersionRead();
    
    printf("Fpga version:\n");
//    bslFpgaVersionRead();
    
    printf("BMC version:\n");
//    bslBmcVersionRead();
    
    printf("image version:\n");
    show_software_prj();
    
    printf("demo_out version:\n");
    show_demo_out_version();
}


void tstModueInfo()
{
    printf("机箱号: %d\n",sysGetCaseNum());
    printf("槽位号: %d\n",sysGetSlotNum());
    printf("节点号: %d\n",bslProcGetId());
}


/*
 * 功能：LED功能测试(默认LED等闪烁3次)。
 * 参数：
 *   无。
 * 返回值：无。
 */
void tstLedFunc()
{
	unsigned int i = 0;
	
	for(i = 0; i < 3; i++)
	{
		sysLEDSetState(0);
		taskDelay(sysClkRateGet()/2);
		sysLEDSetState(1);
		taskDelay(sysClkRateGet()/2);
	}
}

void tstHR3LEDCtrl() 
{
    bslLEDSetState(0, 0);                                               /*  灯灭                        */
    sleep(2);
    bslLEDSetState(1, 0);                                               /*  灯亮                        */
    sleep(2);
    bslLEDSetState(2, 4);                                               /*  灯闪烁 (每秒闪 3 次)        */
    sleep(2);
    bslLEDToggle();
}

void tstHR3CPUReset()
{
    printf("未实现\n");
}

void tstHR3BoardReset()
{
    bslBoardReset(0);                                                   /*  模块复位                    */
    //bslBoardReset(1);                                                   /*  插箱复位                    */
}

#if 0/*LPC空间访问性能测试，读测试和写测试*/
int lpc_test(void)
{
	int i 		= 0;
	int j 		= 0;
	int count 	= 100;
	int var = 0;
	float perf = 0;
	unsigned long long time0;
	unsigned long long time1;
	unsigned long long sum;
	unsigned int addr = 0;
	
	printf("LPC空间功能测试，8K空间读写校验测试。。。\n\r");
	
	printf("开始写入递增数。。。\n\r");
	for(i = 0, j = 0; i < 0x2000; i+=4,j++)
	{
		j = j%0xff;
		bslLpcWriteByte(LPC_BASE_ADDR+i, j);
//		printf("val:%d\n\r", j);
	}
	printf("开始读取写入数据并校验。。。\n\r");
	for(i = 0; i < 0x2000; i+=4)
	{
		if(i == 0x500)//这里地址是Lock base的地址，所以跳过
		{
			continue;
		}
		addr = LPC_BASE_ADDR+i;
		//2022.07.07 FPGA会写gpio中断值，这块地址作为FPGA使用
		if((addr >= 0xffe7d610) && (addr <= 0xffe7d760))
		{
			continue;
		}
		var = bslLpcReadByte(LPC_BASE_ADDR+i);
//		printf("i:%d-val:%d\n\r",i,  var);
		if(var != (i/4)%255)
		{
			printf("i:%d-val:%d,addr=0x%x,写入值为%d\n\r",i,  var,LPC_BASE_ADDR+i,(i/4)%255);
			printf("LPC空间读取校验错误！\n\r");
//			return -1;
		}
	}
	printf("数据校验通过！\n\r");
	
	time0 = bslGetTimeUsec(); 
	for(i = 0; i < count; i++)
	{
		bslLpcReadByte(LPC_SLOT_NUM);
	}
	time1 = bslGetTimeUsec();
	
	sum = time1 - time0;
	
	perf = ((float)sum*1.0)/count;
	
    printf("LPC空间的读取响应时间为 %.1fus.\n",perf);
    
	time0 = bslGetTimeUsec();
	for(i = 0; i < count; i++)
	{
		bslLpcWriteByte(LPC_BASE_ADDR, 0x55);
	}
	time1 = bslGetTimeUsec(); 
	
	sum = time1 - time0;
	
	perf = ((float)sum*1.0)/count;
	
    printf("LPC空间的写入响应时间为 %.1fus.\n",perf);
	
	return 0;
}
#endif

int time_delay_test(void)
{
	float time = 0;
	unsigned long long time0;
	unsigned long long time1;
	unsigned long long time2;
	unsigned long long val;
	
	/*使用高精度时间戳进行时间接口测试*/
	/*udelay 1us*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	udelay(1);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("udelay 1us时间为 %.5fus.\n",time);
    
	/*udelay 50us*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	udelay(50);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("udelay 50us时间为 %.5fus.\n",time);
    
	/*udelay 500 us*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	udelay(500);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("udelay 500us时间为 %.5fus.\n",time);
    
    
	/*usleep*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	usleep(1);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("usleep 1us时间为 %.5fus.\n",time);
    
	/*usleep*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	usleep(100);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("usleep 100us时间为 %.5fus.\n",time);
    
	/*usleep*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	usleep(1000);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("usleep 1000us时间为 %.5fus.\n",time);
    
	/*sleep*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	sleep(1);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	time = (float)val;
	
    printf("sleep 1s时间为 %.5fus.\n",time);
    
	/*sleep*/
	time0 = bslGetTimeUsec();
	time1 = bslGetTimeUsec();
	sleep(5);
	time2 = bslGetTimeUsec();
	
	val = time2 - time1 - (time1 - time0);
	
	
	time = (float)val;
	
    printf("sleep 5s时间为 %.5fus.\n",time);
	
    printf("时间接口准确性测试结束！\n\r");
    
    return 0;
}

double sys_get_runtime(u64 presys_time, int useval)
{
	u64 postsys_time;
	double diff_time;
	
	postsys_time = bslGetTimeUsec();//sys_timestamp(); 
	
	
//	diff_time = ((postsys_time - presys_time))/ sys_timestamp_freq();
	diff_time = ((postsys_time - presys_time)*1.0)/ 1000000;
	
	
	return diff_time;
}




// 数据统计函数
void data_statistic(int* array, int num, int* max_v, int* min_v, int* avr_v)
{
	int i;
	int max = 0, min = array[0];	
	u64 sum = 0;

	// 求最大数、最小数、平均数、和
	for (i = 0; i < num; i++)
	{
		// 求最大值
		if (array[i] > max)
		{
			max = array[i];
		}
		
		// 求最小值
		if (array[i] < min)
		{
			min = array[i];
		}

		// 求和
		sum += array[i];
	}

	// 返回值
	*max_v = max;
	*min_v = min;
	*avr_v = sum / num;
}
