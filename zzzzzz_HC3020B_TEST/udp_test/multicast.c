/*******************************************************************************
*file:         multicast.c 
*description:  组播收发程序 ，测试速率和丢包率
*
*author:        CSIC715 信号处理事业部
*******************************************************************************/ 

#include <stdio.h>
#include <errno.h>
#include <reworks/printk.h>
#include <sys/socket.h>
#include <ip/sockLib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <clock.h>
#include <cpu.h>
#include <udelay.h>
#include <net/if.h>
//#include <sysinfo.h>
#include "multicast.h"


extern unsigned long long ldf_size;
extern unsigned long long ldf_blen;
extern unsigned long long ldf_udelay;
extern unsigned long long ldf_is_bind_nic;
extern char* ldf_send_nic_name;
extern char* ldf_send_ip;
extern char* ldf_recv_nic_name;
extern char* ldf_recv_ip;
extern int ldf_port;
extern char* ldf_multicast_ip;


#pragma DATA_ALIGN (g_cnt,128)
#pragma DATA_SECTION(g_cnt,".data")
u32 g_cnt;

extern u64 t_result1[], t_result2[], t_result3[], t_result4[], t_result5[], t_result6[];
extern u64 t_count;
extern u64 t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11;
extern u32 t_flag0, t_flag1;
extern u64 t_stat0, t_stat1, t_stat2, t_stat3, t_stat4;

#pragma DATA_SECTION (gbe_user_start, ".shm_unc");
u64 gbe_user_start = 0;

#pragma DATA_SECTION (gbe_user_end, ".shm_unc");
u64 gbe_user_end = 0;

#define DATA_RURNS_POS   4  //40000           /*发送数据设置掉头功能 */
     
#define UDP_TEST //单播

#ifdef UDP_TEST
#define UDP_ADDR				ldf_recv_ip//"192.168.100.13"
#else

//测试使用的相关参数
#define MULTICAST_ADDR			ldf_multicast_ip//"224.100.200.133"		//组播的IP地址
//#define MULTICAST_ADDR			"192.168.255.255"		//组播的IP地址
#endif

#define MULTICAST_PORT			ldf_port//12033               //13001			//组播端口号

#if 0
/* -1t: 50MBytes/s */
#define MULTICAST_PACKAGE_SIZE	(2264)	
#define SEND_TIME_US 	15

/* -0t:  59.4  MBytes/s */
#define MULTICAST_PACKAGE_SIZE	(4096)	
#define SEND_TIME_US 	30

/* -2t:  72.1  MBytes/s */
#define MULTICAST_PACKAGE_SIZE	(8192)	
#define SEND_TIME_US 	50

#endif  
//---------------------------20160103bsp------------------------------------
#if 0
/* -1t: 51.1 MB/s  */
#define MULTICAST_PACKAGE_SIZE	(2264)	
#define SEND_TIME_US 	16  
#endif

#if 1 
//21.6MB/s
//#define MULTICAST_PACKAGE_SIZE	(1344)	
//#define SEND_TIME_US 	45  


#define MULTICAST_PACKAGE_SIZE	ldf_size//(1*1024)//(16*1024)	//1024时45MB/s，8*1024时68MB/s，16*1024时65MB/s
//#define SEND_TIME_US 	1//udp5//multi1418MB/s
#define SEND_TIME_US 	ldf_udelay//0//udp5//multi1
#define	UDP_BUF_LEN		ldf_blen

//#define MULTICAST_PACKAGE_SIZE	(2264)	
//#define SEND_TIME_US 	5

/* -1t:   52.2 MB/s */
//#define MULTICAST_PACKAGE_SIZE	(1200)	
//#define SEND_TIME_US 	0   //52.2MB/s


/* -1t:   50.0 MB/s */
//#define MULTICAST_PACKAGE_SIZE	(1800)	
//#define SEND_TIME_US 	9

/* 与PC通讯（PC机组播收）  48.1 MB/s */
//#define MULTICAST_PACKAGE_SIZE	(1400)	
//#define SEND_TIME_US 	10

/* 与PC通讯（PC机组播收）  59.1 MB/s */
//#define MULTICAST_PACKAGE_SIZE	(1400)	
//#define SEND_TIME_US 	4

/* 与PC通讯（PC机组播收）  44.5 MB/s */
//#define MULTICAST_PACKAGE_SIZE	(1024)	
//#define SEND_TIME_US 	4
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  52.3MB/s  */
#define MULTICAST_PACKAGE_SIZE	(3072)	
#define SEND_TIME_US 	21
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  55.9 MB/s  */
#define MULTICAST_PACKAGE_SIZE	(4096)	
#define SEND_TIME_US 	35
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1, 51.9MB/s  */
#define MULTICAST_PACKAGE_SIZE	(4096)	
#define SEND_TIME_US 	40
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1, 58.5MB/s  */
#define MULTICAST_PACKAGE_SIZE	(6144)	
#define SEND_TIME_US 	50
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  62.7 MB/s  */
#define MULTICAST_PACKAGE_SIZE	(8192)	
#define SEND_TIME_US 	66
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  51.04MB/s  */
#define MULTICAST_PACKAGE_SIZE	(12*1024)	
#define SEND_TIME_US 	150
#endif


#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  51.4MB/s  */
#define MULTICAST_PACKAGE_SIZE	(16384)	
#define SEND_TIME_US 	200
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=1,  48.4MB/s  */
#define MULTICAST_PACKAGE_SIZE	(16384)	
#define SEND_TIME_US 	220
#endif

#if 0
/* BSP_ETH_INT_DELAY=5,BSP_ETH_INT_MODE=35 MB/s  */
#define MULTICAST_PACKAGE_SIZE	(32768)	
#define SEND_TIME_US 	700
#endif

//#define LEN 1024                /* int 型数组长度*/
#define LEN                     (MULTICAST_PACKAGE_SIZE/4) 

#define OUT_LOOP_COUNT_MCAST    90000  //9000
#define LOOP_COUNT_MCAST        10000

//#define OUT_LOOP_COUNT_MCAST    90000  //9000
//#define LOOP_COUNT_MCAST        1
  
#define SIZE_SND_BUF   UDP_BUF_LEN//(64*1024)//65535//(32*1024*1024)                   /*发送缓冲区大小*/
#define SIZE_RCV_BUF   UDP_BUF_LEN//(64*1024)//65535//(32 * 1024 * 1024)             /*接收缓冲区大小*/

#pragma DATA_ALIGN (g_mpkt_complete,8)
#pragma DATA_SECTION(g_mpkt_complete,".ddr_user");

#pragma DATA_ALIGN (g_mpkt_imcomplete,8)
#pragma DATA_SECTION(g_mpkt_imcomplete,".ddr_user");

#pragma DATA_ALIGN (g_mpkt_failed,8)
#pragma DATA_SECTION(g_mpkt_failed,".ddr_user");

//int g_mpkt_complete=0, g_mpkt_failed=0,g_mpkt_imcomplete=0;
unsigned int g_mpkt_complete, g_mpkt_failed,g_mpkt_imcomplete;


extern u64 sys_timestamp();
extern u32 sys_timestamp_freq(void);
/***************************************************************************/
//组播接收函数
/***************************************************************************/

u32 sw_count = 0;

void multicast_recv(netTstParam *netParam)
{
	float udpSpeed = 0.0;
	u32 recvBuf[LEN];
	SOCKET udpServer;
	SOCKADDR_IN recvAddr;
	SOCKADDR_IN clientAddr;
	char *mcastAddr = NULL;
	short localPort = 0;
	unsigned int recvSize = 0;
	int clientAddrSize = 0;
	int recvBytes = 0;
	unsigned int coreId = 0;
	unsigned int i,j;
    u64 t_start,t_end;
    float time_s;
    int blastNum=0;
    unsigned int pkt_dropped=0;
    unsigned int total_pkt_dropped=0;
 	float dropped_rate = 0.0;
 	unsigned int pkt_num_tmp = 0, pkt_count_loop = 0;
	
	coreId = cpu_id_get();
	g_mpkt_complete=0;
	g_mpkt_failed=0;
	g_mpkt_imcomplete=0;
	
	//接收输入参数
	localPort = netParam->lcPort;
	recvSize = netParam->netPkgSize;
	mcastAddr = netParam->dstAddr;
	
	//创建socket套接字
	udpServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(udpServer == 0)
	{
		printk("core_%d: udpServer create failed.\r\n",coreId);
		return;
	}	
	
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_recv_nic_name, IFNAMSIZ);
		if(setsockopt(udpServer, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("udp-recv NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
	
	// 设置远程服务器的信息
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_port = htons(localPort);
#ifdef UDP_TEST
	recvAddr.sin_addr.s_addr = htonl(INADDR_ANY);//inet_addr(ldf_recv_ip);
#else	/*ldf 20230824 add:: 多网卡udp组播接收，这里必须是"0.0.0.0"*/
	recvAddr.sin_addr.s_addr = inet_addr("0.0.0.0");
#endif
	if(bind(udpServer, (SOCKADDR *) &recvAddr, sizeof(recvAddr)) < 0)
	{
		printk("core_%d: udpServer bind localPort failed.\r\n",coreId);
		return;
	}
#ifdef UDP_TEST
    printk("core[%d]:udp recv start...\r\n", cpu_id_get());
#else
	
	//加入组播组
	struct ip_mreq mreq;
	mreq.imr_multiaddr.s_addr = inet_addr(mcastAddr);
#ifdef UDP_TEST
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);//inet_addr(ldf_recv_ip);
#else/*ldf 20230824 add:: 多网卡udp组播接收，这里必须填本地ip,不能用INADDR_ANY*/
	mreq.imr_interface.s_addr = inet_addr(ldf_recv_ip);
#endif
	if(setsockopt(udpServer, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq)) < 0)
	{
		printk("core_%d: udp recv add multiGroup setup failed.\r\n",coreId);
		return;
	}
	
	
	unsigned char mcast_loop=0;   /* 组播回环禁止*/
	if(setsockopt(udpServer,IPPROTO_IP,IP_MULTICAST_LOOP,(char *)&mcast_loop, sizeof(mcast_loop)) < 0)
	{
		printk("setsockopt (IP_MULTICAST_LOOP)  failed.\r\n");
		close(udpServer);
		return ;
	}
    printk("core[%d]:multicast recv start...\r\n", cpu_id_get());
#endif
    
    
//    printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	//设置接收缓冲区
	int sockRecvBufLen = SIZE_RCV_BUF;
	if(setsockopt(udpServer, SOL_SOCKET, SO_RCVBUF,(char *)&sockRecvBufLen, sizeof(sockRecvBufLen)) < 0)
	{
		printk("core_%d: udp recv buffer setup failed.\r\n",coreId);
		return;
	}			
	
	//开始接收数据
//	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
    
	clientAddrSize = sizeof(clientAddr);
//    for (i=0;i<OUT_LOOP_COUNT_MCAST;i++)
	i = 0;
	g_cnt = 0;
	while(1)
	{
//		printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
    	g_mpkt_complete=0;
    	g_mpkt_imcomplete=0;
    	//g_mpkt_failed=0;
    	pkt_dropped=0;
    	blastNum=0;
    	total_pkt_dropped=0;
    	
     	t_start=sys_timestamp();
    	for(j=0;j<LOOP_COUNT_MCAST;j++)
	    {
//    	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
    	   recvBytes = recvfrom(udpServer, recvBuf, recvSize, 0, (SOCKADDR *)&clientAddr, &clientAddrSize);
//    	   printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
    	   if (recvBytes < 0)
		   {
//	           printk ("blasteeUDP read error\r\n");
	           g_mpkt_failed++;
               //break;
	       }
           else
           {
        	   if(recvBytes==recvSize)
        		   g_mpkt_complete++;
        	   else
        		   g_mpkt_imcomplete++;
        	   
               blastNum += recvBytes;
               g_cnt++;
           }
#if 0
    	   if(recvBuf[0] != sw_count)
    	   { 
    		   printk("pkt[%d-%d] droped\n", sw_count, recvBuf[0]);
    		   udelay(30);
    		   extern void pktdma_rxch_pause();
    		   pktdma_rxch_pause();
    		   while(1)       //-------------debug found drop pkt
    		   {
    			   sysinfo_led_tgl(0);
    			   pthread_delay(15);
    		   }
    	   }	   
#endif
    	   
#if 1
     	   if(recvBuf[0] != sw_count)
    	   { 
    		   pkt_dropped=recvBuf[0]-sw_count;
//    		   printk("pkt[%d-%d] droped  %d pkt. \r\n", sw_count, recvBuf[0],pkt_dropped);
    		   total_pkt_dropped=total_pkt_dropped+pkt_dropped;
    		   
    		   sw_count=recvBuf[0];
     	   }	   
#endif    
     	   if(sw_count==0xffffffff)
     		   sw_count=0;
     	   else
    	       sw_count++;
 	    }  //for(j=0;j<LOOP_COUNT_MCAST;j++)
    	
    	t_end=sys_timestamp();    	
    	
    	time_s = (float)((double)(t_end-t_start) / (double)(sys_timestamp_freq() / 1000000));
    	
    	udpSpeed = ((float)(blastNum)) / time_s;	//单位：MB/S
    	
    	g_mpkt_failed = g_mpkt_failed+total_pkt_dropped;
    	
    	pkt_count_loop = recvBuf[0] - pkt_num_tmp;/*ldf 20230823 add*/
    	pkt_num_tmp = recvBuf[0];/*ldf 20230823 add*/
    	dropped_rate = (float)(((float)total_pkt_dropped/(float)pkt_count_loop)*100);/*ldf 20230823 add*/
    	
//    	printk ("%d: %fMB/s  %d  %d  dropped:%d\r\n", i,udpSpeed,\
//    			              g_mpkt_complete,g_mpkt_imcomplete,g_mpkt_failed);
    	printf ("%d: %fMB/s  %d  %d  dropped_rate:%.2f%%  total_pkt_dropped:%d\r\n", i,udpSpeed,\
    			              g_mpkt_complete,g_mpkt_imcomplete,dropped_rate,total_pkt_dropped);
    	
//    	printk ("%d: %dbytes/s  %d  %d  dropped:%d\r\n", i,(int)(blastNum/time_s),\
//    			              g_mpkt_complete,g_mpkt_imcomplete,g_mpkt_failed);
//    	printf ("%d: %dbytes/s  %d  %d  dropped:%d\r\n", i,(int)(blastNum/time_s),\
//    			              g_mpkt_complete,g_mpkt_imcomplete,g_mpkt_failed);
//    	printf ("%d: %s %dbytes/s  %d  %d  dropped:%d\r\n", i, MULTICAST_ADDR,(int)(blastNum/time_s),\
//    			              g_mpkt_complete,g_mpkt_imcomplete,g_mpkt_failed);
    	i++;    //while时
   	}	
#ifdef UDP_TEST
	
#else
	/*退出广播组*/
	if(setsockopt(udpServer,IPPROTO_IP,IP_DROP_MEMBERSHIP,(char *)&mreq, sizeof(mreq)) < 0)
	{
		printk("core_%d: udp recv drop multiGroup set failed.\r\n",coreId);
		close(udpServer);
		return;
	}
#endif
	close(udpServer);
	printk("multicast recv end.\r\n");
}
/***************************************************************************/
//组播发送函数
/***************************************************************************/
void multicast_send(netTstParam *netParam)
{
	u32 sendBuf[LEN/*16384*/];
	SOCKET udpClient;
	SOCKADDR_IN serverAddr;
	SOCKADDR_IN sendAddr;
	char *remoteIpAddr = NULL;
	short remotePort = 0, localPort = 0;
	unsigned int sendSize = 0;
	int sendBytes = 0;
	unsigned int coreId = 0;
	unsigned int i,j,ii;
	int pkt_complete=0, pkt_failed=0,pkt_imcomplete=0;
	
	coreId = cpu_id_get();
	
	//接收输入参数
	remoteIpAddr = netParam->dstAddr;
	remotePort = netParam->dstPort;
	sendSize = netParam->netPkgSize;
	localPort = netParam->lcPort;
	
	//初始化发送数据
	for(i = 0; i < LEN; i++)
	{
		sendBuf[i] = 0x12340000+i;	
	}
	
//	*(unsigned char *)(sendBuf) = 0x55;
	
	//创建socket套接字
	udpClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(udpClient </*==*/ 0)
	{
		printk("udpSender create failed!\r\n");
		return;
	}	
	
	
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_send_nic_name, IFNAMSIZ);
		if(setsockopt(udpClient, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("udp-send NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
	
	//设置套接字的选项(设置发送缓冲区，通过实验可以看出这个设置对于接收速度的影响非常大)
	int sockSendBufLen = SIZE_SND_BUF;
	if(setsockopt(udpClient, SOL_SOCKET, SO_SNDBUF,(char *)&sockSendBufLen, sizeof(sockSendBufLen)) < 0)
	{
		printk("core_%d: udp send buffer set failed!\r\n",coreId);
		return;
	}

#ifdef UDP_TEST
	printk("udp send start...\r\n");
#else
	
	/*ldf 20230824 add:: 此处指定组播数据的出口网卡，如果不设置则会根据路由表指定默认路由出口*/
	struct in_addr inaddr = {0};
	inaddr.s_addr = inet_addr(ldf_send_ip);
	if(setsockopt(udpClient, IPPROTO_IP, IP_MULTICAST_IF, (char *)&inaddr, sizeof(inaddr)) < 0)
	{
		printk("core_%d: udp-send NIC bind failed.\r\n",coreId);
		return;
	}
	
	/*ldf 20230824 add:: 加入组播组*/
//	struct ip_mreq mreq;
//	mreq.imr_multiaddr.s_addr = inet_addr(remoteIpAddr);
//	mreq.imr_interface.s_addr = inet_addr(ldf_send_ip);
//	if(setsockopt(udpClient, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq)) < 0)
//	{
//		printk("core_%d: udp-send add multiGroup setup failed.\r\n",coreId);
//		return;
//	}
	
	unsigned char mcast_loop=0;   /* 组播回环禁止*/
	if(setsockopt(udpClient,IPPROTO_IP,IP_MULTICAST_LOOP,(char *)&mcast_loop, sizeof(mcast_loop)) < 0)
	{
		printk("setsockopt (IP_MULTICAST_LOOP)  failed.\r\n");
		close(udpClient);
		return;
	}
	
	
	int ip_ttl_default=64;
	int ip_ttl_len=sizeof(int);
	unsigned char ip_mcast_ttl_default=64;
	int ip_mcast_ttl_len=sizeof(unsigned char);
	getsockopt(udpClient, IPPROTO_IP, IP_TTL,(char *)&ip_ttl_default,&ip_ttl_len);
	getsockopt(udpClient, IPPROTO_IP, IP_MULTICAST_TTL,(char *)&ip_mcast_ttl_default, &ip_mcast_ttl_len);
	printk("default: ip_ttl_default=%d ip_mcast_ttl_defaul=%d\r\n",ip_ttl_default,ip_mcast_ttl_default);
	/* ip_ttl_default=64 ip_mcast_ttl_defaul=1   */
	
	unsigned char ttlVal = 64;
	
	if(setsockopt(udpClient, IPPROTO_IP, IP_MULTICAST_TTL,(char *)&ttlVal, sizeof(ttlVal)) < 0)
	{
		//DEBUG_PRINT("socketOpt_set: udp send buffer set failed!\r\n");
		return -1;
	}
	printk("multicast send start...\r\n");
#endif
    
    
	// 设置远程服务器的信息
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(remotePort);
	serverAddr.sin_addr.s_addr = inet_addr(remoteIpAddr);
	
    i = 0;
//    while(i<10)
    while(1)
	{
    	pkt_complete=0;
    	pkt_imcomplete=0;
    	pkt_failed=0;
    	
    	for(j=0; j < LOOP_COUNT_MCAST; j++)
    	{	
    		sendBuf[0] = (i * LOOP_COUNT_MCAST + j);
    	
      		sendBytes = sendto(udpClient, (char *)sendBuf, sendSize, 0, (SOCKADDR *)&serverAddr, sizeof(serverAddr));  	
//      		printf("<**DEBUG**> [%s():_%d_]:: sendBytes = %d\n", __FUNCTION__, __LINE__,sendBytes);
            if(sendBytes==sendSize)
            	pkt_complete++;
            else
            {	
            	 if (sendBytes < 0)
            		 pkt_failed++; 
            	 else
            		 pkt_imcomplete++; 
            }
            udelay(SEND_TIME_US); 
            
     	}   //for(j=0; j < LOOP_COUNT_MCAST; j++)
    	      
//    	printk("%d:pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", i, pkt_complete,pkt_imcomplete,pkt_failed);
    	printf("%d:pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", i, pkt_complete,pkt_imcomplete,pkt_failed);
//    	udelay(500);//sleep(1);//组播不加的话影响性能
//    	udelay(100000);//danbo 1->4
//    	udelay(200000);//sleep(1);//danbo不加的话影响性能1->2
     	i++;
   	}// while(1)   //for (i=0;i<OUT_LOOP_COUNT_MCAST;i++)
    	
	close(udpClient);
	printk("multicast send end.\r\n");
	return ;
}

void *mcSend_task(void *args)	//用于运行组播发送功能的任务
{
	netTstParam sendParam;
	
#ifdef	UDP_TEST
	//设置发送参数开始发送
	sendParam.dstAddr = UDP_ADDR;//sendto
	sendParam.dstPort = MULTICAST_PORT;
	sendParam.lcPort = MULTICAST_PORT;		
	sendParam.netPkgSize = MULTICAST_PACKAGE_SIZE;
#else
	//设置发送参数开始发送
	sendParam.dstAddr = MULTICAST_ADDR;//sendto
	sendParam.dstPort = MULTICAST_PORT;
	sendParam.lcPort = MULTICAST_PORT;		
	sendParam.netPkgSize = MULTICAST_PACKAGE_SIZE;
#endif
	multicast_send(&sendParam);

	pthread_exit(0);
	return NULL;
}

void *mcRecv_task(void *args)	//用于运行组播接收功能的任务
{
	netTstParam recvParam;
#ifdef	UDP_TEST
	recvParam.dstAddr = UDP_ADDR;	
	recvParam.dstPort = MULTICAST_PORT;	
	recvParam.lcPort = MULTICAST_PORT;
	recvParam.netPkgSize = MULTICAST_PACKAGE_SIZE; 
#else
	//设置接收参数开始接收
	recvParam.dstAddr = MULTICAST_ADDR;	
	recvParam.dstPort = MULTICAST_PORT;	
	recvParam.lcPort = MULTICAST_PORT;
	recvParam.netPkgSize = MULTICAST_PACKAGE_SIZE; 
#endif	
	multicast_recv(&recvParam);
	
	pthread_exit(0);
	return NULL;
}

void *idle_task(void *args)
{
	int coreId;
	coreId=cpu_id_get();
	
	while(1)
	{
		printk("core%d:idle!\r\n",coreId);
		sleep(3);
	}
	
	pthread_exit(0);
	return NULL;
}
