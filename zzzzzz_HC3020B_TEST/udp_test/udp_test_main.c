
#include "multicast.h"        
#include <stdio.h>
//#include <env.h>
#include <stdlib.h>
//#include <io.h>
#include <udelay.h>
#include <time.h>
#include <clock.h>
#include <pthread.h>

//void import_sym()
//{
//	bmc_info_show();
//}

extern unsigned int sysCpuGetID();


void udp_test_main(int is_send)//0-recv, 1-send
{
	
//	int fd = open("/dev/null", 0x2,0777);
//	global_std_set(0, fd);
//	global_std_set(1, fd);
//	global_std_set(2, fd);
	/* 添加用户代码 */ 
	int coreId = cpu_id_get();
	int cpuId = sysCpuGetID();
       
	/* 组播 测试,测试速率和丢包率*/
	pthread_t mcSendThd, mcRecvThd;
	int ret;
	sleep(1);
  
	if(is_send == 0)//recv
//   if((cpuId!=1)&&(coreId==0))
//	if((cpuId == 2)&&(coreId==0))
	{
		printk("\ncore%d: create and run multicast recv task.\r\n",coreId);
		
//		ret = pthread_create2(&mcRecvThd, "multicastRecvThd", /*157 174 155*/175, 0, 1*1024*1024, mcRecv_task,0);
		ret = pthread_create3(&mcRecvThd, "multicastRecvThd", /*157 174 155*/175, 0, 1*1024*1024, 1<<1, mcRecv_task,0);
		if(ret != 0)
		{
			printk("core_%d: bcRecv task create failed.\r\n",coreId);
		}
		pthread_detach(mcRecvThd);
	}
  sleep(5);                                          
#if 1
  if(is_send == 1)//send
//  if((cpuId==1)&&(coreId==0))
  {
	   printk("\ncore%d: create and run multicast send task.\r\n",coreId);
//	   ret = pthread_create2(&mcSendThd, "multicastSendThd", 175, 0, 1*1024*1024, mcSend_task,0);
	   ret = pthread_create3(&mcSendThd, "multicastSendThd", 175, 0, 1*1024*1024, 1<<1, mcSend_task,0);
	   if(ret != 0)
	   {
		  printk("core_%d: bcSend task create failed.\r\n",coreId);
	   }
	   pthread_detach(mcSendThd);
  }  
#endif

	return;
}




extern unsigned long long ldf_udelay;
extern unsigned long long ldf_size;
extern unsigned long long ldf_blen;
extern int shellPlus_parseline(char *cmdline);

void test_udp_loop(void)
{
	pthread_t mcSendThd, mcRecvThd;
	int ret;
	
	ldf_udelay = 0;
	ldf_size = 1024;
	ldf_blen = 65536;
	
	/*recv*/
	printk("\ncreate and run udp recv task.\r\n");
	ret = pthread_create3(&mcRecvThd, "udpRecvThd", 175, 0, 1*1024*1024, 1<<1, mcRecv_task,0);
	if(ret != 0)
	{
		printk("bcRecv task create failed.\r\n");
	}
	pthread_detach(mcRecvThd);
	
	sleep(3);
	
	/*send*/
	printk("\ncreate and run udp send task.\r\n");
	ret = pthread_create3(&mcSendThd, "udpSendThd", 175, 0, 1*1024*1024, 1<<2, mcSend_task,0);
	if(ret != 0)
	{
	  printk("bcSend task create failed.\r\n");
	}
	pthread_detach(mcSendThd);
}
