/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件定义了运行ReWorks操作系统所需初始化接口，该文件被usrInit.c使用
 * 
 * 自动生成的文件。 不要进行编辑
 * 
 */


#ifndef REDE_CONFIGURATION_H_INCLUDED__
#define REDE_CONFIGURATION_H_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif


extern int sys_mem_init(u32 mm_type);
extern int symtbl_module_init(void);
#ifdef SHOW_ENABLE
#include <object_show.h>
#endif

#include <pthread_hook.h>
#include <show.h>
#include <reworks/forced_link.h>
#include <pthread.h>

#ifdef SHOW_ENABLE
#include <pthread_show.h>
extern void pthread_show_init();
#endif
#include <mpart.h>

#ifdef SHOW_ENABLE
#include <mpart_show.h>
extern void mpart_show_init();
#endif

#include <memory.h>

#ifdef SHOW_ENABLE
#include <memory_show.h>
extern void mem_show_init();
#endif

#include <time.h>
#include <ratemon.h>
#include <wdg.h>

#ifdef SHOW_ENABLE
#include <timer_show.h>
extern void timer_show_init();
#include <ratemon_show.h>
extern void rms_show_init();
#include <wdg_show.h>
extern void wdg_show_init();
#endif

#include <semaphore.h>

#ifdef SHOW_ENABLE
#include <sem_show.h>
extern void sem_show_init();
#endif

#ifdef SHOW_ENABLE
#include <mutex_show.h>
extern void pthread_mutex_show_init();
#endif
extern int mutex_module_init(int max_mutexes);

#include <signal.h>
extern int signal_module_init(void);
#ifdef SHOW_ENABLE
#include <signal_show.h>
extern void sig_show_init();
#endif

#include <mqueue.h>

#ifdef SHOW_ENABLE
#include <mqueue_show.h>
extern void mq_show_init();
#endif

#include <event.h>
extern void libc_module_init();
#include <reworks/monitor_cpu.h>
#ifdef INCLUDE_SHELL
extern void reworks_cpuuse_cmd_init();
#endif
extern void fpu_module_init();
extern void vec_module_init();
extern int exception_module_init();
extern int exc_task_module_init();
extern void isr_nestable_set(u32 isr_flag, u32 fpu_flag);
#include <semLib.h>
#include <taskLib.h>
extern void udelay_module_init(void);
extern int cpu_up_sync(int,cpuset_t);
extern void cores_time_sync();
#include <stdio.h>
extern int fsmgr_module_init(void);
extern int io_module_init(void);
extern int tty_module_init(void);
extern int buf_module_init(void);
extern int pty_module_init(int pty_num);
extern int pci_module_init(void);
extern int pci_show_module_init(void);
#include "netInit.h"

#ifdef IPNET_USE_DMA
#include <reworks/types.h>
extern PART_ID memNetPartId;
extern void mempart_to_dma(PART_ID *);
__DMA_MEMORY_PREASSIGNED(netbuf, IPNET_MEMORY_LIMIT << 20, 32);
#endif
#ifdef INCLUDE_SHELL
extern void ipnet_cmd_init();
#endif
extern int hrfs_module_init(int cache_size, int fd_num);
extern int dosfs_module_init(int cache_size, int fd_mum);   
#include <reworksio.h>
extern  void  serial_module_init();
#define INCLUDE_VXBUS
#define INCLUDE_VXB_CMDLINE
#define INCLUDE_HWMEM_ALLOC
#define INCLUDE_PARAM_SYS
#define INCLUDE_PLB_BUS
#define HWMEM_POOL_SIZE 100000

extern void vxbus_module_init();
#include <ip/compat/ifLib.h>
#include <arpa/inet.h>

#define INCLUDE_DMA_SYS
//#define INCLUDE_MII_BUS
//#define INCLUDE_GT64120A_PCI 
#define INCLUDE_PCI_BUS_AUTOCONF
#define INCLUDE_PCI_BUS_SHOW
#define INCLUDE_PCI_OLD_CONFIG_ROUTINES
#define INCLUDE_VXBUS_SHOW
//#define INCLUDE_RTL8169_VXB_END
//#define INCLUDE_GEI825XX_VXB_END 
//#define INCLUDE_GENERICPHY
//#define INCLUDE_BCM54XXPHY
//#define INCLUDE_RTL8169PHY
#define DRV_INTCTLR_I8259
#define INCLUDE_PCI_BUS
#define INCLUDE_PCI 
#define INCLUDE_VXB_LEGACY_INTERRUPTS 

extern void vxbus_net_drv_init(void);
extern int if_addr6_add(char *interfaceName, char *interfaceAddress, int prefix_len, int flags);
extern unsigned int sysCpuGetID();
extern unsigned int sysGetSlotNum();
extern void net_drv_init_gem(int unit);
extern unsigned int sysCpuGetID();
extern unsigned int sysGetSlotNum();
extern void net_drv_init_gem(int unit);
extern int can_module_init(u8 controller);
extern void i2c_module_init(int channel);
extern void lpc_module_init(void);
extern void spi_module_init(u32 controller);
extern void qspi_module_init(void);
extern int sdio_emmc_init(int controller, int fmt_state);
extern void  hrTimerModuleInit (u32 index);
extern void  hrTimerModuleInit (u32 index);
extern void hr3_wdog_module_init(void);
extern void netUtils_module_init();
extern void hr3API_module_init();
extern void UserMemModuleInit(void);
extern int telnet_module_init(void);
extern int ftp_module_init(void);
extern int shell_module_init(int);
#define INCLUDE_VXBUS
#include <vxbUsrCmdLine.c>



#ifdef CONFIGURE_INIT

void sysConfigInition()
{	
	/* 核心内存模块 */
	
	sys_mem_init(KERNEL_MM_METHOD);
	
	/* 符号表 */
	
	symtbl_module_init();
	
	/* 系统对象模块 */
	
	#ifdef SHOW_ENABLE
	object_show_init();
	#endif
	
	/* 任务钩子模块 */
	
	thread_hook_init();
	
	/* 信息显示模块 */
	
	show_module_init();
	
	/* 核心任务模块 */
	
	thread_module_init(REWORKS_MAX_TASKS);
	#ifdef SHOW_ENABLE
	pthread_show_init();
	#endif
	
	/* 内存分区模块 */
	
	mpart_module_init(REWORKS_MAX_MPARTS);
	#ifdef SHOW_ENABLE
	mpart_show_init();
	#endif
	
	/* 用户堆模块 */
	
	heap_mem_init(REWORKS_HEAP_ALGORITHM);
	#ifdef SHOW_ENABLE
	mem_show_init();
	#endif
	
	
	/* 定时器模块 */
	
	timer_module_init(REWORKS_MAX_TIMERS);
	rms_module_init(REWORKS_MAX_RMSES);
	wdg_module_init(REWORKS_MAX_WDGS);
	#ifdef SHOW_ENABLE
	timer_show_init();
	rms_show_init();
	wdg_show_init();
	#endif
	
	/* 信号量模块 */
	
	sem_module_init(REWORKS_MAX_SEMS);
	#ifdef SHOW_ENABLE
	sem_show_init();
	#endif
	
	/* 互斥量模块 */
	
	mutex_module_init(REWORKS_MAX_MUTEXES);
	#ifdef SHOW_ENABLE
	pthread_mutex_show_init();
	#endif
	
	/* 信号模块 */
	
	signal_module_init();
	#ifdef SHOW_ENABLE
	sig_show_init();
	#endif
	
	/* 消息队列模块 */
	
	mq_module_init(REWORKS_MAX_MSGQS);
	#ifdef SHOW_ENABLE
	mq_show_init();
	#endif
	
	/* 事件模块 */
	
	event_module_init();
	
	/* 任务C库支持模块 */
	
	libc_module_init();
	
	/* CPU使用监视模块 */
	
	/* 针对所有任务的CPU使用率统计的模块，在所有新建任务之前初始化。 */
	thread_cpu_use_module_init();
	#ifdef INCLUDE_SHELL
	reworks_cpuuse_cmd_init();
	#endif
	
	/* 浮点支持 */
	
	fpu_module_init();
	
	/* 矢量支持 */
	
	vec_module_init();
	
	/* 异常处理模块 */
	
	exception_module_init();
	exc_task_module_init();
	
	/* 中断控制模块 */
	
	isr_nestable_set(ISR_NESTABLE, FPU_SAVECTX_ENABLE);
	
	/* VxWorks信号量模块 */
	
	semModuleInit();
	
	/* VxWorks任务模块 */
	
	taskLibInit();
	vx_fp_task_always_on(VX_FP_TASK_DEFAULT);

}


void userConfigInition()
{	
	/* 微秒级延时支持 */
	
	#if (!USE_TIME_BASE_REG)
	udelay_module_init();
	#endif
	
	/* CPU支持 */
	
	cpu_up_sync(CPU_NUMBER,CPUSET);
	
	/* 核间时钟同步 */
	
	cores_time_sync();
	
	/* 基本模块 */
	
	fsmgr_module_init();
	#ifndef REWORKS_MIN_CONFIG
	typedef void(*PRINT_REWORKS_LOGO)(const char *fmt, ...);
	extern PRINT_REWORKS_LOGO reworks_logo_show_printx;
	reworks_logo_show_printx = (PRINT_REWORKS_LOGO)printf;
	#endif
	
	
	/* 扩展模块 */
	
	io_module_init();
	
	/* TTY模块 */
	
	tty_module_init();
	
	/* 缓冲模块 */
	
	buf_module_init();
	
	/* 伪终端 */
	
	pty_module_init(PTY_NUMBER);
	
	/* PCI配置支持 */
	
	pci_module_init();
	
	/* PCI信息显示 */
	
	pci_show_module_init();
	
	/* 增强型网络协议栈 */
	
	extern void net_module_init(unsigned int ipnet_memory_limit, int timeout_prio, int ipnet_to, int urgent_collection);
	extern int if_addr_set(char *interfaceName, char *interfaceAddress);
	extern int if_flag_change(char *interfaceName, int flags, int on);
	extern int ip_attach(int unit, char * pDevice);
	uint32_t inet_addr (const char *);
	extern int ifMaskSet (char *interfaceName, int netMask);
	
	#ifdef IPNET_USE_DMA
	mempart_to_dma(&memNetPartId);
	#endif
	
	net_module_init(IPNET_MEMORY_LIMIT, IPNET_TIMEOUT_JOB_PRIO, IPNET_REASSEMBLY_TIMEOUT, IPNET_URGENT_COLLECTION);
	
	#ifdef INCLUDE_SHELL
	ipnet_cmd_init();
	#endif
	
	/* 高可靠文件系统 */
	
	hrfs_module_init(HRFS_CACHE_SIZE,HRFS_MAX_FD_NUM);
	
	/* DOS文件系统 */
	
	dosfs_module_init(DOSFS_CACHE_SIZE, DOSFS_MAX_FD_NUM);
	
	/* 串口模块 */
	
	serial_module_init();
	
	/* VXBUS 基本模块 */
	
	vxbus_module_init();
	
	/* VXBUS 网卡驱动 */
	
	vxbus_net_drv_init();
	
	/* Gem0 网口配置 */
	
	net_drv_init_gem(0);
	ipAttach(0,"gem");
	char ipAddr4_gem0[40];
	if(sysGetSlotNum() == 0xff){
	sprintf(ipAddr4_gem0, GEM0_IP_ADDRESS, 1*100+sysCpuGetID());
	}else
	{
	sprintf(ipAddr4_gem0, GEM0_IP_ADDRESS, sysGetSlotNum()*100+sysCpuGetID());
	}
	ifAddrSet(GEM0_HOST_NAME, ipAddr4_gem0);
	
	char ipAddr6_gem0[40];
	if(sysGetSlotNum() == 0xff){
	sprintf(ipAddr6_gem0, GEM0_IPv6_ADDRESS, 1*1000+sysCpuGetID());
	}else
	{
	sprintf(ipAddr6_gem0, GEM0_IPv6_ADDRESS, sysGetSlotNum()*1000+sysCpuGetID()+1);
	}
	if_addr6_add( GEM0_HOST_NAME, ipAddr6_gem0, GEM0_IPv6_PRELEN, GEM0_IPv6_FLAGS);
	ifMaskSet(GEM0_HOST_NAME, inet_addr(GEM0_SUBNET_MASK));
	ifFlagChange(GEM0_HOST_NAME, 1,TRUE);
	
	/* Gem1 网口配置 */
	
	net_drv_init_gem(1);
	ipAttach(1,"gem");
	char ipAddr4_gem1[40];
	if(sysGetSlotNum() == 0xff){
	sprintf(ipAddr4_gem1, GEM1_IP_ADDRESS, 1*100+sysCpuGetID());
	}else
	{
	sprintf(ipAddr4_gem1, GEM1_IP_ADDRESS, sysGetSlotNum()*100+sysCpuGetID());
	}
	ifAddrSet(GEM1_HOST_NAME, ipAddr4_gem1);
	
	char ipAddr6_gem1[40];
	if(sysGetSlotNum() == 0xff){
	sprintf(ipAddr6_gem1, GEM1_IPv6_ADDRESS, 1*1000+sysCpuGetID());
	}else
	{
	sprintf(ipAddr6_gem1, GEM1_IPv6_ADDRESS, sysGetSlotNum()*1000+sysCpuGetID()+1);
	}
	if_addr6_add( GEM1_HOST_NAME, ipAddr6_gem1, GEM1_IPv6_PRELEN, GEM1_IPv6_FLAGS);
	ifMaskSet(GEM1_HOST_NAME, inet_addr(GEM1_SUBNET_MASK));
	ifFlagChange(GEM1_HOST_NAME, 1,TRUE);
	
	/* CAN驱动支持 */
	
	can_module_init(0);
	can_module_init(1);
	
	/* I2C EEPROM驱动支持 */
	
	i2c_module_init(0);
	i2c_module_init(1);
	
	/* LPC FLASH驱动支持 */
	
	lpc_module_init();
	
	/* SPI FLASH驱动支持 */
	
	spi_module_init(0);
	spi_module_init(1);
	
	/* QSPI FLASH驱动支持 */
	
	qspi_module_init();
	
	/* SDIO0 EMMC驱动支持 */
	
	sdio_emmc_init(0, EMMC0_FMT_STATE);
	
	/* TIMER0驱动支持 */
	
	hrTimerModuleInit(0);
	
	/* TIMER1驱动支持 */
	
	hrTimerModuleInit(1);
	
	/* WDOG驱动支持 */
	
	hr3_wdog_module_init();
	
	/* NetUtils模块 */
	
	netUtils_module_init();
	
	/* HR3API驱动支持 */
	
	hr3API_module_init();
	
	/* USERMEM驱动支持 */
	
	UserMemModuleInit();
	
	/* TELNET服务 */
	
	telnet_module_init();
	
	/* FTP服务 */
	
	ftp_module_init();
	
	/* 人机交互外壳（shell） */
	
	#ifdef CONSOLE_OUTPUT_DEVICE_NAME
	shell_module_init(SHELL_PRIO);
	#endif

}


#endif /* CONFIGURE_INIT */

#ifdef __cplusplus
}
#endif

#endif /* REDE_CONFIGURATION_H_INCLUDED__ */
