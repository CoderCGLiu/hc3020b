/*-------------------------------------------------------------------------+
|
|      程序名称：cache管理（体系结构无关层）
|          描述：系统cache管理通过cache_module_init初始化，一般cache管理的
|				接口包括两个参数：地址和长度，有的接口操作对所有内存空间有效
|				(ENTIRE_CACHE),cacheDmaMalloc和cacheDmaFree接口一般为驱动
|				所调用。
|				cache一般有两种模式：write-through和copyback
|                   write-through:强制所有写操作执行写到cache和写到内存。
|                   copyback:写操作只执行写到cache并不写到内存，只有存在写内存
|                            要求时才执行写内存。
|
|               使用copyback机制性能较好，单需主要内存一致的问题（需要手动操作）。
|				cache不一致的情况有以下三种：
|				1. data cache/RAM，当cache和内存中的数据不同步时，如CPU和其
|				   总线控制器同时访问时，需要使内存一致。
|				2. instruction cache / data cache，当在使用动态加载、挂接中
|				   断等操作时，会对系统代码段进行更新，如果该段代码段已在指令
|				   Cache中，就会发生不一致的情况。这类情况的操作办法：flush
|				   数据cache以及invalidate指令cache。
|				3. shared cache lines，当两个任务使用到同一个cache line时，
|				   如其中一个任务对该cache line做invalidate操作，就可能影响
|				   到另一个任务。这类情况的操作办法：在cache line boundary
|				   分配内存，并且大小为cache line的倍数。
|               系统在保持cache一致性的同时拥有最优性能的方法是使用硬件的总线检测功能
|               (注:不是每个硬件系统都具有此功能)
|				另外相关的是write buffer机制，对内存的一致性也有影响。
|
|        编制人：李健
|        版本号：1.0
|      创建时间：2007-8-16
|      修改记录：
|  最后修改时间：2007-8-16
|  软件版权所有：信息产业部第三十二研究所
|          备注：
|
+--------------------------------------------------------------------------*/

#include <cpu.h>
#include <reworks/types.h>
#include <reworks/kernel_lock.h>
#include <stdlib.h>

#include <reworks/cache.h>
#include <gs464_cpu.h>
#include <irq.h>

/*------------------vx-------------------------*/
typedef	unsigned int	CACHE_MODE;		/* CACHE_MODE */

RE_CACHE_LIB re_cache_lib;

/*-------------------------------------------
 * cache_flush_ptr
 * cache_invalidate_ptr
 * 用于协同总线等模块。
 *-------------------------------------------*/
int (*cache_flush_ptr)( u32, void *, size_t) =NULL;
int (*cache_invalidate_ptr)( u32, void *, size_t) =NULL;
OS_STATUS (*cache_text_update_ptr)(void *address, size_t bytes) = NULL;

extern int cache2_libInit(CACHE_MODE	instMode, CACHE_MODE	dataMode);

void cpc_cache_text_update()
{
//	invalid_itlb(); 仅用于构建保护
	/* huangyuan20140114：cpc的中断已经可以起到pipeline_hazard_barrier()的效果。 */
}

#ifdef __multi_core__

#include <reworks/cpc.h>
int	cache_text_update (void * adrs, size_t bytes)
{	
	/* huangyuan20140115：使用内嵌汇编，对cache干扰更小。 */
	asm volatile(
		".set noreorder\n"
		".set mips64\n"
		"dla $2, jr_target\n"
		"jr.hb $2\n"
		"nop\n"			
		".set mips0\n"
		".set reorder\n"
		"jr_target: "
		:::"$2"
	);
		
	int is_locked = KERNEL_LOCK_OWNED_BY_ME();
	if(!is_locked)
	{
		KERNEL_LOCK_TAKE();
	}

//	invalid_itlb(); 仅用于构建保护

	cpc_broadcast(0, cpc_cache_text_update, NULL, REWORKS_CPC_SYNC);
	
	if(!is_locked)
	{
		KERNEL_LOCK_GIVE();
	}
	return 0;
}
#endif
/*-------------------------------------------------------------------------+
|
|   函数：cache_module_init - cache模块的初始化
|
|   描述：根据指令cache以及数据cache的访问模式初始化cache模块,目前系统支持的
|         cache访问模式是WRITE_THROUGH和WRITE_BACK。
|
| 输入值：inst_cache_mode - 指令cache访问模式
|         data_cache_mode - 数据cache访问模式
| 输出值：无
|
| 返回值：初始化成功是否
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/
extern char mips_cache_exception, mips_cache_exception_end;
int cache_module_init(u32 inst_cache_mode, u32 data_cache_mode)
{
    int ret = cache2_libInit (inst_cache_mode, data_cache_mode);
#ifdef __multi_core__
	cache_text_update_ptr = cache_text_update;
#endif

#ifdef __HUARUI3__
	//FuKai,2020-9-7 ：为了配合华睿3号先期验证，在cache出错时进行打印
	int level;
    level = int_lock();	/* 锁中断 */
	fill_exc_vec_code((void *) CACHE_ERR_EXC_VEC,
			&mips_cache_exception,
			&mips_cache_exception_end);

	int_unlock(level);
#endif

    return (ret);

}

/*-------------------------------------------------------------------------+
|
|   函数：cache_enable - 使能cache
|
|   描述：使能数据cache或指令cache,具体类型由参数cache指定
|
| 输入值：cache_type - cache类型
|
| 输出值：无
|
| 返回值：使能成功是否
|
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/

int cache_enable
(
    u32	cache_type		/* cache_type to enable */
)
{
    return ((re_cache_lib.cache_enable == NULL) ? OS_ERROR :
            (re_cache_lib.cache_enable) (cache_type));
}

/*-------------------------------------------------------------------------+
|
|   函数：cache_disable - 关闭cache
|
|   描述：关闭数据cache或指令cache,具体类型由参数cache指定,并且将cache中的
|         内容全部写回内存。
|
| 输入值：cache_type - cache类型
|
| 输出值：无
|
| 返回值：关闭成功是否
|
|头文件: #include<cache.h>
+--------------------------------------------------------------------------*/

int cache_disable
(
    u32	cache_type		/* cache_type to disable */
)
{
    return ((re_cache_lib.cache_disable == NULL) ? OS_ERROR :
            (re_cache_lib.cache_disable) (cache_type));
    
   
}

/*-------------------------------------------------------------------------+
|
|   函数：cache_flush - 将指定范围的cache中的内容写回内存。
|
|   描述：该例程将数据cache或指令cache内指定范围内的内容写回内存.根据不同的cache
|         设计，这个操作可能引起cache tags无效。如果cache 的访问模式是WRITE_THROUGH,
|         不需要调用该函数。
|
| 输入值：cache_type - cache类型
|         addr       - 内存逻辑地址
|         size       - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：刷新成功是否
|头文件: #include<cache.h>
+--------------------------------------------------------------------------*/


int cache_flush
(
    u32	    cache_type,		/* cache类型 */
    void *	addr,	        /* 内存逻辑地址 */
    size_t	size		    /* 内存大小(单位:字节)*/
)
{
    return ((re_cache_lib.cache_flush == NULL) ? OS_OK :
            (re_cache_lib.cache_flush) (cache_type, addr, size));
}

/*-------------------------------------------------------------------------+
|
|   函数：cache_invalidate -- 使指定范围cache无效
|
|   描述：该例程使数据cache或指令cache内的指定空间为无效.根据不同的cache
|         设计，这个操作可能直接无效cache tags或有刷新的操作。
| 输入值:cache_type      - cache类型
|          addr            - 内存逻辑地址
|          size            - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：无效cache成功是否
|
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/

int cache_invalidate
(
    u32	    cache_type,		/* cache类型  */
    void *	addr,	        /* 内存逻辑地址*/
    size_t	size		    /* 内存大小(单位:字节) */
)
{
    return ((re_cache_lib.cache_invalidate == NULL) ? OS_OK :
            (re_cache_lib.cache_invalidate) (cache_type, addr, size));
}
/*-------------------------------------------------------------------------+
|
|   函数：cache_inst_data_sync - 同步指定地址范围的数据cache和指令cache
|
|   描述：使CPU获得的执行指令和数据与内存一致。
|
| 输入值：addr    - 内存逻辑地址
|         size    - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：更新成功是否
|
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/

int cache_inst_data_sync
(
    void * addr,		/* 内存逻辑地址*/
    size_t size		/*内存大小(单位:字节)*/
)
{
    /* if address not valid, return */
    if ((ptrdiff_t)addr == -1)
	{
      return OS_ERROR;
    }
    return ((re_cache_lib.text_update == NULL) ? OS_OK :
            (re_cache_lib.text_update) (addr, size));
}

/*-----------------------vx--------------------------------------------*/

/*-------------------------------------------------------------------------+
|
|   函数：cacheClear - 清除cache
|
|   描述：根据cache类型、相关地址、大小清除cache，包括flush、invalidate两个操作。
|
| 输入值：cache - cache类型
|        address - 内存地址
|        bytes - 内存大小
|
| 输出值：无
|
| 返回值：清除cache成功是否
|
+--------------------------------------------------------------------------*/

int cache_clear
(
    RE_CACHE_TYPE	cache,		/* cache to clear */
    void *	address,	/* virtual address */
    size_t	bytes		/* number of bytes to clear */
)
{
//    return ((re_cache_lib.clearRtn == NULL) ? OS_ERROR :
//            (re_cache_lib.clearRtn) (cache, address, bytes));
	return 0;
}

void * cache_dma_malloc(size_t bytes)
{
	if(re_cache_lib.dma_malloc == NULL)
	{
//		memalign(CACHE_LINE_SIZE,ROUND_UP (bytes, CACHE_LINE_SIZE));
		return(malloc(bytes));
	}
	else
	{
		return ( void*)( (re_cache_lib.dma_malloc) (bytes));
	}
	
}

void cache_dma_free(void * pBuf )
{
	if(re_cache_lib.dma_free == NULL)
	{
		free(pBuf);
//		return 0;
	}
	else
	{
		(re_cache_lib.dma_free) (pBuf);
	}
}

int cache_pipe_flush()
{
//    return ((re_cache_lib.pipeFlushRtn == NULL) ? OS_OK : 
//            (re_cache_lib.pipeFlushRtn) ());
	return 0;
}

int cache_lock
    (
    RE_CACHE_TYPE	cache,		/* cache to lock */
    void *	address,	/* virtual address */
    size_t	bytes		/* number of bytes to lock */
    )
{
//    return ((re_cache_lib.lockRtn == NULL) ? OS_OK : 
//            (re_cache_lib.lockRtn) (cache, address, bytes));
	return 0;
}

int cache_unlock
    (
    RE_CACHE_TYPE	cache,		/* cache to lock */
    void *	address,	/* virtual address */
    size_t	bytes		/* number of bytes to lock */
    )
{
//    return ((re_cache_lib.unlockRtn == NULL) ? OS_OK : 
//            (re_cache_lib.unlockRtn) (cache, address, bytes));
	return 0;
}
