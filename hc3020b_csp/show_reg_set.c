#include "cpu.h"
extern REGS_INDEX thread_regs_index[];
/*
 * 由“信息显示模块”调用
 * 防止勾选“最小配置”时编译的镜像过大，挪到单独的文件中
 */
void show_reg_set(REG_SET *pRegs)
{
	int res, ix;
	int *reg_hi;
	int *reg_lo;
	u64 *reg64;
	
	if (pRegs->type == SWITCH_TYPE)
	{
		for (ix = 0; thread_regs_index[ix].reg_name != NULL; ix++)
		{
			if ((ix % 4) == 0)
			{
				printf ("\n");
			}
			else
			{
				printf (";%3s","");
			}	
			
			if (ix < 14)
			{	
				if (ix == 0)
				{
					reg_lo = (int *) ((u64)pRegs + thread_regs_index[ix].reg_off);
					printf ("%-6s = %8x", thread_regs_index[ix].reg_name, *reg_lo);
				}
				else
				{
					reg_hi = (int *) ((u64)pRegs + thread_regs_index[ix].reg_off);
					reg_lo = (int *) ((u64)reg_hi + sizeof(u32)); 
					reg64 = (u64*) ((u64)pRegs + thread_regs_index[ix].reg_off);
					printf ("%-6s = %8x %x", thread_regs_index[ix].reg_name, *reg_lo, *reg_hi);
				}
			}
			else
			{
				printf ("%-6s --------N/A--------", thread_regs_index[ix].reg_name);
			}
		}
	}
	else if (pRegs->type == INTERRUPT_TYPE)
	{
		for (ix = 0; thread_regs_index[ix].reg_name != NULL; ix++)
		{
			if ((ix % 4) == 0)
			{
				printf ("\n");
			}
			else
			{
				printf (";%3s","");
			}
			
			if (ix == 0)
			{
				reg_lo = (int *) ((u64)pRegs + thread_regs_index[ix].reg_off);
				printf ("%-6s = %8x", thread_regs_index[ix].reg_name, *reg_lo);
			}
			else
			{
				reg_hi = (int *) ((u64)pRegs + thread_regs_index[ix].reg_off);
				reg_lo = (int *) reg_hi + sizeof(u32); 
				printf ("%-6s = %8x %8x", thread_regs_index[ix].reg_name, *reg_lo, *reg_hi);
			}
		}
	}

//	context_dump(pRegs);
	
	printf ("\n");
}

