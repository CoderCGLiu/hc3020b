/* lw_unaligned_exc.s - 地址不对齐异常处理*/


#define ASM_LANGUAGE
#define _ASMLANGUAGE

#include <asm_linkage.h>
#include <mips_asm.h>
#include <fpu.h> 
#include <bd_opt.h>
#include <arch_mips.h>
	/* internals */
	.rdata
alt:
	.asciz "context_restore(): exceptional context.\n\r"
	
#define	FRAMETID(routine)	FRAMER0(routine)
#define	FRAMEEPC(routine)	FRAMER1(routine)
#define FRAMEREG(routine)	FRAMER2(routine)

	.globl fpp_emulate_branch	/* emulate branch instructions */

	.text
	.set	reorder


/*******************************************************************************
*
* fpp_emulate_branch - 计算MIPS跳转或者分支指令的结果
*
* 计算在分支延迟槽中发生的异常/中断的EPC值
*
* 返回值: 分支目标地址.
*	     
*/

	.ent	fpp_emulate_branch
FUNC_LABEL(fpp_emulate_branch)
#ifndef SOFT_FLOAT

	/* unconditional immediates are easy, 
	   so they go first (jal, j) */

	and	t0, a1, GENERAL_OPCODE_MASK	/* look at opcode field */

	li	t3, J_INSTR			/* load temp j value */
	bne	t0, t3, 1f			/* am I jump always */
	and	a1, TARGET_MASK			/* grab 26 bit target */
	sll	a1, 2				/* shift to word bounds */
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	

	addiu	t1, t1, 4			/* calculate BD slot addr */
	and	t2, t1, ~TARGET_MASK		/* grab upper 4 pc bits */
	or	v0, a1, t2			/* combine TARGET with pc top 
						   4 bits, return value */
	j	ra				/* return	*/

1:	and	t0, a1, GENERAL_OPCODE_MASK	/* look at opcode field */
	li	t3, JAL_INSTR			/* load temp jal value */
	bne	t0, t3, 2f			/* am I jump and link */

	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addu	t2, t1, 8			/* increment epc for ra */
	sw	t2, M_E_STK_RA(a0)		/* replace ra	*/
	and	a1, TARGET_MASK			/* grab 26 bit target */
	sll	a1, 2				/* shift to word bounds */
	addiu	t1, t1, 4			/* calculate BD slot addr */
	and	t2, t1, ~TARGET_MASK		/* grab upper 4 pc bits */
	or	v0, a1, t2			/* combine TARGET with pc top 
						   4 bits, return value */
	j	ra				/* return	*/

2:	bne	t0, zero, 4f			/* are we special opcode */

	/* now for unconditional registers (jr, jalr) */

	and	t0, a1, SPECIAL_MASK		/* look at special field */
	li	t3, JR_INSTR			/* load temp jr value */
	bne	t0, t3, 3f			/* am I jump register */
	srl	a1, (RS_POS - 2)		/* get table register
						   entry, notice word
						   offset (-2)	*/
	lw	t2, regFetchIndexTbl(a1)	/* grab correct instructions */
        move    t7, ra                          /* set return address */
	j	t2				/* excecute the instructions, 
						   return taken care of	*/

3:	and	t0, a1, SPECIAL_MASK		/* look at special field */
	li	t3, JALR_INSTR			/* load temp jalr value */
	bne	t0, t3 , 4f			/* am I jump and 
						   link register */
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addu	t2, t1, 8			/* increment epc for ra */
	sw	t2, M_E_STK_RA(a0)		/* replace ra	*/
	srl	a1, RS_POS - 2			/* get table register
						   entry, notice word
						   offset (-2)	*/
	lw	t2, regFetchIndexTbl(a1)	/* grab correct instructions */
        move    t7, ra                          /* set return address */
	j	t2				/* excecute the instructions,
						   return taken care of	*/
4:
	/* now for the hard part, branches.  Order of importance seems
	   to be as follows :   beq, bne, blez, bgez, bltz, bgtz,
				bcf, bct, bltzal, bgtzal
           we also check for branch likely instructions
	   exceptions occuring in the branch delay slot of a 
	   branch likely should mean that the branch will be taken
           (otherwise  the instruction shopuld have been nullified)
	   we go through the comparison routines just to be sure
	*/

	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t3, t0, GENERAL_OPCODE_MASK	/* get branch opcode	*/

	beq	t3, BEQ_INSTR, 1f
	bne	t3, BEQL_INSTR, 5f		/* are we beq/beql instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	and	t2, t0, RT_MASK			/* look at RT only	*/
	srl	t2, (RT_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RT register	*/
	nop
	move	t5, v0				/* store value in t5	*/

	bne	t4, t5, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/

5:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t3, t0, GENERAL_OPCODE_MASK	/* get branch opcode	*/
	beq	t3, BNE_INSTR,1f
	bne	t3, BNEL_INSTR,6f		/* are we bne/bnel instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	and	t2, t0, RT_MASK			/* look at RT only	*/
	srl	t2, (RT_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RT register	*/
	nop
	move	t5, v0				/* store value in t5	*/

	beq	t4, t5, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/

6:	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t3, t0, GENERAL_OPCODE_MASK	/* get branch opcode	*/
	beq	t3, BLEZ_INSTR,1f
	bne	t3, BLEZL_INSTR,7f		/* are we blez/blezl instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	bgtz	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/

7:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	or	t1, t0, REGIMM			/* turn on REGIMM bits  */
	and	t2, t1, RS_MASK			/* mask RS bits         */
	beq	t2, BGEZ_INSTR, 1f
	bne	t2, BGEZL_INSTR, 8f		/* are we bgez/bgezl instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	bltz	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/

8:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	or	t1, t0, REGIMM			/* turn on REGIMM bits  */
	and	t2, t1, RS_MASK			/* mask RS bits         */
	beq	t2, BLTZ_INSTR, 1f
	bne	t2, BLTZL_INSTR, 9f		/* are we bltz/bltzl instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	bgez	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/
9:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t3, t0, GENERAL_OPCODE_MASK	/* get branch opcode	*/
	beq	t3, BGTZ_INSTR, 1f
	bne	t3, BGTZL_INSTR, 10f		/* are we bgtz/bgtzl instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	blez	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/

10:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t1, t0, ~CC_MASK		/* mask off bits 18-20  */	
	beq     t1, BC1F_INSTR, 1f
	bne	t1, BC1FL_INSTR, 11f		/* are we bc1f/bc1fl instr */

1:	and	t4, a2, CP1_VALUE		/* is fpa true		*/

	bne	zero, t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/
11:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	and	t1, t0, ~CC_MASK		/* mask off bits 18-20  */
	beq	t1, BC1T_INSTR, 1f
	bne	t1, BC1TL_INSTR, 12f		/* are we bc1t/bc1tl instr	*/

1:	and	t4, a2, CP1_VALUE		/* is fpa true		*/

	beq	zero, t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/
12:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	or	t1, t0, REGIMM			/* turn on REGIMM bits  */
	and	t2, t1, RS_MASK			/* mask RS bits         */
	beq	t2, BLTZAL_INSTR, 1f
	bne	t2, BLTZALL_INSTR, 13f		/* are we bltzal/bltzall instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	bgez	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addu	t2, t1, 8			/* increment epc for ra */
	sw	t2, M_E_STK_RA(a0)		/* replace ra	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/
13:
	and	t0, a1, (OFFSET16_MASK << 16)	/* mask high 16 bits	*/
	or	t1, t0, REGIMM			/* turn on REGIMM bits  */
	and	t2, t1, RS_MASK			/* mask RS bits         */
	beq	t2, BGEZAL_INSTR,1f
	bne	t2, BGEZALL_INSTR,14f		/* are we bgtzal/bgtzall instr */

1:	and	t2, t0, RS_MASK			/* look at RS only	*/
	srl	t2, (RS_POS - 2)
	lw	t2, regFetchIndexTbl(t2)	/* get routine address	*/
	jalr	t7, t2				/* read RS register	*/
	nop
	move	t4, v0				/* store value in t4	*/

	bltz	t4, 15f 			/* don't take branch	*/
	and	a1, OFFSET16_MASK		/* look at ls 16 bits	*/
	sll	a1, IMMEDIATE_POS		/* sign extend	*/
	sra	a1, IMMEDIATE_POS - 2
	lw	t1, M_E_STK_EPC(a0)		/* grab epc	*/
	addu	t2, t1, 8			/* increment epc for ra */
	sw	t2, M_E_STK_RA(a0)		/* replace ra	*/
	addiu	t1, t1, 4			/* calculate BD slot addr */
	addu	v0, t1, a1			/* calculate new epc */
	j	ra				/* return	*/


#endif	/* !SOFT_FLOAT */

14:	/* it wasn't a branch that we recognised! */
	li	v0, 1				/* give up	*/
	j	ra				/* return to caller	*/

15:	/* don't take branch, move pc past branch delay slot */
	lw	v0, M_E_STK_EPC(a0)		/* get epc from esf */
	addiu	v0, v0, 8			/* calculate new epc */
	j	ra				/* return to caller	*/
	.end	fpp_emulate_branch
	
/*******************************************************************************
*
* regFetchTable - returns the contents of a given register
*
* This routine has 32 entry points, one for each r3k general purpose
* register.  The user interface is to jal to the register offset of
* regFetchIndexTbl, and expect results in v0.  The return address register
* t7 is uses so we do not have to set up a stack frame.
*
* RETURNS: contents of given register
*
* NOMANUAL - not really a routine but a jump table

*/
	.ent	regFetchTable
regFetchTable:
reg0:
	move	v0, zero
	j	t7
reg1:
	LW	v0,M_E_STK_AT(a0)
	j	t7
reg2:
	LW	v0,M_E_STK_V0(a0)
	j	t7
reg3:
	LW	v0,M_E_STK_V1(a0)
	j	t7
reg4:
	LW	v0,M_E_STK_A0(a0)
	j	t7
reg5:
	LW	v0,M_E_STK_A1(a0)
	j	t7
reg6:
	LW	v0,M_E_STK_A2(a0)
	j	t7
reg7:
	LW	v0,M_E_STK_A3(a0)
	j	t7
reg8:
	LW	v0,M_E_STK_T0(a0)
	j	t7
reg9:
	LW	v0,M_E_STK_T1(a0)
	j	t7
reg10:
	LW	v0,M_E_STK_T2(a0)
	j	t7
reg11:
	LW	v0,M_E_STK_T3(a0)
	j	t7
reg12:
	LW	v0,M_E_STK_T4(a0)
	j	t7
reg13:
	LW	v0,M_E_STK_T5(a0)
	j	t7
reg14:
	LW	v0,M_E_STK_T6(a0)
	j	t7
reg15:
	LW	v0,M_E_STK_T7(a0)
	j	t7
reg16:
	LW	v0,M_E_STK_S0(a0)
	j	t7
reg17:
	LW	v0,M_E_STK_S1(a0)
	j	t7
reg18:
	LW	v0,M_E_STK_S2(a0)
	j	t7
reg19:
	LW	v0,M_E_STK_S3(a0)
	j	t7
reg20:
	LW	v0,M_E_STK_S4(a0)
	j	t7
reg21:
	LW	v0,M_E_STK_S5(a0)
	j	t7
reg22:
	LW	v0,M_E_STK_S6(a0)
	j	t7
reg23:
	LW	v0,M_E_STK_S7(a0)
	j	t7
reg24:
	LW	v0,M_E_STK_T8(a0)
	j	t7
reg25:
	LW	v0,M_E_STK_T9(a0)
	j	t7
reg26:
	LW	v0,M_E_STK_K0(a0)
	j	t7
reg27:
	LW	v0,M_E_STK_K1(a0)
	j	t7
reg28:
	LW	v0,M_E_STK_GP(a0)
	j	t7
reg29:
	LW	v0,M_E_STK_SP(a0)
	j	t7
reg30:
	LW	v0,M_E_STK_FP(a0)
	j	t7
reg31:
	LW	v0,M_E_STK_RA(a0)
	j	t7
	.end	regFetchTable

/*******************************************************************************
*
* regJumpTable - excecution code for jump instruction execution
*
* This routine contains the local symbols used by regJmpIndexTbl to determine
* what action should be taken by certain MIPS instructions.  This routine is
* not callable by any "C" or assembler routines.
*
* RETURNS: N/A
*
* NOMANUAL - not really a routine but a jump table

*/
	.ent	regJumpTable
regJumpTable:
jreg0:
	lw	t1,M_E_STK_ZERO(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg1:
	lw	t1,M_E_STK_AT(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg2:
	lw	t1,M_E_STK_V0(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg3:
	lw	t1,M_E_STK_V1(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg4:
	lw	t1,M_E_STK_A0(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg5:
	lw	t1,M_E_STK_A1(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg6:
	lw	t1,M_E_STK_A2(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg7:
	lw	t1,M_E_STK_A3(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg8:
	lw	t1,M_E_STK_T0(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg9:
	lw	t1,M_E_STK_T1(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg10:
	lw	t1,M_E_STK_T2(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg11:
	lw	t1,M_E_STK_T3(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg12:
	lw	t1,M_E_STK_T4(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg13:
	lw	t1,M_E_STK_T5(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg14:
	lw	t1,M_E_STK_T6(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg15:
	lw	t1,M_E_STK_T7(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg16:
	lw	t1,M_E_STK_S0(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg17:
	lw	t1,M_E_STK_S1(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg18:
	lw	t1,M_E_STK_S2(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg19:
	lw	t1,M_E_STK_S3(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg20:
	lw	t1,M_E_STK_S4(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg21:
	lw	t1,M_E_STK_S5(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg22:
	lw	t1,M_E_STK_S6(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg23:
	lw	t1,M_E_STK_S7(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg24:
	lw	t1,M_E_STK_T8(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg25:
	lw	t1,M_E_STK_T9(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg26:
	lw	t1,M_E_STK_K0(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg27:
	lw	t1,M_E_STK_K1(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg28:
	lw	t1,M_E_STK_GP(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg29:
	lw	t1,M_E_STK_SP(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg30:
	lw	t1,M_E_STK_FP(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
jreg31:
	lw	t1,M_E_STK_RA(a0)
	sw	t1,M_E_STK_EPC(a0)
	j	ra
	.end	regJumpTable

	.data
	.align	4
regJmpIndexTbl:
	.word	jreg0
	.word	jreg1
	.word	jreg2
	.word	jreg3
	.word	jreg4
	.word	jreg5
	.word	jreg6
	.word	jreg7
	.word	jreg8
	.word	jreg9
	.word	jreg10
	.word	jreg11
	.word	jreg12
	.word	jreg13
	.word	jreg14
	.word	jreg15
	.word	jreg16
	.word	jreg17
	.word	jreg18
	.word	jreg19
	.word	jreg20
	.word	jreg21
	.word	jreg22
	.word	jreg23
	.word	jreg24
	.word	jreg25
	.word	jreg26
	.word	jreg27
	.word	jreg28
	.word	jreg29
	.word	jreg30
	.word	jreg31

	.data
	.align	4
regFetchIndexTbl:
	.word	reg0
	.word	reg1
	.word	reg2
	.word	reg3
	.word	reg4
	.word	reg5
	.word	reg6
	.word	reg7
	.word	reg8
	.word	reg9
	.word	reg10
	.word	reg11
	.word	reg12
	.word	reg13
	.word	reg14
	.word	reg15
	.word	reg16
	.word	reg17
	.word	reg18
	.word	reg19
	.word	reg20
	.word	reg21
	.word	reg22
	.word	reg23
	.word	reg24
	.word	reg25
	.word	reg26
	.word	reg27
	.word	reg28
	.word	reg29
	.word	reg30
	.word	reg31
/******************************************************************************
* lw_unaligned_exc - MIPS非对齐地址访问异常处理
*
* 参数：
*  a0:	vecNum
*  a1:	异常上下文
*/
    .text
	.globl	lw_unaligned_exc
	.ent	lw_unaligned_exc
FUNC_LABEL(lw_unaligned_exc)
	SETFRAME(lw_unaligned_exc,8,2)
	.frame	sp, FRAMESZ(lw_unaligned_exc), ra
	dsubu	sp, FRAMESZ(lw_unaligned_exc)			//2016

	SW	ra, FRAMERA(lw_unaligned_exc)(sp)
	/*sw	a0, FRAMEA0(lw_unaligned_exc)(sp)zhangjing*/	/* save vector */
	SW	a1, FRAMEA1(lw_unaligned_exc)(sp)	/* save pEsf */

/*load value of pc */
	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	ld	a3, M_E_STK_EPC(a0)	/* load the epc into a3 */
	lw	a1, 0(a3)		/* load the instr at the epc into a1 */
		

	lw	v0, M_E_STK_CAUSE(a0)	/* load the cause register into v0 */

	bltz	v0, 3f			/* in branch delay slot ? */

	/*
	 * This is not in a branch delay slot (branch delay bit not set) so
	 * calculate the resulting pc (epc+4) into v0 and continue to softFp().
	 */
	daddu	v0, a3, 4
	SW	v0, FRAMEEPC(lw_unaligned_exc)(sp)	/* save the resulting pc */
	b	4f
3:
	/*
	 * This is in a branch delay slot so the branch will have to be
	 * emulated to get the resulting pc (done by calling emulateBranch() ).
	 * The arguments to emulateBranch are:
	 *     a0 - pEsf (exception stack)
	 *     a1 - the branch instruction
	 *     a2 - the floating-point control and status register
	 */

	/*lw	a2, M_E_STK_FPCSR(a0)*/	/* get value of C1_SR */
	mfc1 a2,C1_SR
	jal	fpp_emulate_branch	/* emulate the branch */
	nop
	bnel	v0, 1, emulateOk
	nop
	/* 出错打印异常信息 */
	li v0, 1	
	b process_end_1	

	
emulateOk:

	SW	v0, FRAMEEPC(lw_unaligned_exc)(sp)	/* save the resulting pc */
	
	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* restore exception stack pointer */
	LW	a3, M_E_STK_EPC(a0)	/* load the epc into a3 */

	lw	a1,4(a3)	/* get instruction to be emulated from BD slot */

4:

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	 /* restore the exception stack pointer */
	LW	v0, FRAMEEPC(lw_unaligned_exc)(sp) /* get the resulting pc */
	SW	v0, M_E_STK_EPC(a0) /* store the resulting pc in the epc */
	
	move t5, a1
	move t6, a1
	move t7, a1
	move t0, a1

/*store the opcode to t0, RT to t5, RS to t6, offset to t7*/
	li    t1,0x03e00000
	and t5,t5,t1
	srl t5, t5, 21

	li	t1,0x001f0000
	and t6, t6, t1
	srl t6, t6, 16

	li	t1,0x0000ffff
	and t7, t7, t1
	
	sll t7,t7,0x10
	sra t7,t7,0x10

	lui	t1,0xfc00
	and t0, t0, t1
	srl t0, t0, 26


/*----------------switch to process-------------------*/
/*	
*	opcode	lh-->0x21(33); lw->0x23(35); lhu->0x25(37);
*	opcode	lwu->0x27(39); ld->0x37(55); sh-->0x29(41);
*	opcode	sw-->0x2b(43); sd->0x3f(63).
*---------------------begin---------------------------*/
	
	beq	t0, 0x23, op_lw
	beq	t0, 0x2b, op_sw
	beq	t0, 0x27, op_lwu
	
	beq	t0, 0x21, op_lh_hb
	beq	t0, 0x25, op_lhu
	beq	t0, 0x29, op_sh
	
	beq t0, 0x31, op_lwc1
	beq t0, 0x39, op_swc1
	
	beq t0, 0x35, op_ldc1
	beq t0, 0x3d, op_sdc1
	
	beq	t0, 0x37, op_ld
	
	beq	t0, 0x3f, op_sd

	bne t0,0x3,1f
	nop

	li t3,'Z'
	li t0,0xbfe001e0
	sb t3,0(t0)
	sync

	1:

	
/*	li t3,'D'
	li t0,0xbfd003f8
	sb t3,0(t0)
	sync */
	
	/* huangyuan20110623：返回1，打印异常信息 */
	li v0, 1	

	b process_end_1	/*default just pc+4*/

/*-------------------end of switch--------------------*/


/*-----------------if opcode == op_lh-----------------*/
op_lh_hb:

	
	/*li t3,'B'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/	
	
	li t0, _RTypeSize
	multu t0, t5/*t6*/
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */

	daddu t4, t4, t7

/*	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	lw	a0, FRAMEA1(lw_unaligned_exc)(sp)*/	/* pEsf to a0 */ 
/*	addu a0, a0, t0*/
/*	sw	t4, 0(a0)*/	/* load the RS address to t4 */

	lb  t2, 1(t4)
	lbu t3, 0(t4)

	sll t2, t2, 0x8
	or  t2, t2, t3

/*store*/

	li t0, _RTypeSize
	multu t0, t6/*t5*/
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	sd	t2, 0(a0)	/*re store the RT  */

	b process_end
	
/*----------------end--process of op_lh --------------*/



/*-----------------if opcode == op_lhu-----------------*/
op_lhu:

	
	/*li t3,'C'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/	
	
/*    move a0,t6
	jal serial_puthex
loop:	b loop*/	
	li t0, _RTypeSize
	multu t0, t5/*t6*/
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	addu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */

	daddu t4, t4, t7

	lbu  t2, 1(t4)
	lbu t3, 0(t4)

	sll t2, t2, 0x8
	or  t2, t2, t3

/*store*/

	li t0, _RTypeSize
	multu t0, t6/*t5*/
	mflo t0

	addi t0, t0, E_STK_GREG_BASE


	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	sd	t2, 0(a0)	/*re store the RT  */

	b process_end
/*----------------end--process of op_lhu --------------*/


/*-----------------if opcode == op_lw-----------------*/
op_lw:

	/*li t3,'A'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/	
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */
	
	
	daddu t4, t4, t7


	lwl t3, 3(t4)
	lwr t3, (t4)

/*store*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	sd	t3, 0(a0)	/*re store the RT  */



/*	li t3,'X'
	li t0,0xbfd003f8
	sb t3,0(t0)
	sync	
	nop*/
	
	b process_end
	nop
/*----------------end--process of op_lw ------------*/

/*-----------------if opcode == op_lwu-----------------*/
op_lwu:
	/*li t3,'Q'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/	
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */

	daddu t4, t4, t7

	lwl t3, 3(t4)
	lwr t3, (t4)

	dsll t3, t3, 32
	dsrl t3, t3, 32

/*store*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	sd	t3, 0(a0)	/*re store the RT  */
	
	b process_end
/*----------------end--process of op_lwu ------------*/

/*-----------------if opcode == op_lwc1----------------*/
op_lwc1:
	/*li t3,'D'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */
	
	
	daddu t4, t4, t7


	lwl t3, 3(t4)
	lwr t3, (t4)

/*store*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, FPU_REG_BASE
	
	dsubu a0, sp, FPU_CONTEXT_CONTROL_SIZE+4 /* +4是为了地址8字节对齐 */
	jal mips_fpu_context_save

	daddu t0, a0, t0
	sd	t3, 0(t0)	/*re store the RT  */
	
	jal mips_fpu_context_restore
	
	b process_end
/*----------------end--process of op_lwc1 ------------*/

/*-----------------if opcode == op_ldc1-----------------*/
op_ldc1:
	/*li t3,'E'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/

	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	ld	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	ld	t4, 0(a0)	/* load the RS address to t4 */
	
	daddu t4, t4, t7

	ldl t3, 7(t4)
	ldr t3, (t4)
	
/*store*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, FPU_REG_BASE
	
	dsubu a0, sp, FPU_CONTEXT_CONTROL_SIZE+4 /* +4是为了地址8字节对齐 */
	jal mips_fpu_context_save

	daddu t0, a0, t0
	sd	t3, 0(t0)	/*re store the RT  */
	
	jal mips_fpu_context_restore

	b process_end
/*----------------end--process of op_ldc1 ------------*/

/*-----------------if opcode == op_ld-----------------*/
op_ld:
	/*li t3,'#'
	dli t0,0xffffffffbf0c0000
	sb t3,0(t0)
	sync*/	
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS address to t4 */
	
	/*li t3,'&'
	dli t0,0xffffffffbf0c0000
	sb t3,0(t0)*/
	
	daddu t4, t4, t7

	ldl t3, 7(t4)
	ldr t3, (t4)

/*	li t5,'@'
	dli t7,0xffffffffbf0c0000
	sb t5,0(t7)*/
/*store*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

/*	li t5,'!'
	dli t7,0xffffffffbf0c0000
	sb t5,0(t7)*/
	
	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	sd	t3, 0(a0)	/*re store the RT  */
	
	b process_end
/*----------------end--process of op_ld ------------*/

/*----------------------process of store instuct--------------*/
/*------------------------if op_code = sh-------------------*/
op_sh:
/*-------get the value of source reg-------*/
	/*li t3,'S'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t3, 0(a0)	/* load the value of source reg to t3 */	

	daddu t3, t3, t7

/*------------ get the target address-------*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	lh	t4, 0(a0)	/* load the RS's address to t4 */

/*	addu t4, t4, t7*/


/*---------------store+sdl + sdr-----------*/
	.set	noat
	sb t4, 0(t3)
	srl	t4, 0x8
	sb	t4, 1(t3)
	.set	at

	b process_end
/*---------------end if op_code = op_sh------------------*/


/*--------------------if op_code = sd------------------*/
op_sd:
/*-------get the value of source reg-------*/
	/*li t3,'T'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync	*/

	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	ld	t3, 0(a0)	/* load the value of source reg to t3 */	

	daddu t3, t3, t7

/*------------ get the target address-------*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	ld	t4, 0(a0)	/* load the RS's address to t4 */


/*---------------store+sdl + sdr-----------*/
	sdl t4, 7(t3)
	sdr t4, (t3)
	b process_end
/*---------------end if op_code = op_sd------------------*/



/*--------------------if op_code = sw------------------*/
op_sw:
/*-------get the value of source reg-------*/
	/*li t3,'U'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	ld	t3, 0(a0)	/* load the value of source reg to t3 */	
  
	daddu t3, t3, t7

/*------------ get the target address-------*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS's address to t4 */

/*	addu t4, t4, t7*/


/*---------------store+swl + swr-----------*/
/*			"1:\tswl\t%1, 3(%2)\n"		*/
/*			"2:\tswr\t%1, (%2)\n\t"		*/
	swl t4, 3(t3)
	swr t4, (t3)

	b process_end
/*---------------end if op_code = op_sw------------------*/

/*--------------------if op_code = swc1 ------------------*/
op_swc1:
	/*li t3,'V'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync*/

	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	LW	t3, 0(a0)	/* load the value of source reg to t3 */	
  
	daddu t3, t3, t7

/*------------ get the target address-------*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, FPU_REG_BASE
	
	dsubu a0, sp, FPU_CONTEXT_CONTROL_SIZE+4 /* +4是为了地址8字节对齐 */
	jal mips_fpu_context_save
	 
	daddu a0, a0, t0
	LW	t4, 0(a0)	/* load the RS's address to t4 */

/*---------------store+swl + swr-----------*/
	swl t4, 3(t3)
	swr t4, (t3)
	
	b process_end
/*--------------------if op_code = swc1 ------------------*/

/*--------------------if op_code = sdc1 ------------------*/
op_sdc1:
	/*li t3,'W'
	dli t0,0xffffffffbfe001e0
	sb t3,0(t0)
	sync	*/
	
	li t0, _RTypeSize
	multu t0, t5
	mflo t0

	addi t0, t0, E_STK_GREG_BASE

	LW	a0, FRAMEA1(lw_unaligned_exc)(sp)	/* pEsf to a0 */ 
	daddu a0, a0, t0
	ld	t3, 0(a0)	/* load the value of source reg to t3 */	

	daddu t3, t3, t7

/*------------ get the target address-------*/
	li t0, _RTypeSize
	multu t0, t6
	mflo t0

	addi t0, t0, FPU_REG_BASE

	dsubu a0, sp, FPU_CONTEXT_CONTROL_SIZE+4 /* +4是为了地址8字节对齐 */
	jal mips_fpu_context_save

	daddu a0, a0, t0
	ld	t4, 0(a0)	/* load the RS's address to t4 */

/*---------------store+sdl + sdr-----------*/
	sdl t4, 7(t3)
	sdr t4, (t3)

	b process_end
/*---------------end if op_code = op_sdc1------------------*/

/*	---------------end of s instruct----------------------*/
process_end:
	/* huangyuan20110623：返回0，不打印异常信息 */
	li v0, 0
	
process_end_1:

	LW	a1, FRAMEA1(lw_unaligned_exc)(sp) /* pEsf to a1 */ 
	/*la	a2, M_E_STK_SR(a1)*/	/* pass general register ptr */
	
	/* undo our stack frame */
	LW	ra,FRAMERA(lw_unaligned_exc)(sp)
	daddu	sp,FRAMESZ(lw_unaligned_exc)
	
	/* tail call to the exception handler, we don't return here. */
	j	ra

	.end	lw_unaligned_exc
