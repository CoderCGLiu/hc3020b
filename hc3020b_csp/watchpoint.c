#include <gs464_cpu.h>

#define WATCH_ADDR_MASK 0xfffffff8 /* bit 31-3 */
#define WATCH_READ_SHIFT 1
#define WATCH_WRITE_SHIFT 0

void set_watchpoint(void *watch_addr, boolean r, boolean w)
{
	u64 watch_value = (u64)watch_addr;
	
	watch_value &= WATCH_ADDR_MASK;
	
	if (r)
		watch_value += (1 << WATCH_READ_SHIFT); 

	if (w)
		watch_value += (1 << WATCH_WRITE_SHIFT);
	
	write_c0_watchlo(watch_value);
}

void unset_watchpoint()
{
	write_c0_watchlo(0);
}
/*******************************************************************************
 * 
 * 设置观察点
 * 
 * 		本程序用于设置程序观察点，当改写该地址时，系统会触发异常
 * 
 * 输入：
 * 		watch_addr 要观察的地址值
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void set_wr_watchpoint(void *watch_addr)
{
	u64 watch_value = (u64)watch_addr;
	
	watch_value &= WATCH_ADDR_MASK;
	

	watch_value += (1 << WATCH_WRITE_SHIFT);
	
	write_c0_watchlo(watch_value);
	
	return;
}



struct mtrace_hook
{
	FUNCPTR 		config_watchpoint;		
	FUNCPTR 		cancel_watchpoint;	
};

struct mtrace_hook mtrace_hook_entry = {0} ;
void watchpoint_init()
{
	mtrace_hook_entry.config_watchpoint = set_wr_watchpoint;
	mtrace_hook_entry.cancel_watchpoint = unset_watchpoint;
	
	return;
}
