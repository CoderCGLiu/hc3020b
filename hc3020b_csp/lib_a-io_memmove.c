/*
 FUNCTION
 <<memmove>>---move possibly overlapping memory

 INDEX
 memmove

 ANSI_SYNOPSIS
 #include <string.h>
 void *memmove(void *<[dst]>, const void *<[src]>, size_t <[length]>);

 TRAD_SYNOPSIS
 #include <string.h>
 void *memmove(<[dst]>, <[src]>, <[length]>)
 void *<[dst]>;
 void *<[src]>;
 size_t <[length]>;

 DESCRIPTION
 This function moves <[length]> characters from the block of
 memory starting at <<*<[src]>>> to the memory starting at
 <<*<[dst]>>>. <<memmove>> reproduces the characters correctly
 at <<*<[dst]>>> even if the two areas overlap.


 RETURNS
 The function returns <[dst]> as passed.

 PORTABILITY
 <<memmove>> is ANSI C.

 <<memmove>> requires no supporting OS subroutines.

 QUICKREF
 memmove ansi pure
 */

#include <string.h>
#include <_ansi.h>
#include <stddef.h>
#include <limits.h>

#define __64BIT_SUPPORT__

#ifdef __64BIT_SUPPORT__
#define		FULL_TYPE	long long
#define		HALF_TYPE	long
#define		MIN_TYPE	short
#else
#define		FULL_TYPE	long
#define		HALF_TYPE	short
#define		MIN_TYPE	char
#endif

#define		CHAR_TYPE	char

//1表示源地址与目的地址偏差相同
#define UNALIGNED_SYNC(X, Y)\
	(((FULL_TYPE)(X) & (sizeof (FULL_TYPE) - 1)) == ((FULL_TYPE)(Y) & (sizeof (FULL_TYPE) - 1)))

//0表示X是按FULL_TYPE边界对齐的
#define NOT_ALIGNED(X) ((FULL_TYPE)(X) & (sizeof (FULL_TYPE) - 1))
//0表示HALF_TYPE，
#define NOT_HALF_ALIGNED(X) ((HALF_TYPE)(X) & (sizeof(HALF_TYPE) - 1))
//0表示MIN_TYPE对齐
#define NOT_MIN_ALIGNED(x)	((MIN_TYPE)(x) & (sizeof(MIN_TYPE) - 1))
/* Nonzero if either X or Y is not aligned on a "long" boundary.  */
#define UNALIGNED(X, Y) \
  (((FULL_TYPE)X & (sizeof (FULL_TYPE) - 1)) | ((FULL_TYPE)Y & (sizeof (FULL_TYPE) - 1)))

/* How many bytes are copied each iteration of the 4X unrolled loop.  */
#define BIGBLOCKSIZE    (sizeof (FULL_TYPE) << 2)

/* How many bytes are copied each iteration of the word copy loop.  */
#define LITTLEBLOCKSIZE (sizeof (FULL_TYPE))

#define HALF_BLOCK_SIZE (LITTLEBLOCKSIZE / sizeof(HALF_TYPE))

/* Threshhold for punting to the byte copier.  */
#define TOO_SMALL(LEN)  ((LEN) < BIGBLOCKSIZE)

#define TYPE_BIT(type)	(8*sizeof(type))
#define CHAR_TYPE_BIT	TYPE_BIT(CHAR_TYPE)
#define MIN_TYPE_BIT	TYPE_BIT(MIN_TYPE)
#define HALF_TYPE_BIT	TYPE_BIT(HALF_TYPE)
#define FULL_TYPE_BIT	TYPE_BIT(FULL_TYPE)

#ifdef __ARCH_BIG_ENDIAN__
#ifdef __64BIT_SUPPORT__
#define	MAKE_CHAR_TO_FULL(src)	((FULL_TYPE)*(src)<<(7*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+1)<<(6*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(5*CHAR_TYPE_BIT)\
		| (FULL_TYPE)*((src)+3)<<(4*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+4)<<(3*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+5)<<(2*CHAR_TYPE_BIT) \
		| (FULL_TYPE)*((src)+6)<<(1*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+7))
#endif
#define MAKE_MIN_TO_FULL(src)	((FULL_TYPE)*(src)<<(3*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+1)<<(2*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(1*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+3))
#define MAKE_HALF_TO_FULL(src)	((FULL_TYPE)*(src)<<(1*HALF_TYPE_BIT) | (FULL_TYPE)*((src)+1))
#else
#ifdef __64BIT_SUPPORT__
#define	MAKE_CHAR_TO_FULL(src)	((FULL_TYPE)*(src) | ((FULL_TYPE)*((src)+1))<<(1*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+2))<<(2*CHAR_TYPE_BIT) \
		| ((FULL_TYPE)*((src)+3))<<(3*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+4))<<(4*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+5))<<(5*CHAR_TYPE_BIT) \
		| ((FULL_TYPE)*((src)+6))<<(6*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+7))<<(7*CHAR_TYPE_BIT))
#endif
#define MAKE_MIN_TO_FULL(src)	((FULL_TYPE)*(src) | (FULL_TYPE)*((src)+1)<<(1*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(2*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+3)<<(3*MIN_TYPE_BIT))
#define MAKE_HALF_TO_FULL(src)	((FULL_TYPE)*(src) | (FULL_TYPE)*((src)+1)<<(1*HALF_TYPE_BIT))
#endif

/*SUPPRESS 20*/
_PTR _DEFUN (io_memmove, (dst_void, src_void, length),
		_PTR dst_void _AND
		_CONST _PTR src_void _AND
		size_t length)
{
#if defined(PREFER_SIZE_OVER_SPEED) || defined(__OPTIMIZE_SIZE__)
	char *dst = dst_void;
	_CONST char *src = src_void;

	if (src < dst && dst < src + length)
	{
		/* Have to copy backwards */
		src += length;
		dst += length;
		while (length--)
		{
			*--dst = *--src;
		}
	}
	else
	{
		while (length--)
		{
			*dst++ = *src++;
		}
	}

	return dst_void;
#else
	CHAR_TYPE *dst = dst_void;
	/* 注意需要将src定位unsigned char *，否则可能（通常是这样）编译器默认为signed，这样，10000001扩展到32位时会变为111....10000001 */
	_CONST unsigned CHAR_TYPE *src = src_void;
	FULL_TYPE *aligned_dst;
	_CONST FULL_TYPE *aligned_src;
	int len = length;

	/* 反向拷贝 */
	if (src < dst && dst < src + len)
	{
		/* Destructive overlap...have to copy backwards */
		src += len;
		dst += len;

		/* Use optimizing algorithm for a non-destructive copy to closely 
		 match memcpy. If the size is small or either SRC or DST is unaligned,
		 then punt into the byte copy loop.  This should be rare.  */
		//		if (!TOO_SMALL(len) && !UNALIGNED (src, dst))
		/* 对齐情况相同。*/
		if (UNALIGNED_SYNC(src, dst))
		{
			while(NOT_ALIGNED(dst) && (len > 0))
			{
				*--dst = *--src;
				len--;
			}

			aligned_dst = (FULL_TYPE*)dst;
			aligned_src = (FULL_TYPE*)src;

			/* Copy 4X long words at a time if possible.  */
			while (len >= BIGBLOCKSIZE)
			{
				*--aligned_dst = *--aligned_src;
				*--aligned_dst = *--aligned_src;
				*--aligned_dst = *--aligned_src;
				*--aligned_dst = *--aligned_src;
				len -= BIGBLOCKSIZE;
			}

			/* Copy one long word at a time if possible.  */
			while (len >= LITTLEBLOCKSIZE)
			{
				*--aligned_dst = *--aligned_src;
				len -= LITTLEBLOCKSIZE;
			}

			/* Pick up any residual with a byte copier.  */
			dst = (char*)aligned_dst;
			src = (char*)aligned_src;
		}
		/* 对齐或者不对齐属性不同 */
		else
		{
			/* dst不对齐 */
			while(NOT_ALIGNED(dst) && (len > 0))
			{
				*--dst = *--src;
				len--;
			}
			aligned_dst = (FULL_TYPE*)dst;

			if(NOT_HALF_ALIGNED(src))
			{
				if (NOT_MIN_ALIGNED(src))
				{
					while (len >= BIGBLOCKSIZE)
					{
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
						len -= BIGBLOCKSIZE;
					}
	
					/* Copy one long word at a time if possible.  */
					while (len >= LITTLEBLOCKSIZE)
					{
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
						len -= LITTLEBLOCKSIZE;
					}
				}
				else
				{
					/* Copy 4X long words at a time if possible.  */
					while (len >= BIGBLOCKSIZE)
					{
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
						len -= BIGBLOCKSIZE;
					}
	
					/* Copy one long word at a time if possible.  */
					while (len >= LITTLEBLOCKSIZE)
					{
						src -= LITTLEBLOCKSIZE;
						*--aligned_dst = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
						len -= LITTLEBLOCKSIZE;
					}
				}
			}
			/* short对齐 */
			else
			{
				_CONST unsigned HALF_TYPE * half_ali_src = (unsigned HALF_TYPE *)src;
				/* Copy 4X long words at a time if possible.  */
				while (len >= BIGBLOCKSIZE)
				{
					half_ali_src -= HALF_BLOCK_SIZE;
					*--aligned_dst = MAKE_HALF_TO_FULL(half_ali_src);
					half_ali_src -= HALF_BLOCK_SIZE;
					*--aligned_dst = MAKE_HALF_TO_FULL(half_ali_src);
					half_ali_src -= HALF_BLOCK_SIZE;
					*--aligned_dst = MAKE_HALF_TO_FULL(half_ali_src);
					half_ali_src -= HALF_BLOCK_SIZE;
					*--aligned_dst = MAKE_HALF_TO_FULL(half_ali_src);
					len -= BIGBLOCKSIZE;
				}

				/* Copy one long word at a time if possible.  */
				while (len >= LITTLEBLOCKSIZE)
				{
					half_ali_src -= HALF_BLOCK_SIZE;
					*--aligned_dst = MAKE_HALF_TO_FULL(half_ali_src);
					len -= LITTLEBLOCKSIZE;
				}
				src = (char *)half_ali_src;
			}

			/* Pick up any residual with a byte copier.  */
			dst = (char*)aligned_dst;
		}
		while (len--)
		{
			*--dst = *--src;
		}
	}
	/* 正向拷贝 */
	else
	{
		/* Use optimizing algorithm for a non-destructive copy to closely 
		 match memcpy. If the size is small or either SRC or DST is unaligned,
		 then punt into the byte copy loop.  This should be rare.  */
		//		if (!TOO_SMALL(len) && !UNALIGNED (src, dst))
		if (UNALIGNED_SYNC (src, dst))
	    {
			while(NOT_ALIGNED(dst) && (len > 0))
			{
				*dst++ = *src++;
				len--;
			}
	      aligned_dst = (FULL_TYPE *)dst;
	      aligned_src = (FULL_TYPE *)src;

	     
	      /* Copy 4X long words at a time if possible.  */
	      while (len >= BIGBLOCKSIZE)
	        {
	          *aligned_dst++ = *aligned_src++;
	          *aligned_dst++ = *aligned_src++;
	          *aligned_dst++ = *aligned_src++;
	          *aligned_dst++ = *aligned_src++;
	          len -= BIGBLOCKSIZE;
	        }

	      /* Copy one long word at a time if possible.  */
	      while (len >= LITTLEBLOCKSIZE)
	        {
	          *aligned_dst++ = *aligned_src++;
	          len -= LITTLEBLOCKSIZE;
	        }
	      
	      /* len < sizeof(FULL_TYPE) */
	      
	       /* Pick up any residual with a byte copier.  */
	      dst = (CHAR_TYPE *)aligned_dst;
	      src = (CHAR_TYPE *)aligned_src;
	    }
		/* 对齐情况不同。 */
		else
		{
			/* 逐个拷贝，直至dst满足FULL_TYPE对齐 */
			while(NOT_ALIGNED(dst) && (len > 0))
			{
				*dst++ = *src++;
				len--;
			}
	      aligned_dst = (FULL_TYPE*)dst;
	      
	      /* 这里要加代码 */
	      /* 64位：非32位对齐 
	       * 32位：非16位对齐
	       */
	      if(NOT_HALF_ALIGNED(src))
	      {
	    	  /* 单个字节拷贝吧！ */
	    	  if (NOT_MIN_ALIGNED(src))
	    	  {
	    		  while (len >= BIGBLOCKSIZE)
				  {  			  
					  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  len -= BIGBLOCKSIZE;
				  }
	    		  

				/* Copy one long word at a time if possible.  */
				  while (len >= LITTLEBLOCKSIZE)
					{
					  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  len -= LITTLEBLOCKSIZE;
					}
	    	  }

	          /* Copy 4X long words at a time if possible.  */
	    	  else
	    	  {
	    		  while (len >= BIGBLOCKSIZE)
	    		  {
					  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  len -= BIGBLOCKSIZE;
	    		  }

	          /* Copy one long word at a time if possible.  */
				  while (len >= LITTLEBLOCKSIZE)
					{
					  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
					  src += LITTLEBLOCKSIZE;
					  len -= LITTLEBLOCKSIZE;
					}
	    	  }
	      }
	      else
	      {
	    	  _CONST unsigned HALF_TYPE * half_ali_src = (unsigned HALF_TYPE *)src;
	          /* Copy 4X long words at a time if possible.  */
	          while (len >= BIGBLOCKSIZE)
	            {
	              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
	              half_ali_src += HALF_BLOCK_SIZE;
	              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
	              half_ali_src += HALF_BLOCK_SIZE;
	              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
	              half_ali_src += HALF_BLOCK_SIZE;
	              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
	              half_ali_src += HALF_BLOCK_SIZE;
	              len -= BIGBLOCKSIZE;
	            }

	          /* Copy one long word at a time if possible.  */
	          while (len >= LITTLEBLOCKSIZE)
	            {
	              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
	              half_ali_src += HALF_BLOCK_SIZE;
	              len -= LITTLEBLOCKSIZE;
	            }
	          /* len < LITTLEBLOCKSIZE */
	          src = (char *)half_ali_src;
	      }

	       /* Pick up any residual with a byte copier.  */
	      dst = (char*)aligned_dst;
		}

		while (len--)
		{
			*dst++ = *src++;
		}
	}
	return dst_void;
#endif /* not PREFER_SIZE_OVER_SPEED */
}

void *io_bcopy(const void *src0, void *dst0, size_t len0)
{
	return io_memmove(dst0, src0, len0);
}
