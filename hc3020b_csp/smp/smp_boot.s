#ifdef __multi_core__
#define ASM_LANGUAGE
#include <gs464_cpu.h>
#include <mips_asm.h>

.global start_secondary
.global c_main_secondary

start_secondary: 
	.set noreorder
#if 0
	li		t0, SR_CU1|SR_CU0|SR_UX|SR_KX
	mtc0	t0, C0_SR		   /* ��CPU	*/
#endif
    	//test
#if 1
	li      v1, '3'
    dli	    v0, 0xffffffffbf050030
    sb      v1, 0(v0)
    nop
#endif

	jal	    c_main_secondary			
	nop
#endif
