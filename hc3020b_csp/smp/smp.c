#ifdef __multi_core__
#include <cpu.h>
#include <gs464_cpu.h>
#include <ipi.h>
#include <memory.h>
#include <string.h> /* memset() */
#include <bitops.h> /* ffs16() */
#include <irq.h>
#include <reworks/types.h>
#include <reworks/printk.h>
#include <reworks/sv.h>

extern void start_secondary();
extern void sysWrite32(unsigned int addr,unsigned int data);
extern unsigned int sysRead32(unsigned int addr);

#if 0
volatile void * mailbox_buf[] = {
		(void*)(IPI_BASE + IPI_CORE0_OFFSET + IPI_MAILBOX),
		(void*)(IPI_BASE + IPI_CORE1_OFFSET + IPI_MAILBOX),
		(void*)(IPI_BASE + IPI_CORE2_OFFSET + IPI_MAILBOX),
		(void*)(IPI_BASE + IPI_CORE3_OFFSET + IPI_MAILBOX),
};
int hr2_cpu_start(int cpu, void (*pc)(void), long sp, long gp, long a1){
	if((cpu >= MAX_SMP_CPUS)||(cpu < 1)){
		return 0;
	}


	sysWrite32(mailbox_buf[cpu] + 0x8,sp);
	sysWrite32(mailbox_buf[cpu] + 0x10,gp);
	sysWrite32(mailbox_buf[cpu] + 0x18,a1);
	sysWrite32(mailbox_buf[cpu] + 0x0,pc);
	return 1;
}
#endif


extern void *top_isr_stack_addr;

extern void set_cachetest(char * cache_test, char c, int size);
extern void print_cachetest(char * cache_test, char c);

#ifndef _HC3020B_
#define CORE_IPI_MAILBOX(core) (0x900000001f9304c0 +  core*64 )
#else /*ldf 20230920 add:: for hc3020b*/
#define CORE_IPI_MAILBOX(core) (0x900000001f9304b0 +  core*64 ) 
#endif

void test_slavecore()
{
	*((volatile unsigned long *)0xffffffff87d5ade0) = 0xA55BB66;
}
int _cpu_up(int cpu)
{
	printk("%s() __%d__,  cpu: %d, MAX_SMP_CPUS:%d  \r\n", __func__,__LINE__,cpu,MAX_SMP_CPUS);
	if((cpu >= MAX_SMP_CPUS)||(cpu < 1))
	{
		return 0;
	}

//	printk("%s,%d\n", __FUNCTION__, cpu);

//	printk("enter %s  \r\n", __FUNCTION__);

	//<<===========解锁================

	unsigned int val = sysRead32(0xbf0c0028);
	//printk("_cpu_up val == 0x%x  \r\n", val);
	sysWrite32(0xbf0c0028, 0xa5accede);
	// ====================>>FuKai,2021-5-24

	val = get_c0_sr();
	printk("_cpu_up get_c0_sr222() == 0x%x  \r\n", get_c0_sr());
	val |=0x34000000;
//	val |=SR_KX;
	val |=SR_UX;
//	set_c0_sr(0x340000a0);

	printk("[%d]cp0 config=0x%x\n", cpu_id_get(),get_c0_config());
	printk("[%d]cp0 sr=0x%x\n", cpu_id_get(),get_c0_sr());

	//set_cachetest(NULL, 'E',937);
	//print_cachetest(NULL, '\0');

	printk("%s, %d, pc[0x%lx]  ====== top_isr_stack_addr = 0x%lx\n", __FUNCTION__, __LINE__, start_secondary,top_isr_stack_addr);
//	HR2_cpu_start(cpu, (void*)&start_secondary, (long)(top_isr_stack_addr - (cpu<<12)), 0, 0);
	HR2_cpu_start(cpu, (void*)start_secondary, (long)(top_isr_stack_addr - (cpu<<12)), 0, 0);
//	HR2_cpu_start(cpu, (void*)test_slavecore, (long)(top_isr_stack_addr - (cpu<<12)), 0, 0);
	//printk("XXXXXX[%d]cp0 sr=0x%x\n", cpu_id_get(),*(unsigned long*)CORE_IPI_MAILBOX(1));
	return 1;
} 

int sys_per_cpu_init()
{
//	printk("cache test[%d]: %s\n", cpu_id_get(),cache_test);


//	printk("[ReWorks]:Core[%d] Init...\n\r",cpu_id_get());
	sys_cpu_init();

	u32 val = get_c0_sr();
	val |=0x34000000;
//	val |=SR_KX;
	val |=SR_UX;
	set_c0_sr(val);

	//printk("[%d]cp0 config=0x%x\n", cpu_id_get(),get_c0_config());
	//printk("[%d]cp0 sr=0x%x\n", cpu_id_get(),get_c0_sr());

//	printk("==> %s:%d \r\n",__FUNCTION__, __LINE__);
	mmu_init_per_cpu();
//	printk("==> %s:%d \r\n",__FUNCTION__, __LINE__);

	/* 0x01000000 -> fcr31，设置FS位，解决浮点转换指令处理denormalized数或下溢发生异常问题 */
//	printk("==> %s:%d \r\n",__FUNCTION__, __LINE__);

	/*
	asm(
		"li $2,0x01000000;\n" \
		"ctc1 $2,$31;\n" \
		"sync ;\n"
		:
		:
		:"$2"
	);
	*/
//	printk("==> %s:%d \r\n",__FUNCTION__, __LINE__);
	clock_module_start_per_cpu();
	//printk("==> %s:%d \r\n",__FUNCTION__, __LINE__);
//	set_c0_sr(0x340000a0);
//	print_cachetest(NULL, '\0');
}
#endif
