#define __multi_core__
#ifdef __multi_core__
#include <stdio.h>
#include <reworks/types.h>
#include <reworks/thread.h>
#include <atomic.h>
#include <cpu.h>

extern u32  sys_timestamp_freq(void);

extern u64 sys_timestamp();
extern int is_smp_init_terminated();

volatile atomic_t sync_flag = 0;
extern cpuset_t cpuset_init;

__attribute__ ((aligned(32))) static u64 t[4*4] = {0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee};

__attribute__ ((aligned(32))) static u64 syncTime[4*4] = {0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee,0xeeeeeeee};


static void systm_sync_task(void *arg)
{	

	register u32 cpu_id = cpu_id_get();
	register u32 cpu_id_value = 1 << cpu_id_get();
	printk("[%s][%d] cpuid = %d\n", __FUNCTION__, __LINE__, cpu_id);
	u32 level = int_cpu_lock();
	printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	asm volatile("sync");
	printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	if (UNLIKELY((sync_flag | cpu_id_value) == cpuset_init))
	{
		printk("[%s][%d]\n", __FUNCTION__, __LINE__);
		sync_flag |= cpu_id_value;
//		asm volatile("sync");
	}
	else
	{
		printk("[%s][%d]\n", __FUNCTION__, __LINE__);
		atomic_or(&sync_flag, cpu_id_value);
		printk("[%s][%d]sync_flag = 0x%x, cpuset_init = 0x%x\n", __FUNCTION__, __LINE__, sync_flag, cpuset_init);
		do{
				asm volatile("sync");
		}while (sync_flag != cpuset_init);
		printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	}

    
//	sys_timestamp_init(0xc000100f); /* loongson3a 1000/2000使用该值均可正常启动，如果使用0xc000000f，那么loongson3a 2000无法正常启动 */

//	 syncTime[cpu_id*4] = sys_timestamp();
	printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	sync_counter(cpu_id);
	printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	 
    int_cpu_unlock(level);
    printk("[%s][%d]\n", __FUNCTION__, __LINE__);
}

static void systm_sync_check(void *arg)
{	

	register u32 cpu_id = cpu_id_get();
	register u32 cpu_id_value = 1 << cpu_id_get();
	
	u32 level = int_cpu_lock();
	
	asm volatile("sync");
	if (LIKELY((sync_flag | cpu_id_value) == cpuset_init))
	{
		sync_flag |= cpu_id_value;
//		asm volatile("sync");
	}
	else
	{
		atomic_or(&sync_flag, cpu_id_value);
		
		do{
				asm volatile("sync");
		}while (sync_flag != cpuset_init);
	}

    
    t[cpu_id*4] = sys_timestamp();

    int_cpu_unlock(level);
}

int systm_sync()
{
    cpuset_t affinity;
    int  pri;
    pthread_t tid;
    int i;
  
    sync_flag = 0;
    

    pthread_getschedprio(pthread_self(), &pri);
    pri = pri - 1;

//    printk("cpu_up_num:%d %d\n",cpu_up_num,cpuset_init);
    printk("[%s][%d]\n", __FUNCTION__, __LINE__);
    for(i = 0; i < MAX_SMP_CPUS; i++) {    
    	if(CPUSET_ISSET(cpuset_init, i))
    	{
    		//pthread_create3(&tid, "TBSync", 255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK, 
			pthread_create3(&tid, "TBSync", 255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK,
					4096, 1<<i, systm_sync_task, 0);
			if (tid == 0) {        
				printk("taskCreate faild\n");
				goto FAILD;
			}
	
			printk("[%s][%d]\n", __FUNCTION__, __LINE__);
			pthread_detach(tid); //
    	}
    }
    printk("[%s][%d]\n", __FUNCTION__, __LINE__);
    pthread_delay(sys_clk_rate_get()/10);
    printk("[%s][%d]\n", __FUNCTION__, __LINE__);
    return 0;    
FAILD:
	return -1;
}

int cores_time_check()
{
    cpuset_t affinity;
    thread_t tid;
	int pri;
    int i;
    int cpu_up_num;
    
    sync_flag = 0;
    

    pthread_getschedprio(pthread_self(), &pri);
    pri = pri - 1;

    for(i = 0; i < MAX_SMP_CPUS; i++) {    	

    	if(CPUSET_ISSET(cpuset_init, i))
    	{
			//pthread_create3(&tid, "TBSync", 255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK, 
    	
			pthread_create3(&tid, "TBSync",255 - pri, RE_UNBREAKABLE | RE_KERNEL_TASK,
					4096, 1<<i, systm_sync_check, 0);

		
			if (tid == 0) {        
				printk("taskCreate faild\n");
				goto FAILD;
			}
	
			pthread_detach(tid); //
    	}
    }


    pthread_delay(10);
    
    u32 freq = sys_timestamp_freq();
    printf("freq = 0x%x,%d\n",freq,freq);
    
    cpu_up_num = num_online_cpus();
    
    int *p;
    for(i = 0;i < cpu_up_num; i++)
    {
//    printf("t[%d] = %lld\n", i,t[i*4]);
	  p = &t[i*4];
	  printf("t[%d] = 0x%x , 0x%x\n", i,*p,*(p+1));
//    printf("t[%d] = %fus\n", i,(float)t[i*4]*1000*1000.0/freq);//us
    }
    
 
    for(i = 0;i < cpu_up_num; i++)
	  {
//	  printf("syncTime[%d] = %lld\n", i,syncTime[i*4]);

//	  printf("syncTime[%d] = %fus\n", i,(float)syncTime[i*4]*1000*1000.0/freq);
	  }
    
    return 0;    
FAILD:
	return -1;
}

void cores_time_sync()
{
#ifndef HUARUI3_SMP_64
#error
	//FIXME: 华睿3加速器运行时会用很长时间，暂时屏蔽掉。 FuKai,2020-9-22
// by FuKai,2018-8-14 时钟同步不需要，暂时去掉
	//ww mod 20180907 add
	systm_sync();
	systm_sync();
//	cores_time_check();
#endif
}
#endif
