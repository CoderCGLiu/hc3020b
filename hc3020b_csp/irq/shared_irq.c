/******************************************************************************
 * 
 * 版权：
 *      中国电子科技集团公司第三十二研究所
 * 描述：
 *      实现了PCI设备的中断链表初始化，以及相应的中断操作
 * 修改：
 * 		
 *     
 */
#include <cpu.h>
#include <malloc.h>
#include <reworks/list.h>

#include <irq.h>

extern int is_va_valid(unsigned long address);

/* 共享中断链表节点 */
typedef struct 
{
	struct list_head	node;           	/* 双向链表节点 */
	INT_HANDLER  	 	routine;     		/* 中断处理函数 */
	void     			*parameter;      	/* 函数参数 */
} SHARED_INT_RTN;   


static struct list_head shared_int_list[IRQ_NUM];	/* 中断链表 */


/**
 * @brief 中断操作
 * 
 * 执行指定中断对应的程序
 * 
 * @return N/A
 * @note 此函数非用户可调用程序
 */
static void shared_int(int irq)
{
	struct list_head *pos;
	struct list_head *head;
    
	if( !is_irq_valid(irq) )
		return;
    
	head = &shared_int_list[irq];
	list_for_each(pos, head) {
		SHARED_INT_RTN *rtn = list_entry(pos, SHARED_INT_RTN, node);
		(* rtn->routine) (rtn->parameter);
	}
}


/**
 * @brief 挂接共享中断操作
 * 
 * 指定中断向量对应的操作
 * 
 * @param vector 中断向量
 * @param routine 对应的中断操作函数
 * @param parameter 函数参数
 * 
 * @return OS_OK 挂接成功
 * @return OS_ERROR 挂接失败
 */
int shared_int_install(int irq, INT_HANDLER handler, void *param)
{
	SHARED_INT_RTN *rtn;
	struct list_head *pos;
	struct list_head *head;
	int installed = 0;
	
	if (!is_irq_valid() || 
		!handler || !is_va_valid((unsigned long)handler))
		return OS_ERROR;
	
	head = &shared_int_list[irq];

	if( list_empty(head) ) {
		if( int_install_handler("shared",irq,1, (INT_HANDLER)shared_int, (void *)irq) != OS_OK )
			return OS_ERROR;
	}
    
	rtn = (SHARED_INT_RTN *)malloc(sizeof (SHARED_INT_RTN));
	if( !rtn )
		return OS_ERROR;
    
	memset(rtn, 0, sizeof(SHARED_INT_RTN));
	rtn->parameter = param;
	rtn->routine = handler;

	int level = int_lock ();//2012.7.10 tom<<
	list_add_tail(&rtn->node, &shared_int_list[irq]);
	int_unlock(level);//2012.7.10 tom>>
	
	return OS_OK;
}

/**
 * @brief 终止与中断的挂接
 * 
 * 移除指定共享中断向量挂接的指定实例
 * 
 * @param vector 中断向量
 * @param routine 对应的中断操作函数
 * 
 * @return OS_OK 终止成功
 * @return OS_ERROR 终止失败
 * 
 * @note 无
 */
int shared_int_uninstall(int irq, INT_HANDLER handler)
{
	struct list_head *pos;
	struct list_head *head;
    
	if(!is_irq_valid(irq) || !handler )
	{
		return OS_ERROR;
	}
	head = &shared_int_list[irq];
	
	if(head == NULL)
	{
		return -1;
	}
	list_for_each(pos, head) {
		SHARED_INT_RTN *rtn = list_entry(pos, SHARED_INT_RTN, node);
		
		if( rtn->routine == handler ) {
			list_del(&rtn->node);
			free(rtn);
			if(list_empty(head)) 
			{
				if(int_uninstall_handler(irq))
				{
					return OS_ERROR;
				}
			}
			return OS_OK;
		}
	}
	
	return OS_ERROR;
}


/**
 * @brief 初始化共享中断支持库
 * 
 * 初始化PCI中断操作链表和中断服务程序
 * 
 * @return OS_OK PCI中断支持库初始化成功
 * @return OS_ERROR PCI中断支持库初始化失败
 */
int shared_int_module_init(void)
{
	int i;
	
	for( i = 0; i < IRQ_NUM; i ++ ) {
		memset(&shared_int_list[i], 0, sizeof(struct list_head));
		INIT_LIST_HEAD(&shared_int_list[i]);
	}
    

    return OS_OK;
}
