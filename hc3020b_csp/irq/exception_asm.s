#define ASM_LANGUAGE
#define _ASMLANGUAGE
#include <cpu.h>
#include <gs464_cpu.h>
#include <mips_asm.h>
#include <asm_linkage.h>

.extern isr_common_handler
.extern refill_tlb

	.rdata
str_context_restore:
	.asciz "context_restore(): exceptional context.\n\r"
str_context_restore2:
	.asciz "context_restore(): sp.\n\r"
str_context_restore3:
	.asciz "context_restore(): a0.\n\r"
str_context_restore4:
	.asciz "context_restore(): saved sp.\n\r"
test_str:
	.asciz "cache exception\n\r"
tlbrefill_str:
	.asciz "tlbrefill exception\n\r"


/*     oldLevel = int_lock ();
*
*      ...       * work with interrupts locked out *
*
*     int_unlock (oldLevel);
*/
#ifndef __multi_core__
LEAF(int_lock)
#else
LEAF(int_cpu_lock)
#endif
	.balign 32,0x0
	
	.set noreorder

	mfc0	t0, C0_SR  /* SR -> t0 */
	li		t1, ~SR_IE /* t1 = 0xfffffffe */
    and		t2, t0, t1 /* mask off interrupt-enable bit  */
    mtc0	t2, C0_SR  /* t2 -> SR */
	li		t1, SR_IE  /* t1 = 0x00000001 in delay slot */
	sync
	and		v0, t0, t1 /* return value = t0 & t1 */
	j		ra
	nop

	.set reorder

#ifndef __multi_core__
ENDPROC(int_lock)
#else
ENDPROC(int_cpu_lock)
#endif

#ifndef __multi_core__
LEAF(int_unlock)
#else
LEAF(int_cpu_unlock)
#endif

	.set noreorder

	mfc0	t0, C0_SR  /* SR -> t0 */
    or		t0, t0, a0 /* a0等于0x00000000或 0x00000001 */
    mtc0	t0, C0_SR  /* t0 -> SR */
    nop
    sync
	j		ra
	nop

	.set reorder

#ifndef __multi_core__
ENDPROC(int_unlock)
#else
ENDPROC(int_cpu_unlock)
#endif



LEAF(int_enable)

	.set noreorder
	mfc0	t0, C0_SR  /* SR -> t0 */
	li		t1, ~(SR_IE|SR_EXL|SR_ERL)
	and		t1, t0, t1
	ori		t2, t1, SR_IE /* SR_ERL = 0, SR_EXL = 0, SR_IE = 1  */
    mtc0	t2, C0_SR  /* t2 -> SR */
	sync
	j		ra
	nop
	.set reorder

ENDPROC(int_enable)

LEAF(int_status_get)

	.set noreorder
	mfc0	v0, C0_SR /* SR -> v0 */
	j		ra
	nop
	.set reorder

ENDPROC(int_status_get)

LEAF(int_status_set)

	.set noreorder
	mtc0	a0, C0_SR /* a0 -> SR */
	sync
	j		ra
	nop
	.set reorder

ENDPROC(int_status_set)

LEAF(mips_general_exception)

	.set	noat

	dla	k0, exception_handler
	jr	k0

	.set	at
ENDPROC(mips_general_exception)

LEAF(mips_general_exception_end)
ENDPROC(mips_general_exception_end)

LEAF(mips_cache_exception)

	.set	noat

	dla	a0, test_str
	jal serial_putstr
1:
	b	1b

	.set	at
ENDPROC(mips_cache_exception)

LEAF(mips_cache_exception_end)
ENDPROC(mips_cache_exception_end)

LEAF(mips_tlbrefill_exception)

	.set	noat


	//dla     k0, tlbrefill_str
	//jal		serial_putstr

	dla	k0, tlb_refiller
	jr	k0

	.set	at
ENDPROC(mips_tlbrefill_exception)

LEAF(mips_tlbrefill_exception_end)
ENDPROC(mips_tlbrefill_exception_end)

/************************************************
 * C函数原型：void fill_exc_vec_code(void *exc_vec,
 * void *exc_handler_start, void *exc_handler_end);
 *
 * 这是一个叶子函数，它用于安装异常向量。
 ***********************************************/
LEAF(fill_exc_vec_code)

	.set noreorder
	move	t0, a0
	move	t1, a1
	move	t2, a2
1:
	beq		t1, t2, 2f
	lw		t3, (t1)
	sw		t3, (t0)
	daddiu	t1, t1, 4
	b		1b
	daddiu	t0, t0, 4
2:
	j		ra
	nop

	.set reorder
ENDPROC(fill_exc_vec_code)

LEAF(context_switch)

	.set noreorder
    mfc0	t0, C0_SR

	daddiu	t1, sp, -THR_CONTEXT_CONTROL_SIZE
    li		t2, SWITCH_TYPE
    sw		t2, (t1)
    dla		t2, context_switch
    sd		t2, PC_OFFSET    * R_SZ(t1)

	sw		t0, 4(t1)
    sd		ra, RA_OFFSET    * R_SZ(t1)
    sd		s8, FP_OFFSET    * R_SZ(t1)
    sd		sp, SP_OFFSET    * R_SZ(t1)
    sd		gp, GP_OFFSET    * R_SZ(t1)
    sd		s7, S7_OFFSET    * R_SZ(t1)
    sd		s6, S6_OFFSET    * R_SZ(t1)
    sd		s5, S5_OFFSET    * R_SZ(t1)
    sd		s4, S4_OFFSET    * R_SZ(t1)
    sd		s3, S3_OFFSET    * R_SZ(t1)
    sd		s2, S2_OFFSET    * R_SZ(t1)
    sd		s1, S1_OFFSET    * R_SZ(t1)
    sd		s0, S0_OFFSET    * R_SZ(t1)

    sd      t1, (a0)
    b		context_restore
    move	a0, a1
	.set reorder

	.set noreorder
1:
	dla		a0, test_str
	jal		serial_putstr
2:	nop
	b		2b
	nop
	.set reorder
ENDPROC(context_switch)

LEAF(exception_handler)
	.set	noat

	daddi	k0, sp, -INT_CONTEXT_CONTROL_SIZE
	sd		AT, INT_AT       * R_SZ(k0)

	li		AT, INTERRUPT_TYPE
	sw		AT, (k0)
	mfc0	AT, C0_SR
	sw		AT, 4(k0)
	dmfc0	AT, C0_EPC
	sd		AT, PC_OFFSET    * R_SZ(k0)
	sd		ra, RA_OFFSET    * R_SZ(k0)
    sd		s8, FP_OFFSET    * R_SZ(k0)
    sd		sp, SP_OFFSET    * R_SZ(k0)
    sd		gp, GP_OFFSET    * R_SZ(k0)
    sd		s7, S7_OFFSET    * R_SZ(k0)
    sd		s6, S6_OFFSET    * R_SZ(k0)
    sd		s5, S5_OFFSET    * R_SZ(k0)
    sd		s4, S4_OFFSET    * R_SZ(k0)
    sd		s3, S3_OFFSET    * R_SZ(k0)
    sd		s2, S2_OFFSET    * R_SZ(k0)
    sd		s1, S1_OFFSET    * R_SZ(k0)
    sd		s0, S0_OFFSET    * R_SZ(k0)
	sd		v0, INT_V0       * R_SZ(k0)
	sd		v1, INT_V1       * R_SZ(k0)
	sd		a0, INT_A0       * R_SZ(k0)
	sd		a1, INT_A1       * R_SZ(k0)
	sd		a2, INT_A2       * R_SZ(k0)
	sd		a3, INT_A3       * R_SZ(k0)
	sd		t0, INT_T0       * R_SZ(k0)
	sd		t1, INT_T1       * R_SZ(k0)
	sd		t2, INT_T2       * R_SZ(k0)
	sd		t3, INT_T3       * R_SZ(k0)
	sd		t4, INT_T4       * R_SZ(k0)
	sd		t5, INT_T5       * R_SZ(k0)
	sd		t6, INT_T6       * R_SZ(k0)
	sd		t7, INT_T7       * R_SZ(k0)
	sd		t8, INT_T8       * R_SZ(k0)
	sd		t9, INT_T9       * R_SZ(k0)
	sd		k1, INT_K1       * R_SZ(k0)

	.set	at

	mfhi	v0
	mflo	v1
	sd		v0, INT_MULHI    * R_SZ(k0)
	sd		v1, INT_MULLO    * R_SZ(k0)
#ifdef __multi_core__
    .set 	mips64
	mfc0    v0,$15,1
	//andi    v0, 0x3ff
	//and     v0,0x3
	//ls dual3a
	and     v0,0x7
	sll     v0,3
	
	dla		k1, unsave_thread_esp_smp
    daddu    k1,k1,v0
#else
	dla		k1, unsave_thread_esp
#endif

	sd		k0, (k1)     //k1 要8字节对齐

    dla		t1, temp_status
    sd		k0, (t1)

	mfc0	a0, C0_CAUSE
	andi	t1, a0, 0x7c
	srl	    t1, t1, 0x2
	beqz	t1, 0f
//	move    a0, t1
	nop
	daddiu   sp, sp, - INT_CONTEXT_CONTROL_SIZE - 32
	jal		reworks_exception_handler
	/* never return */ 
#ifdef __multi_core__
0:	
	.set noreorder
	.set 	mips64
	mfc0    v0,$15,1
//	andi    v0, 0x3ff
//	and     v0,0x3
//	ls dual3a
	and     v0,0x7
//	.set    mips3


	/*判断中断是否有嵌套来决定是否要进行堆栈切换*/
	sll     v0,2 	/*注意isr_nest_level_smp的类型是u32，相邻元素间地址差值为4*/
	
	dla		k1, isr_nest_level_smp
    daddu    k1,k1,v0
	lw		k0, (k1)
	bnez    k0,1f
	nop
	
	
	/*主核的中断栈地址为top_isr_stack_addr，为每个核分配大小为4096字节的中断栈，
	在这里通过cpu号计算具体的sp值，保证每个核各自不同的中断栈地址*/

	sll     v0, 10
	move    k1,v0
	ld		k0, top_isr_stack_addr
	dsubu	k0,k0, k1
	
//	bne		sp, k0, 1f
	move	sp, k0		/* Get a new stack */
	
	.set reorder
1:
	daddiu   sp, sp, - INT_CONTEXT_CONTROL_SIZE - 32
	dla		gp, _gp
#else
0:	
	.set noreorder
	ld		k0, top_isr_stack_addr
	bne		sp, k0, 1f
	move	sp, k0		/* Get a new stack */
	daddiu   sp, sp, - INT_CONTEXT_CONTROL_SIZE - 32
	.set reorder
1:
	dla		gp, _gp
#endif
	jal		isr_common_handler
	/* never return */
ENDPROC(exception_handler)

LEAF(context_restore)
	.set noreorder
	#if 0
	dli		t0, 0xffffffff801f0000
	dli		t1, 0xffffffff8fffffff

	blt		sp, t0, 2f
	nop
	bgt		sp, t1, 2f
	nop
	andi	t2, sp, 0x7
	bne		t2, zero, 2f
	nop

	blt		a0, t0, 3f
	nop
	bgt		a0, t1, 3f
	nop
	andi	t2, a0, 0x3
	bne		t2, zero, 3f
	nop

	ld		t3, SP_OFFSET    * R_SZ(a0)
	blt		t3, t0, 4f
	nop
	bgt		t3, t1, 4f
	nop
	andi	t2, t3, 0x7
	bne		t2, zero, 4f
	nop
#endif
	lw		t0, (a0)
	li		t1, INTERRUPT_TYPE
	beq		t0, t1, i_restore
	move	k0, a0
	li		t2, SWITCH_TYPE
	beq		t0, t2, c_restore
	nop
	move       a1,t0
	jal        exp_print
1:	nop
	b		1b
	nop
2:	dla		a0, str_context_restore2
	jal		serial_putstr
	nop
    b		1b
    nop
3:	dla		a0, str_context_restore3
	jal		serial_putstr
	nop
    b		1b
    nop
4:	jal 	serial_puthex
	nop
	move    a0, t3
	jal 	serial_puthex
	nop
	dla		a0, str_context_restore4
	jal		serial_putstr
	nop
	move    a0, ra
	move    a1, sp
	move    a2, ra
	jal     backtrace_stack
	nop
    b		1b
    nop
	.set reorder
	
ENDPROC(context_restore)

c_restore:
	.set noreorder
    ld		s0, S0_OFFSET    * R_SZ(a0)
    ld		s1, S1_OFFSET    * R_SZ(a0)
    ld		s2, S2_OFFSET    * R_SZ(a0)
    ld		s3, S3_OFFSET    * R_SZ(a0)
    ld		s4, S4_OFFSET    * R_SZ(a0)
    ld		s5, S5_OFFSET    * R_SZ(a0)
    ld		s6, S6_OFFSET    * R_SZ(a0)
    ld		s7, S7_OFFSET    * R_SZ(a0)
    ld		gp, GP_OFFSET    * R_SZ(a0)
    ld		sp, SP_OFFSET    * R_SZ(a0)
    ld		s8, FP_OFFSET    * R_SZ(a0)
    ld		ra, RA_OFFSET    * R_SZ(a0)		/* restore context */
    lw		t0, 4(a0)

	mtc0	t0, C0_SR	/* and load the new SR */
	nop
	sync

	j		ra
	nop
	.set reorder

i_restore:

	ld		v1, INT_MULLO * R_SZ(k0)
	ld		v0, INT_MULHI * R_SZ(k0)
	mthi	v0
	mtlo	v1

	.set	noat
	ld		k1, INT_K1       * R_SZ(k0)
	ld		t9, INT_T9       * R_SZ(k0)
	ld		t8, INT_T8       * R_SZ(k0)
	ld		t7, INT_T7       * R_SZ(k0)
	ld		t6, INT_T6       * R_SZ(k0)
	ld		t5, INT_T5       * R_SZ(k0)
	ld		t4, INT_T4       * R_SZ(k0)
	ld		t3, INT_T3       * R_SZ(k0)
	ld		t2, INT_T2       * R_SZ(k0)
	ld		t1, INT_T1       * R_SZ(k0)
	ld		t0, INT_T0       * R_SZ(k0)
	ld		a3, INT_A3       * R_SZ(k0)
	ld		a2, INT_A2       * R_SZ(k0)
	ld		a1, INT_A1       * R_SZ(k0)
	ld		a0, INT_A0       * R_SZ(k0)
	ld		v1, INT_V1       * R_SZ(k0)
	ld		v0, INT_V0       * R_SZ(k0)
    ld		s0, S0_OFFSET    * R_SZ(k0)
    ld		s1, S1_OFFSET    * R_SZ(k0)
    ld		s2, S2_OFFSET    * R_SZ(k0)
    ld		s3, S3_OFFSET    * R_SZ(k0)
    ld		s4, S4_OFFSET    * R_SZ(k0)
    ld		s5, S5_OFFSET    * R_SZ(k0)
    ld		s6, S6_OFFSET    * R_SZ(k0)
    ld		s7, S7_OFFSET    * R_SZ(k0)
    ld		gp, GP_OFFSET    * R_SZ(k0)
    ld		sp, SP_OFFSET    * R_SZ(k0)
    ld		s8, FP_OFFSET    * R_SZ(k0)
	ld		ra, RA_OFFSET    * R_SZ(k0)

	ld		AT, PC_OFFSET    * R_SZ(k0)
	dmtc0	AT, C0_EPC
	lw		AT, 4(k0)
	mtc0	AT, C0_SR
	ld		AT, INT_AT       * R_SZ(k0)
	sync

	eret
	.set	at

LEAF(tlb_refiller)
	.set	noat

#ifdef __multi_core__
	daddi	k0, sp, -INT_CONTEXT_CONTROL_SIZE
	sd		AT, INT_AT       * R_SZ(k0)
#else
	dli		k0, 0xffffffff80100000 - INT_CONTEXT_CONTROL_SIZE
	sd		AT, INT_AT       * R_SZ(k0)
#endif
	li		AT, INTERRUPT_TYPE
	sw		AT, (k0)
	mfc0	AT, C0_SR
	sw		AT, 4(k0)
	dmfc0	AT, C0_EPC
	sd		AT, PC_OFFSET    * R_SZ(k0)
	sd		ra, RA_OFFSET    * R_SZ(k0)
    sd		s8, FP_OFFSET    * R_SZ(k0)
    sd		sp, SP_OFFSET    * R_SZ(k0)
    sd		gp, GP_OFFSET    * R_SZ(k0)
    sd		s7, S7_OFFSET    * R_SZ(k0)
    sd		s6, S6_OFFSET    * R_SZ(k0)
    sd		s5, S5_OFFSET    * R_SZ(k0)
    sd		s4, S4_OFFSET    * R_SZ(k0)
    sd		s3, S3_OFFSET    * R_SZ(k0)
    sd		s2, S2_OFFSET    * R_SZ(k0)
    sd		s1, S1_OFFSET    * R_SZ(k0)
    sd		s0, S0_OFFSET    * R_SZ(k0)
	sd		v0, INT_V0       * R_SZ(k0)
	sd		v1, INT_V1       * R_SZ(k0)
	sd		a0, INT_A0       * R_SZ(k0)
	sd		a1, INT_A1       * R_SZ(k0)
	sd		a2, INT_A2       * R_SZ(k0)
	sd		a3, INT_A3       * R_SZ(k0)
	sd		t0, INT_T0       * R_SZ(k0)
	sd		t1, INT_T1       * R_SZ(k0)
	sd		t2, INT_T2       * R_SZ(k0)
	sd		t3, INT_T3       * R_SZ(k0)
	sd		t4, INT_T4       * R_SZ(k0)
	sd		t5, INT_T5       * R_SZ(k0)
	sd		t6, INT_T6       * R_SZ(k0)
	sd		t7, INT_T7       * R_SZ(k0)
	sd		t8, INT_T8       * R_SZ(k0)
	sd		t9, INT_T9       * R_SZ(k0)
	sd		k1, INT_K1       * R_SZ(k0)

	.set	at

	mfhi	v0
	mflo	v1
	sd		v0, INT_MULHI    * R_SZ(k0)
	sd		v1, INT_MULLO    * R_SZ(k0)

#ifdef __multi_core__
    .set 	mips64
	mfc0    v0,$15,1
	//andi    v0, 0x3ff
	//and     v0,0x3
	//ls dual3a
	and     v0,0x7
	sll     v0,3
	
	dla		k1, unsave_thread_esp_smp
    daddu    k1,k1,v0
	sd		k0, (k1)

#else
	dla		k1, unsave_thread_esp
	sd		k0, (k1)

	ld		sp, top_isr_stack_addr
#endif
	dla		gp, _gp
#ifdef __multi_core__
	nop
	daddiu   sp, sp, - INT_CONTEXT_CONTROL_SIZE - 32
#endif
	jal		refill_tlb
	/* never return */
ENDPROC(tlb_refiller)
