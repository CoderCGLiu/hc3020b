/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了 loongson 体系结构下的中断框架
 * 修改：
 *       2012-04-13，取消中断任务，改在唯一的中断栈上处理中断
 *       2011-11-02，修改硬件中断操作注册方式
 * 		 2011-09-30，规范
 */

#include <string.h>
#include <irq.h>
#include <errno.h>

#include <bsp_int.h>

extern int is_va_valid(unsigned long address);
#define LSN2ECPCI_IRQ_START	0
#include <arch_irq.h>
//#define IRQ_NUM				66

u32 _stack_size = 0x0;
u64 top_isr_stack_addr = 0;
/**
 * BSP中断信息表，用于管理中断（PIC、PCI等）信息
 * 这些信息主要包括：ISR、ISR参数。 
 */
struct irq_data rtos_irqs[IRQ_NUM];

/*==============================================================================
 * 
 * 全局变量
 */
/* BSP硬件中断操作函数指针 */
INT_INIT_FUNPTR	bsp_init_handler = NULL;
INT_ACK_FUNCPTR	bsp_ack_handler = NULL;
INT_ENBALE_FUNCPTR	bsp_enable_handler = NULL;
INT_DISBALE_FUNCPTR	bsp_disable_handler = NULL;
INT_SPECIAL_FUNCPTR	bsp_special_handler = NULL ;
INT_INUM_FUNCPTR bsp_inum_handler = NULL;



/*
 * 返回中断嵌套深度计数
 */
u32 int_count(void)
{
   return isr_nest_level();
}

/*==============================================================================
 * 
 * 中断处理函数安装和卸载，中断使能和屏蔽的API。
 */
/******************************************************************************
 * 
 * 安装中断处理函数
 * 
 * 	输入：
 * 		vecnum：中断号，实际安装的中断号为相对量（相对于0x20）
 * 		handler：ISR
 * 		param：ISR参数
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK，失败返回OS_ERROR 
 * 备注：
 * 		无
 */
int int_install_handler(char *name,
							  int vecnum,          /* 中断号 */
							  int pri,
                              INT_HANDLER handler,  /* ISR */
                              void *param)          /* ISR参数 */
{
    u32 level;
    struct irq_data *data;
#if defined(LOONGSON3A_SMP) || defined(HUARUI2_SMP_64) || defined(HUARUI3_SMP_64)//ww mod 20180726 mod LOONGSON3A_SMP_64->HUARUI2_SMP_64
    if( isr_nest_level() || !is_irq_valid(vecnum))
#elif defined(LOONGSON2H)
    if( isr_nest_level() || vecnum < CLOCK_IRQ/*LSN2ECPCI_IRQ_START*/ || vecnum > IRQ_NUM/*LSN2ECPCI_IRQ_END*/ )
#endif
    {
    	errno = EINVAL; //lijuan for zfml 2019.09.20
        return EINVAL;
    }
 
    if (!handler  || !is_va_valid((unsigned long)handler) || 
    	!name || !is_va_valid((unsigned long)name) ||
    	pri <0 || pri >31)
    {
    	errno = EINVAL;  //lijuan for zfml 2019.09.20
        return EINVAL;
    }

    data = &rtos_irqs[vecnum];
    if( data->handler )
    {
    	errno = EINVAL;  //lijuan for zfml 2019.09.20
    	return EINVAL;
    }

	level = int_lock();
    
	data->handler = handler;
	data->param = param;
	data->name = name;
	data->pri = pri;
	
	int_unlock(level);
	
	return OS_OK;

}


/******************************************************************************
 * 
 * 卸载中断处理函数
 * 
 * 	输入：
 * 		vecnum：中断号，实际安装的中断号为相对量（相对于0x20）
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK，失败返回OS_ERROR 
 * 备注：
 * 		无
 */
int int_uninstall_handler(int vecnum)
{
    u32	level;
    struct irq_data *data;  /* 要修改的软件中断向量表项 */

	/* 检查中断向量号是否合法 */
#if defined(LOONGSON3A_SMP) || defined(HUARUI2_SMP_64) || defined(HUARUI3_SMP_64)//ww mod 20180726 mod LOONGSON3A_SMP_64->HUARUI2_SMP_64
    if( isr_nest_level() || !is_irq_valid(vecnum))
#elif defined(LOONGSON2H)
    if( isr_nest_level() || vecnum < CLOCK_IRQ/*LSN2ECPCI_IRQ_START*/ || vecnum > IRQ_NUM/*LSN2ECPCI_IRQ_END*/ )
#endif
    {
    	errno = EINVAL; //lijuan for zfml 2019.09.20
		return OS_ERROR;
    }
		
    int_disable_pic(vecnum);
    
    data = &rtos_irqs[vecnum];
    
    level = int_lock();
    
	if(data->handler == NULL)
	{
		int_unlock(level);
    	errno = EINVAL; //lijuan for zfml 2019.09.20
		return -1;
	}
	data->handler = NULL;
	data->param = NULL;
	data->name =  NULL;
	data->pri = 0;
	
    int_unlock(level);

    return OS_OK;
}


/******************************************************************************
 * 
 * 初始化中断控制器
 * 
 */
void int_init()
{
	if( bsp_init_handler != NULL )
		(*bsp_init_handler)();
	else 
		printk("hw_intr_init_ptr ==NULL!\n");
}

/*
 * 中断响应，调用硬件中断响应接口
 */
int int_ack(int irq)
{
	if( bsp_ack_handler != NULL) 
		return (*bsp_ack_handler)(irq);
	else 
		printk("int_ack ptr = null \n\r");
	
	return 0;
}


/*******************************************************************************
 * 
 * 中断使能接口
 * 		
 * 		本程序用于使能指定的中断号；本程序为应用编程接口，可提供给应用程序或驱动编程
 * 使用。本程序只负责调用可编程中断控制器PIC的中断使能操作即可，该中断使能操作在PIC
 * 初始化时注册到中断框架，并作为内部函数指针保存在本文件中。
 * 
 * 输入：
 * 		irq：中断向量号
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR 
 */
int int_enable_pic(u32 irq)
{
	if( bsp_enable_handler != NULL) 
		(*bsp_enable_handler)( irq);
	else 
		printk("int_enable_pic = null \n\r");
	
	return OS_OK;
}


/*******************************************************************************
 * 
 * 中断屏蔽接口
 * 		
 * 		本程序用于屏蔽指定的中断号；本程序为应用编程接口，可提供给应用程序或驱动编程
 * 使用。本程序只负责调用可编程中断控制器PIC的中断屏蔽操作即可，该中断屏蔽操作在PIC
 * 初始化时注册到中断框架，并作为内部函数指针保存在本文件中。
 * 
 * 输入：
 * 		irq：中断向量号
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR 
 */
int int_disable_pic(u32 irq)
{
	if( bsp_disable_handler != NULL) 
		 (*bsp_disable_handler)(irq);
	else 
		printk("int_disable_pic = null \n\r");
	
	return OS_OK;
}


/*******************************************************************************
 * 
 * 中断屏蔽接口
 * 		
 * 		本程序用于屏蔽指定的中断号；本程序为应用编程接口，可提供给应用程序或驱动编程
 * 使用。本程序只负责调用可编程中断控制器PIC的中断屏蔽操作即可，该中断屏蔽操作在PIC
 * 初始化时注册到中断框架，并作为内部函数指针保存在本文件中。
 * 
 * 输入：
 * 		irq：中断向量号
 * 输出：
 * 		无
 * 返回：
 * 		
 */
int int_inum(u32 interrupt_pending, u32 irq_array[], u32 irq_num)
{
	if( bsp_inum_handler )
		return (*bsp_inum_handler)(interrupt_pending, irq_array, irq_num);
	
	return -1;
}


/* 硬件中断操作注册接口 */
int int_operations_register(struct int_operations *ops)
{
	bsp_init_handler = ops->init;
	bsp_ack_handler = ops->ack;
	bsp_enable_handler = ops->enable;//bsp_int_enable=>hr3_int_enable
	bsp_disable_handler = ops->disable;//bsp_int_disable=>hr3_int_disable
	bsp_inum_handler = ops->inum;
	bsp_special_handler = ops->special1 ;
	
	return 0;
}

