/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了LOONGSON2F 64位体系结构下的异常分析模块模块
 * 修改：
 * 		 2013-01-06，唐立三，建立
 */
#include <reworks/types.h>
#include <reworks/printk.h>
#include <reworks/thread.h>
#include <pthread.h>
#include <stdio.h>
#include <cpu.h>
#include <irq.h>
//#include <io_asm.h>
#include <string.h>
#include <irq.h>
#include <gs464_cpu.h>
#include <fpu.h>
#include <bd_opt.h>
#include <exception.h>
#include <exceptionp.h>
#include <reworks/list.h>

/**
 * 模块初始化标识
 */
static int exc_analysis_module_init_flag = 0;

/**
 * 异常自动处理标识
 */
static int exc_auto_handling_flag = 0;

/**
 * 异常上下文链表
 */
static struct list_head exc_internal_msg_head;

/**
 * 异常类型结构体定义
 */
struct exc_record
{
	struct list_head			head;
	struct exc_internal_msg		imsg;
};

/**
 *  任务类型
 */
extern task_type_t vx_type_id;
extern task_type_t pthread_type_id;

/**
 * 模块初始化检测
 */
#define EXC_ANALYSIS_INIT_CHECK(retn)	\
	do{	\
		if (!exc_analysis_module_init_flag)	\
		{	\
			return retn;	\
		}\
	}while(0);

/*******************************************************************************
 * 
 * 获取异常记录接口
 * 		
 * 
 * 输入：
 * 		msg：异常消息
 * 输出：
 * 		无
 * 返回：
 * 		成功返回异常消息；失败返回NULL
 */
static struct exc_record *exc_record_get(struct exc_report_msg *msg)
{
	struct exc_record *record;
	struct list_head *pos;
	
	/* 获取异常上下文 */
	list_for_each(pos, &exc_internal_msg_head)
	{
		record = (struct exc_record *)pos;
		if (msg->tid == record->imsg.emsg.tid)
		{
			return record;
		}
	}
	return NULL;
}


/*******************************************************************************
 * 
 * 异常消息分析
 * 
 * 		本接口用于将指定任务的异常信息打印出来。本接口首先根据异常消息中的任务号获取
 * 异常上下文，然后打印异常信息
 * 
 * 输入：
 * 		msg：异常消息
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_display(struct exc_report_msg *msg)
{
	struct exc_record *record;
	
	EXC_ANALYSIS_INIT_CHECK(-1);
	
	/* 获取异常记录 */
	if (NULL == (record = exc_record_get(msg)))
	{			
	    return -1;	
	}
	
    /* 显示上下文内容 */
	extern void backtrace_stack(u64 pc, u64 sp,  u64 ra);    //lvchen  
	backtrace_stack(record->imsg.context.pc, 
					record->imsg.context.sp, 
					record->imsg.context.ra);
	
	/* 自动处理异常 */
	if (exc_auto_handling_flag)
	{
		list_del_init(&record->head);
		free(record);			
	}
	
	return 0;	
}


/*******************************************************************************
 * 
 * 删除异常消息
 * 
 * 		本接口用于从异常消息队列中删除异常消息。
 * 
 * 输入：
 * 		msg：异常消息
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_report_msg_del(struct exc_report_msg *msg)
{
	struct exc_record *record;
	
	EXC_ANALYSIS_INIT_CHECK(-1);
	
	/* 获取异常记录 */
	if (NULL == (record = exc_record_get(msg)))
	{			
	    return -1;	
	}
			
	/* 删除异常信息 */
	list_del_init(&record->head);
	free(record);
	
	return 0;
}


/*******************************************************************************
 * 
 * 异常消息添加接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_internal_msg_store(struct exc_internal_msg *msg)
{
	struct exc_record *record;
	
	if (!exc_analysis_module_init_flag)
	{
		return -1;
	}
	
	if (NULL == (record = (struct exc_record *)malloc(sizeof(struct exc_record))))
	{
		return -1;
	}
	memset(record, 0, sizeof(struct exc_record));

	INIT_LIST_HEAD(&record->head);
	
	/* 保存异常消息 */	
	memcpy(&record->imsg, msg, sizeof(struct exc_internal_msg));
	
	/* 加入链表 */
	list_add(&record->head, &exc_internal_msg_head);
	
	return 0;
}


/*******************************************************************************
 * 
 * 异常消息自动删除接口
 * 
 * 		异常消息自动删除接口用于设置：是否在“异常分析接口”调用之后，自动删除被分析的
 * 异常信息 、自动删除被分析的任务
 * 
 * 输入：
 * 		flag：是否自动删除标识：1-自动删除；0-不自动删除
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_auto_handling(int flag)
{
	if ((0 != flag) && (1 != flag))
	{
		return -1;
	}
	
	exc_auto_handling_flag = flag;
	
	return 0;
}


/*******************************************************************************
 * 
 * 异常分析模块初始化接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_analysis_module_init(void)
{
	/* 检查是否已初始化 */
	if (exc_analysis_module_init_flag)
	{
		return 0;
	}
	
	/* 初始化异常消息链表 */
	INIT_LIST_HEAD(&exc_internal_msg_head);
	
	/* 注册异常上下文保存钩子 */
	exc_handler_register(exc_internal_msg_store);	
	
	/* 设置已经初始化 */
	exc_analysis_module_init_flag = 1;
	
	return 0;
}

