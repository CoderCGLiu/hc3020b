/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了MIPS体系结构下的异常 显示模块
 */
#include <reworks/types.h>
#include <reworks/printk.h>
#include <reworks/thread.h>
#include <mqueue.h>
#include <pthread.h>
#include <cpu.h>
#include <irq.h>
#include <exception.h>
#include <exceptionp.h>
#include <gs464_cpu.h>
#include <exc.h>
#include <exccode.h>

/**
 * 异常任务模块初始化标识
 */
static int exc_task_module_init_flag = 0;

/**
 * 异常管理互斥量
 */
static pthread_mutex_t exc_mutex;

/**
 * 寄存器组内容显示标识
 */
int exception_registers_display = 1;


/* 定义消息优先级 */
#define EXC_MSGS_PRIO					31

/* 异常模块最大消息数 */
#define EXC_TASK_MAX_MSGS				32	

/* 异常模块消息队列ID */
static mqd_t exc_task_mq_id = 0;	

/* 异常模块2线程ID */
pthread_t exc2_task_id;

/*fpcr全局变量*/
extern u32 fp_csr;
/**
 * 异常钩子列表
 */
static EXC_HANDLER_TYPE exc_handler_array[EXC_MAX_HANDLER]; 

/**
 * 龙芯3A支持的异常信息
 */
static char *exc_infos [EXC_TOTAL] =
{
    "Non operational vector",
    "Tlb Modify Exception",
    "Tlb Load Exception",
    "Tlb Store Exception",
    "Address load Exception",
    "Address store Exception",
    "Instruction bus error",
    "Data bus error",
    "Syscall Exception",
    "Breakpoint Exception",
    "Reserved Instruction Exception",
    "Co-processor unusable Exception",
    "Overflow Exception",
    "Trap Exception",
    "Reserved 14 Exception",
    "Floating Point Exception",
    "Reserved 16 Exception",
    "Reserved 17 Exception",
    "Reserved 18 Exception",
    "Reserved 19 Exception",
    "Reserved 20 Exception",
    "Reserved 21 Exception",
    "Reserved 22 Exception",
    "Watch Exception",
    "Reserved 24 Exception",
    "Reserved 25 Exception",
    "Reserved 26 Exception",
    "Reserved 27 Exception",
    "Reserved 28 Exception",
    "Reserved 29 Exception",
    "Reserved 30 Exception",
    "Reserved 31 Exception",
    "Integer Divide by Zero",
    "Integer Overflow",
    "unimplemented FPA operation",
    "invalid FPA operation",
    "FPA div by zero",
    "FPA overflow exception",
    "FPA underflow exception",
    "FPA inexact operation",
    "non-cpu bus error interrupt",
    "Undefined Exception"
 };

void exception_name_print(unsigned int exc_code)
{
	printk("EXCEPTION NAME: %s\n", exc_infos[exc_code]);
}
/*******************************************************************************
 * 
 * 异常钩子初始化接口
 * 
 * 输入： 
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
static int exc_handler_init(void)
{
	int 	i;
	
	/* 清空钩子列表 */
	for (i = 0; i < EXC_MAX_HANDLER; i++)
	{
		exc_handler_array[i] = 0;
	}
	
	return 0;
}


/*******************************************************************************
 * 
 *  注册异常钩子接口
 * 
 * 输入： 
 * 		handler：异常钩子
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_handler_register(EXC_HANDLER_TYPE handler)
{	
	int i;
	
	(handler) ? ({;}) : ({return -1;});
	
	pthread_mutex_lock2(&exc_mutex, WAIT, NO_TIMEOUT);
	
	/* 检查钩子是否已注册 */
	for (i = 0; i < EXC_MAX_HANDLER; i++)
	{
		if (handler == exc_handler_array[i])
		{	
			pthread_mutex_unlock(&exc_mutex);
			return 0;
		}
	}
	
	/* 查找空闲位置 */
	for (i = 0; i < EXC_MAX_HANDLER; i++)
	{
		if (0 == exc_handler_array[i])
		{
			exc_handler_array[i] = handler;
			pthread_mutex_unlock(&exc_mutex);
			
			return 0;
		}
	}
	
	pthread_mutex_unlock(&exc_mutex);	
	
	printk(PRINT_EXCEPTION_CODE"\n", C_EXCEPTION_HANDLER_IS_OVERFLOW);
	
	return -1;	
}


/*******************************************************************************
 * 
 *  注销异常钩子接口
 * 
 * 输入： 
 * 		handler：异常钩子
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_handler_unregister(EXC_HANDLER_TYPE handler)
{
	int i;
	
	(handler) ? ({;}) : ({return -1;});
	
	pthread_mutex_lock2(&exc_mutex, WAIT, NO_TIMEOUT);
	
	for (i = 0; i < EXC_MAX_HANDLER; i++)
	{
		if (handler == exc_handler_array[i])
		{
			exc_handler_array[i] = 0;
			pthread_mutex_unlock(&exc_mutex);
			
			return 0;
		}
	}
	
	pthread_mutex_unlock(&exc_mutex);	
	
	printk(PRINT_EXCEPTION_CODE"\n", C_EXCEPTION_HANDLER_CANNOT_FOUND);
	
	return -1;	
}

/*******************************************************************************
 * 
 * 异常模块2发送消息线程
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static void exc_msg_recv(Context_Ctrl *context, pthread_t tid, int exc_code, int cpu_id, u64 badvaddr, u32 cause)
{
	struct exc_internal_msg imsg;
	
	/* 异常消息内容 */
	memset(&imsg, 0, sizeof(struct exc_internal_msg));
	imsg.emsg.tid 	= tid;
	imsg.emsg.excno	= exc_code;
	imsg.context 	= *context;
	imsg.badvaddr	= badvaddr;
	imsg.cause_reg  = cause;
	imsg.emsg.cpu   = cpu_id;
	
	/* 发送消息 */
	mq_send(exc_task_mq_id, (char *)&imsg, sizeof(imsg), EXC_MSGS_PRIO);
	
	return;
}


/*******************************************************************************
 * 
 * 异常任务模块处理接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void exc_task(void)
{
	struct exc_internal_msg 	imsg;		/* 内部异常消息 */
    int 						i;

    /* 执行while(1)循环，等待异常消息 */
    while (1)
	{
    	/* 接收消息 */
    	if (mq_receive(exc_task_mq_id, (char *)&imsg, sizeof(imsg), (int *)0) == -1)
		{
			continue;	/* 接收失败则继续下次接收 */
		}
    	
    	/* 执行注册的钩子 */
    	for (i = 0; i < EXC_MAX_HANDLER; i++)
    	{
    		if (exc_handler_array[i])
    		{
    			exc_handler_array[i]((struct exc_report_msg *)&imsg);
    		}
    	}
	}

    return;
}


/*******************************************************************************
 * 
 * 异常显示钩子
 * 
 * 输入：
 * 		msg：内部异常消息
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exc_disp_handler(struct exc_report_msg *msg)
{
	struct exc_internal_msg *imsg = (struct exc_internal_msg *)msg;
	Exc_Regs regs;
	
	if ((exception_options & EXC_PRINT) == 0)
	{
		if (exception_options & EXC_ACCESS_ADDR)
		{
			exception_options &= ~EXC_ACCESS_ADDR;
		}
		
		return 0;
	}
	
	exc_context_get((void *)msg->tid, &regs);
	
	/* 打印异常头 */
//	EXC_PRINT_HEADER(msg->tid, msg->excno, msg->cpu);
	printk("\n");
	printk("EXCEPTION NAME: %s\n", exc_infos[msg->excno]);
	printk("Exception Program Counter: 0x%016x\n",regs.reg_context_gp.pc);
	printk("Status Register: 0x%08x\n",regs.reg_context_gp.c0_sr);
	printk("Cause Register:  0x%08x\n",regs.reg_val_cause);
	
	/* 打印任务上下文 */
	if (exception_options & EXC_REG_DISPLAY)
	{
		/* skip exception的情况下单次异常的寄存器组没有单独保存，
		 * 可能被后面的异常覆盖 */
		printk("\n\t---------------Registers Content--------------\n");
		context_dump(&regs.reg_context_gp);
		
		if (msg->excno == EXC_CODE_FPE)
		{
			fpu_context_dump(&regs.reg_context_fp);
		}
	}
	
	/* 打印错误地址 */
	if (exception_options & EXC_ACCESS_ADDR)
	{
		printk("Access Address:  0x%016x\n", regs.reg_val_badaddr);
		exception_options &= ~EXC_ACCESS_ADDR;
	}
	char *ptr_name = thread_name((thread_t)msg->tid);
	printk("\n(0x%08x %s):Task 0x%08x on Cpu %d has been suspended\n",
			                   msg->tid,
			                   (ptr_name == NULL)?"Unknown":ptr_name,
			                   msg->tid,
			                   msg->cpu );
	
	/* 回溯任务栈 */
	if (exception_options & EXC_BACK_TRACE)
	{
		printk("\n---------------Stack Trace--------------\n");
		extern void backtrace_stack(u32 pc, u32 sp,  u32 ra);
		backtrace_stack(regs.reg_context_gp.pc,
				regs.reg_context_gp.sp,
				regs.reg_context_gp.ra);
	}
	
	return 0;
}


/*******************************************************************************
 * 
 * 获取异常任务ID
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回异常任务ID
 */
pthread_t exc_task_id_get(void)
{
	return exc2_task_id;
}


/*******************************************************************************
 * 
 * 初始化异常任务模块
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR
 * 备注：
 * 		异常显示任务不涉及浮点操作，因而任务创建时不加浮点支持选项。如果增加浮点支持
 * 选项，且发生浮点异常，则会导致系统死机。
 */
int exc_task_module_init(void)
{
	int retn;	/* 线程创建返回值 */
	if (exc_task_module_init_flag)
	{
		return 0;
	}
		
	/* 创建异常消息打印队列 */
	if (-1 == (exc_task_mq_id = mq_create(0, 
						EXC_TASK_MAX_MSGS, 
						sizeof(struct exc_internal_msg), 
						PTHREAD_WAITQ_FIFO)))
	{
		printk("[ERROR] exception_module_init2 for create mq\n");
		return -1;
	}
	
	/* 创建异常消息打印线程 */
	if (0 != pthread_create2(&exc2_task_id,
						   "exc_task", 
						   200, 
						   RE_UNBREAKABLE, 
						   65536, 
						   (void *(*)(void *))exc_task, 
						   (void *)0))
	{
		mq_delete(exc_task_mq_id);
		printk("[ERROR] exception_module_init2 for create thread\n");
		return -1;		
	}
	
	/* 创建异常模块互斥量 */    
    if (0 != pthread_mutex_init2(&exc_mutex, 
								  PTHREAD_MUTEX_DEFAULT, 
								  RE_MUTEX_PRIO_INHERIT, 
								  PTHREAD_MUTEX_CEILING))
    {
    	mq_delete(exc_task_mq_id);
		printk("[ERROR] exception task module  for create mutex\n");
    	return -1;
    }
    
    /* 初始化钩子链表 */
    exc_handler_init();
	
    /* 挂接异常钩子 */
    exc_msg_recv_handler = exc_msg_recv;
	
	/* 注册异常显示钩子 */
	exc_handler_register(exc_disp_handler);
	
	/* 异常分析模块初始化 */
	exc_analysis_module_init();
	
	exc_task_module_init_flag = 1;
	
	return 0;
}

void exp_print(u64 context, int type)
{
 printk("%%%%%%%%%%%%%exc_print:0x%lx 0x%lx %d\n",context,type,cpu_id_get());
 while(1);
}
