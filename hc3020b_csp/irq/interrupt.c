
#include <stdio.h>
#include <cpu.h>
#include <errno.h>
#include <irq.h>

#include <schedule.h>
#include <inline_cpu_id_get.h>

#include <fpu.h>
#include <gs464_cpu.h>
#include <bd_opt.h>

#define EM_REG_MEM_READ(x) (*((volatile u32 *)(0xb0000000 + 0x51c0000 + x)))

/**
 * 浮点模块初始化标识
 */
extern volatile int fpu_module_init_flag ;

/**
 * 浮点上下文在TCB中的偏移
 */
extern size_t fpu_context_ptr_offset;

/**
 * 浮点上下文保存和恢复句柄
 */
extern void (*fpu_save_ctx_hdl)(Fp_Context *);
extern void (*fpu_restore_ctx_hdl)(Fp_Context *);


/**
 * 获取注册的浮点上下文
 */
#define reference_thread_fpu_context_ptr(thread_ptr) \
	((Fp_Context **)((size_t)thread_ptr + fpu_context_ptr_offset))

extern u32 read_rtc_sec();
extern void debug_exception_handler
(
		int		exp_vector,		/*异常向量号*/
		void *	debug_frame,	/*异常上下文信息*/
		void *	retain			/*保留参数*/
);
extern int shared_int_module_init(void);

extern u64 top_isr_stack_addr;
extern struct irq_data rtos_irqs[IRQ_NUM];
extern char mips_general_exception, mips_general_exception_end;

#ifdef __multi_core__
u8 *unsave_thread_esp_smp[MAX_SMP_CPUS] ;
#else
u8 *unsave_thread_esp = 0;
#endif
u64 temp_status = 0;
#ifdef __multi_core__
volatile u32 isr_nest_level_smp[MAX_SMP_CPUS];
#else
volatile u32 isr_nest_level_up = 0;
#endif
u32 int_ip2 = 0;
u32 int_ip6 = 0;
u32 int_ip7 = 0;

static size_t irq_offset = 0;

/* 系统中是否允许中断嵌套，用户可在资源配置中设置 */
volatile u32	isr_nestable = 0;

/* 系统中是否允许在中断处理函数中保存浮点上下文，用户可在资源配置中设置 */
volatile u32	fpu_savectx_enable = 1;

void isr_nestable_set(u32 isr_flag, u32 fpu_flag)
{
	isr_nestable = isr_flag;
	fpu_savectx_enable = fpu_flag;
}

void irq_handler_get(int irq)
{
	if( rtos_irqs[irq].handler )
		(*rtos_irqs[irq].handler)(rtos_irqs[irq].param);
}

/*******************************************************************************
 * 
 * 中断处理程序
 * 
 * 		本程序为通用中断处理，在本程序中调用PIC中断响应操作获取最高优先级中断的中断号，
 * 然后从中断信息表irq_data中获取此中断号对应的ISR线程，并调用之。
 * 
 * 输入：
 * 		arg：中断来源
 * 输出：
 * 		无
 * 返回：
 * 		无
 */

int g_intHappen[8] = {0};

static unsigned long long int_count_total = 0;
static unsigned long long int_clock_count = 0; /*INT_CLOCK*/
static unsigned long long int_ipi_count = 0; /*INT_IPI*/
static unsigned long long int_other_count = 0;

void int_show()
{
	printk("======================================\n");
	printk("中断总计数:       %d\n",int_count_total);
	printk("INT_CLOCK计数:    %d\n",int_clock_count);
	printk("INT_IPI计数:      %d\n",int_ipi_count);
	printk("其他中断计数:     %d\n",int_other_count);
	printk("======================================\n");
}

/* 具体处理移入 BSP */
void isr_common_handler(void *arg)
{
	int_count_total++;
//	printk("<**DEBUG**> [%s():_%d_]:: ISR in\n", __FUNCTION__, __LINE__);
#define NESTABLE
	u32 interrupt_pending;
	u32 irq_array[IRQ_NUM];
	u32 irq_num = IRQ_NUM;
	int cpu = inline_cpu_id_get();

	register u32 sr = get_c0_sr();
	

	/* 在栈上记录被中断的上下文指针 */
#if defined(LOONGSON3A_SMP) || defined(LOONGSON3A_SMP_64) || defined(HUARUI2_SMP_64) || defined(HUARUI3_SMP_64)
	interrupt_pending = ((u32)arg & sr) & CAUSE_IPMASK;
	sr &= (0xfffffffc & ~(interrupt_pending & ~(CAUSE_IP6)));
//	sr &= 0xfffffffc;
	set_c0_sr(sr);

//	printk("interrupt_pending==0x%x\r\n", interrupt_pending);




	Context_Ctrl *unsaved_context = (Context_Ctrl*)unsave_thread_esp_smp[cpu];
	int saved_errno = errno;
	if(unsaved_context->type != 0x10105a5a)
		printk("c1:*0x%lx 0x%lx %d\n",unsaved_context,unsaved_context->type,cpu);
	
	if(isr_nest_level_smp[cpu]>10){
		printk("[%08x]\n", sr);
		printk("isr nested more than %d times in cpu %d\n",isr_nest_level_smp[cpu],cpu);
	}
	
   
	if (!interrupt_pending)
	{
		context_restore((Context_Ctrl*) unsave_thread_esp_smp[cpu]);
		printk("no execption, no interruption, no reason\n\r");
		context_dump((Context_Ctrl *) unsave_thread_esp_smp[cpu]);
		while (1)
			;
	}
	
	
	if (fpu_savectx_enable)
	{
		/* 保存浮点上下文  */
		fpu_context_save();
	}
	else if (isr_nest_level_smp[cpu] == 0)
	{
		/* 关闭浮点协处理器，中断调度中不允许浮点运算，
			 * 主要是为了内核定时器的处理函数 */
		asm(
				"mfc0 $2,$12;\n"
				"li   $3,0xdfffffff;\n"
				"and  $2,$3;\n"
				"mtc0 $2,$12;\n"
				"sync \n"
				:
				:
				:"$2","$3"
		);
	}  
//	if(unsaved_context->type != 0x10105a5a)
//		printk("c2:*0x%lx 0x%lx %d\n",unsaved_context,unsaved_context->type,cpu);
	irq_num = int_inum(interrupt_pending, irq_array, irq_num);



	interrupt_pending &= ~(CAUSE_IP7 | CAUSE_IP6 | CAUSE_IP5 | CAUSE_IP4 | CAUSE_IP3 | CAUSE_IP2);

	save_thread_context(cpu, irq_num, unsaved_context);

	int ii;
	for (ii = 0; ii < irq_num; ii++)
	{
//		if(irq_array[ii] != INT_CLOCK)
//		{
//			printk("<**DEBUG**> [%s():_%d_]:: ### cpu%d irq_array[%d]=%d ###\n", 
//					__func__, __LINE__,cpu,ii,irq_array[ii]);
//		}
		switch(irq_array[ii])
		{
		case INT_CLOCK:
		{
			int_clock_count++;
			/* interrupt source: clock */

			/* huangyuan20140603：
			 * 保存被中断的上下文指针到任务控制块
			 * （包含调用系统查看器的中断进入事件钩子） */
//			save_thread_context(cpu, irq_array[ii], unsaved_context);

			isr_nest_level_smp[cpu]++;
	//		if( irq == CLOCK_IRQ )
	//		{

	//ww mod 20180529 del 时钟中断里面有2个需要处理 CDMA和timer本身，所以这里不需要打开中断嵌套		
	#ifdef NESTABLE
			if (isr_nestable)
			{
				set_c0_sr(sr | 0x00000001);
			}
	#endif

			/* 调用中断处理函数 */
			if( rtos_irqs[irq_array[ii]].handler )
				(*rtos_irqs[irq_array[ii]].handler)(rtos_irqs[irq_array[ii]].param);
			
			int_ack(INT_CLOCK);
	//		set_c0_sr(sr | 0x00008000);
			
			
			//ww mod 20180529 del 时钟中断里面有2个需要处理 CDMA和timer本身，所以这里不需要打开中断嵌套			
	#ifdef NESTABLE
			if (isr_nestable)
			{
				set_c0_sr(sr);
			}
	#endif
	
			
			isr_nest_level_smp[cpu]--;
			break;
		}
#ifndef _HC3020B_
		case INT_IPI:
#else/*ldf 20230920 modify:: for 3020b*/
		case INT_IPI0: 
		case INT_IPI1:
#endif
		{
//			printk("<**DEBUG**> [%s():_%d_]:: ### INT_IPI ###\n", __func__, __LINE__);
			int_ipi_count++;
	//		sysLEDSetState(1);
	//		printk("CAUSE_IP6\n");
			/* interrupt source: IPI */
			/* huangyuan20140603：
			 * 保存被中断的上下文指针到任务控制块
			 * （包含调用系统查看器的中断进入事件钩子） */
//			save_thread_context(cpu, irq_array[ii], unsaved_context);

			isr_nest_level_smp[cpu]++;
			
			
			extern void ipi_ack(void);
	//		set_c0_sr(get_c0_sr()&0xffffbfff);
//			ipi_ack();
			int_ack(irq_array[ii]);/*ldf 20230920 modify*/
			//printk("cpuid=%d\n",cpu);
			isr_nest_level_smp[cpu]--;
			
//			interrupt_pending &= ~CAUSE_IP6;
			break;
		}
		default:
		{
			int_other_count++;
			//if(irq_array[ii]>0)
			//printk("[%s][%d]irq_array[ii]=%d\n",__FUNCTION__,__LINE__,irq_array[ii]);

/*			if(irq_array[ii]!=INT_UART0){
				unsigned int _datal = 0;
				unsigned int _datah = 0;
				sysRead64(0x90000000,INTERRUPT_BASE_ADDR+0x20,&_datah,&_datal);
				printk("INTERRUPT_BASE_ADDR+0x20 _datah=%d _datal=%d\r\n", _datah,_datal);
				printk("irq_array[%d]=%d\r\n", ii,irq_array[ii] );}
*/
			//printk("irq_num=%d irq_array[%d]=%d\r\n",irq_num, ii,irq_array[ii] );

			//unsigned int _data1 = 0;
			//unsigned int _data2 = 0;
			//sysRead64(0x90000000,INTERRUPT_STAT_REG,&_data2,&_data1);
			//printk("INTERRUPT_STAT_REG _datah=0x%x _datal=0x%x\r\n", _data2,_data1);

			if (IS_HR3_INT_VALID(irq_array[ii]))
			{
				/* interrupt source: uart */

	//			if(irq_array[ii] != INT_UART0)
	//				printk("irq[%d]\n",irq_array[ii]);

				/* huangyuan20140603：
				 * 保存被中断的上下文指针到任务控制块
				 * （包含调用系统查看器的中断进入事件钩子） */
	//			save_thread_context(cpu, irq_array[ii], unsaved_context);
	//			if(unsaved_context->type != 0x10105a5a)
	//				printk("c11:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii]);
				int_disable_pic(irq_array[ii]);
		//		internal_int_disable(irq);
		//		hr3_int_disable(irq);
				isr_nest_level_smp[cpu]++;
	//			if(unsaved_context->type != 0x10105a5a)
	//					printk("c111:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii]);
		#ifdef NESTABLE
				if (isr_nestable)
				{
					printk("=======================\n");
					set_c0_sr(sr | 0x00000001);
				}
		#endif
	//			if(unsaved_context->type != 0x10105a5a)
	//					printk("c112:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii]);
	//			*(volatile unsigned long*)0xffffffff804f0000 = unsaved_context;
				/* 调用中断处理函数 */
				//if(irq_array[ii]==67 || irq_array[ii]==71)
				//	printk("start irq_array[ii]=%d\n",irq_array[ii]);


				if( rtos_irqs[irq_array[ii]].handler )
					(*rtos_irqs[irq_array[ii]].handler)(rtos_irqs[irq_array[ii]].param);
				//if(irq_array[ii]==67 || irq_array[ii]==71)
				//	printk("end irq_array[ii]=%d\n",irq_array[ii]);
	//			if(unsaved_context->type != 0x10105a5a)
	//				printk("c22:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii]);
				int_ack(irq_array[ii]);

		#ifdef NESTABLE
				if (isr_nestable)
				{
					set_c0_sr(sr);
				}
		#endif
				isr_nest_level_smp[cpu]--;
		//		int_enable_pic(irq);
		//		internal_int_enable(irq);

				int_enable_pic(irq_array[ii]);

		//		int_ack(irq);
	//			interrupt_pending &= ~CAUSE_IP4;
			}
			break;
		} //end of default
		} //end of switch
	}
//	if(unsaved_context->type != 0x10105a5a)
//		printk("c33:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii-1]);
//	extern u32 get_c0_sr();
//	extern void set_c0_sr(u32 val);
//	register u32 srValue = get_c0_sr();
//		srValue |= SR_IE;	
//	set_c0_sr(srValue);

//	if(unsaved_context->type != 0x10105a5a)
//		printk("c3:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii-1]);
	if (interrupt_pending) {
		printk("an exceptional interrupt[%08x]\n\r", interrupt_pending);
		while (1)
			;
	}
	
	if (fpu_savectx_enable)
	{
		/* 恢复浮点上下文  */
		fpu_context_restore();
	}
	else if (isr_nest_level_smp[cpu] == 0)
	{
				/* 打开浮点协处理器 */
		asm(
				"mfc0 $2,$12;\n"
				"li   $3,0x20000000;\n"
				"or  $2,$3;\n"
				"mtc0 $2,$12;\n"
				"sync \n"
				:
				:
				:"$2","$3"
		);
	}

	
	/* huangyuan20090422:
	 * 这里有可能出现唤起的中断任务优先级小于被打断的中断任务的情况
	 * 那么接下来调度程序应该做的是恢复到被打断的地方。 */
	/* 恢复这次的中断现场，或者切换至中断处理过程中出现的待调度任务 */
//	if(unsaved_context->type != 0x10105a5a)
//		printk("c3:*0x%lx 0x%lx %d [%d]\n",unsaved_context,unsaved_context->type,cpu,irq_array[ii-1]);
 
	context_restore_or_switch_to_another_thread_if_necessary(cpu, unsaved_context);
#elif defined(LOONGSON2H)
	Context_Ctrl *unsaved_context = (Context_Ctrl*) unsave_thread_esp;

	int saved_errno = errno;
	
	set_c0_sr(get_c0_sr()&0xfffffffc);
	
	interrupt_pending = (u32) arg & CAUSE_IPMASK;
	//	if(interrupt_pending != 0x8000)
	//	printk("interrupt_pending[0x%x]\n",interrupt_pending);

	/* huangyuan20090728：先忽略它 */

	if (!interrupt_pending) {
		context_restore((Context_Ctrl*) unsave_thread_esp);
		printk("no execption, no interruption, no reason\n\r");
		context_dump((Context_Ctrl *) unsave_thread_esp);
		while (1)
			;
	}
	
	save_thread_context(irq, unsaved_context);

	if (interrupt_pending & CAUSE_IP7) {
			/* interrupt source: clock */

	//		irq = int_inum(CAUSE_IP7);
			irq = CLOCK_IRQ;
//			save_thread_context(irq, unsaved_context);
	//		if (irq == CLOCK_IRQ) {

				int_disable_pic(irq);
				isr_nest_level_up++;
				/* 调用中断处理函数 */
				if (rtos_irqs[irq].handler)
					(*rtos_irqs[irq].handler)(rtos_irqs[irq].param);

				isr_nest_level_up--;
				int_enable_pic(irq);

				int_ack(irq);
	//		}
			interrupt_pending &= ~CAUSE_IP7;
		}
		//	if (interrupt_pending !=0)
		//	printk("interrupt_pending[0x%x]\n",interrupt_pending);

		if (interrupt_pending & CAUSE_IP4) {
			/* interrupt source: 2 */

			irq = int_inum(CAUSE_IP4);
			//		printk("isr_common_handler----------CAUSE_IP4: 0x%x\n", irq);
//			save_thread_context(irq, unsaved_context);

			if (irq >= INT2_IRQ_START && irq < INT3_IRQ_START) {
				if (irq != 66)
					int_disable_pic(irq);
				isr_nest_level_up++;

				/* 调用中断处理函数 */
				if (rtos_irqs[irq].handler)
					(*rtos_irqs[irq].handler)(rtos_irqs[irq].param);

				isr_nest_level_up--;
				if (irq != 66) {
					int_enable_pic(irq);
					int_ack(irq);
				}
			}
			interrupt_pending &= ~CAUSE_IP4;
		}

		if (interrupt_pending & CAUSE_IP3) {
			/* interrupt source: uart */
			//		printk("CAUSE_IP3\n");

			irq = int_inum(CAUSE_IP3);
			//			printk("isr_common_handler----------CAUSE_IP3: 0x%x\n", irq);
//			save_thread_context(irq, unsaved_context);

			if (irq >= INT1_IRQ_START && irq < INT2_IRQ_START) {
				int_disable_pic(irq);
				isr_nest_level_up++;

				/* 调用中断处理函数 */
				if (rtos_irqs[irq].handler)
					(*rtos_irqs[irq].handler)(rtos_irqs[irq].param);

				isr_nest_level_up--;
				int_enable_pic(irq);

				int_ack(irq);
			}
			interrupt_pending &= ~CAUSE_IP3;
		}

		if (interrupt_pending & CAUSE_IP2) {
			/* interrupt source: uart */

			irq = int_inum(CAUSE_IP2);
			//		if ((irq != -1)&&(irq != 3))
			//		    printk("CAUSE_IP2 irq[%d]\n",irq);
//			save_thread_context(irq, unsaved_context);
			if(irq>0)			/*add by liano*/
			{
				
				int_disable_pic(irq);
				int_ip2++;
				isr_nest_level_up++;

				/* 调用中断处理函数 */
				if (rtos_irqs[irq].handler)
					(*rtos_irqs[irq].handler)(rtos_irqs[irq].param);

				isr_nest_level_up--;
				int_enable_pic(irq);
			}
			int_ack(irq);
			interrupt_pending &= ~CAUSE_IP2;

		}
		if (interrupt_pending) {
			printk("an exceptional interrupt[%08x]\n\r", interrupt_pending);
			while (1)
				;
		}

		errno = saved_errno;

		//	schedule(2);

		/* huangyuan20090422:
		 * 这里有可能出现唤起的中断任务优先级小于被打断的中断任务的情况
		 * 那么接下来调度程序应该做的是恢复到被打断的地方。 */
		/* 恢复这次的中断现场，或者切换至中断处理过程中出现的待调度任务 */

		context_restore_or_switch_to_another_thread_if_necessary(unsaved_context);
#else
#error "No valid cpu specified!"
#endif
}


/*******************************************************************************
 * 
 * 中断模块初始化接口
 * 
 * 		本程序用于初始化中断模块；本程序在ReWorks c_main函数中调用。本程序主要完成三
 * 个方面的工作：其一，初始化硬件中断向量表，挂接用户中断；其二，初始化中断信息表（软件
 * 中断向量表）；其三，初始化共享中断的数据结构。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR
 */
int int_module_init()
{
	int cpu;
	int i = 0;
    int level;	
    static int int_module_init_flag = 0;
    
    if (int_module_init_flag)
    {
    	return OS_OK;
    }
//    for(i = 0;i < MAX_SMP_CPUS;i++ )
//    	printk("int_module_init 0x%lx\n",unsave_thread_esp_smp[i]);
    
    printk("[ReWorks]:Int Module Init...\n");
    
    level = int_lock();	/* 锁中断 */
    
	fill_exc_vec_code((void *) GEN_EXC_VEC, &mips_general_exception,
			&mips_general_exception_end);

	/*
	 * 初始化中断信息表数据结构
	 */
	/* 初始化数据结构 */
	memset(rtos_irqs, 0, sizeof(struct irq_data)*IRQ_NUM);

	if (add_registered_field("irq", NULL, &irq_offset) != OS_OK)
	{
		printk("in %s\n\r", __FUNCTION__);
		while(1);
	}

	/* init all controller
	 *   0-15   ------> i8259 interrupt
	 *   16-23  ------> mips cpu interrupt
	 *   32-63  ------> bonito irq
	 */
	//	hw_int_init();
	//	hw_interrup_init();
	int_init();
	
#ifdef __multi_core__
	/* 初始化中断嵌套深度计数 */
	for(cpu = 0; cpu <MAX_SMP_CPUS; cpu++){
		isr_nest_level_smp[cpu] = 0;
	}
#else
	isr_nest_level_up = 0;
#endif
    
	int_unlock(level);
	
	/* 初始化共享中断 */
	shared_int_module_init();		// wjh: todo
	
	int_module_init_flag = 1;
	return OS_OK;
}


u32 isr_nest_level(){
#ifdef __multi_core__
	u32 isr_nest;
	int level = int_lock();
	isr_nest = isr_nest_level_smp[cpu_id_get()];
	int_unlock(level);
	return isr_nest;
#else
	return isr_nest_level_up;
#endif
}
