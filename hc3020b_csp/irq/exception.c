/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了LOONGSON2F 64位体系结构下的异常处理模块
 * 修改：
 * 		 2013-01-06，唐立三，修改异常处理接口，增加异常显示任务，增加异常上报钩子函数
 * 		 2011-11-02，规范
 */
#include <reworks/types.h>
#include <reworks/printk.h>
#include <reworks/thread.h>
#include <reworks/sv.h>
#include <pthread.h>
#include <stdio.h>
#include <cpu.h>
#include <irq.h>
//#include <io_asm.h>
#include <string.h>
#include <irq.h>
#include <gs464_cpu.h>
#include <fpu.h>
#include <bd_opt.h>
#include <exception.h>
#include <exceptionp.h>
#include <pthread_hook.h>
#include <exc.h>
#include <exccode.h>
#include <coredump/coredump.h>
#include <reworks/kernel_lock.h>


extern void exception_name_print(unsigned int exc_code);
extern CORE_DUMP_EXC_HANDLER core_dump_exception_handler;

u32 exception_options = /*EXC_SEND_MSG |*/ EXC_BACK_TRACE | EXC_UNALIGNED_FIX | EXC_PRINT | EXC_ACCESS_ADDR | EXC_REG_DISPLAY;

/**
 * 异常发送任务钩子
 */
EXC_MSG_RECV exc_msg_recv_handler = NULL;

static int exception_module_init_flag = 0;
static size_t reg_val_offset;
/* 异常时打印的栈深度 */
extern int exception_stack_layer;

#ifdef __multi_core__
extern u8 *unsave_thread_esp_smp[];
#else
extern u8 *unsave_thread_esp;
#endif
extern u64 top_isr_stack_addr;
extern u64 temp_status;

#define get_reg_val_ptr_in_thread(thread_ptr) \
	(Exc_Regs **)((size_t)(thread_ptr) + (reg_val_offset))

/* 异常打印及任务是否继续的标志均是全局的 */
static u8 b_skip[EXC_TOTAL];

static void default_exception_handler(u32 exc_code, REG_SET *saved_context);

/*以下为龙芯3A不支持的异常号*/
#define RESERVE_EXC_CODE1		0x10
#define RESERVE_EXC_CODE2		0x11
#define RESERVE_EXC_CODE3		0x12
#define RESERVE_EXC_CODE4		0x13
#define RESERVE_EXC_CODE5		0x14
#define RESERVE_EXC_CODE6		0x15
#define RESERVE_EXC_CODE7		0x16

/* 异常向量表 */
EXC_TBL	exc_tbl[] = 
{
	{0,    0},
	{0x1,  (EXC_HANDLER)default_exception_handler},
	{0x2,  (EXC_HANDLER)default_exception_handler},
	{0x3,  (EXC_HANDLER)default_exception_handler},
	{0x4,  (EXC_HANDLER)default_exception_handler},
	{0x5,  (EXC_HANDLER)default_exception_handler},
	{0x6,  (EXC_HANDLER)default_exception_handler},
	{0x7,  (EXC_HANDLER)default_exception_handler},
	{0x8,  (EXC_HANDLER)default_exception_handler},
	{0x9,  (EXC_HANDLER)default_exception_handler},
	{0xA,  (EXC_HANDLER)default_exception_handler},
	{0xB,  (EXC_HANDLER)default_exception_handler},	
	{0xC,  (EXC_HANDLER)default_exception_handler},
	{0xD,  (EXC_HANDLER)default_exception_handler},
	{0xE,  0},
	{0xF,  (EXC_HANDLER)default_exception_handler},
	{0x10, 0},
	{0x11, 0},
	{0x12, 0},
	{0x13, 0},
	{0x14, 0},
	{0x15, 0},
	{0x16, 0},
	{0x17, (EXC_HANDLER)default_exception_handler}
};



/**
 * 设置需要跳过的异常。
 * @param exc_nu 异常类型编号
 * @param is_skip 打开或关闭跳过，1代表打开，0代表关闭
 */
void set_skip_exc(int exc_nu, int is_skip)
{
	if (exc_nu < 0 || exc_nu > EXC_TOTAL - 1)
	{
		return;
	}
	
	if (is_skip != 0 && is_skip != 1)
	{
		return;
		
	}
	b_skip[exc_nu] = is_skip;
}

/**
 * 设置是否打印异常信息。
 * @param b_p	0：不打印。
 * 				1: 打印。				
 */
void exc_print(int b_p)
{
	if (b_p == 0)
	{
		exception_options &= ~EXC_PRINT;
		return;
	}
		
	if (b_p == 1)
	{
		exception_options |= EXC_PRINT;
		return;
	}
}

/**
 * 将TCB中异常发生时的上下文拷贝给用户
 * @param thread_ptr 待获取上下文的任务
 * @param p_reg_val 用户层传入的Exc_Regs类型指针，用于保存异常发生时的上下文。
 */
void exc_context_get(thread_t thread_ptr, Exc_Regs *p_reg_val)
{
	memcpy(p_reg_val, *get_reg_val_ptr_in_thread(thread_ptr), sizeof(Exc_Regs));
}

/**
 * 初始化任务TCB中的异常上下文模块
 * @param thread_ptr 任务ID
 * @return 失败-1 成功0
 */
static int __exc_tcb_init(thread_t thread_ptr)
{
	Exc_Regs **reg_val_pptr = get_reg_val_ptr_in_thread(thread_ptr);

	if (!*reg_val_pptr)
	{

		*reg_val_pptr = (Exc_Regs *) kmalloc_align(sizeof(Exc_Regs), 32);
		if (*reg_val_pptr == NULL)
		{
			return -1;
		}

	}

	memset((char *) *reg_val_pptr, 0, sizeof(Exc_Regs));
	return 0;
}


/**
 * 从任务TCB中删除异常上下文模块
 * @param thread_ptr 任务ID
 */
static void __exc_tcb_del(thread_t thread_ptr)
{
	Exc_Regs **reg_val_pptr = get_reg_val_ptr_in_thread(thread_ptr);

	if (*reg_val_pptr)
	{
		kfree(*reg_val_pptr);
		*reg_val_pptr = 0;
	}

	return;
}


/**
 * 调试异常入口函数类型
 */
typedef void (*DEBUG_FUNC_PTR)(int, void *, void *);

/**
 * 系统调试异常挂接函数
 */
static DEBUG_FUNC_PTR exception_debug_handler_ptr = NULL;
static DEBUG_FUNC_PTR old_exception_debug_handler_ptr = NULL;
static EXC_HANDLER exception_handler_ptr = NULL;

OS_BOOLEAN is_bp_available(u64 bp)
{
	return TRUE;
}


u32 move_to_next_frame(u64 bp, void *addr)
{
	return 0;
}

/*******************************************************************************
 * 
 * 设置显示所有寄存器内容的开关
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		 成功返回0；失败返回-1
 * 
 */
int exc_reg_disp_set(void)
{
	exception_options |= EXC_REG_DISPLAY;
	
	return 0;
}


/*******************************************************************************
 * 
 * 清除显示所有寄存器内容的开关
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		 成功返回0；失败返回-1
 * 
 */
int exc_reg_disp_clear(void)
{
	exception_options &= ~EXC_REG_DISPLAY;
	
	return 0;
}

void unaligned_fix_on()
{
	exception_options |= EXC_UNALIGNED_FIX;
}

void unaligned_fix_off()
{
	exception_options &= ~EXC_UNALIGNED_FIX;
}

/*******************************************************************************
 * default_exception_handler(u32 vecnum, REG_SET *pregs)
 * 默认异常处理函数,
 * exc_code 	异常向量地址
 * saved_context 	产生异常前的所有寄存器值，指向一帧寄存器的低端地址处(即指向r1处)
 * 
 */

int exception_nest[4] = {0};
REG_SET* first_exp_context[4] = {0};


unsigned long long  count_data_bus_error = 0;/*ldf 20230728 add:: data bus error计数*/
unsigned long long  count_unaligned = 0;/*ldf 20230728 add:: unaligned计数*/

unsigned long long ldf_exc_print = 0;/*ldf 20230526 add:: 异常打印*/

static void default_exception_handler(u32 exc_cause, REG_SET *saved_context)
{
	u32 exc_code = (exc_cause & 0x7c)>>2;
	
////	printk("ISR Exception in cpu %d, ISR nest level:%d\n", cpu,
////			int_count());
//	printk("Exception Program Counter: 0x%016x\n",
//			((Context_Ctrl *)saved_context)->pc);
//	printk("Status Register: 0x%08x\n",
//			((Context_Ctrl *)saved_context)->c0_sr);
//	printk("Cause Register:  0x%08x(exccode: %d)\n", exc_cause, exc_code);
//
//	context_dump(saved_context);
//
//
//	while(1);

#if 1 /*ldf 20230526 add:: 异常打印*/
				if(ldf_exc_print)
				{
//					extern unsigned long ldf_idx;
//					extern unsigned long ldf_val0[100];
//					extern unsigned long ldf_val1[100];
//					extern unsigned long ldf_val2[100];
//					extern unsigned long ldf_val3[100];
//					int i = 0;
//					printk("ldf_idx:%d\n",ldf_idx);
//					for(i = 0; i < 100; i++)
//					{
//						printk("%d  buf:0x%lx  *buf:0x%lx  buf2:0x%lx  *buf2:0x%lx\n",
//								i,ldf_val0[i],ldf_val1[i],ldf_val2[i],ldf_val3[i]);
//					}
					
//					bd_to_context(&context_bd, saved_context);
					printk("\n<***DEBUG***> [%s:_%d_]:: ######################default_exception_handler######################\n",
							__FUNCTION__,__LINE__);
					printk("<***DEBUG***> [%s:_%d_]:: exc_code:0x%x  EPC:0x%lx  RA:0x%lx cpu[%d]\n",
							__FUNCTION__,__LINE__,exc_code,(Context_Ctrl*)saved_context->pc,(Context_Ctrl*)saved_context->ra,cpu_id_get());
//					printk("<***DEBUG***> [%s:_%d_]:: Exception Program Counter: 0x%016x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->pc);
//					printk("<***DEBUG***> [%s:_%d_]:: Status Register: 0x%08x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->c0_sr);
//					printk("<***DEBUG***> [%s:_%d_]:: Cause Register:  0x%08x\n", __FUNCTION__,__LINE__,exc_cause);
					printk("<***DEBUG***> [%s:_%d_]:: r(BADVADDR)=[0x%016x]\n", __FUNCTION__,__LINE__,read_c0_badvaddr());
					printk("<***DEBUG***> [%s:_%d_]:: ######################default_exception_handler######################\n\n",
							__FUNCTION__,__LINE__);
					printk("\n---------------Stack Trace--------------\n");
					extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
					backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
							((Context_Ctrl *)saved_context)->sp,
							((Context_Ctrl *)saved_context)->ra);
				}
#endif /*ldf 20230526 add:: 异常打印*/
	
	Exc_Regs *val_ptr;
	
	/* 获取Exc_Regs结构指针 */
	val_ptr = *get_reg_val_ptr_in_thread(THREAD_CURRENT_GET);
	
	int cpu = cpu_id_get();
	
	Context_Ctrl_BD context_bd;
	u64 epc;
	Fp_Context fpucontext;
	
	exception_nest[cpu]++;
	if(exception_nest[cpu] == 1){
		first_exp_context[cpu] = saved_context;
	}
	/* 分析异常原因 */
exception_analysis:	
	switch (exc_code)
	{
		case EXC_CODE_TLBMOD:
		{
			exception_options |= EXC_ACCESS_ADDR;
			break;
		}
		case EXC_CODE_TLBL:
		{
			exception_options |= EXC_ACCESS_ADDR;
			break;
		}
		case EXC_CODE_TLBS:
		{
			exception_options |= EXC_ACCESS_ADDR;
			break;
		}
		case EXC_CODE_ADEL:
		{
			count_unaligned++;/*ldf 20230801 add*/
			if (exception_options & EXC_UNALIGNED_FIX)
			{
				context_to_bd(saved_context, &context_bd, exc_cause); 
#if 0 /*ldf 20230526 add:: 异常打印*/
				if(ldf_exc_print)
				{
					bd_to_context(&context_bd, saved_context);
					printk("\n<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_ADEL######################\n",
							__FUNCTION__,__LINE__);
					printk("<***DEBUG***> [%s:_%d_]:: exc_code:0x%x  EPC:0x%lx  RA:0x%lx cpu[%d]\n",
							__FUNCTION__,__LINE__,exc_code,(Context_Ctrl*)saved_context->pc,(Context_Ctrl*)saved_context->ra,cpu_id_get());
//					printk("<***DEBUG***> [%s:_%d_]:: Exception Program Counter: 0x%016x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->pc);
//					printk("<***DEBUG***> [%s:_%d_]:: Status Register: 0x%08x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->c0_sr);
//					printk("<***DEBUG***> [%s:_%d_]:: Cause Register:  0x%08x\n", __FUNCTION__,__LINE__,exc_cause);
					printk("<***DEBUG***> [%s:_%d_]:: r(BADVADDR)=[0x%016x]\n", __FUNCTION__,__LINE__,read_c0_badvaddr());
					printk("<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_ADEL######################\n\n",
							__FUNCTION__,__LINE__);
//					printk("\n---------------Stack Trace--------------\n");
//					extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
//					backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
//							((Context_Ctrl *)saved_context)->sp,
//							((Context_Ctrl *)saved_context)->ra);
				}
#endif /*ldf 20230526 add:: 异常打印*/
				if (lw_unaligned_exc(0,&context_bd))
				{
					exception_options |= EXC_ACCESS_ADDR;
								
				}
				else
				{
					bd_to_context(&context_bd, saved_context);
//					printk("EXC_CODE_ADEL  ok 0x%016x [%d]\n",(Context_Ctrl*)saved_context->pc,cpu_id_get());
					exception_nest[cpu]--;
					context_restore((Context_Ctrl*)saved_context);
				}
			}
			break;
		}
		case EXC_CODE_ADES:
		{
			count_unaligned++;/*ldf 20230801 add*/
			if (exception_options & EXC_UNALIGNED_FIX)
			{
				context_to_bd(saved_context, &context_bd, exc_cause);
#if 0 /*ldf 20230526 add:: 异常打印*/
				if(ldf_exc_print)
				{
					bd_to_context(&context_bd, saved_context);
					printk("\n<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_ADES######################\n",
							__FUNCTION__,__LINE__);
					printk("<***DEBUG***> [%s:_%d_]:: exc_code:0x%x  EPC:0x%lx  RA:0x%lx cpu[%d]\n",
							__FUNCTION__,__LINE__,exc_code,(Context_Ctrl*)saved_context->pc,(Context_Ctrl*)saved_context->ra,cpu_id_get());
//					printk("<***DEBUG***> [%s:_%d_]:: Exception Program Counter: 0x%016x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->pc);
//					printk("<***DEBUG***> [%s:_%d_]:: Status Register: 0x%08x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->c0_sr);
//					printk("<***DEBUG***> [%s:_%d_]:: Cause Register:  0x%08x\n", __FUNCTION__,__LINE__,exc_cause);
					printk("<***DEBUG***> [%s:_%d_]:: r(BADVADDR)=[0x%016x]\n", __FUNCTION__,__LINE__,read_c0_badvaddr());
					printk("<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_ADES######################\n\n",
							__FUNCTION__,__LINE__);
//					printk("\n---------------Stack Trace--------------\n");
//					extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
//					backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
//							((Context_Ctrl *)saved_context)->sp,
//							((Context_Ctrl *)saved_context)->ra);
				}
#endif /*ldf 20230526 add:: 异常打印*/
				if (lw_unaligned_exc(0,&context_bd))
				{
					exception_options |= EXC_ACCESS_ADDR;
				}
				else
				{
//					printk("EXC_CODE_ADES err pc 0x%016x [%d]\n",(Context_Ctrl*)context_bd.pc,cpu_id_get());
					
					bd_to_context(&context_bd, saved_context);
					exception_nest[cpu]--;
					context_restore((Context_Ctrl*)saved_context);
				} 
			}
			break;
		}
		case EXC_CODE_IBE:
		{
			 break;
		}
		case EXC_CODE_DBE: /* 7 - data bus error */
		{
#if 0/*ldf 20230728 add:: 出现data bus error时,直接尝试恢复上下文,有概率能恢复成功继续运行,也可能直接死机*/
			Context_Ctrl *saved_context =
#ifdef __multi_core__
		(Context_Ctrl *)unsave_thread_esp_smp[cpu];
#else
		(Context_Ctrl *)unsave_thread_esp;
#endif
//			saved_context->pc += 4;
//			exception_nest[cpu]--;
		
			printk("%s %d EXC_CODE_DBE\n",__func__,__LINE__);
			
//			__asm__ volatile("nop");
//			__asm__ volatile("nop");
//			__asm__ volatile("nop");
//			__asm__ volatile("nop");
//			__asm__ volatile("nop");
			
			count_data_bus_error++;
			
			context_restore(saved_context);
#endif
			
#if 0 /*ldf 20230526 add:: 异常打印*/
				if(ldf_exc_print)
				{
					bd_to_context(&context_bd, saved_context);
					printk("\n<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_DBE######################\n",
							__FUNCTION__,__LINE__);
					printk("<***DEBUG***> [%s:_%d_]:: exc_code:0x%x  EPC:0x%lx  RA:0x%lx cpu[%d]\n",
							__FUNCTION__,__LINE__,exc_code,(Context_Ctrl*)saved_context->pc,(Context_Ctrl*)saved_context->ra,cpu_id_get());
//					printk("<***DEBUG***> [%s:_%d_]:: Exception Program Counter: 0x%016x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->pc);
//					printk("<***DEBUG***> [%s:_%d_]:: Status Register: 0x%08x\n",
//							__FUNCTION__,__LINE__,((Context_Ctrl *)saved_context)->c0_sr);
//					printk("<***DEBUG***> [%s:_%d_]:: Cause Register:  0x%08x\n", __FUNCTION__,__LINE__,exc_cause);
					printk("<***DEBUG***> [%s:_%d_]:: r(BADVADDR)=[0x%016x]\n", __FUNCTION__,__LINE__,read_c0_badvaddr());
					printk("<***DEBUG***> [%s:_%d_]:: ######################EXC_CODE_DBE######################\n\n",
							__FUNCTION__,__LINE__);
//					printk("\n---------------Stack Trace--------------\n");
//					extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
//					backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
//							((Context_Ctrl *)saved_context)->sp,
//							((Context_Ctrl *)saved_context)->ra);
				}
#endif /*ldf 20230526 add:: 异常打印*/
			 break;
		}
		case EXC_CODE_SYS:
		{
			  break;
		}
		case EXC_CODE_BP:
		{
			epc = get_c0_epc();
			if(exc_cause&0x80000000){
				epc += 4;
			}
			u32 break_code = (*((u32*)epc))>>16;
			if(break_code != 0)
			{
				switch(break_code)
				{
				case 7:
					exc_code = 32; /*Integer Divide by Zero*/
					break;
				case 6:
					exc_code = 33; /*Integer Overflow*/
					break;
				default:
					exc_code = 41;
					break;
				}
				break;
			}
			/* huangyuan20101125：在exception_debug_handler_ptr中会发生上下文切换，进而打开中断，异常中断
			 * 处理时共用的全局变量unsave_thread_esp会在中断处理时被修改，故在此用栈上的临时变量保存它的值。 */
			saved_context = (Context_Ctrl *)saved_context;
//			/*允许异常嵌套*/
//			set_c0_sr(get_c0_sr()&0xfffffffc);
			
			if (exception_debug_handler_ptr) {
				exception_debug_handler_ptr(EXC_CODE_BP, (void *) saved_context, 0);
			}
			else 
			{
				printk("exception_debug_handler_ptr == null\n");
				sleep(2);
			}
			exception_nest[cpu]--;
			context_restore(saved_context);
			break;
		}
		case EXC_CODE_RI:
		{
			break;
		}
		case EXC_CODE_CPU:
		{
			break;
		}
		case EXC_CODE_OV:
		{
			break;
		}
		case EXC_CODE_TRAP:
		{
			break;
		}
		case EXC_CODE_FPE:		/* 浮点异常： 15 */
		{
			Fp_Context fpucontext;
			mips_fpu_context_save(&fpucontext);
			saved_context = (Context_Ctrl *)saved_context;
//			printk("float exc!\n");
//			printk("fcsr: %x\n", *((u32 *)((u32)&fpucontext + 32 * 8)));
//			printk("fpcontext_addr: %x\n", &fpucontext);
//			printk("context_bd_addr: %x\n", &context_bd);
			
			context_to_bd((Context_Ctrl *)saved_context, &context_bd, exc_cause);
//			printk("epc: 0x%x\n", context_bd.pc);
			int ret = 0;
			if ((ret = fp_exc_handler(thread_get_options(THREAD_CURRENT_GET) & RE_FP_TASK, &context_bd, 0, &fpucontext)) == 0)
			{
//				printk("fp_exc_handler success!\n");
				/*在fp_exc_handler中会将fpucontext.fpcs中异常清掉，需要将新的fpcs写到寄存器中*/
				mips_fpu_context_restore(&fpucontext);
				bd_to_context(&context_bd, (Context_Ctrl *)saved_context);
//				printk("saved_context.pc: %x\n", saved_context->pc);
				exception_nest[cpu]--;
				
				context_restore((Context_Ctrl*)saved_context);
			}
			/* 关闭浮点异常 */
			asm(
				"li $2,0x01000000;\n" \
				"ctc1 $2,$31;\n" 
				:
				:
				:"$2"
			);
			
			/* 保存及显示浮点上下文内容 */
			if (exception_options & EXC_REG_DISPLAY)
			{			
				fpu_context_dump(&fpucontext);
			}
			
			break;
		}
		case EXC_CODE_WATCH:
		{
			break;
		}
		default:
		{
			printk("unknown: exception code[%d]\n", exc_code);
			break;
		}
	}
	
	if (exception_handler_ptr && kernel_state == 0)
	{
		/* huangyuan20101125：在exception_debug_handler_ptr中会发生上下文切换，进而打开中断，异常中断
		 * 处理时共用的全局变量unsave_thread_esp会在中断处理时被修改，故在此用栈上的临时变量保存它的值。 */
		saved_context = (Context_Ctrl *)saved_context;
//		/*允许异常嵌套*/
//		set_c0_sr(get_c0_sr()&0xfffffffc);
		
		exception_handler_ptr(exc_code, saved_context);
		exception_nest[cpu]--;
		context_restore(saved_context);		
	}
	
	printk("--------------------------------EXCEPTION-------------------------\n\r");
	/* 记录上下文信息 */	
	if (exception_module_init_flag)
	{
		val_ptr->reg_val_cause = exc_cause;
		val_ptr->reg_val_badaddr = read_c0_badvaddr();
		memcpy((char *)(&val_ptr->reg_context_gp), saved_context,
				sizeof(Context_Ctrl));
	}
	
	printk(PRINT_EXCEPTION_CODE"\n", S_CSP_LOONGSON | exc_code);
	exception_name_print(exc_code);
	
	/* 向异常任务发送异常信息 */
	if ((exception_options & EXC_SEND_MSG) && (0 == int_count())
		&& (exc_msg_recv_handler))
	{
		
		exc_msg_recv_handler((void *)saved_context, 
							 THREAD_CURRENT_GET, 
							 exc_code, 
							 cpu, 
							 read_c0_badvaddr(), exc_cause);
	}
	else if (0 < int_count())	/* 中断发生异常 */
	{
		printk("ISR Exception in cpu %d, ISR nest level:%d\n", cpu,
				int_count());
		printk("Exception Program Counter: 0x%016x\n",
				((Context_Ctrl *)saved_context)->pc);
		printk("Status Register: 0x%08x\n",
				((Context_Ctrl *)saved_context)->c0_sr);
		printk("Cause Register:  0x%08x(exccode: %d)\n", exc_cause, exc_code);
		
		context_dump(saved_context);
		
		if (exc_code == EXC_CODE_FPE)
		{
			fpu_context_dump(&fpucontext);
		}
		
		printk("Access Address: 0x%016x\n", read_c0_badvaddr());
		
		if(exception_options & EXC_BACK_TRACE)
		{
			printk("\n---------------Stack Trace--------------\n");
			extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
			backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
					((Context_Ctrl *)saved_context)->sp,
					((Context_Ctrl *)saved_context)->ra);
		}
		
//		while(1);//解决异常重启shell
	}
	else if (exception_options & EXC_PRINT)
	{
		printk("Exception in cpu %d, exc code:0x%x, ISR nest level:%d\n",
				cpu, exc_code,int_count());
		
		if (exception_options & EXC_REG_DISPLAY)
		{
			
			context_dump(saved_context);
			
			if (exc_code == EXC_CODE_FPE)
			{
				fpu_context_dump(&fpucontext);
			}
		}
		
		printk("r(BADVADDR)=[0x%016x]\n", read_c0_badvaddr());
		if (exception_options & EXC_BACK_TRACE)
		{
			extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
			backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
					((Context_Ctrl *)saved_context)->sp,
					((Context_Ctrl *)saved_context)->ra);
		}
	}

	/* 如果相应位置位，则设置PC <- PC+4，任务将不会挂起 */
	if (b_skip[exc_code]) 
	{
		Context_Ctrl *saved_context =
#ifdef __multi_core__
		(Context_Ctrl *)unsave_thread_esp_smp[cpu];
#else
		(Context_Ctrl *)unsave_thread_esp;
#endif
		saved_context->pc += 4;
		exception_nest[cpu]--;
		context_restore(saved_context);
//		SKIP_EXC(cpu);
	}
	
	
	if(exception_options & EXC_WAIT_FOREVER)
	{
		while(1);
	}

	/* 挂起当前异常的任务 */
	printk("============suspend %s - 0x%x\n", thread_name(THREAD_CURRENT_GET), THREAD_CURRENT_GET);
	pthread_suspend(0);
}


/*change coredump place v6.1.0 -> v6.1.1*/
/*Coredump异常处理*/
CORE_DUMP_EXC_HANDLER core_dump_exception_handler = NULL;

extern void reboot();

/* 
 * 设置coredump异常处理函数 
 */
CORE_DUMP_EXC_HANDLER coredump_set_exception_handler(CORE_DUMP_EXC_HANDLER the_exc_handler)
{
	CORE_DUMP_EXC_HANDLER old_exception_handler = core_dump_exception_handler;

	core_dump_exception_handler = the_exc_handler;
	
	return old_exception_handler;
}
/**/
/*******************************************************************************
 * 
 * ReWorks异常处理函数
 * 
 * 		本接口涉及的异常号定义于龙芯3A手册CP0控制寄存器的Cause寄存器的Exc Code字段。
 * 本接口共执行两部分内容：诊断异常类型、通过异常任务打印异常信息。
 * 
 * 输入：
 * 		exc_code：异常号
 * 输出：
 * 		无
 * 返回：
 * 		无
 * 备注：
 * 		根据3A处理器手册对Cause寄存器的说明，Cause寄存器的第2~6位字段用于标识异常号
 * （Exc Code），由于本接口需要对整数除零异常进行判断，因而修改文件exception_asm.s中的
 * 异常处理接口exception_handler：由原来传入Exc Code修改为直接传入Exc Cause寄存器值。
 */
void reworks_exception_handler(u32 exc_cause)
{
	
	int cpu = cpu_id_get();
	
	Context_Ctrl *saved_context; //第二个参数
#ifdef __multi_core__
	saved_context = (REG_SET *)unsave_thread_esp_smp[cpu];
#else
	saved_context = (REG_SET *)unsave_thread_esp;
#endif
	

	
	/*允许异常嵌套*/
	set_c0_sr(get_c0_sr()&0xfffffffc); 
	
#ifdef REDE_SYSTEMVIEWER
	EVT_TASK_CTX_1(EVENT_EXCEPTION, exc_cause);
#endif
	
	
//	if(the_general_exc_handler && kernel_state == 0)
//	{
//		(*the_general_exc_handler)(exc_code, (void*)saved_context);
//	}
//	else
	
	
	if (core_dump_exception_handler)
	{
		core_dump_exception_handler(exc_cause, (void *)saved_context, 0);
	}
	else
	{
		int index = (exc_cause & 0x7c)>>2;
		if(exc_tbl[index].handler != NULL)
			exc_tbl[index].handler(exc_cause, saved_context);
	}
	
	return;
}


/*******************************************************************************
 * 
 * 通用异常处理函数
 * 
 * 输入：
 * 		the_exc_handler：异常处理接口
 * 输出：
 * 		无
 * 返回：
 * 		返回原异常处理接口指针
 * 备注：
 * 		在ReWorks中除调试异常外，不需要设置异常处理接口。
 */
EXC_HANDLER exception_handler_set(EXC_HANDLER the_exc_handler)
{
	EXC_HANDLER old = exception_handler_ptr;

	if (the_exc_handler)
	{
		exception_handler_ptr = the_exc_handler;
	}
	
	return old;
}


/*******************************************************************************
 * 
 * 调试异常处理函数
 * 
 * 输入：
 * 		the_exc_handler：异常处理接口
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int exception_debug_handler_set(DEBUG_FUNC_PTR the_exc_handler)
{
	old_exception_debug_handler_ptr = exception_debug_handler_ptr;
	if (the_exc_handler)
	{
		exception_debug_handler_ptr = the_exc_handler;
	}
	
	exception_handler_set((EXC_HANDLER)the_exc_handler);
	
	return 0;
}


/*******************************************************************************
 * 
 * 异常模块初始化
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		异常分析模块暂时放在本接口中实现，后期建议独立为单独的资源组件配置
 */
int exception_module_init(void)
{
	unsigned level;
	
	if (exception_module_init_flag)
	{
		return 0;
	}

	/* 向任务TCB中的添加异常上下文模块 */
	if (add_registered_field("exc_reg_val", NULL, &reg_val_offset) != 0)
	{
		return -1;
	}
	if (pthread_create_hook_add(__exc_tcb_init) != 0)
	{
		return -1;
	}
	if (pthread_close_hook_add(__exc_tcb_del) != 0)
	{
		return -1;
	}
		
	/* 设置异常栈回溯级数 */
	exception_stack_layer = 10;
	
	exception_module_init_flag = 1;
	
	return 0;  
}



/* 
 * 挂接某个异常的异常处理函数
 * 
 */
int exc_install_handler(int vec, EXC_HANDLER handler)
{
	int i = 0;
	
	if((vec == 0) 
	|| (vec == RESERVE_EXC_CODE1) 
	|| (vec == RESERVE_EXC_CODE2) 
	|| (vec == RESERVE_EXC_CODE3) 
	|| (vec == RESERVE_EXC_CODE4) 
	|| (vec == RESERVE_EXC_CODE5) 
	|| (vec == RESERVE_EXC_CODE6)
	|| (vec == RESERVE_EXC_CODE7))
		return -1;
	
	for(i = 0; i < sizeof(exc_tbl) / sizeof(EXC_TBL); i++) { 
		if(exc_tbl[i].vecnum == vec) {
			exc_tbl[i].handler = handler;
			return 0;
		}
	}	
	return -1;
}

/* 
 * 卸载某个异常的异常处理函数
 * 
 */
int exc_uninstall_handler(int vec)
{
	if((vec == 0) 
	|| (vec == RESERVE_EXC_CODE1) 
	|| (vec == RESERVE_EXC_CODE2) 
	|| (vec == RESERVE_EXC_CODE3) 
	|| (vec == RESERVE_EXC_CODE4) 
	|| (vec == RESERVE_EXC_CODE5) 
	|| (vec == RESERVE_EXC_CODE6)
	|| (vec == RESERVE_EXC_CODE7))
		return 0;
	
	int i = 0;
	for(i = 0; i < sizeof(exc_tbl) / sizeof(EXC_TBL); i++) { 
		if(exc_tbl[i].vecnum == vec) {
			exc_tbl[i].handler = 0;
			return 0;
		}
	}	
	return -1;	
}

/* 
 * 获取某个异常的异常处理函数
 * 
 */
EXC_HANDLER exc_handler_get(int vec)
{
	int i = 0;	
	
	for(i = 0; i < sizeof(exc_tbl) / sizeof(EXC_TBL); i++) { 
		if(exc_tbl[i].vecnum == vec) {
			return exc_tbl[i].handler;
		}
	}	
	return 0;
}

