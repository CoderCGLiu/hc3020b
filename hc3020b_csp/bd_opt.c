/*
 *  带延迟槽优化的地址异常处理相关的上下文转换和测试用例
 */
#include <bd_opt.h>

/* 中断上下文转换到lw_unaligned_exc函数使用的上下文。 */
void context_to_bd(Context_Ctrl *s,Context_Ctrl_BD *bd, u32 cause)
{
	bd->type = s->type;
	bd->pc = s->pc;
	bd->c0_sr = s->c0_sr;
	bd->mulhi = s->mulhi;
	bd->mullo = s->mullo;
	bd->zero = 0;
	bd->AT =  s->AT;
	bd->v0 = s->v0;
	bd->v1 = s->v1;
	bd->a0 = s->a0;
	bd->a1= s->a1;
	bd->a2= s->a2;
	bd->a3= s->a3;
	bd->t0= s->t0;
	bd->t1= s->t1;
	bd->t2= s->t2;
	bd->t3= s->t3;
	bd->t4= s->t4;
	bd->t5= s->t5;
	bd->t6= s->t6;
	bd->t7= s->t7;
	bd->s0= s->s0;
	bd->s1= s->s1;
	bd->s2= s->s2;
	bd-> s3= s->s3;
	bd->s4= s->s4;
	bd->s5= s->s5;
	bd->s6= s->s6;
	bd->s7= s->s7;
	bd->t8 = s->t8;
	bd->t9 = s->t9;
	bd->k0 = 0;
	bd->k1= s->k1;
	bd->gp= s->gp;
	bd->sp= s->sp;
	bd->fp= s->fp;
	bd->ra= s->ra;
	bd->cause = cause;
}

/* lw_unaligned_exc函数使用的上下文转换到中断上下文。 */
void bd_to_context(Context_Ctrl_BD *bd,Context_Ctrl *s)
{
	s->type = bd->type;
	s->pc = bd->pc;
	s->c0_sr = bd->c0_sr;
	s->mulhi = bd->mulhi;
	s->mullo = bd->mullo;
//	bd->zero = 0;
	s->AT =  bd->AT;
	s->v0 = bd->v0;
	s->v1 = bd->v1;
	s->a0 = bd->a0;
	s->a1= bd->a1;
	s->a2= bd->a2;
	s->a3= bd->a3;
	s->t0= bd->t0;
	s->t1= bd->t1;
	s->t2= bd->t2;
	s->t3= bd->t3;
	s->t4= bd->t4;
	s->t5= bd->t5;
	s->t6= bd->t6;
	s->t7= bd->t7;
	s->s0= bd->s0;
	s->s1= bd->s1;
	s->s2= bd->s2;
	s-> s3= bd->s3;
	s->s4= bd->s4;
	s->s5= bd->s5;
	s->s6= bd->s6;
	s->s7= bd->s7;
	s->t8 = bd->t8;
	s->t9 = bd->t9;
//	bd->k0 = 0;
	s->k1= bd->k1;
	s->gp= bd->gp;
	s->sp= bd->sp;
	s->fp= bd->fp;
	s->ra= bd->ra;
}

/* 地址异常处理的测试用例
void testp(void)
{
	int i = 0;
	int *addr_buf = NULL;
	unsigned int nData = 0;
	int nData_int = 0;	
	unsigned short nData_ushort = 0;
	short nData_short = 0;

	long long nData_long = 0;
	
	unsigned int nData1 = 0;
	char buf[255];	
	printf("testp,buf[0x%x]\n",buf);

	for(i=0;i<255;i++)
	{		
		buf[i] = i;		
	}
printf("-----------------unsigned int-------------------\n\n");
#if 1
	nData = *(unsigned int*)(buf+4);
	printf("First nData ==0x%x\n",nData);
	taskDelay(10);

	nData = *(unsigned int*)(buf+5);
	printf("Second nData ==0x%x\n",nData);
	taskDelay(10);

	nData = *(unsigned int*)(buf+6);
	printf("Third nData ==0x%x\n",nData);
	taskDelay(10);

	nData = *(unsigned int*)(buf+7);
	printf("Last nData ==0x%x\n",nData);
	taskDelay(10);
#endif	

printf("---------------------int-------------------------\n\n");
#if 1
	nData_int = *(int*)(buf+4);
	printf("First nData ==0x%x\n",nData_int);
	taskDelay(10);

	nData_int = *(int*)(buf+5);
	printf("Second nData ==0x%x\n",nData_int);
	taskDelay(10);

	nData_int = *(int*)(buf+6);
	printf("Third nData ==0x%x\n",nData_int);
	taskDelay(10);

	nData_int = *(int*)(buf+7);
	printf("Last nData ==0x%x\n",nData_int);
	taskDelay(10);
#endif

printf("-----------------unsigned--short------------------\n\n");
#if 1

	nData_ushort = *(unsigned short*)(buf+4);
	printf("First nData ==0x%x\n",nData_ushort );
	taskDelay(10);

	nData_ushort  = *(unsigned short*)(buf+5);
	printf("Second nData ==0x%x\n",nData_ushort );
	taskDelay(10);

	nData_ushort  = *(unsigned short*)(buf+6);
	printf("Third nData ==0x%x\n",nData_ushort );
	taskDelay(10);

	nData_ushort  = *(unsigned short*)(buf+7);
	printf("Last nData ==0x%x\n",nData_ushort );
	taskDelay(10);
#endif

printf("---------------------short-----------------------\n\n");
#if 1

	nData_short = *( short*)(buf+4);
	printf("First nData ==0x%x\n",nData_short );
	taskDelay(10);

	nData_short  = *( short*)(buf+5);
	printf("Second nData ==0x%x\n",nData_short );
	taskDelay(10);

	nData_short  = *( short*)(buf+6);
	printf("Third nData ==0x%x\n",nData_short );
	taskDelay(10);

	nData_short  = *(short*)(buf+7);
	printf("Last nData ==0x%x\n",nData_short );
	taskDelay(10);
#endif

printf("----------------------long------------------\n\n");
#if 1

	nData_long = *(long long*)(buf+4);
//	printf("First nData ==0x%x\n",nData_long );
	printf("First nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_long+4),*(unsigned int*)(&nData_long));
	taskDelay(10);

	nData_long  = *(long long*)(buf+5);
//	printf("Second nData ==0x%x%x\n",nData_long );
	printf("Second nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_long+4),*(unsigned int*)(&nData_long));
	taskDelay(10);

	nData_long  = *(long long*)(buf+6);
//	printf("Third nData ==0x%x\n",nData_long );
	printf("Third nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_long+4),*(unsigned int*)(&nData_long));
	taskDelay(10);

	nData_long  = *(long long*)(buf+7);
//	printf("Last nData ==0x%x\n",nData_long );
	printf("Last nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_long+4),*(unsigned int*)(&nData_long));
	taskDelay(10);
#endif

printf("----------------------save------------------\n\n");
	taskDelay(10);
#if 1

	*(long long*)(0x8ddd0fa1) = 0x1122334455667788LL;
//	printf("*(long long*)(0x8ddd0fa1) ==0x%x\n\n",*(long long*)(0x8ddd0fa1) );
	printf("*(long long*)(0x8ddd0fa1) ==0x%x%08x\n\n",*(unsigned int*)(0x8ddd0fa1+4),*(unsigned int*)(0x8ddd0fa1));
	taskDelay(10);

	*(unsigned int*)(0x8ddd0fb1) = 0x11223344;
	printf("*(unsigned int*)(0x8ddd0fb1)==0x%x\n\n",*(unsigned int*)(0x8ddd0fb1) );
	taskDelay(10);

	*(int*)(0x8ddd0fc1) = 0x11223344;
	printf("*(int*)(0x8ddd0fc1) ==0x%x\n\n",*(int*)(0x8ddd0fc1) );
	taskDelay(10);

	*(short*)(0x8ddd0fd1) = -1122; //0x1122
	printf("*(short*)(0x8ddd0fd1) ==%d\n\n",*(short*)(0x8ddd0fd1) );
	taskDelay(10);

	*(unsigned short*)(0x8ddd0fe1) = 0x1122;
	printf("*(unsigned short*)(0x8ddd0fe1) ==0x%x\n\n",*(unsigned short*)(0x8ddd0fe1) );
	taskDelay(10);	
	
#endif
	
}

void fp_test()
{
	char buf[100];
	float nData_float;
	double nData_double;
	int i;

	for(i=0;i<100;i++)
	{		
		buf[i] = i;		
	}

printf("----------------------float------------------\n\n");
uthread_delay(10);
#if 1

	nData_float = *(float*)(buf+4);
	printf("First nData ==0x%08x\n",*(unsigned int*)(&nData_float));
	uthread_delay(10);

	nData_float  = *(float*)(buf+5);
	printf("Second nData ==0x%08x\n",*(unsigned int*)(&nData_float));
	uthread_delay(10);

	nData_float  = *(float*)(buf+6);
	printf("Third nData ==0x%08x\n",*(unsigned int*)(&nData_float));
	uthread_delay(10);

	nData_float  = *(float*)(buf+7);
	printf("Last nData ==0x%08x\n",*(unsigned int*)(&nData_float));
	uthread_delay(10);
#endif
	
printf("----------------------double------------------\n\n");
uthread_delay(10);
#if 1

	nData_double = *(double*)(buf+4);
	printf("First nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_double+4),*(unsigned int*)(&nData_double));
	uthread_delay(10);

	nData_double  = *(double*)(buf+5);
	printf("Second nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_double+4),*(unsigned int*)(&nData_double));
	uthread_delay(10);

	nData_double  = *(double*)(buf+6);
	printf("Third nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_double+4),*(unsigned int*)(&nData_double));
	uthread_delay(10);

	nData_double  = *(double*)(buf+7);
	printf("Last nData ==0x%x%08x\n",*(unsigned int*)((unsigned int)&nData_double+4),*(unsigned int*)(&nData_double));
	uthread_delay(10);
#endif
	
printf("----------------------save------------------\n\n");
uthread_delay(10);
#if 1
	nData_float = 0.1122;
	*(float*)(0x8ddd1001) = 0.1122;
	printf("nData_float ==0x%x([%f])\n", *(unsigned int*)(&nData_float), nData_float);
	printf("*(float*)(0x8ddd1001)==0x%x\n\n",*(unsigned int*)(0x8ddd1001));
	uthread_delay(10);

	nData_double = 0.11223344f;
	*(double*)(0x8ddd0ff1) = 0.11223344f;
	printf("nData_double ==0x%x%08x[%f]\n",*(unsigned int*)((unsigned int)&nData_double+4),*(unsigned int*)(&nData_double), nData_double);
	printf("*(double*)(0x8ddd0ff1) ==0x%x%08x\n\n",*(unsigned int*)(0x8ddd0ff1+4),*(unsigned int*)(0x8ddd0ff1));
	uthread_delay(10);
#endif
}

*/
