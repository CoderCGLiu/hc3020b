#include <stdio.h>
#include <schedule.h> /* thread_current */
#include <reworks/types.h>
#include <reworks/printk.h>
//hw
//#include "cpu.h"
#include <gs464_cpu.h>
#include <exceptionp.h>
#include <exception.h>
//hw


#define EXC_SEND_MSG		0x00000001
#define EXC_BACK_TRACE		0x00000002
#define EXC_WAIT_FOREVER	0x00000004

extern u32 exception_options;
extern char mips_tlbrefill_exception, mips_tlbrefill_exception_end;

int (* mmu_page_protect_ptr)(void *start_address , size_t size) = 0;
int (* mmu_page_unprotect_ptr)(void *start_address , size_t size) = 0;

int g_wired_index=0;
#define PAGE_SHIFT_1	12
#define PAGE_SIZE	(1UL << PAGE_SHIFT_1)
#define PAGE_MASK	(~(PAGE_SIZE-1))
#define PAGE_SHIFT 25
#define PAGE_TABLE_SIZE ((0xffffffffff >> PAGE_SHIFT )+ 1)//即是2^8 * 2^7=2^10 * 2^5=32K个表项

struct pte{
	u32 valid;
	u64 entrylo0;
	u64 entrylo1;
};

/* **************************************************
 * 在用户段发生缺页时产生tlb refill异常，而在内核段产生xtlb refill异常
 * */
struct pte page_table[PAGE_TABLE_SIZE];
//struct pte page_table[256];
static char *msk2str(unsigned int mask)
{
static char str[10];
	switch (mask) {
	case PM_4K:	return "4kb";
	case PM_16K:	return "16kb";
	case PM_64K:	return "64kb";
	case PM_256K:	return "256kb";
	case PM_1M:	return "1Mb";
	case PM_4M:	return "4Mb";
	case PM_16M:	return "16Mb";
	}
	sprintf(str,"%08x",mask);
	return str;
}

#if 0
#define BARRIER()					\
	__asm__ __volatile__(				\
		".set\tnoreorder\n\t"			\
		"nop;nop;nop;nop;nop;nop;nop\n\t"	\
		".set\treorder");
#else
	#define BARRIER()					\
	__asm__ __volatile__(				\
		".set\tnoreorder\n\t"			\
		"sync \n\t"	\
		".set\treorder");
#endif

void clear_tlb()
{
	unsigned int i;

	/* huangyuan20090930：把每一表项统一填为：
	 * 4K大小的页，把虚地址0x80000000开始的两个4K页，映射到
	 * 实地址0x00000000那里。页的属性为：非高速缓冲的（cached），
	 * 不脏（dirty）的，无效（valid）的，全局(global)的。
	 *
	 * 注意：虚地址0x80000000在mips体系结构中，为unmapped的地址，
	 * 所以访问0x80000000地址不会经过MMU使用TLB来转换，因此这些
	 * 都是废表项。 目的是使任何访问对mapped属性的地址的访问 ， 产
	 * 生TLB REFILL异常。 */
	for (i = 0; i < TLB_ENTRIES; i++)
	{
		write_c0_index(i);

		write_c0_pagemask(PM_4K);
		write_c0_entryhi(0xffffffff80000000+i*0x2000);
		write_c0_entrylo0(0x0000000000000000);
		write_c0_entrylo1(0x0000000000000000);

		BARRIER();
		__asm__ __volatile__(
			".set noreorder\n\t"
			"tlbwi\n\t"
			".set reorder");
		BARRIER();
	}
}

void dump_tlb(int first)
{
#ifdef _BAO_MOD
	u64 pagemask, s_entryhi, entryhi, entrylo0, entrylo1;
	unsigned long asid;
	unsigned int s_index,  c0, c1, i;
#else
	unsigned long s_entryhi, entryhi, entrylo0, entrylo1, asid;
	unsigned int s_index, pagemask, c0, c1, i;
#endif

	s_entryhi = read_c0_entryhi();
	s_index = read_c0_index();
	asid = s_entryhi & 0xff;
	int last = TLB_ENTRIES;
	for (i = first; i < last; i++) {
		write_c0_index(i);
		BARRIER();
		__asm__ __volatile__(
			".set noreorder\n\t"
			"tlbr\n\t"
			".set reorder");
		BARRIER();
		pagemask = read_c0_pagemask();
		entryhi  = read_c0_entryhi();
		entrylo0 = read_c0_entrylo0();
		entrylo1 = read_c0_entrylo1();

		/* Unused entries have a virtual address of CKSEG0.  */
		if ((entryhi & ~0x1ffffUL) != 0xffffffff80000000 /* CKSEG0 */
		    && (entryhi & 0xff) == asid) {
			/*
			 * Only print entries in use
			 */
			printk("Index: %2d pgmask=%s ", i, msk2str(pagemask));

			c0 = (entrylo0 >> 3) & 7;
			c1 = (entrylo1 >> 3) & 7;

//			printk("va=%011lx asid=%02lx\n",
			printk("va=%16x asid=%02x\n",
			       (entryhi & ~0x1fffUL),
			       entryhi & 0xff);
//			printk("\t[pa=%011lx c=%d d=%d v=%d g=%ld] ",
#ifdef _BAO_MOD
			printk("\t[pa=%016x c=%d d=%d v=%d g=%d] ",
#else
			printk("\t[pa=%08x c=%d d=%d v=%d g=%d] ",
#endif
			       (entrylo0 << 6) & PAGE_MASK, c0,
			       (entrylo0 & 4) ? 1 : 0,
			       (entrylo0 & 2) ? 1 : 0,
			       (entrylo0 & 1));
//			printk("[pa=%011lx c=%d d=%d v=%d g=%ld]\n",
#ifdef _BAO_MOD
			printk("[pa=%016x c=%d d=%d v=%d g=%d]\n",
#else
			printk("[pa=%08x c=%d d=%d v=%d g=%d]\n",
#endif
			       (entrylo1 << 6) & PAGE_MASK, c1,
			       (entrylo1 & 4) ? 1 : 0,
			       (entrylo1 & 2) ? 1 : 0,
			       (entrylo1 & 1));
		}
	}
	printk("\n");

	write_c0_entryhi(s_entryhi);
	write_c0_index(s_index);
	
	BARRIER();
}

void mmu_add_item_wired(u64 virtual_addr,u64 physical_addr,u32 size,u32 option)
{
	int i,index;
	int temp_index;
	index= size/(2*0x1000000);
	for (i = g_wired_index; i < g_wired_index + index; i++)
	{
		temp_index = i - g_wired_index;
		write_c0_entryhi(virtual_addr + 0x1000000 * 2*temp_index);
		write_c0_entrylo0(((physical_addr + 0x1000000 * 2*temp_index) >> 6) + option);
		write_c0_entrylo1(((physical_addr + 0x1000000 * (2*temp_index+1)) >> 6) + option);
		write_c0_pagemask(PM_16M);
		write_c0_index(i);
	
		BARRIER();
		__asm__ __volatile__(
			".set noreorder\n\t"
			"tlbwi\n\t"
			".set reorder");
		BARRIER();
	}
	g_wired_index +=index;
	write_c0_wired(g_wired_index);
}
  
/*****************************************************
 * 添加内存映射项
 * virtual_addr-虚拟地址
 * physical_addr-物理地址
 * size-要映射的内存大小
 * option-映射选项，很据entrylo0、entrylo1寄存器确定
 * TLB每页为16MB
 *****************************************************/
/*  
 * 华睿2号EntryLo0和EntryLo1寄存器
 * 
 *     63 62 61  ~  40 39 ~ 6 5 ~ 3 2 1 0
 *    ┌──┬──┬─────────┬──────┬─────┬─┬─┬─┐
 *    │RI│XI│         │ PFN  │ C   │D│V│G│
 *    └──┴──┴─────────┴──────┴─────┴─┴─┴─┘
 * 
 * PFN域：物理页号，是物理地址的高位（40位物理地址中的高28位）
 * C域：TLB页的Cache一致性属性
 * D域：脏位。如果该位被设置，页面则标记为脏，也就是可写的。实际上这一位在软件中作为防止数据被更改的写保护使用。
 * V域：有效位。当该位被设置时，说明TLB表项是有效的，否则将产生一个TLBL或TLBS例外。
 * G域：全局位。当EntryLo0和EntryLo1中的G位都被设置为1时，处理器将在TLB查找时忽略ASID。
 * 其余：保留。必须按0写入，读时返回0。
 */
void mmu_add_item (u64 virtual_addr,u64 physical_addr,u64 size,u32 option)
{
	int i,index,page_table_index;
	int temp_index;

//	printk("%s, %d, size>>32 0x%x virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, (size>>32)&&0xffffffff,virtual_addr);
//	printk("%s, %d, size 0x%x virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, (size>>0)&&0xffffffff,virtual_addr);
	index= size/(2*0x1000000); 
	
//	printk("%s, %d, index %d virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, index,virtual_addr);
	if(virtual_addr >>32 == 0xFFFFFFFF) /* 高端地址 0x80000000 ~ 0xFFFFFFFF */
	{
#ifdef _BAO_MOD
		page_table_index = (virtual_addr & /*0xffffffff*/0x00000000ffffffffULL)>>PAGE_SHIFT;
#else
		page_table_index = (virtual_addr & /*0xffffffff*/0x000000001fffffffULL)>>PAGE_SHIFT;
#endif
//		printk("%s, %d, page_table_index %d virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, page_table_index,virtual_addr);
	}
#ifdef _BAO_MOD
	else if(virtual_addr >>32 == 0x0) /* 低端地址 0x0 ~ 0x7FFFFFFF */
#else
	else
#endif
	{
		page_table_index = (virtual_addr & /*0xffffffff*/0x00000000ffffffffULL)>>PAGE_SHIFT;
//		printk("mem mmu_add_item:page_table_index %d virtual_addr 0x%lx\n\r",page_table_index,virtual_addr);
//		printk("%s, %d, page_table_index %d virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, page_table_index,virtual_addr);
	} 
#ifdef _BAO_MOD
	else {
		page_table_index = (virtual_addr & /*0xffffffff*/0x000000ffffffffffULL)>>PAGE_SHIFT;
//		printk("%s, %d, page_table_index %d virtual_addr 0x%lx\n\r",__FUNCTION__, __LINE__, page_table_index,virtual_addr);
//		return;
	}
#endif
	for (i = page_table_index; i < page_table_index + index; i++){
		temp_index = i - page_table_index;
//		printk("mmu_add_item:%d\n\r",i);
		page_table[i].entrylo0 = ((physical_addr + 0x1000000 * 2*temp_index) >> 6) + option; /*偶虚页*/
//		printk("page_table[%d].entrylo0:0x%016x\n",i, page_table[i].entrylo0);
		page_table[i].entrylo1 = ((physical_addr + 0x1000000 * (2*temp_index+1)) >> 6) + option; /*奇虚页*/
//		printk("page_table[%d].entrylo1:0x%016x\n",i, page_table[i].entrylo1);
		page_table[i].valid = 1;
	}
	
}
#if 0 /*FuKai，2018-7-7 未用到*/
void mmu_add_item_64(u32 virtual_addr,u64 physical_addr,u32 size,u32 option)
{
	int i,index,page_table_index;
	int temp_index;
//	if(virtual_addr>0x80000000){
//		return mmu_add_item_wired(virtual_addr,physical_addr,size,option);
//	}
	index= size/(2*0x1000000);
	page_table_index = virtual_addr>>PAGE_SHIFT;
	
	for (i = page_table_index; i < page_table_index + index; i++){
		temp_index = i - page_table_index;
//		printk("mmu_add_item:%d size[%d]\n\r",i,TABLE_SIZE);
		page_table[i].entrylo0 = (u32)((physical_addr + 0x1000000 * 2*temp_index) >> 6) + option;
		page_table[i].entrylo1 = (u32)((physical_addr + 0x1000000 * (2*temp_index+1)) >> 6) + option;
		
		page_table[i].valid = 1;
	}
	
}
#endif



/*****************************************************
 * 如果硬件为2G内存，则物理地址的分配为：
 * 0x00000000-0x0fffffff 0-256M
 * 0x90000000-0xffffffff 256M-2G
 *****************************************************/
void test_mmu()
{
	int index=0;
	int i=0;
//	//窗口2
//	MIPS_SW64(0x3ff00010,0x00000000);
//	MIPS_SW64(0x3ff00060,0xF0000000);
//	MIPS_SW64(0x3ff00090,0x000000f0);
	
	//窗口4 MC0 单通道2G (用 于 使 能 对内存高地址空间的访问)
//	MIPS_SW64(0x3ff00020,0x80000000);
//	MIPS_SW64(0x3ff00060,0x80000000);
//	MIPS_SW64(0x3ff000a0,0x000000f0);
 //2G内存，减去低256M
	index = (0x80000000-0x10000000)/(2*0x1000000);
	for (i = 0; i < index; i++)
	{
		write_c0_entryhi(0x10000000 + 0x1000000 * 2*i);
		write_c0_entrylo0(((0x90000000 + 0x1000000 * 2*i) >> 6) + 0x1F);
		write_c0_entrylo1(((0x90000000 + 0x1000000 * (2*i+1)) >> 6) + 0x1F);
		write_c0_pagemask(PM_16M);
		write_c0_index(i);

		BARRIER();
		__asm__ __volatile__(
			".set noreorder\n\t"
			"tlbwi\n\t"
			".set reorder");
		BARRIER();
	}
	
	
}
int mmu_module_init(int type)
{
   int i = 0;
   int cpu;
   
	fill_exc_vec_code((void *)TLB_REFILL_EXC_VEC, &mips_tlbrefill_exception,
			&mips_tlbrefill_exception_end);
	fill_exc_vec_code((void *)XTLB_REFILL_EXC_VEC, &mips_tlbrefill_exception,
			&mips_tlbrefill_exception_end);

    cpu = cpu_id_get();
	clear_tlb();
	
	for(i=0;i < PAGE_TABLE_SIZE; i++)
	{
		page_table[i].valid = 0;
	}
	
	//开放所有tlb为random替换项
	write_c0_wired(g_wired_index);

#ifdef VGA_MEM_MAPPING_PATCH
	int i;
	for (i = 0; i < 64/*2*/; i++)
	{
//		write_c0_entryhi(0xC0000000 + 0x1000000 * 2*i);
//		write_c0_entrylo0(((0x10000000 + 0x1000000 * 2*i) >> 6) + 0x3F);
//		write_c0_entrylo1(((0x10000000 + 0x1000000 * (2*i+1)) >> 6) + 0x3F);
		write_c0_entryhi(0xC0000000 + 0x4000 * 2*i);
		write_c0_entrylo0(((0x10000000 + 0x4000 * 2*i) >> 6) + 0x3F);
		write_c0_entrylo1(((0x10000000 + 0x4000 * (2*i+1)) >> 6) + 0x3F);

//		write_c0_pagemask(PM_16M);
		write_c0_pagemask(PM_16K);
		write_c0_index(i);

		BARRIER();
		__asm__ __volatile__(
			".set noreorder\n\t"
			"tlbwi\n\t"
			".set reorder");
		BARRIER();
	}

	dump_tlb(0, 63);
#endif /* #ifdef VGA_MEM_MAPPING_PATCH */

    printk("[ReWorks]:PAGE_TABLE_SIZE 0x%x\n",PAGE_TABLE_SIZE);
	return 0;
}

extern u64 read_c0_badvaddr();
extern u8 *unsave_thread_esp_smp[];	


void refill_tlb()
{
	u64 badvaddr;
	int index;
	int cpu = cpu_id_get();
	u32 exc_code;
	u32 exc_cause;
	badvaddr = read_c0_badvaddr();
//	if (g_debug)
//	printk("[%s,%d]badvaddr=[0x%x, 0x%x]\n", __FUNCTION__, __LINE__, (badvaddr >> 32) & 0xFFFFFFFFUL, badvaddr & 0xFFFFFFFFUL);
	
	if(badvaddr >>32 == 0xFFFFFFFF)
	{
#ifdef _BAO_MOD
		index = (badvaddr &/*0xffffffff*/0x00000000ffffffffULL)>>PAGE_SHIFT;
#else
		index = (badvaddr &/*0xffffffff*/0x000000001fffffffULL)>>PAGE_SHIFT;
#endif
	}
#ifdef _BAO_MOD
	else if(badvaddr >>32 == 0x0)
#else
	else
#endif
	{
		index = (badvaddr&/*0xffffffff*/0x00000000ffffffffULL)>>PAGE_SHIFT;
	}
#ifdef _BAO_MOD
	else if(badvaddr >>32)
	{	
		index = (badvaddr&/*0xffffffff*/0x000000ffffffffffULL)>>PAGE_SHIFT;
	}
	else
#endif	
	{  
		exception_options |= EXC_ACCESS_ADDR;
		reworks_exception_handler(get_c0_cause());
	}

	if(!(page_table[index].valid)){
		printk("index:%d, lo0=0x%x, lo1=0x%x \n", index, page_table[index].entrylo0, page_table[index].entrylo1);
		printk("\nTLB REFILL exception in CPU[%d]:\n",cpu_id_get());
		Context_Ctrl *saved_context; //第二个参数
		#ifdef __multi_core__
			saved_context = (REG_SET *)unsave_thread_esp_smp[cpu];
		#else
			saved_context = (REG_SET *)unsave_thread_esp;
		#endif
			context_dump(saved_context);
			printk("r(BADVADDR)=[0x%016x]\n", badvaddr);
			extern int isr_nest_level_smp[];
			printk("isr_nest_level_smp[cpu]:%d\n",isr_nest_level_smp[cpu]);
			extern int exception_nest[];
			printk("exception_nest[]:%d\n",exception_nest[cpu]);
			extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
			backtrace_stack(((Context_Ctrl *)saved_context)->pc, 
					((Context_Ctrl *)saved_context)->sp,
					((Context_Ctrl *)saved_context)->ra);
		exception_options |= EXC_ACCESS_ADDR;
		reworks_exception_handler(get_c0_cause());
	}
	
//	printk("entrylo0,entrylo1[0x%lx, 0x%lx]\n",
//			page_table[index].entrylo0,page_table[index].entrylo1);


	write_c0_pagemask(PM_16M);
	write_c0_entrylo0(page_table[index].entrylo0);
	write_c0_entrylo1(page_table[index].entrylo1);
	
	BARRIER();
	__asm__ __volatile__(
		".set noreorder\n\t"
		"tlbwr\n\t"
		".set reorder");
	BARRIER();
	
	

#ifdef __multi_core__
	extern u8 *unsave_thread_esp_smp[];	
	context_restore((Context_Ctrl*) unsave_thread_esp_smp[cpu]);
#else
	extern Context_Ctrl *unsave_thread_esp;	
	context_restore((Context_Ctrl*) unsave_thread_esp);
#endif
	
	printk("no execption, no interruption, no reason\n\r");

#ifdef __multi_core__
	context_dump((Context_Ctrl *) unsave_thread_esp_smp[cpu]);
#else
	context_dump((Context_Ctrl *) unsave_thread_esp);
#endif
	
	while (1);
//	while(1);
}

/*******************************************************************************
*
* sysSetTlb - Programs the available TLB for translations
*
* Programs the onboard TLB table.  Entries 0 to IO_SPACE_NUM_TLBS are
* reserved.  <size> should be a multiple of 8K, 32K, 128K, 512K, 4M,
* 16M, or 32M.  <mode> is the bitwise OR of the one of the first 5
* macros and a combination of one or more of the last three:
*
* TLBLO_NONC_WT - Cacheable, noncoherent, write-through, no write allocate
* TLBLO_NONC_WTA - Cacheable, noncoherent, write-through, write allocate
* TLBLO_NC - Uncacheable, blocking
* TLBLO_NONC - Cacheable non-coherent, write-back
* TLBLO_CUW - Uncacheable, non-blocking
* TLBLO_D   - TLB writeable
* TLBLO_V   - TLB valid
* TLBLO_G   - TLB global
*
* RETURNS: 0 or -1 if size is invalid
*
*/
#define TLBLO_D                 0x4             /* writeable */
#define TLBLO_V                 0x2             /* valid bit */
#define TLBLO_G                 0x1             /* global bit */
#define TLBLO_CMASK             0x00000038
#define TLBLO_MODE		(TLBLO_CMASK | TLBLO_D | TLBLO_V | TLBLO_G)

#define IO_SPACE_PAGE_SIZE	(16*1024*1024)
#define	KSSIZE		0x20000000
#define	K3SIZE		0x20000000
#define IO_SPACE_TOTAL_SIZE	(KSSIZE + K3SIZE)

#define IO_SPACE_NUM_TLBS	(IO_SPACE_TOTAL_SIZE/(2*IO_SPACE_PAGE_SIZE))
void sysClearTlb();
extern int sys_setTlbEntry (unsigned int entry, unsigned long virtual, unsigned long physical, unsigned int mode);
int sysSetTlb
    (
    unsigned long virtAddr,
    unsigned long physAddr,
    unsigned long size,
    unsigned int mode
    )
    {
#if 1 /*commented by wangfq*/
    unsigned int tlbEntry;
    unsigned int pagesize=32*1024*1024, numTlbs;

    /* Pick the largest page size that is aligned with the address */
    do
    {
        /* Is it aligned correctly */
        if ((size & (pagesize - 1)) == 0)
            break;

        pagesize >>= 2;
    } while (pagesize >= 8*1024);

    /* Invalid <size> parameter */
    if (pagesize < 8*1024)
    {
        printk("sysSetTbl: <size> must be a multiple of 8K,32K,128K,512K,4M,16M, or 32MB\n");
        return (-1);
    }

    pagesize /= 2;		/* Per TLB */
    numTlbs = size/pagesize;
    mode &= TLBLO_MODE;

    /* Not enough TLBS ? */
    if (numTlbs > 2*(TLB_ENTRIES - IO_SPACE_NUM_TLBS))
    {
        printk("sysSetTbl: Not enough TLBs to map requested size \n");
        return (-1);
    }

    for (tlbEntry=IO_SPACE_NUM_TLBS; tlbEntry < TLB_ENTRIES/*, numTlbs>0*/; tlbEntry ++)
	{
        if (sys_setTlbEntry(tlbEntry,virtAddr,physAddr,(pagesize|mode)) == -1)
		{
            printk("sysSetTbl: Got error on TBL %d !!!!\n", tlbEntry);
            sysClearTlb();
            return (-1);
		}
 		virtAddr += 2*pagesize;
 		physAddr += 2*pagesize;

	}
#endif /*end commented*/

    return (0);
    }

/*******************************************************************************
*
* sysClearTlb - clear the translation lookaside buffer
*
* This routine clears the entries in the translation lookaside buffer (TLB)
* for the MIPS RM7000 CPU.
*
* RETURNS: N/A
*
*/
extern void sys_clearTlbEntry (int entry);
void sysClearTlb (void)
    {
    register int tlbEntry;printk("!!!!!!!!!!!!!!!!!!!!!\n");
    for (tlbEntry = 0 /*modified by wangfq,original*/; tlbEntry < TLB_ENTRIES; tlbEntry ++)
    	sys_clearTlbEntry (tlbEntry);
    }



/*******************************************************************************
*
* no_text_protect_ptr - disable the mmu protection for text code
*
* RETURNS: N/A
*
*/
/*void no_text_protect_ptr()
{
}*/
int (*no_text_protect_ptr)(void) =0;

#ifdef __multi_core__
void mmu_init_per_cpu(){
	clear_tlb();
}
#endif
