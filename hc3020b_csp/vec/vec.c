/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了矢量任务支持功能，即对设置了矢量支持选项的任务提供矢量上下文保存
 * 		 和恢复功能。
 * 修改：
 * 		 2014-08-20
 * 		 ......
 */
#include <stdio.h>
#include <pthread_hook.h>
//#include <thread-private.h>

#include "vec.h"
//#include "../h/private/exceptionp.h"
#include <exceptionp.h>

extern void *memset(void *s, int c, size_t n);



/**
 * FCSR寄存器定义
 */
/* 异常使能 */
#define FCSR_ENABLES_V		0x00000800
#define FCSR_ENABLES_Z		0x00000400
#define FCSR_ENABLES_O		0x00000200
#define FCSR_ENABLES_U		0x00000100
#define FCSR_ENABLES_I		0x00000080
#define FCSR_ENABLES_ALL	\
	(FCSR_ENABLES_V |	 	\
	 FCSR_ENABLES_Z | 		\
	 FCSR_ENABLES_O | 		\
	 FCSR_ENABLES_U | 		\
	 FCSR_ENABLES_I)
/* 中断关闭  */
#define FCSR_FS				0x01000000



/**
 * 矢量上下文保存和恢复句柄
 */
void (*vec_save_ctx_hdl)(Vec_Context *) = 0;
void (*vec_restore_ctx_hdl)(Vec_Context *) = 0;

/**
 * 矢量模块初始化标识
 */
static int vec_module_init_flag = 0;

/**
 * 矢量上下文在TCB中的偏移
 */
static size_t vec_context_ptr_offset;

/**
 * 获取注册的矢量上下文
 */
#define reference_thread_vec_context_ptr(thread_ptr) \
	((Vec_Context **)((size_t)thread_ptr + vec_context_ptr_offset))


/*******************************************************************************
 * 
 * 矢量寄存器初始化接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
extern void vector_init();
void vec_init(void)
{
	static int vec_init_flag = 0;
	
	if (vec_init_flag)
	{
		return;
	}
	
	/* 初始化矢量控制寄存器 */
	vector_init();
	
	/* 设置保存和恢复钩子，接口在vec_asm.s中实现 */
    vec_save_ctx_hdl 	= mips_vec_context_save;
    vec_restore_ctx_hdl = mips_vec_context_restore;
    
    vec_init_flag = 1;
    
    return;
}

/*******************************************************************************
 * 
 * 矢量任务判断接口
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		是矢量任务返回TRUE；否则返回FALSE
 */
boolean thread_is_vec(thread_t thread_ptr)
{
//	return ((((NThread_Ctrl *)thread_ptr)->options & RE_VEC_TASK) != 0); 
	if (thread_is_fp(thread_ptr) == TRUE) return TRUE;
//	
	return ((thread_get_options(thread_ptr) & RE_VEC_TASK) != 0); 
}

/*******************************************************************************
 * 
 * 矢量上下文初始化接口
 * 
 * 		本接口在矢量任务创建钩子函数中调用。为设置了矢量支持选项的任务分配用于保存
 * 矢量上下文的内存空间，并重新设置矢量协处理器控制器。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
static OS_STATUS  vec_context_init(thread_t thread_ptr)
{
	Vec_Context **vec_context_pptr = reference_thread_vec_context_ptr(thread_ptr);

	/* 检查矢量上下文是否已创建 */
	if (!*vec_context_pptr) 
	{
		/* CACHE_ALIGN_SIZE);  CACHE_AlIGN_SIZE need to fixed, vxworks */
		*vec_context_pptr = (Vec_Context *)kmalloc_align(sizeof(Vec_Context),32); 
		if (*vec_context_pptr == NULL)
		{
			return -1; 
		}
	//	printk("vec:0x%x ,init,(*vec_context_pptr)= 0x%x\n",
	//			thread_ptr,(*vec_context_pptr));
		
		memset((char *)(*vec_context_pptr), 0, sizeof(VEC_CONTEXT));
		
		/* 修改浮点上下文的FCSR寄存器，使能浮点中断；原代码中该数值为0x01000000 */
		(*vec_context_pptr)->fpcs = FCSR_FS;
//	}else
//	{
//		printk("vec:0x%x ,warning,(*vec_context_pptr)= 0x%x\n",
	//			thread_ptr,(*vec_context_pptr));
	}

	return 0;
}


/*******************************************************************************
 * 
 * 矢量上下文创建钩子
 * 	
 * 		本接口在任务创建时调用。用于完成矢量上下文的创建和设置。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static int vec_create_hook(thread_t thread_ptr)
{
//	printk("thread[0x%x] create\n", thread_ptr);
	
	if (vec_module_init_flag != TRUE)
	{
		return -1;
	}

//	printk("vec:0x%x ,is vec = %d ,opt =0x%x\n",thread_ptr,thread_is_vec(thread_ptr),thread_opt(thread_ptr));
	
	if (thread_is_vec(thread_ptr) == TRUE)
	{
//		printk("vec:0x%x ,init\n",thread_ptr);
		return vec_context_init(thread_ptr);
	}

    return 0;
}


/*******************************************************************************
 * 
 * 矢量上下文切换钩子
 * 	
 * 		本接口主要完成两部分工作：将正在执行的线程的矢量上下文保存在任务TCB中，将要
 * 执行的任务上下文从任务TCB中恢复到矢量寄存器中。这两项操作均通过vec_asm.s中提供的
 * 钩子函数完成。
 * 
 * 输入：
 * 		executing：正在执行的任务
 * 		heir：准备执行的任务
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static void vec_switch_hook(thread_t executing, thread_t heir)
{
	Vec_Context * vec_context;
	
	if (vec_module_init_flag != TRUE)
	{
		return;
	}
		
	if (thread_is_vec(executing) == TRUE) 
	{
		vec_context = *reference_thread_vec_context_ptr(executing);
//		printk("vec:0x%x ,save,vec_context 0x%x\n",executing,vec_context);
	//	if(!vec_context)
	//		printk("%s\n",get_thread_name_unsafe(executing));
		vec_save_ctx_hdl(vec_context);
	}

	if (thread_is_vec(heir) == TRUE)
	{
		vec_context = *reference_thread_vec_context_ptr(heir);
	//	printk("vec:0x%x ,restore,vec_context 0x%x\n",heir,vec_context);
	//	if(!vec_context)
	//	{
	//		printk("%s,is_vec= %d,opt[0x%x]\n",get_thread_name_unsafe(heir),
	//				thread_is_vec(heir) ,thread_opt(heir));
	//	}
		vec_restore_ctx_hdl(vec_context);
	}
	
	return;
}

void dump_dbg_vec(Vec_Context * vec_context)
{
	int regno;
	if (vec_context)
	{
		for(regno=0;regno<16;regno++)
		{
			vector_zr * vec_reg_ptr =  (vector_zr *)((vector_zr*)vec_context + regno);
		printk("vec%d:\n", regno);
		printk("%016x  %016x  %016x  %016x\n", 
				*(u64*)(vec_reg_ptr->vec_h2), 
				*(u64*)(vec_reg_ptr->vec_h1), 
					*(u64*)(vec_reg_ptr->vec_l2), 
					*(u64*)(vec_reg_ptr->vec_l1)); 
		}
		
	}
}

void dbg_save_vec(thread_t executing)
{
	Vec_Context * vec_context;
	
	if (vec_module_init_flag != TRUE)
	{
		return;
	}
		
	if (thread_is_vec(executing) == TRUE) 
	{
		vec_context = *reference_thread_vec_context_ptr(executing);
//		vec_save_ctx_hdl(vec_context);
		mips_vec_context_save(vec_context);

		
		dump_dbg_vec(vec_context);
	}

	
	return;
}

void dbg_restore_vec(thread_t heir)
{
	Vec_Context * vec_context;
	
	if (vec_module_init_flag != TRUE)
	{
		return;
	}

	if (thread_is_vec(heir) == TRUE)
	{
		vec_context = *reference_thread_vec_context_ptr(heir);
		vec_restore_ctx_hdl(vec_context);
		
		dump_dbg_vec(vec_context);
	}
	
	return;
}


/*******************************************************************************
 * 
 * 矢量上下文删除钩子
 * 	
 * 		本接口将任务销毁之前调用，用于释放任务创建时申请的矢量上下文空间。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static void vec_delete_hook(thread_t thread_ptr)
{
	if (vec_module_init_flag != TRUE)
	{
		return;
	}
	
	if (thread_is_vec(thread_ptr) == TRUE) 
	{
		Vec_Context *vec_context = *reference_thread_vec_context_ptr(thread_ptr);
		if (vec_context)
		{
			*reference_thread_vec_context_ptr(thread_ptr) = 0;
			kfree(vec_context);
		}
	}

	return;
}


/*******************************************************************************
 * 
 * 打印矢量寄存器组
 * 
 * 输入：
 * 		context：矢量上下文
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void vec_context_dump(Vec_Context *context)
{
	#define DUMP_VEC_CTX(regno) \
	{ \
	printk("vec%d:\n", regno);\
	printk("%016x  %016x  %016x  %016x\n", \
			*(u64*)context->vec##regno.vec_h2, \
			*(u64*)(context->vec##regno.vec_h1), \
			*(u64*)(context->vec##regno.vec_l2), \
			*(u64*)(context->vec##regno.vec_l1)); \
	}
	
	DUMP_VEC_CTX(0);
	DUMP_VEC_CTX(1);
	DUMP_VEC_CTX(2);
	DUMP_VEC_CTX(3);
	DUMP_VEC_CTX(4);
	DUMP_VEC_CTX(5);
	DUMP_VEC_CTX(6);
	DUMP_VEC_CTX(7);
	DUMP_VEC_CTX(8);
	DUMP_VEC_CTX(9);
	DUMP_VEC_CTX(10);
	DUMP_VEC_CTX(11);
	DUMP_VEC_CTX(12);
	DUMP_VEC_CTX(13);
	DUMP_VEC_CTX(14);
	DUMP_VEC_CTX(15);
	DUMP_VEC_CTX(16);
	DUMP_VEC_CTX(17);
	DUMP_VEC_CTX(18);
	DUMP_VEC_CTX(19);
	DUMP_VEC_CTX(20);
	DUMP_VEC_CTX(21);
	DUMP_VEC_CTX(22);
	DUMP_VEC_CTX(23);
	DUMP_VEC_CTX(24);
	DUMP_VEC_CTX(25);
	DUMP_VEC_CTX(26);
	DUMP_VEC_CTX(27);
	DUMP_VEC_CTX(28);
	DUMP_VEC_CTX(29);
	DUMP_VEC_CTX(30);
	DUMP_VEC_CTX(31);

	printk("r(FCSR) =[%08x]\n", context->fpcs);
	return;
}

/*******************************************************************************
 * 
 * 打印任务的矢量上下文
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无 
 * 备注：
 *      如果任务ID为空，则打印当前任务的矢量上下文。
 *      如果任务正在运行，那么打印出的是其上次被调度时的矢量上下文。
 */
#define thread_is_valid(ptr) ((((long)ptr)& 0x80000000) != 0)
void task_vec_context_show(thread_t _thread_ptr)
{
	if (vec_module_init_flag != TRUE) //有矢量支持
	{
		printk("vector does not support!!!\n");
		return;
	}
	extern thread_t thread_current_get();
	thread_t thread_ptr = (_thread_ptr ? _thread_ptr : thread_current_get());
	
	if (thread_is_valid(thread_ptr) !=  TRUE)
	{
		printk("task id[0x%8x] is invalid!\n", thread_ptr);
		return;
	}
	
	if (thread_is_vec(thread_ptr) == TRUE) //任务含矢量属性
	{
		Vec_Context *vec_context = *reference_thread_vec_context_ptr(thread_ptr);
		if (vec_context)
		{
			vec_context_dump(vec_context);
		}
		else
		{
			printk("Failed to get vector context of task[0x%8x]!\n", thread_ptr);			
		}
	}
	else
	{
		printk("No vector context found， task[0x%8x] is not vector task!\n", thread_ptr);
	}
}

/*******************************************************************************
 * 
 * 打印任务上下文的某个矢量寄存器值
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 		regno: 寄存器的编号
 * 输出：
 * 		无
 * 返回：
 * 		无 
 * 备注：
 *      如果任务ID为空，则打印当前任务的寄存器值。
 *      如果任务正在运行，那么打印出的是其上次被调度时的寄存器值。
 *      
 */
void task_vec_reg_show(thread_t _thread_ptr, u8 regno)
{
	if (vec_module_init_flag != TRUE) //有矢量支持
	{
		printk("vector does not support!!!\n");
		return;
	}
	if (regno >= NUM_VEC_REGS)
	{
		printk("regno[%d] exceed max num of vec regs[%d]!!!\n", regno, NUM_VEC_REGS);
		return;
	}
	
	extern thread_t thread_current_get();
	thread_t thread_ptr = (_thread_ptr ? _thread_ptr : thread_current_get());
	
	if (thread_is_valid(thread_ptr) !=  TRUE)
	{
		printk("task id[0x%8x] is invalid!\n", thread_ptr);
		return;
	}
	
	if (thread_is_vec(thread_ptr) == TRUE) //任务含矢量属性
	{
		Vec_Context *vec_context = *reference_thread_vec_context_ptr(thread_ptr);
		vector_zr * vec_reg_ptr =  (vector_zr *)((vector_zr*)vec_context + regno);
		if (vec_context)
		{
			printk("vec%d:\n", regno);
			printk("%016x  %016x  %016x  %016x\n", 
					*(u64*)(vec_reg_ptr->vec_h2), 
					*(u64*)(vec_reg_ptr->vec_h1), 
						*(u64*)(vec_reg_ptr->vec_l2), 
						*(u64*)(vec_reg_ptr->vec_l1)); 
			
		}
		else
		{
			printk("Failed to get vector context of task[0x%8x]!\n", thread_ptr);			
		}
	}
	else
	{
		printk("No vector context found， task[0x%8x] is not vector task!\n", thread_ptr);
	}
}

/*******************************************************************************
 * 
 * 获取某个任务的矢量上下文
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 		regno: 寄存器的编号
 * 输出：
 * 		无
 * 返回：
 * 		无 
 * 备注：
 *      如果任务ID为空，则获取的是当前任务上下文的矢量寄存器值。
 *      如果任务正在运行，那么获取的是其上次被调度时的矢量上下文的寄存器值。
 */
Vec_Context * task_vec_context_get(thread_t _thread_ptr)
{
	if (vec_module_init_flag != TRUE) //有矢量支持
	{
		return NULL;
	}
	extern thread_t thread_current_get();
	thread_t thread_ptr = (_thread_ptr ? _thread_ptr : thread_current_get());
	
	if (thread_is_valid(thread_ptr) !=  TRUE) {
		return NULL;
	}
	
	if (thread_is_vec(thread_ptr) == TRUE) //任务含矢量属性
	{
		return (*reference_thread_vec_context_ptr(thread_ptr));	
	}
	return NULL;
}

/*******************************************************************************
 * 
 * 获取某个任务上下文的某个矢量寄存器值
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无 
 * 备注：
 *      如果任务ID为空，则获取的是当前任务的矢量上下文。
 *      如果任务正在运行，那么获取的是其上次被调度时的矢量上下文。
 */
vector_zr * task_vec_reg_get(thread_t _thread_ptr, u8 regno)
{
	if (vec_module_init_flag != TRUE) //有矢量支持
	{
		return NULL;
	}
	if (regno >= NUM_VEC_REGS)
	{
		return NULL;
	}
	
	extern thread_t thread_current_get();
	thread_t thread_ptr = (_thread_ptr ? _thread_ptr : thread_current_get());
	
	if (thread_is_valid(thread_ptr) !=  TRUE)
	{
		return NULL;
	}
	
	if (thread_is_vec(thread_ptr) == TRUE) //任务含矢量属性
	{
		Vec_Context *vec_context = *reference_thread_vec_context_ptr(thread_ptr);
		return  (vector_zr *)((vector_zr*)vec_context + regno);
	}
	return NULL;
}
/*******************************************************************************
 * 
 * 初始化矢量模块
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序尚未实现注册TCB矢量上下文字段和钩子函数失败的处理
 */
int vec_module_init()
{
	/* 初始化标识检查 */
	if (vec_module_init_flag)
	{
		return 0;
	}
	
	//初始化任务钩子模块
	extern void thread_hook_init();
	thread_hook_init();

	/* 初始化矢量协处理器 */
	vec_init();

	/* 向TCB注册矢量上下文字段 */
	if (add_registered_field("vec_context_ptr", NULL, &vec_context_ptr_offset) != 0)
	{
		return -1;	
	}
//	printk("vec_context_ptr_offset=%d\n", vec_context_ptr_offset);

	/* 注册钩子：创建、切换、删除 */
	if (pthread_create_hook_add(vec_create_hook) != 0)
	{
		return -1;
	}
    if (pthread_switch_hook_add(vec_switch_hook) != 0)
    {
    	return -1;
    }
    if (pthread_close_hook_add(vec_delete_hook) != 0)
    {
    	return -1;
    }
	
    /* 设置初始化标识 */
    vec_module_init_flag = 1;
    
    return 0;
}
