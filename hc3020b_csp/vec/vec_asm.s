/*
 * vec_asm.s
 */
 

#define ASM_LANGUAGE
#define _ASMLANGUAGE

#include <mips_asm.h>
#include <gs464_cpu.h>
#include <asm_linkage.h>



#define ST0_CH          0x00040000
#define ST0_NMI         0x00080000
#define ST0_SR          0x00100000
#define ST0_TS          0x00200000
#define ST0_BEV         0x00400000
#define ST0_RE          0x02000000
#define ST0_FR          0x04000000
#define ST0_CU          0xf0000000
#define ST0_CU0         0x10000000
#define ST0_CU1         0x20000000
#define ST0_CU2         0x40000000
#define ST0_CU3         0x80000000
#define ST0_XX          0x80000000  /* MIPS IV naming */


#define CP2_VIR                 $0
#define CP2_VFCR                $1
#define CP2_VENR                $2
#define CP2_VFR                 $3
#define CP2_VSCR                $4
#define CP2_VCCR                $8


#define VECTOR_OFFSET_VCCR      (32 * (256 / 8)) 
#define VECTOR_VSCR_FS          (1 << 8)   

/* 32 bit register operations */
#define NOP	nop
#define ADD	add
#define STREG	sw
#define LDREG	lw
#define MFCO	mfc0
#define MTCO	mtc0
#define ADDU	add
#define ADDIU	addi
#define R_SZ	4
#define F_SZ	8
#define SZ_INT	4
#define SZ_INT_POW2 2


#ifdef __GNUC__
#define ASM_EXTERN(x,size) .extern x,size
#else
#define ASM_EXTERN(x,size)
#endif

#define FRAME(name,frm_reg,offset,ret_reg)	\
        .globl  name;						\
        .ent    name;						\
name:;										\
        .frame  frm_reg,offset,ret_reg

#define ENDFRAME(name)						\
        .end name

/*
 *  vector_init
 */
.section .bss
    .balign     32
mipsHr2VectorRegZero:
    .space      32
LEAF(vector_init)
	.set noreorder
	.set noat
    .set mips64
    .align   4
    mfc0	t1, C0_SR  /* SR -> t1 */  
    lui		t0, 0x4000 
    or		t0, t0, t1 
    mtc0	t0, C0_SR  /* t0 -> SR */ 
	sync
	
    dla  	t0,mipsHr2VectorRegZero 

    VLDDQ   $z0  , 0(t0)
    VLDDQ   $z1  , 0(t0)
    VLDDQ   $z2  , 0(t0)
    VLDDQ   $z3  , 0(t0)
    VLDDQ   $z4  , 0(t0)
    VLDDQ   $z5  , 0(t0)
    VLDDQ   $z6  , 0(t0)
    VLDDQ   $z7  , 0(t0)
    VLDDQ   $z8  , 0(t0)
    VLDDQ   $z9  , 0(t0)
    VLDDQ   $z10 , 0(t0)
    VLDDQ   $z11 , 0(t0)
    VLDDQ   $z12 , 0(t0)
    VLDDQ   $z13 , 0(t0)
    VLDDQ   $z14 , 0(t0)
    VLDDQ   $z15 , 0(t0)
    VLDDQ   $z16 , 0(t0)
    VLDDQ   $z17 , 0(t0)
    VLDDQ   $z18 , 0(t0)
    VLDDQ   $z19 , 0(t0)
    VLDDQ   $z20 , 0(t0)
    VLDDQ   $z21 , 0(t0)
    VLDDQ   $z22 , 0(t0)
    VLDDQ   $z23 , 0(t0)
    VLDDQ   $z24 , 0(t0)
    VLDDQ   $z25 , 0(t0)
    VLDDQ   $z26 , 0(t0)
    VLDDQ   $z27 , 0(t0)
    VLDDQ   $z28 , 0(t0)
    VLDDQ   $z29 , 0(t0)
    VLDDQ   $z30 , 0(t0)
    VLDDQ   $z31 , 0(t0)

    ctc2 	zero, CP2_VCCR     

    li      t0 , VECTOR_VSCR_FS     
    ctc2   	t0, CP2_VSCR

    mtc0	t1, C0_SR  /* t0 -> SR */ 
    sync
     
    j		ra
	nop
    .set reorder
    .set at
ENDPROC(vector_init)
 
/*
 *  mips_vec_context_save
 *
 *  This routine is responsible for saving the VEC context
 *  at *vec_context_ptr.  If the point to load the VEC context
 *  from is changed then the pointer is modified by this routine.
 *
 *  Sometimes a macro implementation of this is in cpu.h which dereferences
 *  the ** and a similarly named routine in this file is passed something
 *  like a (Context_Control_vec *).  The general rule on making this decision
 *  is to avoid writing assembly language.
 */

/* void mips_vec_context_save(
 *   Vec_Context *vec_context_ptr
 * );
 */
FRAME(mips_vec_context_save,sp,0,ra)
        .set noreorder
        .set noat
        .set mips64
        .align   4

	mfc0	t0, C0_SR  /* SR -> t0 */     
	lui		t1, 0x4000   
    or		t0, t0, t1 
    mtc0	t0, C0_SR  /* t0 -> SR */                                      

	sync

		VSTDQ  $z0,0*32(a0)
		VSTDQ  $z1,1*32(a0)
		VSTDQ  $z2,2*32(a0)
		VSTDQ  $z3,3*32(a0)
		VSTDQ  $z4,4*32(a0)
		VSTDQ  $z5,5*32(a0)
		VSTDQ  $z6,6*32(a0)
		VSTDQ  $z7,7*32(a0)
		VSTDQ  $z8,8*32(a0)
		VSTDQ  $z9,9*32(a0)
		VSTDQ  $z10,10*32(a0)
		VSTDQ  $z11,11*32(a0)		
		VSTDQ  $z12,12*32(a0)
		VSTDQ  $z13,13*32(a0)
		VSTDQ  $z14,14*32(a0)
		VSTDQ  $z15,15*32(a0)		
		VSTDQ  $z16,16*32(a0)
		VSTDQ  $z17,17*32(a0)
		VSTDQ  $z18,18*32(a0)
		VSTDQ  $z19,19*32(a0)
		VSTDQ  $z20,20*32(a0)
		VSTDQ  $z21,21*32(a0)
		VSTDQ  $z22,22*32(a0)
		VSTDQ  $z23,23*32(a0)
		VSTDQ  $z24,24*32(a0)
		VSTDQ  $z25,25*32(a0)
		VSTDQ  $z26,26*32(a0)
		VSTDQ  $z27,27*32(a0)		
		VSTDQ  $z28,28*32(a0)
		VSTDQ  $z29,29*32(a0)
		VSTDQ  $z30,30*32(a0)
		VSTDQ  $z31,31*32(a0)	
				
	cfc2   t1, CP2_VCCR
	sw     t1, VECTOR_OFFSET_VCCR(a0) 
	
        j ra
        NOP
        .set reorder
        .set at
ENDFRAME(mips_vec_context_save)


/*
 *  mips_vec_context_restore
 *
 *  This routine is responsible for restoring the VEC context
 *  at *vec_context_ptr.  If the point to load the VEC context
 *  from is changed then the pointer is modified by this routine.
 *
 *  Sometimes a macro implementation of this is in cpu.h which dereferences
 *  the ** and a similarly named routine in this file is passed something
 *  like a (Context_Control_vec *).  The general rule on making this decision
 *  is to avoid writing assembly language.
 */

/* void mips_vec_context_restore(
 *   Vec_Context *vec_context_ptr
 * )
 */
FRAME(mips_vec_context_restore,sp,0,ra)
        .set noat
        .set noreorder
        .set mips64
        .align   4
        
    mfc0	t0, C0_SR  /* SR -> t0 */    
    lui	t1, 0x4000
    or	t0, t0, t1 
    mtc0	t0, C0_SR  /* t0 -> SR */                                          
      
    sync
        
        VLDDQ $z0,0*32(a0)
        VLDDQ $z1,1*32(a0)
        VLDDQ $z2,2*32(a0)
        VLDDQ $z3,3*32(a0)
        VLDDQ $z4,4*32(a0)
        VLDDQ $z5,5*32(a0)
        VLDDQ $z6,6*32(a0)
        VLDDQ $z7,7*32(a0)
        VLDDQ $z8,8*32(a0)
        VLDDQ $z9,9*32(a0)
        VLDDQ $z10,10*32(a0)
        VLDDQ $z11,11*32(a0)
        VLDDQ $z12,12*32(a0)
        VLDDQ $z13,13*32(a0)
        VLDDQ $z14,14*32(a0)
        VLDDQ $z15,15*32(a0)
        VLDDQ $z16,16*32(a0)
        VLDDQ $z17,17*32(a0)
        VLDDQ $z18,18*32(a0)
        VLDDQ $z19,19*32(a0)
        VLDDQ $z20,20*32(a0)
        VLDDQ $z21,21*32(a0)
        VLDDQ $z22,22*32(a0)
        VLDDQ $z23,23*32(a0)
        VLDDQ $z24,24*32(a0)
        VLDDQ $z25,25*32(a0)
        VLDDQ $z26,26*32(a0)
        VLDDQ $z27,27*32(a0)
        VLDDQ $z28,28*32(a0)
        VLDDQ $z29,29*32(a0)
        VLDDQ $z30,30*32(a0)
        VLDDQ $z31,31*32(a0)
	

	lw      t1, VECTOR_OFFSET_VCCR(a0)
    ctc2    t1, CP2_VCCR
	sync
	
        j ra
        NOP
        .set reorder
        .set at
ENDFRAME(mips_vec_context_restore)


