#ifndef _VEC_H_
#define _VEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  mips/fpu.h for loongson2
 *
 */
#include <cpu.h>
#include <reworks/types.h>

#ifndef ASM_LANGUAGE	

#define NUM_VEC_REGS	32

//QuesIt:这里可以改为u64, FuKai,2014.9.1
#if 1
typedef struct zr
{
	u8 vec_l1[8];		//矢量寄存器低128位
	u8 vec_l2[8];
	u8 vec_h1[8];		//矢量寄存器高128位
	u8 vec_h2[8];
}vector_zr;
#else
typedef struct zr
{
	u64 vec_l1;		//矢量寄存器低128位
	u64 vec_l2;
	u64 vec_h1;		//矢量寄存器高128位
	u64 vec_h2;
}vector_zr;
#endif


//定义64个矢量寄存器的结构体
typedef struct _Vec_Context{
	vector_zr vec0;
	vector_zr vec1;
	vector_zr vec2;
	vector_zr vec3;
	vector_zr vec4;
	vector_zr vec5;
	vector_zr vec6;
	vector_zr vec7;
	vector_zr vec8;
	vector_zr vec9;
	vector_zr vec10;
	vector_zr vec11;
	vector_zr vec12;
	vector_zr vec13;
	vector_zr vec14;
	vector_zr vec15;
	vector_zr vec16;
	vector_zr vec17;
	vector_zr vec18;
	vector_zr vec19;
	vector_zr vec20;
	vector_zr vec21;
	vector_zr vec22;
	vector_zr vec23;
	vector_zr vec24;
	vector_zr vec25;
	vector_zr vec26;
	vector_zr vec27;
	vector_zr vec28;
	vector_zr vec29;
	vector_zr vec30;
	vector_zr vec31;

	u32 fpcs;
} VEC_CONTEXT;

typedef VEC_CONTEXT Vec_Context;


#define VECREG_SET VEC_CONTEXT

/*
 *  mips_vec_context_save
 *
 *  This routine saves the vector context passed to it.
 */
void mips_vec_context_save(Vec_Context *Vec_context_ptr);

/*
 *  mips_vec_context_save
 *
 *  This routine restores the vector context passed to it.
 */
void mips_vec_context_restore(Vec_Context *Vec_context_ptr);

void vec_init(void);

void vec_context_dump(Vec_Context *context);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif
