/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块初始化接口
 * 修改：
 * 		 2011-11-03，唐立三，规范 
 * 
 */
#include <reworks/types.h>
#include <reworks/printk.h>
#include "cpu.h"



extern int(*context_ctrl_to_reg_set_func)();
extern int context_ctrl_to_reg_set(REG_SET *regs, Context_Ctrl *sp);
extern void (*compute_sys_freq_func)(); 
extern void compute_sys_freq( void );
extern unsigned int bsp_clicks_per_usec;
extern u32 get_c0_sr();
extern void set_c0_sr(u32 val);
extern sys_cfg_tbl *sys_cfg_ptr;


#ifdef INCLUDE_UNAME
#include <osinfo.h>

#if defined(HUARUI2_SMP_64)
/**
 * 模块信息
 */
static MOD_INFO hr2_csp_info;

static int hr2_csp_info_init_flag = 0;


int hr2_csp_info_init(void)
{
	if (hr2_csp_info_init_flag)
	{
		return 0;
	}
	
	/* 清理 */
	memset(&hr2_csp_info, 0, sizeof(MOD_INFO));
	
	hr2_csp_info.name			= "CSP";
	hr2_csp_info.version		= MODULE_VERSION;
	hr2_csp_info.subversion	= MODULE_SUBVERSION;
	hr2_csp_info.builddate		= __DATE__;
	hr2_csp_info.buildtime		= __TIME__;
	hr2_csp_info.description	= "HUARUI2 Multicore CPU Support Package";
	
	/* 注册 */
	register_module_info(&hr2_csp_info);
	
	hr2_csp_info_init_flag = 1;
	
	return 0;
}
#elif defined(HUARUI3_SMP_64)
/**
 * 模块信息
 */
static MOD_INFO hr3_csp_info;

static int hr3_csp_info_init_flag = 0;


int hr3_csp_info_init(void)
{
	if (hr3_csp_info_init_flag)
	{
		return 0;
	}

	/* 清理 */
	memset(&hr3_csp_info, 0, sizeof(MOD_INFO));

	hr3_csp_info.name			= "CSP";
	hr3_csp_info.version		= MODULE_VERSION;
	hr3_csp_info.subversion	= MODULE_SUBVERSION;
	hr3_csp_info.builddate		= __DATE__;
	hr3_csp_info.buildtime		= __TIME__;
	hr3_csp_info.description	= "HUARUI3 Multicore CPU Support Package";

	/* 注册 */
	register_module_info(&hr3_csp_info);

	hr3_csp_info_init_flag = 1;

	return 0;
}
#endif /*HUARUI2_SMP_64*/


#endif

/*******************************************************************************
 * 
 * CSP模块初始化接口
 * 
 * 		CSP模块初始化接口在BSP模块初始化之前调用。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无 
 */
void csp_module_init(void)
{
//	fpu_init();
	u32 val;
	context_ctrl_to_reg_set_func = context_ctrl_to_reg_set; 

#ifndef __HUARUI3__
	/*打开协处理2，以使能SIMD*/
	val = get_c0_sr();
	val |= (1 << 30);
    set_c0_sr(val);
#endif

	
	/*添加tls支持(同ls64位)*/
    extern int tls_support_flag;
    tls_support_flag = 1;
    
    extern VOIDFUNCPTR tls_switch_hook_mem_op;
    extern void ls_tls_switch_mem_op(void *mem);
    tls_switch_hook_mem_op = ls_tls_switch_mem_op;
    
#ifdef INCLUDE_UNAME

#if defined(HUARUI2_SMP_64)
	hr2_csp_info_init();
#elif defined(HUARUI3_SMP_64)
	hr3_csp_info_init();
#endif

#endif
	return;
}
