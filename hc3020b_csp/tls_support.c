#define __write_64bit_c0_register(reg,sel,value)\
do{\
if(sizeof(unsigned long)==4)\
	printf("NOT SUPPORTED, STOP");\
else if(sel==0)\
	__asm__ __volatile__(\
	".set push\n"\
	".set mips3\n"\
	"dmtc0 %z0,"#reg"\n"\
	".set pop"\
	::"Jr"(value));\
else\
	__asm__ __volatile__(\
	".set push\n"\
	".set mips64\n"\
	"dmtc0 %z0,"#reg","#sel"\n"\
	".set pop"\
	::"Jr"(value));\
}while(0)

#define __write_32bit_c0_register(reg,sel,value)\
do{\
if(sel==0)\
	__asm__ __volatile__("mtc0 %z0, "#reg" \n"	::"Jr"((unsigned int)(value)));\
else\
	__asm__ __volatile__(\
".set push\n"\
".set mips32\n"\
"mtc0 %z0,"#reg","#sel" \n"\
".set pop"\
::"Jr"((unsigned int)(value)));\
}while(0)

#define __write_ulong_c0_register(reg,sel,val) do{\
	if(sizeof(unsigned long)==4)\
		__write_32bit_c0_register(reg,sel,val);\
	else\
		__write_64bit_c0_register(reg,sel,val);\
}while(0)

#define write_c0_userlocal(val) __write_ulong_c0_register($4,2,val)

#define TP_ADJ(p) ((char *)(p) + 0x7000)

void ls_tls_switch_mem_op(void *mem)
{
	write_c0_userlocal(TP_ADJ(mem));
}
