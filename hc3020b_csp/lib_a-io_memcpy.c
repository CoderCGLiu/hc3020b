/*
FUNCTION
        <<memcpy>>---copy memory regions

ANSI_SYNOPSIS
        #include <string.h>
        void* memcpy(void *<[out]>, const void *<[in]>, size_t <[n]>);

TRAD_SYNOPSIS
        void *memcpy(<[out]>, <[in]>, <[n]>
        void *<[out]>;
        void *<[in]>;
        size_t <[n]>;

DESCRIPTION
        This function copies <[n]> bytes from the memory region
        pointed to by <[in]> to the memory region pointed to by
        <[out]>.

        If the regions overlap, the behavior is undefined.

RETURNS
        <<memcpy>> returns a pointer to the first byte of the <[out]>
        region.

PORTABILITY
<<memcpy>> is ANSI C.

<<memcpy>> requires no supporting OS subroutines.

QUICKREF
        memcpy ansi pure
	*/

#include <_ansi.h>
#include <stddef.h>
#include <limits.h>

#define __64BIT_SUPPORT__

#ifdef __64BIT_SUPPORT__
#define		FULL_TYPE	long long
#define		HALF_TYPE	long
#define		MIN_TYPE	short
#else
#define		FULL_TYPE	long
#define		HALF_TYPE	short
#define		MIN_TYPE	char
#endif

#define		CHAR_TYPE	char

//1表示源地址与目的地址偏差相同
#define UNALIGNED_SYNC(X, Y)\
	(((FULL_TYPE)(X) & (sizeof (FULL_TYPE) - 1)) == ((FULL_TYPE)(Y) & (sizeof (FULL_TYPE) - 1)))

//0表示X是按long边界对齐的
#define NOT_ALIGNED(X) ((FULL_TYPE)(X) & (sizeof (FULL_TYPE) - 1))
//0表示半字对齐，即两字节对齐
#define NOT_HALF_ALIGNED(X) ((HALF_TYPE)(X) & (sizeof(HALF_TYPE) - 1))
//0表示最小类型对齐
#define NOT_MIN_ALIGNED(x)	((MIN_TYPE)(x) & (sizeof(MIN_TYPE) - 1))
/* Nonzero if either X or Y is not aligned on a "long" boundary.  */
#define UNALIGNED(X, Y) \
  (((FULL_TYPE)X & (sizeof (FULL_TYPE) - 1)) | ((FULL_TYPE)Y & (sizeof (FULL_TYPE) - 1)))

/* How many bytes are copied each iteration of the 4X unrolled loop.  */
#define BIGBLOCKSIZE    (sizeof (FULL_TYPE) << 2)

/* How many bytes are copied each iteration of the word copy loop.  */
#define LITTLEBLOCKSIZE (sizeof (FULL_TYPE))

#define HALF_BLOCK_SIZE (LITTLEBLOCKSIZE / sizeof(HALF_TYPE))

/* Threshhold for punting to the byte copier.  */
#define TOO_SMALL(LEN)  ((LEN) < BIGBLOCKSIZE)

#define TYPE_BIT(type)	(8*sizeof(type))
#define CHAR_TYPE_BIT	TYPE_BIT(CHAR_TYPE)
#define MIN_TYPE_BIT	TYPE_BIT(MIN_TYPE)
#define HALF_TYPE_BIT	TYPE_BIT(HALF_TYPE)
#define FULL_TYPE_BIT	TYPE_BIT(FULL_TYPE)

#ifdef __ARCH_BIG_ENDIAN__
#ifdef __64BIT_SUPPORT__
#define	MAKE_CHAR_TO_FULL(src)	((FULL_TYPE)*(src)<<(7*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+1)<<(6*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(5*CHAR_TYPE_BIT)\
		| (FULL_TYPE)*((src)+3)<<(4*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+4)<<(3*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+5)<<(2*CHAR_TYPE_BIT) \
		| (FULL_TYPE)*((src)+6)<<(1*CHAR_TYPE_BIT) | (FULL_TYPE)*((src)+7))
#endif
#define MAKE_MIN_TO_FULL(src)	((FULL_TYPE)*(src)<<(3*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+1)<<(2*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(1*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+3))
#define MAKE_HALF_TO_FULL(src)	((FULL_TYPE)*(src)<<(1*HALF_TYPE_BIT) | (FULL_TYPE)*((src)+1))
#else
#ifdef __64BIT_SUPPORT__
#define	MAKE_CHAR_TO_FULL(src)	((FULL_TYPE)*(src) | ((FULL_TYPE)*((src)+1))<<(1*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+2))<<(2*CHAR_TYPE_BIT) \
		| ((FULL_TYPE)*((src)+3))<<(3*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+4))<<(4*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+5))<<(5*CHAR_TYPE_BIT) \
		| ((FULL_TYPE)*((src)+6))<<(6*CHAR_TYPE_BIT) | ((FULL_TYPE)*((src)+7))<<(7*CHAR_TYPE_BIT))
#endif
#define MAKE_MIN_TO_FULL(src)	((FULL_TYPE)*(src) | (FULL_TYPE)*((src)+1)<<(1*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+2)<<(2*MIN_TYPE_BIT) | (FULL_TYPE)*((src)+3)<<(3*MIN_TYPE_BIT))
#define MAKE_HALF_TO_FULL(src)	((FULL_TYPE)*(src) | (FULL_TYPE)*((src)+1)<<(1*HALF_TYPE_BIT))
#endif

_PTR
_DEFUN (io_memcpy, (dst0, src0, len0),
	_PTR dst0 _AND
	_CONST _PTR src0 _AND
	size_t len0)
{
#if defined(PREFER_SIZE_OVER_SPEED) || defined(__OPTIMIZE_SIZE__)
  char *dst = (char *) dst0;
  char *src = (char *) src0;

  _PTR save = dst0;

  while (len0--)
    {
      *dst++ = *src++;
    }

  return save;
#else
  CHAR_TYPE *dst = dst0;
  _CONST unsigned CHAR_TYPE *src = src0;
  FULL_TYPE *aligned_dst;
  _CONST FULL_TYPE *aligned_src;
  int   len =  len0;

  /* If the size is small, or either SRC or DST is unaligned,
     then punt into the byte copy loop.  This should be rare.  */
  //if (!TOO_SMALL(len) && !UNALIGNED (src, dst))
//	if (!TOO_SMALL(len) && !UNALIGNED_SYNC (src, dst))
	if (UNALIGNED_SYNC (src, dst))
    {
		while(NOT_ALIGNED(dst) && (len > 0))
		{
			*dst++ = *src++;
			len--;
		}
      aligned_dst = (FULL_TYPE *)dst;
      aligned_src = (FULL_TYPE *)src;

     
      /* Copy 4X long words at a time if possible.  */
      while (len >= BIGBLOCKSIZE)
        {
          *aligned_dst++ = *aligned_src++;
          *aligned_dst++ = *aligned_src++;
          *aligned_dst++ = *aligned_src++;
          *aligned_dst++ = *aligned_src++;
          len -= BIGBLOCKSIZE;
        }

      /* Copy one long word at a time if possible.  */
      while (len >= LITTLEBLOCKSIZE)
        {
          *aligned_dst++ = *aligned_src++;
          len -= LITTLEBLOCKSIZE;
        }
      
      /* len < sizeof(FULL_TYPE) */
      
       /* Pick up any residual with a byte copier.  */
      dst = (CHAR_TYPE *)aligned_dst;
      src = (CHAR_TYPE *)aligned_src;
    }
	/* 对齐情况不同。 */
	else
	{
		/* 逐个拷贝，直至dst满足FULL_TYPE对齐 */
		while(NOT_ALIGNED(dst) && (len > 0))
		{
			*dst++ = *src++;
			len--;
		}
      aligned_dst = (FULL_TYPE*)dst;
      
      /* 这里要加代码 */
      /* 64位：非32位对齐 
       * 32位：非16位对齐
       */
      if(NOT_HALF_ALIGNED(src))
      {
    	  /* 单个字节拷贝吧！ */
    	  if (NOT_MIN_ALIGNED(src))
    	  {
    		  while (len >= BIGBLOCKSIZE)
			  {  			  
				  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  len -= BIGBLOCKSIZE;
			  }
    		  

			/* Copy one long word at a time if possible.  */
			  while (len >= LITTLEBLOCKSIZE)
				{
				  *aligned_dst++ = MAKE_CHAR_TO_FULL((unsigned CHAR_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  len -= LITTLEBLOCKSIZE;
				}
    	  }

          /* Copy 4X long words at a time if possible.  */
    	  else
    	  {
    		  while (len >= BIGBLOCKSIZE)
    		  {
				  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  len -= BIGBLOCKSIZE;
    		  }

          /* Copy one long word at a time if possible.  */
			  while (len >= LITTLEBLOCKSIZE)
				{
				  *aligned_dst++ = MAKE_MIN_TO_FULL((unsigned MIN_TYPE *)src);
				  src += LITTLEBLOCKSIZE;
				  len -= LITTLEBLOCKSIZE;
				}
    	  }
      }
      else
      {
    	  _CONST unsigned HALF_TYPE * half_ali_src = (unsigned HALF_TYPE *)src;
          /* Copy 4X long words at a time if possible.  */
          while (len >= BIGBLOCKSIZE)
            {
              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
              half_ali_src += HALF_BLOCK_SIZE;
              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
              half_ali_src += HALF_BLOCK_SIZE;
              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
              half_ali_src += HALF_BLOCK_SIZE;
              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
              half_ali_src += HALF_BLOCK_SIZE;
              len -= BIGBLOCKSIZE;
            }

          /* Copy one long word at a time if possible.  */
          while (len >= LITTLEBLOCKSIZE)
            {
              *aligned_dst++ = MAKE_HALF_TO_FULL(half_ali_src);
              half_ali_src += HALF_BLOCK_SIZE;
              len -= LITTLEBLOCKSIZE;
            }
          /* len < LITTLEBLOCKSIZE */
          src = (char *)half_ali_src;
      }

       /* Pick up any residual with a byte copier.  */
      dst = (char*)aligned_dst;
	}
  while (len--)
  {
    *dst++ = *src++;
  }
  return dst0;
#endif /* not PREFER_SIZE_OVER_SPEED */
}

