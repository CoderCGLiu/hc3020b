#define ASM_LANGUAGE
#define _ASMLANGUAGE
//hw
//#include "mips_asm.h"
//#include "cpu.h"
#include <mips_asm.h>
#include <gs464_cpu.h>
#include <asm_linkage.h>
//hw
/************************************************
 * C函数原型：u32 get_c0_epc();
 *
 * 这是一个叶子函数，它用于取得状态寄存器的值。
 ***********************************************/
LEAF(get_c0_epc)
	dmfc0	v0, C0_EPC /* SR -> v0 */
	j		ra
ENDPROC(get_c0_epc)

/************************************************
 * C函数原型：u32 get_c0_sr();
 *
 * 这是一个叶子函数，它用于取得状态寄存器的值。
 ***********************************************/
LEAF(get_c0_sr)
	mfc0	v0, C0_SR /* SR -> v0 */
	j		ra
ENDPROC(get_c0_sr)

/************************************************
 * C函数原型：void set_c0_sr(u32);
 *
 * 这是一个叶子函数，它用于设置状态寄存器的值。
 ***********************************************/
LEAF(set_c0_sr)
	mtc0	a0, C0_SR /* a0 -> SR */
	sync
	j		ra
ENDPROC(set_c0_sr)


/************************************************
 * C函数原型：u32 get_c0_count();
 *
 * 这是一个叶子函数，它用于取得计数寄存器的值。
 ***********************************************/
LEAF(get_c0_count)
	mfc0	v0, C0_COUNT /* COUNT -> v0 */
	j		ra
ENDPROC(get_c0_count)

/************************************************
 * C函数原型：u32 set_c0_count();
 *
 * 这是一个叶子函数，它用于设置计数寄存器的值。
 ***********************************************/
LEAF(set_c0_count)
	mtc0	a0, C0_COUNT /* a0 -> C0_COUNT */
	j		ra
ENDPROC(set_c0_count)

/************************************************
 * C函数原型：u32 set_c0_compare();
 *
 * 这是一个叶子函数，它用于设置compare寄存器的值。
 ***********************************************/
LEAF(set_c0_compare)
	mtc0	a0, C0_COMPARE /* a0 -> C0_COMPARE */
	j		ra
ENDPROC(set_c0_compare)
LEAF(get_c0_compare)
	mfc0	v0, C0_COMPARE /* a0 -> C0_COMPARE */
	j		ra
ENDPROC(get_c0_compare)
/************************************************
 * C函数原型：u32 get_c0_config();
 *
 * 这是一个叶子函数，它用于取得配置寄存器的值。
 ***********************************************/
LEAF(get_c0_config)
	mfc0	v0, C0_CONFIG /* CONFIG -> v0 */
	j		ra
ENDPROC(get_c0_config)

/************************************************
 * C函数原型：void set_c0_config(u32);
 *
 * 这是一个叶子函数，它用于设置配置寄存器的值。
 ***********************************************/
LEAF(set_c0_config)
	mtc0	a0, C0_CONFIG
	sync
	j		ra
ENDPROC(set_c0_config)

/************************************************
 * C函数原型：u32 get_c0_cause();
 *
 * 这是一个叶子函数，它用于取得原因寄存器的值。
 ***********************************************/
LEAF(get_c0_cause)
	mfc0	v0, C0_CAUSE /* CAUSE -> v0 */
	j		ra
ENDPROC(get_c0_cause)

LEAF(set_c0_cause)
	mtc0	a0, C0_CAUSE
	j		ra
ENDPROC(set_c0_cause)

LEAF(get_c0_ebase)
	mfc0	v0,$15,1
	j		ra
ENDPROC(get_c0_ebase)

LEAF(set_c0_ebase)
	mtc0	a0,$15,1
	j		ra
ENDPROC(set_c0_ebase)

/************************************************
 * C函数原型：u32 get_c0_perf_ctl();
 *
 * 这是一个叶子函数，它用于取得原因寄存器的值。
 ***********************************************/
LEAF(get_c0_perf_ctl)
	mfc0	v0, C0_PERF_CTL /* PERF_CTL -> v0 */
	j		ra
ENDPROC(get_c0_perf_ctl)

/************************************************
 * C函数原型：u32 read_c0_index();
 *
 * 这是一个叶子函数，它用于取得索引寄存器的值。
 ***********************************************/
LEAF(read_c0_index)
	mfc0	v0, C0_INDEX /* INDEX -> v0 */
	j		ra
ENDPROC(read_c0_index)

/************************************************
 * C函数原型：void write_c0_index(u32);
 *
 * 这是一个叶子函数，它用于设置索引寄存器的值。
 ***********************************************/
LEAF(write_c0_index)
	mtc0	a0, C0_INDEX
	j		ra
ENDPROC(write_c0_index)

/************************************************
 * C函数原型：u32 read_c0_entryhi();
 *
 * 这是一个叶子函数，它用于取得表项高位寄存器的值。
 ***********************************************/
LEAF(read_c0_entryhi)
	dmfc0	v0, C0_ENTRYHI /* ENTRYHI -> v0 */
	j		ra
ENDPROC(read_c0_entryhi)

/************************************************
 * C函数原型：void write_c0_entryhi(u32);
 *
 * 这是一个叶子函数，它用于设置表项高位寄存器的值。
 ***********************************************/
LEAF(write_c0_entryhi)
	dmtc0	a0, C0_ENTRYHI
	j		ra
ENDPROC(write_c0_entryhi)

/************************************************
 * C函数原型：u32 read_c0_entrylo0();
 *
 * 这是一个叶子函数，它用于取得表项偶数页低位寄存器的值。
 ***********************************************/
LEAF(read_c0_entrylo0)
	dmfc0	v0, C0_ENTRYLO0 /* ENTRYLO0 -> v0 */
	j		ra
ENDPROC(read_c0_entrylo0)

/************************************************
 * C函数原型：void write_c0_entrylo0(u32);
 *
 * 这是一个叶子函数，它用于设置表项偶数页低位寄存器的值。
 ***********************************************/
LEAF(write_c0_entrylo0)
	dmtc0	a0, C0_ENTRYLO0 /* a0 -> ENTRYLO0 */
	j		ra
ENDPROC(write_c0_entrylo0)

/************************************************
 * C函数原型：u32 read_c0_entrylo1();
 *
 * 这是一个叶子函数，它用于取得表项奇数页低位寄存器的值。
 ***********************************************/
LEAF(read_c0_entrylo1)
	dmfc0	v0, C0_ENTRYLO1 /* ENTRYLO1 -> v0 */
	j		ra
ENDPROC(read_c0_entrylo1)

/************************************************
 * C函数原型：void write_c0_entrylo1(u32);
 *
 * 这是一个叶子函数，它用于设置表项奇数页低位寄存器的值。
 ***********************************************/
LEAF(write_c0_entrylo1)
	dmtc0	a0, C0_ENTRYLO1 /* a0 -> ENTRYLO1 */
	j		ra
ENDPROC(write_c0_entrylo1)

/************************************************
 * C函数原型：u32 read_c0_pagemask();
 *
 * 这是一个叶子函数，它用于取得页掩码寄存器的值。
 ***********************************************/
LEAF(read_c0_pagemask)
	dmfc0	v0, C0_PAGEMASK /* PAGEMASK -> v0 */
	j		ra
ENDPROC(read_c0_pagemask)

/************************************************
 * C函数原型：void write_c0_pagemask(u32);
 *
 * 这是一个叶子函数，它用于设置页掩码寄存器的值。
 ***********************************************/
LEAF(write_c0_pagemask)
	dmtc0	a0, C0_PAGEMASK /* a0 -> PAGEMASK */
	j		ra
ENDPROC(write_c0_pagemask)

/************************************************
 * C函数原型：u64 read_c0_badvaddr();
 *
 * 这是一个叶子函数，它用于取得页掩码寄存器的值。
 ***********************************************/
LEAF(read_c0_badvaddr)
	dmfc0	v0, C0_BADVADDR /* BADVADDR -> v0 */
	j		ra
ENDPROC(read_c0_badvaddr)

/************************************************
 * C函数原型：void write_c0_random(u32);
 *
 * 这是一个叶子函数，它用于设置TLB随机表项索引寄存器的值。
 ***********************************************/
LEAF(write_c0_random)
	mtc0	a0, C0_RANDOM /* a0 -> RANDOM */
	sync
	j		ra
ENDPROC(write_c0_random)

/************************************************
 * C函数原型：void write_c0_wired(u32);
 *
 * 这是一个叶子函数，它用于设置TLB固定表项索引寄存器的值。
 ***********************************************/
LEAF(write_c0_wired)
	mtc0	a0, C0_WIRED /* a0 -> WIRED */
	sync
	j		ra
ENDPROC(write_c0_wired)

/************************************************
 * C函数原型：u32 read_c0_watchlo();
 *
 * 这是一个叶子函数，它用于取得监视点寄存器的值。
 ***********************************************/
LEAF(read_c0_watchlo)
#ifdef _BAO_MOD
	dmfc0	v0, C0_WATCHLO /* WATCHLO -> v0 */
#else
	mfc0	v0, C0_WATCHLO /* WATCHLO -> v0 */
#endif
	j		ra
ENDPROC(read_c0_watchlo)

/************************************************
 * C函数原型：void write_c0_watchlo(u32);
 *
 * 这是一个叶子函数，它用于设置监视点寄存器的值。
 ***********************************************/
LEAF(write_c0_watchlo)
#ifdef _BAO_MOD
	dmtc0	a0, C0_WATCHLO
#else
	mtc0	a0, C0_WATCHLO
#endif
	j		ra
ENDPROC(write_c0_watchlo)

#ifdef _BAO_MOD
LEAF(read_c0_watchhi)
	mfc0	v0, C0_WATCHHI /* WATCHHI -> v0 */
	j		ra
ENDPROC(read_c0_watchhi)

LEAF(write_c0_watchhi)
	mtc0	a0, C0_WATCHHI
	j		ra
ENDPROC(write_c0_watchhi)
#endif /*_BAO_MOD*/

#if 0
/************************************************
 * C函数原型：void udelay(u32);
 *
 * 这是一个叶子函数，它用于等待若干微秒的时间。220  delay 1微秒
 * 备注：2015-05-27 姚秋果 该接口实现不够准确，故舍弃
 ***********************************************/
LEAF(udelay)
	.set noreorder
	li		a1, 199
	mul     a0, a0, a1

1:
	bnez	a0, 1b
	sub		a0, 1
	j		ra
	nop
	.set reorder
ENDPROC(udelay)

#endif

#if 0
LEAF(mips64_writelong)
	                 
	.set noreorder 
	.set 	mips64
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	sd	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writelong)	


LEAF(mips64_readlong)
	                 
	.set noreorder 
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	ld	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_readlong)	
#endif


LEAF(mips64_writelong)
	                 
	.set noreorder 
	.set 	mips64
	li t0, 3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
	nop
	
	dli t1,0x9000100000000000
	
	1:
	or  t1,t1,a1
	sd	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
ENDPROC(mips64_writelong)

LEAF(mips64_readlong)
	                 
	.set noreorder 
	li t0,3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
    nop
    
    dli t1,0x9000100000000000
    
    1:
	or  t1,t1,a1
	ld	v0, 0(t1)
	sync
	j ra
	nop
	.set reorder
ENDPROC(mips64_readlong)	

LEAF(cpu_id_get)
	                 
	.set noreorder 
	mfc0 t0,$15,1
   /* andi t0, 0x3ff*/
   /* andi  t0,0x3 */
   /* ls dual 3a */
   andi t0, 0x7
    move v0,t0 
	j ra
	nop
	.set reorder
	
ENDPROC(cpu_id_get)	

LEAF(cp0_ebase_get)
	                 
	.set noreorder 
	mfc0 t0,$15,1
    move v0,t0 
	j ra
	nop
	.set reorder
	
ENDPROC(cp0_ebase_get)	

LEAF(cp0_ebase_set)
	                 
	.set noreorder 
	mtc0 a0,$15,1
	j ra
	nop
	.set reorder
	
ENDPROC(cp0_ebase_set)	


#if 0
LEAF(mips64_write_HT_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFB000000
	or  t0,t0,a0
	sw	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_write_HT_reg)	

LEAF(mips64_read_HT_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFB000000
	or  t0,t0,a0
	lw	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_read_HT_reg)	

LEAF(mips64_writeb)
	                 
	.set noreorder 
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	sb	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writeb)	

LEAF(mips64_readb)
	                 
	.set noreorder 
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	lb	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_readb)	

LEAF(mips64_writew)
	                 
	.set noreorder 
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	sw	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writew)	

LEAF(mips64_readw)
	                 
	.set noreorder 
	dli	t0,0x9000000000000000
	or  t0,t0,a0
	lw	v0, 0(t0)
	sync
	j ra
	.set reorder
	
ENDPROC(mips64_readw)
#endif

LEAF(mips64_write_HT_reg)
	                 
	.set noreorder
	li t0,3 
	dli	t1,0x90000EFDFB000000
	ble a0,t0,1f
	nop
	dli t1,0x90001EFDFB000000
	1:
	or  t1,t1,a1
	sw	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_write_HT_reg)

LEAF(mips64_read_HT_reg)
	                 
	.set noreorder 
	li t0, 3
	dli	t1,0x90000EFDFB000000
	ble a0, t0, 1f
	nop
	dli	t1,0x90001EFDFB000000
	1:
	or  t1,t1,a1
	lw	v0, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_read_HT_reg)	

LEAF(mips64_writeb)
	                 
	.set noreorder 
	li t0, 3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
	nop
	dli t1,0x9000100000000000
	1:
	or  t1,t1,a1
	sb	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writeb)	

LEAF(mips64_readb)
	                 
	.set noreorder 
	li t0,3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
	nop
	dli t1,0x9000100000000000
	
	1:
	or  t1,t1,a1
	lb	v0, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_readb)	

LEAF(mips64_writew)
	                 
	.set noreorder
	li t0,3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
	nop
	dli	t1,0x9000100000000000
	1:
	or  t1,t1,a1
	sw	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writew)	

LEAF(mips64_readw)
	                 
	.set noreorder 
	li t0,3
	dli	t1,0x9000000000000000
	ble a0,t0,1f
	nop
	dli t1,0x9000100000000000
	
	1:
	or  t1,t1,a1
	lw	v0, 0(t1)
	sync
	j ra
	.set reorder
	
ENDPROC(mips64_readw)

//////////////////////////////
LEAF(mips64_writeb1)
	                 
	.set noreorder 
	li t0, 3
	dli t1,0x9000100000000000
	or  t1,t1,a1
	sb	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writeb1)


LEAF(mips64_readb1)
	                 
	.set noreorder 
	li t0,3
	dli t1,0x9000100000000000
	
	or  t1,t1,a1
	lb	v0, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_readb1)
/////////////////////////////////////////////////////////xyj
LEAF(mips_64addr_writew)
	                 
	.set noreorder 
	sw	a1, 0(a0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips_64addr_writew)

LEAF(mips_64addr_readw)
	                 
	.set noreorder 
	lw	v0, 0(a0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips_64addr_readw)

LEAF(mips_64addr_writeb)
	                 
	.set noreorder 
	sb	a1, 0(a0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips_64addr_writeb)

LEAF(mips_64addr_readb)
	                 
	.set noreorder 
	lb	v0, 0(a0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips_64addr_readb)





//////////////////////////////////////////////////////////////////////////////////////

LEAF(mips64_writew1)
	                 
	.set noreorder
	li t0,3
	dli	t1,0x9000100000000000
	or  t1,t1,a1
	sw	a2, 0(t1)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_writew1)	

LEAF(mips64_write1_HT_reg)
	                 
	.set noreorder 
	dli	t0,0x90001EFDFB000000
	or  t0,t0,a0
	sw	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_write1_HT_reg)
//////////////////////////////

LEAF(MIPS_SW64_lyj_HT1LO_PCICFG_BASE)
	                 
	.set noreorder 
	dli	t0,0x90000efdfe000000
	or  t0,t0,a0
	sw	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
END(MIPS_SW64_lyj_HT1LO_PCICFG_BASE)	

LEAF(MIPS_SW64_lyj_HT1LO_PCICFG_BASE_TP1)
	                 
	.set noreorder 
	dli	t0,0x90000efdff000000
	or  t0,t0,a0
	sw	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
END(MIPS_SW64_lyj_HT1LO_PCICFG_BASE_TP1)	

LEAF(MIPS_LW64_lyj_HT1LO_PCICFG_BASE)
	                 
	.set noreorder 
	dli	t0,0x90000efdfe000000
	or  t0,t0,a0
	lw	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
END(MIPS_LW64_lyj_HT1LO_PCICFG_BASE)	

LEAF(MIPS_LW64_lyj_HT1LO_PCICFG_BASE_TP1)
	                 
	.set noreorder 
	dli	t0,0x90000efdff000000
	or  t0,t0,a0
	lw	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
END(MIPS_LW64_lyj_HT1LO_PCICFG_BASE_TP1)	

LEAF(mips64_write_htuart_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFC000000 /*0x90000EFDFC000000*/
	or  t0,t0,a0
	sb	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_write_htuart_reg)	

LEAF(mips64_read_htuart_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFC000000
	or  t0,t0,a0
	lb	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_read_htuart_reg)	

LEAF(mips64_writeb_htio_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFC000000 /*0x90000EFDFC000000*/
	or  t0,t0,a0
	sb	a1, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_write_htuart_reg)	

LEAF(mips64_readb_htio_reg)
	                 
	.set noreorder 
	dli	t0,0x90000EFDFC000000
	or  t0,t0,a0
	lb	v0, 0(t0)
	sync
	j ra
	nop
	.set reorder
	
ENDPROC(mips64_read_htuart_reg)	

/*原来版本下列几句放在stat.s中-tintin*/
LEAF(thread_idle_entry)
	.set noreorder
	b	thread_idle_entry
	nop
	.set reorder
ENDPROC(thread_idle_entry)

LEAF(thread_idle_entry1)
	.set noreorder
	b	thread_idle_entry1
	nop
	.set reorder
END(thread_idle_entry1)
LEAF(read_c0_cf1)
	mfc0	v0, $16,1
	j		ra
ENDPROC(read_c0_cf1)


LEAF(sys_timestamp_init)
	.set noreorder 
	dmtc0 zero,$25,1 
	mtc0 a0,$25,0
	j ra
	nop
	.set reorder
ENDPROC(sys_timestamp_init)

	.ent reworks_thread
reworks_thread:
	.globl reworks_thread
	nop
ENDPROC(reworks_thread)
.ent reworks_signal
reworks_signal:
	.globl reworks_signal
	nop
ENDPROC(reworks_signal)

#if 0
LEAF(sys_timestamp)
   .set noreorder
    dmfc0	t0,$25,1
    dsrl32	v1,t0,0x0
    li		t1,0xffffffff
    and		v0,t0,t1
    j ra
    nop
    .set reorder
ENDPROC(sys_timestamp) 

   
LEAF(sys_timestamp_sync)
   .set noreorder
   	sync
    dmfc0 t0,$25,1
    dsrl v1,t0,32
    li t1, -1
    and v0,t0,t1
    j ra
    nop
    .set reorder
ENDPROC(sys_timestamp_sync)
#endif


LEAF(get_fcsr)
	.set noreorder
	cfc1 v0, $31
	cfc1 v0, $31
	nop
	nop
	j ra
	nop
	.set reorder
ENDPROC(get_fcsr)


LEAF(set_fcsr)
	.set noreorder
	ctc1 a0, $31
	ctc1 a0, $31
	sync
	nop
	j ra
	nop
	.set reorder
ENDPROC(set_fcsr)
/************************************************
 * C函数原型：u32 get_current_sp();
 *
 * 这是一个叶子函数，它用于取得栈指针的值。
 ***********************************************/
LEAF(get_current_sp)
	move	v0, sp /* sp -> v0 */
	j		ra
ENDPROC(get_current_sp)


LEAF(get_c0_prid)
	mfc0	v0, C0_PRID
	j		ra
ENDPROC(get_c0_prid)


LEAF(MIPS_SW64)
	.set noreorder
	dli t0,0x9000000000000000
	dli t2,0x00000000ffffffff
	and t2,t2,a0
	or t0,t0,t2
	sw a1,0(t0)
	.set reorder
	j ra
ENDPROC(MIPS_SW64)


LEAF(MIPS_LW64)
	.set noreorder
	dli t0,0x9000000000000000
	dli t2,0x00000000ffffffff
	and t2,t2,a0
	or t0,t0,t2
	lw v0,0(t0)
	.set reorder
	j ra
ENDPROC(MIPS_LW64)


LEAF(MIPS_WR64)
	.set noreorder
	move t0,a0
	dsll32 t0,t0,0
	dli t1,0x00000000ffffffff
	and t1,t1,a1
	or t0,t0,t1
	dsll32 t1,a2,0
	dli t2,0x00000000ffffffff
	and t2,a3,t2
	or t1,t1,t2
	sd t1,0(t0)
	.set reorder
	j ra
ENDPROC(MIPS_WR64)


LEAF(MIPS_RD64)
	.set noreorder
	move t0,a0
	dsll32 t0,t0,0
	dli t1,0x00000000ffffffff
	and t1,t1,a1
	or t0,t0,t1
	ld t1,0(t0)
	sw t1,0(a3)
	dsrl32 t1,t1,0
	sw t1,0(a2)
	.set reorder
	j ra
ENDPROC(MIPS_RD64)
