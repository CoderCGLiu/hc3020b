
//#include "reg_def.h"
#define _ASMLANGUAGE
#include <regdef.h>
#include <asm_linkage.h>

/* void *memcpy(void *s1, const void *s2, size_t n);  */

	.text
	.globl	bcopy
	.globl	memmove
	
	.align	5
	.ent	bcopy,0
	.set	noreorder
bcopy:
	move	a3, a0
	move	a0, a1
	j		memmove
	move	a1, a3
	.set	reorder
//	.end	bcopy
ENDPROC(bcopy)	

	.align	5	
	.ent	memmove,0
	.set	noreorder	
	.set	mips64	
memmove:

	bgtu	 a0,  a1, L_back_to_forward
	/* check if the copy size is less than 8. if so, we just copy byte by byte */
	slti	 t0,  a2, 8		 
	bne	 t0,  zero, L_less_8_byte
	move	 v0,  a0				/* exit value */

	/* check the last 3 bits of the address to see if the s1 & s2 are aligned. */
	xor	 t0,  a1,  a0		
	andi	 t0, 0x7
	bne	 t0,  zero, L_unaligned	/* unaligned case */

	/* the last 3 bits of s1 & s2 are the same. now check if the start address
	   is in the middle of a long long word */
	li	 t2,  65500
	bge	 a2,  t2, L_big_num
	andi	 t2,  a1, 0x7
	beq	 t2,  zero, L_cp_4_word	/* we are starting from the start of a long long word */
	li	 t1,  8
	dsub     t1,  t1,  t2

	/* we are starting from the middle, just copy until we meet the next aligned adress */
	dsubu	 a2,  t1
	ldr	 t0, 0( a1)		
	daddu	 a1,  t1
	sdr	 t0, 0( a0)
	daddu	 a0,  t1

	/* now copy 32 bytes(2 long long words) once */
L_cp_4_word:	
	/* check if there are more than 32bytes remaining */
	andi	 t0,  a2, 0x1f			/* 32 bytes mask : 11111 */
	beq	 t0,  a2, L_cp_1_word
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		 	/*  a3 : end address of loop */
	move	 a2,  t0			

L_loop_cp_4_word:	
	ldr	 t0,  0( a1)		
	ldr	 t1,  8( a1)	
	ldr	 t2,  16( a1)	
	ldr	 t3,  24( a1)	

	daddiu	 a0, 32
	daddiu	 a1, 32

	sdr	 t0, -32( a0)
	sdr	 t1, -24( a0)
	sdr	 t2, -16( a0)
	sdr	 t3,  -8( a0)
	bne	 a1,  a3, L_loop_cp_4_word
	nop

	/* less than 32 bytes remaining, copy one long long word once */
L_cp_1_word:	
	andi	 t0,  a2, 0x7		
	beq	 t0,  a2, L_less_8_byte
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		
	move	 a2,  t0

L_loop_cp_1_word:	
	ld	 t0, 0( a1)
	daddiu	 a0, 8
	daddiu	 a1, 8	
	sd	 t0, -8( a0)
	bne	 a1,  a3, L_loop_cp_1_word
	nop

	/* less than 8 bytes remaining, copy one byte once */
L_less_8_byte:	
	blez	 a2, L_finish	
	daddu	 a3,  a2,  a1
L_loop_cp_1_byte:	
	lb	 t0, 0( a1)
	daddiu	 a0, 1
	daddiu	 a1, 1
	sb	 t0, -1( a0)
	bne	 a1,  a3, L_loop_cp_1_byte
	nop

L_finish:	
	jr	 ra			/* exit */
	nop

L_big_num:
#	andi	 t2,  a1, 0x7
	li	 t1,  8
	dsub     t1,  t1,  t2
	beq	 t1,  zero, L_big_cp_8_word	/* we are starting from the start of a long long word */

	/* we are starting from the middle, just copy until we meet the next aligned adress */
	dsubu	 a2,  t1
	ldr	 t0, 0( a1)		
	daddu	 a1,  t1
	sdr	 t0, 0( a0)
	daddu	 a0,  t1

	/* now copy 64 bytes(8 long long words) once */
L_big_cp_8_word:	
	/* check if there are more than 64bytes remaining */
	andi	 t0,  a2, 0x3f			/* 64 bytes mask : 111111 */
	beq	 t0,  a2, L_big_cp_1_word
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		 	/*  a3 : end address of loop */
	move	 a2,  t0			

L_big_loop_cp_8_word:	
	ld	 t0,  0( a1)		
	ld	 t1,  8( a1)	
	ld	 t2,  16( a1)	
	ld	 t3,  24( a1)	
	ld	 t4,  32( a1)		
	ld	 t5,  40( a1)	
	ld	 t6,  48( a1)	
	ld	 t7,  56( a1)	

	daddiu	 a0, 64
	daddiu	 a1, 64

	sd	 t0, -64( a0)
	sd	 t1, -56( a0)
	sd	 t2, -48( a0)
	sd	 t3, -40( a0)
	sd	 t4, -32( a0)
	sd	 t5, -24( a0)
	sd	 t6, -16( a0)
	sd	 t7, -8( a0)
	bne	 a1,  a3, L_big_loop_cp_8_word
	nop

	/* less than 64 bytes remaining, copy one long long word once */
L_big_cp_1_word:	
	andi	 t0,  a2, 0x7		
	beq	 t0,  a2, L_big_less_8_byte
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		
	move	 a2,  t0

L_big_loop_cp_1_word:	
	ld	 t0, 0( a1)
	daddiu	 a0, 8
	daddiu	 a1, 8	
	sd	 t0, -8( a0)
	bne	 a1,  a3, L_big_loop_cp_1_word
	nop

	/* less than 8 bytes remaining, copy one byte once */
L_big_less_8_byte:	
	blez	 a2, L_big_finish	
	daddu	 a3,  a2,  a1
L_big_loop_1_byte:	
	lb	 t0, 0( a1)
	daddiu	 a0, 1
	daddiu	 a1, 1
	sb	 t0, -1( a0)
	bne	 a1,  a3, L_big_loop_1_byte
	nop

L_big_finish:	
	jr	 ra			/* exit */
	nop

	/* handle the unaligned case, use unaligned load/store instructions. 
	   s1 & s2 are both unaligned. we can only make one of them aligned */
L_unaligned:	
	andi	 t0,  a0, 0x7
	li	 a3,  8
	dsub	 a3,  a3, t0
	beq	 a3, 0, Lun_cp_1_word
	dsubu	 a2,  a3
	ldr	 t0, 0( a1)	
	ldl	 t0, 7( a1)
	daddu	 a1,  a3
	sdr	 t0, 0( a0)
	daddu	 a0,  a3


	/* now copy 32 bytes(4 long long words) once */
Lun_cp_8_word:	
	/* check if there are more than 32bytes remaining */
	andi	 t0,  a2, 0x1f			/* 32 bytes mask : 11111 */
	beq	 t0,  a2, Lun_cp_1_word
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		 	/*  a3 : end address of loop */
	move	 a2,  t0			

Lun_loop_8_word:	
	uld	 t0,  0( a1)		
	uld	 t1,  8( a1)	
	uld	 t2, 16( a1)
	uld	 t3, 24( a1)

	daddiu	 a0, 32
	daddiu	 a1, 32

	sdr	 t0, -32( a0)
	sdr	 t1, -24( a0)
	sdr	 t2, -16( a0)
	sdr	 t3,  -8( a0)
	bnel	 a1,  a3, Lun_loop_8_word
	nop


	/* copy one long long word once */
Lun_cp_1_word:	
	andi	 t0,  a2, 0x7
	beq	 t0,  a2, L_less_8_byte
	dsubu	 a3,  a2,  t0	
	daddu	 a3,  a1		
	move	 a2,  t0

Lun_loop_1_word:	
	ldr	 t1, 0( a1)		
	ldl	 t1, 7( a1)
	daddiu	 a0, 8
	daddiu	 a1, 8
	sdr	 t1, -8( a0)
	bne	 a1,  a3, Lun_loop_1_word
	nop

	/* less than 8 bytes left */
	b	L_less_8_byte	

	move	 a2,  t0
L_back_to_forward:
	move	 v0,  a0				/* exit value */
	daddu	 a0,  a2
	daddu	 a1,  a2

	slti	 t0,  a2, 8		 
	bne	 t0,  zero, L_bf_less_8_byte

	/* check the last 3 bits of the address to see if the s1 & s2 are aligned. */
	xor	 t0,  a1,  a0		
	andi	 t0, 0x7
	bne	 t0,  zero, L_bf_unaligned	/* unaligned case */

	/* the last 3 bits of s1 & s2 are the same. now check if the start address
	   is in the middle of a long long word */
	li	 t2,  65500
	bge	 a2,  t2, L_bf_big_num
	andi	 t2,  a1, 0x7
	beq	 t2,  zero, L_bf_cp_4_word	/* we are starting from the start of a long long word */

	/* we are starting from the middle, just copy until we meet the next aligned adress */
	dsubu	 a2,  t2
	ldl	 t0, -1( a1)		
	dsubu	 a1,  t2
	sdl	 t0, -1( a0)
	dsubu	 a0,  t2

	/* now copy 32 bytes(4 long long words) once */
L_bf_cp_4_word:	
	/* check if there are more than 32 bytes remaining */
	andi	 t0,  a2, 0x1f			/* 32 bytes mask : 11111 */
	beq	 t0,  a2, L_bf_cp_1_word
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		 	/*  a3 : end address of loop */
	move	 a2,  t0			

L_bf_loop_cp_4_word:	
	ldr	 t0,  -8( a1)		
	ldr	 t1,  -16( a1)	
	ldr	 t2,  -24( a1)	
	ldr	 t3,  -32( a1)	

	daddiu	 a0, -32
	daddiu	 a1, -32

	sdr	 t3,  0( a0)
	sdr	 t2,  8( a0)
	sdr	 t1,  16( a0)
	sdr	 t0,  24( a0)
	bne	 a1,  a3, L_bf_loop_cp_4_word
	nop

	/* less than 32 bytes remaining, copy one long long word once */
L_bf_cp_1_word:	
	andi	 t0,  a2, 0x7		
	beq	 t0,  a2, L_bf_less_8_byte
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		
	move	 a2,  t0

L_bf_loop_cp_1_word:	
	ld	 t0, -8( a1)
	daddiu	 a0, -8
	daddiu	 a1, -8	
	sd	 t0, 0( a0)
	bne	 a1,  a3, L_bf_loop_cp_1_word
	nop

	/* less than 8 bytes remaining, copy one byte once */
L_bf_less_8_byte:	
	blez	 a2, L_bf_finish	
	dsubu	 a3,  a1,  a2
L_bf_loop_cp_1_byte:	
	lb	 t0, -1( a1)
	daddiu	 a0, -1
	daddiu	 a1, -1
	sb	 t0, 0( a0)
	bne	 a1,  a3, L_bf_loop_cp_1_byte
	nop

L_bf_finish:	
	jr	 ra			/* exit */
	nop

L_bf_big_num:
	beq	 t2,  zero, L_bf_big_cp_8_word	/* we are starting from the start of a long long word */

	/* we are starting from the middle, just copy until we meet the next aligned adress */
	dsubu	 a2,  t2
	ldl	 t0, -1( a1)		
	dsubu	 a1,  t2
	sdl	 t0, -1( a0)
	dsubu	 a0,  t2

	/* now copy 64 bytes(8 long long words) once */
L_bf_big_cp_8_word:	
	/* check if there are more than 64bytes remaining */
	andi	 t0,  a2, 0x3f			/* 64 bytes mask : 111111 */
	beq	 t0,  a2, L_bf_big_cp_1_word
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		 	/*  a3 : end address of loop */
	move	 a2,  t0			

L_bf_big_loop_cp_8_word:	
	ld	 t0,  -8( a1)		
	ld	 t1,  -16( a1)	
	ld	 t2,  -24( a1)	
	ld	 t3,  -32( a1)	
	ld	 t4,  -40( a1)		
	ld	 t5,  -48( a1)	
	ld	 t6,  -56( a1)	
	ld	 t7,  -64( a1)	

	daddiu	 a0, -64
	daddiu	 a1, -64

	sd	 t0,  56( a0)
	sd	 t1,  48( a0)
	sd	 t2,  40( a0)
	sd	 t3,  32( a0)
	sd	 t4,  24( a0)
	sd	 t5,  16( a0)
	sd	 t6,  8( a0)
	sd	 t7,  0( a0)
	bne	 a1,  a3, L_bf_big_loop_cp_8_word
	nop

	/* less than 64 bytes remaining, copy one long long word once */
L_bf_big_cp_1_word:	
	andi	 t0,  a2, 0x7		
	beq	 t0,  a2, L_bf_big_less_8_byte
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		
	move	 a2,  t0

L_bf_big_loop_cp_1_word:	
	ld	 t0, -8( a1)
	daddiu	 a0, -8
	daddiu	 a1, -8	
	sd	 t0, 0( a0)
	bne	 a1,  a3, L_bf_big_loop_cp_1_word
	nop

	/* less than 8 bytes remaining, copy one byte once */
L_bf_big_less_8_byte:	
	blez	 a2, L_bf_big_finish	
	dsubu	 a3,  a1,  a2
L_bf_big_loop_1_byte:	
	lb	 t0, -1( a1)
	daddiu	 a0, -1
	daddiu	 a1, -1
	sb	 t0, 0( a0)
	bne	 a1,  a3, L_bf_big_loop_1_byte
	nop

L_bf_big_finish:	
	jr	 ra			/* exit */
	nop

	/* handle the unaligned case, use unaligned load/store instructions. 
	   s1 & s2 are both unaligned. we can only make one of them aligned */
L_bf_unaligned:	
	andi	 a3,  a0, 0x7
	beq	 a3, 0, Lun_bf_cp_8_word
	dsubu	 a2,  a3
	ldr	 t0, -8( a1)	
	ldl	 t0, -1( a1)
	dsubu	 a1,  a3
	sdl	 t0, -1( a0)
	dsubu	 a0,  a3


	/* now copy 32 bytes(4 long long words) once */
Lun_bf_cp_8_word:	
	/* check if there are more than 64bytes remaining */
	andi	 t0,  a2, 0x1f			/* 32 bytes mask : 11111 */
	beq	 t0,  a2, Lun_bf_cp_1_word
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		 	/*  a3 : end address of loop */
	move	 a2,  t0			

Lun_bf_loop_8_word:	
	uld	 t0,  -8( a1)		
	uld	 t1,  -16( a1)	
	uld	 t2,  -24( a1)
	uld	 t3,  -32( a1)

	daddiu	 a0, -32
	daddiu	 a1, -32

	sdr	 t0,  24( a0)
	sdr	 t1,  16( a0)
	sdr	 t2,   8( a0)
	sdr	 t3,   0( a0)
	bnel	 a1,  a3, Lun_bf_loop_8_word
	nop


	/* copy one long long word once */
Lun_bf_cp_1_word:	
	andi	 t0,  a2, 0x7
	beq	 t0,  a2, L_bf_less_8_byte
	dsubu	 a3,  a2,  t0	
	dsubu	 a3,  a1,  a3		
	move	 a2,  t0

Lun_bf_loop_1_word:	
	ldr	 t1, -8( a1)		
	ldl	 t1, -1( a1)
	daddiu	 a0, -8
	daddiu	 a1, -8
	sdr	 t1, 0( a0)
	bne	 a1,  a3, Lun_bf_loop_1_word
	nop

	/* less than 8 bytes left */
	b	L_bf_less_8_byte	

	move	 a2,  t0

ENDPROC(memmove)
	.set	reorder
//libc_hidden_builtin_def (memmove)
