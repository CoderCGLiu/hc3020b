#include <stdio.h>
#include <time.h>
#include <wdg.h> /* sys_wdg_processing() */
#include <reworks/types.h>
#include <clock.h>
#include <reworks/printk.h>
#include <irq.h>
#include <cpu.h>
#include <gs464_cpu.h>

extern sys_cfg_tbl *sys_cfg_ptr;
extern void sys_wdg_processing(void *argu);
static boolean check_overrun(u32 compare, u32 new_compare, u32 pp_compare);

  
//u32 new_compare = 0;
static u32 cpu_cycles_first_tick = 0; /* 每秒钟第一个滴答的周期数 */
static u32 cpu_cycles_per_tick = 0; /* 每秒钟第一个滴答之后滴答的周期数 */
#ifdef __multi_core__
static u32 current_compare[MAX_SMP_CPUS] = {0}, previous_compare[MAX_SMP_CPUS] = {0};
static u64 big_count[MAX_SMP_CPUS] = {0};
static u64 count_high[MAX_SMP_CPUS]= {0};
static u32 count_last[MAX_SMP_CPUS]= {0};
#else
static u32 current_compare = 0, previous_compare = 0;
static u64 big_count = 0;
#endif
static u32 clock_nest_level = 0;

extern volatile u32	fpu_savectx_enable;


typedef void (* CDMA_PROC)();
CDMA_PROC g_cdma_proc = NULL;

#ifdef __multi_core__
extern volatile u32 isr_nest_level_smp[MAX_SMP_CPUS];
#else
extern volatile u32 isr_nest_level_up;
#endif

void clock_module_start();
#ifdef __multi_core__
void do_clock_per_cpu(void *arg);
#else
void do_clock(void *arg);
#endif
void sys_clock_on()
{

	return;
}

void sys_clock_off()
{
	return;
}

void sys_clock_start()
{
	clock_module_start();
	return;
}

u64 us_since_boot_get()
{

#define US_PER_SEC (1000000)
	u32 level;
	register u32 count;
	register u32 compare;
	register u64 big_count_snapshot;
	
	level = int_lock();
	
	count = get_c0_count();
#ifdef __multi_core__
	compare = previous_compare[0];
	big_count_snapshot = big_count[0];
#else
	compare = previous_compare;
	big_count_snapshot = big_count;
#endif
	
	int_unlock(level);
	
	return (big_count_snapshot + (u32)((s32)count - (s32)compare)) * US_PER_SEC / sys_cfg_ptr->sys_clock_freq; 
}

/* huangyuan20090825：不打算运行的时候变频的，使用配置值足矣。 */
u32 sys_timestamp_freq(void)
{
//	return sys_cfg_ptr->sys_clock_freq*2;
	return sys_cfg_ptr->sys_clock_freq;
}

extern u32 get_c0_count();
u64 sys_timestamp ()
{

	u64 time;
	u64 high;
	int level;
	
	
	level = int_cpu_lock();
	
	u32 count = get_c0_count();
	int cpuid = cpu_id_get();
	
		
	if(count < count_last[cpuid] )
	{
		count_high[cpuid]++;
	}

	count_last[cpuid] = count;
	
	time = (count_high[cpuid] <<32)|count;
	
	int_cpu_unlock(level);
	
	return time;
}

void sync_counter(int cpu_id)
{
    asm volatile
      	(        		
      		"mtc0	%0, $11\n\t" /* t0 -> compare */
            "mtc0	%0, $11\n\t" /* t0 -> compare */
      		"mtc0	$0, $9\n\t" /* zero -> count */
      		:
      		:"r"(cpu_cycles_first_tick)
      	);
				
	current_compare[cpu_id] = cpu_cycles_first_tick;
	previous_compare[cpu_id] = 0;
	count_high[cpu_id] = 0;
	count_last[cpu_id] = 0;

}

OS_STATUS clock_module_init()
{
//	printk("[ReWorks]:Clock Module Init Start\n\r");
	clock_on();

	if (int_install_handler("clk",INT_CLOCK,1, do_clock_per_cpu, (void *)INT_CLOCK) != OS_OK)
	{
		printk("int_install_handler is failed in %s\n",__FUNCTION__);
		return OS_ERROR;
	}
	
//	printk("int_install_handler: clock\n");
	int_enable_pic(INT_CLOCK);
//	printk("[ReWorks]:Clock Module Init Success!\n\r");
	return OS_OK;
}

/* huangyuan20090330：在锁中断的条件下调用。
 * 还要考虑的是：CPU频率发生变化；每秒时钟中断数的变化。 */
u32 sys_clock_rate_set(int ticks_per_second)
{
	u32 cpu_cycles_per_sec = sys_cfg_ptr->sys_clock_freq; //800 000 000即800MHZ
	register u32 ccpt;

	if (!cpu_cycles_per_sec || ticks_per_second <= 0)
	{
		return OS_ERROR;
	}

	ccpt = cpu_cycles_per_sec / ticks_per_second; //800 000 000/100（或者1000）
	
	if (ccpt < 10000)
	{
		/* 时钟中断间隔过小，可能来不及处理 */
		return OS_ERROR;
	}
	
	/* huangyuan20121207: 动态设频率，这里还需要仔细考虑。
	 * 比如秒和滴答的关系（start_flag）…… */
	cpu_cycles_per_tick = ccpt;
	
	cpu_cycles_first_tick = ccpt + (cpu_cycles_per_sec % ticks_per_second); 

	return OS_OK;
}

#ifdef __multi_core__
void clock_module_start_per_cpu(){
//	u32 first_tick_cycles = cpu_cycles_per_tick + cpu_cycles_remainder;
    asm volatile
       	(
       		"mtc0	%0, $11\n\t" /* t0 -> compare */
            "mtc0	%0, $11\n\t" /* t0 -> compare */
       		"mtc0	$0, $9\n\t" /* zero -> count */
       		:
       		:"r"(cpu_cycles_first_tick)
       	);
//    sys_timestamp_init(0xc000000f);
    sys_timestamp_init(0x80000000); //by FuKai,2018-8-14 用默认值初始化性能计数器，怀疑core1启动不了同初始化不对有关
}
#endif
void clock_module_start()
{
	u32 year, month, date, wday, hour, min, sec, sec_prev;
	u8	ahour, amin, asec, ctrl_b;
//	u32 first_tick_cycles = cpu_cycles_per_tick + cpu_cycles_remainder;
    struct tm the_tm;
    time_t t;
    struct timespec the_tp;

    asm volatile
       	(
       		"mtc0	%0, $11\n\t" /* t0 -> compare */
            "mtc0	%0, $11\n\t" /* t0 -> compare */
       		"mtc0	$0, $9\n\t" /* zero -> count */
       		:
       		:"r"(cpu_cycles_first_tick)
       	);
//    sys_timestamp_init(0xc000000f);//华睿2号不适用
    sys_timestamp_init(0x80000000);
#ifdef __multi_core__
    int i;
    for (i = 0; i < MAX_SMP_CPUS; ++i)
    {
    	current_compare[i] = cpu_cycles_first_tick;
    }
#else
    current_compare = cpu_cycles_first_tick;
#endif

}
extern u32 get_c0_cause();

#ifdef __multi_core__
void do_clock_per_cpu(void *arg)
#else
void do_clock(void *arg)
#endif
{
	int cpu = cpu_id_get();
	
	//处理CDMA
	if (g_cdma_proc)
	{
		g_cdma_proc();
	}	
	
	/* fukai,2018-7-6
	 * 用CAUSE寄存器的TI位来判断是否有时钟中断 */
	if ((get_c0_cause() & 0x40000000UL) == 0)
		return;

	register u32 compare;
	register u32 new_compare;
	register u32 pp_compare;
	u64 sum;
	u32 clock_nest_level = 0;
#ifdef __multi_core__
	static u32 start_flag[MAX_SMP_CPUS];
	static int start_flag_inited;
	if (!start_flag_inited)
	{
		int i;
		for (i = 0; i < MAX_SMP_CPUS; ++i)
		{
			start_flag[i] = 1;
		}
		start_flag_inited = 1;
	}
#else
	static u32 start_flag = 1;	
#endif
	
	while (1)
	{
#ifdef __multi_core__
		clock_tick_per_cpu(cpu);
#else
		clock_tick();
#endif
#ifdef __DEBUG__
		asm volatile
		(
			"mfc0	%0, $11\n\t" /* compare -> compare_value */
			:"=&r"(compare)
		);
#else
		
#endif
#ifdef __multi_core__
		compare = current_compare[cpu];
		pp_compare = previous_compare[cpu];
		previous_compare[cpu] = compare;
#else
		compare = current_compare;
		pp_compare = previous_compare;
		previous_compare = compare;
#endif
	
#ifdef __multi_core__
		if ((start_flag[cpu] %= sys_clk_rate_get()) == 0)
#else
		if ((start_flag %= sys_clk_rate_get()) == 0)
#endif
		{
			new_compare = (compare + cpu_cycles_first_tick)|1;
#ifdef __multi_core__
			big_count[cpu] += cpu_cycles_first_tick;
			start_flag[cpu] = 0;
#else
			big_count += cpu_cycles_first_tick;
			start_flag = 0;
#endif
		}
		else
		{
			new_compare = (compare + cpu_cycles_per_tick)|1;
#ifdef __multi_core__
			big_count[cpu] += cpu_cycles_per_tick;
			start_flag[cpu]++;
#else
			big_count += cpu_cycles_per_tick;
			start_flag++;
#endif
		}

		
#ifdef __multi_core__
		current_compare[cpu] = new_compare;
#else	
		current_compare = new_compare;
#endif
		
		int level;		
		level = int_cpu_lock();
		
        asm volatile
		(	
			"mtc0	%0, $11\n\t" /* t0 -> compare */
            "mtc0	%0, $11\n\t" /* t0 -> compare */
			:
			:"r"(new_compare)
		);

		int_cpu_unlock(level);
		
		if (check_overrun(compare, new_compare, pp_compare) == FALSE)
		{
			/* 当count寄存器没有溢出compare到new_compare之间的范围，
			 * 不需进行补全操作，结束compare寄存器更新处理流程。 */
			break;
		}
		
		/* 进行补全 */
		clock_nest_level++;
	}
	
#if 1
	// swei: 2013.4.1 性能测试时有时钟嵌套，去掉打印
	/* huangyuan20130402: 性能测试测试已修改，去除长时间锁中断，恢复打印 */
//	if (clock_nest_level)
//		serial_putdec(clock_nest_level);
#endif
}


static boolean check_overrun(u32 compare, u32 new_compare, u32 pp_compare)
{
	register u32 count;

	/* 获取count值 */
	asm volatile
	(
		"mfc0	%0, $9\n\t"
		:"=&r"(count)
	);

	
	//hewei	
	int cpuid = cpu_id_get();
	if(count < count_last[cpuid] )
	{
		count_high[cpuid]++;
	}
	count_last[cpuid] = count;
	
	
	/* 简单检测是否count值在中断处理期间溢出一个滴答范围 */
	if (compare < new_compare && count > compare && count < new_compare)
	{
		return FALSE;
	}
	else if (compare > new_compare && ((count < new_compare || count > compare)))
	{
		return FALSE;
	}

	/* 如果count在compare之前的一个滴答里 ，忽略不做补全操作。 */
	if (pp_compare < compare && count > pp_compare && count < compare)
	{
		return FALSE;
	}
	else if (pp_compare > compare && ((count < compare || count > pp_compare)))
	{
		return FALSE;
	}
	
	return TRUE;
}

