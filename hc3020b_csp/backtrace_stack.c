#include <stdio.h>
#include <reworks/types.h>
#include <reworks/printk.h>
#include <symtbl.h>

/*表示栈回溯中的ra_offset值未找到*/
#define INIT_OFFSET -1
/**
 * 符号解析钩子函数
 */
FUNCNAME_RESOLVE_HANDLER funcname_resolve_handler = NULL;

typedef enum {
	OTHERS = 0,
	JR_RA = 1,		/* jr ra */
	ADDIU_SP_SP_NEGATIVE = 2, /* addiu sp, sp, -xxx/daddiu sp, sp, -xxx */
	ADDIU_SP_SP_POSITIVE = 3, /* addiu sp, sp, xxx/daddiu sp, sp, xxx */
	SX_RA_SP = 4, /* sw/sd ra, xxx(sp) */
	ERET = 5, /* eret */
	JUMP = 6, /* j, jr */
	JR_K0 = 7 /* jr k0 */
} INST_TYPE;

typedef struct {
	u64 pc;
	u64 sp;
	u64 ra;
} my_frame;

/*与linux类似的追踪函数调用堆栈接口*/
int backtrace(void **buffer, int size);
char **backtrace_symbols(void **buffer, int size);
void backtrace_symbols_fd(void **buffer,int size,int fd);
boolean backtrace_print(char *print_type,int size);


static INST_TYPE disassemble_a_instruction(u64 code_addr)
{
//extern void *_text_start;
//	if (code_addr < (u32)&_text_start)
//	{
//		printk("code address invalid\n");
//		while(1);
//	}
	
	u32 code = *(u32 *)code_addr;
	
	if (code == 0x03e00008)
		return JR_RA;
	
	if (code == 0x03400008)
		return JR_K0;
	
	if (code == 0x42000018)
		return ERET;
	
	switch (code & 0xffff0000)
	{
	    case 0x67bd0000:
	    case 0x27bd0000:
			if (code & 0x00008000)
				return ADDIU_SP_SP_NEGATIVE;
			else
				return ADDIU_SP_SP_POSITIVE;
			break;
		case 0xffbf0000:
		case 0xafbf0000:
			return SX_RA_SP;
			break;
		default:
			break;
	}
	
/* 8.6.1 Instruction Encodings, 'See MIPS Run' 2nd Edition, p. 233 */
#define INST_OP_MASK       0xfc000000 /* instruction encoding table field 31-26 */
#define INST_OP_SHIFT      26

	u32 op = (code & INST_OP_MASK) >> INST_OP_SHIFT;
	
	if (op == 0)
	{
#define INST_SUBCODE_MASK  0x0000003f /* instruction encoding table field 5-0 */
#define INST_SUBCODE_SHIFT 0
		
		u32 subcode = (code & INST_SUBCODE_MASK) >> INST_SUBCODE_SHIFT;
		
		if (subcode == 8)
		{
			return JUMP; /* jr x(not ra) */
		}
//		else if (subcode == 9)
//			return JUMP; /* jalr x */
	}
	else if (op == 2)
		return JUMP; /* j XXX */
//	else if (op == 3)
//		return JUMP; /* jal XXX */
	
	return OTHERS;
}

/* 这个函数从任意一点（包括中断/异常现场）向上回溯一个栈帧 */
/* 回溯算法基于gcc编译器编译出来的几类函数进行分析：
 * 一、有栈分配（addiu sp,sp -xxx）有栈释放（addiu sp,sp,xxx），两者
 * 成对出现，以jr ra收尾。
 * 二、有栈分配有栈释放，但不以jr ra收尾的函数，即jr ra之后还有代码片断。
 * 三、有栈分配无栈释放，无jr ra收尾的函数的函数。函数多为一个循环体。
 * 四、无栈分配释放的，有jr ra结尾，函数不调用其他函数，称为叶子函数。
 * 其他，如编译为一个跳转代码片断的函数，没有栈分配释放，也无jr ra，我们
 * 的回溯算法无法分辨。
 * 
 * 我们的回溯算法一开始向前搜索。 
 * 如果搜索到了jr ra，我们再搜索它的延迟槽。如果其中搜索到栈释放指令，说明
 * 它是一个一型或二型函数，那么我们再向上回溯确定它的栈帧大小和ra寄存器保存
 * 的栈指针偏移量。如果没有搜索到栈释放指令，说明它可能是函数结尾或四型函数
 * ，或者是一个三型函数之后加四型函数。那么我们要进一步判断，判断搜索中是否
 * 有跳转指令，如果没有，可以判断为函数结尾或四型函数，搜索结束， 不需要确定
 * 栈帧大小和ra寄存器保存的栈指针偏移量；如果有，则向后搜索栈分配指令，如果
 * 期间搜索到栈释放或jr ra，则说明回溯到了上面的函数，判断为四型函数，这里
 * 有一种例外情况，参见代码中注释。如果没有搜索到栈释放或jr ra，则说明这是
 * 一个三型函数之后加四型函数，需要确定栈帧大小和ra寄存器保存的栈指针偏移量。
 * 这里还会有例外情况，是回溯算法解决不了的分辨问题，参见代码中注释。
 * 
 * 如果查到栈分配指令，期间没有跳转指令，说明是函数开头，搜索直接结束，不需
 * 要栈帧大小等内容。新创建未执行的线程栈的回溯就是如此。
 * 
 * 如果查到jr k0或eret，期间没有跳转指令，说明是手写的汇编代码，一般不会有
 * 回溯的可能，回溯到了 ，说明系统不太正常。这里停住。也有可能是之前三型函数
 * 向前查找，找到这里，这种概率不高，同样不处理，停住。
 * （需要补充一点是context_switch也是手写汇编，它是目前大多停下的任务回溯的
 * 起点，是能成功回溯起来的。按理说是不能正常回溯，之所以能成功回溯，得益于前
 * 后回汇编的布局，是其满足了向前搜索到jr ra的四型函数的条件，如果日后不能
 * 正常回溯，请在回溯算法中单独处理判断context_switch为四型函数。）
 *
 * 如果查到栈分配指令、jr k0或eret，期间有跳转指令。说明它可能是一个二型或三
 * 型函数。 那么就向后搜索jr ra或栈分配指令。如果搜到栈分配指令，说明它是一个
 * 三型函数，确定栈帧大小和ra寄存器保存的栈指针偏移量，搜索结束。如果搜到jr ra
 * 指令，说明它是二型函数最后一条jr ra之后的代码片断。再向后探测一条指令。如果
 * 整个搜索期间搜到栈释放指令，继续向后确定栈帧大小和ra寄存器保存的栈指针偏移量，
 * 否则没有，说明它是没有栈分配的四型函数，直接结束搜索。
 * 
 * 顶层栈帧向上回溯完毕。如果，找到ra寄存器保存的栈指针偏移量，通过栈指针取
 * 得栈上保存的ra寄存器的值，否则保存原ra值不变；如果找到栈帧大小，那么栈指
 * 针加上它，复原出上一函数调用时的栈指针，否则保留原值；将ra-8赋给pc值，复
 * 原出上一函数调用该函数的现场。
 */
static void unwind_first_frame(my_frame *mfp)
{
	u64 code_addr = mfp->pc;
	INST_TYPE type;
	s16 frame_size = 0, ra_offset = INIT_OFFSET;
	boolean jump_mark = FALSE, addiu_positive_mark = FALSE;
	
	while (1)
	{
		type = disassemble_a_instruction(code_addr);
		
		if (type == JR_RA)
		{
			/* 检查包括jr ra之后一条之后的指令中是否有栈释放指令
			 * 一般是针对优化过的代码 */
			u64 temp_code_addr = code_addr + 4;
			type = disassemble_a_instruction(temp_code_addr);
			
			if (type == ADDIU_SP_SP_POSITIVE)
				addiu_positive_mark = TRUE;
			
			if (addiu_positive_mark == TRUE)
			{
				/* 有栈释放指令，向前查找栈分配指令，确定栈帧大小 */
				do {
					code_addr -= 4;
					type = disassemble_a_instruction(code_addr);
					/* 找出栈帧中记录ra寄存器的相对sp偏移量 */
					if (type == SX_RA_SP)
					{
						ra_offset = (*(u32 *)code_addr) & 0x0000ffff;
					}
				} while (type != ADDIU_SP_SP_NEGATIVE);
				frame_size = (*(u32 *)code_addr) & 0x0000ffff;
			}
			else
			{
				if (jump_mark == FALSE)
				{
					/* 没有跳转，可以确定是叶子函数或是函数结尾  */
					break;
				}
					
				boolean jr_ra_mark = FALSE;
				
				/* 没有栈帧释放指令，表明函数可能是一个叶子函数 */
				do {
					code_addr -= 4;
					type = disassemble_a_instruction(code_addr);
					/* 找出栈帧中记录ra寄存器的相对sp偏移量 */
					if (type == SX_RA_SP)
					{
						ra_offset = (*(u32 *)code_addr) & 0x0000ffff;
					}
					else if (type == ADDIU_SP_SP_POSITIVE)
						addiu_positive_mark = TRUE;
					else if (type == JR_RA)
						jr_ra_mark = TRUE;
					
				} while (type != ADDIU_SP_SP_NEGATIVE);
				
				if ((addiu_positive_mark == FALSE) && (jr_ra_mark == FALSE))
				{
					/* 这是一个只有栈帧分配没有栈帧释放和jr ra的函数，它下面跟一个叶子函数，
					 * 修正原先叶子函数的判断 */
					frame_size = (*(u32 *)code_addr) & 0x0000ffff;
					
					/* 这里还有一种出错的情况，就是一个当前pc落一个有跳转指令叶子函数中的跳转
					 * 指令之前，前面跟了一个只有栈帧分配没有栈帧释放和jr ra的函数，已经无能
					 * 为力区分它是叶子函数了 */
				}
				else
				{
					/* 追查到了上面的函数，保持原有叶子函数的判断 */
					ra_offset = INIT_OFFSET;
					/* 这里还有一种出错的情况，pc落在有栈分配函数jr ra之后的
					 * 代码片断， 后面跟了一个叶子函数，这是会错误判断为叶子函数 */
				}
			}
			
			break;
		}

		if (type == ADDIU_SP_SP_NEGATIVE || type == ERET || type == JR_K0)
		{
			
			if (jump_mark == FALSE)
			{
				if (type == ERET || type == JR_K0)
				{
					/* 这里可能是上下文切换或异常处理相关的代码，不应该出现在这 */
					/* 也可能有例外，可能是一个没有jr ra的函数，下面跟一个eret或
					 * jr k0结束的函数，这时我们的回溯算法就出错了。 */
					printk("backtrace error 1\n");
					while(1);
				}
				else /* function prologue */
					break;
			}
			
			/* 一般说明这是在函数最后一个的jr ra之后的其余代码。
			 * 但也有例外，比如说pc落在一段诸如thread_exit之类优化后，
			 * 没有栈帧，也没有jr ra的函数代码片断中，这里我们的回溯算法
			 * 就要出错了。 */
			
			/* 查找包括jr ra上一条指令在内的指令是否有栈释放的指令 */
			do	{
				code_addr -= 4;
				type = disassemble_a_instruction(code_addr);
				if (type == ADDIU_SP_SP_POSITIVE)
					addiu_positive_mark = TRUE;
				else if (type == SX_RA_SP)
				{
					ra_offset = (*(u32 *)code_addr) & 0x0000ffff;
				}
			} while (type != JR_RA && type != ADDIU_SP_SP_NEGATIVE);
			
			if (type == ADDIU_SP_SP_NEGATIVE)
			{
				/* 说明这是一个只有栈分配没有栈释放和jr ra的函数 */
				frame_size = (*(u32 *)code_addr) & 0x0000ffff;
				break;
			}
			
			u64 temp_code_addr = code_addr - 4;
			type = disassemble_a_instruction(temp_code_addr);
			if (type == ADDIU_SP_SP_POSITIVE)
				addiu_positive_mark = TRUE;
			
			if (addiu_positive_mark == TRUE)
			{
				/* 有栈释放指令，向前查找栈分配指令，确定栈帧大小 */
				do {
					code_addr -= 4;
					type = disassemble_a_instruction(code_addr);
					if (type == SX_RA_SP)
					{
						ra_offset = (*(u32 *)code_addr) & 0x0000ffff;
					}
				} while (type != ADDIU_SP_SP_NEGATIVE);
				frame_size = (*(u32 *)code_addr) & 0x0000ffff;
			}
				/* 没有栈帧释放指令，表明函数是一个叶子函数 */
			break;
		}

		
		if (type == ADDIU_SP_SP_POSITIVE)
			addiu_positive_mark = TRUE;
		
		if (type == JUMP)
			jump_mark = TRUE;
		
		code_addr += 4;
	}
	
	if (ra_offset != INIT_OFFSET)
		mfp->ra = *(u64 *)(mfp->sp + ra_offset);
	
	if (frame_size)
		mfp->sp -= frame_size;
	
	mfp->pc = mfp->ra - 8;
}

static void unwind_a_frame(my_frame *mfp)
{
	u64 code_addr = mfp->pc;
	INST_TYPE type;
	s16 frame_size = 0, ra_offset = INIT_OFFSET;

	while (1)
	{
		do {
			code_addr -= 4;
			type = disassemble_a_instruction(code_addr);
		    if (type == SX_RA_SP)
			{
				ra_offset = (*(u32 *)code_addr) & 0x0000ffff;
			}
		} while (type != ADDIU_SP_SP_NEGATIVE);
		frame_size = (*(u32 *)code_addr) & 0x0000ffff;
		
		/* 当只有栈分配指令没有ra保存指令时，继续查找下一栈分配指令 */
		if(ra_offset == INIT_OFFSET)
			mfp->sp -= frame_size;
		else
			break;
	}
	
//	printk("ra_offset[%d] frame_size[%d]\n", ra_offset, frame_size);
	
	if (ra_offset != INIT_OFFSET)
		mfp->ra = *(u64 *)(mfp->sp + ra_offset);
	
	if (frame_size)
		mfp->sp -= frame_size;
	
	mfp->pc = mfp->ra - 8;
}


/*******************************************************************************
 * 
 * 打印函数名称
 * 
 * 输入：
 * 		函数地址
 * 输出：
 * 		无
 * 返回：
 * 		无
 * 备注：
 * 		2012-01-10，唐立三，将函数名称解析接口替换为钩子函数，该钩子函数在符号表模块
 * 初始化接口中赋值。
 */
static void print_func_name(u64 addr)
{
#define MAX_STRLEN 191
	const char *s;
	size_t offset;
	
	printk("0x%x",  (u64)addr);	

	/* 判断函数解析钩子函数是否设置 */
	if (funcname_resolve_handler)
	{
		s = funcname_resolve_handler((const void *)addr, ((int *)&offset));
		if (s)
		{
			printk("(%s+0x%x)", s, offset);
		}
		else 
		{
			printk("(STATIC FUNC)");
		}
	}
	
	printk("\n\r");
}


void backtrace_stack(u64 pc, u64 sp, u64 ra)
{
	static u32 nest_level = 0;
	u32 count = 0;
	my_frame mf;
	
	if (nest_level++)
	{
		printk("backtrace_stack() nested\n");//backtrace_stack()嵌套了
		while(1);
	}
	
	mf.pc = pc;
	mf.sp = sp;
	mf.ra = ra;
	
extern void reworks_thread();
extern void reworks_signal();
extern void exception_handler();
extern void context_restore();
	while (1)
	{
		print_func_name(mf.pc);
		
		/* 最后一个条件分支是判断pc是否是在exception_handler之中，选context_restore
		 * 是因为它跟在context_restore后面，如果链接情况不是这样，请修改为相应函数。 */
		if (mf.pc == (u64)reworks_thread
		|| mf.pc == (u64)reworks_signal
		|| (mf.pc > (u64)exception_handler && mf.pc < (u64)context_restore))
			break;
		
		if (count == 0)
			unwind_first_frame(&mf);
		else
			unwind_a_frame(&mf);
		
		count++;
	}
	
	nest_level--;
}

/* 从主调函数那一层开始回溯 */
void backtrace_self()
{
	u64 code_addr;
	u64 ra, sp;
	s16 frame_size;
	
	asm volatile (
		"move %0, $ra\n"
		"move %1, $sp\n"
		: "=r"(ra), "=r"(sp)
	);
	
	code_addr = (u64)backtrace_self;
	
	while (disassemble_a_instruction(code_addr) != ADDIU_SP_SP_NEGATIVE)
	{
		code_addr += 4;
	}

	frame_size = (*(u32 *)code_addr) & 0x0000ffff;
	sp -= frame_size;
	
	backtrace_stack(ra-8, sp, ra);
}

/*******************************************************************************
 * 
 * 获取当前线程的调用堆栈
 * 
 * 输入：
 * 		pc sp ra	函数地址
 * 		buffer:		是一个指针列表，获取信息将存放在buffer中
 *		size:		指定buffer中可保存多少个void*元素
 * 输出：
 * 		[0,size]	实际获取的指针数（≤size）;
			-1 		失败

 * 返回：
 * 		无
 * 备注：
 * 		2014-12-29，张燕
 * 		由backtrace_csp()调用
 */
int backtrace_stack_csp(u64 pc, u64 sp, u64 ra,void **buffer,int size)
{
	if(size <= 0)
		return -1;
	
	if(!buffer)
		return -1;
	
	
	static u32 nest_level = 0;
	u32 count = 0;
	my_frame mf;
	
	if (nest_level++)
	{
		printk("backtrace_stack() nested\n");//backtrace_stack()嵌套了
		while(1);
	}
	
	mf.pc = pc;
	mf.sp = sp;
	mf.ra = ra;
	
extern void reworks_thread();
extern void reworks_signal();
extern void exception_handler();
extern void context_restore();


	int backtrace_size = 0;
	
	while (1)
	{
		if(backtrace_size >= size)
			break;

		buffer[backtrace_size] = (void *)mf.pc;

		backtrace_size++;

		
		/* 最后一个条件分支是判断pc是否是在exception_handler之中，选context_restore
		 * 是因为它跟在context_restore后面，如果链接情况不是这样，请修改为相应函数。 */
		if (mf.pc == (u64)reworks_thread
		|| mf.pc == (u64)reworks_signal
		|| (mf.pc > (u64)exception_handler && mf.pc < (u64)context_restore))
			break;
		
		if (count == 0)
			unwind_first_frame(&mf);
		else
			unwind_a_frame(&mf);
		
		count++;	
	}
	
	nest_level--;

	return backtrace_size;
}

/*******************************************************************************
 * 
 * 获取当前线程的调用堆栈
 * 
 * 输入：
 * 
 * 		buffer:		是一个指针列表，获取信息将存放在buffer中
 *		size:		指定buffer中可保存多少个void*元素
 * 输出：
 * 
 * 返回：
 *		[0,size]	实际获取的指针数（≤size）;
 *		-1 			失败
 * 备注：
 * 		2014-12-29，张燕
 * 	
 */
int backtrace(void **buffer, int size)
{
	u64 code_addr;
	u64 ra, sp;
	s16 frame_size;
	
	asm volatile (
		"move %0, $ra\n"
		"move %1, $sp\n"
		: "=r"(ra), "=r"(sp)
	);
	
	code_addr = (u64)backtrace;
	
	while (disassemble_a_instruction(code_addr) != ADDIU_SP_SP_NEGATIVE)
	{
		code_addr += 4;
	}

	frame_size = (*(u32 *)code_addr) & 0x0000ffff;
	sp -= frame_size;
	
	int ret = backtrace_stack_csp(ra-8, sp, ra, buffer, size);
//	printk("[%s] Return %d\n",__FUNCTION__,ret);
	return ret;
}
/*******************************************************************************
 * 
 * 将当前线程的调用堆栈信息转化为一个字符串数组
 * 
 * 输入：
 * 		buffer:		从backtrace()获取的指针数据
 *		size：		backtrace()的返回值

 * 输出：
 * 		
 * 返回：
 * 		char **		一个指向字符串数组的指针；
 * 					(每个字符串包含了一个相对于buffer中对应元素的可打印信息，包括当前函数指针、函数名和函数偏移地址)
 *		NULL 		失败
 * 备注：
 * 		2014-12-29，张燕
 * 		
 */
char **backtrace_symbols(void **buffer, int size)
{
	//判断参数是否符合要求
	if(size <= 0){
		printk("Size = %d Error!\n",size);
		return NULL;
	}
	
	int i;
	for(i = 0; i < size; i++){
		if(buffer[i] == NULL){
			printk("buffer[%d] = NULL!\n",i);
			return NULL;
		}
	}
	
	//创建返回值数组
	char **string = NULL;
	string = (char **)malloc(size*sizeof(char *));
	if(!string){
		printk("Malloc %d Bytes Error!\n",size*sizeof(char *));
		return NULL;
	}
	memset(string,0,size*sizeof(char *));
	
	//创建临时变量字符串
	char *symbol_t = (char *)malloc(sizeof(char) * 100);
	if(!symbol_t){
		printk("Malloc 100 Bytes Error!\n");
		return NULL;
	}
	
//	printk("Ready go!!![%s]\n",__FUNCTION__);
	char *s = NULL;
	int offset = 0;
	//解析函数名、偏移量
	for(i = 0; i < size; i++){
		memset(symbol_t,0,100);
		sprintf(symbol_t,"[0x%x]",(u64)buffer[i]);
//		printk("====1debug%d:%s====\n",i,symbol_t);
		int symbol_t_len = strlen(symbol_t);
		
		/* 判断函数解析钩子函数是否设置 */
		extern FUNCNAME_RESOLVE_HANDLER funcname_resolve_handler;
		if (funcname_resolve_handler)
		{
			s = funcname_resolve_handler(buffer[i], ((int *)&offset));
			if(s)
			{
				sprintf(&symbol_t[symbol_t_len],"(%s+0x%x)",s,offset);
//				printk("====2debug%d:%s====\n",i,symbol_t);
				s = NULL;
			}
			else 
			{
				sprintf(&symbol_t[symbol_t_len],"%s","(STATIC FUNC)");
//				printk("====3debug%d:%s====\n",i,symbol_t);
			}
			
		}
		
		//拷贝到string[i]
		symbol_t_len = strlen(symbol_t);
		string[i] = (char *)malloc(sizeof(char) * (symbol_t_len + 1));
		if(!string[i]){
			printk("Malloc %d Bytes Error!\n",(symbol_t_len + 1));
			for(i = i - 1; i >= 0; i--){
				free(string[i]);
			}
			free(string);
			return NULL;
		}
		memset(string[i],0,symbol_t_len + 1);
		memcpy(string[i],symbol_t,symbol_t_len);
//		printk("====4debug%d:%s====\n",i,string[i]);
	}
	
	if(symbol_t)
		free(symbol_t);
	
	return string;	
}
/*******************************************************************************
 * 
 * 将当前线程的调用堆栈信息转化成的字符串数组存入文件句柄为fd的文件中
 * 
 * 输入：
 * 		buffer:		从backtrace()获取的指针数据
 *		size：		backtrace()的返回值
 *		fd:			写入的文件句柄

 * 输出：
 * 		无
 * 返回：
 * 		无
 * 备注：
 * 		2014-12-29，张燕
 * 		
 */
void backtrace_symbols_fd(void **buffer, int size,int fd)
{
	char **string = backtrace_symbols(buffer,size);
	if(!string){
		return;
	}
	int i;
	int write_size = 0;
	int string_size = 0;
	char *write_buffer_t = (char *)malloc(sizeof(char) * 100);
	if(!write_buffer_t){
		printk("Malloc 100 Bytes Error!\n");
		for(i = 0; i < size; i++){
			free(string[i]);
		}
		free(string);
		return;
	}
	for(i = 0; i < size; i++){
		string_size = strlen(string[i]);
		memset(write_buffer_t,0,100);

		memcpy(write_buffer_t,string[i],string_size);
		write_buffer_t[string_size] = '\0';
		write_buffer_t[string_size + 1] = '\n';
		
		write_size = write(fd,write_buffer_t,string_size + 2);
		if(write_size != string_size + 2){
			printk("Write FILE* %d Error!\n",fd);
			for(i = 0; i < size; i++){
				free(string[i]);
			}
			free(string);
			if(write_buffer_t)
				free(write_buffer_t);
			return;
		}
	}
	
	for(i = 0; i < size; i++){
		free(string[i]);
	}
	free(string);
	if(write_buffer_t)
		free(write_buffer_t);
	return;
}

/*******************************************************************************
 * 
 * 打印当前线程的调用堆栈信息接口
 * 
 * 输入：
 * 		print_type:		字符串"printk"或"printf"(指定打印函数)
 *		size：			最多打印的堆栈信息行数（每行打印一条调用堆栈信息）
 *	
 * 输出：
 * 		无
 * 返回：
 * 		true			打印成功
 * 		false			失败
 * 		
 * 备注：
 * 		2014-12-30，张燕
 * 		
 */
boolean backtrace_print(char *print_type,int size)
{
	if(size <= 0){
		printk("size is %d Error!\n",size);
		return false;
	}
	int print_func_flag = 0;
	if(!print_type){
		printk("print_type is NULL Error!\n");
		return false;
	}
	if(strcmp(print_type,"printk") == 0){
		print_func_flag = 1;
	}
	else if(strcmp(print_type,"printf") == 0){
		print_func_flag = 2;
	}
	else{
		printk("print_type is %s Error!\n",print_type);
		return false;
	}
	void **buffer = (void **)malloc(sizeof(void *)*size);
	if(!buffer){
		printk("Malloc %d Bytes Error!\n",sizeof(void *)*size);
		return false;
	}
	memset(buffer,0,sizeof(void *)*size);
	int ret = backtrace(buffer,size);
	if(ret < 0){
		printk("backtrace() Error!\n");
		free(buffer);
		return false;
	}
	int i;
	char **string = backtrace_symbols(buffer,ret);
	if(!string){
		printk("backtrace_symbols() Error!\n");
		free(buffer);
		return false;
	}
	
	if(print_func_flag == 1){//printk打印
		for(i = 0 ; i < ret; i++){
			printk("%s\n",string[i]);
		}
	}
	else{//printf打印
		for(i = 0 ; i < ret; i++){
			printf("%s\n",string[i]);
		}
	}
	
	//释放内存
	for(i = 0; i < ret; i++){
		if(string[i])
			free(string[i]);
	}
	free(buffer);
	free(string);
	return true;
	
}
#if 0

void testcase()
{
	backtrace_self();
extern void backtrace_example_O0_normal();
extern void backtrace_example_O0_prologue();
extern void backtrace_example_O0_epilogue();
extern void backtrace_example_O0_ra();
extern void backtrace_example_O2_style();
extern void backtrace_example_O2_loop();
extern void backtrace_example_aux_leaf();
extern void backtrace_double_addiuspsp();
	sp(backtrace_example_O0_normal);
	sp(backtrace_example_O0_prologue);
	sp(backtrace_example_O0_epilogue);
	sp(backtrace_example_O0_ra);
	sp(backtrace_example_O2_style);
	//	sp(backtrace_example_O2_loop);
	sp(backtrace_example_aux_leaf);
	/* 这个例子目前还没有测试，肯定通过不过，目前两次栈分配不出现顶层是可以正确回溯的。 */
	sp(backtrace_double_addiuspsp);
	
	/* type in ReWorks shell:
	 * backtrace_self()
	 * uthread_stack_show(0x40001)
	 * ...
	 */
}
	
/* backtrace_testcase.s */

#define ASM_LANGUAGE
#include "h/mips_asm.h"

.text
.set noreorder

.ent backtrace_example_O0_normal
.globl backtrace_example_O0_normal
backtrace_example_O0_normal:
	addiu	sp,sp,-8
	sw	s8,0(sp)
	move	s8,sp
	break
	move	v0,zero
	move	sp,s8
	lw	s8,0(sp)
	addiu	sp,sp,8
	jr	ra
	nop
.end backtrace_example_O0_normal

.ent backtrace_example_O0_prologue
.globl backtrace_example_O0_prologue
backtrace_example_O0_prologue:
	break
	addiu	sp,sp,-8
	sw	s8,0(sp)
	move	s8,sp
	move	v0,zero
	move	sp,s8
	lw	s8,0(sp)
	addiu	sp,sp,8
	jr	ra
	nop
.end backtrace_example_O0_prologue

.ent backtrace_example_O0_epilogue
.globl backtrace_example_O0_epilogue
backtrace_example_O0_epilogue:
	addiu	sp,sp,-8
	sw	s8,0(sp)
	move	s8,sp
	move	v0,zero
	move	sp,s8
	lw	s8,0(sp)
	addiu	sp,sp,8
	break
	jr	ra
	nop
.end backtrace_example_O0_epilogue

.ent backtrace_example_O0_ra
.globl backtrace_example_O0_ra
backtrace_example_O0_ra:
	addiu	sp,sp,-32
	sw	ra,20(sp)
	break
	jal backtrace_example_O0_normal
	nop
	lw	ra,20(sp)
	addiu	sp,sp,32
	jr	ra
	nop
.end backtrace_example_O0_ra

.ent backtrace_example_O2_style
.globl backtrace_example_O2_style
backtrace_example_O2_style:
	addiu	sp,sp,-24
	sw	ra,16(sp)
	j 1f
	nop
0:
	lw	ra,16(sp)
	jr	ra
	addiu	sp,sp,24
1:
	break
	j 0b
	nop
.end backtrace_example_O2_style

.ent backtrace_example_O2_loop
.globl backtrace_example_O2_loop
backtrace_example_O2_loop:
	addiu	sp,sp,-24
	sw	ra,16(sp)
0:
	break
	move v1, v0
	j 0b
	nop
.end backtrace_example_O2_loop

.ent backtrace_example_aux_leaf
.globl backtrace_example_aux_leaf
backtrace_example_aux_leaf:
	break;
    jr  ra
    nop
.end backtrace_example_aux_leaf

.set reorder
 
.ent backtrace_double_addiuspsp
.globl backtrace_double_addiuspsp
backtrace_double_addiuspsp:
	addiu	sp,sp,-24
	sw	ra,16(sp)
	addiu	sp,sp,-24
	move v1, v0
	break
	addiu	sp,sp,24
	lw	ra,16(sp)
    jr  ra
    addiu	sp,sp,24
.end backtrace_double_addiuspsp

#endif
