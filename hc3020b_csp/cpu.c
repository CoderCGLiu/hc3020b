#include <stdio.h>
#include <cpu.h>
#include <reworks/types.h>
#include <reworks/printk.h>
#include <reworks/thread.h>
#include <symtbl.h>
#include <errno.h>

extern void backtrace_stack(u64 pc, u64 sp, u64 ra);
/*-------------------------------------------
 * sys_info_get
 * 用于资源浏览器模块。
 * 
	typedef struct
	{
		char osName[16]; 	//操作系统名字
		char osVersion[16];	//操作系统版本号
		char cpuType[16];   //CPU类型
		char cpuInfo[48];
	} SYSTEM_BASIC_INFO;
-------------------------------------------*/
SYSTEM_BASIC_INFO sys_basic_info;
extern char * reworks_os_version;
SYSTEM_BASIC_INFO *sys_info_get()
{
	char *units = NULL;
	char freq[10] = {0};
	char *board_family = 0;
	memset(&sys_basic_info,0,sizeof(SYSTEM_BASIC_INFO));
	u32 timestamp_freq = sys_timestamp_freq();
	
	strcpy(sys_basic_info.osName,"ReWorks");
	strcpy(sys_basic_info.osVersion, reworks_os_version);
#ifdef HUARUI2_SMP_64
	strcpy(sys_basic_info.cpuType,"HUARUI2");
#elif defined(HUARUI3_SMP_64)
	strcpy(sys_basic_info.cpuType,"HUARUI3");
#endif

#ifdef __multi_core__

#ifdef HUARUI2_SMP_64
	board_family = "HUARUI2 @ ";
#elif defined(HUARUI3_SMP_64)
	board_family = "HUARUI3 @ ";
#endif

#else
	board_family = "Loongson2H @ ";
#endif
	strcpy(sys_basic_info.cpuInfo, board_family);
	
#define MHz	(1000 * 1000)
#define GHz	((MHz) * 1000)
	if (timestamp_freq > 1 * GHz)
	{
		units = "GHz";
		timestamp_freq /= GHz;
	}
	else if (timestamp_freq > 1 * MHz)
	{
		units = "MHz";
		timestamp_freq /= MHz;
	}
#undef MHz
#undef GHz
	
	sprintf(freq, "%d%s", timestamp_freq, units);
	strcat(sys_basic_info.cpuInfo, freq);
	
	return (SYSTEM_BASIC_INFO *) &sys_basic_info;
}


#ifdef __mips_abi_n32__ || #ifdef __mips_abi_n64__
#define ALIGNMENT_SHIFT		 5 
#else/*  not define __mips_abi_n32__ */
#define ALIGNMENT_SHIFT      3
#endif/*  __mips_abi_n32__ */

#define STACKFRAME_ALIGNMENT (2 << ALIGNMENT_SHIFT)

REGS_INDEX thread_regs_index[] = 
{
//	{"type", REG_OFFSET(0)},
	{"c0_sr", REG_OFFSET(1)},
	{"pc", REG64_OFFSET(1)},
	{"s0", REG64_OFFSET(2)},
	{"s1", REG64_OFFSET(3)},
	{"s2", REG64_OFFSET(4)},
	{"s3", REG64_OFFSET(5)},
	{"s4", REG64_OFFSET(6)},
	{"s5", REG64_OFFSET(7)},
	{"s6", REG64_OFFSET(8)},
	{"s7", REG64_OFFSET(9)},
	{"gp", REG64_OFFSET(10)},
	{"sp", REG64_OFFSET(11)},
	{"fp", REG64_OFFSET(12)},
	{"ra", REG64_OFFSET(13)},
	{"AT", REG64_OFFSET(14)},
	{"v0", REG64_OFFSET(15)},
	{"v1", REG64_OFFSET(16)},
	{"a0", REG64_OFFSET(17)},
	{"a1", REG64_OFFSET(18)},
	{"a2", REG64_OFFSET(19)},
	{"a3", REG64_OFFSET(20)}, 
	{"t0", REG64_OFFSET(21)},
	{"t1", REG64_OFFSET(22)},
	{"t2", REG64_OFFSET(23)},
	{"t3", REG64_OFFSET(24)},
	{"t4", REG64_OFFSET(25)},
	{"t5", REG64_OFFSET(26)},
	{"t6", REG64_OFFSET(27)},
	{"t7", REG64_OFFSET(28)},
	{"t8", REG64_OFFSET(29)},
	{"t9", REG64_OFFSET(30)},
	{"k1", REG64_OFFSET(31)},
	{"mulhi", REG64_OFFSET(32)},
	{"mullo", REG64_OFFSET(33)},
	{NULL, 0},
};


/*******************************************************************************
 * 
 * 上下文初始化接口
 * 
 * 		本接口在任务创建和复位接口中调用：thread_ready和reset_thread_context。
 * 
 * 输入：
 * 		the_context：待初始化的上下文
 * 		isr：0
 * 		entry_point：任务入口或调用任务入口的函数
 * 		pre_entry：reworks_signal（nop）
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void context_init(Context_Ctrl *the_context, u32 isr,
		void *entry_point, void *prev_entry)
{
	/* huangyuan20090325:
	 * 假定the_context所指向的上下文区域已经被清零 */
extern char _gp;

	/* huangyuan20120718: 这个函数中编译产生的警告请不要使用强制
	 * 类型转换来消警告（如(u64)((u32)entry_point)）。因为在这里
	 * 要利用MIPS64体系架构的符号扩展特性，即将32位整数0x80000000
	 * 赋给64位整数，希望得到的是0xffffffff80000000，而不是
	 * 0x0000000080000000 */
	u64 stack_tmp = (u64)the_context + sizeof(Context_Ctrl);
	/* The Stack Frame, 'SYSTEM V APPLICATION BINARY INTERFACE - MIPS RISC Processor Supplement' 3rd Edition,
	 * p. 3-15, 3-16 */
	/* alignment. Although the architecture requires only word alignment,
	 * software convention and the operating system require every stack
	 * frame to be doubleword(8 byte) aligned. */
	if (stack_tmp % STACKFRAME_ALIGNMENT)
		stack_tmp = (stack_tmp >> ALIGNMENT_SHIFT) << ALIGNMENT_SHIFT;

	the_context->type	= INTERRUPT_TYPE;
	the_context->c0_sr	= 0x7400fca3; /* LS2H为0x3400ff83, 代表协处理单元的可用性 */
//	the_context->c0_sr	= 0x748800E0;//test


	the_context->pc		= (u64)entry_point;
	the_context->gp		= &_gp;
	the_context->sp		= stack_tmp;
	the_context->fp		= stack_tmp;
	/* 模拟从reworks_main处跳转到thread_handler */
	the_context->ra		= (u64)prev_entry + 8;
}


/*******************************************************************************
 * 
 * 打印全部寄存器内容
 * 
 * 		本接口原名为context_dump，现根据接口含义，将名称修改
 * 
 * 输入：
 * 		context：任务上下文
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void context_dump(Context_Ctrl *context)
{	
	printk("context type[%s]\n",
			(INTERRUPT_TYPE == context->type) ? ("INTERRUPT_TYPE") : ("SWITCH_TYPE"));
	printk("      zero              at               v0               v1\n");
	printk("0000000000000000 %016x %016x %016x\n", context->AT, context->v0, context->v1);
	printk("       a0               a1               a2               a3\n");
	printk("%016x %016x %016x %016x\n", context->a0, context->a1, context->a2, context->a3);
	printk("       t0               t1               t2               t3\n");
	printk("%016x %016x %016x %016x\n", context->t0, context->t1, context->t2, context->t3);
	printk("       t4               t5               t6               t7\n");
	printk("%016x %016x %016x %016x\n", context->t4, context->t5, context->t6, context->t7);
	printk("       s0               s1               s2               s3\n");
	printk("%016x %016x %016x %016x\n", context->s0, context->s1, context->s2, context->s3);
	printk("       s4               s5               s6               s7\n");
	printk("%016x %016x %016x %016x\n", context->s4, context->s5, context->s6, context->s7);
	printk("       t8               t9               k0               k1\n");
	printk("%016x %016x -------NA------- %016x\n", context->t8, context->t9, context->k1);
	printk("       gp               sp               s8               ra\n");
	printk("%016x %016x %016x %016x\n", context->gp, context->sp, context->fp, context->ra);
	printk("r(MULHI) =[%016x]\n", context->mulhi);
	printk("r(MULLO) =[%016x]\n", context->mullo);
	printk("r(STATUS)=[%08x]\n", context->c0_sr);
	printk("r(EPC)   =[%016x]\n", context->pc);
	if (context->pc >= 0xffffffff80000000LL && context->pc < 0xffffffff90000000LL && !(context->pc % 4))
		printk("the code at EPC: %08x\n", *((u32 *)((u64)context->pc)));
}

void arch_thread_stackframe_show(Context_Ctrl* context)
{
    backtrace_stack(context->pc, context->sp, context->ra);
}

int cpu_probe()
{
	return 0;
}

OS_STATUS context_ctrl_to_reg_set(REG_SET *pRegs, Context_Ctrl *sp)
{
	if(pRegs == NULL || sp == NULL)
	{
		return OS_ERROR;
	}

	memcpy(pRegs, sp, sizeof(Context_Ctrl));
	pRegs->sp =  (u64)sp;
	return OS_OK;
}

