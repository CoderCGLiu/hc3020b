/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了浮点任务支持功能，即对设置了浮点支持选项的任务提供浮点上下文保存
 * 		 和恢复功能。
 * 修改：
 * 		 2013-01-07，唐立三，规范
 * 		 ......
 */
#include <stdio.h>
#include <pthread_hook.h>
#include <fpu.h>
#include <exceptionp.h>
#include <thread.h>

extern void *memset(void *s, int c, size_t n);

/**
 * FCSR寄存器定义
 */
/* 异常使能 */
#define FCSR_ENABLES_V		0x00000800
#define FCSR_ENABLES_Z		0x00000400
#define FCSR_ENABLES_O		0x00000200
#define FCSR_ENABLES_U		0x00000100
#define FCSR_ENABLES_I		0x00000080
#define FCSR_ENABLES_ALL	\
	(FCSR_ENABLES_V |	 	\
	 FCSR_ENABLES_Z | 		\
	 FCSR_ENABLES_O | 		\
	 FCSR_ENABLES_U | 		\
	 FCSR_ENABLES_I)

/* 冲刷到0  */

#define FCSR_FS				0x0

/**
 * 浮点上下文保存和恢复句柄
 */
void (*fpu_save_ctx_hdl)(Fp_Context *) = 0;
void (*fpu_restore_ctx_hdl)(Fp_Context *) = 0;

/**
 * 浮点模块初始化标识
 */
volatile int fpu_module_init_flag = 0;

/**
 * 浮点上下文在TCB中的偏移
 */
size_t fpu_context_ptr_offset;

/**
 * 获取注册的浮点上下文
 */
#define reference_thread_fpu_context_ptr(thread_ptr) \
	((Fp_Context **)((size_t)thread_ptr + fpu_context_ptr_offset))


/*******************************************************************************
 * 
 * 浮点协处理器初始化接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void fpu_init(void)
{
	static int fpu_init_flag = 0;
	
	if (fpu_init_flag)
	{
		return;
	}
	
	/* 初始化浮点控制寄存器 */
	/* 0x01000000 -> fcr31，设置FS位，解决浮点转换指令处理denormalized数或下溢发生异常问题 */
	/* 0xf80 -> enables使能所有浮点中断：E、V、Z、O、U、I */

	asm(
		"li $2,0x0;\n" \
		"ctc1 $2,$31;\n" \
		"sync; \n"
		:
		:
		:"$2"
	);

	/* 设置保存和恢复钩子，接口在fpu_asm.s中实现 */
    fpu_save_ctx_hdl 	= mips_fpu_context_save;
    fpu_restore_ctx_hdl = mips_fpu_context_restore;
    
    fpu_init_flag = 1;
    
    return;
}


/*******************************************************************************
 * 
 * 浮点上下文初始化接口
 * 
 * 		本接口在浮点任务创建钩子函数中调用。为设置了浮点支持选项的任务分配用于保存
 * 浮点上下文的内存空间，并重新设置浮点协处理器控制器。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
static OS_STATUS  fpu_context_init(thread_t thread_ptr)
{
	Fp_Context **fpu_context_pptr = reference_thread_fpu_context_ptr(thread_ptr);

	/* 检查浮点上下文是否已创建 */
	if (!*fpu_context_pptr) 
	{
		/* CACHE_ALIGN_SIZE);  CACHE_AlIGN_SIZE need to fixed, vxworks */
		*fpu_context_pptr = (Fp_Context *)kmalloc_align(sizeof(Fp_Context),32); 
		if (*fpu_context_pptr == NULL) 
		{
			return -1; 
		}
		memset((char *)(*fpu_context_pptr), 0, sizeof(FP_CONTEXT));
		
		/* 修改浮点上下文的FCSR寄存器，使能浮点中断；原代码中该数值为0x01000000 */
		(*fpu_context_pptr)->fpcs = FCSR_FS;
	}

	return 0;
}


/*******************************************************************************
 * 
 * 浮点上下文创建钩子
 * 	
 * 		本接口在任务创建时调用。用于完成浮点上下文的创建和设置。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static int fpu_create_hook(thread_t thread_ptr)
{
	if (fpu_module_init_flag != TRUE)
	{
		return -1;
	}

	if (thread_is_fp(thread_ptr) == TRUE)	
	{
		return fpu_context_init(thread_ptr);
	}

    return 0;
}


/*******************************************************************************
 * 
 * 浮点上下文切换钩子
 * 	
 * 		本接口主要完成两部分工作：将正在执行的线程的浮点上下文保存在任务TCB中，将将要
 * 执行的任务上下文从任务TCB中恢复到浮点寄存器中。这两项操作均通过fpu_asm.s中提供的
 * 钩子函数完成。
 * 
 * 输入：
 * 		executing：正在执行的任务
 * 		heir：准备执行的任务
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static void fpu_switch_hook(thread_t executing, thread_t heir)
{
	if (fpu_module_init_flag != TRUE)
	{
		return;
	}
		
	Fp_Context * fpu_context = *reference_thread_fpu_context_ptr(executing);
	if (fpu_context) 
	{
		fpu_save_ctx_hdl(fpu_context);
	}

	fpu_context = *reference_thread_fpu_context_ptr(heir);
	if (fpu_context) 
	{
		fpu_restore_ctx_hdl(fpu_context);
	}
	
	return;
}


/*******************************************************************************
 * 
 * 浮点上下文删除钩子
 * 	
 * 		本接口将任务销毁之前调用，用于释放任务创建时申请的浮点上下文空间。
 * 
 * 输入：
 * 		thread_ptr：任务ID
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static void fpu_delete_hook(thread_t thread_ptr)
{
	if (fpu_module_init_flag != TRUE)
	{
		return;
	}
	
	if (thread_is_fp(thread_ptr) == TRUE) 
	{
		Fp_Context *fpu_context = *reference_thread_fpu_context_ptr(thread_ptr);
		if (fpu_context)
		{
			*reference_thread_fpu_context_ptr(thread_ptr) = 0;
			kfree(fpu_context);
		}
	}

	return;
}


/*******************************************************************************
 * 
 * 打印浮点寄存器组
 * 
 * 输入：
 * 		context：浮点上下文
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void fpu_context_dump(Fp_Context *context)
{
	printk("      fp0              fp1              fp2              fp3       \n");
	printk("%016x %016x %016x %016x\n", context->fp0, context->fp1, context->fp2, context->fp3);
	printk("      fp4              fp5              fp6              fp7       \n");
	printk("%016x %016x %016x %016x\n", context->fp4, context->fp5, context->fp6, context->fp7);
	printk("      fp8              fp9              fp10             fp11       \n");
	printk("%016x %016x %016x %016x\n", context->fp8, context->fp9, context->fp10, context->fp11);
	printk("      fp12             fp13             fp14             fp15       \n");
	printk("%016x %016x %016x %016x\n", context->fp12, context->fp13, context->fp14, context->fp15);
	printk("      fp16             fp17             fp18             fp19       \n");
	printk("%016x %016x %016x %016x\n", context->fp16, context->fp17, context->fp18, context->fp19);
	printk("      fp20             fp21             fp22             fp23       \n");
	printk("%016x %016x %016x %016x\n", context->fp20, context->fp21, context->fp22, context->fp23);
	printk("      fp24             fp25             fp26             fp27       \n");
	printk("%016x %016x %016x %016x\n", context->fp24, context->fp25, context->fp26, context->fp27);
	printk("      fp28             fp29             fp30             fp31       \n");
	printk("%016x %016x %016x %016x\n", context->fp28, context->fp29, context->fp30, context->fp31);
	printk("r(FCSR) =[%08x]\n", context->fpcs);
	
	return;
}

void fpu_context_save() 
{
	thread_t thread_ptr = THREAD_CURRENT_GET;
	Fp_Context **fpu_context_pptr = reference_thread_fpu_context_ptr(thread_ptr);

	if (fpu_module_init_flag && thread_is_fp(thread_ptr))
	{
		if (*fpu_context_pptr)
		{
			fpu_save_ctx_hdl(*fpu_context_pptr);
		}
		
	}
}

void fpu_context_restore() 
{
	thread_t thread_ptr = THREAD_CURRENT_GET;
	Fp_Context **fpu_context_pptr = reference_thread_fpu_context_ptr(thread_ptr);
		
	if (fpu_module_init_flag && thread_is_fp(thread_ptr)) 
	{
		if (*fpu_context_pptr) 
		{
			fpu_restore_ctx_hdl(*fpu_context_pptr);
		}

	}
}

/*******************************************************************************
 * 
 * 初始化浮点模块
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序尚未实现注册TCB浮点上下文字段和钩子函数失败的处理
 */
int fpu_module_init()
{
	/* 初始化标识检查 */
	if (fpu_module_init_flag)
	{
		return 0;
	}

	/* 初始化浮点协处理器 */
	fpu_init();

	/* 向TCB注册浮点上下文字段 */
	if (add_registered_field("fpu_context_ptr", NULL, &fpu_context_ptr_offset) != 0)
	{
		return -1;	
	}

	/* 注册钩子：创建、切换、删除 */
	if (pthread_create_hook_add(fpu_create_hook) != 0)
	{
		return -1;
	}
    if (pthread_switch_hook_add(fpu_switch_hook) != 0)
    {
    	return -1;
    }
    if (pthread_close_hook_add(fpu_delete_hook) != 0)
    {
    	return -1;
    }
	
    /* 设置初始化标识 */
    fpu_module_init_flag = 1;
    
    return 0;
}
