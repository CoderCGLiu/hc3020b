/*
 * fpu_asm.s
 */
#define ASM_LANGUAGE
#define _ASMLANGUAGE

#include <fpu.h>
#include <mips_asm.h>
#include <gs464_cpu.h>
#include <asm_linkage.h>

/* 32 bit register operations */
#define NOP	nop
#define ADD	add
#define STREG	sw
#define LDREG	lw
#define MFCO	mfc0
#define MTCO	mtc0
#define ADDU	add
#define ADDIU	addi
#define R_SZ	8
#define F_SZ	8
#define SZ_INT	4
#define SZ_INT_POW2 2


#ifdef __GNUC__
#define ASM_EXTERN(x,size) .extern x,size
#else
#define ASM_EXTERN(x,size)
#endif

#define FRAME(name,frm_reg,offset,ret_reg)	\
        .globl  name;						\
        .ent    name;						\
name:;										\
        .frame  frm_reg,offset,ret_reg

#define ENDFRAME(name)						\
        .end name


/*
 *  mips_fpu_context_save
 *
 *  This routine is responsible for saving the FP context
 *  at *fp_context_ptr.  If the point to load the FP context
 *  from is changed then the pointer is modified by this routine.
 *
 *  Sometimes a macro implementation of this is in cpu.h which dereferences
 *  the ** and a similarly named routine in this file is passed something
 *  like a (Context_Control_fp *).  The general rule on making this decision
 *  is to avoid writing assembly language.
 */

/* void mips_fpu_context_save(
 *   Fp_Context *fp_context_ptr
 * );
 */
FRAME(mips_fpu_context_save,sp,0,ra)
        .set noreorder
        .set noat

        sdc1 $f0,FP0_OFFSET*F_SZ(a0)
        sdc1 $f1,FP1_OFFSET*F_SZ(a0)
        sdc1 $f2,FP2_OFFSET*F_SZ(a0)
        sdc1 $f3,FP3_OFFSET*F_SZ(a0)
        sdc1 $f4,FP4_OFFSET*F_SZ(a0)
        sdc1 $f5,FP5_OFFSET*F_SZ(a0)
        sdc1 $f6,FP6_OFFSET*F_SZ(a0)
        sdc1 $f7,FP7_OFFSET*F_SZ(a0)
        sdc1 $f8,FP8_OFFSET*F_SZ(a0)
        sdc1 $f9,FP9_OFFSET*F_SZ(a0)
        sdc1 $f10,FP10_OFFSET*F_SZ(a0)
        sdc1 $f11,FP11_OFFSET*F_SZ(a0)
        sdc1 $f12,FP12_OFFSET*F_SZ(a0)
        sdc1 $f13,FP13_OFFSET*F_SZ(a0)
        sdc1 $f14,FP14_OFFSET*F_SZ(a0)
        sdc1 $f15,FP15_OFFSET*F_SZ(a0)
        sdc1 $f16,FP16_OFFSET*F_SZ(a0)
        sdc1 $f17,FP17_OFFSET*F_SZ(a0)
        sdc1 $f18,FP18_OFFSET*F_SZ(a0)
        sdc1 $f19,FP19_OFFSET*F_SZ(a0)
        sdc1 $f20,FP20_OFFSET*F_SZ(a0)
        sdc1 $f21,FP21_OFFSET*F_SZ(a0)
        sdc1 $f22,FP22_OFFSET*F_SZ(a0)
        sdc1 $f23,FP23_OFFSET*F_SZ(a0)
        sdc1 $f24,FP24_OFFSET*F_SZ(a0)
        sdc1 $f25,FP25_OFFSET*F_SZ(a0)
        sdc1 $f26,FP26_OFFSET*F_SZ(a0)
        sdc1 $f27,FP27_OFFSET*F_SZ(a0)
        sdc1 $f28,FP28_OFFSET*F_SZ(a0)
        sdc1 $f29,FP29_OFFSET*F_SZ(a0)
        sdc1 $f30,FP30_OFFSET*F_SZ(a0)
        sdc1 $f31,FP31_OFFSET*F_SZ(a0)
        cfc1 a1,$31                    /* Read FP status/conrol reg */
        cfc1 a1,$31                    /* Two reads clear pipeline */
        NOP
        NOP
        sd a1, FPCS_OFFSET*F_SZ(a0)    /* Store value to FPCS location */
        j ra
        NOP
        .set reorder
        .set at
ENDPROC(mips_fpu_context_save)


/*
 *  mips_fpu_context_restore
 *
 *  This routine is responsible for restoring the FP context
 *  at *fp_context_ptr.  If the point to load the FP context
 *  from is changed then the pointer is modified by this routine.
 *
 *  Sometimes a macro implementation of this is in cpu.h which dereferences
 *  the ** and a similarly named routine in this file is passed something
 *  like a (Context_Control_fp *).  The general rule on making this decision
 *  is to avoid writing assembly language.
 */

/* void mips_fpu_context_restore(
 *   Fp_Context *fp_context_ptr
 * )
 */
FRAME(mips_fpu_context_restore,sp,0,ra)
        .set noat
        .set noreorder

        ldc1 $f0,FP0_OFFSET*F_SZ(a0)
        ldc1 $f1,FP1_OFFSET*F_SZ(a0)
        ldc1 $f2,FP2_OFFSET*F_SZ(a0)
        ldc1 $f3,FP3_OFFSET*F_SZ(a0)
        ldc1 $f4,FP4_OFFSET*F_SZ(a0)
        ldc1 $f5,FP5_OFFSET*F_SZ(a0)
        ldc1 $f6,FP6_OFFSET*F_SZ(a0)
        ldc1 $f7,FP7_OFFSET*F_SZ(a0)
        ldc1 $f8,FP8_OFFSET*F_SZ(a0)
        ldc1 $f9,FP9_OFFSET*F_SZ(a0)
        ldc1 $f10,FP10_OFFSET*F_SZ(a0)
        ldc1 $f11,FP11_OFFSET*F_SZ(a0)
        ldc1 $f12,FP12_OFFSET*F_SZ(a0)
        ldc1 $f13,FP13_OFFSET*F_SZ(a0)
        ldc1 $f14,FP14_OFFSET*F_SZ(a0)
        ldc1 $f15,FP15_OFFSET*F_SZ(a0)
        ldc1 $f16,FP16_OFFSET*F_SZ(a0)
        ldc1 $f17,FP17_OFFSET*F_SZ(a0)
        ldc1 $f18,FP18_OFFSET*F_SZ(a0)
        ldc1 $f19,FP19_OFFSET*F_SZ(a0)
        ldc1 $f20,FP20_OFFSET*F_SZ(a0)
        ldc1 $f21,FP21_OFFSET*F_SZ(a0)
        ldc1 $f22,FP22_OFFSET*F_SZ(a0)
        ldc1 $f23,FP23_OFFSET*F_SZ(a0)
        ldc1 $f24,FP24_OFFSET*F_SZ(a0)
        ldc1 $f25,FP25_OFFSET*F_SZ(a0)
        ldc1 $f26,FP26_OFFSET*F_SZ(a0)
        ldc1 $f27,FP27_OFFSET*F_SZ(a0)
        ldc1 $f28,FP28_OFFSET*F_SZ(a0)
        ldc1 $f29,FP29_OFFSET*F_SZ(a0)
        ldc1 $f30,FP30_OFFSET*F_SZ(a0)
        ldc1 $f31,FP31_OFFSET*F_SZ(a0)
        cfc1 a1,$31                  /* Read from FP status/control reg */
        cfc1 a1,$31                  /* Two reads clear pipeline */
        NOP                          /* NOPs ensure execution */
        NOP
        ld a1,FPCS_OFFSET*F_SZ(a0)      /* Load saved FPCS value */
        ctc1 a1,$31                  /* Restore FPCS register */
        sync
        j ra
        NOP
        .set reorder
        .set at
ENDPROC(mips_fpu_context_restore)
