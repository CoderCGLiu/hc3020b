/* fp_exc.s - 浮点非对齐异常处理 */

#define _ASMLANGUAGE
#define ASM_LANGUAGE
#define _WRS_MIPS_N32_ABI



#include <asm_linkage.h>
#include <mips_asm.h>
#include <gs464_cpu.h>
#include <bd_opt.h>
#include <arch_mips.h>
/* defines */


#undef DEBUG

	.text
	.set	reorder

/******************************************************************************
* fp_exc_handler - Interrupt handler for MIPS floating point unit
*
* On entry:
*  a0:	vecNum
*  a1:	ESFMIPS *
*  a2:	REG_SET *
*/

	.globl	fp_exc_handler
	.ent	fp_exc_handler
fp_exc_handler:
	SETFRAME(fp_exc_handler,4,2)
	.frame	sp, FRAMESZ(fp_exc_handler), ra
	subu	sp, FRAMESZ(fp_exc_handler)

	SW	ra, FRAMERA(fp_exc_handler)(sp)
	sw	a0, FRAMEA0(fp_exc_handler)(sp)	/* save vector */ /* 保存是否浮点任务 */
	sw	a1, FRAMEA1(fp_exc_handler)(sp)	/* save pEsf */
	sw	a2, FRAMEA2(fp_exc_handler)(sp) /*  */
	sw	a3, FRAMEA3(fp_exc_handler)(sp) /* 保存浮点上下文 */
	
#ifdef DEBUG
	/* Count number of exceptions taken */
	.globl	_fp_intr
	.comm	_fp_intr, 4
	lw	t0, _fp_intr
	addu	t0, 1
	sw	t0, _fp_intr
#endif	

/*	_MIPS_PER_CPU_VALUE_GET(t0,taskIdCurrent) */
#if 0
	lw		t0, thread_current	/* 稍后获取任务属性--不再使用，改为参数传递 */
#endif
	move	t1, a0			/* save vector */
	sw		t0, FRAMETID(fp_exc_handler)(sp) /* save taskIdCurrent */
	move	a0, a1			/* a0 becomes pEsf */

#if 0
	subu	t1, t1, EXC_CODE_FPU		/* get exception type */
	sw		t1, FRAMEA2(fp_exc_handler)(sp)		/* save exception type */
	beq		t1, zero, noFpu
#endif


#if 0
	lw		t0, M_E_STK_FPCSR(a0)             /* load FPCSR from stack */
#else
	lw		t0, F_E_STK_FPCSR(a3)             /* 此时a3还没有使用过，直接用a3 */
#endif

#ifdef DEBUG
	/* At this point, t0 holds the unmodified fpscr reg value. */
	.comm	_fp_fpscr, 4
	sw	t0, _fp_fpscr
#endif	
	
#if defined (_WRS_PAL_COPROC_LIB)
	/* 10apr03,pes. Add code to clear masked exceptions in C0_CAUSE. */
	/* ignore masked exceptions */
	and	t1, t0, FP_ENABLE_MASK		/* get a copy of enables */
	sll	t1,(FP_EXC_SHIFT - FP_ENABLE_SHIFT)
	or	t1,FP_EXC_E
	and	t0, t1
#endif

	/*
	 * The following doesn't seem to handle well the case on the R4000
	 * where the SR_FR bit in the task's status register was clear
	 * and the task referenced an odd floating point register.
	 */

	/* check for unimplemented operation */
	
	/* 上面的宏生效了 */
	li	t1, FP_EXC_E
	and	t1, t0, t1
	bnez	t1, noFpu			/* E错误，跳转到noFpu */
	nop

	/*
	 * If the cause of the FP exception was something other than a
	 * unimplemented operation exception, divert the exception to the
	 * proper exception handler.
	 */
	
	/* check for inexact operation */
	li	a0, EXC_CODE_FPE_INEXACT
	li	t1, FP_EXC_I
	and	t1, t0, t1
	bnez	t1, fpuForwardExc
	/* check for underflow */
	li	a0, EXC_CODE_FPE_UNDERFLOW
	li	t1, FP_EXC_U
	and	t1, t0, t1
	bnez	t1, fpuForwardExc
	/* check for overflow */
	li	a0, EXC_CODE_FPE_OVERFLOW
	li	t1, FP_EXC_O
	and	t1, t0, t1
	bnez	t1, fpuForwardExc
	/* check for divide by zero */
	li	a0, EXC_CODE_FPE_DIV0
	li	t1, FP_EXC_Z
	and	t1, t0, t1
	bnez	t1, fpuForwardExc
	/* check for invalid operation */
	li	a0, EXC_CODE_FPE_INVALID
	li	t1, FP_EXC_V
	and	t1, t0, t1
	bnez	t1, fpuForwardExc
	
	/* shouldn't get here */
	li	a0, EXC_CODE_RI
	b	fpuForwardExc
	
noFpu:
	/*
	 * Change: Ability to FP emulate is determined solely by
	 * the task options, not by the SR:CU1 bit
	 */
#if 0
	lw	a3, FRAMETID(fp_exc_handler)(sp)/* get current excecuting task */	/* 栈上保存 */
	lw	a2, WIND_TCB_OPTIONS(a3)/* read options field */			/* 改为参数传入，使用第三个参数 （a2）*/
#endif

/* 判定是否是浮点任务 */
#if 0
	move	a3, a0
	move	a4, v0
	

	lw		a0, FRAMEA1(fp_exc_handler)(sp)
	jal		thread_is_fp
	nop
	move	a2, v0
	move	v0, a4
	move	a0, a3
#endif
	
	/* 浮点任务标志存在A0中 */
	lw		a2, FRAMEA0(fp_exc_handler)(sp)

	
#if 0	
	li	t1, VX_FP_TASK
	and	a2, t1			/* mask all but VX_FP_TASK bit */
#endif
	beqz	a2, 0f			/* are we allowed to use fp unit */
	
	/* 到这里都是对的 */
#if 0
	lw	a3, WIND_TCB_PFPCONTEXT(a3)	/* read pFpContext - FIX */		/* 改为参数传递，使用第四个参数（a3）*/
#else
	lw	a3, FRAMEA3(fp_exc_handler)(sp)
#endif
	beqz	a3, 0f			/* must have FP context too */
	
/*#if defined (_WRS_PAL_COPROC_LIB)*/
#if 0	/* 似乎不应该再次读取 */
	/* In PAL/Coproc, a3 points to COPROC_DESC, need pFpContext */
	lw	a3, 0(a3)			/* read pFpContext */
	beqz	a3, 0f			/* must have FP context too */
#endif /* _WRS_PAL_COPROC_LIB */
	/* Must be ok */
	

	b	1f
0:
	/*
	 * At this point there has been an exception from the floating-point
	 * coprocessor but the current task was not allowed to use it.
	 * This isn't really good enough, the task probably *is* using floating
	 * point but the kernel won't be saving its state...
	 */

	lw	t1, _func_log_msg
	beqz	t1, noLogMsg

	la	a0, strayString
	jal	t1			/* let us know about strays */
noLogMsg:
	
	li	a0, IV_CPU_VEC		/* translates to SIGILL */
	b	send_signal

1:
	lw	a0, FRAMEA1(fp_exc_handler)(sp)	/* pEsf to a0 */ 
	lw	a3, M_E_STK_EPC(a0)	/* load the epc into a3 */
	lw	a1, 0(a3)		/* load the instr at the epc into a1 */
	lw	v0, M_E_STK_CAUSE(a0)	/* load the cause register into v0 */
	
	
#ifdef DEBUG
	/* count of number of exceptions seen */
	.comm	_fp_epc, 4
	.comm	_fp_cause, 4
	sw	a3, _fp_epc
	sw	v0, _fp_cause
#endif	
	bltz	v0, 3f			/* in branch delay slot ? */

	/*
	 * This is not in a branch delay slot (branch delay bit not set) so
	 * calculate the resulting pc (epc+4) into v0 and continue to softFp().
	 */
	addu	v0, a3, 4
	sw	v0, FRAMEEPC(fp_exc_handler)(sp)	/* save the resulting pc */
	b	4f
3:
	/*
	 * This is in a branch delay slot so the branch will have to be
	 * emulated to get the resulting pc (done by calling emulateBranch() ).
	 * The arguments to emulateBranch are:
	 *     a0 - pEsf (exception stack)
	 *     a1 - the branch instruction
	 *     a2 - the floating-point control and status register
	 */
#if 0
	lw	a2, M_E_STK_FPCSR(a0)	/* get value of C1_SR */
#endif
	lw	a2, FRAMEA3(fp_exc_handler)(sp)
	lw	a2, F_E_STK_FPCSR(a2)	/* 栈上读取FPSR，是否可以用cfc1 a2, C1_SR来代替 */
	
	
	
	jal	fpp_emulate_branch	/* emulate the branch */
	bne	v0, 1, emulateOk
	
	/* failed to emulate branch correctly */
	li      a0, IV_CPU_VEC		/* translates to SIGILL */
	b	send_signal

emulateOk:
	sw	v0, FRAMEEPC(fp_exc_handler)(sp)	/* save the resulting pc */
	lw	a0, FRAMEA1(fp_exc_handler)(sp)	/* restore exception stack pointer */
	lw	a3, M_E_STK_EPC(a0)	/* load the epc into a3 */
	lw	a1,	4(a3)	/* get instruction to be emulated from BD slot */

/* 解析异常指令，指令存放在a1中。 */
4:
	/*
	 * Check to see if the instruction to be emulated is a floating-point
	 * instruction.  If it is not then this interrupt must have been caused
	 * by writing to the C1_SR a value which will cause an interrupt.
	 * It is possible however that when writing to the C1_SR the
	 * instruction that is to be "emulated" when the interrupt is handled
	 * looks like a floating-point instruction and will incorrectly be
	 * emulated and a SIGILL will not be sent.  This is the user's problem
	 * because he shouldn't write a value into the C1_SR which should
	 * cause an interrupt.
	 *
	 * 5/20/2002 pes - We can also get to this point if we are trying to
	 * emulate an opcode other than OPCODE_C1. Given that we don't know
	 * whether the software FP emulator can handle other opcodes,
	 * it is now deemed more likely for the problem to be unimplemented
	 * opcodes than generating an interrupt with a value written to C1_SR.
	 *
	 */
	
	srl	a3,a1,OPCODE_SHIFT
	
	beq	a3,OPCODE_C1,10f

#if 1
	beq	a3,0x13,10f	/* LUXC1/LWXC1/LDXC1/SUXC1/SWXC1/SDXC1 */
	beq	a3,0x31,10f	/* LWC1 */
	beq	a3,0x35,10f	/* LDC1 */
	beq	a3,0x39,10f	/* SWC1 */
	beq	a3,0x3d,10f	/* SDC1 */
#endif

	/*
	 * Assume the emulator can't handle any opcode other than OPCODE_C1.
	 * Report as Unimplemented FPA operation.
	 */

	li	a0, IV_FPA_UNIMP_VEC
	b	send_signal
10:
	/*
	 * For now all instructions that cause an interrupt are just handed
	 * off to softFp() to emulate it and come up with correct result.
	 * The arguments to softFp() are:
	 *	a0 - pEsf (exception stack)
	 *	a1 - floating-point instruction
	 *	a2 - fpuIsAlive
	 *	a3 - pFpContext
	 *
	 * What might have be done is for all exceptions for which the trapped
	 * result is the same as the untrapped result is: turn off the enables,
	 * re-excute the instruction, restore the enables and then post a
	 * SIGFPE.
	 */

	lw	a2, FRAMEA2(fp_exc_handler)(sp)	/* load exception type */
	
	/* 直接从栈上读取 */
#if 0
	lw	t0, FRAMETID(fp_exc_handler)(sp)/* get current excecuting task */
	lw	a3, WIND_TCB_PFPCONTEXT(t0)	/* read pFpContext - FIX */
#if defined (_WRS_PAL_COPROC_LIB)
	/* In PAL/Coproc, a3 points to COPROC_DESC, need pFpContext */
	lw	a3, 0(a3)			/* read pFpContext */
#endif /* _WRS_PAL_COPROC_LIB */

#else	/* if 0 */
	lw	a3, FRAMEA3(fp_exc_handler)(sp)	/* 浮点寄存器 */
#endif
	
	jal	softFp			/* emulate away */
/*	bne	v0,zero,8f	*/	/* signal posted */ /* LXLMARK */
	nop

#ifdef ASSERTIONS
	/*
	 * If going back to user code without posting a signal there must
	 * not be any exceptions which could cause an interrupt.
	 */
	lw	a2, FRAMEA2(fp_exc_handler)(sp)	/* load exception type */
	beqz	a2,7f			/* skip if no FPU */
	lw	a0, E_STK_FPCSR(a0)	/* get fp status reg   */
	and	a1,a0,CSR_EXCEPT	/* isolate the exception bits */
	and	a0,CSR_ENABLE 		/* isolate the enable bits */
	or	a0,(UNIMP_EXC >> 5)	/* fake an enable for unimplemented */
	sll	a0,5			/* align both bit sets */
	and	a0,a1			/* check for corresponding bits */
	beq	a0,zero,7f		/* if not then ok */
	PANIC("fp_exc_handler csr exceptions")
7:
#endif	/* ASSERTIONS */

	/*
	 * The instruction was emulated by softFp() without a signal being
	 * posted so now change the epc to the target pc.
	 */
	lw	a0, FRAMEA1(fp_exc_handler)(sp)	 /* restore the exception stack pointer */
	lw	v0, FRAMEEPC(fp_exc_handler)(sp) /* get the resulting pc */
	sw	v0, M_E_STK_EPC(a0)	/* store the resulting pc in the epc */

	b	8f
strayFpExc:

	.globl	fpIntr_done
fpIntr_done:
	/* 恢复，模拟完毕 */
8:
	LW	ra,FRAMERA(fp_exc_handler)(sp)
	addu	sp,FRAMESZ(fp_exc_handler)
	move	v0, zero
	j	ra
	
send_signal:
fpuForwardExc:
	/* Force coprocessor unusable exceptions to excExcHandle */
	

#if 0
	la	v0, IV_CPU_VEC
	beq	a0, v0, fpuForceExc
	
	/*
	 * An FPU exception that we can't handle, forward to the
	 * exception vector entered in excBsrTbl[].
	 *	a0 - vector #
	 * Simply load up the vector from excBsrTbl[] and jump to fpuExc.
	 */
	sll	v0, a0, 2
	la	t0, excBsrTbl
	addu	t0, v0
	lw	v0, (t0)
	b	fpuExc
#else
	/* 返回 -1 */
	li	v0, -1
	lw	ra, FRAMERA(fp_exc_handler)(sp)
	j	ra
	nop
#endif
fpuForceExc:
	/*
	 * An FPU exception that we can't handle, force a call
	 * to excExcHandle, do not use the excBsrTbl[] exception table.
	 */
 #if 0
	la	v0, excExcHandle
#else
		/* 返回 -1 */
	li	v0, -2
	lw	ra, FRAMERA(fp_exc_handler)(sp)
	j	ra
	nop
#endif
fpuExc:
	/*
	 * Undo our frame and call the specified exception handler
	 * exception vector entered in excBsrTbl[].
	 *	a0 - vector #
	 *	v0 - exception handler to call.
	 */
#ifdef DEBUG
	.comm	_fp_exc, 4
	sw	a0, _fp_exc
	
	.comm	_fp_exc_vec, 4
	sw	v0, _fp_exc_vec
#endif
	
	lw	a1, FRAMEA1(fp_exc_handler)(sp) /* pEsf to a1 */ 
	la	a2, M_E_STK_SR(a1)	/* pass general register ptr */
	
	/* undo our stack frame */
	LW	ra,FRAMERA(fp_exc_handler)(sp)
	addu	sp,FRAMESZ(fp_exc_handler)
	
	/* tail call to the exception handler, we don't return here. */
	jr	v0
	
	.end	fp_exc_handler

/******************************************************************************
*
* strayString -
*
*/

	
	.rdata
strayString:
	.asciz	"Stray fp exception\n"
	.byte	0

test_str0:
	.asciz	"test_str0\n"
	.byte	0
test_str1:
	.asciz	"test_str1\n"
	.byte	0
test_str2:
	.asciz	"test_str2\n"
	.byte	0
test_str3:
	.asciz	"test_str3\n"
	.byte	0
test_str4:
	.asciz	"test_str4\n"
	.byte	0
test_str5:
	.asciz	"test_str5\n"
	.byte	0
	
format_d:
	.asciz "debug_asm: %d\n"
format_s:
	.asciz "debug_asm: %s\n"
format_x:
	.asciz "debug_asm: %x\n"
