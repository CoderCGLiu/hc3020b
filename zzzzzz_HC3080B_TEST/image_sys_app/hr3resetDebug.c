/*-------------------------------------------文件信息---------------------------------------------------
**
** 文   件   名: hr2resetDebug.c
**
** 创   建   人: xianing
**
** 文件创建日期: 2019 年 09 月 03 日
**
** 描        述: 单板复位功能的调试工具和手段
*********************************************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <taskLib.h>
#include "hr3reset.h"
#include "hr3APIs.h"
//1.全局变量-普通变量
/*
    1)heartBeatFlag：是否发送心跳包标记，0-发送，1-不发送；
    2)heartBeartCnt：发送心跳包计数
    3)peerRecvCnt：点播收包计数
    4)castRecvCnt：组播收包计数
    5)nPeerSocketID0：点播的socket
    6)nCastSocketID0：组播的socket
    7)localIPAddr0：本地IP地址
    8)nCastSocketID0：组播地址
    5)repairFlag：是否进行链路修复的标记，0-不修复，1-修复
    6)regAckPktRecvFlag：是否收到注册回复包的标记，0-没有收到，1-收到

    查看方法：直接在TargetShell下查看
*/

//2.查看结构体变量值
void printRegPkt(struct REGISTER_PKT  *registerPkt)
{
    printf("pktHeader: 0x%x\n",registerPkt->pktHeader);
    printf("mdlType: %s\n",registerPkt->mdlType);
    printf("pktType: %hhu\n",registerPkt->pktType);
    printf("cabnNum: %hhu\n",registerPkt->cabnNum);
    printf("caseNum: %hhu\n",registerPkt->caseNum);
    printf("slotNum: %hhu\n",registerPkt->slotNum);
    printf("dspCnt: %hhu\n",registerPkt->dspCnt);
    printf("dspNum: %hhu\n",registerPkt->dspNum);
    printf("pktTail: 0x%x\n",registerPkt->pktTail);
}

void printRegAckPkt(struct REGISTER_ACK_PKT *registerAckPkt)
{

    printf("pktHeader = 0x%x\n", registerAckPkt->pktHeader);
    printf("pktType = %hhu\n", registerAckPkt->pktType);
    printf("res = %hhu\n", registerAckPkt->res);
    printf("pktTail = 0x%x\n", registerAckPkt->pktTail);
}

void printHeartBeatPkt(struct HEARTBEAT_PKT *heartBeartPkt)
{
    printf("pktHeader: 0x%x\n",heartBeartPkt->pktHeader);
    printf("mdlType: %s\n",heartBeartPkt->mdlType);
    printf("pktType: %hhu\n",heartBeartPkt->pktType);
    printf("cabnNum: %hhu\n",heartBeartPkt->cabnNum);
    printf("caseNum: %hhu\n",heartBeartPkt->caseNum);
    printf("slotNum: %hhu\n",heartBeartPkt->slotNum);
    printf("dspNum: %hhu\n",heartBeartPkt->dspNum);
    printf("hbIndex: %u\n",heartBeartPkt->hbIndex);
    printf("pktTail: 0x%x\n",heartBeartPkt->pktTail);
}

void printBITNetPkt(struct BITNet_PKT *bitNetPkt)
{
    int i = 0;
    printf("pktHeader: 0x%x\n",bitNetPkt->pktHeader);
    printf("mdlType: %s\n",bitNetPkt->mdlType);
    printf("pktType: %hhu\n",bitNetPkt->pktType);
    printf("cabnNum: %hhu\n",bitNetPkt->cabnNum);
    printf("caseNum: %hhu\n",bitNetPkt->caseNum);
    printf("slotNum: %hhu\n",bitNetPkt->slotNum);
    printf("dspNum: %hhu\n",bitNetPkt->dspNum);
    printf("BIT Mess:\n");
    for(i = 0;i<64;i++)
    {
        printf("0x%02hhx ",bitNetPkt->buf[i]);
    }
    printf("pktTail: 0x%x\n",bitNetPkt->pktTail);
}

//3.

