#include <taskLib.h>
#include "hr3APIs.h"
#include "hr3reset.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include "udpSocketLib.h"
/************************** 函数声明 *****************************/
static void changeRegTimeout(unsigned int timeout);
 void OnPeerSocket0(void * pParent,int nID,char * buf,int len);



unsigned int peerRecvCnt = 0;
unsigned int castRecvCnt = 0;
unsigned int regAckPktRecvFlag = 0;
unsigned int repOkAckPktRecvFlag = 0;

//点播socket
int nPeerSocketID0 = -1;
//组播socket
int nCastSocketID0 = -1; 

unsigned int getRegAckPktRecvFlag()
{
	if(regAckPktRecvFlag == 1)
	return 1;
	else
	return 0;
    //return regAckPktRecvFlag;
}

void setRegAckPktRecvFlag()
{
    regAckPktRecvFlag = 1;
}

unsigned int getRepOkAckPktRecvFlag()
{
    return repOkAckPktRecvFlag;
}

void setRepOkAckPktRecvFlag()
{
    repOkAckPktRecvFlag = 1;
}


/*
 * 功能：网络初始化，创建点播、组播socket
 * 输入：
 *      无
 * 输出：
 *      无。
 * 返回值：
 *      无。
 */
extern pthread_t Cast_Thread_ID,Peer_Thread_ID;
extern int Cast_Sock_ID,Peer_Sock_ID;
struct sockaddr_in	ResetserverAddr, ResetCastAddr;
int ResetRcvPeerSock,ResetSendCastSock; 

void netInit()
{

    char routeCmd[100];
    char config_cmd[50];
    int ret  = 0 ;
    unsigned int tmp = 0;
    unsigned int cabnNum = 0;
    unsigned int caseNum = 0;
    unsigned int slotNum = 0;
    //获取模块槽位插箱信息
    cabnNum = sysGetCabnNum();
    caseNum = sysGetCaseNum();
    slotNum = sysGetSlotNum();
    
    printf("<L>HeartBeatTask Info:cabnNum: %u, caseNum: %u, slotNum: %u\n", cabnNum, caseNum, slotNum);
    //格式化本地IP地址
    sprintf(localIPAddr0,"192.161.%u.%u", cabnNum * 4 + caseNum + 1, slotNum * 16 + sysCpuGetID());
    localIPAddr0[strlen(localIPAddr0)] = '\0';

    //格式化组播IP地址
    sprintf(castIPAddr0,"224.0.16.%u", caseNum + 100);
    castIPAddr0[strlen(castIPAddr0)] = '\0';
    
    printf("localIPAddr0:%s,sw1IPAddr:%s,castIPAddr0:%s\n",localIPAddr0,sw1IPAddr,castIPAddr0);
    nPeerSocketID0 = udpCreateSocket(localIPAddr0, NULL, SOCKET0_PEER_PORT);
    nCastSocketID0 = udpCreateSocket(localIPAddr0, castIPAddr0, SOCKET0_CAST_PORT);
    
    if(nPeerSocketID0 < 0)
    {
        printf("<E>HeartBeatTask Info:udp peer socket create fail!\n");
        return;
    }
    else
    {
        printf("<L>HeartBeatTask Info:udp peer socket create ok.\n");
    }

     if(nCastSocketID0 < 0)//|| (nCastSocketID2 < 0))
    {
        printf("<E>HeartBeatTask Info:udp cast socket create fail!\n");
        return;
    }
    else
    {
        printf("<L>HeartBeatTask Info:udp cast socket create ok\n");
    }


     ret =  udpAttachRecv(nPeerSocketID0,(USRFUNCPTR)OnPeerSocket0);
     if(ret <0)
     {
    	  printf("<E>udpAttachRecv  OnPeerSocket0 create fail!\n");
     }
   
	Peer_Thread_ID = udpGetRecvThreadID(nPeerSocketID0);
	Peer_Sock_ID  = udpGetSockID(nPeerSocketID0);
	
	Cast_Thread_ID0  = udpGetRecvThreadID(nCastSocketID0);
	Cast_Sock_ID0   = udpGetSockID(nCastSocketID0);
}

/*
 * 功能：点播接收回调函数，根据接收到的包做相应处理
 * 输入：
 *      无
 * 输出：
 *      无。nPeerSocketID0
 * 返回值：
 *      无。
 */
void OnPeerSocket0(void * pParent,int nID,char * buf,int len)
{
    peerRecvCnt ++;
    if(len < 0)
    {
        printf("peerSocket0 receive package error, recv_len < 0 \n");
        return;
    }

    if(len == sizeof(regAckPkt))
    {
        memcpy((void*)&regAckPkt, (void*)buf, len);
        printf("<L>HeartBeatTask PeerSocket Callback: recv a register ack pkt\n");
        printRegAckPkt(&regAckPkt);

        if((regAckPkt.pktHeader == REGISTER_ACK_PKT_HEADER) && (regAckPkt.pktTail == REGISTER_ACK_PKT_TAIL))
        {
            //注册回复包
            if(regAckPkt.pktType == PKT_TYPE_REG_ACK)
            {
                 //收收注册回复包
                setRegAckPktRecvFlag();
                printf("收到注册回复，start APP work\n");        
            }
            else if(regAckPkt.pktType == PKT_TYPE_REP_ACK)
            {
                 setRepOkAckPktRecvFlag();
            }
            else
            {
                printf("<E>the regAckPkt.pktType of peerSocket0 receive package error.\n");
                return;
            }
        }
    }
    else 
    {
        printf("peerSocket0 receive package length %d.\n",len);
        return;
    }
 }


static void changeRegTimeout(unsigned int timeout)
{
    if(timeout <= 0)
    {
        printf("the timeout value of changeRegTimeout error.\n");
        return;
    }
    s_regTimeout = timeout;
}
