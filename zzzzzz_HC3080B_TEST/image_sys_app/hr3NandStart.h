#ifndef HR2NANDSTART_H
#define HR2NANDSTART_H
#include "hr2APIs.h"
typedef struct {
    int index;
    int filesize;
    int compressFlag;
    char nandPath[100];
    char memPath[100];
}FileInfo_t;

/*
 * Name：Assac_Nand_Start
 * Func：用于assac模块四片同步启动，封装作为入口函数，abcd四片同步调用
 * Author:LSY
*/

void Assac_Nand_Start();

/*
 * Name：hr2NandRioTransfer
 * Func：用于assac模块四片同步启动，封装作为入口函数，abcd四片同步调用
 * Author:LSY
*/

int hr2NandRioTransfer(unsigned int dspid, unsigned int uiBoxID);

/*
 * appInitDSPARecvFun
 * Func：Assac模块四片同步启动阶段的A片Rio挂接回调函数，用于确认接收到的DbInfo信息是否正确
 * Author:LSY
*/

void appInitDSPARecvFun(RapidIO_Recv param);


/*
 * appInitDSPBCDRecvFun
 * Func：Assac模块四片同步启动阶段的BCD片Rio挂接回调函数，用于确认接收到的DbInfo信息是否正确
 * Author:LSY
*/
void appInitDSPBCDRecvFun(RapidIO_Recv param);


/*
 * parseCfgFile
 * Func：解析CfgFile
 * Author:LSY
*/
unsigned char parseCfgFile(unsigned char dspid,char *cfgFilePath,FileInfo_t *cfgFile,unsigned int *filenum);


/*
 * nandDataSendBCD
 * Func：调用rio发送文件到BCD片
 * Author:LSY
*/
unsigned char nandDataSendBCD( union datacast_64 dataAddr,unsigned short controller,char* rioaddr,unsigned int transferlen,unsigned char index,unsigned char filenum,u16 rabBid,u16 rabCid,u16 rabDid);

/*
 * Assac_Nand_Start_NFS
 * Func：利用网络文件系统启动应用
 * Author:LSY
*/
void Assac_Nand_Start_NFS();


#endif
