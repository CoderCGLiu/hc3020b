/*!************************************************************
 * @file: int_perf.c
 * @brief: 该文件为hr3模块外部中断响应测试例子；
 * 该中断性能测试例子需要硬件配置，例如使用gpio还是串口还是其他中断源。
 * @author: 
 * @date: 30,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/30 |   | 创建
 * 
 **************************************************************/#include <stdio.h>
//#include "hr3APIs.h"
//#include "hr3SysTools.h"
#include <time.h>
#include <semLib.h>

extern void  hrGpioIntEnable (int  iIndex);
extern void  hrGpioIntDisable (int  iIndex);
extern u32 hrGpioGetIntNum(u32 gpioIndex);
extern int hrGpioGetDbgInfo(u32 gpioindex);
extern unsigned long long usr_get_time(struct timespec *ts);

static struct timespec ts;
static int dspID = 0;
static unsigned int tstCnt = 10;
static unsigned long long gpioIntTime0 = 0;
static unsigned long long gpioIntTime1 = 0;
static unsigned long long gpioIntTime2 = 0;
unsigned long long gpioIntTime3 = 0;
static int exit_flag = 0;
static SEM_ID  TestsemIDGpio;

void tGpioPerf(void)
{
    while(exit_flag != 1)
    {
        /*触发gpio中断 , A片gpio19去触发gpio18*/
    	
    	/*gpio19: 输出低电平*/
    	hrGpioSetOutputVal(22, 0);
        
        usleep(500000);
        gpioIntTime0 = sys_timestamp();
//        gpioIntTime0 = usr_get_time(&ts);
        
    	/*gpio19: 输出高电平*/
    	hrGpioSetOutputVal(22, 1);
//    	gpioIntTime2 = sys_timestamp();
        
    	
    	usleep(500000);
    }
}

void tGpioPerf2(void)
{
	unsigned int i = 0;


    for(i = 0; i < tstCnt; i++)
    {
        hrGpioGetIntSem(19);/*获取gpio18的信号量*/
        gpioIntTime1 = sys_timestamp();
//        gpioIntTime1 = usr_get_time(&ts);
//        printf("gpioIntTime0:%ld\n\r", gpioIntTime0);
//        printf("gpioIntTime1:%ld\n\r", gpioIntTime1);
        semGive(TestsemIDGpio);
    }
    
    exit_flag = 1;
  
}

 /*
  * 功能：测试GPIO中断性能
  * 输入：
  *      无
  * 输出：
  *      无
  * 返回值：
  *      无
  */
void tstGpioPerf()
{
	dspID = bslProcGetId();
	float perf = 0.0;
	float tmp, tmp2;
	float tMAX = 0;
	float tMIN = 10;
	unsigned int time = 0, time2 = 0;
	unsigned int count1 = 0;
	unsigned int count2 = 0;
	unsigned int count3 = 0;
	unsigned int sum = 0;
	
    printf("[DSP%d] 请输入期望产生GPIO中断的个数:\n",dspID);  
    scanf("%d", &tstCnt);

    printf("[DSP%d] GPIO中断响应时间测试 开始 (测试%d次) ...\n", dspID, tstCnt);
    
    TestsemIDGpio = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    
    /*gpio初始化*/
    bslUsrgGpioInit();/*GPIO18和GPIO19配置为int模式, 分别对应bit19, bit22*/
	
	/*gpio18: 输入模式，上升沿触发中断*/
	hrGpioSetTransDirection(19, 0);
	hrGpioSetIntMode(19, 0);
	hrGpioSetIntEdge(19, 1);
	
	/*gpio19: 输出模式*/
	hrGpioSetTransDirection(22, 1);
	/*gpio19: 输出低电平*/
	hrGpioSetOutputVal(22, 0);
	
	bslTaskCreateCore("tGpioPerf2",180,0,0x200000,tGpioPerf2, 4, 0,1,2,3,4,5,6,7,8,9);
	
	usleep(100000);
	/*gpio18: 使能中断*/
	hrGpioIntEnable(19);
	
	bslTaskCreateCore("tGpioPerf",120,0,0x200000,tGpioPerf, 5, 0,1,2,3,4,5,6,7,8,9);
	
	while(exit_flag != 1)
	{
		semTake(TestsemIDGpio, WAIT_FOREVER);
				
        time = gpioIntTime1 - gpioIntTime0;
//        time = gpioIntTime1 - gpioIntTime0;
        tmp = time * 1000000.0 / sys_timestamp_freq();
        printf("time:%.3fus\n\r", tmp);
//        time2 = gpioIntTime2 - gpioIntTime0;
//        tmp2 = time2 * 1000000.0 / sys_timestamp_freq();
//        printf("time2:%.3fus\n\r", tmp2);
        
        if(tmp >= 2.0)
        	count1++;
        else if(tmp <= 1.0)
        	count2++;
        else
        	count3++;
        	
        if(tmp > tMAX)
        	tMAX = tmp;
        else if(tmp < tMIN)
        	tMIN = tmp;
        
        sum = sum + time;
	}
	
	hrGpioGetDbgInfo(19);
    
    printf("sum = %u\n", sum);
    perf = ((float)sum*1.0)/tstCnt;
    perf = perf * 1.0 * 1000000 / sys_timestamp_freq();
    
    printf("产生GPIO中断个数 %d.\n", hrGpioGetIntNum(19));
    printf("[DSP%d] GPIO中断响应时间为 %.3fus.\n",dspID, perf);
    printf("tMAX=%.3fus, tMIN=%.3fus\n",tMAX,tMIN);
    printf("[>=2us]count1=%d, [<=1us]count2=%d, [>1us, <2us]count3=%d\n",count1, count2, count3);
}
