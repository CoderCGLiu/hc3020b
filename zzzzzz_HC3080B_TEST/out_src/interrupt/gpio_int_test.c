#include <sys/times.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hr3APIs.h"
#include "semLib.h"
#include "taskLib.h"
#include <clock.h>
//gpio中断测试，add chengshunzu 20210225

#if 1
/*测试中断间隔，必须采用原始信号量semID_GpioInt[i]，否则中间间隔不准确
 */
SEM_ID	semGPIO4 = NULL;
SEM_ID	semGPIO5 = NULL;
unsigned int ulGpio4Cnt = 0;	
unsigned int ulGpio5Cnt = 0;	

//获取高精度的系统时间，单位：us
unsigned long long MyBslGetTimeStamp(void)
{
	u32	ulSysTimeFreq = 0;
	u64 ulSysTimeStamp = 0;
	double	dSysTimeInS = 0.0;
	unsigned long long	ullSysTimeInUs = 0;

	ulSysTimeFreq = sys_timestamp_freq();
	ulSysTimeStamp = sys_timestamp();

	dSysTimeInS = (double)ulSysTimeStamp/(double)ulSysTimeFreq;
	ullSysTimeInUs = (unsigned long long)(dSysTimeInS*1000000);

	return ullSysTimeInUs;
}

void GpioISR(unsigned int gpioIndex)
{
	if(gpioIndex == 4)
	{
		ulGpio4Cnt++;
		if (semGPIO4 != NULL)
			semGive(semGPIO4);			
	}
	else if(gpioIndex == 5)
	{
		ulGpio5Cnt++;
		if (semGPIO5 != NULL)
			semGive(semGPIO5);	
	}

}

#if 0
extern SEM_ID semID_GpioInt[8];
void Gpio4Test(void)
{
	unsigned long long t1 = 0;
	unsigned long long t2 = 0;
	unsigned long long ullTic = 0;
	unsigned long long ullToc = 0;
	while(1)
	{
//		t1 = sys_timestamp(); 
//		semTake(semID_GpioInt[4],WAIT_FOREVER);
//		t2 = sys_timestamp(); 
//		printf("\nGpio4! dT=%.1f ", (double)(t2 - t1)/sys_timestamp_freq()*1000000);
		
		ullTic = MyBslGetTimeStamp();
		semTake(semID_GpioInt[4], WAIT_FOREVER);
		ullToc = MyBslGetTimeStamp();
		printf("\nGotGpio-4! dT=%llu ", ullToc-ullTic);
	}
}

void Gpio5Test(void)
{
	unsigned long long t1 = 0;
	unsigned long long t2 = 0;
	unsigned long long ullTic = 0;
	unsigned long long ullToc = 0;
	while(1)
	{
//		t1 = sys_timestamp(); 
//		semTake(semID_GpioInt[5],WAIT_FOREVER);
//		t2 = sys_timestamp(); 
//		printf("\nGpio5! dT=%.1f ", (double)(t2 - t1)/sys_timestamp_freq()*1000000);
		ullTic = MyBslGetTimeStamp();
		semTake(semID_GpioInt[5], WAIT_FOREVER);
		ullToc = MyBslGetTimeStamp();
		printf("\nGotGpio-5! dT=%llu ", ullToc-ullTic);
	}
}


void GpioIntTestCsz()
{
	if(taskSpawn("tAppGpio-4Test",100,VX_FP_TASK,300000,(FUNCPTR)Gpio4Test,0,0,0,0,0,0,0,0,0,0) == ERROR) //3500000	   /*数据处理任务初始化*/
	{
		printf("Application tAppGpio-3Test create error!\n");
	}
	if(taskSpawn("tAppGpio-5Test",100,VX_FP_TASK,300000,(FUNCPTR)Gpio5Test,0,0,0,0,0,0,0,0,0,0) == ERROR) //3500000	   /*数据处理任务初始化*/
	{
		printf("Application tAppGpio-5Test create error!\n");
	}
}

void GpioIntTest(u32 gpioIndex)
{
	bslUsrgGpioConnect(gpioIndex, GpioISR, gpioIndex);
	if (taskSpawn("tAppGpio-4Test",121,VX_FP_TASK,300000,(FUNCPTR)Gpio4Test,0,0,0,0,0,0,0,0,0,0) == ERROR) //3500000	   /*数据处理任务初始化*/
	{
	printf("Application tAppGpio-3Test create error!\n");
	}
	
	if (taskSpawn("tAppGpio-5Test",121,VX_FP_TASK,300000,(FUNCPTR)Gpio5Test,0,0,0,0,0,0,0,0,0,0) == ERROR) //3500000	   /*数据处理任务初始化*/
	{
	printf("Application tAppGpio-5Test create error!\n");
	}
	
    /* 中断挂接处理*/
    bslUsrgGpioInit(4);
    bslUsrgGpioInit(5);
}
#endif


#endif


/******采用任务方式处理中断 begin *****/
unsigned int gpioInt3Count = 0;
unsigned int gpioInt5Count = 0;
unsigned long long t_int3[2] = {0};
unsigned long long t_int5[2] = {0};
unsigned int t_int_flag[2] = {0};
/********add by zwx start********/
/* 计时不能用sys_timestamp, 中断挂接采用任务 */
void gpio_receive_interrupt(unsigned int gpioIndex)
{
//	printk("GPIO %d 产生中断\n", gpioIndex);
	if(gpioIndex == 4)
	{
		if(gpioInt3Count == 0)
		{
			t_int3[0] = tickGet();
		}
		else if(gpioInt3Count == 1000)
		{
			t_int3[1] = tickGet();
			t_int_flag[0] = 0xff;
		}
		gpioInt3Count++;
	}
	else if(gpioIndex == 5)
	{
		if(gpioInt5Count == 0)
		{
			t_int5[0] = tickGet();
		}
		else if(gpioInt5Count == 1000)
		{
			t_int5[1] = tickGet();
			t_int_flag[1] = 0xff;
		}
		gpioInt5Count++;
	}
	if(gpioInt3Count > 1000)
	{
		printk("GPIOINT4 产生中断次数 %d, 0x%x, 0x%x, 总时间%d ms,a\n", gpioInt3Count, t_int3[1], t_int3[0], (t_int3[1] - t_int3[0])*10);
		gpioInt3Count = 0;
		t_int3[0] = 0;
		t_int3[1] = 0;
	}
	else if(gpioInt5Count > 1000)
	{
		printk("GPIOINT5 产生中断次数 %d, 0x%x, 0x%x, 总时间%d ms\n", gpioInt5Count, t_int5[1], t_int5[0], (t_int5[1] - t_int5[0])*10);
		gpioInt5Count = 0;
		t_int5[0] = 0;
		t_int5[1] = 0;
	}
#if 0
	if(gpioIndex == 3)
	{
		gpioInt3Count++;
	}
	else if(gpioIndex == 5)
	{
		gpioInt5Count++;
	}
#endif
}

void gpio_count_printf(void)
{
	printf("int3cnt = %d, int5cnt = %d.\n", gpioInt3Count, gpioInt5Count);
	gpioInt3Count = 0;
	gpioInt5Count = 0;
}
void gpio_interrupt_connect_test(u32 gpioIndex)
{
	printf("gpio_interrupt_connect_test.\n");
	bslUsrgGpioConnect(gpioIndex, gpio_receive_interrupt, gpioIndex);
	while(1)
	{
		if(t_int_flag[0] == 0xff)
		{
			printf("t_int_flag[0] = 0x%x.\n", t_int_flag[0]);
			t_int_flag[0] = 0xfe;
		}
		if(t_int_flag[1] == 0xff)
		{
			printf("t_int_flag[1] = 0x%x.\n", t_int_flag[1]);
			t_int_flag[1] = 0xfe;
		}
		taskDelay(100);
	}
}
/* 采用该函数测试中断 */
void test_gipo_interrupt(u32 gpioIndex)
{
	int res = -1;
	taskSpawn("GpioIntTask",80,VX_FP_TASK,0x80000,(FUNCPTR)gpio_interrupt_connect_test,gpioIndex,0,0,0,0,0,0,0,0,0);
    if(res != ERROR)
    {
        printf("<L>GpioIntTask Info: Create GpioIntTask  Successful!\n");
    }else
    {
        printf("<L>GpioIntTask Info: Create GpioIntTask failure!\n");
    }
    
    /* gpio初始化*/
    bslUsrgGpioInit();
}
/******采用任务方式处理中断 end****/
