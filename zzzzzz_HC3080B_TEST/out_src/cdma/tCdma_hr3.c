/*!************************************************************
 * @file: tCdma_hr3.c
 * @brief: 该文件为hr3模块核内DMA的测试例子
 * @修改内容：所有接口使用用户接口
 * @author:  
 * @date: 18,05,2022
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/18 |   | 创建
 * 
 **************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <taskLib.h>
//#include "hr3APIs.h"
#include "../hr3_demo_cfg.h"

// hr3APIs.h 276
extern int bslCDMA1DStart(char *srcAddr, char *dstAddr, unsigned int totalSize);
// hr3APIs.h 278
extern int bslCDMA2DStart(char *srcAddr, unsigned int srcRowSize, unsigned int srcRowSpan, char *dstAddr, unsigned int dstRowSpan, unsigned int totalSize);
// hr3APIs.h 285
extern int bslCDMAMtxStartCpx(char *srcAddr,unsigned int srcRowSize, unsigned int srcRowSpan, char * dstAddr, unsigned int dstRowSpan, unsigned int srcRowNum,unsigned int srcColNum, unsigned int wrCacheType, unsigned int rdCacheType,unsigned int elementSize);
// 位于hr3SysTools.c 605
extern unsigned long hrKmToPhys(void*);
// 位于tstFFT.c
#ifdef _1HR3_
extern unsigned long long usr_get_time(struct timespec *ts);
#endif
// 位于hr3APIs.h 103
extern unsigned long long bslGetTimeUsec(void);


#ifndef _1HR3_
//#define _1HR3_
#endif

void tstHR3CDMATest_hr3(void *src0,void *dst0,void *src1,void *dst1,int block,int count)
{
    unsigned long phySrcAddr;
    unsigned long phyDstAddr;

    unsigned long ddr0srcAddr = (unsigned long)src0;
    unsigned long ddr0dstAddr = (unsigned long)dst0;
    unsigned long ddr1srcAddr = (unsigned long)src1;
    unsigned long ddr1dstAddr = (unsigned long)dst1;
    int i;
    int uiErrorNum=0;

    /* 要参与计算的块 */
    unsigned int totalTransSize = block * count;
    printf("\n------------测试信息------------\n");
    printf("block:%d,count:%d\n,size:%d\n",block,count,totalTransSize);
    if(abs(ddr0srcAddr - ddr0dstAddr) < totalTransSize)
    {
    	printf("DDR0地址太接近，请重新输入\n");
    	return -1;
    }
    else if(abs(ddr1srcAddr - ddr1dstAddr) < totalTransSize)
    {
    	printf("DDR1地址太接近，请重新输入\n");
    	return -1;
    }
    else
    {/* 正常计算 */}
      
    printf("\n------测试核内DMA一维传输测试------\n");
#if defined(DDR0_ENABLED)
    bslCDMA1DStart((char *)ddr0srcAddr, (char *)ddr0dstAddr, totalTransSize);
    if (memcmp((char *)ddr0srcAddr, (char *)ddr0dstAddr, totalTransSize) != 0) {
        printf("DDR0-DDR0 核内 DMA 一维传输错误.\r\n");
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr0dstAddr);
        printf("DDR0-DDR0 核内 DMA 一维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr0dstAddr,phyDstAddr);
    } 
#endif
   
#if defined(DDR1_ENABLED)
    bslCDMA1DStart((char *)ddr1srcAddr, (char *)ddr1dstAddr, totalTransSize);
    if (memcmp((char *)ddr1srcAddr, (char *)ddr1dstAddr, totalTransSize) != 0) {
        printf("DDR1-DDR1 核内 DMA 一维传输错误.\r\n");
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr1srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR1-DDR1 核内 DMA 一维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr1srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    bslCDMA1DStart((char *)ddr0srcAddr, (char *)ddr1dstAddr, totalTransSize);
    if (memcmp((char *)ddr0srcAddr, (char *)ddr1dstAddr, totalTransSize) != 0) {
        printf("DDR0-DDR1 核内 DMA 一维传输错误.\r\n");
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR0-DDR1 核内 DMA 一维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
    
    printf("\n------测试核内DMA二维传输测试------\n");
#if defined(DDR0_ENABLED)
    uiErrorNum=0;
    bslCDMA2DStart((char *)ddr0srcAddr, 1024,2048, (char *)ddr0dstAddr, 2048, totalTransSize);
    for (i = 0; i < totalTransSize / 1024; i ++)
    {
        if (memcmp((char *)(ddr0srcAddr + 2048 * i),(char *)(ddr0dstAddr + 2048 * i), 1024) != 0)
        {
            uiErrorNum = uiErrorNum + 1;
        }
    }
    if (0!=uiErrorNum) {
        printf("DDR0-DDR0 核内 DMA 二维传输错误.\r\n");
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr0dstAddr);
        printf("DDR0-DDR0 核内 DMA 二维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr0dstAddr,phyDstAddr);
    }
#endif
#if defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMA2DStart((char *)ddr1srcAddr, 1024,2048, (char *)ddr1dstAddr, 2048, totalTransSize);
    for (i = 0; i < totalTransSize / 1024; i ++)
    {
        if (memcmp((char *)(ddr1srcAddr + 2048 * i),(char *)(ddr1dstAddr + 2048 * i), 1024) != 0)
        {
            uiErrorNum = uiErrorNum + 1;
        }
    }
    if (0!=uiErrorNum) {
        printf("DDR1-DDR1 核内 DMA 二维传输错误.\r\n");
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr1srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR1-DDR1 核内 DMA 二维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr1srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMA2DStart((char *)ddr0srcAddr, 1024,2048, (char *)ddr1dstAddr, 2048, totalTransSize);
    for (i = 0; i < totalTransSize / 1024; i ++)
    {
        if (memcmp((char *)(ddr0srcAddr + 2048 * i),(char *)(ddr1dstAddr + 2048 * i), 1024) != 0)
        {
            uiErrorNum = uiErrorNum + 1; 
        }
    }
    if (0!=uiErrorNum) {
        printf("DDR0-DDR1 核内 DMA 二维传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR0-DDR1 核内 DMA 二维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMA2DStart((char *)ddr1srcAddr, 1024,2048, (char *)ddr0dstAddr, 2048, totalTransSize);
    for (i = 0; i < totalTransSize / 1024; i ++)
    {
        if (memcmp((char *)(ddr1srcAddr + 2048 * i),(char *)(ddr0dstAddr + 2048 * i), 1024) != 0)
        {
            uiErrorNum = uiErrorNum + 1; 
        }
    }
    if (0!=uiErrorNum) {
        printf("DDR1-DDR0 核内 DMA 二维传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr1srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr0dstAddr);
        printf("DDR0-DDR1 核内 DMA 二维传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr1srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr0dstAddr,phyDstAddr);
    }
#endif
    
    printf("\n------测试核内DMA矩阵转置传输测试------\n");
    /* 转置功能测试 */
    unsigned int c,r;
    unsigned int row = 1024,col=1024;
    unsigned int elementS = 4;
    unsigned int srcRowS = row * elementS;
    unsigned int srcRowSp = row * elementS;
    unsigned int dstRowSp = col * elementS;
    unsigned int srcRowN = col;
    unsigned int srcColN = row;
    
#if defined(DDR0_ENABLED)
    uiErrorNum=0;
    bslCDMAMtxStartCpx((int *)ddr0srcAddr,srcRowS,srcRowSp,(int *)ddr0dstAddr,dstRowSp,srcRowN,srcColN,0,0,elementS);
    for(r=0;r<row;r++)
    {
    	for(c=0;c<col;c++)
    	{
    		if(*((int *)ddr0srcAddr+r*1024+c) != *((int *)ddr0dstAddr+c*1024+r))
    		{
    			printf("%d::%d\t",*((int *)ddr0srcAddr+r*1024+c),*((int *)ddr0dstAddr+c*1024+r));
    			if((uiErrorNum + 1)%8==0)
    				printf("\n");
                uiErrorNum = uiErrorNum + 1; 
    		}
    	}
    }
    if (0!=uiErrorNum) {
        printf("DDR0-DDR0 核内 DMA 矩阵转置传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr0dstAddr);
        printf("DDR0-DDR0 核内 DMA 矩阵转置传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr0dstAddr,phyDstAddr);
    }
#endif
#if defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMAMtxStartCpx((int *)ddr1srcAddr,srcRowS,srcRowSp,(int *)ddr1dstAddr,dstRowSp,srcRowN,srcColN,0,0,elementS);
    for(r=0;r<1024;r++)
    {
    	for(c=0;c<1024;c++)
    	{
    		if(*((int *)ddr1srcAddr+r*1024+c) != *((int *)ddr1dstAddr+c*1024+r))
    		{
                uiErrorNum = uiErrorNum + 1; 
    		}
    	}
    }
    if (0!=uiErrorNum) {
        printf("DDR1-DDR1 核内 DMA 矩阵转置传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr1srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR1-DDR1 核内 DMA 矩阵转置传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr1srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMAMtxStartCpx((int *)ddr0srcAddr,srcRowS,srcRowSp,(int *)ddr1dstAddr,dstRowSp,srcRowN,srcColN,0,0,elementS);
    for(r=0;r<1024;r++)
    {
    	for(c=0;c<1024;c++)
    	{
    		if(*((int *)ddr0srcAddr+r*1024+c) != *((int *)ddr1dstAddr+c*1024+r))
    		{
                uiErrorNum = uiErrorNum + 1; 
    		}
    	}
    }
    if (0!=uiErrorNum) {
        printf("DDR0-DDR1 核内 DMA 矩阵转置传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr0srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr1dstAddr);
        printf("DDR0-DDR1 核内 DMA 矩阵转置传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr0srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr1dstAddr,phyDstAddr);
    }
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    uiErrorNum=0;
    bslCDMAMtxStartCpx((int *)ddr1srcAddr,srcRowS,srcRowSp,(int *)ddr0dstAddr,dstRowSp,srcRowN,srcColN,0,0,elementS);
    for(r=0;r<1024;r++)
    {
    	for(c=0;c<1024;c++)
    	{
    		if(*((int *)ddr1srcAddr+r*1024+c) != *((int *)ddr0dstAddr+c*1024+r))
    		{
                uiErrorNum = uiErrorNum + 1; 
    		}
    	}
    }
    if (0!=uiErrorNum) {
        printf("DDR1-DDR0 核内 DMA 矩阵转置传输错误.\r\n"); 
    } else {
    	phySrcAddr = hrKmToPhys((void *)ddr1srcAddr);
    	phyDstAddr = hrKmToPhys((void *)ddr0dstAddr);
        printf("DDR1-DDR0 核内 DMA 矩阵转置传输成功.\r\n");
        printf("srcAddress[L:0x%lx P:0x%lx].\r\n",ddr1srcAddr,phySrcAddr);
        printf("dstAddress[L:0x%lx P:0x%lx].\r\n",ddr0dstAddr,phyDstAddr);
    }
#endif
    return;
}


void tstHR3CDMAPerf_hr3(void *src0,void *dst0,void *src1,void *dst1,int count)
{
    unsigned long ddr0srcPhyAddr = (unsigned long)src0;
    unsigned long ddr0dstPhyAddr = (unsigned long)dst0;
    unsigned long ddr1srcPhyAddr = (unsigned long)src1;
    unsigned long ddr1dstPhyAddr = (unsigned long)dst1;

    unsigned int transSize = 0;
    struct timespec ts;
    int chunk;
    int block;
    unsigned long long tt1,tt2, time;
    float speed;
    int i;

    int packNum;
    packNum = count;

    printf("\n------测试核内DMA一维传输性能------\n");
#if defined(DDR0_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {

#ifdef _1HR3_	     
	tt1 = usr_get_time(&ts);
#else
	tt1 = bslGetTimeUsec();
#endif
        
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA1DStart(((char *)ddr0srcPhyAddr+i*transSize), ((char *)ddr0dstPhyAddr+i*transSize), transSize);
        }
#ifdef _1HR3_
	tt2 = usr_get_time(&ts);
#else		
	tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
		printf("DDR0 -> DDR0 核内DMA传输%d包, 每次传输数据量：%dKB, 花费时间：%lluus, 速度 = %.2f MBps\n",
				packNum, transSize/1024,time,speed);
		tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA1DStart(((char *)ddr1srcPhyAddr+i*transSize), ((char *)ddr1dstPhyAddr+i*transSize), transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
		printf("DDR1 -> DDR1 核内DMA传输%d包, 每次传输数据量：%dKB, 花费时间：%lluus, 速度 = %.2f MBps\n",
				packNum, transSize/1024,time,speed);
		tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA1DStart(((char *)ddr0srcPhyAddr+i*transSize), ((char *)ddr1dstPhyAddr+i*transSize), transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
		printf("DDR0 -> DDR1 核内DMA传输%d包, 每次传输数据量：%dKB, 花费时间：%lluus, 速度 = %.2f MBps\n",
				packNum, transSize/1024,time,speed);
		tt1 = tt2 = 0;
    }

    printf("\n");
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA1DStart(((char *)ddr1srcPhyAddr+i*transSize), ((char *)ddr0dstPhyAddr+i*transSize), transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
		printf("DDR1 -> DDR0 核内DMA传输%d包, 每次传输数据量：%dKB, 花费时间：%lluus, 速度 = %.2f MBps\n",
				packNum, transSize/1024,time,speed);
		tt1 = tt2 = 0;
    }
    printf("\n");
#endif

    printf("\n------测试核内DMA二维传输测试------\n");
#if defined(DDR0_ENABLED)
    chunk = 256;
    block = 256;
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i++)
        {
            bslCDMA2DStart(((char *)ddr0srcPhyAddr+i*transSize), chunk,block, ((char *)ddr0dstPhyAddr+i*transSize), chunk, transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif

        chunk = chunk * 2;
        block = block * 2;

		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR0 -> DDR0 核内DMA二维传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR1_ENABLED)
    chunk = 256;
    block = 256;

    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA2DStart(((char *)ddr1srcPhyAddr+i*transSize), chunk,block, ((char *)ddr1dstPhyAddr+i*transSize), chunk, transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif

        chunk = chunk * 2;
        block = block * 2;

		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR1 -> DDR1 核内DMA二维传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)

   chunk = 256;
   block = 256;

   for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
   {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
       for(i = 0; i < packNum; i ++)
       {
           bslCDMA2DStart(((char *)ddr0srcPhyAddr+i*transSize), chunk,block, ((char *)ddr1dstPhyAddr+i*transSize), chunk, transSize);
       }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif

       chunk = chunk * 2;
       block = block * 2;

		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
       printf("DDR0 -> DDR1 核内DMA二维传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
               packNum, transSize/1024,time,speed);
       tt1 = tt2 = 0;
   }
   printf("\n");
#endif

#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    chunk = 256;
    block = 256;

    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMA2DStart(((char *)ddr1srcPhyAddr+i*transSize), chunk,block, ((char *)ddr0dstPhyAddr+i*transSize), chunk, transSize);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif

        chunk = chunk * 2;
        block = block * 2;

		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR1 -> DDR0 核内DMA二维传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
    printf("\n------测试核内DMA矩阵转置传输性能------\n");
    unsigned int row = 1024,col=1024;
    unsigned int elementS = 4;
    unsigned int srcRowS = row * elementS;
    unsigned int srcRowSp = row * elementS;
    unsigned int dstRowSp = col * elementS;
    unsigned int srcRowN = col;
    unsigned int srcColN = row;

#if defined(DDR0_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMAMtxStartCpx(((char *)ddr0srcPhyAddr+i*transSize),srcRowS,srcRowSp,((char *)ddr0dstPhyAddr+i*transSize),dstRowSp,srcRowN,transSize / srcRowN,0,0,elementS);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR0 -> DDR0 核内DMA矩阵转置传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMAMtxStartCpx(((char *)ddr1srcPhyAddr+i*transSize),srcRowS,srcRowSp,((char *)ddr1dstPhyAddr+i*transSize),dstRowSp,srcRowN,transSize / srcRowN,0,0,elementS);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR1 -> DDR1 核内DMA矩阵转置传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMAMtxStartCpx(((char *)ddr0srcPhyAddr+i*transSize),srcRowS,srcRowSp,((char *)ddr1dstPhyAddr+i*transSize),dstRowSp,srcRowN,transSize / srcRowN,0,0,elementS);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR0 -> DDR1 核内DMA矩阵转置传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif
    
#if defined(DDR0_ENABLED) && defined(DDR1_ENABLED)
    for(transSize = 0x4000; transSize <=0x100000; transSize = transSize * 4)
    {
#ifdef _1HR3_
        tt1 = usr_get_time(&ts);
#else		
        tt1 = bslGetTimeUsec();
#endif
        for(i = 0; i < packNum; i ++)
        {
            bslCDMAMtxStartCpx(((char *)ddr1srcPhyAddr+i*transSize),srcRowS,srcRowSp,((char *)ddr0dstPhyAddr+i*transSize),dstRowSp,srcRowN,transSize / srcRowN,0,0,elementS);
        }
#ifdef _1HR3_
        tt2 = usr_get_time(&ts);
#else		
        tt2 = bslGetTimeUsec();
#endif
		time = tt2 - tt1;
		speed = (float)((packNum  * 1.0 * transSize * 1000000) / time / 1024 / 1024);
        printf("DDR1 -> DDR0 核内DMA矩阵转置传输%d包, 每次传输数据量：%dKB, 花费时间：%lldus, 速度 = %.2f MBps\n",
                packNum, transSize/1024,time,speed);
        tt1 = tt2 = 0;
    }
    printf("\n");
#endif 
    
    return;
}


