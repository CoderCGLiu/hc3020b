#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <taskLib.h>
#include "tool/hr3APIs.h"
#include <semaphore.h>
#include <clock.h>

/* 
 * add by chengshunzu 20210201
 * memcpy�����ܲ��� 
 */

sem_t task_sem[8];

void tstHR2MemcpyPerf_1(unsigned int datanum)
{
    unsigned long long t1 = 0;
    unsigned long long t2 = 0;
    int tstNum;
    int count = 0;

    int *test_data1 = NULL;
    int *test_data2 = NULL;
    
    test_data1 = (int *)malloc(1024 * 1024 * 5);
    if(test_data1 == NULL)
    {
        printf("malloc testdata1 mem error.\n");
        return;
    }
    
    test_data2 = (int *)malloc(1024 * 1024 * 5);
    if(test_data2 == NULL)
    {
        printf("malloc testdata2 mem error.\n");
        return;
    }
    
    memset(test_data1, 0, 1024 * 1024 * 5);
    memset(test_data2, 0, 1024 * 1024 * 5);
    
    tstNum = 1e4;
    t1 = bslGetTimeUsec(); 
    printf("begin Perf1 memcpy, t1 = %lld, datanum=%d. (memcpy, cdma, gdma)\n", t1, datanum);
    for(count = 0; count < tstNum; count++)
    {
    	if(datanum == 0)
    	{
    		memcpy(test_data1, test_data2, 1024 * 1024 * 5);
    	}
    	else if(datanum == 1)
    	{
    		bslCDMA1DStart((char *)test_data1, (char *)test_data2, 1024 * 1024 * 5);
    	}
    	else if(datanum == 2)
    	{
    		bslGDMA1DStart((char *)test_data1, (char *)test_data2, 1024 * 1024 * 5);
    	}
	}
    t2 = bslGetTimeUsec();
    printf("end memcpy, t2 = %lld\n", t2);
    printf("    memcpy (5*1024*1024Bytes * %d) = %.1f ��\n", tstNum, (double)(t2 - t1)/1000000);
    
    free(test_data1);
    test_data1 = NULL;
    
    free(test_data2);
    test_data2 = NULL;
    
}

void tstHR2MemcpyPerf_2(unsigned int datanum)
{
    unsigned long long t1 = 0;
    unsigned long long t2 = 0;
    int tstNum;
    int count = 0;

    int *test_data1 = NULL;
    int *test_data2 = NULL;
    
    test_data1 = (int *)malloc(1024 * 5);
    if(test_data1 == NULL)
    {
        printf("malloc testdata1 mem error.\n");
        return;
    }
    
    test_data2 = (int *)malloc(1024 * 5);
    if(test_data2 == NULL)
    {
        printf("malloc testdata2 mem error.\n");
        return;
    }
    memset(test_data1, 0, 1024 * 5);
    memset(test_data2, 0, 1024 * 5);
    
    tstNum = 1e6;
    t1 = bslGetTimeUsec(); 
    printf("begin Perf2 memcpy, t1 = %lld, datanum=%d. (memcpy, cdma, gdma)\n", t1, datanum);
    for(count = 0; count < tstNum; count++)
    {
    	if(datanum == 0)
    	{
    		memcpy(test_data1, test_data2, 1024 * 5);
    	}
    	else if(datanum == 1)
    	{
    		bslCDMA1DStart((char *)test_data1, (char *)test_data2, 1024 * 5);
    	}
    	else if(datanum == 2)
    	{
    		bslGDMA1DStart((char *)test_data1, (char *)test_data2, 1024 * 5);
    	}
	}
    t2 = bslGetTimeUsec();
    printf("end memcpy, t2 = %lld\n", t2);
    printf("    memcpy (5*1024Bytes * %d) = %.1f ��\n", tstNum, (double)(t2 - t1)/1000000);
//    printf("    memcpy (5*1024Bytes * %d) = %.1f ΢��\n", tstNum, (double)(t2 - t1)/sys_timestamp_freq()*1000000);
    
    free(test_data1);
    test_data1 = NULL;
    
    free(test_data2);
    test_data2 = NULL;
}

/*
 * datanum:0,ֱ��memcpy
 * datanum:1,��˲�����cdma����
 * datanum:2,gdma����
 */
void tstHR2MemcpyPerf_3(unsigned int datanum)
{
    unsigned long long t1 = 0;
    unsigned long long t2 = 0;
    int tstNum;
    int count = 0;
    unsigned int databyte = 0;
    databyte =1024 * (512+128);

    int *test_data1 = NULL;
    int *test_data2 = NULL;
    
    
    test_data1 = (int *)malloc(databyte);
    if(test_data1 == NULL)
    {
        printf("malloc testdata1 mem error.\n");
        return;
    }
    
    test_data2 = (int *)malloc(databyte);
    if(test_data2 == NULL)
    {
        printf("malloc testdata2 mem error.\n");
        return;
    }
    memset(test_data1, 0, databyte);
    memset(test_data2, 0, databyte);
    
    tstNum = 1;//1e4;
    t1 = bslGetTimeUsec();//bslGetTimeUsec(); 
    
    for(count = 0; count < tstNum; count++)
    {
    	if(datanum == 0)
    	{
//    		memcpy(test_data1, test_data2, databyte);
    		memcpy((void*)0x30000000, (void*)0x40000000, databyte);
    	}
    	else if(datanum == 1)
    	{
    		bslCDMA1DStart((char *)test_data1, (char *)test_data2, databyte);
    	}
    	else if(datanum == 2)
    	{
    		bslGDMA1DStart((char *)test_data1, (char *)test_data2, databyte);
    	}
	}
    t2 = bslGetTimeUsec();//bslGetTimeUsec();
    printf("begin Perf3 memcpy, t1 = %lld, datanum=%d. (memcpy, cdma, gdma)\n", t1, datanum);
    printf("end memcpy, t2 = %lld\n", t2);
    printf("    memcpy (512KBytes * %d) = %.1f ΢��\n", tstNum, (double)(t2 - t1));
    
    free(test_data1);
    test_data1 = NULL;
    
    free(test_data2);
    test_data2 = NULL;
}

void task_cdma_1(void)
{
	tstHR2MemcpyPerf_1(1);
}

void task_cdma_2(void)
{
	tstHR2MemcpyPerf_2(1);
}

void task_clear_0()
{
	sem_wait(&task_sem[0]);
	memcpy((void*)0x10000000, (void*)0x20000000, 1024 * 1024 *2);
	sem_post(&task_sem[4]);
}
void task_clear_1()
{
	sem_wait(&task_sem[1]);
	memcpy((void*)0x11000000, (void*)0x21000000, 1024 * 1024 *2);
	sem_post(&task_sem[5]);
}
void task_clear_2()
{
	sem_wait(&task_sem[2]);
	memcpy((void*)0x12000000, (void*)0x22000000, 1024 * 1024 *2);
	sem_post(&task_sem[6]);
}
void task_clear_3()
{
	sem_wait(&task_sem[3]);
	memcpy((void*)0x13000000, (void*)0x23000000, 1024 * 1024 *2);
	sem_post(&task_sem[7]);
}

int test_cdma(int flag)
{
	TASK_ID res;
	TASK_ID error_res = -1;
    sem_init(&task_sem[0], 0, 0);
    sem_init(&task_sem[1], 0, 0);
    sem_init(&task_sem[2], 0, 0);
    sem_init(&task_sem[3], 0, 0);
    sem_init(&task_sem[4], 0, 0);
    sem_init(&task_sem[5], 0, 0);
    sem_init(&task_sem[6], 0, 0);
    sem_init(&task_sem[7], 0, 0);
    printf("-------------1\n");
    /* 5MB cdma */
    if(flag == 0)
    {
        res = bslTaskCreateCore("taskcdma_1", 100, 0,0x20000,(FUNCPTR)task_cdma_1, 0,0,1,2,3,4,5,6,7,8,9);
        if(res == error_res)
        {
            printf("Core 0 Create task_cdma_1 ERROR!\n");
            return -1;
        }
    }
    /* 5KB cdma */
    if(flag == 1)
    {
        res = bslTaskCreateCore("taskcdma_2", 100, 0,0x20000,(FUNCPTR)task_cdma_2, 0,0,1,2,3,4,5,6,7,8,9);
        if(res == error_res)
        {
            printf("Core 0 Create task_cdma_2 ERROR!\n");
            return -1;
        }
    }
    
    /* ��cache */
    if(flag == 3)
    {
        res = bslTaskCreateCore("taskclear_0", 100, 0,0x20000,(FUNCPTR)task_clear_0, 0,0,1,2,3,4,5,6,7,8,9);
        printf("-------------2\n");
        if(res == error_res)
        {
            printf("Core 0 Create taskclear_0 ERROR!\n");
            return -1;
        }
        res = bslTaskCreateCore("taskclear_1", 100, 0,0x20000,(FUNCPTR)task_clear_1, 1,0,1,2,3,4,5,6,7,8,9);
        printf("-------------1\n");
        if(res == error_res)
        {
            printf("Core 1 Create taskclear_1 ERROR!\n");
            return -1;
        }
        res = bslTaskCreateCore("taskclear_2", 100, 0,0x20000,(FUNCPTR)task_clear_2, 2,0,1,2,3,4,5,6,7,8,9);
        printf("-------------3\n");
        if(res == error_res)
        {
            printf("Core 2 Create taskclear_2 ERROR!\n");
            return -1;
        }
        res = bslTaskCreateCore("taskclear_3", 100, 0,0x20000,(FUNCPTR)task_clear_3, 3,0,1,2,3,4,5,6,7,8,9);
        if(res == error_res)
        {
            printf("Core 3 Create taskclear_3 ERROR!\n");
            return -1;
        }
        
        sleep(1);
        sem_post(&task_sem[0]);
        sem_post(&task_sem[1]);
        sem_post(&task_sem[2]);
        sem_post(&task_sem[3]);
        
        sem_wait(&task_sem[7]);
        sem_wait(&task_sem[4]);
        sem_wait(&task_sem[5]);
        sem_wait(&task_sem[6]);
        
        tstHR2MemcpyPerf_3(0);
    }
    return 0;
}

void test_task(void)
{
	bslTaskCreateCore("test_cdma", 100, 0,0x20000,(FUNCPTR)test_cdma, 3,3,1,2,3,4,5,6,7,8,9);
}
