/*!************************************************************
 * @file: tSMP.c
 * @brief: 该文件为hr3模块利用visipl库对绑核和多核并发进行测试
 * @author: 
 * @date: 18,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/18 |  | 创建
 * 
 **************************************************************/

/*
 * 头文件
 * */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <taskLib.h>
#include <math.h>
#include <memLib.h>
//#include "hr3APIs.h"
#include "../visipl/vsip.h"

/*
 * 外部函数声明
 * */
// tstFFT.c
extern int ccfftipST(float *src1,float *src2,float *dst);


/*
 * 函数声明
 * */
void tstFFTApp();

void tstDspSmp();

/*
 * 函数实现
 * */

//FFT程序
void tstFFTApp()
{

   int i;
   int len=32*1024*1024; 
   float *src1, *src2;
   float *dst;
   unsigned long maxtime=0, mintime=0x7fffffff;
   *(unsigned int*)0x10000000 = 0;
   *(unsigned int*)0x10000004 = 0x7fffffff;

   srand(1);

   //real number buffer len*4, complex number buffer len*8
   src1 = (float *)memalign(128, len*4);
   src2 = (float *)memalign(128, len*4); 
   dst =  (float *)memalign(128, len*4);

   printf("src1_addr = %#x, src2_addr = %#x, dst_addr = %#x\n", src1, src2, dst);
   printf("测试SMP功能.\n");

   printf("开始初始化源数据...\n");
   for(i=0; i<len; i++)
   {
       src1[i] = (1.0 + i) / 100.0;
       src2[i] = 10.0 * (len-i);
   }

   printf("源数据初始化完成.\n");
   vsip_init((void*)0);
   printf("\n8K点FFT依次绑定core0-core3运行100次\n");
   printf("core\tData(K Point)\tAverTime(us)\n"); 
    for(i=0;i<4;i++)
    {
//        printf("\ntest ccfftip STperformance\n");

        bslTaskCreateCore("ccfftipST",100,VX_FP_TASK, 0x20000,(FUNCPTR)ccfftipST,i,
                src1+i*8*1024*2,src2+i*8*1024*2,dst+i*8*1024*2,3,4,5,6,7,8,9);
        sleep(3);

    }
    printf("\n8K点FFT分别绑定core0-core3同时运行100次\n");
    printf("core\tData(K Point)\tAverTime(us)\n");
    for(i=0;i<4;i++)
    {
//     printf("\ntest ccfftip STperformance\n");
       bslTaskCreateCore("ccfftipST",100,VX_FP_TASK, 0x20000,(FUNCPTR)ccfftipST,i,
               src1+i*8*1024*2,src2+i*8*1024*2,dst+i*8*1024*2,3,4,5,6,7,8,9);
    }
    sleep(3);

   maxtime = *(unsigned int*)0x10000000;
   mintime = *(unsigned int*)0x10000004;


    printf("不同核运行FFT的最大时间相差比例为 %.3f%%,", 1.0*(maxtime-mintime)/maxtime * 100);
    if(1.0*(maxtime-mintime)/maxtime <= 0.4)
    {
        printf("小于40%%.\n");
    }else
    {
        printf("大于40%%.\n");
    }

    vsip_finalize((void*)0);
    free(src1);
    free(src2);
    free(dst);

    return  (0);
}



void tstDspSmp()
{
    tstFFTApp();
}

