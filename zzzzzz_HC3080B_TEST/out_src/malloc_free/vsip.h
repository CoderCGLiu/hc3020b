/***********************************************************************
*      _   _  ___  ____                                        
*     | \ | |/ _ \/ ___|                            N.A.Software Ltd
*     |  \| | |_| \___ \                            1 Prospect Road
*     | |\  |  _  |___) |                           Prenton
*     |_| \_|_| |_|____/                            CH42 8LE
* 
*     COPYRIGHT (c) 2005 N.A.Software Ltd          +44 (0) 151 609 1911
* 
************************************************************************
  
  Project      :  VSIPL
  Created      :  April 2014
  Description  :  Main VSIP library header file
  Version      :  4.13.13
  ***********************************************************************/

#ifndef VSIP_HEADER_H
#define VSIP_HEADER_H

/* standard include files */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <limits.h>

/************************************************************************
 * VSIPL types
 ************************************************************************/
/* #defined limits and variables */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#define VSIP_PI    M_PI

#define VSIP_MAX_SCALAR_F  FLT_MAX
#define VSIP_MAX_SCALAR_I  INT_MAX
#define VSIP_MAX_SCALAR_SI SHRT_MAX
#define VSIP_MAX_SCALAR_BL VSIP_MAX_SCALAR_I

#define VSIP_MIN_SCALAR_F  FLT_MIN
#define VSIP_MIN_SCALAR_I  INT_MIN
#define VSIP_MIN_SCALAR_SI SHRT_MIN
#define VSIP_MIN_SCALAR_BL VSIP_MIN_SCALAR_I

#define VSIP_MIN_SCALAR_VI 0
#define VSIP_MAX_SCALAR_VI UINT_MAX     /* ULONG_MAX   zhongkai modified for vi use unsigned int */

#ifndef POINTER_SIZE_64BIT
#define POINTER_SIZE_64BIT
#endif

#define VSIP_FALSE 0
#define VSIP_TRUE  1
typedef enum {
  VSIP_ALG_TIME = 0,		/*  Minimize execution time */
  VSIP_ALG_SPACE = 1,		/*  Minimize memory required */
  VSIP_ALG_NOISE = 2		/*  Minimize computational noise, Maximize Accuracy */
} vsip_alg_hint;

typedef enum {
  VSIP_BIASED = 0,		/*  Biased */
  VSIP_UNBIASED = 1		/*  Unbiased */
} vsip_bias;

struct vsip_blockobject_bl;

typedef struct vsip_blockobject_bl vsip_block_bl;

struct vsip_blockobject_f;

typedef struct vsip_blockobject_f vsip_block_f;

struct vsip_blockobject_i;

typedef struct vsip_blockobject_i vsip_block_i;

struct vsip_blockobject_mi;

typedef struct vsip_blockobject_mi vsip_block_mi;

struct vsip_blockobject_si;

typedef struct vsip_blockobject_si vsip_block_si;

struct vsip_blockobject_vi;

typedef struct vsip_blockobject_vi vsip_block_vi;

struct vsip_cblockobject_f;

typedef struct vsip_cblockobject_f vsip_cblock_f;

struct vsip_cblockobject_i;

typedef struct vsip_cblockobject_i vsip_cblock_i;

struct vsip_cblockobject_si;

typedef struct vsip_cblockobject_si vsip_cblock_si;

#ifdef POINTER_SIZE_64BIT
#if defined(_WIN64)
typedef unsigned long long vsip_scalar_vi;
#else
typedef unsigned int vsip_scalar_vi;
#endif
#else
typedef unsigned int vsip_scalar_vi;
#endif

typedef vsip_scalar_vi vsip_length;

typedef enum {
  VSIP_TR_LOW = 0,		/* only the lower half of the matrix is stored */
  VSIP_TR_UPP = 1		/* only the upper half of the matrix is stored */
} vsip_mat_uplo;

typedef struct {
  vsip_mat_uplo uplo;		/*  Upper or lower triangular matrix */
  vsip_length n;		/*  Matrix size is N by N */
} vsip_cchol_attr_f;

struct vsip_ccholdobject_f;

typedef struct vsip_ccholdobject_f vsip_cchol_f;

typedef enum {
  VSIP_SUPPORT_FULL = 0,	/*Maximum region */
  VSIP_SUPPORT_SAME = 1,	/*Input and output same size */
  VSIP_SUPPORT_MIN = 2		/*Region without zero extending the kernel, reference */
} vsip_support_region;

typedef struct {
  vsip_scalar_vi ref_len;	/*  Reference length */
  vsip_scalar_vi data_len;	/*  Data input length */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_vi lag_len;	/*  Output (lags) length */
} vsip_ccorr1d_attr_f;

struct vsip_ccorr1dobject_f;

typedef struct vsip_ccorr1dobject_f vsip_ccorr1d_f;

typedef struct {
  vsip_scalar_vi r, c;
} vsip_scalar_mi;

typedef enum {
  VSIP_NONSYM = 0,		/*  Non-symmetric */
  VSIP_SYM_EVEN_LEN_ODD = 1,	/*  (Even) Symmetric, odd length */
  VSIP_SYM_EVEN_LEN_EVEN = 2	/*  (Even) Symmetric, even length */
} vsip_symmetry;

typedef struct {
  vsip_scalar_mi ref_size;	/*  Reference data size, M by N */
  vsip_scalar_mi data_size;	/*  Data size, P by Q */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_mi lag_size;	/*  Output size, S by T */
} vsip_ccorr2d_attr_f;

struct vsip_ccorr2dobject_f;

typedef struct vsip_ccorr2dobject_f vsip_ccorr2d_f;

typedef enum {
  VSIP_STATE_NO_SAVE = 1,	/*  Save state for continuous filtering */
  VSIP_STATE_SAVE = 2		/*  Don't save state, single call filter */
} vsip_obj_state;

typedef struct {
  vsip_scalar_vi kernel_len;	/*  Kernel length */
  vsip_symmetry symm;		/*  Kernel symmetry */
  vsip_scalar_vi in_len;	/*  Filter input segment length */
  vsip_scalar_vi out_len;	/*  Filter output segment length */
  vsip_length decimation;	/*  Decimation factor */
  vsip_obj_state state;		/*  Save state information */
} vsip_cfir_attr_f;

struct vsip_cfirobject_f;

typedef struct vsip_cfirobject_f vsip_cfir_f;

typedef struct {
  vsip_mat_uplo uplo;		/*  Upper or lower triangular matrix */
  vsip_length n;		/*  Matrix size is N by N */
} vsip_chol_attr_f;

struct vsip_choldobject_f;

typedef struct vsip_choldobject_f vsip_chol_f;

typedef struct {
  vsip_length n;		/*  Matrix size is N by N */
} vsip_clu_attr_f;

struct vsip_cluobject_f;

typedef struct vsip_cluobject_f vsip_clu_f;

typedef vsip_scalar_vi vsip_offset;

#ifdef POINTER_SIZE_64BIT
#if defined(_WIN64)
typedef signed long long vsip_stride;
#else
typedef signed long int vsip_stride;
#endif
#else
typedef signed int vsip_stride;
#endif

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_cblock_f *block;
} vsip_cmattr_f;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_cblock_i *block;
} vsip_cmattr_i;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_cblock_si *block;
} vsip_cmattr_si;

typedef enum {
  VSIP_CMPLX_INTERLEAVED = 0,	/* Interleaved */
  VSIP_CMPLX_SPLIT = 1,		/* Split, separate real and imaginary */
  VSIP_CMPLX_NONE = 2		/* No preferred storage layout */
} vsip_cmplx_mem;

/* default memory layout for this library is split */
#define VSIP_CMPLX_MEM VSIP_CMPLX_SPLIT

struct vsip_cmviewobject_f;

typedef struct vsip_cmviewobject_f vsip_cmview_f;

struct vsip_cmviewobject_i;

typedef struct vsip_cmviewobject_i vsip_cmview_i;

struct vsip_cmviewobject_si;

typedef struct vsip_cmviewobject_si vsip_cmview_si;

typedef struct {
  vsip_scalar_vi kernel_len;	/*  Kernel length, M */
  vsip_symmetry symm;		/*  Kernel symmetry */
  vsip_scalar_vi data_len;	/*  Data input length */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_vi out_len;	/*  Output length */
  vsip_length decimation;	/*  Output decimation factor, D */
} vsip_conv1d_attr_f;

struct vsip_conv1dobject_f;

typedef struct vsip_conv1dobject_f vsip_conv1d_f;

typedef struct {
  vsip_scalar_mi kernel_size;	/*  Kernel size, M by N */
  vsip_symmetry symm;		/*  Kernel symmetry */
  vsip_scalar_mi in_size;	/*  Data input size, P by Q */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_mi out_size;	/*  Output size, S by T */
  vsip_length decimation;	/*  Output decimation factor, D */
} vsip_conv2d_attr_f;

struct vsip_conv2dobject_f;

typedef struct vsip_conv2dobject_f vsip_conv2d_f;

typedef struct {
  vsip_scalar_vi ref_len;	/*  Reference length */
  vsip_scalar_vi data_len;	/*  Data input length */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_vi lag_len;	/*  Output (lags) length */
} vsip_corr1d_attr_f;

struct vsip_corr1dobject_f;

typedef struct vsip_corr1dobject_f vsip_corr1d_f;

typedef struct {
  vsip_scalar_mi ref_size;	/*  Reference data size, M by N */
  vsip_scalar_mi data_size;	/*  Data size, P by Q */
  vsip_support_region support;	/*  Output region of support */
  vsip_scalar_mi lag_size;	/*  Output size, S by T */
} vsip_corr2d_attr_f;

struct vsip_corr2dobject_f;

typedef struct vsip_corr2dobject_f vsip_corr2d_f;

typedef enum {
  VSIP_QRD_NOSAVEQ = 0,		/*  Do not save Q */
  VSIP_QRD_SAVEQ = 1,		/*  Save Q */
  VSIP_QRD_SAVEQ1 = 2		/*  Save Skinny Q */
} vsip_qrd_qopt;

typedef struct {
  vsip_length m;		/*  Input matrix is M by N */
  vsip_length n;		/*  Input matrix is M by N */
  vsip_qrd_qopt Qopt;		/*  Matrix Q is saved/not saved */
} vsip_cqr_attr_f;

struct vsip_cqrobject_f;

typedef struct vsip_cqrobject_f vsip_cqr_f;

typedef float vsip_scalar_f;

typedef struct {
  vsip_scalar_f r, i;
} vsip_cscalar_f;

typedef signed int vsip_scalar_i;

typedef struct {
  vsip_scalar_i r, i;
} vsip_cscalar_i;

typedef signed short int vsip_scalar_si;

typedef struct {
  vsip_scalar_si r, i;
} vsip_cscalar_si;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_cblock_f *block;
} vsip_cvattr_f;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_cblock_i *block;
} vsip_cvattr_i;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_cblock_si *block;
} vsip_cvattr_si;

struct vsip_cvviewobject_f;

typedef struct vsip_cvviewobject_f vsip_cvview_f;

struct vsip_cvviewobject_i;

typedef struct vsip_cvviewobject_i vsip_cvview_i;

struct vsip_cvviewobject_si;

typedef struct vsip_cvviewobject_si vsip_cvview_si;

typedef enum {
  VSIP_FFT_IP = 0,		/*  In-Place */
  VSIP_FFT_OP = 1		/*  Out-of-Place */
} vsip_fft_place;

typedef enum {
  VSIP_FFT_FWD = -1,		/*  Forward */
  VSIP_FFT_INV = 1		/*  Inverse (or reverse) */
} vsip_fft_dir;

typedef struct {
  vsip_scalar_mi input;
  vsip_scalar_mi output;
  vsip_fft_place place;		/*  In/Out -of-Place */
  vsip_scalar_f scale;		/*  Scale factor */
  vsip_fft_dir dir;		/*  Forward or Inverse */
} vsip_fft2d_attr_f;

struct vsip_fft2dobject_f;

typedef struct vsip_fft2dobject_f vsip_fft2d_f;

typedef struct {
  vsip_scalar_vi input;		/*  Input length */
  vsip_scalar_vi output;	/*  Output length */
  vsip_fft_place place;		/*  In/Out -of-Place */
  vsip_scalar_f scale;		/*  Scale factor */
  vsip_fft_dir dir;		/*  Forward or Inverse */
} vsip_fft_attr_f;

struct vsip_fftobject_f;

typedef struct vsip_fftobject_f vsip_fft_f;

typedef enum {
  VSIP_ROW = 0,			/*  Row, C style */
  VSIP_COL = 1			/*  Column, FORTRAN style */
} vsip_major;

typedef struct {
  vsip_scalar_mi input;		/*  Input size, M by N */
  vsip_scalar_mi output;	/*  Output size, P by Q */
  vsip_fft_place place;		/*  In/Out -of-Place */
  vsip_scalar_f scale;		/*  Scale factor */
  vsip_fft_dir dir;		/*  Forward or Inverse */
  vsip_major major;		/*  By Row/Col */
} vsip_fftm_attr_f;

struct vsip_fftmobject_f;

typedef struct vsip_fftmobject_f vsip_fftm_f;

typedef struct {
  vsip_scalar_vi kernel_len;	/*  Kernel length */
  vsip_symmetry symm;		/*  Kernel symmetry */
  vsip_scalar_vi in_len;	/*  Filter input segment length */
  vsip_scalar_vi out_len;	/*  Filter output segment length */
  vsip_length decimation;	/*  Decimation factor */
  vsip_obj_state state;		/*  Save state information */
} vsip_fir_attr_f;

struct vsip_firobject_f;

typedef struct vsip_firobject_f vsip_fir_f;

typedef enum {
  VSIP_HIST_RESET = 1,		/*  Histogram Reset */
  VSIP_HIST_ACCUM = 2		/*  Histogram Accumulate */
} vsip_hist_opt;

typedef vsip_scalar_vi vsip_index;

typedef struct {
  vsip_length n;		/*  Matrix size is N by N */
} vsip_lu_attr_f;

struct vsip_luobject_f;

typedef struct vsip_luobject_f vsip_lu_f;

typedef enum {
  VSIP_MAT_NTRANS = 0,		/*  No transformation */
  VSIP_MAT_TRANS = 1,		/*  Matrix Transpose */
  VSIP_MAT_HERM = 2,		/*  Matrix Hermitian (Conjugate Transpose) */
  VSIP_MAT_CONJ = 3		/*  Matrix Conjugate */
} vsip_mat_op;

typedef enum {
  VSIP_MAT_LSIDE = 0,		/*  Left side */
  VSIP_MAT_RSIDE = 1		/*  Right side */
} vsip_mat_side;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_block_bl *block;
} vsip_mattr_bl;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_block_f *block;
} vsip_mattr_f;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_block_i *block;
} vsip_mattr_i;

typedef struct {
  vsip_offset offset;
  vsip_stride row_stride;
  vsip_length row_length;
  vsip_stride col_stride;
  vsip_length col_length;
  vsip_block_si *block;
} vsip_mattr_si;

typedef enum {
  VSIP_MEM_NONE = 0,		/* No hint */
  VSIP_MEM_RDONLY = 1,		/* Read Only */
  VSIP_MEM_CONST = 2,		/* Constant */
  VSIP_MEM_SHARED = 3,		/* Shared */
  VSIP_MEM_SHARED_RDONLY = 4,	/* Shared, Read Only */
  VSIP_MEM_SHARED_CONST = 5	/* Shared, Constant */
} vsip_memory_hint;

struct vsip_mviewobject_bl;

typedef struct vsip_mviewobject_bl vsip_mview_bl;

struct vsip_mviewobject_f;

typedef struct vsip_mviewobject_f vsip_mview_f;

struct vsip_mviewobject_i;

typedef struct vsip_mviewobject_i vsip_mview_i;

struct vsip_mviewobject_si;

typedef struct vsip_mviewobject_si vsip_mview_si;

typedef struct {
  vsip_length m;		/*  Input matrix is M by N */
  vsip_length n;		/*  Input matrix is M by N */
  vsip_qrd_qopt Qopt;		/*  Matrix Q is saved/not saved */
} vsip_qr_attr_f;

struct vsip_qrobject_f;

typedef struct vsip_qrobject_f vsip_qr_f;

typedef enum {
  VSIP_COV = 0,			/*  Solve a covariance linear system problem */
  VSIP_LLS = 1			/*  Solve a linear least squares problem */
} vsip_qrd_prob;

struct vsip_randomstate;

typedef struct vsip_randomstate vsip_randstate;

typedef enum {
  VSIP_PRNG = 0,		/* Portable random number generator */
  VSIP_NPRNG = 1		/* Non-portable random number generator */
} vsip_rng;

typedef signed int vsip_scalar_bl;
typedef vsip_scalar_bl vsip_bool;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_bl *block;
} vsip_vattr_bl;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_f *block;
} vsip_vattr_f;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_i *block;
} vsip_vattr_i;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_mi *block;
} vsip_vattr_mi;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_si *block;
} vsip_vattr_si;

typedef struct {
  vsip_offset offset;
  vsip_stride stride;
  vsip_length length;
  vsip_block_vi *block;
} vsip_vattr_vi;

struct vsip_vviewobject_bl;

typedef struct vsip_vviewobject_bl vsip_vview_bl;

struct vsip_vviewobject_f;

typedef struct vsip_vviewobject_f vsip_vview_f;

struct vsip_vviewobject_i;

typedef struct vsip_vviewobject_i vsip_vview_i;

struct vsip_vviewobject_mi;

typedef struct vsip_vviewobject_mi vsip_vview_mi;

struct vsip_vviewobject_si;

typedef struct vsip_vviewobject_si vsip_vview_si;

struct vsip_vviewobject_vi;

typedef struct vsip_vviewobject_vi vsip_vview_vi;

void vsip_CADD_f(vsip_cscalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_CDIV_f(vsip_cscalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_CEXP_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CJMUL_f(vsip_cscalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_CLOG_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CMPLX_f(vsip_scalar_f a, vsip_scalar_f b, vsip_cscalar_f * r);

void vsip_CMUL_f(vsip_cscalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_CNEG_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CONJ_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CRDIV_f(vsip_cscalar_f x, vsip_scalar_f y, vsip_cscalar_f * z);

void vsip_CRECIP_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CRSUB_f(vsip_cscalar_f x, vsip_scalar_f y, vsip_cscalar_f * z);

void vsip_CSQRT_f(vsip_cscalar_f x, vsip_cscalar_f * y);

void vsip_CSUB_f(vsip_cscalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_MATINDEX(vsip_index r, vsip_index c, vsip_scalar_mi * mi);

void vsip_RCADD_f(vsip_scalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_RCMUL_f(vsip_scalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_RCSUB_f(vsip_scalar_f x, vsip_cscalar_f y, vsip_cscalar_f * z);

void vsip_RECT_f(vsip_scalar_f radius, vsip_scalar_f theta,
		 vsip_cscalar_f * r);

vsip_scalar_f vsip_acos_f(const vsip_scalar_f A);

vsip_scalar_f vsip_arg_f(vsip_cscalar_f x);

vsip_scalar_f vsip_asin_f(const vsip_scalar_f A);

vsip_scalar_f vsip_atan2_f(const vsip_scalar_f A, const vsip_scalar_f B);

vsip_scalar_f vsip_atan_f(const vsip_scalar_f A);

int vsip_blockadmit_bl(vsip_block_bl * block, vsip_scalar_bl update);

int vsip_blockadmit_f(vsip_block_f * block, vsip_scalar_bl update);

int vsip_blockadmit_i(vsip_block_i * block, vsip_scalar_bl update);

int vsip_blockadmit_mi(vsip_block_mi * block, vsip_scalar_bl update);

int vsip_blockadmit_si(vsip_block_si * block, vsip_scalar_bl update);

int vsip_blockadmit_vi(vsip_block_vi * block, vsip_scalar_bl update);

vsip_block_bl *vsip_blockbind_bl(vsip_scalar_bl * user_data,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_f *vsip_blockbind_f(vsip_scalar_f * user_data,
			       vsip_length num_items, vsip_memory_hint hint);

vsip_block_i *vsip_blockbind_i(vsip_scalar_i * user_data,
			       vsip_length num_items, vsip_memory_hint hint);

vsip_block_mi *vsip_blockbind_mi(vsip_scalar_vi * user_data,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_si *vsip_blockbind_si(vsip_scalar_si * user_data,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_vi *vsip_blockbind_vi(vsip_scalar_vi * user_data,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_bl *vsip_blockcreate_bl(vsip_length num_items,
				   vsip_memory_hint hint);

vsip_block_f *vsip_blockcreate_f(vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_i *vsip_blockcreate_i(vsip_length num_items,
				 vsip_memory_hint hint);

vsip_block_mi *vsip_blockcreate_mi(vsip_length num_items,
				   vsip_memory_hint hint);

vsip_block_si *vsip_blockcreate_si(vsip_length num_items,
				   vsip_memory_hint hint);

vsip_block_vi *vsip_blockcreate_vi(vsip_length num_items,
				   vsip_memory_hint hint);

void vsip_blockdestroy_bl(vsip_block_bl * block);

void vsip_blockdestroy_f(vsip_block_f * block);

void vsip_blockdestroy_i(vsip_block_i * block);

void vsip_blockdestroy_mi(vsip_block_mi * block);

void vsip_blockdestroy_si(vsip_block_si * block);

void vsip_blockdestroy_vi(vsip_block_vi * block);

vsip_scalar_bl *vsip_blockfind_bl(const vsip_block_bl * block);

vsip_scalar_f *vsip_blockfind_f(const vsip_block_f * block);

vsip_scalar_i *vsip_blockfind_i(const vsip_block_i * block);

vsip_scalar_vi *vsip_blockfind_mi(const vsip_block_mi * block);

vsip_scalar_si *vsip_blockfind_si(const vsip_block_si * block);

vsip_scalar_vi *vsip_blockfind_vi(const vsip_block_vi * block);

vsip_scalar_bl *vsip_blockrebind_bl(vsip_block_bl * block,
				    vsip_scalar_bl * new_data);

vsip_scalar_f *vsip_blockrebind_f(vsip_block_f * block,
				  vsip_scalar_f * new_data);

vsip_scalar_i *vsip_blockrebind_i(vsip_block_i * block,
				  vsip_scalar_i * new_data);

vsip_scalar_vi *vsip_blockrebind_mi(vsip_block_mi * block,
				    vsip_scalar_vi * new_data);

vsip_scalar_si *vsip_blockrebind_si(vsip_block_si * block,
				    vsip_scalar_si * new_data);

vsip_scalar_vi *vsip_blockrebind_vi(vsip_block_vi * block,
				    vsip_scalar_vi * new_data);

vsip_scalar_bl *vsip_blockrelease_bl(vsip_block_bl * block,
				     vsip_scalar_bl update);

vsip_scalar_f *vsip_blockrelease_f(vsip_block_f * block,
				   vsip_scalar_bl update);

vsip_scalar_i *vsip_blockrelease_i(vsip_block_i * block,
				   vsip_scalar_bl update);

vsip_scalar_vi *vsip_blockrelease_mi(vsip_block_mi * block,
				     vsip_scalar_bl update);

vsip_scalar_si *vsip_blockrelease_si(vsip_block_si * block,
				     vsip_scalar_bl update);

vsip_scalar_vi *vsip_blockrelease_vi(vsip_block_vi * block,
				     vsip_scalar_bl update);

vsip_cscalar_f vsip_cadd_f(vsip_cscalar_f x, vsip_cscalar_f y);

int vsip_cblockadmit_f(vsip_cblock_f * block, vsip_scalar_bl update);

int vsip_cblockadmit_i(vsip_cblock_i * block, vsip_scalar_bl update);

int vsip_cblockadmit_si(vsip_cblock_si * block, vsip_scalar_bl update);

vsip_cblock_f *vsip_cblockbind_f(vsip_scalar_f * user_data1,
				 vsip_scalar_f * user_data2,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_cblock_i *vsip_cblockbind_i(vsip_scalar_i * user_data1,
				 vsip_scalar_i * user_data2,
				 vsip_length num_items,
				 vsip_memory_hint hint);

vsip_cblock_si *vsip_cblockbind_si(vsip_scalar_si * user_data1,
				   vsip_scalar_si * user_data2,
				   vsip_length num_items,
				   vsip_memory_hint hint);

vsip_cblock_f *vsip_cblockcreate_f(vsip_length num_items,
				   vsip_memory_hint hint);

vsip_cblock_i *vsip_cblockcreate_i(vsip_length num_items,
				   vsip_memory_hint hint);

vsip_cblock_si *vsip_cblockcreate_si(vsip_length num_items,
				     vsip_memory_hint hint);

void vsip_cblockdestroy_f(vsip_cblock_f * block);

void vsip_cblockdestroy_i(vsip_cblock_i * block);

void vsip_cblockdestroy_si(vsip_cblock_si * block);

void vsip_cblockfind_f(const vsip_cblock_f * block,
		       vsip_scalar_f ** user_data1,
		       vsip_scalar_f ** user_data2);

void vsip_cblockfind_i(const vsip_cblock_i * block,
		       vsip_scalar_i ** user_data1,
		       vsip_scalar_i ** user_data2);

void vsip_cblockfind_si(const vsip_cblock_si * block,
			vsip_scalar_si ** user_data1,
			vsip_scalar_si ** user_data2);

void vsip_cblockrebind_f(vsip_cblock_f * block, vsip_scalar_f * new_data1,
			 vsip_scalar_f * new_data2,
			 vsip_scalar_f ** old_data1,
			 vsip_scalar_f ** old_data2);

void vsip_cblockrebind_i(vsip_cblock_i * block, vsip_scalar_i * new_data1,
			 vsip_scalar_i * new_data2,
			 vsip_scalar_i ** old_data1,
			 vsip_scalar_i ** old_data2);

void vsip_cblockrebind_si(vsip_cblock_si * block, vsip_scalar_si * new_data1,
			  vsip_scalar_si * new_data2,
			  vsip_scalar_si ** old_data1,
			  vsip_scalar_si ** old_data2);

void vsip_cblockrelease_f(vsip_cblock_f * block, vsip_scalar_bl update,
			  vsip_scalar_f ** user_data1,
			  vsip_scalar_f ** user_data2);

void vsip_cblockrelease_i(vsip_cblock_i * block, vsip_scalar_bl update,
			  vsip_scalar_i ** user_data1,
			  vsip_scalar_i ** user_data2);

void vsip_cblockrelease_si(vsip_cblock_si * block, vsip_scalar_bl update,
			   vsip_scalar_si ** user_data1,
			   vsip_scalar_si ** user_data2);

vsip_fft2d_f *vsip_ccfft2dip_create_f(const vsip_index rows,
				      const vsip_index cols,
				      const vsip_scalar_f scale,
				      const vsip_fft_dir dir,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

void vsip_ccfft2dip_f(const vsip_fft2d_f * plan, const vsip_cmview_f * XY);

vsip_fft2d_f *vsip_ccfft2dop_create_f(const vsip_index rows,
				      const vsip_index cols,
				      const vsip_scalar_f scale,
				      const vsip_fft_dir dir,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

void vsip_ccfft2dop_f(const vsip_fft2d_f * plan, const vsip_cmview_f * X,
		      const vsip_cmview_f * Y);

vsip_fft_f *vsip_ccfftip_create_f(const vsip_index length,
				  const vsip_scalar_f scale,
				  const vsip_fft_dir dir,
				  const vsip_length ntimes,
				  const vsip_alg_hint hint);

void vsip_ccfftip_f(const vsip_fft_f * plan, const vsip_cvview_f * xy);

vsip_fftm_f *vsip_ccfftmip_create_f(const vsip_index rows,
				    const vsip_index cols,
				    const vsip_scalar_f scale,
				    const vsip_fft_dir dir,
				    const vsip_major major,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

void vsip_ccfftmip_f(const vsip_fftm_f * plan, const vsip_cmview_f * XY);

vsip_fftm_f *vsip_ccfftmop_create_f(const vsip_index rows,
				    const vsip_index cols,
				    const vsip_scalar_f scale,
				    const vsip_fft_dir dir,
				    const vsip_major major,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

void vsip_ccfftmop_f(const vsip_fftm_f * plan, const vsip_cmview_f * X,
		     const vsip_cmview_f * Y);

vsip_fft_f *vsip_ccfftop_create_f(const vsip_index length,
				  const vsip_scalar_f scale,
				  const vsip_fft_dir dir,
				  const vsip_length ntimes,
				  const vsip_alg_hint hint);

void vsip_ccfftop_f(const vsip_fft_f * plan, const vsip_cvview_f * x,
		    const vsip_cvview_f * y);

vsip_cchol_f *vsip_cchold_create_f(const vsip_mat_uplo uplo,
				   const vsip_length N);

int vsip_cchold_destroy_f(vsip_cchol_f * chold);

int vsip_cchold_f(vsip_cchol_f * chold, const vsip_cmview_f * A);

void vsip_cchold_getattr_f(const vsip_cchol_f * chold,
			   vsip_cchol_attr_f * attr);

int vsip_ccholsol_f(const vsip_cchol_f * chold, const vsip_cmview_f * XB);

vsip_ccorr1d_f *vsip_ccorr1d_create_f(const vsip_length M,
				      const vsip_length N,
				      const vsip_support_region support,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

int vsip_ccorr1d_destroy_f(vsip_ccorr1d_f * plan);

void vsip_ccorr1d_getattr_f(const vsip_ccorr1d_f * plan,
			    vsip_ccorr1d_attr_f * attr);

vsip_ccorr2d_f *vsip_ccorr2d_create_f(vsip_length M, vsip_length N,
				      vsip_length P, vsip_length Q,
				      const vsip_support_region support,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

int vsip_ccorr2d_destroy_f(vsip_ccorr2d_f * plan);

void vsip_ccorr2d_getattr_f(const vsip_ccorr2d_f * plan,
			    vsip_ccorr2d_attr_f * attr);

void vsip_ccorrelate1d_f(const vsip_ccorr1d_f * plan, const vsip_bias bias,
			 const vsip_cvview_f * ref, const vsip_cvview_f * x,
			 const vsip_cvview_f * y);

void vsip_ccorrelate2d_f(const vsip_ccorr2d_f * plan, vsip_bias bias,
			 const vsip_cmview_f * ref, const vsip_cmview_f * x,
			 const vsip_cmview_f * y);

int vsip_ccovsol_f(const vsip_cmview_f * A, const vsip_cmview_f * XB);

vsip_cscalar_f vsip_cdiv_f(vsip_cscalar_f x, vsip_cscalar_f y);

vsip_scalar_f vsip_ceil_f(const vsip_scalar_f A);

vsip_cscalar_f vsip_cexp_f(vsip_cscalar_f x);

vsip_cfir_f *vsip_cfir_create_f(const vsip_cvview_f * kernel,
				const vsip_symmetry symm, const vsip_length N,
				const vsip_length D,
				const vsip_obj_state state,
				const vsip_length ntimes,
				const vsip_alg_hint hint);

int vsip_cfir_destroy_f(vsip_cfir_f * plan);

void vsip_cfir_getattr_f(const vsip_cfir_f * plan, vsip_cfir_attr_f * attr);

void vsip_cfir_reset_f(vsip_cfir_f * fir);

int vsip_cfirflt_f(vsip_cfir_f * plan, const vsip_cvview_f * x,
		   const vsip_cvview_f * y);

void vsip_cgemp_f(const vsip_cscalar_f alpha, const vsip_cmview_f * A,
		  const vsip_mat_op Aop, const vsip_cmview_f * B,
		  const vsip_mat_op Bop, const vsip_cscalar_f beta,
		  const vsip_cmview_f * R);

void vsip_cgems_f(const vsip_cscalar_f alpha, const vsip_cmview_f * A,
		  const vsip_mat_op Aop, const vsip_cscalar_f beta,
		  const vsip_cmview_f * C);

vsip_chol_f *vsip_chold_create_f(const vsip_mat_uplo uplo,
				 const vsip_length N);

int vsip_chold_destroy_f(vsip_chol_f * chold);

int vsip_chold_f(vsip_chol_f * chold, const vsip_mview_f * A);

void vsip_chold_getattr_f(const vsip_chol_f * chold, vsip_chol_attr_f * attr);

int vsip_cholsol_f(const vsip_chol_f * chold, const vsip_mview_f * XB);

vsip_cscalar_f vsip_cjmul_f(vsip_cscalar_f x, vsip_cscalar_f y);

int vsip_cllsqsol_f(const vsip_cmview_f * A, const vsip_cmview_f * XB);

vsip_cscalar_f vsip_clog_f(const vsip_cscalar_f A);

vsip_clu_f *vsip_clud_create_f(const vsip_length N);

int vsip_clud_destroy_f(vsip_clu_f * lud);

int vsip_clud_f(vsip_clu_f * lud, const vsip_cmview_f * A);

void vsip_clud_getattr_f(const vsip_clu_f * lud, vsip_clu_attr_f * attr);

int vsip_clusol_f(const vsip_clu_f * clud, const vsip_mat_op opA,
		  const vsip_cmview_f * XB);

void vsip_cmadd_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_cmview_f * R);

vsip_scalar_f vsip_cmag_f(vsip_cscalar_f x);

vsip_scalar_f vsip_cmagsq_f(vsip_cscalar_f x);

void vsip_cmalldestroy_f(vsip_cmview_f * matrix);

void vsip_cmalldestroy_i(vsip_cmview_i * matrix);

void vsip_cmalldestroy_si(vsip_cmview_si * matrix);

vsip_cmview_f *vsip_cmbind_f(vsip_cblock_f * block, vsip_offset offset,
			     vsip_stride col_stride, vsip_length col_length,
			     vsip_stride row_stride, vsip_length row_length);

vsip_cmview_i *vsip_cmbind_i(vsip_cblock_i * block, vsip_offset offset,
			     vsip_stride col_stride, vsip_length col_length,
			     vsip_stride row_stride, vsip_length row_length);

vsip_cmview_si *vsip_cmbind_si(vsip_cblock_si * block, vsip_offset offset,
			       vsip_stride col_stride, vsip_length col_length,
			       vsip_stride row_stride,
			       vsip_length row_length);

vsip_cmview_f *vsip_cmcloneview_f(const vsip_cmview_f * matrix);

vsip_cmview_i *vsip_cmcloneview_i(const vsip_cmview_i * matrix);

vsip_cmview_si *vsip_cmcloneview_si(const vsip_cmview_si * matrix);

vsip_cvview_f *vsip_cmcolview_f(const vsip_cmview_f * matrix, vsip_index j);

vsip_cvview_i *vsip_cmcolview_i(const vsip_cmview_i * matrix, vsip_index j);

vsip_cvview_si *vsip_cmcolview_si(const vsip_cmview_si * matrix,
				  vsip_index j);

void vsip_cmconj_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cmcopy_f_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

vsip_cmview_f *vsip_cmcreate_f(vsip_length col_length, vsip_length row_length,
			       vsip_major rc, vsip_memory_hint hint);

vsip_cmview_i *vsip_cmcreate_i(vsip_length col_length, vsip_length row_length,
			       vsip_major rc, vsip_memory_hint hint);

vsip_cmview_si *vsip_cmcreate_si(vsip_length col_length,
				 vsip_length row_length, vsip_major rc,
				 vsip_memory_hint hint);

void vsip_cmcumsum_f(const vsip_cmview_f * A, vsip_major dir,
		     const vsip_cmview_f * B);

void vsip_cmcumsum_i(const vsip_cmview_i * A, vsip_major dir,
		     const vsip_cmview_i * B);

vsip_cblock_f *vsip_cmdestroy_f(vsip_cmview_f * matrix);

vsip_cblock_i *vsip_cmdestroy_i(vsip_cmview_i * matrix);

vsip_cblock_si *vsip_cmdestroy_si(vsip_cmview_si * matrix);

vsip_cvview_f *vsip_cmdiagview_f(const vsip_cmview_f * matrix,
				 vsip_stride index);

vsip_cvview_i *vsip_cmdiagview_i(const vsip_cmview_i * matrix,
				 vsip_stride index);

vsip_cvview_si *vsip_cmdiagview_si(const vsip_cmview_si * matrix,
				   vsip_stride index);

void vsip_cmdiv_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_cmview_f * R);

void vsip_cmexp_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cmexpoavg_f(vsip_scalar_f x, const vsip_cmview_f * A,
		      const vsip_cmview_f * B);

void vsip_cmfill_f(const vsip_cscalar_f a, const vsip_cmview_f * R);

void vsip_cmgather_f(const vsip_cmview_f * A, const vsip_vview_mi * B,
		     const vsip_cvview_f * R);

vsip_cscalar_f vsip_cmget_f(const vsip_cmview_f * matrix, vsip_index i,
			    vsip_index j);

vsip_cscalar_i vsip_cmget_i(const vsip_cmview_i * matrix, vsip_index i,
			    vsip_index j);

vsip_cscalar_si vsip_cmget_si(const vsip_cmview_si * matrix, vsip_index i,
			      vsip_index j);

void vsip_cmgetattrib_f(const vsip_cmview_f * matrix, vsip_cmattr_f * attr);

void vsip_cmgetattrib_i(const vsip_cmview_i * matrix, vsip_cmattr_i * attr);

void vsip_cmgetattrib_si(const vsip_cmview_si * matrix,
			 vsip_cmattr_si * attr);

vsip_cblock_f *vsip_cmgetblock_f(const vsip_cmview_f * matrix);

vsip_cblock_i *vsip_cmgetblock_i(const vsip_cmview_i * matrix);

vsip_cblock_si *vsip_cmgetblock_si(const vsip_cmview_si * matrix);

vsip_length vsip_cmgetcollength_f(const vsip_cmview_f * matrix);

vsip_length vsip_cmgetcollength_i(const vsip_cmview_i * matrix);

vsip_length vsip_cmgetcollength_si(const vsip_cmview_si * matrix);

vsip_stride vsip_cmgetcolstride_f(const vsip_cmview_f * matrix);

vsip_stride vsip_cmgetcolstride_i(const vsip_cmview_i * matrix);

vsip_stride vsip_cmgetcolstride_si(const vsip_cmview_si * matrix);

vsip_offset vsip_cmgetoffset_f(const vsip_cmview_f * matrix);

vsip_offset vsip_cmgetoffset_i(const vsip_cmview_i * matrix);

vsip_offset vsip_cmgetoffset_si(const vsip_cmview_si * matrix);

vsip_length vsip_cmgetrowlength_f(const vsip_cmview_f * matrix);

vsip_length vsip_cmgetrowlength_i(const vsip_cmview_i * matrix);

vsip_length vsip_cmgetrowlength_si(const vsip_cmview_si * matrix);

vsip_stride vsip_cmgetrowstride_f(const vsip_cmview_f * matrix);

vsip_stride vsip_cmgetrowstride_i(const vsip_cmview_i * matrix);

vsip_stride vsip_cmgetrowstride_si(const vsip_cmview_si * matrix);

void vsip_cmherm_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cminvlu_f(const vsip_cmview_f * A, const vsip_vview_i * V,
		    const vsip_cmview_f * R);

void vsip_cminvqr_f(const vsip_cmview_f * A,const vsip_cmview_f * R);

void vsip_cmjmul_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

void vsip_cmleq_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_mview_bl * R);

void vsip_cmleq_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		  const vsip_mview_bl * R);

void vsip_cmlne_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_mview_bl * R);

void vsip_cmlne_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		  const vsip_mview_bl * R);

void vsip_cmlog_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cmmag_f(const vsip_cmview_f * A, const vsip_mview_f * R);

vsip_scalar_f vsip_cmmeansqval_f(const vsip_cmview_f * A);

vsip_cscalar_f vsip_cmmeanval_f(const vsip_cmview_f * A);

void vsip_cmmul_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_cmview_f * R);

void vsip_cmneg_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

vsip_cscalar_f vsip_cmplx_f(vsip_scalar_f re, vsip_scalar_f im);

void vsip_cmprod_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

void vsip_cmprod_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		   const vsip_cmview_i * R);

void vsip_cmprod_si(const vsip_cmview_si * A, const vsip_cmview_si * B,
		    const vsip_cmview_si * R);

void vsip_cmprodh_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		    const vsip_cmview_f * R);

void vsip_cmprodh_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		    const vsip_cmview_i * R);

void vsip_cmprodh_si(const vsip_cmview_si * A, const vsip_cmview_si * B,
		     const vsip_cmview_si * R);

void vsip_cmprodj_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		    const vsip_cmview_f * R);

void vsip_cmprodj_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		    const vsip_cmview_i * R);

void vsip_cmprodj_si(const vsip_cmview_si * A, const vsip_cmview_si * B,
		     const vsip_cmview_si * R);

void vsip_cmprodt_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		    const vsip_cmview_f * R);

void vsip_cmprodt_i(const vsip_cmview_i * A, const vsip_cmview_i * B,
		    const vsip_cmview_i * R);

void vsip_cmprodt_si(const vsip_cmview_si * A, const vsip_cmview_si * B,
		     const vsip_cmview_si * R);

void vsip_cmput_f(const vsip_cmview_f * matrix, vsip_index i, vsip_index j,
		  vsip_cscalar_f value);

void vsip_cmput_i(const vsip_cmview_i * matrix, vsip_index i, vsip_index j,
		  vsip_cscalar_i value);

void vsip_cmput_si(const vsip_cmview_si * matrix, vsip_index i, vsip_index j,
		   vsip_cscalar_si value);

vsip_cmview_f *vsip_cmputattrib_f(vsip_cmview_f * matrix,
				  const vsip_cmattr_f * attr);

vsip_cmview_i *vsip_cmputattrib_i(vsip_cmview_i * matrix,
				  const vsip_cmattr_i * attr);

vsip_cmview_si *vsip_cmputattrib_si(vsip_cmview_si * matrix,
				    const vsip_cmattr_si * attr);

vsip_cmview_f *vsip_cmputcollength_f(vsip_cmview_f * matrix, vsip_length n2);

vsip_cmview_i *vsip_cmputcollength_i(vsip_cmview_i * matrix, vsip_length n2);

vsip_cmview_si *vsip_cmputcollength_si(vsip_cmview_si * matrix,
				       vsip_length n2);

vsip_cmview_f *vsip_cmputcolstride_f(vsip_cmview_f * matrix, vsip_stride s2);

vsip_cmview_i *vsip_cmputcolstride_i(vsip_cmview_i * matrix, vsip_stride s2);

vsip_cmview_si *vsip_cmputcolstride_si(vsip_cmview_si * matrix,
				       vsip_stride s2);

vsip_cmview_f *vsip_cmputoffset_f(vsip_cmview_f * matrix, vsip_offset offset);

vsip_cmview_i *vsip_cmputoffset_i(vsip_cmview_i * matrix, vsip_offset offset);

vsip_cmview_si *vsip_cmputoffset_si(vsip_cmview_si * matrix,
				    vsip_offset offset);

vsip_cmview_f *vsip_cmputrowlength_f(vsip_cmview_f * matrix, vsip_length n1);

vsip_cmview_i *vsip_cmputrowlength_i(vsip_cmview_i * matrix, vsip_length n1);

vsip_cmview_si *vsip_cmputrowlength_si(vsip_cmview_si * matrix,
				       vsip_length n1);

vsip_cmview_f *vsip_cmputrowstride_f(vsip_cmview_f * matrix, vsip_stride s1);

vsip_cmview_i *vsip_cmputrowstride_i(vsip_cmview_i * matrix, vsip_stride s1);

vsip_cmview_si *vsip_cmputrowstride_si(vsip_cmview_si * matrix,
				       vsip_stride s1);

void vsip_cmrecip_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

vsip_cvview_f *vsip_cmrowview_f(const vsip_cmview_f * matrix, vsip_index i);

vsip_cvview_i *vsip_cmrowview_i(const vsip_cmview_i * matrix, vsip_index i);

vsip_cvview_si *vsip_cmrowview_si(const vsip_cmview_si * matrix,
				  vsip_index i);

void vsip_cmrsdiv_f(const vsip_cmview_f * A, vsip_scalar_f x,
		    const vsip_cmview_f * R);

void vsip_cmscatter_f(const vsip_cvview_f * A, const vsip_cmview_f * R,
		      const vsip_vview_mi * B);

void vsip_cmsqrt_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cmsub_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		  const vsip_cmview_f * R);

vsip_cmview_f *vsip_cmsubview_f(const vsip_cmview_f * matrix, vsip_index i,
				vsip_index j, vsip_length m, vsip_length n);

vsip_cmview_i *vsip_cmsubview_i(const vsip_cmview_i * matrix, vsip_index i,
				vsip_index j, vsip_length m, vsip_length n);

vsip_cmview_si *vsip_cmsubview_si(const vsip_cmview_si * matrix, vsip_index i,
				  vsip_index j, vsip_length m, vsip_length n);

vsip_cscalar_f vsip_cmsumval_f(const vsip_cmview_f * A);

vsip_cscalar_i vsip_cmsumval_i(const vsip_cmview_i * A);

void vsip_cmswap_f(const vsip_cmview_f * A, const vsip_cmview_f * B);

void vsip_cmtrans_f(const vsip_cmview_f * A, const vsip_cmview_f * R);

void vsip_cmtrans_i(const vsip_cmview_i * A, const vsip_cmview_i * R);

void vsip_cmtrans_si(const vsip_cmview_si * A, const vsip_cmview_si * R);

vsip_cmview_f *vsip_cmtransview_f(const vsip_cmview_f * matrix);

vsip_cmview_i *vsip_cmtransview_i(const vsip_cmview_i * matrix);

vsip_cmview_si *vsip_cmtransview_si(const vsip_cmview_si * matrix);

vsip_cscalar_f vsip_cmul_f(vsip_cscalar_f x, vsip_cscalar_f y);

void vsip_cmvprod_f(const vsip_cmview_f * A, const vsip_cvview_f * X,
		    const vsip_cvview_f * Y);

void vsip_cmvprod_i(const vsip_cmview_i * A, const vsip_cvview_i * X,
		    const vsip_cvview_i * Y);

void vsip_cmvprod_si(const vsip_cmview_si * A, const vsip_cvview_si * X,
		     const vsip_cvview_si * Y);

vsip_cscalar_f vsip_cneg_f(vsip_cscalar_f x);

void vsip_complete();

vsip_cscalar_f vsip_conj_f(vsip_cscalar_f x);

vsip_conv1d_f *vsip_conv1d_create_f(const vsip_vview_f * kernel,
				    const vsip_symmetry symm,
				    const vsip_length N, const vsip_length D,
				    const vsip_support_region support,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

int vsip_conv1d_destroy_f(vsip_conv1d_f * plan);

void vsip_conv1d_getattr_f(const vsip_conv1d_f * plan,
			   vsip_conv1d_attr_f * attr);

vsip_conv2d_f *vsip_conv2d_create_f(const vsip_mview_f * kernel,
				    const vsip_symmetry symm,
				    const vsip_length P, const vsip_length Q,
				    const vsip_length decimate,
				    const vsip_support_region support,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

int vsip_conv2d_destroy_f(vsip_conv2d_f * plan);

void vsip_conv2d_getattr_f(const vsip_conv2d_f * plan,
			   vsip_conv2d_attr_f * attr);

void vsip_convolve1d_f(const vsip_conv1d_f * plan, const vsip_vview_f * x,
		       const vsip_vview_f * y);

void vsip_convolve2d_f(const vsip_conv2d_f * plan, const vsip_mview_f * x,
		       const vsip_mview_f * y);

vsip_corr1d_f *vsip_corr1d_create_f(const vsip_length M, const vsip_length N,
				    const vsip_support_region support,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

int vsip_corr1d_destroy_f(vsip_corr1d_f * plan);

void vsip_corr1d_getattr_f(const vsip_corr1d_f * plan,
			   vsip_corr1d_attr_f * attr);

vsip_corr2d_f *vsip_corr2d_create_f(vsip_length M, vsip_length N,
				    vsip_length P, vsip_length Q,
				    const vsip_support_region support,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

int vsip_corr2d_destroy_f(vsip_corr2d_f * plan);

void vsip_corr2d_getattr_f(const vsip_corr2d_f * plan,
			   vsip_corr2d_attr_f * attr);

void vsip_correlate1d_f(const vsip_corr1d_f * plan, const vsip_bias bias,
			const vsip_vview_f * ref, const vsip_vview_f * x,
			const vsip_vview_f * y);

void vsip_correlate2d_f(const vsip_corr2d_f * plan, vsip_bias bias,
			const vsip_mview_f * ref, const vsip_mview_f * x,
			const vsip_mview_f * y);

vsip_scalar_f vsip_cos_f(const vsip_scalar_f A);

vsip_scalar_f vsip_cosh_f(const vsip_scalar_f A);

int vsip_covsol_f(const vsip_mview_f * A, const vsip_mview_f * XB);

vsip_cqr_f *vsip_cqrd_create_f(const vsip_length M, const vsip_length N,
			       const vsip_qrd_qopt qopt);

int vsip_cqrd_destroy_f(vsip_cqr_f * qrd);

int vsip_cqrd_f(vsip_cqr_f * qrd, const vsip_cmview_f * A);

void vsip_cqrd_getattr_f(const vsip_cqr_f * qrd, vsip_cqr_attr_f * attr);

int vsip_cqrdprodq_f(const vsip_cqr_f * qrd, const vsip_mat_op opQ,
		     const vsip_mat_side apQ, const vsip_cmview_f * C);

int vsip_cqrdsolr_f(const vsip_cqr_f * qrd, const vsip_mat_op OpR,
		    const vsip_cscalar_f alpha, const vsip_cmview_f * XB);

int vsip_cqrsol_f(const vsip_cqr_f * qrd, const vsip_qrd_prob prob,
		  const vsip_cmview_f * XB);

vsip_cscalar_f vsip_crandn_f(vsip_randstate * state);

vsip_cscalar_f vsip_crandu_f(vsip_randstate * state);

vsip_cscalar_f vsip_crdiv_f(vsip_cscalar_f x, vsip_scalar_f y);

vsip_cscalar_f vsip_crecip_f(vsip_cscalar_f x);

vsip_fft2d_f *vsip_crfft2dop_create_f(const vsip_index rows,
				      const vsip_index cols,
				      const vsip_scalar_f scale,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

void vsip_crfft2dop_f(const vsip_fft2d_f * plan, const vsip_cmview_f * X,
		      const vsip_mview_f * Y);

vsip_fftm_f *vsip_crfftmop_create_f(const vsip_index rows,
				    const vsip_index cols,
				    const vsip_scalar_f scale,
				    const vsip_major major,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

void vsip_crfftmop_f(const vsip_fftm_f * plan, const vsip_cmview_f * X,
		     const vsip_mview_f * Y);

vsip_fft_f *vsip_crfftop_create_f(const vsip_index length,
				  const vsip_scalar_f scale,
				  const vsip_length ntimes,
				  const vsip_alg_hint hint);

void vsip_crfftop_f(const vsip_fft_f * plan, const vsip_cvview_f * x,
		    const vsip_vview_f * y);

void vsip_crmdiv_f(const vsip_cmview_f * A, const vsip_mview_f * B,
		   const vsip_cmview_f * R);

void vsip_crmsub_f(const vsip_cmview_f * A, const vsip_mview_f * B,
		   const vsip_cmview_f * R);

vsip_cscalar_f vsip_crsub_f(vsip_cscalar_f x, vsip_scalar_f y);

void vsip_crvdiv_f(const vsip_cvview_f * A, const vsip_vview_f * B,
		   const vsip_cvview_f * R);

void vsip_crvsub_f(const vsip_cvview_f * A, const vsip_vview_f * B,
		   const vsip_cvview_f * R);

void vsip_csmadd_f(vsip_cscalar_f x, const vsip_cmview_f * A,
		   const vsip_cmview_f * R);

void vsip_csmdiv_f(const vsip_cscalar_f alpha, const vsip_cmview_f * A,
		   const vsip_cmview_f * R);

void vsip_csmmul_f(const vsip_cscalar_f alpha, const vsip_cmview_f * A,
		   const vsip_cmview_f * R);

void vsip_csmsub_f(vsip_cscalar_f x, const vsip_cmview_f * A,
		   const vsip_cmview_f * R);

vsip_cscalar_f vsip_csqrt_f(vsip_cscalar_f x);

vsip_cmplx_mem vsip_cstorage();

vsip_cscalar_f vsip_csub_f(vsip_cscalar_f x, vsip_cscalar_f y);

void vsip_csvadd_f(const vsip_cscalar_f a, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_csvdiv_f(const vsip_cscalar_f alpha, const vsip_cvview_f * A,
		   const vsip_cvview_f * R);

void vsip_csvmul_f(const vsip_cscalar_f a, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_csvsub_f(const vsip_cscalar_f a, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

int vsip_ctoepsol_f(const vsip_cvview_f * T, const vsip_cvview_f * B,
		    const vsip_cvview_f * W, const vsip_cvview_f * X);

void vsip_cvadd_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * R);

void vsip_cvadd_i(const vsip_cvview_i * A, const vsip_cvview_i * B,
		  const vsip_cvview_i * R);

void vsip_cvalldestroy_f(vsip_cvview_f * vektor);

void vsip_cvalldestroy_i(vsip_cvview_i * vektor);

void vsip_cvalldestroy_si(vsip_cvview_si * vektor);

void vsip_cvam_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		 const vsip_cvview_f * C, const vsip_cvview_f * R);

vsip_cvview_f *vsip_cvbind_f(vsip_cblock_f * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

vsip_cvview_i *vsip_cvbind_i(vsip_cblock_i * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

vsip_cvview_si *vsip_cvbind_si(vsip_cblock_si * block, vsip_offset offset,
			       vsip_stride stride, vsip_length length);

vsip_cvview_f *vsip_cvcloneview_f(const vsip_cvview_f * vektor);

vsip_cvview_i *vsip_cvcloneview_i(const vsip_cvview_i * vektor);

vsip_cvview_si *vsip_cvcloneview_si(const vsip_cvview_si * vektor);

void vsip_cvconj_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvcopy_f_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

vsip_cvview_f *vsip_cvcreate_f(vsip_length length, vsip_memory_hint hint);

vsip_cvview_i *vsip_cvcreate_i(vsip_length length, vsip_memory_hint hint);

vsip_cvview_si *vsip_cvcreate_si(vsip_length length, vsip_memory_hint hint);

void vsip_cvcumsum_f(const vsip_cvview_f * A, const vsip_cvview_f * B);

void vsip_cvcumsum_i(const vsip_cvview_i * A, const vsip_cvview_i * B);

vsip_cblock_f *vsip_cvdestroy_f(vsip_cvview_f * vektor);

vsip_cblock_i *vsip_cvdestroy_i(vsip_cvview_i * vektor);

vsip_cblock_si *vsip_cvdestroy_si(vsip_cvview_si * vektor);

void vsip_cvdiv_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * R);

vsip_cscalar_f vsip_cvdot_f(const vsip_cvview_f * A, const vsip_cvview_f * B);

void vsip_cvexp_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvexpoavg_f(const vsip_scalar_f a, const vsip_cvview_f * B,
		      const vsip_cvview_f * C);

void vsip_cvfill_f(const vsip_cscalar_f a, const vsip_cvview_f * R);

void vsip_cvgather_f(const vsip_cvview_f * X, const vsip_vview_vi * I,
		     const vsip_cvview_f * Y);

void vsip_cvgatherbl_f(const vsip_cvview_f * X, const vsip_vview_bl * I, 
                       const vsip_cvview_f * Y);

vsip_cscalar_f vsip_cvget_f(const vsip_cvview_f * vektor, vsip_index j);

vsip_cscalar_i vsip_cvget_i(const vsip_cvview_i * vektor, vsip_index j);

vsip_cscalar_si vsip_cvget_si(const vsip_cvview_si * vektor, vsip_index j);

void vsip_cvgetattrib_f(const vsip_cvview_f * vektor, vsip_cvattr_f * attr);

void vsip_cvgetattrib_i(const vsip_cvview_i * vektor, vsip_cvattr_i * attr);

void vsip_cvgetattrib_si(const vsip_cvview_si * vektor,
			 vsip_cvattr_si * attr);

vsip_cblock_f *vsip_cvgetblock_f(const vsip_cvview_f * vektor);

vsip_cblock_i *vsip_cvgetblock_i(const vsip_cvview_i * vektor);

vsip_cblock_si *vsip_cvgetblock_si(const vsip_cvview_si * vektor);

vsip_length vsip_cvgetlength_f(const vsip_cvview_f * vektor);

vsip_length vsip_cvgetlength_i(const vsip_cvview_i * vektor);

vsip_length vsip_cvgetlength_si(const vsip_cvview_si * vektor);

vsip_offset vsip_cvgetoffset_f(const vsip_cvview_f * vektor);

vsip_offset vsip_cvgetoffset_i(const vsip_cvview_i * vektor);

vsip_offset vsip_cvgetoffset_si(const vsip_cvview_si * vektor);

vsip_stride vsip_cvgetstride_f(const vsip_cvview_f * vektor);

vsip_stride vsip_cvgetstride_i(const vsip_cvview_i * vektor);

vsip_stride vsip_cvgetstride_si(const vsip_cvview_si * vektor);

vsip_cscalar_f vsip_cvjdot_f(const vsip_cvview_f * A,
			     const vsip_cvview_f * B);

void vsip_cvjmul_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_cvleq_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_vview_bl * R);

void vsip_cvleq_i(const vsip_cvview_i * A, const vsip_cvview_i * B,
		  const vsip_vview_bl * R);
		  
void vsip_cvlmerg_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
                    const vsip_vview_bl * C, const vsip_cvview_f * R);

void vsip_cvlne_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_vview_bl * R);

void vsip_cvlne_i(const vsip_cvview_i * A, const vsip_cvview_i * B,
		  const vsip_vview_bl * R);

void vsip_cvlog_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvma_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		 const vsip_cvview_f * C, const vsip_cvview_f * R);

void vsip_cvmag_f(const vsip_cvview_f * A, const vsip_vview_f * R);

vsip_scalar_f vsip_cvmeansqval_f(const vsip_cvview_f * A);

vsip_cscalar_f vsip_cvmeanval_f(const vsip_cvview_f * A);

void vsip_cvmmul_f(const vsip_cvview_f * A, const vsip_cmview_f * B,
		   const vsip_major major, const vsip_cmview_f * R);

vsip_scalar_f vsip_cvmodulate_f(const vsip_cvview_f * A,
				const vsip_scalar_f nu,
				const vsip_scalar_f phi,
				const vsip_cvview_f * R);

void vsip_cvmprod_f(const vsip_cvview_f * X, const vsip_cmview_f * A,
		    const vsip_cvview_f * Y);

void vsip_cvmprod_i(const vsip_cvview_i * X, const vsip_cmview_i * A,
		    const vsip_cvview_i * Y);

void vsip_cvmprod_si(const vsip_cvview_si * X, const vsip_cmview_si * A,
		     const vsip_cvview_si * Y);

void vsip_cvmsa_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cscalar_f c, const vsip_cvview_f * R);

void vsip_cvmsb_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * C, const vsip_cvview_f * R);

void vsip_cvmul_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * R);

void vsip_cvneg_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvouter_f(const vsip_cscalar_f alpha, const vsip_cvview_f * X,
		    const vsip_cvview_f * Y, const vsip_cmview_f * R);

void vsip_cvput_f(vsip_cvview_f * vektor, vsip_index j, vsip_cscalar_f value);

void vsip_cvput_i(vsip_cvview_i * vektor, vsip_index j, vsip_cscalar_i value);

void vsip_cvput_si(vsip_cvview_si * vektor, vsip_index j,
		   vsip_cscalar_si value);

vsip_cvview_f *vsip_cvputattrib_f(vsip_cvview_f * vektor,
				  const vsip_cvattr_f * attr);

vsip_cvview_i *vsip_cvputattrib_i(vsip_cvview_i * vektor,
				  const vsip_cvattr_i * attr);

vsip_cvview_si *vsip_cvputattrib_si(vsip_cvview_si * vektor,
				    const vsip_cvattr_si * attr);

vsip_cvview_f *vsip_cvputlength_f(vsip_cvview_f * vektor, vsip_length length);

vsip_cvview_i *vsip_cvputlength_i(vsip_cvview_i * vektor, vsip_length length);

vsip_cvview_si *vsip_cvputlength_si(vsip_cvview_si * vektor,
				    vsip_length length);

vsip_cvview_f *vsip_cvputoffset_f(vsip_cvview_f * vektor, vsip_offset offset);

vsip_cvview_i *vsip_cvputoffset_i(vsip_cvview_i * vektor, vsip_offset offset);

vsip_cvview_si *vsip_cvputoffset_si(vsip_cvview_si * vektor,
				    vsip_offset offset);

vsip_cvview_f *vsip_cvputstride_f(vsip_cvview_f * vektor, vsip_stride stride);

vsip_cvview_i *vsip_cvputstride_i(vsip_cvview_i * vektor, vsip_stride stride);

vsip_cvview_si *vsip_cvputstride_si(vsip_cvview_si * vektor,
				    vsip_stride stride);

void vsip_cvrandn_f(vsip_randstate * state, const vsip_cvview_f * R);

void vsip_cvrandu_f(vsip_randstate * state, const vsip_cvview_f * R);

void vsip_cvrecip_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvrsdiv_f(const vsip_cvview_f * A, const vsip_scalar_f b,
		    const vsip_cvview_f * R);

void vsip_cvsam_f(const vsip_cvview_f * A, const vsip_cscalar_f b,
		  const vsip_cvview_f * C, const vsip_cvview_f * R);

void vsip_cvsbm_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * C, const vsip_cvview_f * R);

void vsip_cvscatter_f(const vsip_cvview_f * X, const vsip_cvview_f * Y,
		      const vsip_vview_vi * I);

void vsip_cvsma_f(const vsip_cvview_f * A, const vsip_cscalar_f b,
		  const vsip_cvview_f * C, const vsip_cvview_f * R);

void vsip_cvsmsa_f(const vsip_cvview_f * A, const vsip_cscalar_f b,
		   const vsip_cscalar_f c, const vsip_cvview_f * R);

void vsip_cvsqrt_f(const vsip_cvview_f * A, const vsip_cvview_f * R);

void vsip_cvsub_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		  const vsip_cvview_f * R);

vsip_cvview_f *vsip_cvsubview_f(const vsip_cvview_f * vektor,
				vsip_index index, vsip_length length);

vsip_cvview_i *vsip_cvsubview_i(const vsip_cvview_i * vektor,
				vsip_index index, vsip_length length);

vsip_cvview_si *vsip_cvsubview_si(const vsip_cvview_si * vektor,
				  vsip_index index, vsip_length length);

vsip_cscalar_f vsip_cvsumval_f(const vsip_cvview_f * A);

vsip_cscalar_i vsip_cvsumval_i(const vsip_cvview_i * A);

void vsip_cvswap_f(const vsip_cvview_f * A, const vsip_cvview_f * B);

vsip_scalar_f vsip_exp10_f(const vsip_scalar_f A);

vsip_scalar_f vsip_exp_f(const vsip_scalar_f A);

int vsip_fft2d_destroy_f(vsip_fft2d_f * plan);

void vsip_fft2d_getattr_f(const vsip_fft2d_f * plan,
			  vsip_fft2d_attr_f * attr);

int vsip_fft_destroy_f(vsip_fft_f * plan);

void vsip_fft_getattr_f(const vsip_fft_f * plan, vsip_fft_attr_f * attr);

int vsip_fftm_destroy_f(vsip_fftm_f * plan);

void vsip_fftm_getattr_f(const vsip_fftm_f * plan, vsip_fftm_attr_f * attr);

int vsip_finalize(void *ptr);

vsip_fir_f *vsip_fir_create_f(const vsip_vview_f * kernel,
			      const vsip_symmetry symm, const vsip_length N,
			      const vsip_length D, const vsip_obj_state state,
			      const vsip_length ntimes,
			      const vsip_alg_hint hint);

int vsip_fir_destroy_f(vsip_fir_f * plan);

void vsip_fir_getattr_f(const vsip_fir_f * plan, vsip_fir_attr_f * attr);

void vsip_fir_reset_f(vsip_fir_f * fir);

int vsip_firflt_f(vsip_fir_f * plan, const vsip_vview_f * x,
		  const vsip_vview_f * y);

vsip_scalar_f vsip_floor_f(const vsip_scalar_f A);

vsip_scalar_f vsip_fmod_f(const vsip_scalar_f A, const vsip_scalar_f B);

void vsip_gemp_f(const vsip_scalar_f alpha, const vsip_mview_f * A,
		 const vsip_mat_op Aop, const vsip_mview_f * B,
		 const vsip_mat_op Bop, const vsip_scalar_f beta,
		 const vsip_mview_f * R);

void vsip_gems_f(const vsip_scalar_f alpha, const vsip_mview_f * A,
		 const vsip_mat_op Aop, const vsip_scalar_f beta,
		 const vsip_mview_f * C);

vsip_scalar_f vsip_hypot_f(const vsip_scalar_f A, const vsip_scalar_f B);

vsip_scalar_f vsip_imag_f(vsip_cscalar_f x);

int vsip_init(void *ptr);

int vsip_llsqsol_f(const vsip_mview_f * A, const vsip_mview_f * XB);

vsip_scalar_f vsip_log10_f(const vsip_scalar_f A);

vsip_scalar_f vsip_log_f(const vsip_scalar_f A);

vsip_lu_f *vsip_lud_create_f(const vsip_length N);

int vsip_lud_destroy_f(vsip_lu_f * lud);

int vsip_lud_f(vsip_lu_f * lud, const vsip_mview_f * A);

void vsip_lud_getattr_f(const vsip_lu_f * lud, vsip_lu_attr_f * attr);

int vsip_lusol_f(const vsip_lu_f * lud, const vsip_mat_op opA,
		 const vsip_mview_f * XB);

void vsip_macos_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_madd_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_madd_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_i * R);

vsip_scalar_f vsip_mag_f(const vsip_scalar_f A);

void vsip_malldestroy_bl(vsip_mview_bl * matrix);

void vsip_malldestroy_f(vsip_mview_f * matrix);

void vsip_malldestroy_i(vsip_mview_i * matrix);

void vsip_malldestroy_si(vsip_mview_si * matrix);

vsip_scalar_bl vsip_malltrue_bl(const vsip_mview_bl * A);

void vsip_mand_bl(const vsip_mview_bl * A, const vsip_mview_bl * B,
		  const vsip_mview_bl * R);

void vsip_mand_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_i * R);

void vsip_mand_si(const vsip_mview_si * A, const vsip_mview_si * B,
		  const vsip_mview_si * R);

vsip_scalar_bl vsip_manytrue_bl(const vsip_mview_bl * A);

void vsip_marg_f(const vsip_cmview_f * A, const vsip_mview_f * R);

void vsip_masin_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_matan2_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * R);

void vsip_matan_f(const vsip_mview_f * A, const vsip_mview_f * R);

vsip_scalar_mi vsip_matindex(vsip_index r, vsip_index c);

vsip_scalar_f vsip_max_f(const vsip_scalar_f A, const vsip_scalar_f B);

vsip_scalar_i vsip_max_i(const vsip_scalar_i A, const vsip_scalar_i B);

vsip_mview_bl *vsip_mbind_bl(vsip_block_bl * block, vsip_offset offset,
			     vsip_stride col_stride, vsip_length col_length,
			     vsip_stride row_stride, vsip_length row_length);

vsip_mview_f *vsip_mbind_f(vsip_block_f * block, vsip_offset offset,
			   vsip_stride col_stride, vsip_length col_length,
			   vsip_stride row_stride, vsip_length row_length);

vsip_mview_i *vsip_mbind_i(vsip_block_i * block, vsip_offset offset,
			   vsip_stride col_stride, vsip_length col_length,
			   vsip_stride row_stride, vsip_length row_length);

vsip_mview_si *vsip_mbind_si(vsip_block_si * block, vsip_offset offset,
			     vsip_stride col_stride, vsip_length col_length,
			     vsip_stride row_stride, vsip_length row_length);

void vsip_mclip_f(const vsip_mview_f * A, const vsip_scalar_f t1,
		  const vsip_scalar_f t2, const vsip_scalar_f c1,
		  const vsip_scalar_f c2, const vsip_mview_f * R);

vsip_mview_bl *vsip_mcloneview_bl(const vsip_mview_bl * matrix);

vsip_mview_f *vsip_mcloneview_f(const vsip_mview_f * matrix);

vsip_mview_i *vsip_mcloneview_i(const vsip_mview_i * matrix);

vsip_mview_si *vsip_mcloneview_si(const vsip_mview_si * matrix);

void vsip_mcmagsq_f(const vsip_cmview_f * A, const vsip_mview_f * R);

void vsip_mcmaxmgsq_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		      const vsip_mview_f * R);

vsip_scalar_f vsip_mcmaxmgsqval_f(const vsip_cmview_f * A,
				  vsip_scalar_mi * index);

void vsip_mcminmgsq_f(const vsip_cmview_f * A, const vsip_cmview_f * B,
		      const vsip_mview_f * R);

vsip_scalar_f vsip_mcminmgsqval_f(const vsip_cmview_f * A,
				  vsip_scalar_mi * index);

void vsip_mcmplx_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_cmview_f * R);

vsip_index vsip_mcolindex(vsip_scalar_mi mi);

vsip_vview_bl *vsip_mcolview_bl(const vsip_mview_bl * matrix, vsip_index j);

vsip_vview_f *vsip_mcolview_f(const vsip_mview_f * matrix, vsip_index j);

vsip_vview_i *vsip_mcolview_i(const vsip_mview_i * matrix, vsip_index j);

vsip_vview_si *vsip_mcolview_si(const vsip_mview_si * matrix, vsip_index j);

void vsip_mcopy_bl_bl(const vsip_mview_bl * A, const vsip_mview_bl * R);

void vsip_mcopy_bl_f(const vsip_mview_bl * A, const vsip_mview_f * R);

void vsip_mcopy_f_bl(const vsip_mview_f * A, const vsip_mview_bl * R);

void vsip_mcopy_f_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mcopy_f_i(const vsip_mview_f * A, const vsip_mview_i * R);

void vsip_mcopy_f_si(const vsip_mview_f * A, const vsip_mview_si * R);

void vsip_mcopy_i_f(const vsip_mview_i * A, const vsip_mview_f * R);

void vsip_mcopy_i_i(const vsip_mview_i * A, const vsip_mview_i * R);

void vsip_mcopy_i_si(const vsip_mview_i * A, const vsip_mview_si * R);

void vsip_mcopy_si_f(const vsip_mview_si * A, const vsip_mview_f * R);

void vsip_mcopy_si_i(const vsip_mview_si * A, const vsip_mview_i * R);

void vsip_mcopy_si_si(const vsip_mview_si * A, const vsip_mview_si * R);

void vsip_mcos_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mcosh_f(const vsip_mview_f * A, const vsip_mview_f * R);

vsip_mview_bl *vsip_mcreate_bl(vsip_length col_length, vsip_length row_length,
			       vsip_major rc, vsip_memory_hint hint);

vsip_mview_f *vsip_mcreate_f(vsip_length col_length, vsip_length row_length,
			     vsip_major rc, vsip_memory_hint hint);

vsip_mview_i *vsip_mcreate_i(vsip_length col_length, vsip_length row_length,
			     vsip_major rc, vsip_memory_hint hint);

vsip_mview_si *vsip_mcreate_si(vsip_length col_length, vsip_length row_length,
			       vsip_major rc, vsip_memory_hint hint);

void vsip_mcumsum_f(const vsip_mview_f * A, vsip_major dir,
		    const vsip_mview_f * B);

void vsip_mcumsum_i(const vsip_mview_i * A, vsip_major dir,
		    const vsip_mview_i * B);

vsip_block_bl *vsip_mdestroy_bl(vsip_mview_bl * matrix);

vsip_block_f *vsip_mdestroy_f(vsip_mview_f * matrix);

vsip_block_i *vsip_mdestroy_i(vsip_mview_i * matrix);

vsip_block_si *vsip_mdestroy_si(vsip_mview_si * matrix);

vsip_vview_bl *vsip_mdiagview_bl(const vsip_mview_bl * matrix,
				 vsip_stride index);

vsip_vview_f *vsip_mdiagview_f(const vsip_mview_f * matrix,
			       vsip_stride index);

vsip_vview_i *vsip_mdiagview_i(const vsip_mview_i * matrix,
			       vsip_stride index);

vsip_vview_si *vsip_mdiagview_si(const vsip_mview_si * matrix,
				 vsip_stride index);

void vsip_mdiv_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_meuler_f(const vsip_mview_f * A, const vsip_cmview_f * R);

void vsip_mexp10_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mexp_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mexpoavg_f(vsip_scalar_f x, const vsip_mview_f * A,
		     const vsip_mview_f * B);

void vsip_mfill_f(const vsip_scalar_f a, const vsip_mview_f * R);

void vsip_mfill_i(const vsip_scalar_i a, const vsip_mview_i * R);

void vsip_mfill_si(const vsip_scalar_si a, const vsip_mview_si * R);

void vsip_mgather_f(const vsip_mview_f * A, const vsip_vview_mi * B,
		    const vsip_vview_f * R);

void vsip_mgather_i(const vsip_mview_i * A, const vsip_vview_mi * B,
		    const vsip_vview_i * R);

void vsip_mgather_si(const vsip_mview_si * A, const vsip_vview_mi * B,
		     const vsip_vview_si * R);

vsip_scalar_bl vsip_mget_bl(const vsip_mview_bl * matrix, vsip_index i,
			    vsip_index j);

vsip_scalar_f vsip_mget_f(const vsip_mview_f * matrix, vsip_index i,
			  vsip_index j);

vsip_scalar_i vsip_mget_i(const vsip_mview_i * matrix, vsip_index i,
			  vsip_index j);

vsip_scalar_si vsip_mget_si(const vsip_mview_si * matrix, vsip_index i,
			    vsip_index j);

void vsip_mgetattrib_bl(const vsip_mview_bl * matrix, vsip_mattr_bl * attr);

void vsip_mgetattrib_f(const vsip_mview_f * matrix, vsip_mattr_f * attr);

void vsip_mgetattrib_i(const vsip_mview_i * matrix, vsip_mattr_i * attr);

void vsip_mgetattrib_si(const vsip_mview_si * matrix, vsip_mattr_si * attr);

vsip_block_bl *vsip_mgetblock_bl(const vsip_mview_bl * matrix);

vsip_block_f *vsip_mgetblock_f(const vsip_mview_f * matrix);

vsip_block_i *vsip_mgetblock_i(const vsip_mview_i * matrix);

vsip_block_si *vsip_mgetblock_si(const vsip_mview_si * matrix);

vsip_length vsip_mgetcollength_bl(const vsip_mview_bl * matrix);

vsip_length vsip_mgetcollength_f(const vsip_mview_f * matrix);

vsip_length vsip_mgetcollength_i(const vsip_mview_i * matrix);

vsip_length vsip_mgetcollength_si(const vsip_mview_si * matrix);

vsip_stride vsip_mgetcolstride_bl(const vsip_mview_bl * matrix);

vsip_stride vsip_mgetcolstride_f(const vsip_mview_f * matrix);

vsip_stride vsip_mgetcolstride_i(const vsip_mview_i * matrix);

vsip_stride vsip_mgetcolstride_si(const vsip_mview_si * matrix);

vsip_offset vsip_mgetoffset_bl(const vsip_mview_bl * matrix);

vsip_offset vsip_mgetoffset_f(const vsip_mview_f * matrix);

vsip_offset vsip_mgetoffset_i(const vsip_mview_i * matrix);

vsip_offset vsip_mgetoffset_si(const vsip_mview_si * matrix);

vsip_length vsip_mgetrowlength_bl(const vsip_mview_bl * matrix);

vsip_length vsip_mgetrowlength_f(const vsip_mview_f * matrix);

vsip_length vsip_mgetrowlength_i(const vsip_mview_i * matrix);

vsip_length vsip_mgetrowlength_si(const vsip_mview_si * matrix);

vsip_stride vsip_mgetrowstride_bl(const vsip_mview_bl * matrix);

vsip_stride vsip_mgetrowstride_f(const vsip_mview_f * matrix);

vsip_stride vsip_mgetrowstride_i(const vsip_mview_i * matrix);

vsip_stride vsip_mgetrowstride_si(const vsip_mview_si * matrix);

void vsip_mhypot_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * R);

void vsip_mimag_f(const vsip_cmview_f * A, const vsip_mview_f * R);

vsip_mview_f *vsip_mimagview_f(const vsip_cmview_f * complex_matrix);

vsip_mview_i *vsip_mimagview_i(const vsip_cmview_i * complex_matrix);

vsip_mview_si *vsip_mimagview_si(const vsip_cmview_si * complex_matrix);

vsip_scalar_f vsip_min_f(const vsip_scalar_f A, const vsip_scalar_f B);

vsip_scalar_i vsip_min_i(const vsip_scalar_i A, const vsip_scalar_i B);

void vsip_minvclip_f(const vsip_mview_f * A, const vsip_scalar_f t1,
		     const vsip_scalar_f t2, const vsip_scalar_f t3,
		     const vsip_scalar_f c1, const vsip_scalar_f c2,
		     const vsip_mview_f * R);

void vsip_minvlu_f(const vsip_mview_f * A, const vsip_vview_i * V,
		   const vsip_mview_f * R);

void vsip_mleq_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mleq_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mlge_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mlge_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mlgt_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mlgt_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mlle_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mlle_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mllt_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mllt_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mlne_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_bl * R);

void vsip_mlne_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_bl * R);

void vsip_mlog10_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mlog_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mmag_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mmax_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_mmaxmg_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * R);

vsip_scalar_f vsip_mmaxmgval_f(const vsip_mview_f * A,
			       vsip_scalar_mi * index);

vsip_scalar_f vsip_mmaxval_f(const vsip_mview_f * A, vsip_scalar_mi * index);

vsip_scalar_f vsip_mmeansqval_f(const vsip_mview_f * A);

vsip_scalar_f vsip_mmeanval_f(const vsip_mview_f * A);

void vsip_mmin_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_mminmg_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * R);

vsip_scalar_f vsip_mminmgval_f(const vsip_mview_f * A,
			       vsip_scalar_mi * index);

vsip_scalar_f vsip_mminval_f(const vsip_mview_f * A, vsip_scalar_mi * index);

void vsip_mmul_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_mmul_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_i * R);

void vsip_mneg_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mneg_i(const vsip_mview_i * A, const vsip_mview_i * R);

void vsip_mnot_bl(const vsip_mview_bl * A, const vsip_mview_bl * R);

void vsip_mnot_i(const vsip_mview_i * A, const vsip_mview_i * R);

void vsip_mnot_si(const vsip_mview_si * A, const vsip_mview_si * R);

void vsip_mor_bl(const vsip_mview_bl * A, const vsip_mview_bl * B,
		 const vsip_mview_bl * R);

void vsip_mor_i(const vsip_mview_i * A, const vsip_mview_i * B,
		const vsip_mview_i * R);

void vsip_mor_si(const vsip_mview_si * A, const vsip_mview_si * B,
		 const vsip_mview_si * R);

void vsip_mpolar_f(const vsip_cmview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * C);

void vsip_mprod_f(const vsip_mview_f * A, const vsip_mview_f * B,
		  const vsip_mview_f * R);

void vsip_mprod_i(const vsip_mview_i * A, const vsip_mview_i * B,
		  const vsip_mview_i * R);

void vsip_mprod_si(const vsip_mview_si * A, const vsip_mview_si * B,
		   const vsip_mview_si * R);

void vsip_mprodt_f(const vsip_mview_f * A, const vsip_mview_f * B,
		   const vsip_mview_f * R);

void vsip_mprodt_i(const vsip_mview_i * A, const vsip_mview_i * B,
		   const vsip_mview_i * R);

void vsip_mprodt_si(const vsip_mview_si * A, const vsip_mview_si * B,
		    const vsip_mview_si * R);

void vsip_mput_bl(const vsip_mview_bl * matrix, vsip_index i, vsip_index j,
		  vsip_scalar_bl value);

void vsip_mput_f(const vsip_mview_f * matrix, vsip_index i, vsip_index j,
		 vsip_scalar_f value);

void vsip_mput_i(const vsip_mview_i * matrix, vsip_index i, vsip_index j,
		 vsip_scalar_i value);

void vsip_mput_si(const vsip_mview_si * matrix, vsip_index i, vsip_index j,
		  vsip_scalar_si value);

vsip_mview_bl *vsip_mputattrib_bl(vsip_mview_bl * matrix,
				  const vsip_mattr_bl * attr);

vsip_mview_f *vsip_mputattrib_f(vsip_mview_f * matrix,
				const vsip_mattr_f * attr);

vsip_mview_i *vsip_mputattrib_i(vsip_mview_i * matrix,
				const vsip_mattr_i * attr);

vsip_mview_si *vsip_mputattrib_si(vsip_mview_si * matrix,
				  const vsip_mattr_si * attr);

vsip_mview_bl *vsip_mputcollength_bl(vsip_mview_bl * matrix, vsip_length n2);

vsip_mview_f *vsip_mputcollength_f(vsip_mview_f * matrix, vsip_length n2);

vsip_mview_i *vsip_mputcollength_i(vsip_mview_i * matrix, vsip_length n2);

vsip_mview_si *vsip_mputcollength_si(vsip_mview_si * matrix, vsip_length n2);

vsip_mview_bl *vsip_mputcolstride_bl(vsip_mview_bl * matrix, vsip_stride s2);

vsip_mview_f *vsip_mputcolstride_f(vsip_mview_f * matrix, vsip_stride s2);

vsip_mview_i *vsip_mputcolstride_i(vsip_mview_i * matrix, vsip_stride s2);

vsip_mview_si *vsip_mputcolstride_si(vsip_mview_si * matrix, vsip_stride s2);

vsip_mview_bl *vsip_mputoffset_bl(vsip_mview_bl * matrix, vsip_offset offset);

vsip_mview_f *vsip_mputoffset_f(vsip_mview_f * matrix, vsip_offset offset);

vsip_mview_i *vsip_mputoffset_i(vsip_mview_i * matrix, vsip_offset offset);

vsip_mview_si *vsip_mputoffset_si(vsip_mview_si * matrix, vsip_offset offset);

vsip_mview_bl *vsip_mputrowlength_bl(vsip_mview_bl * matrix, vsip_length n1);

vsip_mview_f *vsip_mputrowlength_f(vsip_mview_f * matrix, vsip_length n1);

vsip_mview_i *vsip_mputrowlength_i(vsip_mview_i * matrix, vsip_length n1);

vsip_mview_si *vsip_mputrowlength_si(vsip_mview_si * matrix, vsip_length n1);

vsip_mview_bl *vsip_mputrowstride_bl(vsip_mview_bl * matrix, vsip_stride s1);

vsip_mview_f *vsip_mputrowstride_f(vsip_mview_f * matrix, vsip_stride s1);

vsip_mview_i *vsip_mputrowstride_i(vsip_mview_i * matrix, vsip_stride s1);

vsip_mview_si *vsip_mputrowstride_si(vsip_mview_si * matrix, vsip_stride s1);

void vsip_mreal_f(const vsip_cmview_f * A, const vsip_mview_f * R);

vsip_mview_f *vsip_mrealview_f(const vsip_cmview_f * complex_matrix);

vsip_mview_i *vsip_mrealview_i(const vsip_cmview_i * complex_matrix);

vsip_mview_si *vsip_mrealview_si(const vsip_cmview_si * complex_matrix);

void vsip_mrecip_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mrect_f(const vsip_mview_f * A, const vsip_mview_f * B,
		  const vsip_cmview_f * R);

vsip_index vsip_mrowindex(vsip_scalar_mi mi);

vsip_vview_bl *vsip_mrowview_bl(const vsip_mview_bl * matrix, vsip_index i);

vsip_vview_f *vsip_mrowview_f(const vsip_mview_f * matrix, vsip_index i);

vsip_vview_i *vsip_mrowview_i(const vsip_mview_i * matrix, vsip_index i);

vsip_vview_si *vsip_mrowview_si(const vsip_mview_si * matrix, vsip_index i);

void vsip_mrsqrt_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mscatter_f(const vsip_vview_f * A, const vsip_mview_f * R,
		     const vsip_vview_mi * B);

void vsip_mscatter_i(const vsip_vview_i * A, const vsip_mview_i * R,
		     const vsip_vview_mi * B);

void vsip_mscatter_si(const vsip_vview_si * A, const vsip_mview_si * R,
		      const vsip_vview_mi * B);

void vsip_msdiv_f(const vsip_mview_f * A, vsip_scalar_f x,
		  const vsip_mview_f * R);

void vsip_msin_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_msinh_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_msq_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_msqrt_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_msub_f(const vsip_mview_f * A, const vsip_mview_f * B,
		 const vsip_mview_f * R);

void vsip_msub_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_i * R);

vsip_mview_bl *vsip_msubview_bl(const vsip_mview_bl * matrix, vsip_index i,
				vsip_index j, vsip_length m, vsip_length n);

vsip_mview_f *vsip_msubview_f(const vsip_mview_f * matrix, vsip_index i,
			      vsip_index j, vsip_length m, vsip_length n);

vsip_mview_i *vsip_msubview_i(const vsip_mview_i * matrix, vsip_index i,
			      vsip_index j, vsip_length m, vsip_length n);

vsip_mview_si *vsip_msubview_si(const vsip_mview_si * matrix, vsip_index i,
				vsip_index j, vsip_length m, vsip_length n);

vsip_scalar_f vsip_msumsqval_f(const vsip_mview_f * A);

vsip_scalar_vi vsip_msumval_bl(const vsip_mview_bl * A);

vsip_scalar_f vsip_msumval_f(const vsip_mview_f * A);

vsip_scalar_i vsip_msumval_i(const vsip_mview_i * A);

void vsip_mswap_f(const vsip_mview_f * A, const vsip_mview_f * B);

void vsip_mswap_i(const vsip_mview_i * A, const vsip_mview_i * B);

void vsip_mswap_si(const vsip_mview_si * A, const vsip_mview_si * B);

void vsip_mtan_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mtanh_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mtrans_bl(const vsip_mview_bl * A, const vsip_mview_bl * R);

void vsip_mtrans_f(const vsip_mview_f * A, const vsip_mview_f * R);

void vsip_mtrans_i(const vsip_mview_i * A, const vsip_mview_i * R);

void vsip_mtrans_si(const vsip_mview_si * A, const vsip_mview_si * R);

vsip_mview_bl *vsip_mtransview_bl(const vsip_mview_bl * matrix);

vsip_mview_f *vsip_mtransview_f(const vsip_mview_f * matrix);

vsip_mview_i *vsip_mtransview_i(const vsip_mview_i * matrix);

vsip_mview_si *vsip_mtransview_si(const vsip_mview_si * matrix);

void vsip_mvprod_f(const vsip_mview_f * A, const vsip_vview_f * X,
		   const vsip_vview_f * Y);

void vsip_mvprod_i(const vsip_mview_i * A, const vsip_vview_i * X,
		   const vsip_vview_i * Y);

void vsip_mvprod_si(const vsip_mview_si * A, const vsip_vview_si * X,
		    const vsip_vview_si * Y);

void vsip_mxor_bl(const vsip_mview_bl * A, const vsip_mview_bl * B,
		  const vsip_mview_bl * R);

void vsip_mxor_i(const vsip_mview_i * A, const vsip_mview_i * B,
		 const vsip_mview_i * R);

void vsip_mxor_si(const vsip_mview_si * A, const vsip_mview_si * B,
		  const vsip_mview_si * R);

void vsip_polar_f(vsip_cscalar_f a, vsip_scalar_f * r, vsip_scalar_f * t);

vsip_scalar_f vsip_pow_f(const vsip_scalar_f A, const vsip_scalar_f B);

vsip_qr_f *vsip_qrd_create_f(const vsip_length M, const vsip_length N,
			     const vsip_qrd_qopt qopt);

int vsip_qrd_destroy_f(vsip_qr_f * qrd);

int vsip_qrd_f(vsip_qr_f * qrd, const vsip_mview_f * A);

void vsip_qrd_getattr_f(const vsip_qr_f * qrd, vsip_qr_attr_f * attr);

int vsip_qrdprodq_f(const vsip_qr_f * qrd, const vsip_mat_op opQ,
		    const vsip_mat_side apQ, const vsip_mview_f * C);

int vsip_qrdsolr_f(const vsip_qr_f * qrd, const vsip_mat_op OpR,
		   const vsip_scalar_f alpha, const vsip_mview_f * XB);

int vsip_qrsol_f(const vsip_qr_f * qrd, const vsip_qrd_prob prob,
		 const vsip_mview_f * XB);

vsip_randstate *vsip_randcreate(const vsip_index seed,
				const vsip_index numprocs,
				const vsip_index id, const vsip_rng portable);

int vsip_randdestroy(vsip_randstate * rand);

vsip_scalar_f vsip_randn_f(vsip_randstate * state);

vsip_scalar_f vsip_randu_f(vsip_randstate * state);

vsip_cscalar_f vsip_rcadd_f(vsip_scalar_f x, vsip_cscalar_f y);

vsip_fft2d_f *vsip_rcfft2dop_create_f(const vsip_index rows,
				      const vsip_index cols,
				      const vsip_scalar_f scale,
				      const vsip_length ntimes,
				      const vsip_alg_hint hint);

void vsip_rcfft2dop_f(const vsip_fft2d_f * plan, const vsip_mview_f * X,
		      const vsip_cmview_f * Y);

vsip_fftm_f *vsip_rcfftmop_create_f(const vsip_index rows,
				    const vsip_index cols,
				    const vsip_scalar_f scale,
				    const vsip_major major,
				    const vsip_length ntimes,
				    const vsip_alg_hint hint);

void vsip_rcfftmop_f(const vsip_fftm_f * plan, const vsip_mview_f * X,
		     const vsip_cmview_f * Y);

vsip_fft_f *vsip_rcfftop_create_f(const vsip_length length,
				  const vsip_scalar_f scale,
				  const vsip_length ntimes,
				  const vsip_alg_hint hint);

void vsip_rcfftop_f(const vsip_fft_f * plan, const vsip_vview_f * x,
		    const vsip_cvview_f * y);

void vsip_rcmadd_f(const vsip_mview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

void vsip_rcmdiv_f(const vsip_mview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

void vsip_rcmmul_f(const vsip_mview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

void vsip_rcmsub_f(const vsip_mview_f * A, const vsip_cmview_f * B,
		   const vsip_cmview_f * R);

vsip_cscalar_f vsip_rcmul_f(vsip_scalar_f x, vsip_cscalar_f y);

vsip_cscalar_f vsip_rcsub_f(vsip_scalar_f x, vsip_cscalar_f y);

void vsip_rcvadd_f(const vsip_vview_f * A, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_rcvdiv_f(const vsip_vview_f * A, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_rcvmul_f(const vsip_vview_f * A, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

void vsip_rcvsub_f(const vsip_vview_f * A, const vsip_cvview_f * B,
		   const vsip_cvview_f * R);

vsip_scalar_f vsip_real_f(vsip_cscalar_f x);

vsip_cscalar_f vsip_rect_f(vsip_scalar_f r, vsip_scalar_f t);

void vsip_rscmadd_f(vsip_scalar_f x, const vsip_cmview_f * A,
		    const vsip_cmview_f * R);

void vsip_rscmdiv_f(vsip_scalar_f x, const vsip_cmview_f * A,
		    const vsip_cmview_f * R);

void vsip_rscmmul_f(const vsip_scalar_f alpha, const vsip_cmview_f * A,
		    const vsip_cmview_f * R);

void vsip_rscmsub_f(vsip_scalar_f x, const vsip_cmview_f * A,
		    const vsip_cmview_f * R);

void vsip_rscvadd_f(const vsip_scalar_f a, const vsip_cvview_f * B,
		    const vsip_cvview_f * R);

void vsip_rscvdiv_f(vsip_scalar_f a, const vsip_cvview_f * B,
		    const vsip_cvview_f * R);

void vsip_rscvmul_f(const vsip_scalar_f a, const vsip_cvview_f * B,
		    const vsip_cvview_f * R);

void vsip_rscvsub_f(const vsip_scalar_f a, const vsip_cvview_f * B,
		    const vsip_cvview_f * R);

vsip_scalar_f vsip_rsqrt_f(const vsip_scalar_f A);

void vsip_rvcmmul_f(const vsip_vview_f * A, const vsip_cmview_f * B,
		    const vsip_major major, const vsip_cmview_f * R);

vsip_scalar_f vsip_sin_f(const vsip_scalar_f A);

vsip_scalar_f vsip_sinh_f(const vsip_scalar_f A);

void vsip_smadd_f(vsip_scalar_f x, const vsip_mview_f * A,
		  const vsip_mview_f * R);

void vsip_smadd_i(vsip_scalar_i x, const vsip_mview_i * A,
		  const vsip_mview_i * R);

void vsip_smdiv_f(vsip_scalar_f x, const vsip_mview_f * A,
		  const vsip_mview_f * R);

void vsip_smmul_f(const vsip_scalar_f alpha, const vsip_mview_f * A,
		  const vsip_mview_f * R);

void vsip_smsub_f(vsip_scalar_f x, const vsip_mview_f * A,
		  const vsip_mview_f * R);

void vsip_smsub_i(vsip_scalar_i x, const vsip_mview_i * A,
		  const vsip_mview_i * R);

vsip_scalar_f vsip_sqrt_f(const vsip_scalar_f A);

void vsip_svadd_f(const vsip_scalar_f a, const vsip_vview_f * B,
		  const vsip_vview_f * R);

void vsip_svadd_i(const vsip_scalar_i a, const vsip_vview_i * B,
		  const vsip_vview_i * R);

void vsip_svadd_si(const vsip_scalar_si a, const vsip_vview_si * B,
		   const vsip_vview_si * R);

void vsip_svdiv_f(const vsip_scalar_f a, const vsip_vview_f * B,
		  const vsip_vview_f * R);

void vsip_svmul_f(const vsip_scalar_f a, const vsip_vview_f * B,
		  const vsip_vview_f * R);

void vsip_svmul_i(const vsip_scalar_i a, const vsip_vview_i * B,
		  const vsip_vview_i * R);

void vsip_svmul_si(const vsip_scalar_si a, const vsip_vview_si * B,
		   const vsip_vview_si * R);

void vsip_svsub_f(const vsip_scalar_f a, const vsip_vview_f * B,
		  const vsip_vview_f * R);

void vsip_svsub_i(const vsip_scalar_i a, const vsip_vview_i * B,
		  const vsip_vview_i * R);

void vsip_svsub_si(const vsip_scalar_si a, const vsip_vview_si * B,
		   const vsip_vview_si * R);

vsip_scalar_f vsip_tan_f(const vsip_scalar_f A);

vsip_scalar_f vsip_tanh_f(const vsip_scalar_f A);

int vsip_toepsol_f(const vsip_vview_f * T, const vsip_vview_f * B,
		   const vsip_vview_f * W, const vsip_vview_f * X);

void vsip_vacos_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vadd_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

void vsip_vadd_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_i * R);

void vsip_vadd_si(const vsip_vview_si * A, const vsip_vview_si * B,
		  const vsip_vview_si * R);

void vsip_valldestroy_bl(vsip_vview_bl * vektor);

void vsip_valldestroy_f(vsip_vview_f * vektor);

void vsip_valldestroy_i(vsip_vview_i * vektor);

void vsip_valldestroy_mi(vsip_vview_mi * vektor);

void vsip_valldestroy_si(vsip_vview_si * vektor);

void vsip_valldestroy_vi(vsip_vview_vi * vektor);

vsip_scalar_bl vsip_valltrue_bl(const vsip_vview_bl * A);

void vsip_vam_f(const vsip_vview_f * A, const vsip_vview_f * B,
		const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vand_bl(const vsip_vview_bl * A, const vsip_vview_bl * B,
		  const vsip_vview_bl * R);

void vsip_vand_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_i * R);

void vsip_vand_si(const vsip_vview_si * A, const vsip_vview_si * B,
		  const vsip_vview_si * R);

vsip_scalar_bl vsip_vanytrue_bl(const vsip_vview_bl * A);

void vsip_varg_f(const vsip_cvview_f * A, const vsip_vview_f * R);

void vsip_vasin_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vatan2_f(const vsip_vview_f * A, const vsip_vview_f * B,
		   const vsip_vview_f * R);

void vsip_vatan_f(const vsip_vview_f * A, const vsip_vview_f * R);

vsip_vview_bl *vsip_vbind_bl(vsip_block_bl * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

vsip_vview_f *vsip_vbind_f(vsip_block_f * block, vsip_offset offset,
			   vsip_stride stride, vsip_length length);

vsip_vview_i *vsip_vbind_i(vsip_block_i * block, vsip_offset offset,
			   vsip_stride stride, vsip_length length);

vsip_vview_mi *vsip_vbind_mi(vsip_block_mi * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

vsip_vview_si *vsip_vbind_si(vsip_block_si * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

vsip_vview_vi *vsip_vbind_vi(vsip_block_vi * block, vsip_offset offset,
			     vsip_stride stride, vsip_length length);

void vsip_vceil_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vcfarmin_f(vsip_vview_f * A, vsip_vview_f * B,
          const vsip_scalar_i win, const vsip_scalar_i interval,
          const vsip_vview_f * R);

void vsip_vcfarmax_f(vsip_vview_f * A, vsip_vview_f * B,
          const vsip_scalar_i win, const vsip_scalar_i interval,
          const vsip_vview_f * R);

void vsip_vclip_f(const vsip_vview_f * A, const vsip_scalar_f t1,
		  const vsip_scalar_f t2, const vsip_scalar_f c1,
		  const vsip_scalar_f c2, const vsip_vview_f * R);

void vsip_vclip_i(const vsip_vview_i * A, const vsip_scalar_i t1,
		  const vsip_scalar_i t2, const vsip_scalar_i c1,
		  const vsip_scalar_i c2, const vsip_vview_i * R);

void vsip_vclip_si(const vsip_vview_si * A, const vsip_scalar_si t1,
		   const vsip_scalar_si t2, const vsip_scalar_si c1,
		   const vsip_scalar_si c2, const vsip_vview_si * R);

vsip_vview_bl *vsip_vcloneview_bl(const vsip_vview_bl * vektor);

vsip_vview_f *vsip_vcloneview_f(const vsip_vview_f * vektor);

vsip_vview_i *vsip_vcloneview_i(const vsip_vview_i * vektor);

vsip_vview_mi *vsip_vcloneview_mi(const vsip_vview_mi * vektor);

vsip_vview_si *vsip_vcloneview_si(const vsip_vview_si * vektor);

vsip_vview_vi *vsip_vcloneview_vi(const vsip_vview_vi * vektor);

void vsip_vcmagsq_f(const vsip_cvview_f * A, const vsip_vview_f * R);

void vsip_vcmaxmgsq_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		      const vsip_vview_f * R);

vsip_scalar_f vsip_vcmaxmgsqval_f(const vsip_cvview_f * A,
				  vsip_index * index);

void vsip_vcminmgsq_f(const vsip_cvview_f * A, const vsip_cvview_f * B,
		      const vsip_vview_f * R);

vsip_scalar_f vsip_vcminmgsqval_f(const vsip_cvview_f * A,
				  vsip_index * index);

void vsip_vcmplx_f(const vsip_vview_f * A, const vsip_vview_f * B,
		   const vsip_cvview_f * R);

void vsip_vcopy_bl_bl(const vsip_vview_bl * A, const vsip_vview_bl * R);

void vsip_vcopy_bl_f(const vsip_vview_bl * A, const vsip_vview_f * R);

void vsip_vcopy_f_bl(const vsip_vview_f * A, const vsip_vview_bl * R);

void vsip_vcopy_f_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vcopy_f_i(const vsip_vview_f * A, const vsip_vview_i * R);

void vsip_vcopy_f_si(const vsip_vview_f * A, const vsip_vview_si * R);

void vsip_vcopy_i_f(const vsip_vview_i * A, const vsip_vview_f * R);

void vsip_vcopy_i_i(const vsip_vview_i * A, const vsip_vview_i * R);

void vsip_vcopy_i_si(const vsip_vview_i * A, const vsip_vview_si * R);

void vsip_vcopy_i_vi(const vsip_vview_i * A, const vsip_vview_vi * R);

void vsip_vcopy_mi_mi(const vsip_vview_mi * A, const vsip_vview_mi * R);

void vsip_vcopy_si_f(const vsip_vview_si * A, const vsip_vview_f * R);

void vsip_vcopy_si_i(const vsip_vview_si * A, const vsip_vview_i * R);

void vsip_vcopy_si_si(const vsip_vview_si * A, const vsip_vview_si * R);

void vsip_vcopy_vi_i(const vsip_vview_vi * A, const vsip_vview_i * R);

void vsip_vcopy_vi_vi(const vsip_vview_vi * A, const vsip_vview_vi * R);

void vsip_vcos_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vcosh_f(const vsip_vview_f * A, const vsip_vview_f * R);

vsip_vview_bl *vsip_vcreate_bl(vsip_length length, vsip_memory_hint hint);

vsip_vview_f *vsip_vcreate_blackman_f(const vsip_length N,
				      const vsip_memory_hint hint);

vsip_vview_f *vsip_vcreate_cheby_f(const vsip_length N,
				   const vsip_scalar_f ripple,
				   const vsip_memory_hint hint);

vsip_vview_f *vsip_vcreate_f(vsip_length length, vsip_memory_hint hint);

vsip_vview_f *vsip_vcreate_hanning_f(const vsip_length N,
				     const vsip_memory_hint hint);

vsip_vview_i *vsip_vcreate_i(vsip_length length, vsip_memory_hint hint);

vsip_vview_f *vsip_vcreate_kaiser_f(const vsip_length N,
				    const vsip_scalar_f beta,
				    const vsip_memory_hint hint);

vsip_vview_mi *vsip_vcreate_mi(vsip_length length, vsip_memory_hint hint);

vsip_vview_si *vsip_vcreate_si(vsip_length length, vsip_memory_hint hint);

vsip_vview_vi *vsip_vcreate_vi(vsip_length length, vsip_memory_hint hint);

vsip_scalar_f vsip_vcsummgval_f(const vsip_cvview_f * A);

void vsip_vcumsum_f(const vsip_vview_f * A, const vsip_vview_f * B);

void vsip_vcumsum_i(const vsip_vview_i * A, const vsip_vview_i * B);

vsip_block_bl *vsip_vdestroy_bl(vsip_vview_bl * vektor);

vsip_block_f *vsip_vdestroy_f(vsip_vview_f * vektor);

vsip_block_i *vsip_vdestroy_i(vsip_vview_i * vektor);

vsip_block_mi *vsip_vdestroy_mi(vsip_vview_mi * vektor);

vsip_block_si *vsip_vdestroy_si(vsip_vview_si * vektor);

vsip_block_vi *vsip_vdestroy_vi(vsip_vview_vi * vektor);

void vsip_vdiv_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

vsip_scalar_f vsip_vdot_f(const vsip_vview_f * A, const vsip_vview_f * B);

void vsip_veuler_f(const vsip_vview_f * A, const vsip_cvview_f * R);

void vsip_vexp10_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vexp_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vexpoavg_f(const vsip_scalar_f a, const vsip_vview_f * B,
		     const vsip_vview_f * C);

void vsip_vfill_f(const vsip_scalar_f a, const vsip_vview_f * R);

void vsip_vfill_i(const vsip_scalar_i a, const vsip_vview_i * R);

void vsip_vfill_si(const vsip_scalar_si a, const vsip_vview_si * R);

void vsip_vfloor_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vgather_f(const vsip_vview_f * X, const vsip_vview_vi * I,
		    const vsip_vview_f * Y);

void vsip_vgatherbl_f(const vsip_vview_f * X, const vsip_vview_bl * I, 
		      const vsip_vview_f * Y);

void vsip_vgather_i(const vsip_vview_i * X, const vsip_vview_vi * I,
		    const vsip_vview_i * Y);

void vsip_vgather_si(const vsip_vview_si * X, const vsip_vview_vi * I,
		     const vsip_vview_si * Y);

vsip_scalar_bl vsip_vget_bl(const vsip_vview_bl * vektor, vsip_index j);

vsip_scalar_f vsip_vget_f(const vsip_vview_f * vektor, vsip_index j);

vsip_scalar_i vsip_vget_i(const vsip_vview_i * vektor, vsip_index j);

vsip_scalar_mi vsip_vget_mi(const vsip_vview_mi * vektor, vsip_index j);

vsip_scalar_si vsip_vget_si(const vsip_vview_si * vektor, vsip_index j);

vsip_scalar_vi vsip_vget_vi(const vsip_vview_vi * vektor, vsip_index j);

void vsip_vgetattrib_bl(const vsip_vview_bl * vektor, vsip_vattr_bl * attr);

void vsip_vgetattrib_f(const vsip_vview_f * vektor, vsip_vattr_f * attr);

void vsip_vgetattrib_i(const vsip_vview_i * vektor, vsip_vattr_i * attr);

void vsip_vgetattrib_mi(const vsip_vview_mi * vektor, vsip_vattr_mi * attr);

void vsip_vgetattrib_si(const vsip_vview_si * vektor, vsip_vattr_si * attr);

void vsip_vgetattrib_vi(const vsip_vview_vi * vektor, vsip_vattr_vi * attr);

vsip_block_bl *vsip_vgetblock_bl(const vsip_vview_bl * vektor);

vsip_block_f *vsip_vgetblock_f(const vsip_vview_f * vektor);

vsip_block_i *vsip_vgetblock_i(const vsip_vview_i * vektor);

vsip_block_mi *vsip_vgetblock_mi(const vsip_vview_mi * vektor);

vsip_block_si *vsip_vgetblock_si(const vsip_vview_si * vektor);

vsip_block_vi *vsip_vgetblock_vi(const vsip_vview_vi * vektor);

vsip_length vsip_vgetlength_bl(const vsip_vview_bl * vektor);

vsip_length vsip_vgetlength_f(const vsip_vview_f * vektor);

vsip_length vsip_vgetlength_i(const vsip_vview_i * vektor);

vsip_length vsip_vgetlength_mi(const vsip_vview_mi * vektor);

vsip_length vsip_vgetlength_si(const vsip_vview_si * vektor);

vsip_length vsip_vgetlength_vi(const vsip_vview_vi * vektor);

vsip_offset vsip_vgetoffset_bl(const vsip_vview_bl * vektor);

vsip_offset vsip_vgetoffset_f(const vsip_vview_f * vektor);

vsip_offset vsip_vgetoffset_i(const vsip_vview_i * vektor);

vsip_offset vsip_vgetoffset_mi(const vsip_vview_mi * vektor);

vsip_offset vsip_vgetoffset_si(const vsip_vview_si * vektor);

vsip_offset vsip_vgetoffset_vi(const vsip_vview_vi * vektor);

vsip_stride vsip_vgetstride_bl(const vsip_vview_bl * vektor);

vsip_stride vsip_vgetstride_f(const vsip_vview_f * vektor);

vsip_stride vsip_vgetstride_i(const vsip_vview_i * vektor);

vsip_stride vsip_vgetstride_mi(const vsip_vview_mi * vektor);

vsip_stride vsip_vgetstride_si(const vsip_vview_si * vektor);

vsip_stride vsip_vgetstride_vi(const vsip_vview_vi * vektor);

void vsip_vhisto_f(const vsip_vview_f * A, const vsip_scalar_f min,
		   const vsip_scalar_f max, const vsip_hist_opt opt,
		   const vsip_vview_f * R);

void vsip_vhypot_f(const vsip_vview_f * A, const vsip_vview_f * B,
		   const vsip_vview_f * R);

void vsip_vimag_f(const vsip_cvview_f * A, const vsip_vview_f * R);

vsip_vview_f *vsip_vimagview_f(const vsip_cvview_f * complex_vector);

vsip_vview_i *vsip_vimagview_i(const vsip_cvview_i * complex_vector);

vsip_vview_si *vsip_vimagview_si(const vsip_cvview_si * complex_vector);

vsip_length vsip_vindexbool(const vsip_vview_bl * X, vsip_vview_vi * Y);

void vsip_vinvclip_f(const vsip_vview_f * A, const vsip_scalar_f t1,
		     const vsip_scalar_f t2, const vsip_scalar_f t3,
		     const vsip_scalar_f c1, const vsip_scalar_f c2,
		     const vsip_vview_f * R);

void vsip_vinvclip_i(const vsip_vview_i * A, const vsip_scalar_i t1,
		     const vsip_scalar_i t2, const vsip_scalar_i t3,
		     const vsip_scalar_i c1, const vsip_scalar_i c2,
		     const vsip_vview_i * R);

void vsip_vinvclip_si(const vsip_vview_si * A, const vsip_scalar_si t1,
		      const vsip_scalar_si t2, const vsip_scalar_si t3,
		      const vsip_scalar_si c1, const vsip_scalar_si c2,
		      const vsip_vview_si * R);

void vsip_vleq_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vleq_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);

void vsip_vlge_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vlge_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);

void vsip_vlgt_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vlgt_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);

void vsip_vlle_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vlle_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);

void vsip_vllt_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vllt_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);
		 
void vsip_vlmerg_f(const vsip_vview_f * A, const vsip_vview_f * B, 
		   const vsip_vview_bl * C,const vsip_vview_f * R);

void vsip_vlne_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_bl * R);

void vsip_vlne_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_bl * R);

void vsip_vlog10_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vlog_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vma_f(const vsip_vview_f * A, const vsip_vview_f * B,
		const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vmag_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vmag_i(const vsip_vview_i * A, const vsip_vview_i * R);

void vsip_vmag_si(const vsip_vview_si * A, const vsip_vview_si * R);

void vsip_vmax_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

void vsip_vmaxmg_f(const vsip_vview_f * A, const vsip_vview_f * B,
		   const vsip_vview_f * R);

vsip_scalar_f vsip_vmaxmgval_f(const vsip_vview_f * A, vsip_index * index);

vsip_scalar_f vsip_vmaxval_f(const vsip_vview_f * A, vsip_index * index);

vsip_scalar_f vsip_vmeansqval_f(const vsip_vview_f * A);

vsip_scalar_f vsip_vmeanval_f(const vsip_vview_f * A);

void vsip_vmin_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

void vsip_vminmg_f(const vsip_vview_f * A, const vsip_vview_f * B,
		   const vsip_vview_f * R);

vsip_scalar_f vsip_vminmgval_f(const vsip_vview_f * A, vsip_index * index);

vsip_scalar_f vsip_vminval_f(const vsip_vview_f * A, vsip_index * index);

void vsip_vmmul_f(const vsip_vview_f * A, const vsip_mview_f * B,
		  const vsip_major major, const vsip_mview_f * R);

vsip_scalar_f vsip_vmodulate_f(const vsip_vview_f * A, const vsip_scalar_f nu,
			       const vsip_scalar_f phi,
			       const vsip_cvview_f * R);

void vsip_vmprod_f(const vsip_vview_f * X, const vsip_mview_f * A,
		   const vsip_vview_f * Y);

void vsip_vmprod_i(const vsip_vview_i * X, const vsip_mview_i * A,
		   const vsip_vview_i * Y);

void vsip_vmprod_si(const vsip_vview_si * X, const vsip_mview_si * A,
		    const vsip_vview_si * Y);

void vsip_vmsa_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_scalar_f c, const vsip_vview_f * R);

void vsip_vmsb_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vmul_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

void vsip_vmul_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_i * R);

void vsip_vmul_si(const vsip_vview_si * A, const vsip_vview_si * B,
		  const vsip_vview_si * R);

void vsip_vneg_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vneg_i(const vsip_vview_i * A, const vsip_vview_i * R);

void vsip_vneg_si(const vsip_vview_si * A, const vsip_vview_si * R);

void vsip_vnot_bl(const vsip_vview_bl * A, const vsip_vview_bl * R);

void vsip_vnot_i(const vsip_vview_i * A, const vsip_vview_i * R);

void vsip_vnot_si(const vsip_vview_si * A, const vsip_vview_si * R);

void vsip_vor_bl(const vsip_vview_bl * A, const vsip_vview_bl * B,
		 const vsip_vview_bl * R);

void vsip_vor_i(const vsip_vview_i * A, const vsip_vview_i * B,
		const vsip_vview_i * R);

void vsip_vor_si(const vsip_vview_si * A, const vsip_vview_si * B,
		 const vsip_vview_si * R);

void vsip_vouter_f(const vsip_scalar_f a, const vsip_vview_f * X,
		   const vsip_vview_f * Y, const vsip_mview_f * R);

void vsip_vpolar_f(const vsip_cvview_f * A, const vsip_vview_f * R,
		   const vsip_vview_f * P);

void vsip_vput_bl(vsip_vview_bl * vektor, vsip_index j, vsip_scalar_bl value);

void vsip_vput_f(vsip_vview_f * vektor, vsip_index j, vsip_scalar_f value);

void vsip_vput_i(vsip_vview_i * vektor, vsip_index j, vsip_scalar_i value);

void vsip_vput_mi(vsip_vview_mi * vektor, vsip_index j, vsip_scalar_mi value);

void vsip_vput_si(vsip_vview_si * vektor, vsip_index j, vsip_scalar_si value);

void vsip_vput_vi(vsip_vview_vi * vektor, vsip_index j, vsip_scalar_vi value);

vsip_vview_bl *vsip_vputattrib_bl(vsip_vview_bl * vektor,
				  const vsip_vattr_bl * attr);

vsip_vview_f *vsip_vputattrib_f(vsip_vview_f * vektor,
				const vsip_vattr_f * attr);

vsip_vview_i *vsip_vputattrib_i(vsip_vview_i * vektor,
				const vsip_vattr_i * attr);

vsip_vview_mi *vsip_vputattrib_mi(vsip_vview_mi * vektor,
				  const vsip_vattr_mi * attr);

vsip_vview_si *vsip_vputattrib_si(vsip_vview_si * vektor,
				  const vsip_vattr_si * attr);

vsip_vview_vi *vsip_vputattrib_vi(vsip_vview_vi * vektor,
				  const vsip_vattr_vi * attr);

vsip_vview_bl *vsip_vputlength_bl(vsip_vview_bl * vektor, vsip_length length);

vsip_vview_f *vsip_vputlength_f(vsip_vview_f * vektor, vsip_length length);

vsip_vview_i *vsip_vputlength_i(vsip_vview_i * vektor, vsip_length length);

vsip_vview_mi *vsip_vputlength_mi(vsip_vview_mi * vektor, vsip_length length);

vsip_vview_si *vsip_vputlength_si(vsip_vview_si * vektor, vsip_length length);

vsip_vview_vi *vsip_vputlength_vi(vsip_vview_vi * vektor, vsip_length length);

vsip_vview_bl *vsip_vputoffset_bl(vsip_vview_bl * vektor, vsip_offset offset);

vsip_vview_f *vsip_vputoffset_f(vsip_vview_f * vektor, vsip_offset offset);

vsip_vview_i *vsip_vputoffset_i(vsip_vview_i * vektor, vsip_offset offset);

vsip_vview_mi *vsip_vputoffset_mi(vsip_vview_mi * vektor, vsip_offset offset);

vsip_vview_si *vsip_vputoffset_si(vsip_vview_si * vektor, vsip_offset offset);

vsip_vview_vi *vsip_vputoffset_vi(vsip_vview_vi * vektor, vsip_offset offset);

vsip_vview_bl *vsip_vputstride_bl(vsip_vview_bl * vektor, vsip_stride stride);

vsip_vview_f *vsip_vputstride_f(vsip_vview_f * vektor, vsip_stride stride);

vsip_vview_i *vsip_vputstride_i(vsip_vview_i * vektor, vsip_stride stride);

vsip_vview_mi *vsip_vputstride_mi(vsip_vview_mi * vektor, vsip_stride stride);

vsip_vview_si *vsip_vputstride_si(vsip_vview_si * vektor, vsip_stride stride);

vsip_vview_vi *vsip_vputstride_vi(vsip_vview_vi * vektor, vsip_stride stride);

void vsip_vramp_f(const vsip_scalar_f alpha, const vsip_scalar_f beta,
		  const vsip_vview_f * R);

void vsip_vramp_i(const vsip_scalar_i alpha, const vsip_scalar_i beta,
		  const vsip_vview_i * R);

void vsip_vramp_si(const vsip_scalar_si alpha, const vsip_scalar_si beta,
		   const vsip_vview_si * R);

void vsip_vrandn_f(vsip_randstate * state, const vsip_vview_f * R);

void vsip_vrandu_f(vsip_randstate * state, const vsip_vview_f * R);

void vsip_vreal_f(const vsip_cvview_f * A, const vsip_vview_f * R);

vsip_vview_f *vsip_vrealview_f(const vsip_cvview_f * complex_vector);

vsip_vview_i *vsip_vrealview_i(const vsip_cvview_i * complex_vector);

vsip_vview_si *vsip_vrealview_si(const vsip_cvview_si * complex_vector);

void vsip_vrecip_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vrect_f(const vsip_vview_f * R, const vsip_vview_f * P,
		  const vsip_cvview_f * A);

void vsip_vrsqrt_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vsam_f(const vsip_vview_f * A, const vsip_scalar_f b,
		 const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vsbm_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vscatter_f(const vsip_vview_f * X, const vsip_vview_f * Y,
		     const vsip_vview_vi * I);

void vsip_vscatter_i(const vsip_vview_i * X, const vsip_vview_i * Y,
		     const vsip_vview_vi * I);

void vsip_vscatter_si(const vsip_vview_si * X, const vsip_vview_si * Y,
		      const vsip_vview_vi * I);

void vsip_vsdiv_f(const vsip_vview_f * A, const vsip_scalar_f b,
		  const vsip_vview_f * R);

void vsip_vsin_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vsinh_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vsma_f(const vsip_vview_f * A, const vsip_scalar_f b,
		 const vsip_vview_f * C, const vsip_vview_f * R);

void vsip_vsmsa_f(const vsip_vview_f * A, const vsip_scalar_f b,
		  const vsip_scalar_f c, const vsip_vview_f * R);

void vsip_vsq_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vsqrt_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vsub_f(const vsip_vview_f * A, const vsip_vview_f * B,
		 const vsip_vview_f * R);

void vsip_vsub_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_i * R);

void vsip_vsub_si(const vsip_vview_si * A, const vsip_vview_si * B,
		  const vsip_vview_si * R);

vsip_vview_bl *vsip_vsubview_bl(const vsip_vview_bl * vektor,
				vsip_index index, vsip_length length);

vsip_vview_f *vsip_vsubview_f(const vsip_vview_f * vektor, vsip_index index,
			      vsip_length length);

vsip_vview_i *vsip_vsubview_i(const vsip_vview_i * vektor, vsip_index index,
			      vsip_length length);

vsip_vview_mi *vsip_vsubview_mi(const vsip_vview_mi * vektor,
				vsip_index index, vsip_length length);

vsip_vview_si *vsip_vsubview_si(const vsip_vview_si * vektor,
				vsip_index index, vsip_length length);

vsip_vview_vi *vsip_vsubview_vi(const vsip_vview_vi * vektor,
				vsip_index index, vsip_length length);

vsip_scalar_f vsip_vsumsqval_f(const vsip_vview_f * A);

vsip_scalar_vi vsip_vsumval_bl(const vsip_vview_bl * A);

vsip_scalar_f vsip_vsumval_f(const vsip_vview_f * A);

vsip_scalar_i vsip_vsumval_i(const vsip_vview_i * A);

vsip_scalar_si vsip_vsumval_si(const vsip_vview_si * A);

void vsip_vswap_f(const vsip_vview_f * A, const vsip_vview_f * B);

void vsip_vswap_i(const vsip_vview_i * A, const vsip_vview_i * B);

void vsip_vswap_si(const vsip_vview_si * A, const vsip_vview_si * B);

void vsip_vtan_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vtanh_f(const vsip_vview_f * A, const vsip_vview_f * R);

void vsip_vthr_f(const vsip_vview_f * A, const vsip_scalar_f thr,
        const vsip_vview_f * R);

void vsip_vxor_bl(const vsip_vview_bl * A, const vsip_vview_bl * B,
		  const vsip_vview_bl * R);

void vsip_vxor_i(const vsip_vview_i * A, const vsip_vview_i * B,
		 const vsip_vview_i * R);

void vsip_vxor_si(const vsip_vview_si * A, const vsip_vview_si * B,
		  const vsip_vview_si * R);

/************************************************************************
 * Error checking for development mode
 ************************************************************************/
/* The error checking mechanism is same as in TASP implementation. */

#ifdef VSIP_DEVELOPMENT_MODE

/* public enumerated errors */
typedef enum {
  vsip_invalid_structure_object = 1 << 0,
  vsip_bad_public_array_pointer = 1 << 1,
  vsip_bad_private_array_storage = 1 << 2,
  vsip_non_zero_binding_counter = 1 << 3,
  vsip_view_outside_data_array = 1 << 4,
  vsip_view_index_exceeds_length = 1 << 5,
  vsip_nonconformal_view_lengths = 1 << 6,
  vsip_clobbered_source_operand = 1 << 7
} vsip_error;

vsip_error vsip_set_error_mask(vsip_error mask);
void vsip_put_error_mask(vsip_error mask);
int vsip_check_error(vsip_error mask);

#define VSIP_SET_ERROR_MASK(t,m) vsip_error t = vsip_set_error_mask((vsip_error)(m));
#define VSIP_PUT_ERROR_MASK(m)   vsip_put_error_mask((vsip_error)(m));

#else

#define VSIP_SET_ERROR_MASK(t,m)
#define VSIP_PUT_ERROR_MASK(m)

#endif /* VSIP_DEVELOPMENT_MODE */

/********************************************************************/
/*
** The error checking mechanism and some other source code is derived
** from TASP VSIPL sources.  The copyright notice of the TASP sources
** follows.
** --------------------------------------------------------------------
** For TASP VSIPL Documentation and Code neither the United States
** Government, the United States Navy, nor any of their employees,
** makes any warranty, express or implied, including the warranties of
** merchantability and fitness for a particular purpose, or assumes
** any legal liability or responsibility for the accuracy,
** completeness, or usefulness of any information, apparatus, product,
** or process disclosed, or represents that its use would not infringe
** privately owned rights.
** --------------------------------------------------------------------
** Copyright (c) Hughes Research Laboratories, Inc. 1997 A
** non-exclusive, non-royalty bearing license is hereby granted to all
** persons to copy, modify, distribute and produce derivative works
** for any purpose, provided that this copyright notice and following
** disclaimer appear on all copies.
** 
** THIS LICENSE INCLUDES NO WARRANTEES, EXPRESSED OR IMPLIED, WHETHER ORAL 
** OR WRITTEN, WITH RESPECT TO THE SOFTWARE OR OTHER MATERIAL, INCLUDING 
** BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY, OR FITNESS 
** FOR A PARTICULAR PURPOSE, OR ARISING FROM A COURSE OF PERFORMANCE, OR 
** DEALING, OR FROM USAGE OR TRADE, OR OF NON-INFRINGEMENT OF ANY PATENTS 
** OF THIRD PARTIES.
**
** This material is based upon work supported by Ft. Huachuca/DARPA under
** Contract No.  DABT63-96-C-0060.  Any opinions, findings and conclusions
** or recommendations expressed in this material are those of the author(s)
** and do not necessarily reflect the views of Ft. Huachuca/DARPA.  */

#endif /* VSIP_HEADER_H */
/************************************************************************
 * End of file
 ************************************************************************/
