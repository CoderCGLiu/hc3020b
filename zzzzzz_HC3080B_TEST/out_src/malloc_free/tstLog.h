#include "logLib.h"

#define logMsg(fmt, a, b, c, d, e, f)   \
        do {    \
            API_LogMsg(fmt, (PVOID)(a), (PVOID)(b), (PVOID)(c), \
                       (PVOID)(d), (PVOID)(e), (PVOID)(f),    \
                       0, 0, 0, 0, FALSE);  \
        } while (0)
