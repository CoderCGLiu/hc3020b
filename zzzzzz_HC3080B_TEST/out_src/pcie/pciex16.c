#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semLib.h>
#include "hr3APIs.h"


/*映射关系：
 * <RC端>访问outbound地址, 即  ep_mem_axi_addr <-> ep_mem_pcie_addr <-> ep_ib_axi_addr, 即EP端的inbound地址
 * <EP端>访问outbound地址, 即  ep_ob_axi_addr <-> ep_ob_pcie_addr <-> rc_ib_axi_addr, 即RC端的inbound地址
 * 
 *                    AXI ADDR          <==>       PCIE ADDR           <==>        AXI ADDR
 * <RC AXI> 0x3000_0000-0x31ff_ffff   <==>   0x3000_0000-0x31ff_ffff   <==>  0x1a00_0000 <EP-CFG> (2^25) type0 CFG
 * <RC-OB> 0x3200_0000-0x3200_ffff <==> 0x1f45_0000-0x1f45_ffff <==> 0x1f45_0000-0x1f45_ffff <EP-IB> (2^16) MSI
 * <RC-IB> 0x20_9000_0000-0x20_91ff_ffff <==> 0x1_8000_0000-0x1_81ff_ffff <==> 0x1_8000_0000-0x1_81ff_ffff <EP-OB> (2^25)DMA
 * <RC-IB2> 0x1f45_0000-0x1f45_ffff <==> 0x3300_0000-0x3300_ffff  <==>  0x3300_0000-0x3300_ffff <EP-OB2>  (2^16)  MSI
 * 
 *
 * */

static int dspID;
static int mem_size = 1<<25;
static unsigned long rc_ib_addr = 0x9000002090000000;
static unsigned long ep_ob_addr = 0x9000000180000000;
static SEM_ID semID_PCIE_MEM_RD_Complete;//接收端接收完成，通知本侧检查数据
static SEM_ID semID_PCIE_MEM_RD_Complete2;//接收端接收完成，通知对侧发送数据
static SEM_ID semID_PCIE_MEM_WR_Complete;//发送端发送完成，通知对侧接收数据
static SEM_ID semID_PCIE_MEM_WR_Complete2;//对侧接收端接收完成，通知本侧继续发送数据
static int test_loop = 10;/*测试次数*/
static unsigned char data = 0x12;/*测试数据初始值*/
static int exit_flag_send = 0;
static int exit_flag_recv = 0;

extern void pciex16_trig_msi_int(void);
extern unsigned int sysCpuGetID();

static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			printf("Check failed! index=%d, val=0x%x, except_val=0x%x\n",\
					index,valDst,valSrc);
			return -1;
		}
    }
	printf("Check OK!\n");
	return 0;
}
/*发送端msi中断回调函数*/
int test_pciex16_msi_callback_send(void)
{
	//对侧接收端接收完成，通知本侧继续发送数据
	semGive(semID_PCIE_MEM_WR_Complete2);
	
	return 0;
}

/*接收端msi中断回调函数*/
int test_pciex16_msi_callback_recv(const unsigned char *srcAddr, unsigned char *dstAddr, int len)
{
	printf("<***DEBUG***> [%s:_%d_]:: srcAddr = 0x%lx, dstAddr = 0x%lx, len = 0x%x\n",
			__FUNCTION__,__LINE__,srcAddr,dstAddr,len);
	
	/*从MEM读取数据出来*/
	memcpy(dstAddr, srcAddr, len);
	/*本测接收完成，通知本侧进行检查数据*/
	semGive(semID_PCIE_MEM_RD_Complete);
	
	return 0;
}

/*通知对侧读取数据*/
void tPcieTrigMsiRead(void)
{
	while(1)
	{
		semTake(semID_PCIE_MEM_WR_Complete, WAIT_FOREVER);//本侧发送完成
		if(exit_flag_send)
			break;
//		pciex16_trig_msi_int();//通知对侧读取数据
	}
	return;
}

/*接收端通知对侧发送数据*/
void tPcieTrigMsiWrite(void)
{
	while(1)
	{
		semTake(semID_PCIE_MEM_RD_Complete2, WAIT_FOREVER);//本侧读取完成
		if(exit_flag_recv)
			break;
//		pciex16_trig_msi_int();//通知对侧发送端继续发送数据
	}
	return;
}

void tPcieSendFun(void *arg)
{
	int i = 0, j = 0;
	unsigned char *test_ddr_addr_wr = (unsigned char *)arg;

	for(i = 0; i < test_loop; i++)
	{
		semTake(semID_PCIE_MEM_WR_Complete2, WAIT_FOREVER);//对侧接收端接收完成，通知本侧继续发送数据

		data = data + i;
		for(j = 0; j < mem_size; j++)
		{
			*(test_ddr_addr_wr + j) = (unsigned char)(j + data);
		}
		
		if(dspID == 0)//ep
		{
			memcpy(ep_ob_addr, test_ddr_addr_wr, mem_size);
		}
		else if(dspID == 1)//rc
		{
			memcpy(rc_ib_addr, test_ddr_addr_wr, mem_size);
		}
		
		semGive(semID_PCIE_MEM_WR_Complete);//发送端发送完成，通知对侧接收数据
		printf("[DSP%d] i=%d 数据发送完成\n",dspID,i);
		usleep(10);
	}
	
	exit_flag_send = 1;
	
	semGive(semID_PCIE_MEM_WR_Complete);/*解除任务tPcieTrigMsiRead阻塞*/
	semDelete(semID_PCIE_MEM_WR_Complete);
	semDelete(semID_PCIE_MEM_WR_Complete2);
	free(test_ddr_addr_wr);
	
	return;
}

/*接收端检查数据*/
void tPcieRecvFun(void *arg)
{
	unsigned char *test_ddr_addr_rd = (unsigned char *)arg;
	int ret = 0;
	int err_count = 0;
	int err_cycle = 0;
	int recv_num = 0;
	int i = 0;
	
	while(1)
	{
		if(recv_num >= test_loop)
		{
			exit_flag_recv = 1;
			break;
		}
		
		semTake(semID_PCIE_MEM_RD_Complete, WAIT_FOREVER);//接收端接收完成，通知本侧检查数据
		
		printf("[DSP%d] num=%d 开始检查数据 ...\n",dspID,recv_num);
		data = data + recv_num;
		for(i = 0; i < mem_size; i++)
		{
			if((*(test_ddr_addr_rd + i)) != (unsigned char)(data + i))
			{
				err_count++;
				printf("[DSP%d] num=%d [0x%lx]=0x%x, except=0x%x\n",dspID,recv_num,err_count,(*(test_ddr_addr_rd + i)),(data + i));
			}
		}
		
		if(err_count > 0)
		{
			printf("[DSP%d] num=%d 数据检查错误, 错误共计: %d bytes\n",dspID,recv_num,err_count);
			err_cycle++;
		}
		else
		{
			printf("[DSP%d] num=%d 数据检查正确\n",dspID,recv_num);
		}
		
		semGive(semID_PCIE_MEM_RD_Complete2);//接收端接收完成，通知对侧发送数据
		
		recv_num++;
	}
	
	
	if(err_cycle != 0)
		printf("[DSP%d] Test Pciex16 mem read/write 【FAIL】 \n",dspID);
	else
		printf("[DSP%d] Test Pciex16 mem read/write 【PASS】 \n",dspID);
	
	semGive(semID_PCIE_MEM_RD_Complete2);/*解除任务tPcieTrigMsiWrite阻塞*/
	semDelete(semID_PCIE_MEM_RD_Complete);
	semDelete(semID_PCIE_MEM_RD_Complete2);
	free(test_ddr_addr_rd);
	
	return;
}

void test_pciex16_mem_rw(int TxDspID)
{
	dspID = sysCpuGetID();
	unsigned char *test_ddr_addr_rd = NULL;
	unsigned char *test_ddr_addr_wr = NULL;
	
	printf("[DSP%d] Test Pciex16 mem read/write Start ...\n",dspID);
	
	if(TxDspID == dspID)//发送端
	{
		sleep(5);//要先启动接收端
		
		semID_PCIE_MEM_WR_Complete = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
		semID_PCIE_MEM_WR_Complete2 = semBCreate(SEM_Q_PRIORITY, SEM_FULL);
		
		test_ddr_addr_wr = (unsigned char *)malloc(mem_size);
		if(test_ddr_addr_wr == NULL)
		{
			printf("<ERROR> [%s:_%d_]:: test_ddr_addr_wr malloc failed\n",__FUNCTION__,__LINE__);
			return;
		}
		printf("<**DEBUG**> [%s:_%d_]:: test_ddr_addr_wr = 0x%lx\n",__FUNCTION__,__LINE__,test_ddr_addr_wr);
		memset(test_ddr_addr_wr, 0, mem_size);
		
		if(dspID == 0) //ep
		{
			bslPcieRecInit(test_pciex16_msi_callback_send, NULL,NULL,NULL);
		}
		else if(dspID == 1)//rc
		{
			bslPcieRecInit(test_pciex16_msi_callback_send, NULL,NULL,NULL);
		}
		bslTaskCreateCore("tPcieSendFun",120,0,0x200000,(FUNCPTR) tPcieSendFun, 4, (_Vx_usr_arg_t)test_ddr_addr_wr,1,2,3,4,5,6,7,8,9);
		bslTaskCreateCore("tPcieTrigMsiRead",120,0,0x200000,(FUNCPTR) tPcieTrigMsiRead, 5, 0,1,2,3,4,5,6,7,8,9);
	}
	else//接收端
	{
		semID_PCIE_MEM_RD_Complete = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
		semID_PCIE_MEM_RD_Complete2 = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
		
		test_ddr_addr_rd = (unsigned char *)malloc(mem_size);
		if(test_ddr_addr_rd == NULL)
		{
			printf("<ERROR> [%s:_%d_]:: test_ddr_addr_rd malloc failed\n",__FUNCTION__,__LINE__);
			return;
		}
		printf("<**DEBUG**> [%s:_%d_]:: test_ddr_addr_rd = 0x%lx\n",__FUNCTION__,__LINE__,test_ddr_addr_rd);
		memset(test_ddr_addr_rd, 0, mem_size);
		
		if(dspID == 0) //ep
		{
			bslPcieRecInit(test_pciex16_msi_callback_recv, ep_ob_addr, test_ddr_addr_rd, mem_size);
		}
		else if(dspID == 1)//rc
		{
			bslPcieRecInit(test_pciex16_msi_callback_recv, rc_ib_addr, test_ddr_addr_rd, mem_size);
		}
		bslTaskCreateCore("tPcieRecvFun",120,0,0x200000,(FUNCPTR) tPcieRecvFun, 4, (_Vx_usr_arg_t)test_ddr_addr_rd,1,2,3,4,5,6,7,8,9);
		bslTaskCreateCore("tPcieTrigMsiWrite",120,0,0x200000,(FUNCPTR) tPcieTrigMsiWrite, 5, 0,1,2,3,4,5,6,7,8,9);
	}

	return;
}
