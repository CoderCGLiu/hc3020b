#include "../basic/testing.h" 

void testing_SemCCreate()
{
	bmSemCreate (BM_SEM_C);

}

void testing_SemCDelete()
{
	bmSemDelete (BM_SEM_C);

}

void testing_SemCFlush()
{
	bmSemFlush (BM_SEM_C);

}

void testing_SemCGiveQEmpty()
{
	bmSemGiveQEmpty (BM_SEM_C);

}

void testing_SemCGiveQFull()
{
	bmSemGiveQFull(BM_SEM_C);
}

void testing_SemCGiveTake()
{ 
	bmSemGiveTake(BM_SEM_C);
}

void testing_SemCTakeEmpty()
{ 
	bmSemTakeEmpty(BM_SEM_C);
}

void testing_SemCTakeFull()
{
	bmSemTakeFull(BM_SEM_C);
}

void testing_semCTest()
{
	printf("----------------semCTest-----------------------\n");
	testing_SemCCreate();
	
	testing_SemCDelete();
	
	testing_SemCFlush();
	
	testing_SemCGiveQEmpty();
	
	testing_SemCGiveQFull();
	
	testing_SemCGiveTake();
	
	testing_SemCTakeEmpty();
	
	testing_SemCTakeFull();

	
}
