#include "../basic/testing.h" 

static u64 taskSpawn_test[TESTING_COUNT]; 
static u64 taskActivate_test[TESTING_COUNT]; 
static u64 taskSpawnDelete_test[TESTING_COUNT]; 
static u64 taskDelete_test[TESTING_COUNT]; 

static u64 taskSuspendReady_test[TESTING_COUNT]; 
static u64 taskSuspendPend_test[TESTING_COUNT]; 
static u64 taskSuspendSusp_test[TESTING_COUNT]; 
static u64 taskSuspendDelay_test[TESTING_COUNT]; 

static u64 taskResumeReady_test[TESTING_COUNT]; 
static u64 taskResumePend_test[TESTING_COUNT]; 
static u64 taskResumeSusp_test[TESTING_COUNT]; 
static u64 taskResumeDelay_test[TESTING_COUNT]; 

static u64 taskLock_test[TESTING_COUNT];
static u64 taskUnlock_test[TESTING_COUNT];
static u64 taskUnlockNoLock_test[TESTING_COUNT];
 
#ifdef __multi_core__
#define taskLock taskCpuLock
#define taskUnlock taskCpuLock
#endif

static void bmTaskDummy (int *sync_ptr)
{
	if (sync_ptr)
	{
		*sync_ptr = 1;
	}
	
    while (1)
        ;
}

/******************************************************************
*
* bmTaskPend - function that is used as a task that is pended 
*
* This function acts as a pended task to aid the task benchmarks.
*
* RETURNS: N/A
*
*/
static void bmTaskPend (SEM_ID sem, int *sync_ptr)
{   
	*sync_ptr = 1;
    semTake (sem, WAIT_FOREVER);
}


/*******************************************************************
*
* bmTaskSuspend - function that is used as a task that is suspended
*
* This function acts as a suspended task to aid the task benchmarks.
*
* RETURNS: N/A
*
*/
static void bmTaskSuspend (int *sync_ptr)
{
	*sync_ptr = 1;
    while (1)
        {  
        taskSuspend (0);
        }
}



/*****************************************************************
*
* bmTaskDelay - function that is used as a task that is delayed 
*
* This function acts as a delayed task to aid the task benchmarks.
*
* RETURNS: N/A
*
*/
static void bmTaskDelay (int *sync_ptr)
{
	*sync_ptr = 1;
    while (1)
       {  

       /* 100000/60 approximately 1666 s / 27 m */  
       taskDelay (100000);
       }
}
void testing_taskActivate()
{
    int i; 
    int level; 
    u64 a, b, c ;  
    int priority;
    WIND_TCB tcb;
    char stack[0x2000];
    char *stack_ptr = stack + 0x1000;
    int ret;

    /* Make sure that this task's priority is higher that the one that is
     * going to be spawned.
     */
    taskPriorityGet (0, &priority);

    tick_set(0); 
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		level = int_lock();
		
		ret = taskInit(&tcb, "bmTaskInit", priority + 1, VX_NO_STACK_FILL, 
				     stack_ptr, 0x1000, (FUNCPTR) bmTaskDummy,0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	 
		a = user_timestamp ();
		b = user_timestamp ();
		
		taskActivate ((int)&tcb);
		
		c = user_timestamp ();

		int_unlock(level); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskActivate_test[i] = (c - b) - (b - a);			 
		}

		taskDelete ((int)&tcb);
	} 
    
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("taskActivate",taskActivate_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskActivate",taskActivate_test, TESTING_COUNT, 0);
#endif
 
}
void testing_taskSpawn() 
{
	int i;
	u64		a, b, c ;  
    int level;  
	int task;  
	int priority;
#ifdef __multi_core__  
	cpuset_t affinity;
    
	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 0);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */ 
    taskPriorityGet (0, &priority);

    tick_set(0);
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
	
        task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
	                   0x1000, (FUNCPTR)bmTaskDummy,
			   0,0,0,0,0,0,0,0,0,0 );

        c = user_timestamp ();  
		
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSpawn_test[i] = (c - b) - (b - a);			 
		}
		  taskDelete (task);
	} 

#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	taskCpuAffinitySet(0, affinity);
#endif
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("taskSpawn",taskSpawn_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskSpawn",taskSpawn_test, TESTING_COUNT, 0);
#endif
       
}
void testing_taskSpawnDelete() 
{
	int i;
	u64		a, b, c ;  
    int level;  
	int task;  
	int priority; 
#ifdef __multi_core__
	cpuset_t affinity;

	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 0);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
    taskPriorityGet (0, &priority); 
     
    tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		

		level = int_lock();
		a = user_timestamp();
		b = user_timestamp();
	
		task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
						   0x1000, (FUNCPTR)bmTaskDummy,
				   0,0,0,0,0,0,0,0,0,0 ); 
		taskDelete (task);
		
		c = user_timestamp ();
		
		int_unlock(level); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSpawnDelete_test[i] = (c - b) - (b - a);	
		}
	}
	
#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("taskSpawnDelte",taskSpawnDelete_test, TESTING_COUNT, 1);
#else
	 inter_statistical("taskSpawnDelte",taskSpawnDelete_test, TESTING_COUNT, 0);
#endif
}
void testing_taskDelete() 
{
	int i;
	u64		a, b, c ;  
    int level;  
	int task;  
	int priority; 
#ifdef __multi_core__
	cpuset_t affinity;
	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 0);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */

    taskPriorityGet (0, &priority); 
     
    tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		

		level = int_lock();
		
		task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
							   0x1000, (FUNCPTR)bmTaskDummy,
					   0,0,0,0,0,0,0,0,0,0 );
		
		a = user_timestamp();
		b = user_timestamp(); 
			
		taskDelete (task);
		
		c = user_timestamp ();
		
		int_unlock(level); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskDelete_test[i] = (c - b) - (b - a);	
		}
	}
#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("taskDelte",taskDelete_test, TESTING_COUNT, 1);
#else
	 inter_statistical("taskDelte",taskDelete_test, TESTING_COUNT, 0);
#endif
}



void testing_taskSuspendReady()
{
	int i;
	u64		a, b, c ;  
    int level;  
	int task;  
	int priority;
	volatile int sync_var = 0;
#ifdef __multi_core__
	cpuset_t affinity;
	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 0);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
	
	taskPriorityGet (0, &priority);
	  
	/* 创建任务 */
   if ((task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
					   0x1000, (FUNCPTR)bmTaskDummy,
			   &sync_var,0,0,0,0,0,0,0,0,0 )) == ERROR) 
	{
		printf("taskSpawn error\n");
	}

#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 1);
	taskCpuAffinitySet(task, affinity);

	while (sync_var != 1)
		;
#endif /* #ifdef __multi_core__ */
	while (taskIsReady(task) == FALSE)
		;
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskSuspend (task);
		
		c = user_timestamp();	
 
		taskResume (task);
		
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSuspendReady_test[i] = (c - b) - (b - a);			 
		}
	}
	 taskDelete(task);

#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */

	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("taskSuspendReady",taskSuspendReady_test, TESTING_COUNT, 1);
#else
	 inter_statistical("taskSuspendReady",taskSuspendReady_test, TESTING_COUNT, 0);
#endif

}


void testing_taskSuspendPend()
{
	int i;
	u64		a, b, c ;  
	int level;  
	int task; 
	int priority;
	SEM_ID         sem;
	volatile int sync_var = 0; 
    
	taskPriorityGet (0, &priority);
	  
    /* Create the semaphore that the task is going to pend on. */
   if ((sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY)) == NULL)
	{
	   printf("semBCreate error\n");
	}
   
	/* 创建任务 */
   if ((task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
  	                   0x1000, (FUNCPTR)bmTaskPend, (int)sem,
  	                 (int)&sync_var,0,0,0,0,0,0,0,0 )) == ERROR)
	{
		printf("taskSpawn error\n");
	}
   
   /* Make sure that the new task can run to take the semaphore. */
   taskPrioritySet (0, priority + 2);
	while (sync_var != 1)
		;
	while (taskIsReady(task) == TRUE)
		;
   taskPrioritySet (0, priority);
   
   
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskSuspend (task);
		
		c = user_timestamp();	
 
		taskResume (task);
		
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSuspendPend_test[i] = (c - b) - (b - a);			 
		}
	}
	 taskDelete(task);
	 semDelete (sem); 
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("taskSuspendPend",taskSuspendPend_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskSuspendPend",taskSuspendPend_test, TESTING_COUNT, 0);
#endif

}


void testing_taskSuspendSusp()
{
	int i;
	u64		a, b, c ;  
	int level;  
	int task; 
	int priority;
	volatile int sync_var = 0;

	taskPriorityGet (0, &priority);
	  
	 /* 创建任务 */
	if ((task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
					   0x1000, (FUNCPTR)bmTaskSuspend,
					   &sync_var,0,0,0,0,0,0,0,0,0 )) == ERROR) 
	{
		printf("taskSpawn error\n");
	} 
   
    /* Make sure that the new task can run to suspend itself */
    taskPrioritySet (0, priority + 2);
    
	while (sync_var != 1)
		;
	while (taskIsReady(task) == TRUE)
		;
	
    taskPrioritySet (0, priority);	
    
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		taskSuspend (task);
		
		c = user_timestamp();	
 
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
//		
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
//		taskResume (task);
		
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSuspendSusp_test[i] = ((c - b) - (b - a))/10;			 
		}
	}
	 taskDelete(task); 
	
	/* 测试完成，对测试结果进行统计分析 */	 

#ifdef ANALYSIS
	inter_statistical("taskSuspendSusp",taskSuspendSusp_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskSuspendSusp",taskSuspendSusp_test, TESTING_COUNT, 0);
#endif
}



void testing_taskSuspendDelay()
{
	int i;
	u64		a, b, c ;  
	int level;  
	int task; 
	int priority;
	volatile int sync_var = 0;

	taskPriorityGet (0, &priority);
	  
	 /* 创建任务 */
	if ((task = taskSpawn ("bmTaskb", priority + 1, VX_NO_STACK_FILL,
            0x1000, (FUNCPTR)bmTaskDelay,
            &sync_var,0,0,0,0,0,0,0,0, 0)) == ERROR)
	{
		printf("taskSpawn error\n");
	} 
   
   /* Make sure that the new task can run to take the semaphore. */
	taskPrioritySet (0, priority + 2);
	while (sync_var != 1)
		;
	while (taskIsReady(task) == TRUE)
		;
	taskPrioritySet (0, priority);
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskSuspend (task); 
		
		c = user_timestamp();	
 
		taskResume (task);
		
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskSuspendDelay_test[i] = (c - b) - (b - a);			 
		}
	}
	 taskDelete(task); 
	
	/* 测试完成，对测试结果进行统计分析 */	 

#ifdef ANALYSIS
	inter_statistical("taskSuspendDelay",taskSuspendDelay_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskSuspendDelay",taskSuspendDelay_test, TESTING_COUNT, 0);
#endif
}



void testing_taskResumeReady()
{
	int i;
	int task; 	 
	u64		a, b, c ;  
    int level;  
    int priority;
    volatile int sync_var = 0;
#ifdef __multi_core__
	cpuset_t affinity;
	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 0);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
    
    taskPriorityGet (0, &priority);
    
    if ((task = taskSpawn ("bmTaskb", priority + 1, 0,
  	                   0x1000, (FUNCPTR)bmTaskDummy,
  	                 &sync_var,0,0,0,0,0,0,0,0,0 )) == ERROR)
  	{
    	printf("taskSpawn error\n");
  	}

#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity, 1);
	taskCpuAffinitySet(task, affinity);
	while (sync_var != 1)
		;
#endif /* #ifdef __multi_core__ */
	while (taskIsReady(task) == FALSE)
		;
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskResumeReady_test[i] = ((c - b) - (b - a))/5;			 
		}
	}
 
	taskDelete(task);

#ifdef __multi_core__
	CPUSET_ZERO(affinity);
	taskCpuAffinitySet(0, affinity);
#endif /* #ifdef __multi_core__ */
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("taskResumeReady",taskResumeReady_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskResumeReady",taskResumeReady_test, TESTING_COUNT, 0);
#endif

}



void testing_taskResumePend()
{
	int i;
	int task; 	 
	u64		a, b, c ;  
    int level;  
    int priority;
    SEM_ID         sem; 
    volatile int sync_var = 0;   
       
    taskPriorityGet (0, &priority);
    
    if ((sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY)) == NULL)
	{
    	printf("semBCreate error\n");
	}
    
    if ((task = taskSpawn ("bmTaskb", priority + 1, 0,
            0x1000, (FUNCPTR)bmTaskPend, (int)sem,
            (int)&sync_var,0,0,0,0,0,0,0,0 )) == ERROR)
  	{
    	printf("taskSpawn error\n");
  	}
    /* Make sure that the new task can run to take the semaphore. */
	taskPrioritySet (0, priority + 2);
	while (sync_var != 1)
		;
	while (taskIsReady(task) == TRUE)
		;
		
	taskPrioritySet (0, priority);
 
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskResumePend_test[i] = ((c - b) - (b - a))/5;			 
		}
	}
 
	taskDelete(task);
	semDelete (sem); 
	  
	  
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("taskResumePend",taskResumePend_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskResumePend",taskResumePend_test, TESTING_COUNT, 0);
#endif

}

void testing_taskResumeSusp()
{
	int i;
	int task; 	 
	u64		a, b, c ;  
    int level;  
    int priority;
    volatile int sync_var = 0;
    
    taskPriorityGet (0, &priority);
    
    if ((task = taskSpawn ("bmTaskb", priority + 1, 0,
            0x1000, (FUNCPTR)bmTaskSuspend,
            &sync_var,0,0,0,0,0,0,0,0, 0 )) == ERROR)
  	{
    	printf("taskSpawn error\n");
  	}

    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		
		 /* Make sure that the new task can run to suspend itself */
		taskPrioritySet (0, priority + 2);
		while (taskIsReady(task) == TRUE)
			;
		taskPrioritySet (0, priority);
		        
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskResume (task);	 
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskResumeSusp_test[i] = (c - b) - (b - a);			 
		}
	}
 
	taskDelete(task);
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("taskResumeSusp",taskResumeSusp_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskResumeSusp",taskResumeSusp_test, TESTING_COUNT, 0);
#endif

}


void testing_taskResumeDelay()
{
	int i;
	int task; 	 
	u64		a, b, c ;  
    int level;  
    int priority;
    volatile int sync_var = 0;
    
    taskPriorityGet (0, &priority);
    
    if ((task = taskSpawn ("bmTaskb", priority + 1, 0,
            0x1000, (FUNCPTR)bmTaskDelay,
            &sync_var,0,0,0,0,0,0,0,0, 0 )) == ERROR)
  	{
    	printf("taskSpawn error\n");
  	}
    /* Make sure that the new task can run to "sleep" */
    taskPrioritySet (0, priority + 2);
	while (sync_var != 1)
		;
	while (taskIsReady(task) == TRUE)
		;
    taskPrioritySet (0, priority);
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		taskResume (task);
		taskResume (task);	
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskResumeDelay_test[i] = ((c - b) - (b - a))/5;			 
		}
	}
 
	taskDelete(task);
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("taskResumeDelay",taskResumeDelay_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskResumeDelay",taskResumeDelay_test, TESTING_COUNT, 0);
#endif

}

void testing_taskLock()
{
	int i;
	u64		a, b, c ;  
    int level;   
    volatile int ret;
	 
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		ret = taskLock(); 
		
		c = user_timestamp();	
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
		taskUnlock(); 
	
		int_unlock(level); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskLock_test[i] = ((c - b) - (b - a))/10;			 
		}
	}
  
	/* 测试完成，对测试结果进行统计分析 */	 
	
	#ifdef ANALYSIS
		inter_statistical("taskLock",taskLock_test, TESTING_COUNT, 1);
	#else
		inter_statistical("taskLock",taskLock_test, TESTING_COUNT, 0);
	#endif

}


void testing_taskUnlock()
{
	int i; 
			 
	u64		a, b, c ;  
    int level;   
    volatile int ret;
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		level = int_lock();
		
		taskLock(); 
		taskLock(); 
		taskLock(); 
		taskLock(); 
		taskLock(); 
			
		a = user_timestamp();
		b = user_timestamp();
		
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskUnlock_test[i] = ((c - b) - (b - a))/5;			 
		}
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("taskUnlock",taskUnlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskUnlock",taskUnlock_test, TESTING_COUNT, 0);
#endif

}

void testing_taskUnlockNoLock()
{
	int i; 		 
	u64		a, b, c ;  
    int level;   
    
    volatile int ret;
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		level = int_lock(); 
			
		a = user_timestamp();
		b = user_timestamp();
		
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock();  
		
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		ret = taskUnlock(); 
		
		
		c = user_timestamp();	
		
		int_unlock(level);  
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			taskUnlockNoLock_test[i] = ((c - b) - (b - a))/10;			 
		}
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("taskUnlockNoLock",taskUnlockNoLock_test, TESTING_COUNT, 1);
#else
	inter_statistical("taskUnlockNoLock",taskUnlockNoLock_test, TESTING_COUNT, 0);
#endif
}

void testing_taskTest()
{
	
	testing_taskSpawn();
	
#ifndef REWORKS_TEST
	/*** Reworks不支持 **/
	/* testing_taskActivate(); */
#endif
	
	testing_taskSpawnDelete();
	
	testing_taskDelete();
	
	/* time taskSuspend on a ready task. */
	testing_taskSuspendReady(); 
	
	/* time taskSuspend on a pended task. */
	testing_taskSuspendPend(); 
	/* time taskSuspend on a suspended task */
	testing_taskSuspendSusp(); 
	/* time taskSuspend on a delayed task. */
	testing_taskSuspendDelay(); 
	/*time taskResume on a ready task.*/
	testing_taskResumeReady(); 
	/*time taskResume on a pended task.*/
	testing_taskResumePend(); 
	/* time taskResume on a suspended task */
	testing_taskResumeSusp(); 
	/* time taskResume on a delayed task. */
	testing_taskResumeDelay(); 
	

	testing_taskLock(); 
	testing_taskUnlock(); 
	testing_taskUnlockNoLock();  
	
}
