#include "../basic/testing.h" 

void testing_SemBCreate()
{
	bmSemCreate (BM_SEM_B);

}

void testing_SemBDelete()
{
	bmSemDelete (BM_SEM_B);

}

void testing_SemBFlush()
{
	bmSemFlush (BM_SEM_B);

}

void testing_SemBGiveQEmpty()
{
	bmSemGiveQEmpty (BM_SEM_B);

}

void testing_SemBGiveQFull()
{
	bmSemGiveQFull(BM_SEM_B);
}

void testing_SemBGiveTake()
{
	bmSemGiveTake(BM_SEM_B);
}

void testing_SemBTakeEmpty()
{ 
	bmSemTakeEmpty(BM_SEM_B); 
}

void testing_SemBTakeFull()
{
	bmSemTakeFull(BM_SEM_B);
}
void testing_semBTest()
{
	printf("----------------semBTest-----------------------\n");
	testing_SemBCreate();
	
	testing_SemBDelete();
	
	testing_SemBFlush();
	
	testing_SemBGiveQEmpty();
	
	testing_SemBGiveQFull();
	
	testing_SemBGiveTake();
	
	testing_SemBTakeEmpty();
	
	testing_SemBTakeFull();

	
}
