#include "../basic/testing.h" 


static SEM_ID  sem;
static u64 bmSemTakeEmpty_test[TESTING_COUNT];
void testing_SemMCreate()
{ 
	bmSemCreate (BM_SEM_M);  

}

void testing_SemMDelete()
{ 
	bmSemDelete (BM_SEM_M); 

}

 


void testing_SemMGiveQEmpty()
{ 
	bmSemGiveQEmpty (BM_SEM_M); 

}

void testing_SemMGiveQFull()
{ 
	bmSemGiveQFull(BM_SEM_M); 
}

void testing_SemMGiveTake()
{ 
	bmSemGiveTake(BM_SEM_M);
}

void testing_SemMTakeEmptyOwn()
{
	int i;
	u64		a, b, c ;  
    int level;    
    tick_set(0); 
    
    sem = semMCreate (SEM_Q_FIFO); 
    semTake (sem, NO_WAIT);
    
    for(i = 0; i < TESTING_COUNT; i++) {
       	level = int_lock();
       	
       	a = user_timestamp ();
   		b = user_timestamp ();
   		
   		semTake (sem, NO_WAIT);
   		
   		c = user_timestamp ();
       		
   		int_unlock(level);
   		
   	 
   		/* 有效性检测 */
   		if(tick_get() > 0) {			
   			i--; /* 产生了时钟中断，测量无效 */	
   			tick_set(0);			
   		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
   			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
   		} else {
   			bmSemTakeEmpty_test[i] = (c - b) - (b - a);			 
   		}
       }
       	
       semDelete (sem);	
   	
   	/* 测试完成，对测试结果进行统计分析 */	 
   #ifdef ANALYSIS
   	inter_statistical("SemMTakeEmpty",bmSemTakeEmpty_test, TESTING_COUNT, 1);
   #else
   	inter_statistical("SemMTakeEmpty",bmSemTakeEmpty_test, TESTING_COUNT, 0);
   #endif
}


void testing_SemMTakeFull()
{
	bmSemTakeFull(BM_SEM_M);
}
void testing_semMTest()
{
	
	printf("----------------semMTest-----------------------\n");
	testing_SemMCreate();
	
	testing_SemMDelete();
	 
	
	testing_SemMGiveQEmpty();
	
	testing_SemMGiveQFull();//
	
	testing_SemMGiveTake();
	
	testing_SemMTakeEmptyOwn();//
	
	testing_SemMTakeFull();

	
}
