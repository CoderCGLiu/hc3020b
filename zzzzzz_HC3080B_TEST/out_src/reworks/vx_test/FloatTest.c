#include "../basic/testing.h"
#include <math.h>
/* 测试结果 */
static u64 acos_Test[TESTING_COUNT];
static u64 asin_Test[TESTING_COUNT];
static u64 atan_Test[TESTING_COUNT];
static u64 atan2_Test[TESTING_COUNT];
static u64 cos_Test[TESTING_COUNT];
static u64 cosh_Test[TESTING_COUNT];
static u64 sin_Test[TESTING_COUNT];
static u64 sinh_Test[TESTING_COUNT];
static u64 tan_Test[TESTING_COUNT];
static u64 tanh_Test[TESTING_COUNT];
static u64 ceil_Test[TESTING_COUNT];
static u64 exp_Test[TESTING_COUNT];
static u64 fabs_Test[TESTING_COUNT];
static u64 floor_Test[TESTING_COUNT];
static u64 fmod_Test[TESTING_COUNT];
static u64 frexp_Test[TESTING_COUNT];
static u64 ldexp_Test[TESTING_COUNT];
static u64 log_Test[TESTING_COUNT];
static u64 log10_Test[TESTING_COUNT];
static u64 modf_Test[TESTING_COUNT];
static u64 pow_Test[TESTING_COUNT];
static u64 sqrt_Test[TESTING_COUNT];

#define  DOP1 3.14151
#define  DOP2 2.71828 
#define  DOP3 0.5


void acos_float_test(void) {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP3 ;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);
		acos(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			acos_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("acos",acos_Test, TESTING_COUNT, 1);
#else
	inter_statistical("acos", acos_Test, TESTING_COUNT, 0);
#endif 

}
void asin_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP3;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);
		asin(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			asin_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("asin",asin_Test, TESTING_COUNT, 1);
#else
	inter_statistical("asin", asin_Test, TESTING_COUNT, 0);
#endif

}
void atan_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);
		atan(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			atan_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("atan",atan_Test, TESTING_COUNT, 1);
#else
	inter_statistical("atan", atan_Test, TESTING_COUNT, 0);
#endif

}

void atan2_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;
	double DOP1_inter = DOP1;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);
		atan2(DOP1_inter, DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			atan2_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("atan2",atan2_Test, TESTING_COUNT, 1);
#else
	inter_statistical("atan2", atan2_Test, TESTING_COUNT, 0);
#endif
}

void cos_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);
		cos(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);
		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			cos_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("cos",cos_Test, TESTING_COUNT, 1);
#else
	inter_statistical("cos", cos_Test, TESTING_COUNT, 0);
#endif
}

void cosh_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);
		cosh(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			cosh_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("cosh",cosh_Test, TESTING_COUNT, 1);
#else
	inter_statistical("cosh", cosh_Test, TESTING_COUNT, 0);
#endif		

}

void sin_float_test() {
	int i;

	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);
		sin(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sin_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("sin",sin_Test, TESTING_COUNT, 1);
#else
	inter_statistical("sin", sin_Test, TESTING_COUNT, 0);
#endif			

}

void sinh_float_test() {
	int i;

	u64 a;
	u64 b;
	u64 c;
	int intLevel;
	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);
		sinh(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sinh_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("sinh",sinh_Test, TESTING_COUNT, 1);
#else
	inter_statistical("sinh", sinh_Test, TESTING_COUNT, 0);
#endif	

}

void tan_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);
		tan(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			tan_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("tan",tan_Test, TESTING_COUNT, 1);
#else
	inter_statistical("tan", tan_Test, TESTING_COUNT, 0);
#endif		

}

void tanh_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);
		tanh(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			tanh_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("tanh",tanh_Test, TESTING_COUNT, 1);
#else
	inter_statistical("tanh", tanh_Test, TESTING_COUNT, 0);
#endif	

}

void ceil_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);
		ceil(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			ceil_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("ceil",ceil_Test, TESTING_COUNT, 1);
#else
	inter_statistical("ceil", ceil_Test, TESTING_COUNT, 0);
#endif	
}

void exp_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);
		exp(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);
		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			exp_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("exp",exp_Test, TESTING_COUNT, 1);
#else
	inter_statistical("exp", exp_Test, TESTING_COUNT, 0);
#endif		

}

void fabs_float_test() {
	int i, j;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);
		fabs(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			fabs_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("fabs",fabs_Test, TESTING_COUNT, 1);
#else
	inter_statistical("fabs", fabs_Test, TESTING_COUNT, 0);
#endif	
}

void floor_float_test() {
	int i, j;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);
		floor(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			floor_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("floor",floor_Test, TESTING_COUNT, 1);
#else
	inter_statistical("floor", floor_Test, TESTING_COUNT, 0);
#endif	

}

void fmod_float_test() {
	int i, j;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;
	double DOP1_inter = DOP1;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);
		fmod(DOP1_inter, DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);
		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			fmod_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("fmod",fmod_Test, TESTING_COUNT, 1);
#else
	inter_statistical("fmod", fmod_Test, TESTING_COUNT, 0);
#endif	
}

void frexp_float_test() {
	int i;
	volatile int s;

	double DOP2_inter = DOP2;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));
		frexp(DOP2_inter, (int*) (&s));

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			frexp_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("frexp",frexp_Test, TESTING_COUNT, 1);
#else
	inter_statistical("frexp", frexp_Test, TESTING_COUNT, 0);
#endif	
}

void ldexp_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);
		ldexp(DOP2_inter, 13);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			ldexp_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("ldexp",ldexp_Test, TESTING_COUNT, 1);
#else
	inter_statistical("ldexp", ldexp_Test, TESTING_COUNT, 0);
#endif	
}

void log_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP1_inter = DOP1;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);
		log(DOP1_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			log_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("log",log_Test, TESTING_COUNT, 1);
#else
	inter_statistical("log", log_Test, TESTING_COUNT, 0);
#endif	
}

void log10_float_test() {
	int i;

	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();

		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);
		log10(DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			log10_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("log10",log10_Test, TESTING_COUNT, 1);
#else
	inter_statistical("log10", log10_Test, TESTING_COUNT, 0);
#endif	
}

void modf_float_test() {

	int i;
	volatile double d;

	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));
		modf(DOP2_inter, (double*) (&d));

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			modf_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("modf",modf_Test, TESTING_COUNT, 1);
#else
	inter_statistical("modf", modf_Test, TESTING_COUNT, 0);
#endif	
}

void pow_float_test() {
	int i;
	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;
	double DOP1_inter = DOP1;

	tick_set(0);

	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();
		a = user_timestamp();
		b = user_timestamp();
		
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);
		pow(DOP1_inter, DOP2_inter);

		c = user_timestamp();
		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pow_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("pow",pow_Test, TESTING_COUNT, 1);
#else
	inter_statistical("pow", pow_Test, TESTING_COUNT, 0);
#endif	

}

void sqrt_float_test() {
	int i;

	u64 a;
	u64 b;
	u64 c;
	int intLevel;

	double DOP2_inter = DOP2;

	tick_set(0);
	for (i = 0; i < TESTING_COUNT; i++) {
		intLevel = int_lock();

		a = user_timestamp();
		b = user_timestamp();

		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);
		sqrt(DOP2_inter);

		c = user_timestamp();

		int_unlock(intLevel);

		/* 有效性检测 */
		if (tick_get() > 0) {
			i--; /* 产生了时钟中断，测量无效 */
			tick_set(0);
		} else if ((a > b) || (b > c) || (c - b) < (b - a)) {
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sqrt_Test[i] = ((c - b) - (b - a)) / 10;
		}
	}

	/* 测试完成，对测试结果进行统计分析 */
#ifdef ANALYSIS
	inter_statistical("sqrt",sqrt_Test, TESTING_COUNT, 1);
#else
	inter_statistical("sqrt", sqrt_Test, TESTING_COUNT, 0);
#endif	

}

void testing_float() {
	acos_float_test();
	asin_float_test();
	atan_float_test();
	atan2_float_test();
	cos_float_test();
	cosh_float_test();
	sin_float_test();
	sinh_float_test();
	tan_float_test();
	tanh_float_test();
	ceil_float_test();
	exp_float_test();
	fabs_float_test();
	floor_float_test();
	fmod_float_test();
	frexp_float_test();
	ldexp_float_test();
	log_float_test();
	log10_float_test();
	modf_float_test();
	pow_float_test();
	sqrt_float_test();
}
