#include "../basic/testing.h" 
 
static u64 MQCreate_test[TESTING_COUNT];
static u64 MQDelete_test[TESTING_COUNT];
static u64 MQSendNoPend_test[TESTING_COUNT]; 
static u64 MQSendPend_test[TESTING_COUNT]; 
static u64 MQSendQFull_test[TESTING_COUNT]; 
static u64 MQRecvAvail_test[TESTING_COUNT];  
static u64 MQRecvNoAvail_test[TESTING_COUNT];  

static MSG_Q_ID	      queue;
#define	BM_MSG_MAX_MSG_LEN	1
#define	BM_MSG_MAX_MESSAGES	1000
static char     bmMsgQBuffer[BM_MSG_MAX_MSG_LEN];


static void bmMsgQReceiveTask( MSG_Q_ID *queue,int *sync_ptr ) 
{
    while (1)
    {
    	*sync_ptr = 1;
    	msgQReceive (*queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, WAIT_FOREVER);
    }
	
}
 

void testing_MQCreate()
{
	int i;
	u64		a, b, c ;  
    int level;    

	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock(); 
	
		a = user_timestamp();
		b = user_timestamp();
		
		/* 打开消息队列  */
	   queue = msgQCreate (BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, 
							MSG_Q_FIFO);
		
		c = user_timestamp();	
		
		if (queue == NULL) {
			printf("testing_MQCreate error\n\n");
		}
		
		msgQDelete (queue);
		int_unlock(level); 		
 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQCreate_test[i] = (c - b) - (b - a);			 
		} 
		
	}
	
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQCreate",MQCreate_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQCreate",MQCreate_test, TESTING_COUNT, 0);
#endif

}

void testing_MQDelete()
{
	int i;
	u64		a, b, c ;  
    int level;    

	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock(); 
	
		
		/* 打开消息队列  */
	   queue = msgQCreate (BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, 
							MSG_Q_FIFO);
	   
		a = user_timestamp();
		b = user_timestamp();
	
		msgQDelete (queue);
		c = user_timestamp();	 
		
		int_unlock(level); 		
 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQDelete_test[i] = (c - b) - (b - a);			 
		} 
		
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQDelete",MQDelete_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQDelete",MQDelete_test, TESTING_COUNT, 0);
#endif

}

void testing_MQSendNoPend()
{
	int i; 	 
	u64		a, b, c ;  
    int level;   
	
	/* 打开消息队列  */
   queue = msgQCreate (BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, 
						MSG_Q_FIFO);

	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
	
		a = user_timestamp();
		b = user_timestamp();
		
		msgQSend (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT,
					  MSG_PRI_NORMAL); 
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQSendNoPend_test[i] = (c - b) - (b - a);			 
		}
	}
	 msgQDelete (queue);
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQSendNoPend",MQSendNoPend_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQSendNoPend",MQSendNoPend_test, TESTING_COUNT, 0);
#endif

}

void testing_MQSendPend()
{
	int i; 	 
	u64		a, b, c ;  
    int level;   
	 
    int               msgQReceiveTask;
    int               priority;
    
    volatile int sync_var = 0;  
    
    taskPriorityGet (0, &priority);

	/* 打开消息队列  */
    queue = msgQCreate (BM_MSG_MAX_MESSAGES,
			     BM_MSG_MAX_MSG_LEN,
			     MSG_Q_FIFO); 
    
    /* Spawn the task to read the queue
     * at a lower priority than this so that it will not run yet.
     */
    msgQReceiveTask =  taskSpawn ("msgQTask", priority + 1, VX_NO_STACK_FILL, 	0x1000,
                                 (FUNCPTR)bmMsgQReceiveTask, (int)(&queue),
                                 (int)&sync_var, 0, 0, 0, 0, 0, 0, 0, 0);
    
 
    if (msgQReceiveTask  == ERROR)
	{
		msgQDelete (queue);
		printf("taskSpawn msgQReceiveTask error\n\n");
	}

	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		/* Set our priority lower than the task.  It will run, read the
		 * queue and then block before we return from the taskPrioritySet call.
		 */ 
		
		/* The task is pending in its msgQReceive call.  Set our priority to
		 * higher than the task.  Measure the msgQSend(). 
		 */
		
		taskPrioritySet (0, priority + 2);
		 while (sync_var != 1)
			;
		while (taskIsReady(msgQReceiveTask) == TRUE)
			;
			
		taskPrioritySet (0, priority); 
		
		a = user_timestamp();
		b = user_timestamp();
		
		msgQSend (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT,
				  MSG_PRI_NORMAL); 
		
		c = user_timestamp();	
		
		int_unlock(level);  
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQSendPend_test[i] = (c - b) - (b - a);			 
		}
	}
	 msgQDelete (queue);
	 taskDelete(msgQReceiveTask);
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQSendPend",MQSendPend_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQSendPend",MQSendPend_test, TESTING_COUNT, 0);
#endif

}



void testing_MQSendQFull()
{
	int i; 	 
	u64		a, b, c ;  
    int level; 
    int counter;
	
    /* Create the queue and fill it. */
    if ((queue = msgQCreate (BM_MSG_MAX_MESSAGES,
			     BM_MSG_MAX_MSG_LEN,
			     MSG_Q_FIFO)) == NULL)
	{
    	printf("msgQCreate error\n\n");
	}

    for (counter = 0; counter < BM_MSG_MAX_MESSAGES; counter++)
	{
    	msgQSend (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT,
		  MSG_PRI_NORMAL);
	}
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
	
		a = user_timestamp();
		b = user_timestamp();
		
		msgQSend (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT,
			  MSG_PRI_NORMAL);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQSendQFull_test[i] = (c - b) - (b - a);			 
		}
	}
	 msgQDelete (queue);
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQSendQFull",MQSendQFull_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQSendQFull",MQSendQFull_test, TESTING_COUNT, 0);
#endif

}
void testing_MQRecvAvail()
{
	int i;
	u64		a, b, c ;  
    int level;  
    
	/* 打开消息队列  */
   queue = msgQCreate (BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, 
						MSG_Q_FIFO);
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		msgQSend (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT,
							  MSG_PRI_NORMAL); 
		
		a = user_timestamp();
		b = user_timestamp();
		
		msgQReceive (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQRecvAvail_test[i] = (c - b) - (b - a);			 
		}
	}
 
	msgQDelete(queue);
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQRecvAvail",MQRecvAvail_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQRecvAvail",MQRecvAvail_test, TESTING_COUNT, 0);
#endif

}
 
void testing_MQRecvNoAvail()
{
	int i;
	u64		a, b, c ;  
	int level;  
	
	/* 打开消息队列  */
   queue = msgQCreate (BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, 
						MSG_Q_FIFO);
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		msgQReceive (queue, bmMsgQBuffer, BM_MSG_MAX_MSG_LEN, NO_WAIT);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			MQRecvNoAvail_test[i] = (c - b) - (b - a);			 
		}
	}
	 
	msgQDelete(queue);
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("MQRecvNoAvail",MQRecvNoAvail_test, TESTING_COUNT, 1);
#else
	inter_statistical("MQRecvNoAvail",MQRecvNoAvail_test, TESTING_COUNT, 0);
#endif
}

void testing_MQTest()
{
	
	testing_MQCreate();
	testing_MQDelete();
	testing_MQSendNoPend();
	testing_MQSendPend();
	testing_MQSendQFull();
	testing_MQRecvAvail();
	testing_MQRecvNoAvail();
}
