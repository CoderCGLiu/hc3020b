#include "../basic/testing.h" 

//static WDOG_ID WatchDogId; 
static int ut_func_run = 0;
static u64 WdCreate_test[TESTING_COUNT]; 
static u64 WdStart_test[TESTING_COUNT]; 
static u64 WdCancel_test[TESTING_COUNT];  

static void ut_func(int arg)
{
	ut_func_run ++;
} 

void testing_wdStartQEmpty()
{
	volatile WDOG_ID WatchDogId;
	int i;
	int ret; 		 
	u64		a, b, c ;  
    int level;   
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		if((WatchDogId = wdCreate()) == NULL)
		{
			printf("wd create err\n");
			return ; 
		}
		/* Wait for that watchdog to expire. */
//	    taskDelay (2);
	    
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		ret = wdStart (WatchDogId,2, (FUNCPTR)ut_func,0);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		wdDelete(WatchDogId);
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			WdStart_test[i] = (c - b) - (b - a);			 
		}
	}

	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("wdStartQEmpty",WdStart_test, TESTING_COUNT, 1);
#else
	inter_statistical("wdStartQEmpty",WdStart_test, TESTING_COUNT, 0);
#endif

}

void testing_wdStartQFull()
{
	volatile WDOG_ID WatchDogId;
	int i;
	int ret; 		 
	u64		a, b, c ;  
    int level;   
    WDOG_ID  firstWdId;
    
    /* Start a watchdog so that the watchdog tick queue has something in it. */
    if ((firstWdId = wdCreate ()) == NULL)
    {
    	printf("wd create err firstWdId\n");
    	return ;
    }
    wdStart (firstWdId,2, (FUNCPTR)ut_func,0);
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		if((WatchDogId = wdCreate()) == NULL)
		{
			printf("wd create err\n");
			return ; 
		}
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		ret = wdStart (WatchDogId,2, (FUNCPTR)ut_func,0);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		
		wdDelete(WatchDogId);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			WdStart_test[i] = (c - b) - (b - a);			 
		}
		
	}
 
	wdDelete(firstWdId);
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("wdStartQFull",WdStart_test, TESTING_COUNT, 1);
#else
	inter_statistical("wdStartQFull",WdStart_test, TESTING_COUNT, 0);
#endif

}

void testing_wdCreate()
{
	volatile WDOG_ID WatchDogId;
	int i;
	int ret; 		 
	u64		a, b, c ;  
    int level;   
  
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
	    WatchDogId = wdCreate(); 
		
		c = user_timestamp();	
		
		int_unlock(level); 	 
		
		wdDelete(WatchDogId);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			WdCreate_test[i] = (c - b) - (b - a);			 
		}
	}
 
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("WdCreate",WdCreate_test, TESTING_COUNT, 1);
#else
	inter_statistical("WdCreate",WdCreate_test, TESTING_COUNT, 0);
#endif

}
void testing_WdCancel()
{
	volatile WDOG_ID WatchDogId;
	int i;
	int ret; 		 
	u64		a, b, c ;  
    int level;  
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		if((WatchDogId = wdCreate()) == NULL)
		{
			printf("wd create err\n");
			return ; 
		}
		
		wdStart (WatchDogId,2, (FUNCPTR)ut_func,0);
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		ret =  wdCancel (WatchDogId);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		wdDelete(WatchDogId);
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			WdCancel_test[i] = (c - b) - (b - a);			 
		}
	}
 

	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("WdCancel",WdCancel_test, TESTING_COUNT, 1);
#else
	inter_statistical("WdCancel",WdCancel_test, TESTING_COUNT, 0);
#endif

}

void testing_WdDel(BOOL start)
{
	volatile WDOG_ID WatchDogId;
	int i;
	int ret; 		 
	u64		a, b, c ;  
	int level;  
	
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		if((WatchDogId = wdCreate()) == NULL)
		{
			printf("wd create err\n");
			return ; 
		} 
		if(start == TRUE)
		{
			wdStart (WatchDogId,2, (FUNCPTR)ut_func,0);
		}
		
		level = int_lock();
		
		a = user_timestamp();
		b = user_timestamp();
		
		wdDelete(WatchDogId);
		
		c = user_timestamp();	
		
		int_unlock(level); 		
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			WdCancel_test[i] = (c - b) - (b - a);			 
		}
	}

	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	if(start == TRUE)
	{
	    inter_statistical("WdDelStarted",WdCancel_test, TESTING_COUNT, 1);
	}
	else
	{
		inter_statistical("WdDelNotStarted",WdCancel_test, TESTING_COUNT, 1);
	}
#else
	if(start == TRUE)
	{
	    inter_statistical("WdDelStarted",WdCancel_test, TESTING_COUNT, 0);
	}
	else
	{
		inter_statistical("WdDelNotStarted",WdCancel_test, TESTING_COUNT, 0);
	}
#endif
}

void testing_WdDelStarted()
{
    testing_WdDel(TRUE);   /* start before delete */
}

void testing_WdDelNotStarted()
{
    testing_WdDel(FALSE);   /* don't start before delete */
}
 

void testing_WDTest()
{
	testing_wdCreate();
	testing_wdStartQEmpty();
	testing_wdStartQFull();
	testing_WdDelStarted();
	testing_WdDelNotStarted();
	testing_WdCancel();
}
