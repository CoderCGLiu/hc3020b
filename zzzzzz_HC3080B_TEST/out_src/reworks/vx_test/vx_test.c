#include "../basic/testing.h" 

#ifdef REWORKS_TEST
#include <tty_ioctl.h>
#else
#include <ioctl.h>
#include <ioLib.h>
#endif

void printfHead()
{
	int ret;
	unsigned long long wait_target;
	u32 freq;
	u32 baud_rate;
	int baud_ret;
	
	ret =  printf("----------------------------------------------------------------------------------------------\n");
	ret += printf("|        test name          |  test times|min time(us)|max time(us)|avr time(us)|    std (us)|\n");
	ret += printf("----------------------------------------------------------------------------------------------\n");
	
	freq = user_timestamp_freq();
#ifdef REWORKS_TEST
	baud_ret = ioctl(fileno(stdout), TTY_GET_BAUDRATE, (void *)&baud_rate);
#else
	baud_ret = ioctl(fileno(stdout), FIOBAUDRATE, (void *)&baud_rate);
#endif

	if (baud_ret == -1) {
		sleep(1);
	} else {
		wait_target = user_timestamp();
		wait_target += (unsigned long long) ret * freq / (baud_rate / 10);
		wait_target += freq / 1000;

		while (user_timestamp() < wait_target)
			;
	}
}

void printfEnd()
{
	printf("----------------------------------------------------------------------------------------------\n");
}


void testing_vxworks()
{
	testing_taskTest();

	testing_semBTest();
	testing_semCTest();
	testing_semMTest();
	
	testing_MQTest(); 


	testing_WDTest();
	
	test_mem();
	


}
