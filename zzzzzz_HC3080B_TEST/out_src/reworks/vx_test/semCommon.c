#include "../basic/testing.h" 


static u64 semCreate_test[TESTING_COUNT]; 
static u64 semDelete_test[TESTING_COUNT]; 
static u64 semFlush_test[TESTING_COUNT]; 
static u64 SemGiveQEmpty_test[TESTING_COUNT]; 
static u64 SemGiveQFull_test[TESTING_COUNT];
static u64 SemGiveTake_test[TESTING_COUNT];
static u64 bmSemTakeEmpty_test[TESTING_COUNT];
static u64 SemTakeFull_test[TESTING_COUNT];

static SEM_ID  sem;

static  void bmSemTakeTask(SEM_ID *sem, int *sync_ptr)
{

    while (1)
	{
    	*sync_ptr = 1;
    	semTake (*sem, WAIT_FOREVER);
	}

}

/******************************************************************************
*
* bmSemTakeGiveTask - function to block the current task
*
* RETURNS: N/A
*
* NOMANUAL
*/

static void bmSemTakeGiveTask( SEM_ID *sem,int *sync_ptr)
 {

    while (1)
	{
    	*sync_ptr = 1;
		semTake (*sem, WAIT_FOREVER);
		semGive (*sem);
	}

}
void bmSemCreate( int semType)
{
	int i;
	u64		a, b, c ;  
    int level;   
    
	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
	     switch (semType)
		 {
	    	case BM_SEM_B:
	    	    a = user_timestamp ();
	    	    b = user_timestamp ();
	    	    sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
	    	    c = user_timestamp ();
	    	    break;
	    	    
	    	case BM_SEM_C:
	    	    a = user_timestamp ();
	    	    b = user_timestamp ();
	    	    sem = semCCreate (SEM_Q_FIFO, 0);
	    	    c = user_timestamp ();
	    	    break;
	    	    
	    	case BM_SEM_M:
	    	    a = user_timestamp ();
	    	    b = user_timestamp ();
	    	    sem = semMCreate (SEM_Q_FIFO);
	    	    c = user_timestamp ();
	    	    break;
	    	    
	    	default:
	    	    sem = NULL;
	    	    break;
		 }
		int_unlock(level); 		
		semDelete(sem);
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			semCreate_test[i] = (c - b) - (b - a);			 
		}
	}
 
	
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("semCreate",semCreate_test, TESTING_COUNT, 1);
#else
	inter_statistical("semCreate",semCreate_test, TESTING_COUNT, 0);
#endif

}


void bmSemDelete( int semType)
{
	int i;
	u64		a, b, c ;  
    int level;   
    
	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
	    switch (semType)
	    	{
	    	case BM_SEM_B:
	    	    sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
	    	    break;
	    	    
	    	case BM_SEM_C:
	    	    sem = semCCreate (SEM_Q_FIFO, 0);
	    	    break;
	    	    
	    	case BM_SEM_M:
	    	    sem = semMCreate (SEM_Q_FIFO);
	    	    break;
	    	    
	    	default:
	    	    sem = NULL;
	    	    break;
	    	}
		a = user_timestamp ();
		b = user_timestamp ();
		semDelete (sem);
		c = user_timestamp ();
		int_unlock(level); 		
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			semDelete_test[i] = (c - b) - (b - a);			 
		}
	}
 
	semDelete(sem);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("semDelete",semDelete_test, TESTING_COUNT, 1);
#else
	inter_statistical("semDelete",semDelete_test, TESTING_COUNT, 0);
#endif

}

void bmSemFlush( int semType)
{
	int i;
	u64		a, b, c ;  
    int level;   
    int             priority;
    int             semTakeTask;
    int ret; 
    
    volatile int sync_var = 0; 
    
	/* 测试并记录测试结果*/
	tick_set(0);
	
   switch (semType)
	{
		case BM_SEM_B:
			 sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			break;
			
		case BM_SEM_C:
			sem = semCCreate (SEM_Q_FIFO, 0);
			break; 
			
		default:
			sem = NULL;
			break;
	}
   
	taskPriorityGet (0, &priority); 
	if ((semTakeTask = taskSpawn ("taskB", priority + 1, 0, 0x1000,
					  (FUNCPTR)bmSemTakeTask, (int)(&sem),
					  (int)&sync_var, 0, 0, 0, 0, 0, 0, 0, 0)) == ERROR)	    
	{
		semDelete (sem);
		printf("taskSpawn semTakeTask error\n");
	}
	
	
   for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
	 
	   /* 
	   	 * Set our priority lower than the task.  It will run, take the
	   	 * semaphore and then block before we return from the
	   	 * taskPrioritySet call.
	   	 */ 
	  	/* 
		   	 * The task is pending in its semTake call.  Set our priority to
		   	 * higher than the task.  Measure the semFlush(). 
		   	 */
	    taskPrioritySet (0, priority + 2);
	 	while (sync_var != 1)
	 		;
	 	while (taskIsReady(semTakeTask) == TRUE)
	 		;
	    taskPrioritySet (0, priority);
	   	
	  
		a = user_timestamp ();
		b = user_timestamp ();
		semFlush (sem);
		c = user_timestamp ();
		int_unlock(level); 		
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			semFlush_test[i] = (c - b) - (b - a);			 
		}
	}
   ret = taskDelete (semTakeTask);
   if(ret == ERROR)
   {
	   printf("taskDelete error bmSemFlush\n");
   }
	semDelete(sem);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("semFlush",semFlush_test, TESTING_COUNT, 1);
#else
	inter_statistical("semFlush",semFlush_test, TESTING_COUNT, 0);
#endif

}


void bmSemGiveQEmpty(int semType)
{
	int i;
	u64		a, b, c ;  
    int level;   
    
    tick_set(0);
    
    switch (semType)
   	{
   		case BM_SEM_B:
   			sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
   			break;
   			
   		case BM_SEM_C:
   			sem = semCCreate (SEM_Q_FIFO, 0);
   			break;
   			
   		case BM_SEM_M:
   			sem = semMCreate (SEM_Q_FIFO);
   			if (sem != NULL)
   			{
   				semTake (sem, NO_WAIT);
   			}
   			break;
   			
   		default:
   			sem = NULL;
   			break;
   	}
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	level = int_lock();
    			
		a = user_timestamp ();
		b = user_timestamp ();
		semGive (sem);
		c = user_timestamp ();
		
		semTake (sem,WAIT_FOREVER);
		
		int_unlock(level);
		
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			SemGiveQEmpty_test[i] = (c - b) - (b - a);			 
		}
    	
    }
    semDelete (sem);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("SemGiveQEmpty",SemGiveQEmpty_test, TESTING_COUNT, 1);
#else
	inter_statistical("SemGiveQEmpty",SemGiveQEmpty_test, TESTING_COUNT, 0);
#endif
	
}

void bmSemGiveQFull(int semType)
{
	int i;
	u64		a, b, c ;  
    int level;   
    int             priority;
	int             semTakeGiveTask;
	 volatile int sync_var = 0; 
	 
    tick_set(0);
    
    
    switch (semType)
   	{
   		case BM_SEM_B:
   			sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
   			break;
   			
   		case BM_SEM_C:
   			sem = semCCreate (SEM_Q_FIFO, 0);
   			break;
   			
   		case BM_SEM_M:
   			sem = semMCreate (SEM_Q_FIFO);
   			if (sem != NULL)
   			{
   				semTake (sem, NO_WAIT);
   			}
   			break;

   		default:
   			sem = NULL;
   			break;
   	}
    taskPriorityGet (0, &priority);
        
    if ((semTakeGiveTask =
	 taskSpawn ("semTask", priority + 1, 0, 0x1000, (FUNCPTR)bmSemTakeGiveTask,
			(int)(&sem), (int)&sync_var, 0, 0, 0, 0, 0, 0, 0, 0)) == ERROR)
	{
		semDelete (sem);
		printf("taskSpawn semTakeGiveTask error\n");
	}
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	level = int_lock();
    			
    	/* 
		 * Set our priority lower than the task.  It will run, take the
		 * semaphore and then block before we return from the
		 * taskPrioritySet call.
		 */
		/* 
		 * The task is pending in its semTake call.  Set our priority to
		 * higher than the task.  Measure the semGive(). 
		 */ 
	    taskPrioritySet (0, priority + 2);
	 	while (sync_var != 1)
	 		;
	 	while (taskIsReady(semTakeGiveTask) == TRUE)
	 		;
	    taskPrioritySet (0, priority);
		
		a = user_timestamp ();
		b = user_timestamp ();
		semGive (sem);
		c = user_timestamp ();

		semTake (sem, WAIT_FOREVER);
		
		int_unlock(level);
		
		
		/* 有效性检测 */
		if ((a > b) || (b > c) || (c - b) < (b - a))
		{			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			SemGiveQFull_test[i] = (c - b) - (b - a);			 
		}
    }
    
    taskDelete (semTakeGiveTask);
    semDelete (sem);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("SemGiveQFull",SemGiveQFull_test, TESTING_COUNT, 1);
#else
	inter_statistical("SemGiveQFull",SemGiveQFull_test, TESTING_COUNT, 0);
#endif
	
}


void bmSemGiveTake(int semType)
{
	int i;
	u64		a, b, c ;  
    int level;    
    tick_set(0);    
    
    switch (semType)
   	{
   		case BM_SEM_B:
   			sem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
   			break;
   			
   		case BM_SEM_C:
   			sem = semCCreate (SEM_Q_FIFO, 0);
   			break;
   			
   		case BM_SEM_M:
   			sem = semMCreate (SEM_Q_FIFO);
   			if (sem != NULL)
   			{
   				semTake (sem, NO_WAIT);
   			}
   			break;
   			
   		default:
   			sem = NULL;
   			break;
   	}
    
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	level = int_lock();
    			
    	a = user_timestamp ();
		b = user_timestamp ();
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		semGive (sem);
		semTake (sem, WAIT_FOREVER);
		c = user_timestamp ();
		
		int_unlock(level);
		
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			SemGiveTake_test[i] = ((c - b) - (b - a))/10;			 
		}
    	
    }
    
    semDelete (sem);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("SemGiveTake",SemGiveTake_test, TESTING_COUNT, 1);
#else
	inter_statistical("SemGiveTake",SemGiveTake_test, TESTING_COUNT, 0);
#endif
	
}


void bmSemTakeEmpty (int semType)
{
	int i;
	u64		a, b, c ;  
    int level;    
    
    
    register SEM_ID  sem1;
    register SEM_ID  sem2; 
    register SEM_ID  sem3; 
    register SEM_ID  sem4;
    tick_set(0);
    switch (semType)
   	{
   		case BM_SEM_B:
   		    sem1 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem2 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem3 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem4 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
   			break;
   			
   		case BM_SEM_C:
   			sem1 = semCCreate (SEM_Q_FIFO, 0);
			sem2 = semCCreate (SEM_Q_FIFO, 0);
			sem3 = semCCreate (SEM_Q_FIFO, 0);
			sem4 = semCCreate (SEM_Q_FIFO, 0);
   			break;
   		 
   			
   		default:
   			sem1 = sem2 = sem3 = sem4 = NULL;
   			break;
   	}
    
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	level = int_lock();
    	
    	a = user_timestamp ();
		b = user_timestamp ();
		
		semTake (sem1, NO_WAIT);
		semTake (sem2, NO_WAIT);
		semTake (sem3, NO_WAIT);
		semTake (sem4, NO_WAIT);
		
		c = user_timestamp ();
    		
		int_unlock(level);
		
	/*	semGive(sem1);
		semGive(sem2);
		semGive(sem3);
		semGive(sem4);*/
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			bmSemTakeEmpty_test[i] = ((c - b) - (b - a))/4;			 
		}
    }
    	
    semDelete (sem1);	
    semDelete (sem2);	
    semDelete (sem3);	
    semDelete (sem4);	
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("SemTakeEmpty",bmSemTakeEmpty_test, TESTING_COUNT, 1);
#else
	inter_statistical("SemTakeEmpty",bmSemTakeEmpty_test, TESTING_COUNT, 0);
#endif
	
}


void bmSemTakeFull(int semType)
{
	int i;
	u64		a, b, c ;  
    int level;    
   
    
    register SEM_ID  sem1;
    register SEM_ID  sem2; 
    register SEM_ID  sem3; 
    register SEM_ID  sem4;
    tick_set(0); 
    
    switch (semType)
   	{		case BM_SEM_B:
		   sem1 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem2 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem3 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			sem4 = semBCreate (SEM_Q_FIFO, SEM_EMPTY);
			break;
			
		case BM_SEM_C:
			sem1 = semCCreate (SEM_Q_FIFO, 0);
			sem2 = semCCreate (SEM_Q_FIFO, 0);
			sem3 = semCCreate (SEM_Q_FIFO, 0);
			sem4 = semCCreate (SEM_Q_FIFO, 0);
			break;
			
        case BM_SEM_M:
    	sem1 = semMCreate (SEM_Q_FIFO);
    	sem2 = semMCreate (SEM_Q_FIFO);
    	sem3 = semMCreate (SEM_Q_FIFO);
    	sem4 = semMCreate (SEM_Q_FIFO);
    	if (sem1 != NULL) semTake (sem1, NO_WAIT);
    	if (sem2 != NULL) semTake (sem2, NO_WAIT);
    	if (sem3 != NULL) semTake (sem3, NO_WAIT);
    	if (sem4 != NULL) semTake (sem4, NO_WAIT);
    	break;
			
		default:
			sem1 = sem2 = sem3 = sem4 = NULL;
			break;
   	}
    
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	level = int_lock();
    			
    	semGive (sem1);
    	semGive (sem2);
    	semGive (sem3);
    	semGive (sem4);
    	
    	a = user_timestamp ();
		b = user_timestamp ();
		
		semTake (sem1, NO_WAIT);
		semTake (sem2, NO_WAIT);
		semTake (sem3, NO_WAIT);
		semTake (sem4, NO_WAIT);
		
		c = user_timestamp ();
		
		int_unlock(level); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			SemTakeFull_test[i] = ((c - b) - (b - a))/4;			 
		}
    	
    }
    
    semDelete (sem1);
    semDelete (sem2);
    semDelete (sem3);
    semDelete (sem4);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("SemTakeFull",SemTakeFull_test, TESTING_COUNT, 1);
#else
	inter_statistical("SemTakeFull",SemTakeFull_test, TESTING_COUNT, 0);
#endif
	
}
