/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */
#include "../basic/block_config.h"
#include "../basic/testing.h" 
#ifdef REWORKS_TEST
#include <reworksio.h>
#include <device.h>
#include <cpu.h>
#include <memory.h>
#else
#include <types/vxTypesOld.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/fcntlcom.h>
#endif

/*******************************************************************************
 * 
 * 测试fread接口性能
 * 
 * 		本接口基于处理器时间戳计时。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static double 	rate = 0.0;
static int fread_test_once(BLOCK_SIZE block_size,FSKIND kind )
{
	char *buf;
	u64  	start1;
	u64  	start2;
	u64  	end;
	u64		gettime_time = 0;
	u64		run_time = 0;
	FILE 	*fp;
	int 	ret;
	u32		filesize;
	u32     blocksize = block_size.blocksize;
	struct stat st;
	double 	total_time = 0.0;
	double 	total_seconds = 0.0;
	
	
	BLOCK_TOKEN token = block_size.token;
	
	/* 创建并打开待写入文件 */
	if (0 != stat(READ_FILENAME, &st))
	{
		return -1;
	}
	filesize = st.st_size;
	
	if (NULL == (fp = fopen(READ_FILENAME, "r+")))
	{
		return -1;
	}
	
	/* 准备数据 */
	buf = (char*)malloc(blocksize);
	
	while (1)
	{
		start1 = user_timestamp();
		start2 = user_timestamp();
		
		/* 向文件中写入数据 */
		ret = fread(buf, blocksize, 1, fp); 	
		if(0 >= ret)
		{
			break;
		}
		
		end = user_timestamp();
		
		gettime_time 	+= (start2 - start1);
		run_time 		+= (end - start2);
	}

	fclose(fp);
	
	/* 计算结果 */
	total_time 		= (run_time - gettime_time);		
	total_seconds 	= total_time / user_timestamp_freq();
	rate			= (filesize) / (total_seconds * 1024 * 1024);
	printf("total_seconds = [%f]", total_seconds);
	printf("  rate = %f\n", rate);
	
	/* 记录数据 */
#if 0
	result.opsname 		= "fread";
	result.blocksize 	= blocksize;
	result.filesize 	= filesize;
	result.usingtime 	= total_seconds;
	result.rate 		= rate;
	block_test_record(&result);
#endif
	
	
	
	/* 清理环境 */
	free(buf);
	if(token == END_TOKEN)
		rm(READ_FILENAME);
	
	return 0;
}


/*******************************************************************************
 * 
 * 测试fread接口性能
 * 
 * 		本接口基于处理器时间戳计时。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
int fread_test(FSKIND kind)
{
	int i;
	int iterator = sizeof(blocksize_array) / sizeof(BLOCK_SIZE);
	
	printf("Function %s Executing\n", __FUNCTION__);	
	
	for (i = 0; i < iterator; i++)
	{
		if (0 != fread_test_once(blocksize_array[i], kind) )
		{
			return -1;
		}
		block_result.fread_avg[i] = rate; 
	}
	
	return 0;
}
 
