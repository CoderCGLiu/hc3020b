/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */
 
#include "../basic/block_config.h"
#include "../basic/testing.h" 
#ifdef REWORKS_TEST
#include <reworksio.h>
#include <device.h>
#include <cpu.h>
#include <memory.h>
#else
#include <types/vxTypesOld.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/fcntlcom.h>
#endif

/*******************************************************************************
 * 
 * 测试write接口性能
 * 
 * 		本接口基于处理器时间戳计时。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
static double 	rate = 0.0;
static int write_test_once(BLOCK_SIZE block_size, FSKIND kind)
{
	int  	i; 
	u64  	start1;
	u64  	start2;
	u64  	end;
	u64		gettime_time = 0;
	u64		run_time = 0;
	int 	fd;
	int 	ret;
	u32 	iterators;
	u32     blocksize = block_size.blocksize;
	double 	total_time = 0.0;
	double 	total_seconds = 0.0;
	BLOCK_TOKEN token = block_size.token;

	/* 准备数据 */
	char *buf = (char*)malloc(blocksize);
	memset(buf, 'A', blocksize);

	/* 创建并打开待写入文件 */
	if (-1 == (fd = open(WRITE_FILENAME, O_CREAT | O_RDWR, 0777)))
	{
		free(buf);
		return -1;
	}

	/* 计算执行的写操作次数 */
	iterators = WRITE_FILESIZE / blocksize;	


	for (i = 0; i < iterators; i++)
	{
		start1 = user_timestamp();
		start2 = user_timestamp();

		/* 向文件中写入数据 */
		ret = write(fd, buf, blocksize); 	
		if(ret == -1)
		{
			printf("Error in write,ret = %d\n",ret);
			return -1;
		}

		end = user_timestamp();

		gettime_time 	+= (start2 - start1);
		run_time 		+= (end - start2);
	}

	close(fd);

	/* 计算结果 */
	total_time 		= (double)(run_time - gettime_time);		
	total_seconds 	= (double)(total_time / user_timestamp_freq());
	rate			= (double)((WRITE_FILESIZE) / (total_seconds * 1024 * 1024));
	printf("total_seconds = [%f]", total_seconds);
	printf("  rate = %f\n", rate);

#if 0
	/* 记录数据 */
	result.opsname 		= "write";
	result.blocksize 	= blocksize;
	result.filesize 	= WRITE_FILESIZE;
	result.usingtime 	= total_seconds;
	result.rate 		= rate;
	block_test_record(&result);
#endif
	

	
	/* 清理环境 */
	free(buf);
	if(token != END_TOKEN)
		rm(WRITE_FILENAME);

	return 0;
}


/*******************************************************************************
 * 
 * 测试write接口性能
 * 
 * 		本接口基于处理器时间戳计时。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */


int write_test(FSKIND kind)
{
	int i;
	int iterator = sizeof(blocksize_array) / sizeof(BLOCK_SIZE);	


	printf("Function %s Executing\n", __FUNCTION__);


	for (i = 0; i < iterator; i++)
	{
		write_test_once(blocksize_array[i], kind);
		block_result.write_avg[i] = rate; 
	}


	return 0;
}

