
#include "../basic/block_config.h"
#ifdef FLASH_DEVICE
	 
		



unsigned short FfxHookNTOneNANDReadRegister2(unsigned short uOffset)
{
	unsigned short Value;
	volatile unsigned short *pReg;

    pReg = (volatile unsigned short *)((volatile unsigned short *)0xE8000000 + uOffset);

    Value = *pReg;

    return Value;
}

int FfxHookNTOneNANDReadyWait2()
{
    unsigned short IntStatus;
    int       ffxStat;
    int i = 20;

    IntStatus = 0;


    /* Loop while no interrupt indication from the OneNAND chip's register */
    while (((IntStatus & 0x8000) == 0)
       /* && (i != 0)*/)

    {
//    	udelay(1000);
    	i--;
        IntStatus = FfxHookNTOneNANDReadRegister2(0xF241);
//        printf("IntStatus------1-----0x%x\n", IntStatus);
    }

	/* If there was an interrupt (regardless of whether the timeout has elapsed) */
	/* then the function was successful.  Otherwise, to get here, the timeout    */
    /* must have occurred                                                        */

    /* Read the register again in case the processor was busy between the time the
     *  interrupt occurred and the loop above was able to read it, and therefore
     *  time passed, but the device was indeed ready.
     */
    IntStatus = FfxHookNTOneNANDReadRegister2(0xF241);
//    printf("IntStatus-----------0x%x\n", IntStatus);

	if (IntStatus & 0x8000)
	{
	    ffxStat = 0;
	} 
	else 
	{
	    ffxStat = -1;
	}

    return ffxStat;
}


void FfxHookNTOneNANDWriteRegister22(unsigned short uOffset, unsigned short Value)
{
	 unsigned short volatile *pReg;
	
	 pReg = (unsigned short volatile *)((unsigned short volatile *)0xE8000000 + uOffset);

	 *pReg = Value;
}

/*OneNAND_erase:块擦除操作（这里的块大小是128KB，与bread和bwrite中块大小定义不同）
 * 
 * ulStartBlock  起始块号
 * uBlocks       块数
 * 
 * */
int OneNAND_erase(unsigned int ulStartBlock, unsigned int uBlocks)
{
	unsigned int uBlock;
	unsigned short StartAddress1Val;
	
	int ffxStat;
	int Status;
	
	uBlock = ulStartBlock;
	
	while(uBlocks)
	{
//		printf("OneNAND_erase-------uBlock = 0x%x\n", uBlock);
		
		StartAddress1Val = (uBlock & 0x1ffff);
		
		FfxHookNTOneNANDWriteRegister22(0xF100, StartAddress1Val);
		
		//start erase 
	    /*  Clear interrupts.
	    */
	    FfxHookNTOneNANDWriteRegister22(0xF241, 0);

	    /*  write erase command
	    */
	    FfxHookNTOneNANDWriteRegister22(0xF220, 0x0094);
	    
	    //判断状态
	    ffxStat = FfxHookNTOneNANDReadyWait2();
	    if(ffxStat != 0)
	    {
	    	printf("8641D_OneNAND_erase---------error\n");
	    	printf("OneNAND_erase---2----uBlock = 0x%x\n", uBlock);
	    	return -1;
	    }
	    else
	    {
	    	Status = FfxHookNTOneNANDReadRegister2(0xF240);

			if ((Status & 0x0400) == 0x0400)
			{
				if (Status & 0x2000)
				{
					unsigned short ECCStatus;

					/*  A load error occured. The only situation in which a load error
						can occur without the existence of uncorrectable errors is if
						the OneNAND device gets unexpectedly reset while performing a
						load. FlashFX never does that deliberately, but there are
						unusual situations in hardware that could cause such a thing
						to happen. Therefore, initialize the return status to indicate
						that an unknown hardware status exists, and then try to
						discover if it is an uncorrectable bit error.
					*/
					ffxStat = -1;

					/*  Look for uncorrectable errors.
					*/
					ECCStatus = FfxHookNTOneNANDReadRegister2(0xFF00);
					if (ECCStatus & 0x8888)
						ffxStat = 2;
					if (ECCStatus & 0x2222)
					{
						if (ffxStat == 2)
						{
							printf("8641D_OneNAND_erase------error----1\n");
							ffxStat = -1;
							return -1;
						}
						else
						{
							printf("8641D_OneNAND_erase-----error-----2\n");
							ffxStat = -1;
							return -1;
						}
					}
				}
				else if (Status & 0x1000)
				{
					printf("8641D_OneNAND_erase-----error-----3\n");
					ffxStat = -1;
					return -1;
				}
				else if (Status & 0x0800)
				{
					printf("8641D_OneNAND_erase------error----4\n");
					ffxStat = -1;
					return -1;
				}
				else
				{
					/*  Although OneNAND supports erase suspend, FlashFX does
						suspend an erase operation for OneNAND devices. Therefore
						the only way we can get here is the issuance of an
						invalid command, which would be a software fault:
					*/
					printf("8641D_OneNAND_erase----error------5\n");
					ffxStat = -1;
					return -1;
				}

			}
			else
			{
				ffxStat = 0;
			}
	    }
	    
	    uBlocks--;
	    uBlock++;
	}
	
	return ffxStat;
}
#endif
