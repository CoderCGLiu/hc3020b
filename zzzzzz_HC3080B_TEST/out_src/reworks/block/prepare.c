/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */
#include "../basic/block_config.h"
#include "../basic/testing.h" 
#ifdef REWORKS_TEST
#include <reworksio.h>
#include <device.h>
#include <cpu.h>
#include <memory.h>
#else
#include <types/vxTypesOld.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/fcntlcom.h>

#endif
/**
 * 写测试文件大小 
 */
int WRITE_FILESIZE;
BLOCK_TEST_RESULT	block_result;
/*******************************************************************************
 * 
 * 完成测试前的准备工作
 * 
 * 		主要完成：块设备创建、文件系统挂载
 * 
 * device:   设备名称
 * size:     测试文件的大小(MB)
 * 
 */
void mount_prepare(FSKIND kind, char *block_name, char *block_p1_name,char *mount_c)
{
	if(kind == 0)
	{
		/* 格式化 , 挂载文件系统DOSFS*/
		format("dosfs", block_name);
		mount("dosfs", block_p1_name, mount_c); 
		block_result.fskind = "dosfs";
	}
	else
	{
		/* 格式化 , 挂载文件系统HRFS*/
		format("hrfs", block_name);
		mount("hrfs", block_p1_name, mount_c); 
		block_result.fskind = "hrfs";
	}
	 
}
int block_test_prepare(DEVICE device, FSKIND kind, int size)
{
	switch (device) {
	case ATA:
		printf("****************test ata****************\n");
		
#ifdef REWORKS_TEST
#if (defined(TARGET_LOONGSON2F)||defined(TARGET_PENTIUM)||defined(PPC_8245_PLAT) ||defined(TARGET_LOOGSON3A))
		/* 创建设备 */
//		atadev_create(0, 0, BLKDEV_NAME);  
		
//		mount_prepare(kind,BLKDEV_NAME, BLKDEV_NAME_P1, MP_NAME );
//		block_result.testname = "ATA";
#endif
#endif 
		break;
	case SATA:
		printf("****************test sata****************\n");
		
#ifdef REWORKS_TEST
#if (defined TARGET_LOONGSON2H) ||defined(TARGET_LOOGSON3A)
		/* 创建设备 */
//		satadev_create(0, 0, SATABLKDEV_NAME); 		
//		mount_prepare(kind,SATABLKDEV_NAME, SATABLKDEV_NAME_P1, MP_NAME );
//		block_result.testname = "SATA";
#endif
#endif 
		break;
	case RAM:
		printf("**************test ram ***************\n");
#ifdef REWORKS_TEST
		/* 创建设备 */
		ramdev_create(RAMDEV_NAME,40);		
		mount_prepare(kind,RAMDEV_NAME, RAMDEV_NAME_P1, MP_NAME ); 
		block_result.testname = "RAM";
#else
		/***** 使用内存文件系统，先格式化,
		 * vxworks勾选FOLDER_DOSFS2以及
		 * INCLUDE_DISK_UTIL模块编译通过********/
		dosFsVolFormat("/ram0",0,0);
		block_result.testname = "RAM";
#endif
		break;

	case USB:
		/* 格式化 */
		printf("*************test usb ****************\n");
#ifdef REWORKS_TEST	 
		mount_prepare(kind,USBDEV_NAME, USBDEV_NAME_P1, MP_NAME ); 
		block_result.testname = "USB";
#endif
		break;
	case FLASH:
		printf("************test flash ***************\n");
		kind = 1; //保证为hrfs
		block_result.testname = "FLASH";
		block_result.fskind = "hrfs";
		break;
	}
	
	WRITE_FILESIZE = size * 1024 * 1024;

	return 0;
}
