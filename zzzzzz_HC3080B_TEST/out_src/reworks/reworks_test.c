int reworks_test(void)
{
	/*文件系统功能性能测试*/
	block_test_prepare(4,1,32);
	write_test(1);
	read_test(1);
	sleep(1);
	
	/*C库测试*/
	bcopy_test();
	memcpy_test(); 
	memmove_test();
	memset_test();
	string_Test_short();
	string_Test();
	
	sleep(1);
	
	/*vx接口的功能性能测试*/
//	testing_vxworks(); 
	
	sleep(1);
	/*posix标准接口的功能性能测试*/ 
//	testing_posix();
	return 0;
}
