/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：
 * 修改：
 * 		
 * 
 */
 
 
#include "../basic/testing.h" 

#define MBW_LOOPS		10
static int mbw_loop; 
static int m,n;
static int k = 0 ;
 
static unsigned int test_size[] = {
		32 * 1024 * 1024
};
#define TEST_SIZE sizeof(test_size) / sizeof(unsigned int) 

static double result; 

typedef unsigned long long TIME_TYPE; 

#define TIME_DECLARE unsigned long long start, end;

 
#define TIME_START_GET	\
	start = user_timestamp();
	
#define TIME_END_GET	\
	end = user_timestamp();
 

#define TIME_COMPUTE	\
	do{	\
		result = (double)(end - start) /  user_timestamp_freq();	\
		printf("src + %d , dest + %d ,memsize - %d",m, n, k); \
		printf("  time: %f", result);	\
		printf("  performance: %f\n", (memsize * MBW_LOOPS) / ((1024 * 1024) * result));	\
	}while(0);

  
 


static int memcpy_set(unsigned char *src, unsigned char *dest, unsigned int memsize)
{
	
	TIME_DECLARE;	
	INTER_TEST_RESULT	result_2;	
	TIME_START_GET;
	
	char *memcpy_result;
	
	
	for (mbw_loop = 0; mbw_loop < MBW_LOOPS; mbw_loop++)
	{
		memcpy_result = memcpy(dest, src, memsize);
		
		if(memcpy_result != dest)
		{
			printf("memcpy error mbw_loop = %d  \n",mbw_loop);
		}
	}
	
	TIME_END_GET;
		
	TIME_COMPUTE;
	result_2.testname = "memcpy";
	result_2.avg = (memsize * MBW_LOOPS) / ((1024 * 1024) * result);
	other_test_record(&result_2);
	return 0;		
}


 
static int memcpy_body(unsigned int memsize)
{
	unsigned char *src, *dest;		/* 用于拷贝的缓冲 */

	
	/******
	if (NULL == (src = malloc(memsize))){
		return -1;
	}
	if (NULL == (dest = malloc(memsize))){
		free(src);
		return -1;
	}
	***************/
	
	if (NULL == (src = memalign(32, memsize))){
		return -1;
	}
	if (NULL == (dest = memalign(32, memsize))){
		free(src);
		return -1;
	}
	printf("\n*********  MEMSIZE = %d  MB********\n",memsize/1024/1024);
	
	/****Align****/
	//memcpy_set(src, dest, memsize); 
	
	for(m = 0; m < 8; m++)
	{
		for( n = 0; n < 8; n++)
		{
			if(m < n)
				k = n ;
			else 
				k = m;
			/****Not Align****/			
			memcpy_set(src + m , dest + n, memsize - k);	
		}

	}
		
 
	free(src);
	free(dest);
	
	return 0;	
}


int memcpy_test(void)
{

	int i;
 
	for (i = 0; i < TEST_SIZE; i++){
		memcpy_body(test_size[i]); 
	} 

//	int_unlock(level); 
	 
	return 0;
}
