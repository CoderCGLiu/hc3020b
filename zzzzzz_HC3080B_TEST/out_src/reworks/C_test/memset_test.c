/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：
 * 修改：
 * 		
 * 
 */
 
 
#include "../basic/testing.h" 

#define MBW_LOOPS		10
static int mbw_loop; 
 
static unsigned int test_size[] = {
		32 * 1024 * 1024
};
#define TEST_SIZE sizeof(test_size) / sizeof(unsigned int) 

static double result; 

typedef unsigned long long TIME_TYPE; 

#define TIME_DECLARE unsigned long long start, end;

#define TIME_START_GET	\
	start = user_timestamp();
	
#define TIME_END_GET	\
	end = user_timestamp();

#define TIME_COMPUTE	\
	do{	\
		result = (double)(end - start) /  user_timestamp_freq();	\
		printf("  time: %f", result);	\
		printf("  performance: %f\n", (memsize * MBW_LOOPS) / ((1024 * 1024) * result));	\
	}while(0);

  
 


static int memset_set(unsigned char *src, unsigned int memsize)
{
	
	TIME_DECLARE;	
	INTER_TEST_RESULT	result_2;	
	TIME_START_GET;
		
	char *memset_result;
	
	for (mbw_loop = 0; mbw_loop < MBW_LOOPS; mbw_loop++)
	{
		memset_result = memset(src,'a',memsize);
		
		if(memset_result != src)
		{
			printf("memset error mbw_loop = %d  \n",mbw_loop);
		}
		
	}
	
	TIME_END_GET;
		
	TIME_COMPUTE;
	result_2.testname = "memset";
	result_2.avg = (memsize * MBW_LOOPS) / ((1024 * 1024) * result);
	other_test_record(&result_2);
	return 0;		
}


 
static int memset_body(unsigned int memsize)
{
	unsigned char *src;		 
	/*
	if (NULL == (src = malloc(memsize))){
		return -1;
	}
	*/
	if (NULL == (src = memalign(32, memsize))){
		return -1;
	} 
	printf("\n*********  MEMSIZE = %d  ********\n",memsize/1024/1024);
	
	/****Align****/
	memset_set(src,  memsize);
	
	/****Not Align1****/
	memset_set(src + 1,  memsize - 1);
	
	/****Not Align2****/
	memset_set(src + 2,  memsize - 2);
	
	/****Not Align3****/
	memset_set(src + 3,  memsize - 3);
	
	/****Not Align4****/
	memset_set(src + 4,  memsize - 4);
	
	/****Not Align5****/
	memset_set(src + 5 ,  memsize - 5);
	
	/****Not Align6****/
	memset_set(src + 6,  memsize - 6);
	
	/****Not Align7****/
	memset_set(src + 7,  memsize - 7);
	 
	
	free(src); 
	
	return 0;	
}


int memset_test(void)
{
	
	int i;
    int level;   
    
    level = int_lock();

	for (i = 0; i < TEST_SIZE; i++){
		memset_body(test_size[i]); 
	}

	int_unlock(level); 
	
	return 0;
}
