#include "../basic/testing.h"  
 


/* 测试结果 */
static u64 strcmp_Test[TESTING_COUNT]; 
static u64 strchr_Test[TESTING_COUNT]; 
static u64 strcpy_Test[TESTING_COUNT]; 
static u64 strncpy_Test[TESTING_COUNT]; 
static u64 strcspn_Test[TESTING_COUNT];
static u64 strlen_Test[TESTING_COUNT];
static u64 strpbrk_Test[TESTING_COUNT];


void string_strcmp_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
	char *string_a = "aBcdefaBcdefaBcdefaBcdef";
	char *string_b = "aBcdefaBcdefaBcdefABcdef";
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        ret = strcmp(string_a, string_b);

		c = user_timestamp();
        int_unlock(intLevel);
        
        if (ret != 32) 
		{
			printf("string_strcmp_test -- strcmp error,TESTING_COUNT= %d \n",i);
			return ;
		}
	 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strcmp_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strcmp",strcmp_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strcmp",strcmp_Test, TESTING_COUNT, 0);
#endif 

}

 

void string_strchr_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
	char *str = "skjhdaskj23424gdfg";
	char *t = "3424gdfg";
	char *p;
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
    	p = strchr(str, '3');

		c = user_timestamp();
        int_unlock(intLevel);
        
        ret = strcmp(p, t);
        if (ret != 0) 
		{
			printf("string_strchr_test -- strchr error,TESTING_COUNT= %d \n",i);
			return ;
		}
	 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strchr_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strchr",strchr_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strchr",strchr_Test, TESTING_COUNT, 0);
#endif 

}
void string_strcpy_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
	char string_a[30] = "aBcdefaBcdefaBcdefaBcdef";
	char string_b[] = "AbCdEfAbCdEfAbCdEfAbCdEf";
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        strcpy(string_a, string_b);

		c = user_timestamp();
        int_unlock(intLevel);  

        
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strcpy_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strcpy",strcpy_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strcpy",strcpy_Test, TESTING_COUNT, 0);
#endif 

}
void string_strncpy_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
	char string_a[30] = "string(1)string(1)";
	char string_b[] = "string(2)string(2)";
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        strncpy(string_a, string_b,30);

		c = user_timestamp();
        int_unlock(intLevel);  

        
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strncpy_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strncpy",strncpy_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strncpy",strncpy_Test, TESTING_COUNT, 0);
#endif 

}
 
void string_strcspn_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
    char *str = "Reworks is a real time system. The new version is 4.7.1";
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        ret = strcspn(str, " ");

		c = user_timestamp();
        int_unlock(intLevel); 
	 
        if (ret != 7) 
		{
			printf("string_strchr_test -- strchr error,TESTING_COUNT= %d \n",i);
			return ;
		}
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strcspn_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strcspn",strcspn_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strcspn",strcspn_Test, TESTING_COUNT, 0);
#endif 

}

void string_strlen_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
    char *str = "123456781234567812345678";
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        ret = strlen(str);

		c = user_timestamp();
        int_unlock(intLevel); 
	 
        if (ret != 24) 
		{
			printf("string_strlen_test -- strlen error,TESTING_COUNT= %d \n",i);
			return ;
		}
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strlen_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strlen",strlen_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strlen",strlen_Test, TESTING_COUNT, 0);
#endif 

}


void string_strpbrk_test(void)
{
	int i;  
	u64  a;
	u64  b;
	u64  c; 
    int intLevel;  
    int ret;
    
    char *str = "0123456789012345678901234567890";
	char *t = "3456789012345678901234567890";
    char *p;
	
	tick_set(0);
	
    for( i = 0; i < TESTING_COUNT; i++ )
    {	
        intLevel = int_lock();
        
        a = user_timestamp(); 
        b = user_timestamp();
        
        p = strpbrk(str,"4398");

		c = user_timestamp();
        int_unlock(intLevel); 
	 
        ret = strcmp(t, p);
        if (ret != 0) 
		{
			printf("string_strpbrk_test -- strpbrk error,TESTING_COUNT= %d \n",i);
			return ;
		}
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			strpbrk_Test[i] = (c - b) - (b - a);			 
		}
 	}
    
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("strpbrk",strpbrk_Test, TESTING_COUNT, 1);
#else
	inter_statistical("strpbrk",strpbrk_Test, TESTING_COUNT, 0);
#endif 

}


 

void string_Test()
{
	/* 比较字符串 */
	string_strcmp_test(); 
	
	/* 查找字符串中第一个出现的指定字符 */
	string_strchr_test();
	
	/* 拷贝字符串 */
	string_strcpy_test();
	
	/* 拷贝字符串 */
	string_strncpy_test();
	
	/* 返回字符串中连续不含制定字符串内容的字符数 */
	string_strcspn_test();
	
	/* 返回字符串长度 */
	string_strlen_test();
	
	/* 查找字符串中第一个出现的指定字符*/
	string_strpbrk_test();
 
	

}
