/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */

#include "../basic/testing.h"


int inter_fildes = -1;

 
#define INTER_HEADER			"testname,min,max,avg,std,硬件平台_对比,操作系统_对比,版本_对比\n"


int inter_test_start(void)
{
	inter_fildes = -1;
	if (-1 == (inter_fildes = open(RECORD_INTER_FILENAME, O_CREAT | O_RDWR | O_APPEND, 0777)))
	{
		return -1;
	}
	printf("record_test_start\n");
	write(inter_fildes, INTER_HEADER, strlen(INTER_HEADER));
	
	return 0;
}


int inter_test_end(void)
{
	if (-1 == inter_fildes)
	{
		return -1;
	} 
	
	close(inter_fildes);
	
	return 0;
}

 

int inter_test_record(INTER_TEST_RESULT *result)
{
#define BUF_LEN		256
	char buffer[BUF_LEN];
  
	
	sprintf(buffer, "%-25s,%10.6f,%10.6f,%10.6f,%10.6f,%s,%s,%s\n", 
			result->testname, result->max, result->min, result->avg,result->avr,
			TEST_PLAT,TEST_SYSTEM,TEST_VERSION);
//	sprintf(buffer, "%-25s\n",
//				result->testname);
	write(inter_fildes, buffer, strlen(buffer)); 
	
	return 0;
}

 
