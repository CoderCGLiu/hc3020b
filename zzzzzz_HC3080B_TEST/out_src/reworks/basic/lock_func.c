#include "testing.h"

int int_lock1()
{
#ifdef TARGET_REWORKS_MEC7004
	
	int level;
	asm volatile ("pushf ; popl %0 ; andl $0x00000200, %0 ; cli"
			: "=rm" (level) : /* no input */ : "memory");

	return level;
#endif
}

void int_unlock1(int level)
{
#ifdef TARGET_REWORKS_MEC7004
	asm volatile ("testl $0x00000200, %0 ; jz 0f ; sti ; 0:"
			: /* no output */ : "rm" (level) : "memory");
#endif

}
 
