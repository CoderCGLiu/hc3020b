
#ifndef _REWORKS_BLOCK_TEST_CONFIG_H_
#define _REWORKS_BLOCK_TEST_CONFIG_H_


#ifdef __cplusplus
extern "C"
{
#endif
#include "testing.h"
	
#ifdef REWORKS_TEST
#include <reworks/types.h>

#define READ_FILENAME		"/ffx0/default_file"
#define WRITE_FILENAME		"/ffx0/default_file"
	
#else
#include <ramDrv.h> 
#include <blkIo.h> 
#include <dosFsLib.h> 
	
#define device device_vx
	
/**** vxworks根据实际情况修改读写文件名称 **********/
//#define READ_FILENAME		"/ahci01/default_file"
//#define WRITE_FILENAME		"/ahci01/default_file"
	
#define READ_FILENAME		"/ram0/default_file"
#define WRITE_FILENAME		"/ram0/default_file"
	
#endif
extern int WRITE_FILESIZE;


//#define FLASH_DEVICE    //定义flash设备宏，测试flash时放开此宏,测试flash时偏移b_offset


 
 
/**
 * 块设备的块大小
 */	
#ifdef FLASH_DEVICE
#define BLKDEV_BLOCKSIZE	2048 
#define B_OFFSET  7680  //15MB开始bread、bwrite ----7680*2048/1024/1024

#else
#define BLKDEV_BLOCKSIZE	512 
#define B_OFFSET  2048   //1MB开始bread、bwrite ----2048*512/1024/1024
#endif

/*flash设备名称*/
#define FLASHDEV_NAME   "/dev/ffxdk0"
/**
 * 块设备名称
 */
#define BLKDEV_NAME			"/dev/block"
	
/**
 * 要测试的块设备分区名称
 */	
#define BLKDEV_NAME_P1		"/dev/blockp1"
/**
 * 块设备名称
 */
#define SATABLKDEV_NAME			"/dev/sata"
	
/**
 * 要测试的块设备分区名称
 */	
#define SATABLKDEV_NAME_P1		"/dev/satap1"	
/**
 * 内存设备名称
 */
#define RAMDEV_NAME			"/dev/ram0"
		
/**
 * 要测试的内存设备分区名称
 */	
#define RAMDEV_NAME_P1		"/dev/ram0p1"	
	
/**
 * U盘设备名称
 */
#define USBDEV_NAME      "/dev/umass0"

/**
 * 要测试的U盘设备分区名称
 */	
#define USBDEV_NAME_P1	"/dev/umass0p1"


	
/**
 * 挂载点名称
 */	
#define MP_NAME				"/c"
	
///**
// * 待测试文件系统名称
// */	
//#define FS_NAME				"dosfs"
//	

typedef enum fskind{
	DOSFS,HRFS
}FSKIND ;

/**
 * 设备类型
 */
typedef enum device{
	ATA, SATA, RAM, USB,FLASH
}DEVICE ;

/*
 * 块大小顺序标记
 */
typedef enum block_size_token {
	START_TOKEN,
	MID_TOKEN,
	END_TOKEN
}BLOCK_TOKEN;

typedef struct block_size {
	BLOCK_TOKEN token;
	int blocksize;
}BLOCK_SIZE;
	
/**
 * 每次读写块大小定义 
 */
static BLOCK_SIZE blocksize_array[] =
{
//	{START_TOKEN, 16},
//	{MID_TOKEN, 32},
//	{MID_TOKEN, 64},
//	{MID_TOKEN, 256},
//	{MID_TOKEN, 512},
//	{MID_TOKEN, 1024},
//	{MID_TOKEN, 2048},
//	{MID_TOKEN, 4096},
//	{MID_TOKEN, 8192},       /* 8K */
//	{MID_TOKEN, 16384},      /* 16K */
//	{MID_TOKEN, 32768},      /* 32K */
//	{MID_TOKEN, 65536},      /* 64K */ 
//	{MID_TOKEN, 131072   },    //128 
//	{MID_TOKEN, 262144   },   //256 
	{START_TOKEN, 524288   },    //512
	{MID_TOKEN, 1048576},    /* 1M */
	{MID_TOKEN, 1048576 * 2},    /* 2M */
	{MID_TOKEN, 1048576 * 4},    /* 4M */
	{MID_TOKEN, 1048576 * 8},    /* 8M */
	{MID_TOKEN, 1048576 * 16},    /* 16M */
	{END_TOKEN, 1048576 * 32},    /* 32M */  
	
};
#define ITERATOR (sizeof(blocksize_array) / sizeof(BLOCK_SIZE))

typedef struct block_test_result
{
	char 		*testname;
	char        *fskind;
	double      write_avg[ITERATOR];
	double      read_avg[ITERATOR];
	double      fwrite_avg[ITERATOR];
	double      fread_avg[ITERATOR];
}BLOCK_TEST_RESULT;
extern int block_test_record(BLOCK_TEST_RESULT *result);

static BLOCK_SIZE bread_array[] =
{
//	{START_TOKEN, 512},
//	{MID_TOKEN, 1024},
//	{MID_TOKEN, 2048},
//	{MID_TOKEN, 4096},
//	{MID_TOKEN, 8192},       /* 8K */
//	{MID_TOKEN, 16384},      /* 16K */
//	{MID_TOKEN, 32768},      /* 32K */
//	{MID_TOKEN, 65536},      /* 64K */ 
//	{MID_TOKEN, 131072   },    //128
//	{MID_TOKEN, 262144   },   //256 
	{START_TOKEN, 524288   },    //512
	{MID_TOKEN, 1048576},    /* 1M */
	{MID_TOKEN, 1048576 * 2},    /* 2M */
	{MID_TOKEN, 1048576 * 4},    /* 4M */
	{MID_TOKEN, 1048576 * 8},    /* 8M */
	{MID_TOKEN, 1048576 * 16},    /* 16M */
	{END_TOKEN, 1048576 * 32},    /* 32M */  
	
};

#if 0
/**
 * 结果记录
 */
typedef struct block_test_result
{
	char 		*opsname;
	u32			filesize;
	u32			blocksize;
	double		usingtime;
	double		rate;	
}BLOCK_TEST_RESULT;

/**
 * 文件名称
 */
#define RECORD_FILENAME			"/block_test_records.xml"

/**
 * 记录接口
 */
int block_test_start(void);
int block_test(DEVICE, int);
int block_test_record(BLOCK_TEST_RESULT *result);
int block_test_end(void);
int block_test_record_start(const char *name);
int block_test_record_end(const char *name);

#endif

extern BLOCK_TEST_RESULT	block_result;
/**
 * 测试项
 */
extern int block_test_prepare(DEVICE, FSKIND, int);
extern int write_test(FSKIND);
extern int fwrite_test(FSKIND);
extern int read_test(FSKIND);
extern int fread_test(FSKIND);
extern int bwrite_test(DEVICE);
extern int bread_test(DEVICE);


#ifdef __cplusplus
}
#endif
	

#endif
