/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */

#include "../basic/testing.h"


int other_fildes = -1;

 
#define OTHER_HEADER			"testname,avg,硬件平台_对比,操作系统_对比,版本_对比\n"


int other_test_start(void)
{
	other_fildes = -1;
	if (-1 == (other_fildes = open(RECORD_OTHER_FILENAME, O_CREAT | O_RDWR| O_APPEND, 0777)))
	{
		return -1;
	}
	printf("record_test_start\n");
	write(other_fildes, OTHER_HEADER, strlen(OTHER_HEADER));
	
	return 0;
}


int other_test_end(void)
{
	if (-1 == other_fildes)
	{
		return -1;
	} 
	
	close(other_fildes);
	
	return 0;
}

 

int other_test_record(INTER_TEST_RESULT *result)
{
	#define BUF_LEN		256
	char buffer[BUF_LEN]; 
	
	sprintf(buffer, "%-25s,%10.6f,%s,%s,%s\n", result->testname, result->avg,
			TEST_PLAT,TEST_SYSTEM,TEST_VERSION);
//	sprintf(buffer, "%-25s\n",
//				result->testname);
	write(other_fildes, buffer, strlen(buffer)); 
	
	return 0;
}

 
