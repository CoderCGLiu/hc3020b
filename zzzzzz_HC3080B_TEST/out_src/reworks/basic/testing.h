/*******************************************************************************
 * 版权：
 * 		 中国电子科技集团公司第三十二研究所
 * 描述：
 * 		测试主要参数定义，平台信息定义。
 * 		
 * 修改：
 * 		 2013-06-18，宋伟，创建. 
 * 		
 */

#ifndef __TESTING_H__
#define __TESTING_H__

#define REWORKS_TEST
//#define __multi_core__
#define REWORKS_TEST
#ifdef REWORKS_TEST
	#include <stdio.h>
	#include <reworks/types.h>
	#include <reworks/printk.h>
	#include <reworksio.h>
	#include <memory.h>
	#include <string.h>
	#include <irq.h>
	#include <clock.h>
	#include <udelay.h>
	#include <io_asm.h>
	#include <semLib.h>
	#include <vxWorks.h>
	#include <wdLib.h>
	#include <taskLib.h>
	#include <msgQLib.h>
	#include <event.h>  
	#include <semaphore.h>
	#include <pthread.h>
#else
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <vxWorks.h>
	#include <intLib.h>	
	#include <semLib.h>
	#include <wdLib.h>
	#include <taskLib.h>
	#include <msgQLib.h>
	#define	u32				UINT32
	#define	u64				UINT64

	#define int_lock		intCpuLock
	#define int_unlock		intCpuUnlock 
	#define tick_get		tickGet
	#define tick_set		tickSet
	#define global_std_get 	ioGlobalStdGet
	#define global_std_set	ioGlobalStdSet
//	#define sys_clk_rate_get sysClkRateGet
//	#define sys_clk_rate_set sysClkRateSet
	#define int_enable_pic intEnable
	#define int_disable_pic intDisable
	#define get_c0_sr sysStatusGet
	#define set_c0_sr sysStatusSet
#ifdef __multi_core__
	#define intLock		intCpuLock
	#define intUnlock		intCpuUnlock 
#endif

#endif


/* 测量平台定义 */
//#define TARGET_SBC8641D
//#define TARGET_SBC8640D
//#define TARGET_PENTIUM
//#define TARGET_P1020
//#define TARGET_LOONGSON2F
//#define TARGET_LOOGSON3A
#define TARGET_HUARUI2
//#define TARGET_LOONGSON1B
//#define TARGET_LOONGSON2H
//#define TARGET_HUARUI
//#define TARGET_SOPC80
//#define TARGET_HUARUI2
/**
 * 结果记录
 */
typedef struct inter_test_result
{
	char 		*testname;
	double         max;
	double         min;
	double         avg;
	double		avr;	
}INTER_TEST_RESULT; 

#define RECORD_INTER_FILENAME			"/inter_test_records.csv"
#define RECORD_OTHER_FILENAME			"/other_test_records.csv"
#define RECORD_BLOCK_FILENAME			"/block_test_records.csv"

#ifdef TARGET_LOOGSON3A
#define TEST_PLAT  "龙芯3A"
#define TEST_VERSION "ReDeV5.0RC2-LS3A-A150928"
//#define TEST_VERSION "ReDeV5.0RC1-LS-A140624"
#endif

#ifdef TARGET_HUARUI2
#define TEST_PLAT  "华睿2号"
#define TEST_VERSION "V6.0.1-HR2_64-A20181129"
#endif

#ifdef REWORKS_TEST
#define TEST_SYSTEM "reworks"
#else
#define TEST_SYSTEM "vxworks"
#endif

extern int inter_test_start(void);
extern int inter_test_end(void);
extern int inter_test_record(INTER_TEST_RESULT *result);
extern int other_test_start(void);
extern int other_test_end(void);
extern int other_test_record(INTER_TEST_RESULT *result);
extern int block_test_start(void);
extern int block_test_end(void);
//extern int block_test_record(BLOCK_TEST_RESULT *result);
/* 
 * 测试是否打印中间循环数据
 */
//#define ANALYSIS
/*
 * 数据类型定义 
 */
typedef void (*ISRFUNC)(void *);

/* 
 * 指令代码常数
 */
#define INST_NOP	0x60000000
#define INST_BLR	0x4e800020

/* 测试次数 */
#define TESTING_COUNT		1000

#define JITTER_ITERATOR_COMP		8000000
#define TEST_ITER		100000 // jitter 测试中子循环的执行次数

/* 测试 malloc 接口时使用的内存大小 */
#define SIZE_SMALL  (64)
#define SIZE_LARGE  (4096 + 128 ) 
#define NMEMB (1024)

/* 为测试任务相关接口添加的  */
#define TASK_STACK_SIZE 1024

/* 检测溢出 */
extern void overflow_test(void);
extern int mem_calibration_entry(void);
/*
 * 测试层实现的时间获取函数
 */
extern u32  sys_timestamp_freq(void);
extern u64 	sys_timestamp (void);

/*
 * 确定测试使用计时方式sys_timestamp或者user_timestamp
 */
//#define _USER_TIME_TEST
#ifdef _USER_TIME_TEST
extern u32  user_timestamp_freq(void);
extern u64 	user_timestamp (void);
#else
#define user_timestamp  sys_timestamp
#define user_timestamp_freq sys_timestamp_freq
#endif
 
extern u32 sys_clk_rate_get(void);
extern int sys_clk_rate_set(int ticks_per_second);
extern u64 readTime();
/*
 * 测试环境：是否关闭/打开中断
 */
extern void enable_pic();
extern void disable_pic(); 

/*
 * 测试环境
 */
extern int test_int_lock();
extern int test_int_unlock();

/* cache 影响测试 */
extern int cache_mbw_entry(void);
extern void cache_dhry(int loops);

/* cpu 测试 */
extern int cpu_whets();
extern int cpu_dhry();

/* float 测试 */
extern void testing_float();
 
/* 中断 测试 */
extern void testing_interrupt();

/* 时间抖动 测试 */
extern int testing_jitter(void);

/* 计时开销 测试 */
extern void testing_kaixiao();

/* mbw 测试 */
extern int mbw_entry(void);

/* 系统时钟开销 测试 */
extern void tick_time(void);

/* 实时性能 */
extern void realtime_test();

/*最长锁中断时间测试用例*/
extern void int_lock_test();

/*信号量混洗时间*/
extern void vxSem_shuffing();

/*vxworks 接口性能测试 */
extern void testing_vxworks();
extern void testing_taskSpawn();
extern void testing_taskDelete();
extern void testing_taskSuspend();
extern void testing_taskResume();
extern void testing_semBTake();
extern void testing_semBGive();
extern void statistical_analysis(u64 *array, u32 num, u32 flag);
extern void inter_statistical(char *testname,u64 *array, u32 num, u32 flag);
extern void ContextSwitchTimer_vx( int iteration);
extern void testing_Malloc(int size);
extern void testing_Free(int size);
extern void testing_Calloc (int nmemb, int size);
extern void testing_Realloc (int size);
extern void test_mem();


#define BM_SEM_B 1
#define BM_SEM_C 2
#define BM_SEM_M 3

extern void testing_semBTest();
extern void testing_semCTest();
extern void testing_semMTest();
extern void bmSemCreate( int semType);
extern void bmSemDelete( int semType);
extern void bmSemFlush( int semType);
extern void bmSemGiveQEmpty(int semType);
extern void bmSemGiveQFull(int semType);
extern void bmSemGiveTake(int semType);
extern void bmSemTakeEmpty (int semType);
extern void bmSemTakeFull(int semType);

extern void testing_MQTest();
extern void testing_MQCreate();
extern void testing_MQDelete();
extern void testing_MQSendNoPend();
extern void testing_MQSendPend();
extern void testing_MQSendQFull();
extern void testing_MQRecvAvail();
extern void testing_MQRecvNoAvail();

extern void testing_EventTest();
extern void testing_EventSendNoPend();
extern void testing_EventSendPend();
extern void testing_EventReceiveAvail();
extern void testing_EventReceiveNoAvail();

extern void testing_WDTest(); 
extern void sem_shuffling();


#endif
