
/******************************************************************************
 * 
 * 版权：
 * 		中国电子科技集团公司第三十二研究所
 * 描述：
 * 		本文件实现测量结果的数理统计。
 * 		 可给出结果的最大数、最小数、平均数、3倍标准差。
 * 		 由于测量过程已经最大化避免外部干扰，所以方差的计算暂不考虑粗大误差的剔除。
 * 修改：
 * 		2014-01-14，宋 伟，建立 
 */

#include <stdio.h>
#include <math.h>
#include "testing.h"

#ifdef REWORKS_TEST
#include <tty_ioctl.h>
#else
#include <ioctl.h>
#include <ioLib.h>
#endif




void inter_statistical(char *testname,u64 *array, u32 num, u32 flag)
{
	int i;
	u32 freq;
	u64 max, min, sum;		
	double avr, sum_error;
	double variance;
	double std;
	INTER_TEST_RESULT	result;
	/*
	 * @0424
	 */
	int ret;
	unsigned long long wait_target;
	u32 baud_rate;
	int baud_ret;
	/*
	 * @End 0424
	 */
	
	
	/* 求取最大数、最小数、平均数、和 */
	max = array[1];
	min = array[1]; 	
	avr = 0;
	sum = 0;	
	for(i = 1; i < num; i++) {
		if(array[i] > max)
			max = array[i];
		
		if(array[i] < min)
			min = array[i];
		
		sum += array[i];
	}
 
	avr = sum * 1.0  / (num -1);

	/* 求取方差、标准差 */	
	sum_error = 0;
	for(i = 1; i < num; i++) {
		sum_error += (array[i] - avr) * (array[i] - avr);
	}
	
	variance = sum_error / (num - 2); /* 方差 */
	
	std = sqrt(variance);	/* 标准差 */
	
	freq = user_timestamp_freq();
#ifdef REWORKS_TEST
	baud_ret = ioctl(fileno(stdout), TTY_GET_BAUDRATE, (void *)&baud_rate);
#else
	baud_ret = ioctl(fileno(stdout), FIOBAUDRATE, (void *)&baud_rate);
#endif
	
	ret = printf("| %-25s | %10d | %10.6f | %10.6f | %10.6f | %10.6f |\n",
			testname , (int)num,
			(min * 1000000.0 / freq), 
			(max * 1000000.0 / freq), 
			(avr * 1000000.0 / freq),
			(std * 3.0 * 1000000.0 / freq));
	
	/* 记录数据 */
	result.testname = testname;
	result.max 	= (min * 1000000.0 / freq);
	result.min 	= (max * 1000000.0 / freq);
	result.avg 	= (avr * 1000000.0 / freq);
	result.avr 		= (std * 3.0 * 1000000.0 / freq);
	inter_test_record(&result);
	
	if (baud_ret == -1) {
		sleep(1);
	} else {
		wait_target = user_timestamp();
		wait_target += (unsigned long long) ret * freq / (baud_rate / 10);
		wait_target += freq / 1000;

		while (user_timestamp() < wait_target)
			;
	}
	if(flag == 1) {
		for(i = 1; i < num; i++) {
//			printf("%lld\n", array[i]);
			printf("%f\n", (array[i]*1000000.0)/freq);
		}
	}
	
}
