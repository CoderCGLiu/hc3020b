#include "../basic/testing.h" 
#include "posix_test.h"
void testing_semempty_init()
{
	bmsem_init (BM_SEM_EMPTY);

}

void testing_semempty_destroy()
{
	bmsem_destroy (BM_SEM_EMPTY);

}

void testing_semempty_flush()
{
	bmsem_flush (BM_SEM_EMPTY);

}

void testing_semempty_wait()
{
	bmsem_wait (BM_SEM_EMPTY);

}

void testing_semempty_post()
{
	bmsem_post(BM_SEM_EMPTY);
}

void testing_semempty_trywait()
{
	bmsem_trywait(BM_SEM_EMPTY);	
}

void testing_semempty()
{
	 
	testing_semempty_init();
	
	testing_semempty_destroy();
	
	testing_semempty_flush();
	
	testing_semempty_wait();
	
	testing_semempty_post();
	
	testing_semempty_trywait();
	
}
