#include <mqueue.h>

#ifdef __multi_core__
#define pthread_lock pthread_cpu_lock
#define pthread_unlock pthread_cpu_unlock
#endif
#define	BM_MSG_MAX_MSG_LEN	1
#define	BM_MSG_MAX_MESSAGES	1000 
#define MSGSTR "0123456789"
#define NAMESIZE 50
#define BUFFER 40
#define MAXMSG 10
#define MSG_SIZE	50

#define BM_SEM_EMPTY 0
#define BM_SEM_FULL 1

extern void testing_pthread();
extern void testing_pthread_create();  
extern void testing_pthread_createDelete();
extern void testing_pthreadDelete(); 
extern void testing_pthread_suspend();  
extern void testing_pthread_resume();   
extern void testing_pthread_lock(); 
extern void testing_pthread_unlock(); 
extern void testing_pthread_unlockNoLock();  

extern void testing_pthreadrwlock();
extern void testing_pthreadrwlock_init(); 	
extern void testing_pthreadrwlock_destroy();
extern void testing_pthreadrwlock_rdlock();
extern void testing_pthreadrwlock_wrlock();  
extern void testing_pthreadrwlock_unlock();  

extern void testing_mutex();
extern void testing_pthread_mutex_init();
extern void testing_pthread_mutex_destroy();
extern void testing_pthread_mutex_lock();
extern void testing_pthread_mutex_unlock();	

extern void testing_semfull();
extern void testing_semfull_init();
extern void testing_semfull_destroy();
extern void testing_semfull_flush();
extern void testing_semfull_wait();
extern void testing_semfull_post();
extern void testing_semfull_trywait();

extern void testing_semempty(); 
extern void testing_semempty_init(); 
extern void testing_semempty_destroy(); 
extern void testing_semempty_flush(); 
extern void testing_semempty_wait(); 
extern void testing_semempty_post(); 
extern void testing_semempty_trywait();

extern void bmsem_init( int semType);
extern void bmsem_destroy( int semType);
extern void bmsem_flush( int semType);
extern void bmsem_post(int semType);
extern void bmsem_wait (int semType);
extern void bmsem_trywait (int semType);

extern void testing_MQ();
extern void testing_mq_create();
extern void testing_mq_delete();
extern void testing_mq_open();
extern void testing_mq_close();
extern void testing_mq_unlink();
extern void testing_mq_send();
extern void testing_mq_receive();
 

extern void testing_Timer(); 
extern void testing_TimerCreate(); 	
extern void testing_TimerDelete();   
	 
 
