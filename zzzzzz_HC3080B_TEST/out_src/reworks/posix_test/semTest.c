#include "../basic/testing.h" 
#include "posix_test.h"

static u64 sem_init_test[TESTING_COUNT]; 
static u64 sem_destroy_test[TESTING_COUNT]; 
static u64 sem_flush_test[TESTING_COUNT]; 
static u64 sem_post_test[TESTING_COUNT]; 
static u64 sem_wait_test[TESTING_COUNT];
static u64 sem_trywait_test[TESTING_COUNT];
static  void bmsem_waitTask(sem_t  *sem)
{

    while (1)
	{
    	sem_wait(&sem);
    	
	}

}

void bmsem_init( int semType)
{
	int i;
	u64		a, b, c ;  
	sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	} 
 
    switch (semType)
    	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
        }
 
    sem_destroy(&mysemp);

	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {		

	     switch (semType)
		 {
	     	 case BM_SEM_EMPTY:
	    	    a = sys_timestamp ();
	    	    b = sys_timestamp ();
	    	    
	    	    sem_init (&mysemp, 0, 0);
	    	    
	    	    c = sys_timestamp ();
	    	    break;
	    	    
	    	case BM_SEM_FULL:
	    	    a = sys_timestamp ();
	    	    b = sys_timestamp ();
	    	    
	    	    sem_init (&mysemp, 0, 1);
	    	    
	    	    c = sys_timestamp ();	    	    
	    	    break;	  	    	    
	    	default:
	    		printf("sem type error\n"); 
	    	    break;
		 }
		 
	     sem_destroy(&mysemp);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sem_init_test[i] = (c - b) - (b - a);			 
		}
	}
 
	
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_init",sem_init_test, TESTING_COUNT, 1);
#else
	inter_statistical("sem_init",sem_init_test, TESTING_COUNT, 0);
#endif

}


void bmsem_destroy( int semType)
{
	int i;
	u64		a, b, c ;  
	sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	}  

    switch (semType)
	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
	}
 
    sem_destroy(&mysemp);

	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {	 

		
        switch (semType)
    	{
 
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;

		default:
    		printf("sem type error\n");
			break;
    	}
		a = sys_timestamp ();
		b = sys_timestamp ();
		
		sem_destroy(&mysemp);
		
		c = sys_timestamp ();
	
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sem_destroy_test[i] = (c - b) - (b - a);			 
		}
	}
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_destroy",sem_destroy_test, TESTING_COUNT, 1);
#else
	inter_statistical("sem_destroy",sem_destroy_test, TESTING_COUNT, 0);
#endif

}

void bmsem_flush( int semType)
{
	int i;
	u64		a, b, c ;    
    pthread_t        semTakeTask; 
    sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	}  

    switch (semType)
	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
	}
    
	/* 测试并记录测试结果*/
	tick_set(0); 
 
    
   pthread_create(&semTakeTask,NULL,bmsem_waitTask, mysemp);
   
   //确保bmsem_waitTask运行，sem_wait
   pthread_delay(1);
		
   for(i = 0; i < TESTING_COUNT; i++) {		
	  
		a = sys_timestamp ();
		b = sys_timestamp ();
		
		sem_flush (&mysemp);
		
		c = sys_timestamp (); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sem_flush_test[i] = (c - b) - (b - a);			 
		}
	}
   pthread_cancelforce(semTakeTask);

	sem_destroy(&mysemp);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_flush",sem_flush_test, TESTING_COUNT, 1);
#else
	inter_statistical("sem_flush",sem_flush_test, TESTING_COUNT, 0);
#endif

}


void bmsem_post(int semType)
{
	int i;
	u64		a, b, c ;  
    sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	}  

    switch (semType)
	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
	}

     /* load into the cache */
	
    sem_post (&mysemp);
    sem_wait (&mysemp);
    tick_set(0);
    
    for(i = 0; i < TESTING_COUNT; i++) {
    	  			
	
		a = sys_timestamp ();
		b = sys_timestamp ();
		
		sem_post (&mysemp);
		
		c = sys_timestamp ();
	
		sem_wait (&mysemp);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			sem_post_test[i] = (c - b) - (b - a);			 
		}
    	
    }
	sem_destroy (&mysemp);	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_post",sem_post_test, TESTING_COUNT , 1);
#else
	inter_statistical("sem_post",sem_post_test, TESTING_COUNT , 0);
#endif
	
}

void bmsem_wait (int semType)
{
	int i;
	u64		a, b, c ;     
    sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	}  

    switch (semType)
	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
	}

     /* load into the cache */
	
    sem_post (&mysemp);
    sem_wait (&mysemp);
 	
    tick_set(0);
  
    for(i = 0; i < TESTING_COUNT; i++) {
    	
    	sem_post (&mysemp);
 		a = sys_timestamp ();
 		b = sys_timestamp ();
 		 
 		sem_wait (&mysemp);
 		
 		c = sys_timestamp (); 
	
 		/* 有效性检测 */
 		if(tick_get() > 0) {			
 			i--; /* 产生了时钟中断，测量无效 */	
 			tick_set(0);			
 		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
 			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
 		} else {
 			sem_post_test[i] = (c - b) - (b - a);			 
 		}
     	
     }
 	sem_destroy (&mysemp);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_wait",sem_wait_test, TESTING_COUNT, 1);
#else
	inter_statistical("sem_wait",sem_wait_test, TESTING_COUNT, 0);
#endif
	
}

void bmsem_trywait (int semType)
{
	int i;
	u64		a, b, c ;     
    sem_t   mysemp; 
    
    if (semType != BM_SEM_EMPTY &&semType != BM_SEM_FULL )
  	{
		printf("sem type error\n");
		return ;
  	}  

    switch (semType)
	{
    	case BM_SEM_EMPTY:
    		sem_init (&mysemp, 0, 0);
    	    break;

    	case BM_SEM_FULL:
    		sem_init (&mysemp, 0, 1);
    	    break;
    	default:
    		printf("sem type error\n"); 
    	    break;
	}

     /* load into the cache */
	
    sem_post (&mysemp);
    sem_trywait (&mysemp);
 	
    tick_set(0);
  
    for(i = 0; i < TESTING_COUNT; i++) {
    	
    	sem_post (&mysemp);
 		a = sys_timestamp ();
 		b = sys_timestamp ();
 		 
 		sem_trywait (&mysemp);
 		
 		c = sys_timestamp (); 
	
 		/* 有效性检测 */
 		if(tick_get() > 0) {			
 			i--; /* 产生了时钟中断，测量无效 */	
 			tick_set(0);			
 		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
 			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
 		} else {
 			sem_post_test[i] = (c - b) - (b - a);			 
 		}
     	
     }
 	sem_destroy (&mysemp);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("sem_trywait",sem_trywait_test, TESTING_COUNT, 1);
#else
	inter_statistical("sem_trywait",sem_trywait_test, TESTING_COUNT, 0);
#endif
	
}
	
 

