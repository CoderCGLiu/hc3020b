#include "../basic/testing.h" 
#include "posix_test.h"
 
static u64 mq_create_test[TESTING_COUNT];
static u64 mq_delete_test[TESTING_COUNT];
static u64 mq_open_test[TESTING_COUNT]; 
static u64 mq_close_test[TESTING_COUNT]; 
static u64 mq_unlink_test[TESTING_COUNT]; 
static u64 mq_send_test[TESTING_COUNT]; 
static u64 mq_receive_test[TESTING_COUNT];  


 
 

void testing_mq_create()
{
	int i;
	u64		a, b, c ;  
    mqd_t	      msq;
    mqd_t	      msq2; 

    pthread_delay (1);     /* synchronize with the system tick */
    msq2 = mq_create(0, BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, PTHREAD_WAITQ_FIFO);
	mq_delete (msq2);
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
	
		a = sys_timestamp();
		b = sys_timestamp();
		
		/* 打开消息队列  */
		msq = mq_create(0, BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, PTHREAD_WAITQ_FIFO);
		
		c = sys_timestamp();	
		
		if (msq == (mqd_t)-1)    {
			printf("testing_mq_create error\n\n");
		}
		
		mq_delete (msq);

			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_create_test[i] = (c - b) - (b - a);			 
		} 
		
	}
	
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_create",mq_create_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_create",mq_create_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_delete()
{
	int i;
	u64		a, b, c ;  
    mqd_t	      msq; 

    pthread_delay (1);     /* synchronize with the system tick */
    msq = mq_create(0, BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, PTHREAD_WAITQ_FIFO);
    mq_delete (msq);
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		
		/* 打开消息队列  */
		msq = mq_create(0, BM_MSG_MAX_MESSAGES, BM_MSG_MAX_MSG_LEN, PTHREAD_WAITQ_FIFO);
	   
		a = sys_timestamp();
		b = sys_timestamp();
	
		mq_delete (msq);
		c = sys_timestamp();	  
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_delete_test[i] = (c - b) - (b - a);			 
		} 
		
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_delete",mq_delete_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_delete",mq_delete_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_open()
{
	int i; 	 
	u64		a, b, c ;
	char qname[NAMESIZE]; 
	mqd_t queue1, queue;
	
    pthread_delay (1);     /* synchronize with the system tick */
    
	sprintf(qname, "/mq_open_1-1_%d", getpid());
	queue1 = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL);
	if (queue1 == (mqd_t)-1) 
	{
		perror("mq_open() did not return success");
		return;
	} 
	mq_close(queue1);
	mq_unlink(qname);
 

	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) { 
	
		a = sys_timestamp();
		b = sys_timestamp();
		
		queue = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL);
		
		c = sys_timestamp();	
		mq_close(queue);
		mq_unlink(qname);  
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_open_test[i] = (c - b) - (b - a);			 
		}
	}
	
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_open",mq_open_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_open",mq_open_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_close()
{
	int i; 	 
	u64		a, b, c ;
	char qname[NAMESIZE]; 
	mqd_t queue1, queue;
	
    pthread_delay (1);     /* synchronize with the system tick */
	sprintf(qname, "/mq_open_1-1_%d", getpid());
	queue1 = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL);
	if (queue1 == (mqd_t)-1) 
	{
		perror("mq_open() did not return success");
		return;
	} 
	mq_close(queue1);
	mq_unlink(qname); 


	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 

		
		queue = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL);
		a = sys_timestamp();
		b = sys_timestamp();

		mq_close(queue);
		c = sys_timestamp();	
		mq_unlink(qname); 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_close_test[i] = (c - b) - (b - a);			 
		}
	}

	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_close",mq_close_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_close",mq_close_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_unlink()
{
	int i; 	 
	u64		a, b, c ;
	char qname[NAMESIZE]; 
	mqd_t queue1, queue;
	
    pthread_delay (1);     /* synchronize with the system tick */
	sprintf(qname, "/mq_open_1-1_%d", getpid());
	queue1 = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL);
	if (queue1 == (mqd_t)-1) 
	{
		perror("mq_open() did not return success");
		return;
	} 
	mq_close(queue1);
	mq_unlink(qname);
 
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) { 
		
		queue = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, NULL); 
		mq_close(queue);
		a = sys_timestamp();
		b = sys_timestamp();
	
		mq_unlink(qname);
		c = sys_timestamp(); 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_unlink_test[i] = (c - b) - (b - a);			 
		}
	}

	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_unlink",mq_unlink_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_unlink",mq_unlink_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_send()
{
	int i; 	 
	u64		a, b, c ;
	char qname[NAMESIZE], msgrcd[BUFFER];
	const char *msgptr = MSGSTR;
	mqd_t queue; 
	unsigned pri;
	
    pthread_delay (1);     /* synchronize with the system tick */
    
	sprintf(qname, "/mq_send_%d", getpid());
	
	struct mq_attr attr;
	attr.mq_msgsize = BUFFER;
	attr.mq_maxmsg = BUFFER;
	attr.mq_waitqtype = PTHREAD_WAITQ_PRIO;
	
	queue = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, &attr);
	if (queue == (mqd_t)-1) 
	{
		perror("mq_open() did not return success");
		return;
	} 
	
	mq_send(queue, msgptr, strlen(msgptr), 1); 


	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
	 
		a = sys_timestamp();
		b = sys_timestamp();
		
		mq_send(queue, msgptr, strlen(msgptr), 1);
		c = sys_timestamp();
		mq_receive(queue, msgrcd, BUFFER, &pri); 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_send_test[i] = (c - b) - (b - a);			 
		}
	}

	mq_close(queue);
	mq_unlink(qname); 
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_send",mq_send_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_send",mq_send_test, TESTING_COUNT, 0);
#endif

}

void testing_mq_receive()
{
	int i; 	 
	u64		a, b, c ;
	char qname[NAMESIZE], msgrcd[BUFFER];
	const char *msgptr = MSGSTR;
	mqd_t queue;
	struct mq_attr attr;
	unsigned pri;
	
    pthread_delay (1);     /* synchronize with the system tick */
	sprintf(qname, "/mq_receive_%d", getpid());
	
	attr.mq_msgsize = BUFFER;
	attr.mq_maxmsg = BUFFER;
	attr.mq_waitqtype = PTHREAD_WAITQ_PRIO;
	
	queue = mq_open(qname, O_CREAT |O_RDWR, S_IRUSR | S_IWUSR, &attr);
	if (queue == (mqd_t)-1) 
	{
		perror("mq_open() did not return success");
		return;
	} 
	
	mq_send(queue, msgptr, strlen(msgptr), 1);
	mq_receive(queue, msgrcd, BUFFER, &pri); 


	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
		mq_send(queue, msgptr, strlen(msgptr), 1);
		a = sys_timestamp();
		b = sys_timestamp();

		mq_receive(queue, msgrcd, BUFFER, &pri);
		c = sys_timestamp(); 
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			mq_receive_test[i] = (c - b) - (b - a);			 
		}
	}


	mq_close(queue);
	mq_unlink(qname);
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("mq_receive",mq_receive_test, TESTING_COUNT, 1);
#else
	inter_statistical("mq_receive",mq_receive_test, TESTING_COUNT, 0);
#endif

}




void testing_MQ()
{
	
	testing_mq_create();
	testing_mq_delete();
	testing_mq_open();
	testing_mq_close();
	testing_mq_unlink();
	testing_mq_send();
	testing_mq_receive();
	
}
