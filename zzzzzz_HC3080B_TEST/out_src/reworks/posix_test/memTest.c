#include "../basic/testing.h" 
 
static u64 Malloc_test[TESTING_COUNT]; 
static u64 Free_test[TESTING_COUNT];  

static u64 Calloc_test[TESTING_COUNT]; 
static u64 Realloc_test[TESTING_COUNT];  


void posix_testing_Malloc(int size)
{
	int i;
	u64		a, b, c ;  
    int level;  
    char *ptr;
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		ptr = malloc (size);
		
		c = sys_timestamp();	
		
		int_unlock(level); 		
 
		free (ptr);
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			Malloc_test[i] = (c - b) - (b - a);			 
		}
	}
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("Malloc",Malloc_test, TESTING_COUNT, 1);
#else
	inter_statistical("Malloc",Malloc_test, TESTING_COUNT, 0);
#endif

}
void posix_testing_Free(int size)
{
	int i;
	u64		a, b, c ;  
    int level;  
    char *ptr;
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		ptr = malloc (size);
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		free (ptr);
		
		c = sys_timestamp();	
		
		int_unlock(level); 		
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			Free_test[i] = (c - b) - (b - a);			 
		}
	}
 
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("free",Free_test, TESTING_COUNT, 1);
#else
	inter_statistical("free",Free_test, TESTING_COUNT, 0);
#endif

}
 
void posix_testing_Calloc (int nmemb, int size)
{
	int i;
	u64		a, b, c ;  
    int level;  
    char *ptr;
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		ptr = calloc(nmemb, size);
		
		c = sys_timestamp();	
		
		int_unlock(level); 		
 
		free (ptr);
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			Calloc_test[i] = (c - b) - (b - a);			 
		}
	}
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("Calloc",Calloc_test, TESTING_COUNT, 1);
#else
	inter_statistical("Calloc",Calloc_test, TESTING_COUNT, 0);
#endif

}

void posix_testing_Realloc (int size)
{
	int i;
	u64		a, b, c ;  
    int level;  
    char *ptr;
	
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {		

		level = int_lock();
		
		ptr = malloc (size);
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		ptr = realloc(ptr, size);
		
		c = sys_timestamp();	
		
		int_unlock(level); 		
 
		free (ptr);
			
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			Realloc_test[i] = (c - b) - (b - a);			 
		}
	}
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("Realloc",Realloc_test, TESTING_COUNT, 1);
#else
	inter_statistical("Realloc",Realloc_test, TESTING_COUNT, 0);
#endif

}

void posix_test_mem()
{
	posix_testing_Malloc(SIZE_SMALL); 
	
	posix_testing_Malloc(SIZE_LARGE); 
	
	posix_testing_Free(SIZE_SMALL); 
	posix_testing_Free(SIZE_LARGE); 
}
