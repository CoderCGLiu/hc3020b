#include "../basic/testing.h" 
#include "posix_test.h"
static u64 pthreadrwlock_init_test[TESTING_COUNT]; 
static u64 pthreadrwlock_destroy_test[TESTING_COUNT]; 
static u64 pthreadrwlock_rdlock_test[TESTING_COUNT]; 
static u64 pthreadrwlock_wrlock_test[TESTING_COUNT];  
static u64 pthreadrwlock_unlock_test[TESTING_COUNT];  

 
 
 
 
void testing_pthreadrwlock_init() 
{
	int i;
	u64		a, b, c ;  
	pthread_rwlockattr_t rwlockattr;	
	pthread_rwlock_t rwlock;
	
    tick_set(0); 
    
    pthread_rwlockattr_init(&rwlockattr);
    
    pthread_rwlock_init(&rwlock, &rwlockattr);
    pthread_rwlock_destroy(&rwlock);
    
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
		a = sys_timestamp();
		b = sys_timestamp();
	
		pthread_rwlock_init(&rwlock, &rwlockattr);

        c = sys_timestamp ();  
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadrwlock_init_test[i] = (c - b) - (b - a);			 
		}
		pthread_rwlock_destroy(&rwlock);
	} 

    pthread_rwlockattr_destroy(&rwlockattr);
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthreadrwlock_init",pthreadrwlock_init_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthreadrwlock_init",pthreadrwlock_init_test, TESTING_COUNT, 0);
#endif
       
}



void testing_pthreadrwlock_destroy() 
{
	int i;
	u64		a, b, c ;  
	pthread_rwlockattr_t rwlockattr;	
	pthread_rwlock_t rwlock;
	
    tick_set(0);
    
    pthread_rwlockattr_init(&rwlockattr);
    
    pthread_rwlock_init(&rwlock, &rwlockattr);
    pthread_rwlock_destroy(&rwlock);
    
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
    	pthread_rwlock_init(&rwlock, &rwlockattr);
    	
		a = sys_timestamp();
		b = sys_timestamp();
	
		pthread_rwlock_destroy(&rwlock);

        c = sys_timestamp ();  
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadrwlock_destroy_test[i] = (c - b) - (b - a);			 
		}
		
	} 

    pthread_rwlockattr_destroy(&rwlockattr);
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthreadrwlock_destroy",pthreadrwlock_destroy_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthreadrwlock_destroy",pthreadrwlock_destroy_test, TESTING_COUNT, 0);
#endif
       
}
 

void testing_pthreadrwlock_rdlock() 
{
	int i;
	u64		a, b, c ;  
	pthread_rwlockattr_t rwlockattr;	
	pthread_rwlock_t rwlock;
	
    tick_set(0);
    
    pthread_rwlockattr_init(&rwlockattr);    
    pthread_rwlock_init(&rwlock, &rwlockattr);
    
    pthread_rwlock_rdlock(&rwlock);
    pthread_rwlock_unlock(&rwlock);
    		
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		 
    	
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_rwlock_rdlock(&rwlock); 

        c = sys_timestamp ();  
 
        pthread_rwlock_unlock(&rwlock);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadrwlock_rdlock_test[i] = (c - b) - (b - a);			 
		}
		
	} 
    pthread_rwlock_destroy(&rwlock);
    pthread_rwlockattr_destroy(&rwlockattr);
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthreadrwlock_rdlock",pthreadrwlock_rdlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthreadrwlock_rdlock",pthreadrwlock_rdlock_test, TESTING_COUNT, 0);
#endif
       
}



void testing_pthreadrwlock_wrlock() 
{
	int i;
	u64		a, b, c ;  
	pthread_rwlockattr_t rwlockattr;	
	pthread_rwlock_t rwlock;
	
    tick_set(0);
    
    pthread_rwlockattr_init(&rwlockattr);    
    pthread_rwlock_init(&rwlock, &rwlockattr);
    
    pthread_rwlock_wrlock(&rwlock);
    pthread_rwlock_unlock(&rwlock);
    		
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		 
    	
		a = sys_timestamp();
		b = sys_timestamp();
		
		 pthread_rwlock_wrlock(&rwlock);

        c = sys_timestamp ();  
 
        pthread_rwlock_unlock(&rwlock);
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadrwlock_wrlock_test[i] = (c - b) - (b - a);			 
		}
		
	} 
    pthread_rwlock_destroy(&rwlock);
    pthread_rwlockattr_destroy(&rwlockattr);
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthreadrwlock_wrlock",pthreadrwlock_wrlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthreadrwlock_wrlock",pthreadrwlock_wrlock_test, TESTING_COUNT, 0);
#endif
       
}

 

void testing_pthreadrwlock_unlock() 
{
	int i;
	u64		a, b, c ;  
	pthread_rwlockattr_t rwlockattr;	
	pthread_rwlock_t rwlock;
	
    tick_set(0);
    
    pthread_rwlockattr_init(&rwlockattr);    
    pthread_rwlock_init(&rwlock, &rwlockattr);
    
    pthread_rwlock_wrlock(&rwlock);
    pthread_rwlock_unlock(&rwlock);
    		
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		 
    	pthread_rwlock_wrlock(&rwlock);
		
    	a = sys_timestamp();
		b = sys_timestamp();

		pthread_rwlock_unlock(&rwlock);

        c = sys_timestamp ();  
 
     
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadrwlock_wrlock_test[i] = (c - b) - (b - a);			 
		}
		
	} 
    pthread_rwlock_destroy(&rwlock);
    pthread_rwlockattr_destroy(&rwlockattr);
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthreadrwlock_unlock",pthreadrwlock_unlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthreadrwlock_unlock",pthreadrwlock_unlock_test, TESTING_COUNT, 0);
#endif
       
}
void testing_pthreadrwlock()
{
	
 
	testing_pthreadrwlock_init(); 	
	testing_pthreadrwlock_destroy();
	testing_pthreadrwlock_rdlock();
	testing_pthreadrwlock_wrlock();  
	testing_pthreadrwlock_unlock(); 
	 
	
}
