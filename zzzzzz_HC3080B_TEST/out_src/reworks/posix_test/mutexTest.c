#include "../basic/testing.h" 
#include "posix_test.h"

static u64 pthread_mutex_init_test[TESTING_COUNT]; 
static u64 pthread_mutex_destroy_test[TESTING_COUNT]; 
static u64 pthread_mutex_lock_test[TESTING_COUNT]; 
static u64 pthread_mutex_unlock_test[TESTING_COUNT]; 


 void testing_pthread_mutex_init()
{
	int i;
	u64		a, b, c ;  
    pthread_mutex_t mutex;
    int ret;

 
    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
    	printf("pthread_mutex_init Error\n");
    	return;
    }
	pthread_mutex_destroy(&mutex);
	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {			
		a = sys_timestamp ();
	    b = sys_timestamp ();
	    ret = pthread_mutex_init (&mutex, NULL);
	    c = sys_timestamp ();
	    if (ret != 0)
	    {
	    	printf("pthread_mutex_init Error\n");
	    	return;
	    }
		pthread_mutex_destroy(&mutex);		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_mutex_init_test[i] = (c - b) - (b - a);			 
		}
	}
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("pthread_mutex_init",pthread_mutex_init_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_mutex_init",pthread_mutex_init_test, TESTING_COUNT, 0);
#endif

}

 void testing_pthread_mutex_destroy()
{
	int i;
	u64		a, b, c ;  
    pthread_mutex_t mutex;
    pthread_mutex_t mutex1;
    pthread_mutex_t mutex2;
    pthread_mutex_t mutex3;
    pthread_mutex_t mutex4;
    int ret;
    
    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
    	printf("pthread_mutex_init Error\n");
    	return;
    }
	pthread_mutex_destroy(&mutex);
	/* 测试并记录测试结果*/
	tick_set(0);
	
	for(i = 0; i < TESTING_COUNT; i++) {			

	    pthread_mutex_init (&mutex1, NULL);
	    pthread_mutex_init (&mutex2, NULL);
	    pthread_mutex_init (&mutex3, NULL);
	    pthread_mutex_init (&mutex4, NULL);
	    
		a = sys_timestamp ();
	    b = sys_timestamp ();
	    
		pthread_mutex_destroy(&mutex1);		
		pthread_mutex_destroy(&mutex2);	
		pthread_mutex_destroy(&mutex3);	
		ret = pthread_mutex_destroy(&mutex4);	
		
	    c = sys_timestamp ();
	   if(ret != 0) printf("ret = %d\n",ret);
	   
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_mutex_init_test[i] = ((c - b) - (b - a))/4;			 
		}
	}
	 	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("pthread_mutex_destroy",pthread_mutex_destroy_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_mutex_destroy",pthread_mutex_destroy_test, TESTING_COUNT, 0);
#endif

}

void testing_pthread_mutex_lock()
{
	int i;
	u64		a, b, c ;  
    pthread_mutex_t mutex; 
 
    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
    	printf("pthread_mutex_init Error\n");
    	return;
    }
    if(pthread_mutex_lock(&mutex)!= 0)
    {
    	printf("pthread_mutex_lock Error\n");
    	return;
    }
    if(pthread_mutex_unlock(&mutex)!= 0)
    {
    	printf("pthread_mutex_unlock Error\n");
    	return;
    }

	/* 测试并记录测试结果*/
	tick_set(0);	
   for(i = 0; i < TESTING_COUNT; i++) {		
	  
		a = sys_timestamp ();
		b = sys_timestamp ();
		pthread_mutex_lock (&mutex);
		
		c = sys_timestamp ();
		pthread_mutex_unlock(&mutex);
	
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_mutex_lock_test[i] = (c - b) - (b - a);			 
		}
	}
	pthread_mutex_destroy(&mutex);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("pthread_mutex_lock",pthread_mutex_lock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_mutex_lock",pthread_mutex_lock_test, TESTING_COUNT, 0);
#endif

}
void testing_pthread_mutex_unlock()
{
	int i;
	u64		a, b, c ;  
    pthread_mutex_t mutex; 

    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
    	printf("pthread_mutex_init Error\n");
    	return;
    }
    if(pthread_mutex_lock(&mutex)!= 0)
    {
    	printf("pthread_mutex_lock Error\n");
    	return;
    }
    if(pthread_mutex_unlock(&mutex)!= 0)
    {
    	printf("pthread_mutex_unlock Error\n");
    	return;
    }

	/* 测试并记录测试结果*/
	tick_set(0);	
   for(i = 0; i < TESTING_COUNT; i++) {		
		pthread_mutex_lock (&mutex);
		
		a = sys_timestamp ();
		b = sys_timestamp ();
		
		pthread_mutex_unlock(&mutex);
		
		c = sys_timestamp ();

	
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_mutex_unlock_test[i] = (c - b) - (b - a);			 
		}
	}
	pthread_mutex_destroy(&mutex);
	
	/* 测试完成，对测试结果进行统计分析 */	 	
#ifdef ANALYSIS
	inter_statistical("pthread_mutex_lock",pthread_mutex_unlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_mutex_lock",pthread_mutex_unlock_test, TESTING_COUNT, 0);
#endif

}

void testing_mutex()
{ 
	
	testing_pthread_mutex_init();
	
	testing_pthread_mutex_destroy();
	
	testing_pthread_mutex_lock();
	
	testing_pthread_mutex_unlock();	
}



