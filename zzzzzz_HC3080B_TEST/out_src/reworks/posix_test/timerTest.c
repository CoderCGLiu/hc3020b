#include "../basic/testing.h" 
#include "posix_test.h"
static u64 TimerCreate_test[TESTING_COUNT]; 
static u64 TimerDelete_test[TESTING_COUNT];   
  
#define SIGTOTEST SIGALRM

void testing_TimerCreate() 
{
	int i;
	u64		a, b, c ;   
	timer_t tid;
	struct sigevent ev = {0};
	
	ev.sigev_notify = SIGEV_SIGNAL;
	ev.sigev_signo = SIGTOTEST;
	
    tick_set(0); 
    
    timer_create(CLOCK_REALTIME, &ev, &tid);
    timer_delete(tid);
    
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
		a = sys_timestamp();
		b = sys_timestamp();
	
		timer_create(CLOCK_REALTIME, &ev, &tid);

        c = sys_timestamp ();  
        
        timer_delete(tid);
        
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			TimerCreate_test[i] = (c - b) - (b - a);			 
		} 
	}  
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("timer_create",TimerCreate_test, TESTING_COUNT, 1);
#else
	inter_statistical("timer_create",TimerCreate_test, TESTING_COUNT, 0);
#endif
       
}




void testing_TimerDelete() 
{
	int i;
	u64		a, b, c ;   
	timer_t tid;
	struct sigevent ev = {0};
	
	ev.sigev_notify = SIGEV_SIGNAL;
	ev.sigev_signo = SIGTOTEST;
	
    tick_set(0); 
    
    timer_create(CLOCK_REALTIME, &ev, &tid);
    timer_delete(tid);
    
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
    	timer_create(CLOCK_REALTIME, &ev, &tid);
    	
		a = sys_timestamp();
		b = sys_timestamp();
	
		timer_delete(tid);

        c = sys_timestamp ();   
      
        
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			TimerDelete_test[i] = (c - b) - (b - a);			 
		} 
	}  
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("timer_delete",TimerDelete_test, TESTING_COUNT, 1);
#else
	inter_statistical("timer_delete",TimerDelete_test, TESTING_COUNT, 0);
#endif
       
}

void testing_Timer()
{
	
 
	testing_TimerCreate(); 	
	testing_TimerDelete();   
	 
	
}
