#include "../basic/testing.h" 
#include "posix_test.h"
static u64 pthread_create_test[TESTING_COUNT];  
static u64 pthread_createDelete_test[TESTING_COUNT]; 
static u64 pthreadDelete_test[TESTING_COUNT]; 

static u64 pthread_suspend_test[TESTING_COUNT]; 
static u64 pthread_resume_test[TESTING_COUNT]; 

static u64 pthread_lock_test[TESTING_COUNT];
static u64 pthread_unlock_test[TESTING_COUNT];
static u64 pthread_unlockNoLock_test[TESTING_COUNT];
 
static u64 pthread_setschedprio_test[TESTING_COUNT];
static u64 pthread_getschedprio_test[TESTING_COUNT];

#ifdef __multi_core__
#define pthread_lock taskCpuLock
#define pthread_unlock taskCpuLock
#endif

static void *bmTaskDummy ()
{
 
	
    while (1)
        ;
}





void testing_pthread_create() 
{
	int i;
	u64		a, b, c ;  
	pthread_t task;  

	pthread_create(&task, NULL,bmTaskDummy, NULL);
 
    tick_set(0);
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
		a = sys_timestamp();
		b = sys_timestamp();
	
 
		pthread_create(&task, NULL,bmTaskDummy, NULL); 

        c = sys_timestamp ();  
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_create_test[i] = (c - b) - (b - a);			 
		}
		pthread_cancelforce(task);
	} 

 
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthread_create",pthread_create_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_create",pthread_create_test, TESTING_COUNT, 0);
#endif
       
}
void testing_pthread_createDelete() 
{
	int i;
	u64		a, b, c ;  
	pthread_t task;  
 
	/* load into the cache */
	pthread_create(&task, NULL,bmTaskDummy, NULL);
	pthread_cancelforce (task);
	
    tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		

		a = sys_timestamp();
		b = sys_timestamp();
	
		pthread_create(&task, NULL,bmTaskDummy, NULL);
		pthread_cancelforce (task);
		
		c = sys_timestamp ();
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_createDelete_test[i] = (c - b) - (b - a);	
		}
	}
	
 
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("pthread_createDelete",pthread_createDelete_test, TESTING_COUNT, 1);
#else
	 inter_statistical("pthread_createDelete",pthread_createDelete_test, TESTING_COUNT, 0);
#endif
}
void testing_pthreadDelete() 
{
	int i;
	u64		a, b, c ;  
	pthread_t task;   
     
    tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		pthread_create(&task, NULL,bmTaskDummy, NULL);
		 
		a = sys_timestamp();
		b = sys_timestamp(); 
			
		pthread_cancelforce(task); 
		
		c = sys_timestamp (); 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthreadDelete_test[i] = (c - b) - (b - a);	
		}
	}
 
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("taskDelete",pthreadDelete_test, TESTING_COUNT, 1);
#else
	 inter_statistical("taskDelete",pthreadDelete_test, TESTING_COUNT, 0);
#endif
}



void testing_pthread_suspend()
{
	int i;
	u64		a, b, c ;  
	pthread_t task;    
 
	/* 创建任务 */
	pthread_create(&task, NULL,bmTaskDummy, NULL);


	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) 
	{		
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_suspend (task);
		
		c = sys_timestamp();	
 
		pthread_resume (task);
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_suspend_test[i] = (c - b) - (b - a);			 
		}
	}
	 pthread_cancelforce(task); 
	
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	 inter_statistical("pthread_suspend",pthread_suspend_test, TESTING_COUNT, 1);
#else
	 inter_statistical("pthread_suspend",pthread_suspend_test, TESTING_COUNT, 0);
#endif

}


 

void testing_pthread_resume()
{
	int i;
	int task; 	 
	u64		a, b, c ;   
	  
	/* 创建任务 */
	pthread_create(&task, NULL,bmTaskDummy, NULL);
 
	while (pthread_is_ready(task) == FALSE)
		;
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) { 
 
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_resume (task);	
		pthread_resume (task);
		pthread_resume (task);	
		pthread_resume (task);
		pthread_resume (task);	
		
		c = sys_timestamp();	 
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_resume_test[i] = ((c - b) - (b - a))/5;			 
		}
	}
 
	pthread_cancelforce(task);

 
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("pthread_resume",pthread_resume_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_resume",pthread_resume_test, TESTING_COUNT, 0);
#endif

}


void testing_pthread_lock()
{
	int i;
	u64		a, b, c ;  
	  
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) { 
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		
		c = sys_timestamp();	
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
	

		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_lock_test[i] = ((c - b) - (b - a))/10;			 
		}
	}
  
	/* 测试完成，对测试结果进行统计分析 */	 
	
	#ifdef ANALYSIS
		inter_statistical("pthread_lock",pthread_lock_test, TESTING_COUNT, 1);
	#else
		inter_statistical("pthread_lock",pthread_lock_test, TESTING_COUNT, 0);
	#endif

}


void testing_pthread_unlock()
{
	int i;  
	u64		a, b, c ;   
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 

		
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
		pthread_lock(); 
			
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		
		c = sys_timestamp();	 
 
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_unlock_test[i] = ((c - b) - (b - a))/5;			 
		}
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	
#ifdef ANALYSIS
	inter_statistical("pthread_unlock",pthread_unlock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_unlock",pthread_unlock_test, TESTING_COUNT, 0);
#endif

}

void testing_pthread_unlockNoLock()
{
	int i; 		 
	u64		a, b, c ;  
    
	/* 测试并记录测试结果*/
	tick_set(0);
	for(i = 0; i < TESTING_COUNT; i++) {	 
 
			
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock();  
		
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		pthread_unlock(); 
		
		
		c = sys_timestamp();  
		
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_unlockNoLock_test[i] = ((c - b) - (b - a))/10;			 
		}
	} 
	
	/* 测试完成，对测试结果进行统计分析 */	 
#ifdef ANALYSIS
	inter_statistical("pthread_unlockNoLock",pthread_unlockNoLock_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_unlockNoLock",pthread_unlockNoLock_test, TESTING_COUNT, 0);
#endif
}

void testing_pthread_setschedprio() 
{
	int i;
	u64		a, b, c ;  
	pthread_t task;  
	int prio;

	pthread_create(&task, NULL,bmTaskDummy, NULL);\
	
	pthread_setschedprio(task, 160);
	pthread_getschedprio(task, &prio);
	if (prio != 160)
	{
		printf("pthread_setschedprio Fail\n");
		return;
	}
    tick_set(0);
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_setschedprio(task, 160);
		
        c = sys_timestamp ();  
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_setschedprio_test[i] = (c - b) - (b - a);			 
		}
	} 
	pthread_cancelforce(task);
 
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthread_setschedprio",pthread_setschedprio_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_setschedprio",pthread_setschedprio_test, TESTING_COUNT, 0);
#endif
       
}


void testing_pthread_getschedprio() 
{
	int i;
	u64		a, b, c ;  
	pthread_t task;  
	int prio;

	pthread_create(&task, NULL,bmTaskDummy, NULL);
	pthread_setschedprio(task, 160);
	pthread_getschedprio(task, &prio);
	if (prio != 160)
	{
		printf("pthread_setschedprio Fail\n");
		return;
	}
    tick_set(0);
    for (i = 0; i < TESTING_COUNT; i++)
	{	
		
		a = sys_timestamp();
		b = sys_timestamp();
		
		pthread_getschedprio(task, &prio);
		
        c = sys_timestamp ();  
 
		/* 有效性检测 */
		if(tick_get() > 0) {			
			i--; /* 产生了时钟中断，测量无效 */	
			tick_set(0);			
		} else if ((a > b) || (b > c) || (c - b) < (b - a)){			
			i--; /* 没有产生中断，或者时钟溢出，测量无效，实际上有可能是错误 */
		} else {
			pthread_getschedprio_test[i] = (c - b) - (b - a);			 
		}
	} 
	pthread_cancelforce(task);
 
 
	/* 测试完成，对测试结果进行统计分析 */	  
#ifdef ANALYSIS
	inter_statistical("pthread_getschedprio",pthread_getschedprio_test, TESTING_COUNT, 1);
#else
	inter_statistical("pthread_getschedprio",pthread_getschedprio_test, TESTING_COUNT, 0);
#endif
       
}

void testing_pthread()
{ 
 
	testing_pthread_create();  
	testing_pthread_createDelete();
	testing_pthreadDelete();

	testing_pthread_lock(); 
	testing_pthread_unlock(); 
	testing_pthread_unlockNoLock(); 
}


 
