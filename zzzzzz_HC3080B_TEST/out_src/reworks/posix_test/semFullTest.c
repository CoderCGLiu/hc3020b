#include "../basic/testing.h" 
#include "posix_test.h"
void testing_semfull_init()
{
	bmsem_init (BM_SEM_FULL);

}

void testing_semfull_destroy()
{
	bmsem_destroy (BM_SEM_FULL);

}

void testing_semfull_flush()
{
	bmsem_flush (BM_SEM_FULL);

}

void testing_semfull_wait()
{
	bmsem_wait (BM_SEM_FULL);

}

void testing_semfull_post()
{
	bmsem_post(BM_SEM_FULL);
}

void testing_semfull_trywait()
{
	bmsem_trywait (BM_SEM_FULL);

}

void testing_semfull()
{
	 
	testing_semfull_init();
	
	testing_semfull_destroy();
	
	testing_semfull_flush();
	
	testing_semfull_wait();
	
	testing_semfull_post();
	
	testing_semfull_trywait();

	
}
