#include<stdio.h>
#include<unistd.h>
#include <time.h>

#include "hr3APIs.h"
#include "./gvar.h"

extern void test_lpcflash_rw(unsigned int testAddr, unsigned int test_len, unsigned char first_data, int is_DumpData);
extern int case_bsp_tests_can_1(void);
extern void test_i2c_eeprom_rw(int channel, unsigned char test_addr, unsigned int test_len, unsigned int first_data, int is_DumpData);

int test_timer_callback2(u64 param)
{
//	static int i = 0;
//	
//	if(i)
//	{
		printk("<INFO> [%s:__%d__]::  --- Timer%d ---\n",__FUNCTION__,__LINE__,param);
//		i = 0;
//	}
//	else
//		i = 1;
	return 0;
}

u32 cpuID;
u32 dspID;
int g_slotNum;
unsigned char dead_flag = 0;

#if 1 /*ldf 20230605 add*/
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			printf("Check failed! index=%d, val=0x%x, except_val=0x%x\n",\
					index,valDst,valSrc);
			return -1;
		}
    }
	printf("Check OK!\n");
	return 0;
}

#define TEST_BUF_LEN	16
int test_lpc_rw(void)
{
	unsigned int  testAddr = 0xffefd500;
	unsigned char testData[TEST_BUF_LEN] = {
			0x12,0x34,0x56,0x78,
			0x9a,0xbc,0xde,0xf0,
			0x21,0x43,0x65,0x87,
			0xa9,0xcb,0xed,0x0f
	};
	unsigned char rbuf[TEST_BUF_LEN] = {0};
	int ret = 0;
	
	bslLpcReadData(testAddr, sizeof(rbuf)/sizeof(rbuf[0]), rbuf);
	printf("lpc read finished.\n");
	DumpUint8(rbuf, sizeof(rbuf)/sizeof(rbuf[0]));
	
	bslLpcWriteData(testAddr, sizeof(testData)/sizeof(testData[0]), testData);
	printf("lpc write finished.\n");
	
	bslLpcReadData(testAddr, sizeof(rbuf)/sizeof(rbuf[0]), rbuf);
	printf("lpc read finished.\n");
	DumpUint8(rbuf, sizeof(rbuf)/sizeof(rbuf[0]));
	
	ret = CheckUint8(testData, rbuf, TEST_BUF_LEN);
	if(ret < 0)
	{
		printf("lpc test failed.\n");
		return -1;
	}
	printf("lpc test successed.\n");
	return 0;
	
}
#endif

void tMenu()
{   
	printf("HR3集成测试：\n");
	printf("0 -----测试菜单 \n");
	printf("1 -----基本硬件特性测试\n");
//	printf("2 -----机箱、槽位、节点号获取测试\n");
	printf("3 -----单板复位测试\n");
	printf("4 -----DDR空间读写校验测试\n");
	printf("5 -----GDMA传输功能测试\n");
	printf("6 -----GDMA传输性能测试\n");
	printf("7-----CDMA传输功能测试\n");
	printf("8-----CDMA传输性能测试\n");
//	printf("9 -----Nand Flash 擦除功能测试\n");
	printf("10 -----Nand Flash 读写功能测试\n");
	printf("11-----Nand Flash 性能测试\n");
	printf("12 -----SPI Flash 功能测试\n");
	printf("13 -----SPI Flash 性能测试\n");
//	printf("14-----SMP FFT计算测试\n");
//	printf("15-----srioTestFun(SRIO0, 0)\n");
//	printf("16-----srioTestFun(SRIO0, 1)\n");
//	printf("17-----srioTestFun(SRIO0, 2)\n");
//	printf("18-----srioTestFun(SRIO1, 0)\n");
//	printf("19-----srioTestFun(SRIO1, 1)\n");
//	printf("20-----srioTestFun(SRIO1, 2)\n");
//	printf("21-----SRIO A发B（发送端统计速率，2端口并发）\n");
//	printf("22-----SRIO B发A（发送端统计速率，2端口并发）\n");
//	printf("23-----FFT拷机测试\n");
//	printf("24-----LPC空间测试\n");
//	printf("25-----延时与测时接口测试\n");
	printf("26-----memcpy测试\n");
	printf("27-----任务绑定核测试\n");
//	printf("28-----面板LED灯测试\n");
	printf("29-----中断响应性能测试\n");
//	printf("30-----网络TCP性能测试(目的ID、包长、端口可选)\n");
//	printf("31-----网络UDP性能测试(目的ID、包长、端口可选)\n");
//	printf("32-----网络TCP传输测试(接收端数据校验)\n");
//	printf("33-----网络UDP传输测试(接收端数据校验)\n");
//	printf("34-----网络UDP组播传输测试(接收端数据校验)\n");
//	printf("35-----OS(ReWorks)核心接口测试\n");
	printf("36-----malloc_free测试\n");
	printf("37-----spinlock测试\n");
//	printf("38-----dpc脉压测试\n");
//	printf("39-----异常日志测试\n");
//	printf("40-----中断回调函数测试\n");
//	printf("41-----1800枚举配置\n");
//    printf("42-----malloc_free、spinlock、dpc脉压三合一测试\n");
//    printf("43-----FFT计算结果校验测试\n");
//    printf("44-----电子标签测试\n");
//	printf("45-----PCIE mem读写测试, dsp0 -> dsp1\n");
//	printf("46-----PCIE mem读写测试, dsp1 -> dsp0\n");
	printf("47 -----Qspi Flash 功能测试\n");
	printf("48 -----Qspi Flash 性能测试\n");
	printf("49 -----SRIO两路对接测试，0发1\n");
	printf("50 -----SRIO两路对接测试，1发0\n");
	printf("51 -----LPC FLASH功能测试\n");
	printf("52 -----CAN两路对接测试\n");
	printf("53 -----I2C测试\n");
	printf("54 -----TIMER测试\n");
	
    printf("100----退出\n");
}

int demo(void)
{
	int casenum;
	u32 bTrue = 1;
	int err_num = 0;
	int ok_num = 0;
	
	dspID = bslProcGetId();
	cpuID = bslCoreGetId();
	g_slotNum = bslGetSlotNum();
	
	printf(" DSP%d CORE%d   Hello Reworks!\n",dspID,cpuID);
	
	printf("\r\n");	
	usleep(200000);

	tMenu(); 
					
	while(bTrue)
	{
		printf("--------------------------------------\n");
		printf("Input Case Num.\n");
		printf("--------------------------------------\n");
		scanf("%d", &casenum);
		printf("casenum = %d\n", casenum);
		if(ok_num>100)
		{
			printf("NOTE:测试次数超过100次，请重新进入hr2_demo.\n");
			return;
		}
		if(err_num>100)
		{
			printf("NOTE:不正确操作超过100次，请重新进入hr2_demo.\n");
			return;
		}
		switch(casenum)
		{
			case 0:
				tMenu(); 
				ok_num++;
				break;
			case 1:
				tstBHC();
				ok_num++;
				break;
//			case 2:
//				tstModueInfo();
//				ok_num++;
//				break;
			case 3:  	//单板复位测试
				tstHR3BoardReset();
				ok_num++;
				break;
			case 4:				//DDR空间读写校验测试
				tstHR3DDRTest();
				ok_num++;
				break;
				
            case 5:                                 //gdma功能测试
//                tstGDMAtest();
            	tstHR3GDMATest_hr3(0x1080000000, 0x10c0000000, 0x1180000000, 0x11c0000000, 0x100000, 1);
            	ok_num++;
                break;

            case 6:                                 //gdma性能测试
//                tstGDMAperf();
            	tstHR3GDMAPerf_hr3(0x1080000000, 0x10c0000000, 0x1180000000, 0x11c0000000, 1000);
            	ok_num++;
                break;
 
            case 7:                                 //cdma功能测试
//            	tstCDMAtest();
            	tstHR3CDMATest_hr3(0x1080000000, 0x10c0000000, 0x1180000000, 0x11c0000000, 0x100000, 1);
            	ok_num++;
                break;

            case 8:                                 //cdma性能测试
//                tstCDMAperf();
            	tstHR3CDMAPerf_hr3(0x1080000000, 0x10c0000000, 0x1180000000, 0x11c0000000, 1000);
            	ok_num++;
                break;

//            case 9:                                 //nand擦除功能测试
//				tstNandflashErase();
//				ok_num++;
//				break;

			case 10:                                 //nand功能测试
				tstNandflashFunc();
				ok_num++;
				break;

			case 11:                                //nand性能测试
				tstNandflashPerf();
				ok_num++;
				break;

			case 12:
				tstNorflashFunc();	//SPI Norflash功能测试
				ok_num++;
				 break;
			case 13:
				tstNorflashPerf();	//SPI Norflash性能测试
				ok_num++;
				break;
				
//			case 14:
//				tstDspSmp();	//SMP FFT计算测试
//				ok_num++;
//				break;
				
			case 15:
				srioTestFun(SRIO0, 0);//平均传输速率=2001.720MB/s
				ok_num++;
				break;
			case 16:
				srioTestFun(SRIO0, 1);//平均传输速率=2001.438MB/s
				ok_num++;
				break;
			case 17:
				srioTestFun(SRIO0, 2);//平均传输速率=1880.541MB/s
				ok_num++;
				break;
			case 18:
				srioTestFun(SRIO1, 0);//平均传输速率=3145.808MB/s
				ok_num++;
				break;
			case 19:
				srioTestFun(SRIO1, 1);//平均传输速率=3147.512MB/s
				ok_num++;
				break;
			case 20:
				srioTestFun(SRIO1, 2);//平均传输速率=2645.505MB/s
				ok_num++;
				break;
			case 21:		//SRIO A发B（发送端统计速率，2端口并发）
				srioTestFun(SRIO0, 0);//平均传输速率=1911.158MB/s
				srioTestFun(SRIO1, 0);//平均传输速率=2472.521MB/s
				ok_num++;
				break;
			case 22:		//SRIO B发A（发送端统计速率，2端口并发）
				srioTestFun(SRIO0, 1);//平均传输速率=1963.266MB/s
				srioTestFun(SRIO1, 1);//平均传输速率=2271.929MB/s
				ok_num++;
				break;
//            case 23:
//                tstHR3App();	//FFT拷机测试
//                ok_num++;
//                break;
//            case 24:			//LPC空间测试
////                tstHR3App();	
//            	test_lpc_rw(); /*ldf 20230605 add*/
//                ok_num++;
//                break;
            case 25:
            	time_delay_test();	//延时与测时接口测试
            	ok_num++;
				break;
            case 26:	//memcpy测试
				tstMemcpy(0x1080000000, 0x1180000000, 0x10000000);
				ok_num++;
				break;
            case 27:	//任务绑定核测试
            	task_bind_core_test();
            	ok_num++;
            	break;
//            case 28:	//面板LED灯测试
//            	tstLedFunc();
//            	ok_num++;
//            	break;
            case 29:	//中断响应性能测试
            	tstGpioPerf();
            	ok_num++;
            	break;
            	
//            case 30:	//网络TCP性能测试(目的ID、包长、端口可选)
//#ifdef NEW_LIB_32
//				/*2022.07.02,csz,32所库ipcomtelnet关联在0核，未绑核存在问题，telnet无响应无打印，绑3核*/
//				res = bslTaskCreateCore("_hr2NetTestTask",100,0,0x100000,(FUNCPTR)hr2_net_test,3,0,1,2,3,4,5,6,7,8,9);
//				if(res != ERROR)
//				{
//					printf("<L>hr2_net_test Info: Create _hr2_net_testTask Successful!\n");
//				}else
//				{
//					printf("<L>hr2_net_test Info: Create _hr2_net_testTask failure!\n");
//				}
//#else
//				hr3_net_test();
//#endif
//            	ok_num++;
//            	break;
//            case 31:	//网络UDP性能测试(目的ID、包长、端口可选)
//#ifdef NEW_LIB_32
//				/*2022.07.02,csz,32所库ipcomtelnet关联在0核，未绑核存在问题，telnet无响应无打印，绑3核*/
//				res = bslTaskCreateCore("_hr2NetTestTask",100,0,0x100000,(FUNCPTR)hr2_net_test,3,0,1,2,3,4,5,6,7,8,9);
//				if(res != ERROR)
//				{
//					printf("<L>hr2_net_test Info: Create _hr2_net_testTask Successful!\n");
//				}else
//				{
//					printf("<L>hr2_net_test Info: Create _hr2_net_testTask failure!\n");
//				}
//#else
//            	hr3_net_test();
//#endif
//            	ok_num++;
//            	break;
//            case 32:	//网络TCP传输测试(接收端数据校验)
//            	tstTcp();
//            	ok_num++;
//               	break;
//            case 33:	//网络UDP传输测试(接收端数据校验)
//            	UDPPEERTEST();
//            	ok_num++;
//               	break;
//            case 34:	//网络UDP组播传输测试(接收端数据校验)
//            	testGmacCastFun();
//            	ok_num++;
//               	break; 	
//            case 35:	//OS(ReWorks)核心接口测试
//            	reworks_test();
//            	ok_num++;
//            	break;     	
            case 36:	//malloc_free测试
            	printf("malloc free测试\n");
//            	test_main(5);
            	test_malloc_free();
            	ok_num++;
            	break;
            case 37:	/* spinlock测试 */
            	printf("spinlock测试\n");
            	my_spinlock();
            	ok_num++;
            	break;
//            case 38:	/* dpc脉压测试 */
//            	printf("dpc脉压测试\n");
//            	MY_dpc();
//            	ok_num++;
//            	break;
//            case 39:	/* 异常日志测试 */
//             	printf("异常日志测试\n");
//             	exception_fun(); 
//             	ok_num++;
//             	break;
//			case 40:	/*xxx ldf 20230608:: gpio中断处理不会调用回调函数，需要在应用层获取gpio信号量*/
//				/********add by zwx start********/
//				/*中断回调函数测试*/
//				gpio_interrupt_connect_test();//挂接gpio中断回调函数
//				break;	
//			    /********add by zwx end********/	
//			case 41:
//				/********add by lijun start********/
//				/*1800枚举（为保证测试用例正常运行应先在开始测试前枚举1800）*/
//				dsp403_enum_demo();
//				break;	
			    /********add by lijun end********/	
//		    case 42:	//malloc_free、spinlock、dpc脉压三合一测试
//		    	dead_flag = 1;
//				printf("malloc free测试\n");
//				test_main(5);
//				printf("spinlock测试\n");
//				my_spinlock();
//				printf("dpc脉压测试\n");
//				MY_dpc();
//				ok_num++;
//				break;
//			case 43:	//FFT计算结果校验测试
//				printf("FFT计算结果校验测试\n");
//				ffttest();
//				ok_num++;
//				break;
//			case 44:	//电子标签测试
//				printf("电子标签测试\n");
//				bslShoweLabel();
//				ok_num++;
//				break;

			case 45:	//PCIE mem读写测试, dsp0 -> dsp1
				printf("PCIE mem读写测试, dsp0 -> dsp1\n");
				test_pciex16_mem_rw(0);
				ok_num++;
				break;
			case 46:	//PCIE mem读写测试, dsp1 -> dsp0
				printf("PCIE mem读写测试, dsp1 -> dsp0\n");
				test_pciex16_mem_rw(1);
				ok_num++;
				break;
				
			case 47:
				tstQspiflashFunc();	//Qspiflash功能测试
				ok_num++;
				 break;
			case 48:
				tstQspiflashPerf();	//Qspiflash性能测试
				ok_num++;
				break;
				
			case 49:
				srioTestFun2(0);	//SRIO两路对接测试，0发1
				ok_num++;
				break;
			case 50:
				srioTestFun2(1);	//SRIO两路对接测试，1发0
				ok_num++;
				break;
				
			case 51:		//LPC FLASH功能测试
				test_lpcflash_rw(0xffe52000, 4096, 0x12, 0);	
				ok_num++;
				break;
			case 52:			//CAN两路对接测试
				case_bsp_tests_can_1();
				ok_num++;
				break;
			case 53:				//I2C测试
				test_i2c_eeprom_rw(1, 0x100, 0x1000, 0x12, 0);
				ok_num++;
				break;
			case 54:				//TIMER测试
				bslTimerInit(1, TIMER_MODE_CYCLE, 0x12345678, test_timer_callback2, 1);
				bslTimerSetState(0, 1);
				ok_num++;
				break;
			
			case 100:
				bTrue=0;
				break;
			default:
				err_num++; 
				if(err_num > 10)
				{
					printf("\n\r");
				}
				printf("Please Input Correct Number!\n");
				break;
		}
	}
	
//	enableShell(); 
	printf("Test Exit!\n");
	
	return 0;
	
}

	
