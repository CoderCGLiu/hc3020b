#include<stdio.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>
#include<mqueue.h>
#include <fcntl.h>

#include "hr3APIs.h"
#include "../gvar.h"

#include "../../mylog/mylog.h"/*ldf 20230606 add:: log*/

extern unsigned int bslProcGetId(void);
typedef enum {SRIO00 = 0, SRIO11 =1} sriox;
extern void rab_link_and_mode_status(sriox base_addr);

#define MAX_RIO_ADDRESS 0x10000000

struct NET_CON_RIOPAK{
    unsigned int packetNum;
    unsigned int packetIndex;
    unsigned int packetLength;
    unsigned int packetDelay;
};

static union datacast_64 memAddr0, dataAddr0, rioAddr0, memAddr1, dataAddr1, rioAddr1;
static u32 g_uiRabID2[2];
static u8 TxType;
static sem_t SemStart;
static sem_t SemStartRx;
static sem_t SemRioDB;
static mqd_t MsgRioDB;


void srioInit2(void)
{
	u16 SRIO_0, SRIO_1;
	u32 windowSize = 0x10000000;
	
	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;
	
    //RAB0 ID
    g_uiRabID2[SRIO_0] = 0x10;
    //RAB1 ID
    g_uiRabID2[SRIO_1] = 0x20;
	    	
	bslRioSetID(SRIO_0, g_uiRabID2[SRIO_0]);	//设置ID	
	printf("<INFO> [%s:_%d_]:: 读取rab0 ID:0x%x\n", __FUNCTION__,__LINE__,bslRioGetID(SRIO_0));
	bslRioSetID(SRIO_1, g_uiRabID2[SRIO_1]);	//设置ID	
	printf("<INFO> [%s:_%d_]:: 读取rab1 ID:0x%x\n", __FUNCTION__,__LINE__,bslRioGetID(SRIO_1));

	memAddr0.l64 = 0x1080000000;
	memAddr1.l64 = 0x1180000000;
	dataAddr0.l64 = 0x10c0000000;
	dataAddr1.l64 = 0x11c0000000;
	rioAddr0.l32.h = 0;
	rioAddr0.l32.l = 0x40000000;//0x60000000; 
	rioAddr1.l32.h = 0; 
	rioAddr1.l32.l = 0x70000000;//0x70000000; 

	
	//设置接受窗
//	printf("<INFO> [%s:_%d_]:: 设置rab0接受窗\n", __FUNCTION__,__LINE__);
//	bslRioSetIW(SRIO_0, rioAddr0.l32.l, dataAddr0.l64, windowSize);
//	printf("<INFO> [%s:_%d_]:: 设置rab1接受窗\n", __FUNCTION__,__LINE__);
//	bslRioSetIW(SRIO_1, rioAddr1.l32.l, dataAddr1.l64, windowSize);

    //设置RAB0维护发送窗，大小1M
//	bslRioSetMaintOW(SRIO_0);
   // bslUsrRapidIOSetOW(SRIO0, 0, 0xffff, RAB_OW_MAINT_BASE_ADDR(SRIO0), _rioAddr0, 0x100000, RAB_APIO_MAINTENANCE);
//	/*等待RAB连接*/
//	rab_wait_for_linkup(SRIO_0);
//	rab_wait_for_linkup(SRIO_1);
	
	/*SRIO门铃初始化*/
	bslRioDBInit(SRIO_0, NULL);
	bslRioDBInit(SRIO_1, NULL);

	
	/*SRIO_1发送DoorBell测试*/
//	if(cpuID == 0)
//	{
//		printf("rab1发送测试门铃\n");
//		bslRioSendDB(SRIO_1, g_uiRabID2[SRIO_1][(cpuID+1)%2], 0xab14);
//	}
	
//	udelay(10000);
	    
    /*SRIO_0收取门铃消息*/
//    bslRioGetDBInfo(SRIO_0, &info, &id);

//    setRioIW();
//
//	if(bslProcGetId() == 0)
//		srio_enumerate();
    
//
//    sleep(1);
//
//    bspSrioDbInit();
    
}


//RAB接收信息统计
void tRioRecvInfo2(u8 controller)
{
    int iError = 0;
//    size_t stLen = 2;
    unsigned short uiRioAddrOffset = 0;
//    unsigned int *puiBuf;
//    unsigned int *puiBuf0;
    unsigned int uiSendNum = 0;
    unsigned int uiPrintSpace = 4000;
    unsigned long long ullTimeStart = 0;
    unsigned long long ullTimeStop = 0;
    unsigned long long ullTimeAll = 0;
//    struct timespec tsUs;
//    RapidIO_Snd SndParam;
    float fSpeed = 0.0;

    while(MsgRioDB == -1)
    {
        usleep(1000);
        //printf("RAB[%d]:等待消息队列创建成功.\n", controller);
    }
    
    printf("<INFO> [%s:_%d_]:: 执行tRioRecvInfo[%d]任务.\n", __FUNCTION__,__LINE__,controller);

    while(true)
    {
    	printf("<INFO> [%s:_%d_]:: ===================.\n", __FUNCTION__,__LINE__);
    	
        iError = mq_receive(MsgRioDB, (char*)&uiRioAddrOffset, sizeof(uiRioAddrOffset), NULL);
        if(iError <= 0)
        {
            continue;
        }
//        puiBuf = (unsigned int *)((unsigned long long)puiBuf0 + (uiRioAddrOffset * K32) % MAX_RIO_ADDRESS);
//        SndParam.transferLen = puiBuf[3];

        uiSendNum ++;
        if(uiSendNum % uiPrintSpace == 1)
        {
//            ullTimeStart = bslGetTimeUsec();
        }
        else if(uiSendNum % uiPrintSpace == 0)
        {
//            ullTimeStop = bslGetTimeUsec();
//            fSpeed = (float)((1.0 * uiPrintSpace) / ((ullTimeStop - ullTimeStart) / 1000000.0));
//            ullTimeAll += (ullTimeStop - ullTimeStart);
//            printf("<INFO> [%s:_%d_]:: RAB[%d]:接收第%u包,接收速率=%.3fMB/s.\n", __FUNCTION__,__LINE__,controller, uiSendNum, fSpeed);

        }

        if(uiSendNum >= (10 * uiPrintSpace))
        {
//            fSpeed = (float)((1.0 * uiSendNum) / (ullTimeAll / 1000000.0));
//            printf("<INFO> [%s:_%d_]:: RAB[%d]:总共接收%u包(1MB/包),平均接收速率=%.3fMB/s.\n", __FUNCTION__,__LINE__,controller, uiSendNum, fSpeed);
            ullTimeAll = 0;
            uiSendNum = 0;
            return;
        }
    }
}
#if 1

void tRioRecvFun2(u8 controller)
{
	int iError = 0;
    int i;
//    u32 uiCpuID;
    RapidIO_Recv param;
    unsigned int *puiRecvBuf;
    unsigned int *puiRecvBuf0;
    unsigned short uiRioAddrOffset0 = 0;
    unsigned int uiRecvNum = 0;
    size_t stLen = 2;
    unsigned short usRabID;
    char msgname[20] = {0};
 //   unsigned int uiPrintSpace = 4000;

    unsigned long long ullTimeStart = 0;
    unsigned long long ullTimeStop = 0;
    unsigned long long ullTimeAll = 0;
 //   struct timespec tsUs;
    float fSpeed = 0.0;


//    uiCpuID = bslProcGetId();

    if(controller == 0)
    {
        puiRecvBuf = (unsigned int *)dataAddr0.l64;
    }
    else if(controller == 1)
    {
        puiRecvBuf = (unsigned int *)dataAddr1.l64;;
    }
    else
    {
        printf("<ERROR> [%s:_%d_]:: controller不匹配，tRioRecvFun任务退出.\n",__FUNCTION__,__LINE__);
        return;
    }

    sleep(2);
    usRabID = bslRioGetID(controller);

    sprintf(msgname, "%s%d", "MsgRioDB", controller);
    if(MsgRioDB == -1)
        MsgRioDB = mq_create(0, 1024, 4, PTHREAD_WAITQ_FIFO);  //队列缓冲个数为1024，单个消息的最大长度为4Byte
    if(MsgRioDB == -1)
    {
        printf("<ERROR> [%s:_%d_]:: RAB[%d]:消息队列创建失败.\n", __FUNCTION__,__LINE__,controller);
        return;
    }

    while(true)
    {
        //memset(puiRecvBuf, 0, 256 * 1024 * 1024);                      //清零256MB空间大小的内存空间

        sem_wait(&SemStartRx);
        while(SemRioDB == 0)
        {
            usleep(100);
        }
        printf("<INFO> [%s:_%d_]:: RAB[%d]:准备接收门铃信息.\n", __FUNCTION__,__LINE__,controller);

        uiRecvNum = 0;

        while(true)
        {
//        	printf("------------------------2\n");
            bslUsrRioDBRecvQuick(controller, &param);
            if(param.controller == controller)
            {
//            	printf("<**DEBUG**> [%s:_%d_]:: RAB[%d]: dbInfo = 0x%x.\n", __FUNCTION__,__LINE__,controller,param.dbInfo);
                if(param.dbInfo == 0xfff0)          //收到注册回复门铃，发送信号量告诉发送端可以发送数据了
                {
                    sem_post(&SemRioDB);
                }
                else
                {
#if 0
                	puiRecvBuf0 = (unsigned int *)((ulong)puiRecvBuf + (param.dbInfo * K32) % MAX_RIO_ADDRESS);
                    if(puiRecvBuf0[3] == 0)
                    {
                        printf("<ERROR> [%s:_%d_]:: 包长度错误. len=0x%x.\n", __FUNCTION__,__LINE__,puiRecvBuf0[3]);
                    }
                    else if(puiRecvBuf0[0] != 0xaa55aa55 || (puiRecvBuf0[puiRecvBuf0[3] / 4 - 1] != 0x7e7e7e7e))
                    {
                        printf("<ERROR> [%s:_%d_]:: 包头/包尾错误.Head=0x%x, Tail=0x%x.\n", __FUNCTION__,__LINE__,puiRecvBuf0[0], puiRecvBuf0[puiRecvBuf0[3] / 4 - 1]);
                    }
                    else if((puiRecvBuf0[1] != param.srcID) || (puiRecvBuf0[2] != usRabID))
                    {
                        printf("<ERROR> [%s:_%d_]:: 源ID/目的ID填写错误.SrcID=0x%x, DstID=0x%x.\n", __FUNCTION__,__LINE__,puiRecvBuf0[1], puiRecvBuf0[2]);
                    }
                    else if((puiRecvBuf0[4] + 100) != puiRecvBuf0[puiRecvBuf0[3] / 4 - 2])
                    {
                        printf("<ERROR> [%s:_%d_]:: 数据校验错误.Sn=%d, SnCheck=%d.\n", __FUNCTION__,__LINE__,puiRecvBuf0[4], puiRecvBuf0[puiRecvBuf0[3] / 4 - 2]);
                    }
                    else
                    {
                        iError = mq_send(MsgRioDB[controller], (char*)&uiRioAddrOffset0, /*stLen*/sizeof(uiRioAddrOffset0), 1);
                        if (iError != 0)
                        {
                            printf("<ERROR> [%s:_%d_]:: RAB[%d]:发送消息队列失败.\n", __FUNCTION__,__LINE__,controller);
                        }
                        for(i = 5; i < (puiRecvBuf0[3] / 4 - 2); i++)
                        {
                        	if(puiRecvBuf0[i] != (i - 5))
                        		break;
                        }
                        if(i != (puiRecvBuf0[3] / 4 - 2))
                        {
                        	printf("<ERROR> [%s:_%d_]:: 数据校验失败\n",__FUNCTION__,__LINE__);
                        }
                        else
                        {
                        	printf("<INFO> [%s:_%d_]:: [CPU%d] 数据校验通过\n",__FUNCTION__,__LINE__,cpu_id_get());
                        }
                        
                        
                        uiRecvNum++;
//                        if((uiRecvNum%400) == 0)
//                        {
                        	printf("<INFO> [%s:_%d_]:: [CPU%d] 接收包数：%d\n", __FUNCTION__,__LINE__,cpu_id_get(),uiRecvNum);
//                        }
                        if(uiRecvNum >= 40000)
                        {
                            uiRecvNum = 0;
                            printf("RAB[%d]:测试完成.\n", controller);
                            break;
                        }

        //                    uiRecvNum++;
        //
        //                    if(uiRecvNum % uiPrintSpace == 1)
        //                    {
        //                        ullTimeStart = usr_get_time(&tsUs);
        //                    }
        //                    else if(uiRecvNum % uiPrintSpace == 0)
        //                    {
        //                        ullTimeStop = usr_get_time(&tsUs);
        //                        fSpeed = (float)((1.0 * puiRecvBuf0[3] * uiPrintSpace / 1024 / 1024) / ((ullTimeStop - ullTimeStart) / 1000000.0));
        //                        ullTimeAll += (ullTimeStop - ullTimeStart);
        //                        printf("RAB[%d]:接收第%u包,接收速率=%.3fMBps.\n", controller, uiRecvNum, fSpeed);
        //                        //printf("RAB[%d]:发送消息,第%d包.\n", controller, uiRecvNum);
        //                    }
        //
        //                    if(uiRecvNum >= 40000)
        //                    {
        //                        //每包大小1M
        //                        fSpeed = (float)((1.0 * uiRecvNum) / (ullTimeAll / 1000000.0));
        //                        printf("RAB[%d]:总共接收%u包,接收速率=%.3fMBps.\n", controller, uiRecvNum, fSpeed);
        //                        uiRecvNum = 0;
        //                        ullTimeAll = 0;
        //                        //return;
        //                    }
                    }
#else

                    uiRioAddrOffset0 = param.dbInfo;
                    //puiRecvBuf0 = (unsigned int *)((ulong)puiRecvBuf + (param.dbInfo * K32) % MAX_RIO_ADDRESS);
//                    iError = mq_send(MsgRioDB[controller], (char*)&uiRioAddrOffset0, stLen, 1);
//                    if (iError != 0)
//                    {
//                        printf("RAB[%d]:发送消息队列失败.\n", controller);
//                    }
                    uiRecvNum++;
					if((uiRecvNum%4000) == 0)
					{
						printf("<INFO> [%s:_%d_]:: [CPU%d] 接收包数：%d\n", __FUNCTION__,__LINE__,cpu_id_get(),uiRecvNum);
					}
                    if(uiRecvNum >= 40000)
                    {
                        uiRecvNum = 0;
                        printf("RAB[%d]:测试完成.\n", controller);
                        break;
                    }
#endif                  
                }
            }
        }

        //printf("RAB[%d]:测试完成4.\n", controller);
//        sleep(1);
     }
}


void tRioSendFun2(u8 controller)
{
    unsigned long long ullTime_intreval = 0;
    unsigned int uiSendSizeAll = 0;
    unsigned int uiSendNum = 0;
    int ret1 = 0, ret2 = 0;
    int i;
    int iError = 0;
//    size_t stLen = 2;
//    struct timespec tsUs;
    unsigned long long ullTimeStart = 0;
    unsigned long long ullTimeStop = 0;
    unsigned long long ullTimeAll = 0;
    float fSpeed = 0.0;
    unsigned int uiPrintSpace = 4000;/*每隔 uiPrintSpace 个包计算一次速度*/
    unsigned short uiRioAddrOffset = 0;
    unsigned int uiFreeBuff = 0;//100 * 1024 * 1024;    //最后100MB空间不用
//    unsigned int uiSlotNum = sysGetSlotNum();
    unsigned int uiRegTime = 0;//门铃注册时间
    struct NET_CON_RIOPAK Ncrp;
    Ncrp.packetNum = 40000; //包数
    Ncrp.packetLength = 1 * 1024 * 1024; //包长
    RapidIO_Snd SndParam;
    union datacast_64 MemAddr, MemAddrBase;
    unsigned int *puiSendBuf = NULL;

    unsigned int uiRioAddrBase;                     //rapidio总线虚拟地址
    
    while(TRUE)
    {
    	sem_wait(&SemStart);

//         uiSendNum = 0;
         uiRegTime = 0;
         uiRioAddrOffset = 0;
         ullTimeStart = 0;
         ullTimeStop = 0;
         ullTimeAll = 0;

        if(controller == 0)
        {
            uiRioAddrBase = rioAddr0.l32.l;
            MemAddr = memAddr0;
            MemAddrBase = memAddr0;
            puiSendBuf = (unsigned int *)memAddr0.l64;
        }
        else if(controller == 1)
        {
            uiRioAddrBase = rioAddr1.l32.l;
            MemAddr = memAddr1;
            MemAddrBase = memAddr1;
            puiSendBuf = (unsigned int *)memAddr1.l64;
        }
        else
        {
            printf("<ERROR> [%s:_%d_]:: controller不匹配，任务退出.\n",__FUNCTION__,__LINE__);
            return;
        }

        sleep(2);
        //memset(puiSendBuf, 0, 256 * 1024 * 1024);//清零512MB空间大小的内存空间

        printf("<INFO> [%s:_%d_]:: 包数=%d--包长=%dMB\n", __FUNCTION__,__LINE__,Ncrp.packetNum, Ncrp.packetLength/1024/1024);
        printf("<INFO> [%s:_%d_]:: Dest ID:RAB[%d]-0x%x\n\r", __FUNCTION__,__LINE__,((controller + 1) % 2), g_uiRabID2[(controller + 1) % 2]);


        if(SemRioDB == 0)
            sem_init2(&SemRioDB, 0, SEM_BINARY, PTHREAD_WAITQ_PRIO, 0);
        if(SemRioDB == 0)
        {
            printf("<ERROR> [%s:_%d_]:: RAB[%d]:注册门铃信号量创建失败.\n",__FUNCTION__,__LINE__, controller);
        }
        else
            printf("<INFO> [%s:_%d_]:: RAB[%d]:注册门铃信号量创建成功.\n",__FUNCTION__,__LINE__, controller);

        do
        {
            sleep(1);
            //向目标节点发送注册门铃
            if(bslRioSendDB(controller, g_uiRabID2[(controller + 1) % 2], 0xfff0) < 0)
            {
            	printf("<ERROR> [%s:_%d_]:: [RAB%d]发送门铃失败. srcID=0x%x, info=0x%x\n", 
            			__FUNCTION__,__LINE__,controller,g_uiRabID2[controller],0xfff0);
            }
            iError = sem_wait2(&SemRioDB, WAIT, NO_TIMEOUT);
            if (iError != 0 ) //ETIMEDOUT
            {
                uiRegTime ++;
                printf("<Warnning> [%s:_%d_]:: RAB[%d]:获取注册门铃信号量超时%us!\n", __FUNCTION__,__LINE__,controller, uiRegTime);
            }
            else
            {
                printf("<INFO> [%s:_%d_]:: RAB[%d]:获取注册门铃信号量成功!\n", __FUNCTION__,__LINE__,controller);
                sem_destroy(&SemRioDB);
                SemRioDB = 0;
            }
        }while(iError != 0);

        //    while(uiCpuID == 0)
        //    {
        //        sleep(10);
        //    }
        
        //while(uiCpuID == 0)
        while((controller == TxType))
        {
//        	printf("------------------------uiSendNum=%u\n",uiSendNum);
            puiSendBuf = (unsigned long *)MemAddr.l64;
            puiSendBuf[0] = 0xaa55aa55;
            puiSendBuf[1] = g_uiRabID2[controller];//将来填写源ID
            puiSendBuf[2] = g_uiRabID2[(controller + 1) % 2];//将来填写目的ID
            puiSendBuf[3] = Ncrp.packetLength;
            puiSendBuf[4] = uiSendNum;//填写包序号
//            for(i = 5; i < (Ncrp.packetLength / 4 - 2); i++)
//            	puiSendBuf[i] = i - 5;
            puiSendBuf[Ncrp.packetLength / 4 - 2] = uiSendNum + 100;
            puiSendBuf[Ncrp.packetLength / 4 - 1] = 0x7e7e7e7e;
 
            //填充发送参数
            SndParam.rw = 1;
            SndParam.controller = controller;
            SndParam.rioAddr    = uiRioAddrBase;
            SndParam.transferLen = Ncrp.packetLength;
            SndParam.dbInfo_s   = 0;
            SndParam.dbInfo = uiRioAddrOffset;//本次rapidio数据帧头位置
            SndParam.dstID = g_uiRabID2[(controller + 1) % 2];
            SndParam.rioAddr = uiRioAddrBase + (uiRioAddrOffset * K32) % MAX_RIO_ADDRESS;

            //MemAddr.l64 += (uiRioAddrOffset * K32) % MAX_RIO_ADDRESS;
            
            
           ret1 = bslRioDmaWrite(controller, MemAddr, SndParam.rioAddr, SndParam.transferLen, SndParam.dstID);
           if(ret1 != 0)
           {
               printf("<ERROR> [%s:_%d_]:: RAB[%d]:数据发送失败\n", __FUNCTION__,__LINE__,controller);
           }

            ret2 = bslRioSendDB(controller, SndParam.dstID, SndParam.dbInfo);
            if(ret2 != 0)
            {
                printf("<ERROR> [%s:_%d_]:: RAB[%d]:门铃发送失败\n", __FUNCTION__,__LINE__,controller);
            }

            MemAddr.l64 = MemAddrBase.l64 + (uiRioAddrOffset * K32) % MAX_RIO_ADDRESS;

            if((SndParam.transferLen % K32) != 0)
                uiRioAddrOffset = uiRioAddrOffset + SndParam.transferLen / K32 + 1;
            else
                uiRioAddrOffset = uiRioAddrOffset + SndParam.transferLen / K32;

            if(MAX_RIO_ADDRESS - uiRioAddrOffset * K32 <= uiFreeBuff)//剩余空间判断
            {
                uiRioAddrOffset = 0;
            }

            uiSendNum ++;
            if(uiSendNum % uiPrintSpace == 1)
            {
            	ullTimeStart = bslGetTimeUsec();
            }
            else if(uiSendNum % uiPrintSpace == 0)
            {
                ullTimeStop =  bslGetTimeUsec();
                ullTime_intreval = ullTimeStop - ullTimeStart;//us
                uiSendSizeAll = SndParam.transferLen * uiPrintSpace / 1024 / 1024;//MB
//                printf("<DEBUG> [%s:_%d_]:: [%u] ullTime_intreval=%llu us, uiSendSizeAll=%u MB\n", __FUNCTION__,__LINE__,uiSendNum,ullTime_intreval,uiSendSizeAll);
                fSpeed = (float)(1.0 * uiSendSizeAll * 1000000 / ullTime_intreval);//MB/s
                ullTimeAll += ullTime_intreval;
                printf("<INFO> [%s:_%d_]:: [CPU%d] RAB[%d]:第%u包,ullTime=%lluus,fSpeed=%.3fMB/s.\n", 
                		__FUNCTION__,__LINE__,cpu_id_get(),controller, uiSendNum, ullTime_intreval, fSpeed);
            }

            if(uiSendNum >= Ncrp.packetNum)
            {
                //fSpeed = (float)((SndParam.transferLen * uiSendNum  * (u64)1500000000 / ullTimeAll) >> 20);
                fSpeed = (float)((1.0 * SndParam.transferLen * uiSendNum / 1024 / 1024) / (ullTimeAll / 1000000.0));
                printf("<INFO> [%s:_%d_]:: RAB[%d]:总共传输%u包(1MB/包),平均传输速率=%.3fMB/s.\n\n", __FUNCTION__,__LINE__,controller, uiSendNum, fSpeed);
                rab_link_and_mode_status(SRIO0);
                rab_link_and_mode_status(SRIO1);
                break;
            }

//            usleep(500000);
            //if(uiRioAddrOffset == 0)
            //    usleep(500);           //此处时间待测，对端数据消耗能力强，500us以上都可，是否考虑使用门铃通知后续传输？
            
        }
    }
}
#endif

void srioTestFun2(u8 controller)
{
    u32 uiCpuID;
    static u8 initFlag[2] = {0, 0};

    uiCpuID = cpuID;
    
    srioInit2();

    printf("DSP[%d] Enter Srio[%d] Module Test!.\n", uiCpuID, controller);
    printf("--------------------------------------\n");

    TxType = controller;

    if(initFlag[controller] == 0)
    {
        initFlag[controller] = 1;

        /* 创建用于同步的信号量 ---- 信号量初始处于不可用状态，任务按优先级顺序排队等待*/
        if(sem_init2(&SemStart, 0, SEM_BINARY, PTHREAD_WAITQ_PRIO, 0) != 0)
        {
            printf("RAB[%d]:APP Tx SemaphoreB create fail.\n", controller);
        }

        if(sem_init2(&SemStartRx, 0, SEM_BINARY, PTHREAD_WAITQ_PRIO, 0) != 0)
        {
            printf("RAB[%d]:APP Rx SemaphoreB create fail.\n", controller);
        }
        
//        bslTaskCreateCore("tRioRecvInfo",120,0,0x200000,(FUNCPTR) tRioRecvInfo2, 4, controller,1,2,3,4,5,6,7,8,9);

        bslTaskCreateCore("tRioRecvFun",120,0,0x200000,(FUNCPTR) tRioRecvFun2, 5, ((controller + 1) % 2),1,2,3,4,5,6,7,8,9);
        usleep(100000);
        bslTaskCreateCore("tRioSendFun",120,0,0x200000,(FUNCPTR) tRioSendFun2, 4, controller, 1,2,3,4,5,6,7,8,9);
    }

    if((SemStart == 0) || (SemStartRx == 0))
    {
        printf("RAB[%d]:APP SemaphoreB create fail.\n", controller);
        return;
    }

    sem_post(&SemStartRx);
    sem_post(&SemStart);
    
}

