/*!************************************************************
 * @file: hr3_demo_cfg.h
 * @brief: 该文件为hr3模块集成测试例子配置头文件
 * @author: xupeng
 * @date: 15,05,2023
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2023/05/15 |   | 创建
 **************************************************************/

/*4华睿模块*/
//#define __4HR3_VPXOLD__
//#define __4HR3_XYG__
#define __4HR3_GC4HR__

/*DDR*/
#define DDR_SIZE_4G		0x100000000//4G
#define DDR_SIZE_2G 	0x80000000 //2G
#define DDR_SIZE_1G		0x40000000 //1G
#define DDR_SIZE_512MB	0x20000000 //512MB

#define DDR0_VIRTUAL_ADDR		0x90000000//0x1080000000
#define DDR1_VIRTUAL_ADDR		0x1800000000

//#define DDR0_PHY_ADDR0			0x20000000
//#define DDR0_PHY_ADDR1			0x850000000
//#define DDR1_PHY_ADDR0			0x1000000000

// 根据硬件平台规格进行调整
#define DDR0_ENABLED
#ifdef DDR0_ENABLED
#define DDR0_1G_TEST
//#define DDR0_2G_TEST
//#define DDR0_4G_TEST
#endif

//#define DDR1_ENABLED
#ifdef DDR1_ENABLED
#define DDR1_1G_TEST
#define DDR1_2G_TEST
#define DDR1_4G_TEST
#endif

/*RIO*/
#define RIO_TEST_ADDR_10000000	0x10000000
#define RIO_TEST_ADDR_20000000	0x10000000
#define RIO_TEST_ADDR_30000000  0x30000000
#define RIO_TEST_ADDR_40000000  0x40000000

#define RIO_BAR_ADDR_10000000   0x10000000
#define RIO_BAR_ADDR_20000000   0x20000000
#define RIO_BAR_ADDR_30000000	0x30000000
#define RIO_BAR_ADDR_40000000   0x40000000

#define RIO_TARGET_ID	0x10 


/*NOR FLASH*/
#define SPI_TEST_SIZE		(2*1024*1024)
#define SPI_TEST_START		(0x600000)//起始地址2MB以后

/*QSPI FLASH*/
#define QSPI_TEST_SIZE		(2*1024*1024)
#define QSPI_TEST_START		(0x600000)//起始地址2MB以后


/*根据枚举方式来选择*/
#define ENUMERATE_DYNAMIC

/*测试板卡的槽位号，注意大机箱和小机箱的-3问题*/
#define SLOT_NUM	4



