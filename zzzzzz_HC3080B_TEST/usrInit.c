/*******************************************************************************
 *
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：用户应用入口程序
 * 修改：
 *
 */

#define CONFIGURE_INIT
#include "system.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cpuset.h>
#include <pthread.h>

#include "mylog/mylog.h"

//#define _TEST_PCIEX16_WX1820_
//#define _TEST_SDIO_
//#define _TEST_WDOG_
//#define _TEST_RTC_
//#define _TEST_TIMER_
//#define _TEST_LPC_
//#define _TEST_I2C_
//#define _TEST_QSPI_
//#define _TEST_SPINOR_   
//#define _TEST_HRFS_
//#define _TEST_GPIO_
//#define _TEST_NAND_
//#define _TEST_CAN_
//#define _TEST_RIO_
//#define _TEST_PCIE_
//#define _TEST_GDMA_
//#define _TEST_CDMA_
//#define _TEST_STRCMP_
//#define _TEST_BSPINFO_
//#define _TEST_1848_
//#define _TEST_GETTIME_
//#define _TEST_UNALIGNED_EXC_


static void DumpUint8(const unsigned char *pAddr, unsigned int length);
static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length);
static void delay_cnt(unsigned int count);


/*******************************************************************************
 *
 * 用户应用程序入口函数
 *
 * 		本程序用于初始化用户应用。
 *
 * 输入：
 * 		无
 * 输出：
 * 		无 
 * 返回：
 * 		无
 */
void UserInit(void)
{
//	printf("sizeof(ptrdiff_t) = %d\n",sizeof(ptrdiff_t));//8
	
	/* 添加用户代码 */
//	sleep(3);
	extern unsigned int sysCpuGetID();
//	printk("<**DEBUG**> [%s():_%d_]:: UserInit Start ......\n", __func__, __LINE__);
	MYLOG(MYLOG_USER, MYLOG_LEVEL_INFO, "UserInit Start ......");/*ldf 20230528 add:: log*/
	extern void mylog(int mod, int level, const char *func, int line, const char *fmt, ...);/*ldf 20230528 add:: log*/
//	mylog(MYLOG_USER, MYLOG_LEVEL_INFO, __func__, __LINE__, "[DSP%d] UserInit Start ......",sysCpuGetID());/*ldf 20230528 add:: log*/
	
//	extern unsigned long long ldf_test_3080b;/*ldf 20230814 add:: 临时添加的变量, 和3080b对测*/
//	extern int shellPlus_parseline(char *cmdline);
//	if(ldf_test_3080b)
//	{
//		shellPlus_parseline("ifconfig gem0 192.168.100.13");
//		shellPlus_parseline("ifconfig gem0 lladdr 00:d8:61:1c:67:03");
////		shellPlus_parseline("ping 192.168.100.12");
//	}
	
#if 0
	int count = 0;
	int level = 0;
	level = int_cpu_lock();
	printk("     <**DEBUG**> [%s():_%d_]:: int cpu locked!! \n", __FUNCTION__, __LINE__);
	while(1)
	{
		count++;
		printk("     <**DEBUG**> [%s():_%d_]:: count = %d \n", __FUNCTION__, __LINE__,count);
		if(count >= 500)
		{
			int_cpu_unlock(level);
			printk("     <**DEBUG**> [%s():_%d_]:: int cpu unlocked!! \n", __FUNCTION__, __LINE__);
			break;
		}
	}
#endif

//	ifAddrAdd("txgbe0", "192.168.0.100", "192.168.0.255", ntohl(inet_addr("255.255.255.0")));
//	ifAddrAdd("txgbe0", "192.168.1.100", "192.168.1.255", ntohl(inet_addr("255.255.255.0")));
//	ifAddrAdd("txgbe1", "192.168.0.101", "192.168.0.255", ntohl(inet_addr("255.255.255.0")));
//	ifAddrAdd("txgbe1", "192.168.1.101", "192.168.1.255", ntohl(inet_addr("255.255.255.0")));
//	ifAddrAdd("gem0", "192.168.4.12", "192.168.4.255", ntohl(inet_addr("255.255.255.0")));
//	ifAddrAdd("gem0", "192.168.1.111", "192.168.1.255", ntohl(inet_addr("255.255.255.0")));
	
//	extern void DumpErrorLoggerReg(void);
//	DumpErrorLoggerReg();/*ldf 20230719 add:: 打印DDR/GMAC ErrorLogger寄存器值*/
	
//	extern void zhyf_open_log_mem();
//	zhyf_open_log_mem();/*ldf 20230620 add:: debug*/
	
//	asm volatile(
//		".set noreorder\n"
//		"li      $3, 'Q'\n"
//		"dli	 $2, 0x900000001f080030\n"
//		"sb      $3, 0($2)\n"
//		"nop"
//	);

	
#ifdef _TEST_PCIEX16_WX1820_
//	txgbe_net_drv_init2(TXGBE_NUMBER0);
//	ipAttach(TXGBE_NUMBER0,"txgbe");
//	ifAddrSet( TXGBE_HOST_NAME0, TXGBE_IP_ADDRESS0);
//	if_addr6_add( TXGBE_HOST_NAME0, TXGBE_IPv6_ADDRESS0, TXGBE_IPv6_PRELEN0, TXGBE_IPv6_FLAGS0);
//	ifMaskSet(TXGBE_HOST_NAME0, htonl(inet_addr(TXGBE_SUBNETMASK0)));
//	ifFlagChange( TXGBE_HOST_NAME0, 1, TRUE);
#endif
	
#ifdef _TEST_SDIO_
//	fdisk("/dev/emmc0",100,0,0,0);
//	format("dosfs","/dev/emmc0p1");
//	mount("dosfs","/dev/emmc0p1","/emmc0");
	
//	fdisk("/dev/emmc1",100,0,0,0);
//	format("dosfs","/dev/emmc1p1");
//	mount("dosfs","/dev/emmc1p1","/emmc1");
#endif
	
#ifdef _TEST_WDOG_
	extern void hr3_wdog_module_init(void);
	hr3_wdog_module_init();
//	extern void rst(void);
//	rst();
	extern void test_wdog(void);
	test_wdog();
#endif

#ifdef _TEST_RTC_
	extern int rtc_module_init(void);
	rtc_module_init();
#endif
	
#ifdef _TEST_BSPINFO_	/*打印BSP版本信息*/
	extern void bslBspVersionRead(void);
	bslBspVersionRead();
#endif

#ifdef _TEST_UNALIGNED_EXC_
	extern void unaligned_exc_print_switch(int open_or_close);
	typedef  struct heiha{
	   char aa; 
	   int bb;  
	   char cc; 
	   int dd;
	  }s_interface9,*p_interface9;
	  
	  unaligned_exc_print_switch(1);
	  
	  s_interface9 nana;
	   nana.aa = 1;
	   nana.bb = 2;
	   nana.cc = 3;
	   nana.dd = 4;
	   unsigned int hhh = (unsigned int)(&nana);
	   s_interface9 *xiaxia = (s_interface9 *)(hhh+1);
	   printk("111 xiaxia:0x%x\n",xiaxia);
	  // xiaxia->bb = 10; //触发非对齐异常
	   printk("222\n");
#endif

#ifdef _TEST_GETTIME_
	#include <time.h>
	#include <clock.h>
	#include <udelay.h>
	printf("<DEBUG> [%s:__%d__]:: _TEST_GETTIME_ ......\n",__FUNCTION__,__LINE__);
	extern unsigned long long usr_get_time(struct timespec *ts);
	extern unsigned long long bslGetTimeUsec(void);
	unsigned long long t0,t1, t2;
	struct timespec test_time, nanosleep_time;
	nanosleep_time.tv_sec = 0;
	nanosleep_time.tv_nsec = 3 * 1000;
	
	/*先执行一遍，将代码和参数装入cache*/
	bslGetTimeUsec();//usr_get_time(&test_time);//
	usleep(3);//udelay(3);//nanosleep(&nanosleep_time, NULL);//sleep(1);//delay_cnt(100);//约1us
	
	t0 = bslGetTimeUsec();//usr_get_time(&test_time);//
	t1 = bslGetTimeUsec();//usr_get_time(&test_time);//
	usleep(3);//udelay(3);//nanosleep(&nanosleep_time, NULL);//sleep(1);//delay_cnt(100);//约1us
	t2 = bslGetTimeUsec();//usr_get_time(&test_time);//
	printf("<DEBUG> [%s:__%d__]:: t0 = %llu us, t1 = %llu us, t2 = %llu us, time = %llu us\n",
			__FUNCTION__,__LINE__,t0,t1,t2,t2-t1-(t1-t0));
	printf("<DEBUG> [%s:__%d__]:: _TEST_GETTIME_ OVER\n",__FUNCTION__,__LINE__);
#endif
	
#ifdef _TEST_1848_ /*1848*/
	extern int bslUsrRioInit(void);
	bslUsrRioInit();
//	unsigned int get_1848_reg(unsigned int offset);
//	get_1848_reg(0);
#endif
      
#ifdef _TEST_STRCMP_	/*strcmp*/
#define TEST(flag) ((flag) ? ".MIPS.options" : ".options")
	int ret = 2;
	printk("[%s][%d]\n", __FUNCTION__, __LINE__);
	ret = strcmp(".MIPS.options",".MIPS.options");
	printk("[%s][%d] ret = %d\n", __FUNCTION__, __LINE__, ret);
	const char * name;
	name = ".MIPS.options";
	boolean flag = 0;
	printk("name = %s addr = 0x%lx\n", name, name);
	printk("TEST(flag) = %s addr = 0x%lx\n", TEST(flag), TEST(flag));
	ret = strcmp(name,TEST(flag));
	printk("[%s][%d] ret = %d\n", __FUNCTION__, __LINE__, ret);
#endif
      
#ifdef _TEST_CDMA_	/*CDMA*/
	extern void  hrCdmaInstInit (void);
	hrCdmaInstInit();
	int block = 0x100000;
	int count = 1;
	int totalTransSize = 0x10000000;
	unsigned char *src0 = (unsigned char *)malloc(totalTransSize);
	unsigned char *dst0 = (unsigned char *)malloc(totalTransSize);
	unsigned char *src1 = (unsigned char *)malloc(totalTransSize);
	unsigned char *dst1 = (unsigned char *)malloc(totalTransSize);
	printf("<DEBUG> [%s:__%d__]:: src0 = %p, dst0 = %p\n",__FUNCTION__,__LINE__,src0,dst0);
	printf("<DEBUG> [%s:__%d__]:: src1 = %p, dst1 = %p\n",__FUNCTION__,__LINE__,src1,dst1);
	memset(src0, 0, totalTransSize);
	memset(dst0, 0, totalTransSize);
	memset(src1, 0, totalTransSize);
	memset(dst1, 0, totalTransSize);
	int i = 0;
	for(i=0;i<totalTransSize;i++)
	{
		*(src0 + i) = 0x12 + i;
		*(src1 + i) = 0x34 + i;
	}
	extern void tstHR2CDMATest_hr2(void *src0,void *dst0,void *src1,void *dst1,int block,int count);
//	tstHR2CDMATest_hr2(src0, dst0, src1, dst1, block, count);
	extern void tstHR2CDMAPerf_hr2(void *src0,void *dst0,void *src1,void *dst1,int count);
	tstHR2CDMAPerf_hr2(src0, dst0, src1, dst1, 2000);
#endif

#ifdef _TEST_GDMA_	/*GDMA*/
	extern void  hrGdmaInstInit (void);
	hrGdmaInstInit();
	int block = 0x100000;
	int count = 1;
	int totalTransSize = 0x10000000;
	unsigned char *src0 = (unsigned char *)malloc(totalTransSize);
	unsigned char *dst0 = (unsigned char *)malloc(totalTransSize);
	unsigned char *src1 = (unsigned char *)malloc(totalTransSize);
	unsigned char *dst1 = (unsigned char *)malloc(totalTransSize);
	/*src0 = 0x1164bf9220, dst0 = 0x1154bf9200*/
	/*src1 = 0x1144bf91e0, dst1 = 0x1134bf91c0*/
	printf("<DEBUG> [%s:__%d__]:: src0 = %p, dst0 = %p\n",__FUNCTION__,__LINE__,src0,dst0);
	printf("<DEBUG> [%s:__%d__]:: src1 = %p, dst1 = %p\n",__FUNCTION__,__LINE__,src1,dst1);
	memset(src0, 0, totalTransSize);
	memset(dst0, 0, totalTransSize);
	memset(src1, 0, totalTransSize);
	memset(dst1, 0, totalTransSize);
	int i = 0;
	for(i=0;i<totalTransSize;i++)
	{
		*(src0 + i) = 0x12 + i;
		*(src1 + i) = 0x34 + i;
	}
	extern void tstHR2GDMATest_hr2(void *src0,void *dst0,void *src1,void *dst1,int block,int count);
	tstHR2GDMATest_hr2(src0, dst0, src1, dst1, block, count);
	extern void tstHR2GDMAPerf_hr2(void *src0,void *dst0,void *src1,void *dst1,int count);
	tstHR2GDMAPerf_hr2(src0, dst0, src1, dst1, 2000);
#endif

#ifdef _TEST_PCIE_	/*PCIe*/
	extern int pci_show_module_init(void);
	extern int pci_module_init(void);
	extern int  hr3PciDrv (void);
	pci_module_init();
	pci_show_module_init();
	hr3PciDrv();
#endif

#ifdef _TEST_RIO_	/*SRIO*/
//	extern void test_srio(void);
#endif

#ifdef _TEST_CAN_	/*CAN*/
//	extern int can_module_init(u8 controller);
//	can_module_init(0);
//	can_module_init(1);
	extern void can_test_init(void);
	extern int case_bsp_tests_can_1(void);
	can_test_init();
	case_bsp_tests_can_1();
#endif

#ifdef _TEST_NAND_	/*NAND Flash*/
//	extern int flash_pageio_init(void);
//	flash_pageio_init();
//	extern void erasenandall(void);
//	erasenandall();

//	#define FFX_FS_TYPE		2
//	#define FFX_DISK_MNT_NAME		"/ffx0"
//	#define FFX_DISK_NUM		0
//	#define FFX_DEV_NUM		0
//	#define FFX_DEV_OFF		0
//	#define FFX_DISK_LEN		0xFFFFFFFF
//	#define FFX_DISK_CACH		128
//	#define FFX_FMT_STATE		2
//	#define FFX_FS_FORMAT		1
//	extern void nandflash_config_init(void);
//	extern void usrFlashFXComponentInit (void);
//	extern STATUS   usrFlashFXDeviceInit(char *pszDevName, unsigned nDiskNum, unsigned nDeviceNum, unsigned nOffsetKB,\
//		unsigned nLengthKB, unsigned nFormatState, unsigned nFileSystemID, unsigned fsFormat, unsigned nCacheSize);
//	nandflash_config_init();
//	usrFlashFXComponentInit();
//	usrFlashFXDeviceInit(FFX_DISK_MNT_NAME, FFX_DISK_NUM, FFX_DEV_NUM, FFX_DEV_OFF,\
//			FFX_DISK_LEN, FFX_FMT_STATE, FFX_FS_TYPE, FFX_FS_FORMAT, FFX_DISK_CACH);
	
	extern void erasenandall(void);
	erasenandall();
	extern BOOL readNandId(void);
	readNandId();
#endif

#ifdef _TEST_GPIO_	/*gpio*/
	extern void gpio_test_init(void);
	extern void  hrGpioInstInit (void);
	extern void gpio_test(unsigned int pin_num1, unsigned int pin_num2);
//	hrGpioInstInit();
	gpio_test_init();
//	gpio_test(13,14);
//	gpio_output_test(0, 1);
//	gpio_output_test(1, 1);
//	gpio_output_test(2, 1);
//	gpio_output_test(3, 1);
//	gpio_output_test(16, 1);
//	gpio_output_test(17, 1);
//	gpio_output_test(18, 1);
//	gpio_output_test(19, 1);
#endif

#ifdef _TEST_HRFS_	/*hrfs*/
	ramdev_create("/dev/ram0",200);
	format("hrfs", "/dev/ram0");
	mount("hrfs", "/dev/ram0p1", "/c");
#endif

#ifdef _TEST_SPINOR_	/*spi nor*/
	extern void spi_module_init(u32 controller);
	extern unsigned int NorFlashReadid(int controller);
	extern void test_norflash_rw(int controller, unsigned int addr, u32 len, int is_DumpData);
	spi_module_init(0);
	spi_module_init(1);
//	NorFlashReadid(0);
	test_norflash_rw(0,0x300000,256, 1);
	
//	#define NOR_FFX_FS_TYPE		2
//	#define NOR_FFX_DISK_MNT_NAME		"/ffx1"
//	#define NOR_FFX_DISK_NUM		0
//	#define NOR_FFX_DEV_NUM		0
//	#define NOR_FFX_DEV_OFF		0
//	#define NOR_FFX_DISK_LEN		0xFFFFFFFF
//	#define NOR_FFX_DISK_CACH		128
//	#define NOR_FFX_FMT_STATE		2
//	#define NOR_FFX_FS_FORMAT		1
//	extern void usrFlashFXComponentInit (void);
//	extern STATUS   usrFlashFXDeviceInit(char *pszDevName, unsigned nDiskNum, unsigned nDeviceNum, unsigned nOffsetKB,\
//		unsigned nLengthKB, unsigned nFormatState, unsigned nFileSystemID, unsigned fsFormat, unsigned nCacheSize);
//	usrFlashFXComponentInit();
//	usrFlashFXDeviceInit(NOR_FFX_DISK_MNT_NAME, NOR_FFX_DISK_NUM, NOR_FFX_DEV_NUM, NOR_FFX_DEV_OFF,\
//			NOR_FFX_DISK_LEN, NOR_FFX_FMT_STATE, NOR_FFX_FS_TYPE, NOR_FFX_FS_FORMAT, NOR_FFX_DISK_CACH);
#endif

#ifdef _TEST_QSPI_	/*qspi, TODO:存放的uboot镜像,请小心测试*/
//	extern void qspiflashGetId(void);
//	extern void test_qspiflash_read_new(void);
//	extern void test_qspiflash_new();
//	qspiflashGetId();
//	test_qspiflash_read_new();//read test
//	test_qspiflash_new();//write test
	
	extern void lib_qspi_module_init(void);
	lib_qspi_module_init();
#endif

#ifdef _TEST_I2C_	/*i2c*/
//	extern int  cdnsI2cInit(u32  uiChannel);
//	cdnsI2cInit(0);
//	cdnsI2cInit(1);
//	extern void i2ctest(int i);
//	i2ctest(0x50);

//	extern void i2c_module_init(int channel);
//	i2c_module_init(0);
//	i2c_module_init(1);
	
//	extern int task0_init(struct s_data *data);
//	extern int task0_start(struct s_data *data);
//	extern int task0_check(struct s_data *data);
//	task0_init(NULL);
//	task0_start(NULL);
//	task0_check(NULL);
	
//	extern int task5_init(struct s_data *data);
//	extern int task5_start(struct s_data *data);
//	extern int task5_check(struct s_data *data);
//	task5_init(NULL);
//	task5_start(NULL);
//	task5_check(NULL); 
	
	extern int bslI2CInit(unsigned int channel,unsigned int mode,unsigned int slaveId);
	extern int bslI2CMTransData(unsigned int devIndex,unsigned int transType,unsigned char* ddrAddr,unsigned int length,unsigned int slaveId,unsigned int memAddr);
	
//	int i=0;
	u8 rbuf[512];
//	u8 wbuf[512];
//	for (i=0; i<256; i++)
//		wbuf[i] = i;
//	printk("<DEBUG> [%s:__%d__]:: ddrAddr = 0x%16x\n",__FUNCTION__,__LINE__,ddrAddr);
	bslI2CInit(0,0,0);
//	bslI2CMTransData(0, 1, wbuf, 256, 0x50, 0x00);
//	DumpUint8(wbuf, 256);
//	printk("\n\nwbuf: ");
//	for (i=0; i<256; i++)
//		printk("0x%x ", wbuf[i]);
//	printk("\r\n");
	
//	bslI2CMTransData(0, 0, rbuf, 256, 0x50, 0x00);
//	DumpUint8(rbuf, 256);
	bslI2CMTransData(0, 0, rbuf, 4, 0x3, 0x00);
	DumpUint8(rbuf, 4); 
//	printk("\n\nrbuf: ");
//	for (i=0; i<256; i++)
//		printk("0x%x ", rbuf[i]);
//	printk("\r\n");
#endif

#ifdef _TEST_LPC_	/*lpc*/
	extern int test_lpc_rw(void);
//	test_lpc_rw();
	
	extern void test_lpcflash_rw(unsigned int testAddr, unsigned int test_len, unsigned char first_data, int is_DumpData);
	test_lpcflash_rw(0xffe52000, 64, 0x12, 0);
#endif	

#ifdef _TEST_TIMER_	/*timer*/
//	extern void  hrTimerModuleInit (u32 index);
//	hrTimerModuleInit(0);
//	hrTimerModuleInit(1);
	extern void test_timer_init();
	extern void test_timer_reload(int n);
	test_timer_init();
	
	typedef int 		(*FUNCPTR) ();
	#define TIMER_MODE_ONCE		1
	#define TIMER_MODE_CYCLE	0
	extern int test_timer_callback(u64 param);
	extern int bslTimerInit(unsigned int timerIndex, unsigned int mode,unsigned long long value,FUNCPTR callback,int param);
	extern int bslTimerSetState(unsigned int timerIndex,unsigned int state);
	bslTimerInit(1, TIMER_MODE_CYCLE, 0x12345678, test_timer_callback, 1);
	bslTimerSetState(0, 1);
#endif 
	
	MYLOG(MYLOG_USER, MYLOG_LEVEL_INFO, "UserInit End ......");/*ldf 20230528 add:: log*/
//	mylog(MYLOG_USER, MYLOG_LEVEL_INFO, __FUNCTION__, __LINE__, "UserInit End ......");/*ldf 20230528 add:: log*/

	return;
}

static void delay_cnt(unsigned int count)
{
	volatile unsigned int i = 0;
	for(i=0;i<count;i++)
	{
		;
	}
}

static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
					index,valDst,valSrc);
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}


/********************************** TEST ***********************************************************/
#ifdef _TEST_RIO_
void test_srio(void)
{
	/*门铃测试 : PASS*/
	extern void doorbellCmdMain(void); 
	doorbellCmdMain(); 

	/*DMA读写正确性测试 : PASS*/
	extern void bslDmaCmdMain(void);
	bslDmaCmdMain();
    
	/*DMA读写速度测试 : PASS, 测得不准确*/
	extern void bslDmaSpeedCmdMain(void);
	bslDmaSpeedCmdMain();
	
	/*datasream测试  : PASS*/
	extern void datasream_main(void);
	datasream_main();
	
	/*DME测试  : PASS*/
	extern void dme_main(void);
	dme_main();
	
	/*PIO测试  : PASS*/
	extern void pio_main(void);
	pio_main();
	
	/*PIO maint测试  : PASS*/
	extern void pio_maint_main(void);
	pio_maint_main();
	
	/*P2P测试 : 应该需要两块板卡，分别进行收发
	 * TODO:待整理框架,API保持和yihui一致*/
	extern void p2p_main(void);
	p2p_main();   
	
	/*bootsrio测试 : 测试方法不太清楚
	 * TODO:待整理框架,API保持和yihui一致*/
	extern void bootsrio_main(void);
	bootsrio_main(); 
}
#endif       


#define TEST_CPUS	8
typedef struct test_unaligned
{
	int		num1;	
	char	ch1;	
	short	num2;	
	void*	addr;	
	char	ch2;	
} __attribute__((packed)) TEST_UNALIGNED;

TEST_UNALIGNED	*test_unaligned_struct = {NULL};
unsigned long long test_addr = 0xC627AEE7;//0xffffffe0;
unsigned long long test_quit_flag = 0;
unsigned long long test_count = 0;
static void T_test_data_bus_err()
{
	unsigned long long tmp = 0;
	
	while(!test_quit_flag)
	{
		test_count++;
		*(unsigned long long*)&(test_unaligned_struct->addr) = test_addr;
		tmp = (unsigned long long)test_unaligned_struct->addr;
	}
	
	return;
}

void test_data_bus_err()
{
	int i = 0;
	pthread_t t[TEST_CPUS];

	test_unaligned_struct = (TEST_UNALIGNED*)malloc(sizeof(TEST_UNALIGNED));
	printf("test_unaligned_struct=0x%lx, &test_unaligned_struct->addr=0x%lx\n",\
			test_unaligned_struct,&test_unaligned_struct->addr);
	
	for(i = 0; i < TEST_CPUS; i++)
	{
		pthread_create2(&t[i], "test_data_bus_err", 120, 0, 0x100000, T_test_data_bus_err, NULL);
		pthread_detach(t[i]);
	}
	
	return;
}


extern u32 sys_clk_rate_get(void);
static unsigned int test_cpu_is_alive_count[8] = {0};
static void do_test_cpu_is_alive(void)
{
	int cpuid = cpu_id_get();
	
	while(1)
	{
		printf("----------- CPU%d  ##%d ------------\n",cpuid, test_cpu_is_alive_count[cpuid]);
		test_cpu_is_alive_count[cpuid]++;
		pthread_delay(sys_clk_rate_get()*10);
	}
}
void test_cpu_is_alive(void)
{
	int i = 0;
	pthread_t t[8];
	
	for(i = 0; i < 8; i++)
	{
		pthread_create3(&t[i], "test_task_run", 170, 0, 0x10000, 1<<i, do_test_cpu_is_alive, NULL);
		pthread_detach(t[i]);
	}
}


unsigned long long ldf_udelay = 0;//200;
unsigned long long ldf_size = 1024;//1024;//8192;//32768;//2048;//16384;
unsigned long long ldf_blen = 131072;//65536;//131072;//32768;//16384;
unsigned long long ldf_is_bind_nic = 0;
char* ldf_send_nic_name = "txgbe1";
char* ldf_send_ip = "192.168.0.101";
char* ldf_recv_nic_name = "txgbe0";
char* ldf_recv_ip = "192.168.0.100";
int ldf_port = 5000;
char* ldf_multicast_ip = "224.100.200.133";
static void do_net_test_run(int type)
{
	switch(type)
	{
	case 0:
		/*Multicast Recv
		 *
		 * */
		Multicast_Recv();
		break;
	case 1:
		/*blaster: tcp recv
		 *
		 * */
		blasteeTCP(ldf_port, ldf_size, ldf_blen, 0);
		break;
	case 2:
		/*blaster: tcp send
		 *
		 * */
		blasterTCP(ldf_recv_ip, ldf_port, ldf_size, ldf_blen, 0);
		break;
	case 3:
		/*blaster: udp recv
		 *
		 * */
		blasteeUDP(ldf_port, ldf_size, ldf_blen, 0);
		break;
	case 4:
		/*blaster: udp send
		 *
		 * */
		blasterUDP(ldf_recv_ip, ldf_port, ldf_size, ldf_blen, 0);
		break;
	case 5://iperf: tcp send
		iperf3("iperf3 -c 192.168.100.105 -l 64K -i 1 -t 2000 -p 5201");
		break;
	case 6://iperf: udp send
		iperf3("iperf3 -c 192.168.100.105 -u -b 5G -i 1 -t 2000 -p 5201");
		break;
	default://iperf: tcp-recv or udp-recv
		iperf3("iperf3 -s -i 1 -p 5201");
		break;
	}
}

/*
 * blaster: 1-tcp_recv, 2-tcp_send, 3-udp_recv, 4-udp_send
 * iperf: 5-tcp send, 6-udp send, 7-tcp recv or udp recv
 * */
void net_test_run(int type)
{
	pthread_t t;
	cpuset_t cpus;
	
//	pthread_create3(&t, "do_net_test_run", 120, 0, 0x100000, 1<<2, do_net_test_run, type);
	pthread_create2(&t, "do_net_test_run", 120, 0, 0x100000, do_net_test_run, type);
	CPUSET_ZERO(cpus);
	CPUSET_SET(cpus, 2);
	pthread_affinity_set(t, cpus);
	pthread_detach(t);
}

void test_ifconfig(int is_3080b)
{
	extern int shellPlus_parseline(char *cmdline);
	if(is_3080b)
	{
		shellPlus_parseline("ifconfig gem0 192.168.100.13");
		shellPlus_parseline("ifconfig gem0 lladdr 00:d8:61:1c:67:10");
//		shellPlus_parseline("ping 192.168.100.12");

//		shellPlus_parseline("ifconfig gem1 192.168.101.13");
//		shellPlus_parseline("ifconfig gem1 lladdr 00:d8:61:1c:67:11");
//		
//		shellPlus_parseline("ifconfig gem2 192.168.102.13");
//		shellPlus_parseline("ifconfig gem2 lladdr 00:d8:61:1c:67:12");
//		
//		shellPlus_parseline("ifconfig gem3 192.168.103.13");
//		shellPlus_parseline("ifconfig gem3 lladdr 00:d8:61:1c:67:13");
	}
}


void test_net_loop(int type) /*0-tcp, 1-udp*/
{
	pthread_t mcSendThd, mcRecvThd;
	int ret;
	
	if(type == 0){ //tcp
		/*recv*/
		printk("\ncreate and run tcp recv task.\r\n");
		ret = pthread_create3(&mcRecvThd, "tcpRecvThd", 175, 0, 1*1024*1024, 1<<1, do_net_test_run, 1);
		if(ret != 0)
		{
			printk("bcRecv task create failed.\r\n");
		}
		pthread_detach(mcRecvThd);

		sleep(3);

		/*send*/
		printk("\ncreate and run tcp send task.\r\n");
		ret = pthread_create3(&mcSendThd, "tcpSendThd", 175, 0, 1*1024*1024, 1<<2, do_net_test_run, 2);
		if(ret != 0)
		{
			printk("bcSend task create failed.\r\n");
		}
		pthread_detach(mcSendThd);
	}
	else{ //udp
		/*recv*/
		printk("\ncreate and run udp recv task.\r\n");
		ret = pthread_create3(&mcRecvThd, "udpRecvThd", 175, 0, 1*1024*1024, 1<<1, do_net_test_run, 3);
		if(ret != 0)
		{
			printk("bcRecv task create failed.\r\n");
		}
		pthread_detach(mcRecvThd);

		sleep(3);

		/*send*/
		printk("\ncreate and run udp send task.\r\n");
		ret = pthread_create3(&mcSendThd, "udpSendThd", 175, 0, 1*1024*1024, 1<<2, do_net_test_run, 4);
		if(ret != 0)
		{
			printk("bcSend task create failed.\r\n");
		}
		pthread_detach(mcSendThd);
	}
}






