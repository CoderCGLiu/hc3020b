/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件定义了运行ReWorks操作系统所需头文件和宏定义，该文件被usrInit.c使用
 * 
 * 自动生成的文件。 不要进行编辑
 * 
 */

#define EMMC0_FMT_STATE		1

#define INCLUDE_MQUEUE
#define REWORKS_MAX_MSGQS		1024

#define CLB_AGENT
#define CLB_AGENT_PRIORITY		201
#define CLB_AGENT_STACKSIZE		32768
#define CLB_NETWORK_SERV_PORT		1534
#define CLB_DEV_NAME		"Serial:/dev/serial0"
#define CLB_NETWORK_PROTOCOL		1

#define INCLUDE_PTHREAD_COND
#define REWORKS_MAX_CONDS		1024

#define INCLUDE_PCI_CONFIG
#define SYS_TICKS_PER_TIMESLICE		5
#define SYS_TICKS_PER_SEC		100

#define DRV_TIMER1

#define INCLUDE_HR3

#define DRV_NAND

#define INCLUDE_CONSOLE
#define CONSOLE_INPUT_DEVICE_NAME		"/dev/serial0"
#define CONSOLE_OUTPUT_DEVICE_NAME		"/dev/serial0"

#define INCLUDE_USER_HEAP
#define REWORKS_HEAP_ALGORITHM		1

#define INCLUDE_OBJECT

#define INCLUDE_EXC_CSP

#define INCLUDE_SHELL
#define SHELL_PRIO		170
#define SHELL_USERNAME		""
#define SHELL_PASSWORD		""
#define FFX_DEV_NUM		0
#define FFX_DEV_OFF		0
#define FFX_DISK_CACH		128
#define FFX_DISK_LEN		0xFFFFFFFF
#define FFX_DISK_MNT_NAME		"/ffx0"
#define FFX_DISK_NUM		0
#define FFX_FMT_STATE		1
#define FFX_FS_FORMAT		0
#define FFX_FS_TYPE		2

#define INCLUDE_SIGNAL

#define DRV_LPC

#define HGMAC_DEV3
#define GEM3_SUBNET_MASK		"255.255.255.0"
#define GEM3_HOST_NAME		"gem3"
#define GEM3_IP_ADDRESS		"192.168.3.%d"
#define GEM3_IPv6_ADDRESS		"fe80::300:56cf:%d"
#define GEM3_IPv6_PRELEN		64
#define GEM3_IPv6_FLAGS		0x04

#define INCLUDE_TTY_MODULE

#define DRV_RIOC0

#define INCLUDE_STD_IO_DEV
#define STD_INPUT_DEVICE_NAME		"/dev/serial0"
#define STD_OUTPUT_DEVICE_NAME		"/dev/serial0"

#define INCLUDE_VXWORKS_SEM
#define KERNEL_MM_METHOD		0
#define USE_TIME_BASE_REG		1

#define DRV_I2C

#define HGMAC_DEV1
#define GEM1_SUBNET_MASK		"255.255.255.0"
#define GEM1_HOST_NAME		"gem1"
#define GEM1_IP_ADDRESS		"192.168.1.%d"
#define GEM1_IPv6_ADDRESS		"fe80::300:56ef:%d"
#define GEM1_IPv6_PRELEN		64
#define GEM1_IPv6_FLAGS		0x04

#define HGMAC_DEV0
#define GEM0_SUBNET_MASK		"255.255.255.0"
#define GEM0_HOST_NAME		"gem0"
#define GEM0_IP_ADDRESS		"192.168.0.%d"
#define GEM0_IPv6_ADDRESS		"fe80::300:56ff:%d"
#define GEM0_IPv6_PRELEN		64
#define GEM0_IPv6_FLAGS		0x04

#define INCLUDE_DYNAMIC_MODULE
#define DYNAMIC_LOAD_RAM_SIZE		16

#define DRV_PCIE
#define TXGBE_HOST_NAME0		"txgbe0"
#define TXGBE_SUBNETMASK0		"255.255.255.0"
#define TXGBE_IP_ADDRESS0		"192.168.4.100"
#define TXGBE_NUMBER0		0
#define TXGBE_IPv6_ADDRESS0		"fe80::300:52ff:1000"
#define TXGBE_IPv6_PRELEN0		64
#define TXGBE_IPv6_FLAGS0		0x04

#define INCLUDE_USERMEM

#define DRV_GPIO1

#define ABI_N64
#define CPU_NUMBER		8
#define CPUSET		0xFFFFFFFF

#define INCLUDE_VX

#define INCLUDE_PTHREAD_HOOK

#define INCLUDE_PTHREAD_MUTEX
#define REWORKS_MAX_MUTEXES		1024

#define INCLUDE_VXBUS_MODULE
#define TXGBE_HOST_NAME1		"txgbe1"
#define TXGBE_SUBNETMASK1		"255.255.255.0"
#define TXGBE_IP_ADDRESS1		"192.168.5.100"
#define TXGBE_NUMBER1		1
#define TXGBE_IPv6_ADDRESS1		"fe80::300:52ff:1001"
#define TXGBE_IPv6_PRELEN1		64
#define TXGBE_IPv6_FLAGS1		0x04

#define INCLUDE_DOSFS
#define DOSFS_CACHE_SIZE		131072
#define DOSFS_MAX_FD_NUM		100

#define INCLUDE_IPNET
#define IPNET_MEMORY_LIMIT		16
#define IPNET_TIMEOUT_JOB_PRIO		2
#define IPNET_REASSEMBLY_TIMEOUT		60
#define IPNET_URGENT_COLLECTION		1

#define INCLUDE_FTP
#define FTP_PRIORITY		169
#define FTP_USERNAME		""
#define FTP_PASSWORD		""

#define HGMAC_DEV2
#define GEM2_SUBNET_MASK		"255.255.255.0"
#define GEM2_HOST_NAME		"gem2"
#define GEM2_IP_ADDRESS		"192.168.2.%d"
#define GEM2_IPv6_ADDRESS		"fe80::300:56df:%d"
#define GEM2_IPv6_PRELEN		64
#define GEM2_IPv6_FLAGS		0x04

#define INCLUDE_SHOW

#define INCLUDE_KERNEL_PTHREAD
#define REWORKS_MAX_TASKS		1024

#define INCLUDE_VXWORKS_TASK
#define VX_FP_TASK_DEFAULT		0

#define INCLUDE_TELNET
#define TELNET_PRIORITY		171
#define TELNET_USERNAME		""
#define TELNET_PASSWORD		""

#define INCLUDE_PCI_SHOW

#define DRV_TIMER0

#define INCLUDE_EVENT

#define INCLUDE_IO_MODULE

#define INCLUDE_MEMORY
#define SYS_KERNEL_MEM_SIZE		128
#define SYS_MEM_SIZE		4096UL
#define SYS_MEM_START_ADDRESS		0xffffffff80000000
#define ISR_NESTABLE		0
#define FPU_SAVECTX_ENABLE		1

#define INCLUDE_TIME_SYNC

#define INCLUDE_BUF

#define INCLUDE_PTY
#define PTY_NUMBER		1

#define INCLUDE_SERIAL

#define INCLUDE_MEM_PART
#define REWORKS_MAX_MPARTS		128

#define INCLUDE_SYMBOL_TABLE

#define INCLUDE_HRFS
#define HRFS_MAX_FD_NUM		100
#define HRFS_CACHE_SIZE		131072
#define EMMC1_FMT_STATE		1

#define INCLUDE_TIMER
#define REWORKS_MAX_TIMERS		64
#define REWORKS_MAX_RMSES		64
#define REWORKS_MAX_WDGS		64

#define INCLUDE_SEMAPHORE
#define REWORKS_MAX_SEMS		1024


