#include "net_inter_test.h"

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_MC3
 * 用例版本：S-2018-09-11
 * 测试内容：组播功能测试。
 * 测试说明：windows和reworks分别作为组播服务器（发送端）和组播客户端（接收端）
 *       发送和接收组播包，检查ReWorks是否成功收到组播包。 
 ***********************************************/
void Multicast_Recv()
{
	int fd;
	int len;
	int i = 0;
	int ret;
	struct sockaddr_in addr, sender;
	struct ip_mreq ipmr;
	char buf[BUFLEN];
	
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	
	bzero((char *)&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);
	
	bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	
	memset(&ipmr, 0, sizeof(ipmr));
	ipmr.imr_multiaddr.s_addr = inet_addr(GROUPIP);
	ipmr.imr_interface.s_addr = inet_addr(IP);//reworks ip
	
	pthread_delay(sys_clk_rate_get());
	
	//设置socket接口选项 
	setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *)&ipmr, sizeof(ipmr));
	
	len = sizeof(sender);
	
	while(i < TESTNUM)
	{
		ret = recvfrom(fd, buf, BUFLEN, 0, (struct sockaddr *)&sender, &len);
		
		if(ret < 0)
		{
			perror("recvfrom() error!");
			return;
		}
		else
		{
			buf[ret] = '\0';
			printk("counter = %d, recvive data is %s\n", i, buf);
			pthread_delay(sys_clk_rate_get());
			i ++;
		}
	}
	
	//设置socket接口选项 
	setsockopt(fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void *)&ipmr, sizeof(ipmr));
	
	close(fd);
	
	return;
}

void Multicast_Send()
{
	int fd;
	struct sockaddr_in addr;
	char buf[] = "This is a test message";
	
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	
	bzero((char *)&addr, sizeof(struct sockaddr_in));	
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(GROUPIP);
	addr.sin_port = htons(port);
	
	struct ifreq ifreq;
	struct in_addr inaddr;
	
	strncpy(ifreq.ifr_name, NET_HOST_NAME, IFNAMSIZ);
	
	//获取接口地址
	ioctl(fd, SIOCGIFADDR, &ifreq);
	
	memcpy(&inaddr,
		 &((struct sockaddr_in *)&ifreq.ifr_ifru.ifru_addr)->sin_addr,
		 sizeof(struct in_addr));
	
	//手动添加路由 
	routeAdd(GROUPIP, TESTIP);
	
	//设置socket接口选项 
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_IF, (void *)&inaddr, sizeof(inaddr));
	
	while(1)
	{
		sendto(fd, buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr));
	}
	
	close(fd);
	
	return;
}
