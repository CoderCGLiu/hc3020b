/* blasterUDPvx.c - UDP ethernet transfer benchmark for VxWorks */

/* Copyright 1986-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
04a,10may01,jgn  checkin for AE
02b,18May99,gow  created from blaster.c
*/

/*
DESCRIPTION
   With this module, VxWorks transmits blasts of UDP packets to
   a specified target IP/port.

SYNOPSIS
   blasterUDP (targetname, port, size, bufsize, zbuf)

   where:

   targetname = network address of "blasteeUDP"
   port = UDP port to connect with on "blasteeUDP"
   size = number of bytes per "blast"
   bufsize = size of transmit-buffer for blasterUDP's BSD socket
             (usually, size == bufsize)
   zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)

EXAMPLE
   To start this test, issue the following VxWorks command on the
   target:

   sp blasteeUDP,5000,1000,1000

   then issue the following UNIX commands on the host:

   gcc -O -Wall -o blasterUDP blasterUDPux.c -lsocket   (solaris)
   gcc -O -Wall -DLINUX -o blasterUDP blasterUDPux.c    (linux)
   blasterUDP 10.255.255.8 5000 1000 1000  (use appropriate IP address)

   Note: blasteeUDP should be started before blasterUDP because
   blasterUDP needs a port to send UDP packets to.

   To stop this test, call blasteeUDPQuit() in VxWorks or kill
   blasterUDP on UNIX with a control-c.

   The "blasterUDP"/"blasteeUDP" roles can also be reversed. That is,
   the VxWorks target can be the "blasterUDP" and the UNIX host can
   be the "blasteeUDP". In this case, issue the following UNIX
   commands on the host:

   gcc -O -Wall -o blasteeUDP blasteeUDPux.c -lsocket    (solaris)
   gcc -O -Wall -DLINUX -o blasteeUDP blasteeUDPux.c     (linux)
   blasteeUDP 5000 1000 1000

   and issue the following VxWorks command on the target
   (use appropriate IP address):

   sp blasterUDP,"10.255.255.4",5000,1000,1000

   To stop the test, call blasterUDPQuit() in VxWorks or kill
   blasteeUDP on Unix with a control-c.

CAVEATS
   Since this test loads the network heavily, the target and host
   should have a dedicated ethernet link.

   Be sure to compile VxWorks with zbuf support (INCLUDE_ZBUF_SOCK).

   The bufsize parameter is disabled for VxWorks. Changing UDP socket
   buffer size with setsockopt() in VxWorks somehow breaks the socket.
*/

//#include <vxworks.h>
//#include <types.h>
#include <sys/socket.h>
//#include <sockLib.h>
/* #include "zbufSockLib.h" */
//#include <inetLib.h>
//#include <ioLib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <vxworks.h>
#include <taskLib.h>
#include <wdg.h>
#include <net/if.h>
 
static wdg_t blastWd;
static int blastNum;
static int blastCount;   /*带宽计算计数*/
static int quitFlag;     /* set to 1 to quit blasterUDP gracefully */
void blasterUDPQuit(void);          /* global for shell accessibility */
static void blastRate(void);

static unsigned long long send_count = 0;
static unsigned long long pkt_complete = 0;
static unsigned long long pkt_imcomplete = 0;
static unsigned long long pkt_failed = 0;
static unsigned long long total_byte = 0;
static unsigned long long interval_time = 5;//5s

/*****************************************************************
 *
 * blasterUDP - Transmit blasts of UDP packets to blasteeUDP
 * 
 * blasterUDP (targetname, port, size, bufsize, zbuf)
 * 
 * where:
 * 
 * targetname = network address of "blasteeUDP"
 * port = UDP port on "blasteeUDP" to receive packets
 * size = number of bytes per "blast"
 * bufsize = size of transmit-buffer for blasterUDP's BSD socket
 * zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)
 * 
 * RETURNS:  N/A
 */
#if 0
void blasterUDP_func(char *targetAddr, int port, int size, int blen, int zbuf)
{
    struct sockaddr_in	sin;
    int s;      /* socket descriptor */
    int nsent; /* how many bytes sent */
    char *buffer;
    int level;

    /* setup BSD socket for transmitting blasts */

    if((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
    	printf("cannot create socket\n");
        return;
    }

    sin.sin_addr.s_addr	= inet_addr(targetAddr);
    sin.sin_port	= htons(port);
    sin.sin_family 	= AF_INET;
    bzero((char *)&sin.sin_zero, 8);  /* zero the rest of the struct */

    if((buffer = (char *)malloc(size)) == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
    	close(s);
        return;
	}
    /* setup watchdog to periodically report network throughput*/
	if ((wdg_create(&blastWd)) != 0)
	{
		printf("cannot create blast watchdog\n");
		free(buffer);
		return;
	}

    wdg_start(blastWd, sys_clk_rate_get()*10, blastRate, 0);
/* setting buffer size breaks UDP sockets for some reason.  This
 * happens on Linux too (but not Solaris). Hence setsockopt() is
 * commented till further notice.
 */

    if(setsockopt(s, SOL_SOCKET, SO_SNDBUF, (void *)&blen,
                   sizeof (blen)) < 0)
	{
    	printf("setsockopt SO_SNDBUF failed\n");
    	close(s);
		free(buffer);
        return;
	}
  
    quitFlag = 0;
    blastNum = 0;
    blastCount = 0;

    /* Loop that transmits blasts */

    while(!quitFlag)
	{
/*	if (quitFlag == 1)
	    break;
        if (zbuf)
            nsent = zbufSockBufSendto(s, buffer, size, NULL, 0, 0,
                                      (struct sockaddr *)&sin,
                                      sizeof(struct sockaddr_in));
        else  */
//    	level = int_cpu_lock();/*ldf 20230630 add:: debug*/
//    	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        nsent = sendto(s, buffer, size, 0, (struct sockaddr *)&sin, 
        			sizeof(struct sockaddr_in));
//        printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
//        int_cpu_unlock(level);/*ldf 20230630 add:: debug*/
        
        /* sendto() and zbufSockBufSendto() both OS_ERROR-out for
         * some strange reason. Somehow this doesn't seem to
         * affect the test, and we can get away with ignoring
         * the OS_ERRORs.
         */

        if(nsent>0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum+=nsent;
        	send_count++;
        	total_byte += blastNum;
        }
        else
        {
        	printf("<ERROR> [%s():_%d_]:: ret=0x%x\n", __FUNCTION__, __LINE__,nsent);
        }
//        printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
//        printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
    }
    
    /* cleanup */
    close(s);
    free(buffer);
    printf("blasterUDP exit.\n");
}
void blasterUDP (char *targetAddr, int port, int size, int blen, int zbuf)
{
	int tid;
	
	tid = taskSpawn("blasterUDP", 110, VX_FP_TASK, 0x100000, (FUNCPTR)blasterUDP_func,
			targetAddr, port, size, blen, zbuf, 0, 0, 0, 0, 0);
	
	pthread_detach((pthread_t)tid);

	
	return;
}
#else
void blasterUDP(char *targetAddr, int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in	sin;
    int s;      /* socket descriptor */
    int nsent; /* how many bytes sent */
    char *buffer;
    int level;/*ldf 20230630 add:: debug*/
    int i =0;

    /* setup BSD socket for transmitting blasts */

    if((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
    	printf("cannot create socket\n");
        return;
    }
    
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_send_nic_name, IFNAMSIZ);
		if(setsockopt(s, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("udp-send NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif

    sin.sin_addr.s_addr	= inet_addr(targetAddr);
    sin.sin_port	= htons(port);
    sin.sin_family 	= AF_INET;
    bzero((char *)&sin.sin_zero, 8);  /* zero the rest of the struct */

    if((buffer = (char *)malloc(size)) == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
    	close(s);
        return;
	}
    
    for(i = 0; i < size; i++)
    {
    	buffer[i] = 0x12 + i;
    }
    
    /* setup watchdog to periodically report network throughput*/
	if ((wdg_create(&blastWd)) != 0)
	{
		printf("cannot create blast watchdog\n");
		free(buffer);
		return;
	}

    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);
/* setting buffer size breaks UDP sockets for some reason.  This
 * happens on Linux too (but not Solaris). Hence setsockopt() is
 * commented till further notice.
 */

    if(setsockopt(s, SOL_SOCKET, SO_SNDBUF, (void *)&blen,
                   sizeof (blen)) < 0)
	{
    	printf("setsockopt SO_SNDBUF failed\n");
    	close(s);
		free(buffer);
        return;
	}
  
    quitFlag = 0;
    blastNum = 0;
    blastCount = 0;
    send_count = 0;
    pkt_complete = 0;
    pkt_imcomplete = 0;
    pkt_failed = 0;
    total_byte = 0;

    /* Loop that transmits blasts */

    while(!quitFlag)
	{
/*	if (quitFlag == 1)
	    break;
        if (zbuf)
            nsent = zbufSockBufSendto(s, buffer, size, NULL, 0, 0,
                                      (struct sockaddr *)&sin,
                                      sizeof(struct sockaddr_in));
        else  */

    	*(unsigned int*)buffer = (unsigned int)send_count;
    	
        nsent = sendto(s, buffer, size, 0, (struct sockaddr *)&sin, 
        			sizeof(struct sockaddr_in));
        
        /* sendto() and zbufSockBufSendto() both OS_ERROR-out for
         * some strange reason. Somehow this doesn't seem to
         * affect the test, and we can get away with ignoring
         * the OS_ERRORs.
         */

        if(nsent > 0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum += nsent;
        	total_byte += nsent;
        	if(nsent == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }
        else
        {
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:udp send failed!! ret=0x%x\n", __FUNCTION__, __LINE__,send_count,nsent);
        }
//        printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
//        printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", send_count, pkt_complete,pkt_imcomplete,pkt_failed);
        send_count++;
	}
    
    /* cleanup */
    close(s);
    free(buffer);
    wdg_delete(blastWd);
    printf("blasterUDP exit.\n");
}
#endif

/* watchdog handler. reports network throughput */
static void blastRate ()
{
    if(blastNum > 0)
	{
    	printk("UDP-SEND: NO.%u pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d  |  speed:%d bytes/sec  total:%u MB\r\n", 
    			send_count,pkt_complete,pkt_imcomplete,pkt_failed,blastNum/interval_time,total_byte/1024/1024);
    	blastNum = 0;    	
	}
    else
    	printk("No bytes send in the last %u seconds.\n", interval_time);
    blastCount++;
//    if(blastCount > 30) {
//    	quitFlag = 1;
//    	return;
//    }
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);
}

/* make blasterUDP stop */

void blasterUDPQuit(void)
{
    quitFlag = 1;
}
