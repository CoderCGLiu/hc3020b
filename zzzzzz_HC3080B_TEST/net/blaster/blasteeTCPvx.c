/* blasteeTCPvx.c - TCP ethernet transfer benchmark for VxWorks */
                    
/* Copyright 1986-2001 Wind River Systems, Inc. */

/*               
modification history
--------------------
04a,10may01,jgn  checkin for AE
03c,20May99,gow  Split off from blastee.c
03b,17May99,gow  Extended to zero-copy sockets
03a,02Mar99,gow  Better documentation. Tested Linux as "blastee".
02a,19dec96,lej  Fitted formatting to Wind River convention
01b,15May95,ism  Added header
01a,24Apr95,ism  Unknown Date of origin
*/                               
                                 
/*
DESCRIPTION
   With this module, a VxWorks target ("blasteeTCP") receives
   blasts of TCP packets and reports network throughput on 10
   second intervals.

SYNOPSIS
   blasteeTCP (port, size, bufsize, zbuf)

   where:

   port = port that "blasterTCP" should connect with
   size = number of bytes per "blast"
   bufsize = size of receive-buffer for blasteeTCP's BSD socket
             (usually, size == bufsize)
   zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)

EXAMPLE
   To start this test, issue the following VxWorks command on the
   target:

   sp blasteeTCP,5000,16000,16000

   then issue the following UNIX commands on the host:

   gcc -O -Wall -o blasterTCP blasterTCPux.c -lsocket   (solaris)
   gcc -O -Wall -DLINUX -o blasterTCP blasterTCPux.c    (linux)
   blasterTCP 10.255.255.8 5000 16000 16000  (use appropriate IP address)

   Note: blasteeTCP should be started before blasterTCP because
   blasterTCP needs a port to connect to.

   To stop this test, call blasteeTCPQuit() in VxWorks or kill
   blasterTCP on UNIX with a control-c.

   The "blasterTCP"/"blasteeTCP" roles can also be reversed. That is,
   the VxWorks target can be the "blasterTCP" and the UNIX host can
   be the "blasteeTCP". In this case, issue the following UNIX
   commands on the host:

   gcc -O -Wall -o blasteeTCP blasteeTCPux.c -lsocket    (solaris)
   gcc -O -Wall -DLINUX -o blasteeTCP blasteeTCPux.c     (linux)
   blasteeTCP 5000 16000 16000

   and issue the following VxWorks command on the target
   (use appropriate IP address):

   sp blasterTCP,"10.255.255.4",5000,16000,16000

   To stop the test, call blasterTCPQuit() in VxWorks or kill
   blasteeTCP on Unix with a control-c.

CAVEATS
   Since this test loads the network heavily, the target and host
   should have a dedicated ethernet link.

   Be sure to compile VxWorks with zbuf support (INCLUDE_ZBUF_SOCK).
*/

//#include <vxworks.h>
//#include <ioLib.h>
//#include <logLib.h>
//#include "memLib.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <sockLib.h>
//#include "zbufSockLib.h"
//#include "zbufLib.h"
#include <stdio.h>		
#include <stdlib.h>
#include <string.h>
//#include <sysLib.h>
//#include "tftpLib.h"
#include <wdLib.h>
#include <taskLib.h>
#include <clock.h>
#include <reworks/printk.h>
#include <wdg.h>
#include <pthread.h>
#include <vxworks.h>
#include <taskLib.h>
#include <net/if.h>

static wdg_t blastWd;    /* for periodic throughput reports */
static int blastNum;     /* number of bytes read per 10 second interval */
static int blastCount;   /*带宽计算计数*/
static char *buffer;     /* receive buffer */
static int sock;        /* server socket descriptor */
static int snew;        /* per-client socket descriptor */
static int tid;         /* task ID for cleanup purposes */
static int quitFlag;    /* flag for stopping test */

/* forward declarations */

static void blastRate ();
void blasteeTCPQuit(void);       /* global for shell accessibility */

static unsigned long long recv_count = 0;
static unsigned long long pkt_complete = 0;
static unsigned long long pkt_imcomplete = 0;
static unsigned long long pkt_failed = 0;
static unsigned long long total_byte = 0;
static unsigned long long interval_time = 5;//5s
static unsigned long long sw_count = 0;
static unsigned long long pkt_dropped = 0;
static unsigned long long total_pkt_dropped = 0;

/*****************************************************************
 *
 * blasteeTCP - Accepts blasts of TCP packets from blasterTCP
 * 
 * This program accepts blasts of TCP packets from blasterTCP
 * and prints out the throughput every 10 seconds.
 * 
 * blasteeTCP 
 * where:
 * 
 * port = TCP port to connect with on "blasterTCP"
 * size = number of bytes per "blast"
 * bufsize = size of receive-buffer for blasteeTCP's BSD socket
 * zbuf = whether to use zero-copy sockets (1 = yes, 0 = no)
 * 
 * RETURNS:  N/A
 */

#if 0
void blasteeTCP_func(int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in serverAddr, clientAddr;
    int len;    /* sizeof(clientAddr) */
    int nrecv;  /* number of bytes received */

    tid = pthread_self();
    
    if((buffer = (char *)malloc(size)) == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
    	
    	return;
	}

    /* setup watchdog to periodically report network throughput */

    if((wdg_create(&blastWd)) != 0)
	{
    	printf("cannot create blast watchdog\n");
    	free(buffer);
    	
    	return;
	}
    
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);

    /* setup BSD socket to read blasts from */

    bzero((char *)&serverAddr, sizeof(serverAddr));
    bzero((char *)&clientAddr, sizeof(clientAddr));

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
    	printf("cannot open socket\n");
        wdg_delete(blastWd);
		free(buffer);
		
        return;
	}

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port   = htons(port);

    if(bind(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
	{
    	printf("bind OS_ERROR\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    if(listen(sock, 5) < 0)
	{
    	printf("listen failed\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    len = sizeof(clientAddr);

    while((snew = accept(sock, (struct sockaddr *)&clientAddr, &len)) == -1);

    blastNum = 0;
    quitFlag = 0;
    blastCount = 0;

    if(setsockopt(snew, SOL_SOCKET, SO_RCVBUF, (char *)&blen, sizeof(blen)) < 0)
	{
    	printf("setsockopt SO_RCVBUF failed\n");
        close(sock);
        close(snew);
        wdg_delete(blastWd);
		free(buffer);		
        return;
	}
    printf("connect ok\n");
    /* loop that reads TCP blasts */
    while(1)
	{
    	if(quitFlag == 1)
    		break;
        nrecv = read(snew, buffer, size);   
        
        if(nrecv > 0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum += nrecv;
        	total_byte += nrecv;
        	if(nrecv == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }
        else
        {
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:tcp recv failed!! ret=0x%x\n", __FUNCTION__, __LINE__,recv_count,nrecv);
        }

        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
        
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", recv_count, pkt_complete,pkt_imcomplete,pkt_failed);
		recv_count++;
	}
    /* cleanup */
    wdg_delete(blastWd);
    close(sock);
    close(snew);
    free(buffer);    
    printf("blasteeTCP end.\n");
}

void blasteeTCP (int port, int size, int blen, unsigned long zbuf)
{
	int tid;
	
	tid = taskSpawn("blasteeTCP", 170, VX_FP_TASK, 0x100000, (FUNCPTR)blasteeTCP_func,
			port, size, blen, zbuf, 0, 0, 0, 0, 0, 0);
	
//	pthread_detach((pthread_t)tid);
	
	return;
}
#else
void blasteeTCP(int port, int size, int blen, unsigned long zbuf)
{
    struct sockaddr_in serverAddr, clientAddr;
    int len;    /* sizeof(clientAddr) */
    int nrecv;  /* number of bytes received */

    tid = pthread_self();
    
    if((buffer = (char *)malloc(size)) == NULL)
	{
    	printf("cannot allocate buffer of size %d\n", size);
    	
    	return;
	}

    /* setup watchdog to periodically report network throughput */

    if((wdg_create(&blastWd)) != 0)
	{
    	printf("cannot create blast watchdog\n");
    	free(buffer);
    	
    	return;
	}
    
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);

    /* setup BSD socket to read blasts from */

    bzero((char *)&serverAddr, sizeof(serverAddr));
    bzero((char *)&clientAddr, sizeof(clientAddr));

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
    	printf("cannot open socket\n");
        wdg_delete(blastWd);
		free(buffer);
		
        return;
	}
    
#if 1/*ldf 20230831 add:: 绑定网卡*/
    extern unsigned long long ldf_is_bind_nic;
    extern char* ldf_send_nic_name;
    extern char* ldf_recv_nic_name;
    if(ldf_is_bind_nic == 1){
		struct ifreq ifr;
		strncpy(ifr.ifr_name, ldf_recv_nic_name, IFNAMSIZ);
		printk("ifr.ifr_name : %s\n",ifr.ifr_name);
		if(setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, &ifr, IFNAMSIZ) < 0)
		{
			printk("tcp-recv NIC bind failed.  errno=%d\r\n",errno);
			return;
		}
    }
#endif
    
    int reuse = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
	{
    	printf("setsockopt SO_REUSEADDR failed\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port   = htons(port);

    if(bind(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
	{
    	printf("bind OS_ERROR\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    if(listen(sock, 5) < 0)
	{
    	printf("listen failed\n");
        close(sock);
        wdg_delete(blastWd);
        free(buffer);
        
        return;
	}

    len = sizeof(clientAddr);

    while((snew = accept(sock, (struct sockaddr *)&clientAddr, &len)) == -1);

    blastNum = 0;
    quitFlag = 0;
    blastCount = 0;
    recv_count = 0;
    pkt_complete = 0;
    pkt_imcomplete = 0;
    pkt_failed = 0;
    total_byte = 0;

    if(setsockopt(snew, SOL_SOCKET, SO_RCVBUF, (char *)&blen, sizeof(blen)) < 0)
	{
    	printf("setsockopt SO_RCVBUF failed\n");
        close(sock);
        close(snew);
        wdg_delete(blastWd);
		free(buffer);		
        return;
	}
    printf("[%s:%d] newfd = %d\n", inet_ntoa(clientAddr.sin_addr),ntohs(clientAddr.sin_port),snew);
    
    /* loop that reads TCP blasts */
    while(1)
	{
    	if(quitFlag == 1)
    		break;
        nrecv = read(snew, buffer, size);   
        
        if(nrecv > 0){
//        	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
        	blastNum += nrecv;
        	total_byte += nrecv;
        	if(nrecv == size)
        		pkt_complete++;
        	else
        		pkt_imcomplete++;
        }
        else
        {
        	pkt_failed++;
        	printf("<ERROR> [%s():_%d_]:: NO.%d:tcp recv failed!! ret=0x%x\n", __FUNCTION__, __LINE__,recv_count,nrecv);
        }

        extern unsigned long long ldf_udelay;/*ldf 20230718 add*/
        usleep(ldf_udelay);/*ldf 20230718 add*/
        
//        printf("  NO.%u  pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d\r\n", recv_count, pkt_complete,pkt_imcomplete,pkt_failed);
		recv_count++;
	}
    /* cleanup */
    wdg_delete(blastWd);
    close(sock);
    close(snew);
    free(buffer);    
    printf("blasteeTCP end.\n");
}
#endif

/* watchdog handler. reports network throughput */

static void blastRate()
{
    if(blastNum > 0)
	{
    	printk("TCP-RECV: NO.%u pkt_complete:%d  pkt_imcomplete:%d  pkt_failed:%d  |  speed:%d bytes/sec  total:%u MB\r\n", 
    			recv_count,pkt_complete,pkt_imcomplete,pkt_failed,blastNum/interval_time,total_byte/1024/1024);
    	blastNum = 0;    	
	}
    else
    	printk("No bytes send in the last %u seconds.\n", interval_time);
    blastCount++;
//    if(blastCount > 30) {
//    	quitFlag = 1;
//    	return;
//    }
    wdg_start(blastWd, sys_clk_rate_get()*interval_time, blastRate, 0);
}

/* make blasteeTCP stop */

void blasteeTCPQuit(void)
{
    quitFlag = 1;
    pthread_delay(60); 
    
    /* try to end gracefully */
    if(pthread_verifyid(tid) == OS_OK) /* blasteeTCP still trying to read() */
    {
        close(sock);
        close(snew);
        
        wdg_delete(blastWd);
        free(buffer);
        pthread_cancel(tid);
        
        printf ("blasteeTCP forced stop.\n");
    }
}
