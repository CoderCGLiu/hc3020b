#ifndef __IPV4_IPV6_H__
#define __IPV4_IPV6_H__

//#define INET6

//#define MACADDR "2C:27:D7:31:B5:F4" //瀹夸富鏈虹殑 mac 鍦板潃
#define  MACADDR  "6C:3B:E5:1F:9B:D8" //zkw瀹夸富鏈虹殑 mac 鍦板潃

#ifdef _SBC8641D_
#define if_name_unit  "mottsec0"  // 8641d eTSEC1 缃戝彛閰嶇疆
#endif

#ifdef __MEC7004__
#define if_name_unit  "em0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef _P1020RDB_
#define if_name_unit  "mottsec0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef _LOONGSON2F_64_
#define if_name_unit  "gei0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef _LOONGSON2H_64_
#define if_name_unit  "gmac0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef _LOONGSON1B_BSP_
#define if_name_unit  "gmac0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef _LOONGSON3A_780E_
#define if_name_unit  "em0"  // 璇锋牴鎹綉鍗＄殑閰嶇疆杩涜淇敼
#endif

#ifdef INET6

#define SOCK_DOMAIN	AF_INET6
typedef struct sockaddr_in6 sockaddr_in_type;
#define m_sin_family	sin6_family
#define m_sin_port		sin6_port
#define m_sin_flowinfo	sin6_flowinfo
#define m_sin_addr		sin6_addr
#define m_sin_scope_id	sin6_scope_id
#define addr_any		in6addr_any
#define m_addrlen			sizeof(struct sockaddr_in6)
#define m_sin_len               sin6_len
#define M_INET_ADDRSTRLEN    INET6_ADDRSTRLEN
#define LOCAL_IPADDR   "::1"
#define TESTIP "fe80::6e3b:e5ff:fe1f:9bd8" //瀹夸富鏈�IP
#define IP "fe80::204:9fff:fe02:82b1" //鐩爣鏈�IP 192.168.1.13
#define TESTERRORIP "fe80::204:9fff:fe02:82b2"
#else // !INET6

#define SOCK_DOMAIN AF_INET
typedef struct sockaddr_in sockaddr_in_type;
#define m_sin_family	sin_family
#define m_sin_port		sin_port
#define m_sin_flowinfo	sin_flowinfo
#define m_sin_addr		sin_addr
#define addr_any		INADDR_ANY
#define m_addrlen			sizeof(struct sockaddr)
#define m_sin_len               sin_len
#define M_INET_ADDRSTRLEN    INET_ADDRSTRLEN
#define LOCAL_IPADDR   "127.0.0.1"
#define TESTIP "192.168.1.22"
#define IP "192.168.100.12"
#define TESTERRORIP "192.168.1.29"
#endif

#endif //__IPV4_IPV6_H__
