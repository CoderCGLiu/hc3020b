#include "net_inter_test.h"

//#define BROADCAST_ADDR "192.168.0.255" 

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_BC3
 * 用例版本：S-2018-09-11
 * 测试内容：广播功能测试。
 * 测试说明：Windows和ReWorks分别作为广播服务器（发送端）和广播客户端
 *       （接收端）发送和接收广播包，检查ReWorks是否成功收到广播包。 
 ***********************************************/
void Broadcast_Recv()
{
	int i;
	int fd;
	int recvNum;
	char buffer[64];
	struct sockaddr_in addr;
	
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	
	bzero((char *)&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	
	bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	
	for(i = 0; i< 10;i++)
	{
		if((recvNum = recv(fd, buffer, sizeof(buffer), 0)) == OS_ERROR)
		{
			printk("recv() broadcast failed\n");
			return;
		}
		else
			printk("counter = %d, received %d bytes of broadcast message:  %s\n", i, recvNum, buffer);
	}
	
	close(fd);
	
	return;
}
#if 0
void Broadcast_Send()
{
	int fd;
	int sendNum;
	struct sockaddr_in addr;
	char buffer[] = "Hello ReWorks!";
	
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	
	routeAdd("0.0.0.0", TESTIP);
	
	int on = 1;
	
	setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (char *)&on, sizeof(int));
	
	bzero((char *)&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(BROADCAST_ADDR);
	
	while(1)
	{
		if((sendNum = sendto(fd, buffer, sizeof(buffer), 0, (struct sockaddr *)&addr,
							sizeof(struct sockaddr_in))) == OS_ERROR)
		{
			perror("sendto() broadcast failed ");
			return;
		}
		
		pthread_delay(sys_clk_rate_get() * 3);
		
		printk("%d bytes of broadcast message sent: %s\n", sendNum, buffer);
	}
	
	close (fd);

	return;
}
#endif
