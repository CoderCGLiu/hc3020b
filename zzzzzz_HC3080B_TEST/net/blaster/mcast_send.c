#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MCAST_SEND_IP	"192.168.1.105"

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_MC2
 * 用例版本：S-2021-05-24
 * 测试内容：组播功能测试。
 * 测试说明：ReWorks作为发送端发送组播包。 
 ***********************************************/
void multcast_send(char *groupIp, int port)
{ 
	int sock_fd;
	struct sockaddr_in addr;
	char buf[] = "This is a test message";
	int len;
	int ret;

	if((sock_fd = socket(AF_INET,SOCK_DGRAM,0))<0)
	{
		printf("socket error\n");
		return ;
	}
	
	memset((void*)&addr,0,sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(groupIp);
	addr.sin_port = htons(port);
	
	len = sizeof(addr);
	
	u_char ttl = 15;
	u_char is_loop = 1;

	struct in_addr inaddr;
	
	struct sockaddr_in sin;
	sin.sin_addr.s_addr = inet_addr(MCAST_SEND_IP);
	memcpy(&inaddr, &sin.sin_addr, sizeof(struct in_addr));
	
	int blen = 65535;
    setsockopt(sock_fd, SOL_SOCKET, SO_SNDBUF, (void *)&blen, sizeof(blen));

	ret = setsockopt(sock_fd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl,sizeof(ttl));
	printf("setsockopt IP_MULTICAST_TTL ret = %d\n",ret);
	
	ret = setsockopt(sock_fd, IPPROTO_IP, IP_MULTICAST_IF, &inaddr,sizeof(inaddr));
	printf("setsockopt IP_MULTICAST_IF ret = %d\n",ret);
	
	ret = setsockopt(sock_fd, IPPROTO_IP, IP_MULTICAST_LOOP, &is_loop, sizeof(is_loop));
	printf("setsockopt IP_MULTICAST_LOOP ret = %d\n",ret);
	
	while(1)
	{
		ret = sendto(sock_fd, buf, strlen(buf), 0, (struct sockaddr *)&addr, len);
		if(ret < 0)
		{
			perror("sendto error\n");
		}
		else
		{
			sleep(5);
			printf("sendto success\n");
		}
	}
	
	close(sock_fd);
	return ; 
}
