#include <drv/net_board_com.h>
#include "../basic/testing.h"

//#include "../basic/ipcom_pkt.h"

static struct sockaddr_ll
{
    u_int8_t    sll_len;        /* Length of this structure */
    u_int8_t    sll_family;     /* Always AF_PACKET */

    /* 
     * Link layer protocol (frame type) in network order, not used when 
     * sending frames on an SOCK_RAW type socket 
     */
    u_int16_t   sll_protocol;  
    u_int32_t   sll_ifindex;    /* Interface index, must not be zero */

    /* Link layer type (IFT_xxx), only used for received frames */
    u_int16_t   sll_hatype;     

    /* 
     * Packet type (PACKET_xxx), only used for received frames. Valid
     * types are:
     * PACKET_HOST - for packets addressed to the local host
     * PACKET_BROADCAST  - for physical layer broadcast packets to all 
     *                     hosts on this link
     * PACKET_MULTICAST - for packets sent to a physical layer multicast
     *                   address
     * PACKET_OTHERHOST - for packets send to some other host that has
     *                    been caught by a device driver in promiscuous
     *                    mode
     * PACKET_OUTGOING - for packets originated from local host that
     *                   is looped back to a packet socket.
     */
    u_int8_t    sll_pkttype;  
    
    u_int8_t    sll_halen;      /* Length of link layer address */
    u_int8_t    sll_addr[8];    /* Link layer address */
};

#define PAU_BUFFER_MAX 2048
static int recv_test_quit = 0;
static int send_test_quit = 0;
static int print_flag = 0;
#define PAU_ETH_TYPE 0x0800 //以太网协议type

#define PAU_ETH_ALEN 6
#define T_ETH_PROTOCOL_ALL 0
#define PAUSE_FRAME_TYPE 0x8808 //PAUSE帧类型
#define PAUSE_OPCODE 0x0001  //PAUSE帧操作码
#define PAUSE_DST_MAC {0x01,0x80,0xC2,0x00,0x00,0x01} //PAUSE帧目的mac地址
#define PAUSE_TIME 0xFFFF //PAUSE帧要求对方停止的时间

//发送PAUSE帧网卡信息
#define SRC_NC_NAME  "gmac0" 
#define SRC_NC_MAC {0x00,0x55,0x7a,0xb5,0x7d,0xf7} //网卡MAC地址

//接收PAUSE帧网卡信息
#define RECV_NC_NAME  "gmac1" 
#define RECV_NC_MAC {0x00,0x55,0x7a,0xb5,0x7d,0xf8}
		
void SendPauseFrame(void)
{
	int i = 0;
	int frame_length;
//	unsigned char send_buf[PRO_BUFFER_MAX];
	unsigned char data[PAU_BUFFER_MAX];
	unsigned char ether_frame[PAU_BUFFER_MAX];
	unsigned char dst_mac[PAU_ETH_ALEN] = PAUSE_DST_MAC; //RAW_NC_MAC_R;	//ljc对方mac
	unsigned char src_mac[PAU_ETH_ALEN] = SRC_NC_MAC;

	int send_len = 0;
	int RawSock = -1;
	struct sockaddr_ll sll,TempAddr;
	struct ifreq ifstruct;
//	int sockAddrSize = sizeof(struct sockaddr);
	int count = 5000;
	
	RawSock = socket(PF_PACKET, SOCK_RAW, htons(T_ETH_PROTOCOL_ALL));
	if(RawSock == -1)
	{   
		printf("create RawSock failed\n");
		return;
	}

	//get net card index
	memset(&ifstruct, 0, sizeof (ifstruct));
	strcpy(ifstruct.ifr_name,SRC_NC_NAME);
	if(-1 == ioctl(RawSock,SIOCGIFINDEX,&ifstruct))
	{
		printf("ioctl SIOCGIFINDEX failed\n");
		return;
	}

	memset(&sll,0,sizeof(sll));
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex = ifstruct.ifr_ifindex;
	memcpy (sll.sll_addr, src_mac, PAU_ETH_ALEN);
//	printf("MAC address: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",sll.sll_addr[0],sll.sll_addr[1],sll.sll_addr[2],sll.sll_addr[3],sll.sll_addr[4],sll.sll_addr[5]);
	sll.sll_halen = htons(PAU_ETH_ALEN);
	
	 // Fill out ethernet frame header.
	frame_length = PAU_ETH_ALEN + PAU_ETH_ALEN + 2 + 2 + 2;
	// Destination and Source MAC addresses
	memcpy (ether_frame, dst_mac, PAU_ETH_ALEN);
	memcpy (ether_frame + PAU_ETH_ALEN, src_mac, PAU_ETH_ALEN);
    ether_frame[12] = PAUSE_FRAME_TYPE / 256;
    ether_frame[13] = PAUSE_FRAME_TYPE % 256;
    ether_frame[14] = PAUSE_OPCODE / 256;
    ether_frame[15] = PAUSE_OPCODE % 256;
    ether_frame[16] = PAUSE_TIME / 256;
    ether_frame[17] = PAUSE_TIME % 256;

	if(-1 == bind(RawSock,(struct sockaddr *)&sll,sizeof(sll)))
	{
		printf("bind RawSock failed\n");
		return;
	}

	while(count){
		printf("send pause frame...\n");
		send_len = sendto(RawSock,ether_frame,frame_length,0,(struct sockaddr *)&sll,sizeof(sll));
		if(print_flag) printf("send pause frame end!\n");
		if(send_len <= 0){
			printf("sendto failed send_len = %d errno = %d\n",send_len,errno);
			return;
		}else{	
			count--;
			//将print_flag置1可打印发送数据
			if(print_flag)
			{
				printf("send_len %d: ",send_len);
				for(i = 0;i < send_len; i++)
					printf("0x%x ",ether_frame[i]);
				printf("\n");
			}
		}
	}
	printf("send pause frame end\n");
	close(RawSock);
	return;
}

void RecvPacket(void)
{
	int i = 0;
	unsigned char recv_buf[PAU_BUFFER_MAX] = {0};

	int recv_len = 0;
	unsigned char src_mac[PAU_ETH_ALEN] = SRC_NC_MAC;
//	unsigned char oth_mac[PRO_ETH_ALEN] = PRO_NC_MAC_OTH;
	int RawSock = -1;
	struct sockaddr_ll sll,TempAddr;
	int sockAddrSize = sizeof(struct sockaddr);
	struct ifreq ifstruct;
	u64 t1,t2;
//	int PassFlag = TS_FAIL;
//	int equalflag = 0;
	
	RawSock = socket(PF_PACKET, SOCK_RAW, htons(T_ETH_PROTOCOL_ALL));
	if(RawSock == -1)
	{   
		printf("create RawSock failed\n");
		return;
	}

	//get net card index
	memset(&ifstruct, 0, sizeof (ifstruct));
	strcpy(ifstruct.ifr_name,SRC_NC_NAME);
	if(-1 == ioctl(RawSock,SIOCGIFINDEX,&ifstruct))
	{
		printf("ioctl SIOCGIFINDEX failed\n");
		return;
	}

	memset(&sll,0,sizeof(sll));
	sll.sll_family = PF_PACKET; 
	sll.sll_ifindex = ifstruct.ifr_ifindex;
	memcpy (sll.sll_addr, src_mac, PAU_ETH_ALEN);
//	printf("MAC address: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",sll.sll_addr[0],sll.sll_addr[1],sll.sll_addr[2],sll.sll_addr[3],sll.sll_addr[4],sll.sll_addr[5]);
	sll.sll_halen = htons(PAU_ETH_ALEN);

	// Destination and Source MAC addresses
	if(-1 == bind(RawSock,(struct sockaddr *)&sll,sizeof(sll)))
	{
		printf("bind RawSock failed\n");
		return;
	}

	while(1)
	{
		if(recv_test_quit)
			break;
		memset(recv_buf,0,sizeof(recv_buf));
		printf("recv packet...\n");
		t1 = sys_timestamp();
		recv_len = recvfrom(RawSock,recv_buf,sizeof(recv_buf),0,(struct sockaddr *)&TempAddr,(int *)&sockAddrSize); 
		t2 = sys_timestamp();
//		if(print_flag) printf("recv success!\n");
		if(recv_len <= 0){
			printf("recvfrom error\n");
			return;
		}else{
			if(recv_buf[0]&0x01) continue;
			printf("recv packet success!\n");
			if(print_flag)
			{
				printf("recv_len %d: ",recv_len);
				for(i = 0;i < recv_len; i++)
					printf("0x%x ",recv_buf[i]);
				printf("\n");
				printf("time is %d\n",t2-t1);
			}
		}
		recv_len = 0;
		pthread_delay(3*sys_clk_rate_get());
	}

//	printf("test end, flag = %d\n",PassFlag);
//	Other_GoOnChoice( "test_promisc",PassFlag);
	close(RawSock);
	return;
}

void SendPacket(void)
{
	int i = 0;
	int datalen,frame_length;
//	unsigned char send_buf[PRO_BUFFER_MAX];
	unsigned char data[PAU_BUFFER_MAX];
	unsigned char ether_frame[PAU_BUFFER_MAX];
	unsigned char dst_mac[PAU_ETH_ALEN] = SRC_NC_MAC; //RAW_NC_MAC_R;	//ljc对方mac
	unsigned char src_mac[PAU_ETH_ALEN] = RECV_NC_MAC;

	int send_len = 0;
	int RawSock = -1;
	struct sockaddr_ll sll,TempAddr;
	struct ifreq ifstruct;
//	int sockAddrSize = sizeof(struct sockaddr);
//	int count = 10000;
	u64 t1,t2;
	
	RawSock = socket(PF_PACKET, SOCK_RAW, htons(T_ETH_PROTOCOL_ALL));
	if(RawSock == -1)
	{   
		printf("create RawSock failed\n");
		return;
	}

	//get net card index
	memset(&ifstruct, 0, sizeof (ifstruct));
	strcpy(ifstruct.ifr_name,RECV_NC_NAME);
	if(-1 == ioctl(RawSock,SIOCGIFINDEX,&ifstruct))
	{
		printf("ioctl SIOCGIFINDEX failed\n");
		return;
	}

	memset(&sll,0,sizeof(sll));
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex = ifstruct.ifr_ifindex;
	memcpy (sll.sll_addr, src_mac, PAU_ETH_ALEN);
//	printf("MAC address: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",sll.sll_addr[0],sll.sll_addr[1],sll.sll_addr[2],sll.sll_addr[3],sll.sll_addr[4],sll.sll_addr[5]);
	sll.sll_halen = htons(PAU_ETH_ALEN);
	
	datalen = 12;
	data[0] = 'h';
	data[1] = 'e';
	data[2] = 'l';
	data[3] = 'l';
	data[4] = 'o';
	data[5] = ' ';
	data[6] = 'w';
	data[7] = 'o';
	data[8] = 'r';
	data[9] = 'l';
	data[10] = 'd';
	data[11] = '!';
	 // Fill out ethernet frame header.
	frame_length = PAU_ETH_ALEN + PAU_ETH_ALEN + 2 + datalen;
	// Destination and Source MAC addresses
	memcpy (ether_frame, dst_mac, PAU_ETH_ALEN);
	memcpy (ether_frame + PAU_ETH_ALEN, src_mac, PAU_ETH_ALEN);
    ether_frame[12] = PAU_ETH_TYPE / 256;
    ether_frame[13] = PAU_ETH_TYPE % 256;
    memcpy (ether_frame + 14 , data, datalen);

	if(-1 == bind(RawSock,(struct sockaddr *)&sll,sizeof(sll)))
	{
		printf("bind RawSock failed\n");
		return;
	}

	while(1){
		if(send_test_quit)
			break;
		printf("send packet...\n");
		t1 = sys_timestamp();
		send_len = sendto(RawSock,ether_frame,frame_length,0,(struct sockaddr *)&sll,sizeof(sll));
		t2 = sys_timestamp();
		if(print_flag) printf("send packet end!\n");
		if(send_len <= 0){
			printf("sendto failed send_len = %d errno = %d\n",send_len,errno);
			return ;
		}else{	
			//将print_flag置1可打印发送数据
			if(print_flag)
			{
				printf("send_len %d: ",send_len);
				for(i = 0;i < send_len; i++)
					printf("0x%x ",ether_frame[i]);
				printf("\n");
				printf("time is %d\n",t2-t1);
			}
		}
//		pthread_delay(3*sys_clk_rate_get());
	}
	printf("send packet end\n");
	close(RawSock);
	return;
}

void flowcon_recv_stop_flag()
{
	recv_test_quit = 1;
}

void flowcon_send_stop_flag()
{
	send_test_quit = 1;
}

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_FLOWCON1
 * 用例版本：2021-11-11
 * 测试内容：网络流控功能测试（pause发送端）。
 * 测试说明：测试网卡的流量控制功能中网卡发送pause帧的能力。 
 ***********************************************/
int test_flowcontrol_sendpause()
{
	pthread_t tid;
	int ret = pthread_create2(&tid, "SendPauseFrame", 155, 0, 0x10000, SendPauseFrame, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printf("pthread_create2 -error %d!\n", ret);
		return ERROR;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printf("pthread_detach failed\n");
		return ERROR;
	}
}

int test_flowcontrol_recvpkt()
{
	pthread_t tid;
	int ret = pthread_create2(&tid, "RecvPacket", 155, 0, 0x10000, RecvPacket, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printf("pthread_create2 -error %d!\n", ret);
		return ERROR;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printf("pthread_detach failed\n");
		return ERROR;
	}
}

/************************************************
 * 测试名称：网络测试
 * 测试标识：BSP_NET_FLOWCON2
 * 用例版本：2021-11-11
 * 测试内容：网络流控功能测试（pause接收端）。
 * 测试说明：测试网卡的流量控制功能中网卡处理pause帧的能力。 
 ***********************************************/
int test_flowcontrol_sendpkt()
{
	pthread_t tid;
	int ret = pthread_create2(&tid, "SendPacket", 155, 0, 0x10000, SendPacket, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printf("pthread_create2 -error %d!\n", ret);
		return ERROR;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printf("pthread_detach failed\n");
		return ERROR;
	}
}
