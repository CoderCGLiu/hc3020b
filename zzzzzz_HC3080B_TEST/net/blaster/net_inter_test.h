#ifdef _RTP_TEST
#include <vxworks.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <errnoLib.h>
#include <eventLib.h>
#include <taskLib.h>
#include <private/taskLibP.h>
#include <sdLib.h>
#include <hookLib.h>
#include <semLib.h>
#include <msgQLib.h>
#include <sysLib.h>
#include <memLib.h>
#include <memPartLib.h>
#include <string.h>
#include <tickLib.h>
#include <signal.h>
#include <ioLib.h> 
//#include <Stat.h>
#include <sys/stat.h>
#include <Utime.h>
//#include <dirLib.h>
#include <time.h> 
#include <Dirent.h>
#include <rtpLibCommon.h>
#include <hostLib.h>

#include <inetLib.h> 
#ifdef _USER_TEST
#include <dlfcn.h>
#include <ipnet/ipioctl.h>
#endif
#ifdef _KERNEL_TEST
#include <taskVarLib.h>
#include <wdLib.h>
#include <intLib.h>
#include <clock.h> 
#include <timer_show.h> 
#include <inline/semLibInline.h>
#include <private/taskSysCall.h>
#include <iosLib.h>
#include <arpLib.h> 
#endif

#else
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <select.h>
#include <vxworks.h>
#include <ioLib.h>
#include <logLib.h>
#include <pipeDrv.h> 
#include <iosLib.h>
#include <sysLib.h>
#include <memLib.h>
#include <errnoLib.h> 
#include <tickLib.h>
#include <usrLib.h>
#include <kernelLib.h>
#include <objLib.h>  
#ifndef __multi_core__
#include <taskVarLib.h>
#endif
#include <taskHookLib.h>
#include <memPartLib.h>
#include <intLib.h>
#include <taskLib.h>
#include <wdLib.h>
#include <semLib.h>
#include <msgQLib.h>
#include <cpu.h>
#include <reworks/printk.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <getopt.h>
#include <types/vxTypesOld.h>
#include <log.h>
#include <reworksio.h>
#include <mutex_show.h>
#include <sched.h>
#include <pthread.h>
#include <pthread_show.h>
#include <pthread_hook.h>
#include <mqueue.h>
#include <mqueue_show.h>
#include <cond_show.h>
#include <event.h>
#include <semaphore.h>
#include <sem_show.h>
#include <ratemon.h>
#include <ratemon_show.h>
#include <time.h>
#include <timer_show.h>
#include <wdg.h>
#include <wdg_show.h>
#include <rwlock_show.h>
#include <signal.h>
#include <signal_show.h>
#include <clock.h>
#include <memory.h>
#include <memory_show.h>
#include <mpart.h>
#include <mpart_show.h>
#include <hashLib.h>
#include <math.h>
#include <types/vxTypesOld.h>

#ifdef SW410_PLAT
#include <hostLib.h>
#include <inetLib.h>
#else
#include <ip/hostLib.h>
#include <ip/inetLib.h>
#include <ip/compat/routeLib.h> 
//typedef	unsigned int socklen_t;
#endif
 
#include <netInit.h>
//#include <sys/rtos_bsdnet.h>

/* VERSION-1256 */
//#include<ip/wrapperHostLib.h>
#include<arpLib.h>
#endif

/* 网络头文件 */	
#include <sys/socket.h>
//#include <sys/rtos_bsdnet.h>
//#include <net_ioctl.h> 
#include <sys/types.h>
#include <net/if.h>
#include <net/route.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h> 
#include <unistd.h> 
#include <time.h> 
#include "ipv4_ipv6.h"  //区分ipv4和ipv6，使用INET6宏进行区分

#ifdef _WRS_CONFIG_LP64
#include <reworks/thread.h>
typedef thread_t T_thread_t;
#else
typedef int T_thread_t;   
#endif

#if !defined(_AFTER_RC1) || defined(_RTP_TEST)
//typedef int _Vx_usr_arg_t; 
#else
/* 1、ReDeV4.7.1、ReDeV4.7.2、ReDeV5.0RC2上未定义_Vx_usr_arg_t
 * 2、 在5.1版本上，vxworks vxTypes.h中的定义*/
#ifdef _WRS_CONFIG_LP64
typedef long _Vx_usr_arg_t;
#else
typedef int _Vx_usr_arg_t;
#endif 
 
#endif
#if 0
typedef enum       /* VXTEST_STATUS - return value from test case */
    {
    VXTEST_PASS   = 0,   /* Test passed perfectly */
    VXTEST_FAIL   = 1,   /* Test result differed from expected */
    VXTEST_ERROR  = 2,   /* DEPRECATED - use VXTEST_ABORT instead */
    VXTEST_ABORT  = 3,   /* Inconclusive - test couldn't run properly */    
    VXTEST_SKIP   = 4    /* Test is skipped  */
    } VXTEST_STATUS;
#endif
#define OK  0
#if defined(_USER_TEST) && defined(_RTP_TEST)
#define OS_ERROR (-1)
#endif
#define NET_PASS        0
#define NET_FAIL        1
#define NET_UNRESOLVED  2
#define NET_UNSUPPORTED 4
#define NET_UNTESTED    5
    
#define CLEAN_WORKS // 功能测试中清理环境的宏
//#define OLD_NET   /*测试增强网络协议栈请注释掉此宏*/
#define BUFSIZE 512 
#define BUFLEN 1024
#define TESTNUM 10 
#define GROUPIP "230.230.230.230"  
#define port 10412
#define backlog 16  
#define NET_HOST_NAME "em0"
 

    
#ifdef _RTP_TEST
//#ifndef _KERNEL_TEST	
extern SEM_ID g_semBID; // 用于和 rtp 的main() 函数同步
//#endif
#endif
 
extern int console_fd;
extern char MSG[128];
//extern char *if_name_unit;
extern char *if_name_unit_error;
extern void NET_GoOnChoice(char *testid,int ret_value);
extern pthread_t net_Test(void);
extern void *net_test_task(void *arg);
extern pthread_t test_thread(void (*func)(int), int pri); 

/* 自动化总测试入口 */
extern void net_Lib_test();

/* 网络接口测试 */
extern void netApi_test(); 

/* vx_test网络接口测试 */
extern void SockLib_Test();

/*网络ioctl测试*/
extern void NetIoctl_Test();

/*网络select测试*/
extern void NetSelect_Test();
extern int NetSelect_Test1();
extern int NetSelect_Test2();

/*route add测试*/
extern void route_add_test();
extern int route_add_test_task(); 
/*arp_add测试*/
extern void arp_test();
extern int arp_add_test_task1(); 
extern int arpAdd_test_task1(); 
extern int arp_add_test_task2(); 
extern int arpAdd_test_task2(); 
extern int arp_add_test_task3(); 
extern int arpAdd_test_task3(); 
extern int arp_delete_test_task4(); 
extern int arpDelete_test_task4(); 
/* hostLib_Test测试*/
extern void hostLib_Test();

/* inet_Test测试*/
extern void inet_Test();

/* Netdb_Test测试*/
extern void Netdb_Test();

extern void getip(struct ifreq*ifr, char* addr);

extern void Ifconfig_Test();

/**********************非自动化测试**************************************/
/* 组播测试 */
extern int multcastGet_Test();
/* 广播测试 */
extern int BroadGet_Test(int prot_num);

/*网络读写测试*/
extern void NetRead_Write_Test();
extern int NetRead_Test();
extern int NetWrite_Test();

#ifdef _PRODUCT_TEST

/* 时钟频率问题，一般不测试 */
extern int net_sysclock_test(void);

/* 网络tftp client测试 */
extern void tftp_test();

/* 网络select测试*/
extern void *net_server_entry(void *arg);
extern int test_select(void);
#endif

/* vxPing.c*/
#define TASK_PRI       100   /* Priority of spawned tasks */
#define TASK_STACK_SIZE  10000 /* stack size of the spawned tasks */
#define ICMP_HDRLEN	 sizeof (struct icmp)
#define IP_HEAD		 sizeof (struct ip)
#define MY_ICMP_ID     0x1234

/* Networking */
extern int echoTcpClientSock (char *, int); /* set up TCP socket */
extern void echoTcpClient (int, int, char*); /*UDP echo client*/
extern int vxUdpClient();
extern int vxUdpServer();
extern int vxPing(char *hostName, int numPackets);
