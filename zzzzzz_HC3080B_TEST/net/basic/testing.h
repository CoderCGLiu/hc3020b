/*******************************************************************************
 * 版权：
 * 		 中国电子科技集团公司第三十二研究所
 * 描述：
 * 		测试主要参数定义，平台信息定义。
 * 		
 * 修改：
 * 		 2013-06-18，宋伟，创建. 
 * 		
 */

#ifndef __TESTING_H__
#define __TESTING_H__

#define _PRODUCT_TEST

#include <reworks/types.h>
#include <irq.h>
#include <udelay.h>
#include <io_asm.h>
#include <vxWorks.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <select.h>

//#include <plat_defined.h> //ajm，针对找不到VX_MAX_SMP_CPUS增加该头文件包含
/*Clib 添加头文件*/
#include <wchar.h>
#include <wctype.h>
#include <float.h>
#include <locale.h>
#include <langinfo.h>

#ifndef ZFML
#include <ioLib.h>
#include <logLib.h>
#include <pipeDrv.h> 
#include <iosLib.h>
#include <sysLib.h>
#include <memLib.h>
#include <errnoLib.h> 
#include <tickLib.h>
//#include <usrLib.h>
#include <kernelLib.h>
#include <objLib.h>   
#endif

#ifndef __multi_core__
#include <taskVarLib.h>
#endif

#ifndef ZFML
#include <taskHookLib.h>
#include <memPartLib.h>
#include <intLib.h>
#include <taskLib.h>
#include <wdLib.h>
#include <semLib.h>
#include <msgQLib.h>
#include <types/vxTypesOld.h>
#include <vxworks.h>
#include <hashLib.h>
#endif

#include <cpu.h>
#include <reworks/printk.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <getopt.h>
#include <log.h>
#include <reworksio.h>
#include <mutex_show.h>
#include <sched.h>
#include <pthread.h>
#include <pthread_show.h>
#include <pthread_hook.h>
#include <mqueue.h>
#include <mqueue_show.h>
#include <cond_show.h>
#include <event.h>
#include <semaphore.h>
#include <sem_show.h>
#include <ratemon.h>
#include <ratemon_show.h>
#include <time.h>
#include <timer_show.h>
#include <wdg.h>
#include <wdg_show.h>
#include <rwlock_show.h>
#include <signal.h>
#include <signal_show.h>
#include <clock.h>
#include <memory.h>
#include <memory_show.h>
#include <mpart.h>
#include <mpart_show.h>
#include <math.h>

#ifndef ZFML
#include <ip/compat/routeLib.h> 
#include <arpLib.h>
#include <ip/inetLib.h>
#include <ip/hostLib.h>
#include <sys/rtos_bsdnet.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/route.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h> 
#include <unistd.h> 
#include <object_show.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <lwip/netdb.h>
#include <io/tty_ioctl.h>
#endif

/* 测量平台定义 */
//#define TARGET_SBC8641D
//#define TARGET_SBC8640D
//#define TARGET_P1020
//#define TARGET_PPC4080

//#define TARGET_PENTIUM

//#define TARGET_LOOGSON3A
//#ifdef TARGET_LOOGSON3A
//#define TARGET_LOONGSON3A_R2  	//中科梦兰
//#define TARGET_LOONGSON3A_R3		//3A3000 -ajm
//#endif
#define TARGET_LOONGSON2K
//#define TARGET_LOONGSON2H
//#define TARGET_LOONGSON2F
//#define TARGET_LOONGSON1B
//#define TARGET_HUARUI
//#define TARGET_SOPC80
//#define TARGET_LOOGSON3A_1000
//#define TARGET_LOOGSON2K1000
//#define TARGET_FT1500
//#define TARGET_CET32_FT1500 //32所FT1500套件
//#define TARGET_MPC7448
// #define TARGET_MPC8569
//#define S698PM_PLAT
//#define TARGET_ARM7_IMX6D_PLAT
//#define TARGET_ULTRASCALE
//#define TARGET_P4080

/**
 * 结果记录
 */
typedef struct inter_test_result
{
	char 		*testname;
	double         max;
	double         min;
	double         avg;
	double		avr;	
}INTER_TEST_RESULT; 

#define RECORD_INTER_FILENAME			"/inter_test_records.csv"
#define RECORD_OTHER_FILENAME			"/other_test_records.csv"
#define RECORD_BLOCK_FILENAME			"/block_test_records.csv"

#ifdef TARGET_LOOGSON3A
#define LOONGSON_3A_PLAT
#define __multi_core__
/* 核的数量 */
#define CPU_MAX_NUM	 4
#define TEST_PLAT  "龙芯3A"
//#define TEST_VERSION "ReDeV5.0RC2-LS3A-A150928"
#ifdef TARGET_LOONGSON3A_R3    //ajm
#define TEST_VERSION "ReDeV5.1.2-LS3A-a170309"     //ajm
#else                                              //ajm
#define TEST_VERSION "ReDeV5.0RC1-LS-A140624"
#endif
#endif

#ifdef TARGET_LOONGSON2K
#define LOONGSON_2K_PLAT
//#define __multi_core__
/* 核的数量 */
#define CPU_MAX_NUM	 2
#define TEST_PLAT  "龙芯2K"
#define TEST_VERSION "ReDeV5.0RC1-LS-A140624"
#endif

#ifdef TARGET_LOONGSON1B    
#define TEST_PLAT  "龙芯1B"
#define TEST_VERSION "ReDeV-LS1B"     
#endif

#ifdef TARGET_PENTIUM
#define TEST_PLAT  "X86"
#define TEST_VERSION "ReDeV4.7.2_X86_A20140812"
#endif

//#ifdef REWORKS_TEST
//#define TEST_SYSTEM "reworks"
//#else
//#define TEST_SYSTEM "vxworks"
//#endif

#define TEST_SYSTEM "reworks"


extern int inter_test_start(void);
extern int inter_test_end(void);
extern int inter_test_record(INTER_TEST_RESULT *result);
extern int other_test_start(void);
extern int other_test_end(void);
extern int other_test_record(INTER_TEST_RESULT *result);

/* 
 * 测试是否打印中间循环数据
 */
//#define ANALYSIS
/*
 * 数据类型定义 
 */
typedef void (*ISRFUNC)(void *);

/* 
 * 指令代码常数
 */
#define INST_NOP	0x60000000
#define INST_BLR	0x4e800020

/* 测试次数 */
#define TESTING_COUNT		1000	

#define JITTER_ITERATOR_COMP		8000000
#define TEST_ITER		100000 // jitter 测试中子循环的执行次数

/* 测试 malloc 接口时使用的内存大小 */
#define SIZE_SMALL  (64)
#define SIZE_LARGE  (4096 + 128 ) 
#define NMEMB (1024)

/* 为测试任务相关接口添加的  */
#define TASK_STACK_SIZE 1024

/*测试用例返回值*/
#define TS_PASS        0
#define TS_FAIL        1
#define TS_UNRESOLVED  2
#define TS_UNSUPPORTED 4
#define TS_UNTESTED    5 

/* 检测溢出 */
extern void overflow_test(void);
extern int mem_calibration_entry(void);
/*
 * 测试层实现的时间获取函数
 */
extern u32  sys_timestamp_freq(void);
extern u64 	sys_timestamp (void);

#define user_timestamp_init sys_timestamp_init
#define user_timestamp  sys_timestamp
#define user_timestamp_freq sys_timestamp_freq

 
extern u32 sys_clk_rate_get(void);
extern int sys_clk_rate_set(int ticks_per_second);
extern u64 readTime();
/*
 * 测试环境：是否关闭/打开中断
 */
extern void enable_pic();
extern void disable_pic(); 

/*
 * 测试环境
 */
extern int test_int_lock();
extern int test_int_unlock();
extern void atadev_create();
extern void satadev_create();

/* cache 影响测试 */
extern int cache_mbw_entry(void);
extern void cache_dhry(int loops);

/* cpu 测试 */
extern int cpu_whets();
extern int cpu_dhry();

/* float 测试 */
extern void testing_float();
 
/* 中断 测试 */
extern void testing_interrupt();

/* 时间抖动 测试 */
extern int testing_jitter(void);

/* 计时开销 测试 */
extern void testing_kaixiao();

/* mbw 测试 */
extern int mbw_entry(void);

/* 系统时钟开销 测试 */
extern void tick_time(void);

/* 实时性能 */
extern void realtime_test();

/*最长锁中断时间测试用例*/
extern void int_lock_test();

/*信号量混洗时间*/
extern void vxSem_shuffing();

/*vxworks 接口性能测试 */
extern void testing_vxworks();
extern void testing_taskSpawn();
extern void testing_taskDelete();
extern void testing_taskSuspend();
extern void testing_taskResume();
extern void testing_semBTake();
extern void testing_semBGive();
extern void statistical_analysis(u64 *array, u32 num, u32 flag);
extern void inter_statistical(char *testname,u64 *array, u32 num, u32 flag);
extern void ContextSwitchTimer_vx( int iteration);
extern void testing_Malloc(int size);
extern void testing_Free(int size);
extern void testing_Calloc (int nmemb, int size);
extern void testing_Realloc (int size);
extern void test_mem();


#define BM_SEM_B 1
#define BM_SEM_C 2
#define BM_SEM_M 3

extern void testing_semBTest();
extern void testing_semCTest();
extern void testing_semMTest();
extern void bmSemCreate( int semType);
extern void bmSemDelete( int semType);
extern void bmSemFlush( int semType);
extern void bmSemGiveQEmpty(int semType);
extern void bmSemGiveQFull(int semType);
extern void bmSemGiveTake(int semType);
extern void bmSemTakeEmpty (int semType);
extern void bmSemTakeFull(int semType);

extern void testing_MQTest();
extern void testing_MQCreate();
extern void testing_MQDelete();
extern void testing_MQSendNoPend();
extern void testing_MQSendPend();
extern void testing_MQSendQFull();
extern void testing_MQRecvAvail();
extern void testing_MQRecvNoAvail();

extern void testing_EventTest();
extern void testing_EventSendNoPend();
extern void testing_EventSendPend();
extern void testing_EventReceiveAvail();
extern void testing_EventReceiveNoAvail();

extern void testing_WDTest(); 
extern void sem_shuffling();


#endif
