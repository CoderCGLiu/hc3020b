#ifndef __REWORKS_OTHERTEST_H__
#define __REWORKS_OTHERTEST_H__
#include "plat_defined.h"
#define TS_PASS        0
#define TS_FAIL        1
#define TS_UNRESOLVED  2
#define TS_UNSUPPORTED 4
#define TS_UNTESTED    5 

#define PTHREAD_STACK_SIZE 1024*4
#define PRI 110 

/* defines ExecHandling */
#define PIPE_NAME		"/pipe/server"
#define NUM_MSGS                10

#define CORRECT 1 

#define user_timestamp  sys_timestamp
#define user_timestamp_freq sys_timestamp_freq
 
extern u32 sys_timestamp_freq(void);
extern u64 sys_timestamp (void);

 
#ifdef _RTP_TEST 
#define u32 UINT32
#define u64 UINT64
#define u16 UINT16
#if defined(PENTIUM_PLAT) && defined(_KERNEL_TEST)
#define RAM_SIZE 8
#define BUFFER_LEN				1024*1024*1023
#define FILESIZE 8
#endif
#else

#ifdef PENTIUM_PLAT
#ifdef __multi_core__
/* 多核版本 内存只能配置成 256MB（物理内存是 2GB）*/
#define RAM_SIZE 200
#define BUFFER_LEN				1024*1024*150
#define FILESIZE 150
#else
#define RAM_SIZE 500
#define BUFFER_LEN				1024*1024*400
#define FILESIZE 400
#endif
#endif

#ifdef LOONGSON_3A_PLAT 
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif

#ifdef LOONGSON_2K_PLAT 
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif

#ifdef HUARUI2_SMP_64_PLAT 
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif


#ifdef PPC_8641D_PLAT 
#define RAM_SIZE 200
#define BUFFER_LEN				1024*1024*150
#define FILESIZE 150
#endif

#ifdef LOONGSON_1B_PLAT 
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#ifdef LOONGSON_2H_PLAT 
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#ifdef PPC_1020_PLAT 
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif

#ifdef PPC_8245_PLAT 
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#ifdef PPC2041_PLAT
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*100
#define FILESIZE 200
#endif 

#ifdef PPC4080_PLAT
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*100
#define FILESIZE 200
#endif 

#ifdef PPC8377_PLAT
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#ifdef SW410_PLAT
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#ifdef ARMv7_IMX6Q_PLAT
#define RAM_SIZE 100
#define BUFFER_LEN				1024*1024*50
#define FILESIZE 50
#endif

#if defined ARM7_CC_FT1500_PLAT || defined ARM7_CET32_FT2000_PLAT || defined ARM7_CET32_FT1500_PLAT
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif

#ifdef ARM7_FT2000_PLAT
#define RAM_SIZE 400
#define BUFFER_LEN				1024*1024*200
#define FILESIZE 200
#endif
#endif


#ifdef _WRS_CONFIG_LP64
#include <ReWorks/thread.h>
#define T_thread_t  thread_t
typedef long T_tick_t;   
#else
typedef int T_thread_t;   
typedef int T_tick_t;   
#endif

#if !defined(_AFTER_RC1) || defined(_RTP_TEST)
//typedef int _Vx_usr_arg_t; 
#else
/* 1、ReDeV4.7.1、ReDeV4.7.2、ReDeV5.0RC2上未定义_Vx_usr_arg_t
 * 2、 在5.1版本上，vxworks vxTypes.h中的定义
#ifdef _WRS_CONFIG_LP64
typedef long _Vx_usr_arg_t;
#else
typedef int _Vx_usr_arg_t;
#endif 
*/
#endif


/*startTaskTest 中，创建TEST_TASK_LOOP个任务，每个任务开辟TEST_TASK_STACK栈，操作TEST_TASK_BUF MB数据*/
#define TEST_TASK_LOOP	10			//开辟任务数
#define TEST_TASK_STACK	0x200000	//任务栈大小2MB
#define TEST_TASK_BUF	0x1c0000	//操作数据量
#define TEST_TASK_PRIORITY	120		//任务优先级


extern void Other_GoOnChoice(char *testid,int ret_value); 
extern void test_prepare();
extern pthread_t  test_thread(void (*func)(int), int pri);
extern void *ReWorks_OtherTest_task(void *arg);
extern pthread_t ReWorks_OtherTest(void);  

/********************ReWorks other test*********************************/
/* 自动化测试总入口 */
extern void ReWorks_AUTO();

/* os_Test 测试*/
extern void os_Test();
extern int sysClkRateGet_Set_test();
extern int timeslice_zero_test();
extern int vx_sys_clk_rate_test1();
extern int tickSet_Get_test();
extern int test_oi();
extern int sys_info_get_test();
extern int pthread_cancel_state();
//extern int mutex_test();
extern int startTaskTest();

extern void backtrace_test();
extern void spe_restore_save_test();   

/* mmu测试 */
extern void cache_mmu_test();
extern int mmu_add_item_task();
extern int mmu_icache_dcache_test();
extern int mmu_icache_test();
extern int mmu_dcache_test();
extern int icache_dcache_test();
extern int icache_only_test();

/* cache校验 */
extern int mem_calibration_entry(void);
extern int mbw_entry(void);

/* 内存测试 */
extern void test_large_prepare();
extern int large_memory_Test();
extern int memory_stat_hook(); 
 

/* printf 测试*/
extern void printf_test();
extern int printk_test();
extern int printf_tab_test();
extern int sprintf_test();

/* C库非自动化 测试*/
extern void CLib_other_test(); 


#ifndef _RTP_TEST
/* ReWorks PCI test, RTP 版本不支持 */
extern void pci_device_show_test();
extern void pci_test_8139();
extern void pci_test_82574();

/* 中断嵌套测试, RTP 版本不支持， 多核不支持 */
extern void int_nest_test_8641d();
extern void int_nest_test_8640d();
extern void int_nest_test_1020();
extern void int_nest_test_3803();
extern void int_nest_test_timer_loongson3A(void);
extern void int_nest_test_uart_loongson3A(void);

/* 异常挂接测试 */
extern void test_exc_func(int vec, REG_SET *pregs);
extern void test_exc_install_handler();
extern void test_exc_uninstall_handler();
extern void test_exc0x700_install();
extern void test_exc0x700_uninstall();



#ifdef _PRODUCT_TEST
/* 容量测试 */
typedef struct rongLiangTest {
   int max_num;
   int current_num;
} rongLiangTest;

/* 容量测试总入口，可以连跑测试 */
extern void test_rongliang();
extern struct rongLiangTest object_getinfo(char *testname);
extern int mq_longliang_test();
extern int sem_longliang_test();
extern int timer_longliang_test();
extern int wdg_longliang_test();
extern int mutex_longliang_test();
extern int rms_longliang_test();
#endif

#else
/* 错误检测，仅RTP版本测试 */
extern int error_report_show_test();
extern void heapErr (void);
extern void heaptest (void);

#endif 

extern void tick_sysClk_Test();
extern int sysClkRateGet_Set_test();
extern int tickSet_Get_test();

/* 异常测试 */
extern void exception_pentium(int excp);
extern void exception_loongson(int excp);
extern void exception_ppc_8641d(int excp);
extern void exception_ppc_1020(int excp);
extern void exception_ppc_4080(int excp);
extern void exception_huarui(int excp);

extern void skip_exc_test(); 

/* 龙芯3A 创建printf打印浮点（"%f"格式打印）的任务，删除任务时用户堆内存会减少128字节 */
extern void test_printf_task_start();
extern void test_printf_task_stop();

/* 零地址异常测试 */
extern void exception_addr0_test1();
extern void exception_addr0_test2();
extern void exception_addr0_test3();

/* RTC_Test 测试 */
extern void RTC_Test();
extern int rtc_read_test(void);
extern int rtc_write_test(int year,int month,int day,int hour,int min,int sec,int wday);

/* 多核监控服务，单多核支持 */
extern void monitor_test();

/* 835 测试矢量库 */
//extern STATUS testContinuumTask(UINT32 samples,UINT8 time);
extern int testVsipTask(int L,int times);

/* LS3A3000,LS2K,FT1500,FT2000添加TLS支持 */
extern int TLS_Test();

/* 龙芯浮点数异常问题 */
extern int fptest2();
extern int fptest3();
extern int fptest4();
extern int fptest5();
extern int fptest6();
extern int fptest7();
extern int fptest8();
extern int fptest9();
extern int fptest10();
extern int fptest11();

/********************SMP test*********************************/
/* 多核测试 */
#ifdef __multi_core__
extern void SMP_Test();
extern void spinlock_test();
extern int example_mq_receive_smp();
extern int example_sem_wait_smp();
extern int pthread_create3_test1();
extern void Error_func_test();
extern void MEM_BARRIER_RW_Test(void);
extern void MEM_BARRIER_W_Test(void);
extern void MEM_BARRIER_R_Test(void);
extern void multi_cpu_test_start();
extern int PI(int nCores); 

/* 多核任务管理模块 */
extern int cpu_id_get_test_1();
extern int cpu_id_get_test_2();
extern int cpu_isup_test_1();
extern int cpu_isup_test_2();
extern int cpu_isup_test_3();
extern int cpu_isup_test_4();
extern int get_up_cpuset_test_1();
extern int num_online_cpus_test_1();
extern int pthread_affinity_get_test_1();
extern int pthread_affinity_get_test_2();
extern int pthread_affinity_set_test_1();
extern int pthread_affinity_set_test_2();
extern int pthread_affinity_set_test_3();
extern int pthread_affinity_set_test_4();
extern int pthread_create3_test_1();
extern int pthread_create3_test_2();
extern int pthread_create3_test_3();
extern int pthread_create3_test_4();
extern int pthread_create3_test_5();
extern int pthread_create3_test_6();
extern int pthread_create3_test_7();
extern int pthread_create3_test_8();
extern int pthread_create3_test_9();
extern int pthread_create3_test_10();
extern void cpu_id_get_test();
extern void cpu_isup_test();
extern void get_up_cpuset_test();
extern void num_online_cpus_test();
extern void pthread_affinity_get_test();
extern void pthread_affinity_set_test();
extern void pthread_create3_test();

extern void dosomething1();
extern void bond_task(pthread_t tid);
extern void *SMP_test_task_1();
extern void *SMP_test_task_2();
extern void *SMP_test_task_3();
extern void *SMP_test_task_4();
extern void *SMP_test_task_5();
extern void *SMP_test_task_6();
extern void *SMP_test_task_7();
extern void *SMP_test_task_8();

/* 多核轮转调度测试 */
extern void SMP_dosomething();
extern void *SMP_task_1();
extern void *SMP_task_2();
extern void *SMP_task_3();
extern void *SMP_task_4(); 
extern int SMP_schedule_test1();
extern int SMP_schedule_test2();

/* 多核调度管理 */
extern int task_schedule_test1();
extern int task_schedule_test2();
extern int task_schedule_test3();

/* 多核自动分配和负载*/
extern int SMP_Assign_Load_test1();
extern int SMP_Assign_Load_test2();
extern int SMP_Assign_Load_test3();
extern int SMP_Assign_Load_test4();
extern int SMP_Assign_Load_test5();
extern int SMP_Assign_Load_test6();

/* 多核绑核测试 */
extern void task_affinity_test();
extern int task_affinity_test1();
extern int task_affinity_test2();
extern int task_affinity_test3();
extern int task_affinity_test4();
extern int task_affinity_test5();
extern int task_affinity_test6();
extern int task_affinity_test7();
extern int task_affinity_test8();
extern int task_affinity_test9();
extern int task_affinity_test10();

extern void *task_lock_1();
extern void *task_lock_2();
extern void *task_lock_3();
extern void *task_lock_4();
extern void *task_lock_5();
extern void *task_lock_6();
extern void *task_lock_7();
extern void *task_lock_8();

extern int int_cpu_lock_test1();
extern int int_cpu_lock_test2();
extern int int_cpu_lock_test3();
extern int int_cpu_lock_test4();
extern int int_cpu_lock_test5(); 
extern int int_cpu_lock_test6(); 
extern int pthread_cpu_lock_test1();
extern int pthread_cpu_lock_test2();
extern int pthread_cpu_lock_test3();		
extern int spin_lock_test1();
extern int spin_lock_test2();
extern int spin_lock_test3();
extern int spin_lock_irq_test1();
extern int spin_lock_irq_test2();
extern int spin_lock_irq_test3();
extern int spin_lock_irq_test4();
extern int spin_lock_irq_test5();
extern int spin_lock_irq_test6();

/* CPC 核间中断测试*/
extern int example_cpc_call();
extern int example_cpc_broadcast();

extern void cpc_test();
extern int example_cpc_call_1();
extern int example_cpc_call_2();
extern int example_cpc_call_3();
extern int example_cpc_broadcast_1();
extern int example_cpc_broadcast_2();
extern int example_cpc_broadcast_3();
#endif

/********************VxWorks other test*********************************/
extern void VxWorks_AUTO();
extern void vxAtomicTest();
extern void vx_log_fd_test();
extern void vx_fioLib_test();
extern void memDrv_test();
extern void vx_pci_test();
extern void re_vx_test();
extern void vx_pci_test();

extern int iosDevShow_test1();
extern int iosDrvRemove_test1();
extern int iosDrvRemove_test2();
extern int iosDrvRemove_test3();
extern int iosdevlist_test();
extern void vxMemDev_drive();
extern void pty_test();
/* vxworks demo */
/* POSIX */
extern void setup();

/* ExecHandling */
extern int recoverDemo();
extern int posixTimerStartDemo();
extern int recoverExcDemo();
extern int deadlineWdDemo();

/********************IO other test*********************************/

#if defined(_RTP_TEST) && defined(_USER_TEST)
#define OS_ERROR ERROR
#endif

#define FS_LOOP 10 

#define ATA_FILE  "/ata0a/ata_file"
#define RAM_FILE  "/c/ram_file" 

extern void IO_Test();
#if defined(_PRODUCT_TEST) || defined(PENTIUM_PLAT) || defined(LOONGSON_3A_PLAT)
extern void test_ata_dosfs_performance();
#endif
extern void create_file_prepare2(void);
extern void file_Test();
extern int test_write(int wsize);
extern int write_ftruncate(int clen);
extern int Reworks_adapter_test(void);
extern int test_filesystem();
extern int write_profile_string(char* chAppName,char* chKeyName,char* chValue,char* chFileName);
extern int example_monitor_fsinfo(); 
extern int write_profile_string_test();


/**TTY SELECT ***/
extern int tty_select(void);
extern int tty_select_multi(void);


/**TTY ioctl ***/
extern void tty_io_test();

/**键码转换模块*/
extern int keycode_test(void);

#endif
