/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2013-6-6
 * 
 */

#include "../basic/testing.h"
#include "../basic/block_config.h"
/**
 * 写测试文件大小 
 */
int WRITE_FILESIZE;
int block_fildes = -1;
BLOCK_TEST_RESULT	block_result;

#define BLOCK_HEADER  "testname,write速度,read速度,fwrite速度,fread速度,文件系统类型,硬件平台_对比,操作系统_对比,版本_对比\n"
#define BUF_LEN	256

int block_test_start(void)
{
	block_fildes = -1;
	if(-1 == (block_fildes = open(RECORD_BLOCK_FILENAME, O_CREAT | O_RDWR | O_APPEND, 0777)))
	{
		return -1;
	}
	printf("record_test_start\n");
	
	write(block_fildes, BLOCK_HEADER, strlen(BLOCK_HEADER));
	
	return 0;
}

int block_test_record(BLOCK_TEST_RESULT *result)
{
	int i;
	char buffer[BUF_LEN];
 
	for(i = 0; i < ITERATOR ; i++)
	{
		sprintf(buffer, "%-25s,%10.6f,%10.6f,%10.6f,%10.6f,%s,%s,%s,%s\n", result->testname,
			result->write_avg[i], result->read_avg[i], result->fwrite_avg[i], result->fread_avg[i],
			result->fskind, TEST_PLAT,TEST_SYSTEM, TEST_VERSION);

		write(block_fildes, buffer, strlen(buffer)); 
	}
	
	return 0;
}

int block_test_end(void)
{
	block_test_record(&block_result);
	if(-1 == block_fildes)
	{
		return -1;
	} 
	
	close(block_fildes);
	
	return 0;
}
