#include "iperf.h"

#define MAXARGS 50
#define Server 1
#define TCP_client 2
#define UDP_client 3

/************************************************
 * 测试名称：benchmark测试-Iperf
 * 测试标识：BM_IPERF
 * 用例版本：U.S. Department of Energy V3.7
 * 测试内容：使用Iperf测试网络性能。
 * 测试说明：测试最大TCP和UDP带宽性能。 
 ***********************************************/
int iperf3(char * paramString /* a string of command options. */) {
	int argc;
	char *tempArgv[MAXARGS + 2];
	char *newString = NULL;
	char **argv = tempArgv;
	int ret = 0;
	int alen = 0;

	if (paramString == NULL) /* let ipnet_cmd_route() prints the usage */
	{
		argv[0] = "iperf3";
		argc = 1;
		goto cmd;
	}

	alen = strlen(paramString) + 1;
	if ((newString = calloc(1, alen)) == NULL) {
		printf("iperf3: calloc failed\n");
		return -1;
	}

	strcpy(newString, paramString);
	ret = getOptServ(newString, "iperf3", &argc, argv, MAXARGS + 2);
	if (ret != 0) {
		printf("Too many arguments.\n");
		//errnoSet(EINVAL);
		goto done;
	}

cmd: if ((ret = iperf_main(argc, argv)) == 0) {
		ret = 0;
		goto done;
	}

	/* Convert the IPCOM error code to VxWorks errno */
	//errnoSet(IPCOM_ERRNO(ret)); //wn 2012.5.11
	ret = -1;

done: if (newString)
		free(newString);
	return (argc == 1) ? 0 : ret; /* override ret if usage is printed */
}
