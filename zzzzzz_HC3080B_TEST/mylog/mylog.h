#ifndef _MYLOG_H_
#define _MYLOG_H_


/*设置打印级别 */
enum mylog_level
{
	MYLOG_LEVEL_NONE = 0,		/* 0 */
	MYLOG_LEVEL_DEBUG,		/* 1 */
	MYLOG_LEVEL_INFO,		/* 2 */
	MYLOG_LEVEL_WARN,		/* 3 */
	MYLOG_LEVEL_ERROR,		/* 4 */
	MYLOG_LEVEL_END
};

/*设置打印模块 */
enum mylog_module
{
	MYLOG_USER = 0,  /* 0 */
	MYLOG_CACHE,	/* 1 */
	MYLOG_CAN,		/* 2 */
	MYLOG_UART,		/* 3 */
	MYLOG_I2C,		/* 4 */
	MYLOG_SPI,		/* 5 */
	MYLOG_QSPI,		/* 6 */
	MYLOG_NAND,		/* 7 */
	MYLOG_LPC,		/* 8 */
	MYLOG_GPIO,		/* 9 */
	MYLOG_GMAC,		/* 10 */
	MYLOG_SRIO,		/* 11 */
	MYLOG_PCIE,		/* 12 */
	MYLOG_TIMER,		/* 13 */
	MYLOG_WATCHDOG,		/* 14 */
	MYLOG_RTC,		/* 15 */
	MYLOG_SDIO,		/* 16 */
	MYLOG_CDMA,		/* 17 */
	MYLOG_GDMA,		/* 18 */
	MYLOG_MODULE_END
};

/* 打印开关 */
enum mylog_switch
{
	MYLOG_OFF = 0,
	MYLOG_ON,
};


/* log 打印 重写 */
void mylog(int mod, int level, const char *func, int line, const char *fmt, ...);
/* 设置 打印开关和级别 */
void mylog_ctrl(int mod, int on1_or_off0, int level);

/*
 *@MYLOG
 */ 
#define MYLOG(mod, level, fmt, ...)  \
	mylog(mod, level, __FUNCTION__, __LINE__, fmt, ##__VA_ARGS__)


#endif /*_MYLOG_H_*/
