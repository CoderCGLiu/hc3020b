/*******************************************************************************
*file:         multicast.h 
*description:   
*property:      
*date:          201603     
*ver:           1.0
*author:        CSIC715 信号处理事业部
*******************************************************************************/ 

#ifndef MCFUN_H_
#define MCFUN_H_
/***************************************************************************/
//网络套接字类型自定义
/***************************************************************************/
typedef		int					SOCKET;
typedef 	struct in_addr 		IN_ADDR;
typedef 	struct sockaddr_in 	SOCKADDR_IN;
/***************************************************************************/
//高精度时间戳
/***************************************************************************/
extern unsigned long sys_timestamp();
typedef union
{
	unsigned long long timeVal;
	unsigned long val[2];
}sysTime;
/***************************************************************************/
//测试接口使用的结构体
/***************************************************************************/
typedef struct net_test_param
{
	char	*dstAddr;			//接收方的IP地址
	short 	dstPort;			//接收方使用的端口号
	short	lcPort;				//发送方使用的端口号
	unsigned int netPkgSize;	//每次发送/接收的数据包的大小
}netTstParam;
/***************************************************************************/
//Shell终端测试命令接口
/***************************************************************************/
//tcp测试使用的终端命令
void multicast_send(netTstParam *netParam);
void multicast_recv(netTstParam *netParam);

void *mcRecv_task(void *args);
void *mcSend_task(void *args);
void udp_send(netTstParam *netParam);

#endif
